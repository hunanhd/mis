﻿#include "stdafx.h"
#include "PropertyDlg.h"
#include "config.h"
#include "UIHelper.h"
#include "MineGE/HelperClass.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
IMPLEMENT_DYNAMIC( PropertyDlg, CDialog )
PropertyDlg::PropertyDlg( CWnd* pParent /*= NULL*/, CString func /*= _T("")*/ )
    : CDialog( PropertyDlg::IDD, pParent )
{
    m_showAll = true; // 默认显示全部数据
    m_func = func;
}
PropertyDlg::~PropertyDlg()
{
}
void PropertyDlg::DoDataExchange( CDataExchange* pDX )
{
    CDialog::DoDataExchange( pDX );
}
BEGIN_MESSAGE_MAP( PropertyDlg, CDialog )
    ON_BN_CLICKED( IDOK, &PropertyDlg::OnBnClickedOk )
END_MESSAGE_MAP()

// PropertyDataDlg 消息处理程序
BOOL PropertyDlg::OnInitDialog()
{
    CDialog::OnInitDialog();
    // 创建, 定位, 显示CMFCPropertyGridCtrl
    CRect rect;
    GetDlgItem( IDC_PROP_POS )->GetWindowRect( &rect );
    ScreenToClient( &rect );
    m_propertyDataList.Create( WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_BORDER, rect, this, ( UINT ) - 1 );
    m_propertyDataList.SetDescriptionRows( 4 );
	// 初始化
    this->Init();
    return TRUE;  // return TRUE unless you set the focus to a control
    // 异常: OCX 属性页应返回 FALSE
}
bool PropertyDlg::Init()
{
	m_propertyDataList.RemoveAll();
	if( m_objId.isNull() ) return false;
	//显示单独分组窗口名为分组名
	if( !m_func.IsEmpty() )
	{
		SetWindowText( m_func );
	}
	// 获取类型名称
	CString type;
	if( !ArxDataTool::GetTypeName( m_objId, type ) ) return false;
#ifdef USE_GROUP
	// 获取分组
	AcStringArray funcs, funcFieldsInfo;
	if( m_func.IsEmpty() )
	{
		FuncHelper::GetFuncs( type, funcs );
	}
	else
	{
		funcs.append( m_func );
	}
	if( funcs.isEmpty() ) return false;
	// 获取每个分组下的字段,记录到funcFieldsInfo数组中
	// 用$隔开
	int funcNum = funcs.length();
	for ( int i = 0; i < funcNum; i++ )
	{
		AcStringArray fields;
		FuncHelper::GetFields( type, funcs[i].kACharPtr(), fields );
		if( fields.isEmpty() ) continue;
		funcFieldsInfo.append( _T( "$" ) );
		funcFieldsInfo.append( funcs[i] );
		for( int j = 0; j < fields.length(); j++ )
		{
			funcFieldsInfo.append( fields[j] );
		}
	}
	if( funcFieldsInfo.isEmpty() ) return false;
	// 创建属性控件
	PropertyDataUpdater::BuildPropGridCtrlByFunc( &m_propertyDataList, type, funcFieldsInfo );
	// 填充属性数据
	bool ret = PropertyDataUpdater::ReadDataFromGEByFunc( &m_propertyDataList, m_objId );
#else
	//获取字段
	AcStringArray fileds;
	if( m_showAll )
	{
		FieldHelper::GetAllFields( type, fileds );
	}
	else
	{
		fileds.append( m_fields );
	}
	// 创建属性数据控件
	PropertyDataUpdater::BuildPropGridCtrl( &m_propertyDataList, type, fileds );
	// 填充属性数据
	bool ret = PropertyDataUpdater::ReadDataFromGE( &m_propertyDataList, m_objId );
#endif
	// 填充数据失败
	if( !ret )
	{
		m_propertyDataList.EnableWindow( FALSE );
		m_propertyDataList.ShowWindow( SW_HIDE );
	}
	else
	{
		m_propertyDataList.ExpandAll( TRUE );
	}
	return ret;
}
void PropertyDlg::OnBnClickedOk()
{
	// 保存数据
	this->Save();
    OnOK();
}
void PropertyDlg::Save()
{
    UpdateData( TRUE ); // 更新控件
    // 更新图元的属性数据
    PropertyDataUpdater::WriteDataToGEByFunc( &m_propertyDataList, m_objId );
}
void PropertyDlg::SetEntity( const AcDbObjectId& objId )
{
    m_objId = objId;
}
void PropertyDlg::AddField( const CString& field )
{
    m_fields.append( field );
}
void PropertyDlg::ShowAll( bool bFlag )
{
    m_showAll = bFlag;
}
