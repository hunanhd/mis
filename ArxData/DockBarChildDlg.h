#pragma once
#include "acui.h"
#include "Resource.h"
// 将非模态对话框的3个消息放在了基类DockBarChildDlg
// 这样派生类就可以只专注对话框本身的消息处理
// 同时减少重复代码
class DockBarChildDlg : public CAcUiDialog 
{
	DECLARE_DYNAMIC (DockBarChildDlg)
protected:
	DockBarChildDlg(UINT idd, CWnd *pParent =NULL, HINSTANCE hInstance = NULL) ;
	~DockBarChildDlg();
public:
	virtual void Save() {} // 子对话框关闭的时候保存数据,默认实现为空
protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnCommand (WPARAM wParam, LPARAM lParam) ;
	afx_msg void OnSize (UINT nType, int cx, int cy) ;
	afx_msg LRESULT OnAcadKeepFocus (WPARAM wParam, LPARAM lParam) ;
} ;
