#pragma once
#include "aduiPaletteSet.h"
#include "resource.h"
class PropertyPaletteSet : public CAdUiPaletteSet 
{    
	DECLARE_DYNCREATE(PropertyPaletteSet)
	PropertyPaletteSet();
protected:
	virtual void GetMinimumSize(CSize& size);
	virtual void GetMaximumSize(CSize& size);
	virtual BOOL PreTranslateMessage(MSG *pMsg);
	
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate (LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	//afx_msg void OnTimer(UINT_PTR nIDEvent); // ��ʱ��
public:
	void LoadPaletteSet();
	void SavePaletteSet();
};
