#pragma once
#include "aced.h"
/*
 * 使用反应器处理数据库的保存和关闭
 * 关闭dwg时消息的响应顺序:
 * commandWillStart --> beginDocClose( 或 docCloseWillStart) --> commandEnded --> kUnloadDwg
 AcApDocument* pDoc = curDoc();
 if( !ArxDwgHelper::IsNewDwg( pDoc ) )
 {
 pDoc->database()->saveAs( pDoc->fileName() );
 }
 */
class EditorReactor : public AcEditorReactor2
{
protected:
    bool mbAutoInitAndRelease ;
public:
    EditorReactor ( const bool autoInitAndRelease = true ) ;
    virtual ~EditorReactor () ;
    virtual void Attach () ;
    virtual void Detach () ;
    virtual AcEditor* Subject () const ;
    virtual bool IsAttached () const ;
    // 命令即将开始执行
    //virtual void commandWillStart( const ACHAR* cmdStr );
    // 命令执行结束
    //virtual void commandEnded( const ACHAR* cmdStr );
    // dwg文件保存消息响应
    virtual void saveComplete ( AcDbDatabase* pDwg, const ACHAR* pActualName );
    // 文档即将关闭(docCloseWillStart在kUnloadDwg之前触发)
    //virtual void docCloseWillStart( AcDbDatabase* pDwg );
	// 监视PickSet的变化
	virtual void pickfirstModified(void);
    // 鼠标双击(有问题)
    //virtual void beginDoubleClick(const AcGePoint3d& clickPoint);
};
