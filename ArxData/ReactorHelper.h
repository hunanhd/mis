#pragma once
class ReactorHelper
{
public:
    static void CreateEditorReactor();
    static void RemoveEditorReactor();
    static void CreateDocManagerReactor();
    static void RemoveDocManagerReactor();
};
