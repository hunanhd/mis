#include "StdAfx.h"
#include "PropertyPaletteSet.h"
#include "Propertypalette.h"
//#include "XmlHelper.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
// The file name of the persisted paletteset
#define PALETTESET_FILENAME _T("TestPaletteSet.xml")
//#define TIMERID 8
IMPLEMENT_DYNCREATE(PropertyPaletteSet, CAdUiPaletteSet)
BEGIN_MESSAGE_MAP(PropertyPaletteSet, CAdUiPaletteSet)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	//ON_WM_TIMER()
END_MESSAGE_MAP()
PropertyPaletteSet::PropertyPaletteSet()
{
	
}
// Called by the palette set framework to determine size constraints.
// Override these methods to provide minimum and maximum palette set sizes.
void PropertyPaletteSet::GetMinimumSize(CSize& size)
{
	size.cx = 300;
	size.cy = 600;
}
void PropertyPaletteSet::GetMaximumSize(CSize& size)
{
	size.cx = 300;
	size.cy = 600;
}
BOOL PropertyPaletteSet::PreTranslateMessage(MSG *pMsg)
{
	if ( pMsg->message == WM_CHAR || pMsg->message == WM_SYSCHAR )
	{
		return CWnd::PreTranslateMessage(pMsg);
	}
	else
	{
		return CAdUiPaletteSet::PreTranslateMessage(pMsg);
	}
}
int PropertyPaletteSet::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CAdUiPaletteSet::OnCreate(lpCreateStruct) == -1)
		return -1;
	//SetTimer(TIMERID,1000,0);  //这里就相当于设定了timer,如果要停掉timer就是KillTimer(TIMERID)
	return 0;
}
void PropertyPaletteSet::OnDestroy()
{
	// Save PaletteSet data
	SavePaletteSet();
	CAdUiPaletteSet::OnDestroy();
}
void PropertyPaletteSet::LoadPaletteSet()
{
	//MSXML::IXMLDOMDocumentPtr pDoc = NULL;
	//HRESULT hr = pDoc.CreateInstance(CLSID_DOMDocumentAcad);
	//// Get the file where the palette set's properties are saved
	//const TCHAR *pRoam;
	//TCHAR paletteBuffer[MAX_PATH];
	//BOOL bResult=acdbHostApplicationServices()->getRoamableRootFolder(pRoam);
	//_tcscpy(paletteBuffer, pRoam);
	//_tcscat(paletteBuffer, PALETTESET_FILENAME);
	//CComVariant var(paletteBuffer);
	//VARIANT_BOOL bReturn = pDoc->load(var);
	//if (bReturn == VARIANT_TRUE) // success!
	//{
	//	MSXML::IXMLDOMNodePtr pNode;
	//	GetChildNode(pDoc->documentElement,_T("PS"),&pNode);
	//	// Call the base class version of Load.
	//	// This would re-establish the properties of
	//	// the palette set
	//	Load(pNode);
	//}
}
void PropertyPaletteSet::SavePaletteSet()
{
	//// Persist the palette set data in TestPaletteSet.xml
	//MSXML::IXMLDOMDocumentPtr pDoc = NULL;
	//HRESULT hr = pDoc.CreateInstance(CLSID_DOMDocumentAcad);
	//// Save pallete set into roaming profile
	//const TCHAR *pRoam;
	//TCHAR paletteBuffer[MAX_PATH];
	//BOOL bResult=acdbHostApplicationServices()->getRoamableRootFolder(pRoam);
	//_tcscpy(paletteBuffer, pRoam);
	//_tcscat(paletteBuffer, PALETTESET_FILENAME);
	//CComVariant var(paletteBuffer);
	//// create a root element
	//MSXML::IXMLDOMNodePtr pNode;
	//if (AddChildNode(pDoc, _T("PS"), MSXML::NODE_ELEMENT, &pNode) == FALSE)
	//	return;
	//// This calls the base class implementation of Save 
	//// The base class adds its properties (as XML nodes) into pNode
	//Save(pNode);
	//// Save the xml document
	//hr = pDoc->save(var);
}
//static void RandomBackground(MyRandom& rng, const AcDbObjectId& objId)
//{
//	Adesk::UInt8 r = rng.randomInt(0, 255);
//	Adesk::UInt8 g = rng.randomInt(0, 255);
//	Adesk::UInt8 b = rng.randomInt(0, 255);
//	EntityData::SetDouble( objId, _T("填充"), (double)ArxUtilHelper::RGB_2_LONG(r, g, b) );
//}
//
//void PropertyPaletteSet::OnTimer(UINT_PTR nIDEvent)
//{
//	// 当前选择集中的图元id
//	static AcDbObjectIdArray objIds;
//
//	if( nIDEvent == TIMERID )
//	{
//		//CPoint pt;
//		//GetCursorPos(&pt);
//		//CRect rc;
//		//GetWindowRect(&rc);
//		//if(!rc.PtInRect(pt))
//		//{
//		//	// 鼠标在窗口外,更新数据
//		//	//AfxMessageBox(_T("鼠标在窗口外!"));
//		//}
//		//acutPrintf(_T("\n计时器printf"));
//
//		ArxDocLockSwitch lock_switch;
//
//		if(objIds.isEmpty())
//		{
//			ArxUtilHelper::GetPickSetEntity( objIds );
//		}
//
//		MyRandom rng;
//		for(int i=0;i<objIds.length();i++)
//		{
//			RandomBackground(rng, objIds[i]);
//		}
//	}
//}