#pragma once
/*
ObjectARX Developer's Guide --> Advanced Topics --> The Multiple Document Interface --> MDI-Enhanced Level Expand All
These additional steps will make your application integrate completely with the MDI.
1、Consider migrating your AcEditorReactor::commandXxx() and AcEditorReactor::LispXxx() callbacks to work instead from
   AcDocMananagerReactor::documentLockModeWillChange() and AcDocMananagerReactor::documentLockModeChanged() notifications.
   Then they will account for application execution context operations that were previously difficult to detect by ObjectARX applications.
2、Avoid using acedCommand(). Use AcApDocManager::setStringToExecute() instead, because it has a document parameter.
3、Avoid using the kLoadDwg and kUnloadDwg cases, and use the documentToBeCreated() and documentToBeDestroyed() reactors instead.
4、Support the document-independent database feature. For more information, see  Document-Independent Databases.
5、Support multi-document commands. For more information, see  Multi-Document Commands.
6、Support all events that occur within the application execution context. For more information, see Application Execution Context.
根据地1、3条的建议,在mdi中最好用AcApDocManagerReactor类型反应器替代kLoadDwg、kUnloadDwg、AcEditorReactor::commandXxx等消息
AcApDocManagerReactor有个很严重的问题:
    直接双击启动cad时新建的dwg或打开的dwg并不会触发documentCreated()
    只有点击cad里的"新建"或"打开"菜单才会触发documentCreated()!!!
*/
class DocManagerReactor: public AcApDocManagerReactor
{
public:
    DocManagerReactor();
    // dwg文件切换
    virtual void documentToBeActivated( AcApDocument* pDoc );
    // 替代kLoadDwg消息(新建dwg文件、打开dwg文件)
    virtual void documentCreated( AcApDocument* pDoc );
    // 替代kUnloadDwg(dwg文件关闭)
    virtual void documentToBeDestroyed( AcApDocument* pDoc );
    virtual void documentLockModeWillChange(
        AcApDocument* pDoc,
        AcAp::DocLockMode myCurrentMode,
        AcAp::DocLockMode myNewMode,
        AcAp::DocLockMode currentMode,
        const ACHAR* pGlobalCmdName );
    virtual void documentLockModeChangeVetoed(
        AcApDocument* pDoc,
        const ACHAR* pGlobalCmdName );
    virtual void documentLockModeChanged(
        AcApDocument* pDoc,
        AcAp::DocLockMode myPreviousMode,
        AcAp::DocLockMode myCurrentMode,
        AcAp::DocLockMode currentMode,
        const ACHAR* pGlobalCmdName );
};