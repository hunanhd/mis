#include "StdAfx.h"
#include "ContextMenuHelper.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"

#include "cmdarx.h"         // command mode 
#include "defaultarx.h"     // default mode
#include "editarx.h"        // edit mode

cmCommandContext *gpCmCommand;
defaultContext *gpDefContext;
myEntityContext *gpEntMC;
myCircleContext *gpCircMC;
myLineContext *gpLineMC;

void ContextMenuHelper::Init(void* pAppID)
{	
    gpCmCommand = new cmCommandContext;   // command mode
    gpDefContext = new defaultContext;    // default mode
    gpEntMC = new myEntityContext;        // edit mode - for all entites
    gpCircMC = new myCircleContext;       // edit mode - for circle(s)
    gpLineMC = new myLineContext;         // edit mode - for line(s)

    acDocManager->pushResourceHandle(_hdllInstance);

    // ��̬ע������,������������صĲ˵�
    CString str1 = _T("JL");
    CString str2 = _T("TestContextMenu");
    CString str3 = _T("TestContextMenu");
    // For command mode, supply ptr to AcEdUIContext object when
    // regestering the command with the editor.
    acedRegCmds->addCommand(str1, str2, str3, ACRX_CMD_MODAL, &cm, gpCmCommand);

    //    CString menuLabel;
    //    menuLabel.LoadString(IDS_CMDARX_MENU_LABEL);

    // No name is given for the menu, thus AutoCAD will use the filename
    // without the arx app as the name. In this case it will be "contextmenu"
    // to give the name you should call
    // acedAddDefaultContextMenu(&gpDefContext,gpAppID,name);  
    Adesk::Boolean b = acedAddDefaultContextMenu(gpDefContext, pAppID);
    assert(b == Adesk::kTrue);

    // gpEntMC is added to the class AcDbEntity. This will show whenever
    // something derived from AcDbEntity is selected.
    /*CString*/ str1 = _T("\nError registering context menu\n");

    if (acedAddObjectContextMenu(AcDbEntity::desc(), gpEntMC, pAppID) == Adesk::kFalse)
        acutPrintf(str1);

    // gpCircMC is added to AcDbCircle. This will show up whenever a circle or something
    // derived from it is selected.      
    if (acedAddObjectContextMenu(AcDbCircle::desc(), gpCircMC, pAppID) == Adesk::kFalse)
        acutPrintf(str1);    

    // gpLineMC is added to AcDbLine. This will show up whenever a line or something
    // derived from it is selected.    
    if (acedAddObjectContextMenu(AcDbLine::desc(), gpLineMC, pAppID) == Adesk::kFalse)
        acutPrintf(str1);

    acDocManager->popResourceHandle();
}

void ContextMenuHelper::UnInit()
{
    HINSTANCE hInst = AfxGetResourceHandle();
    AfxSetResourceHandle(_hdllInstance);

    // Lets be a good arx app and remove the menus before unloading.
    CString str = _T("\nError removing context menu\n");

    // Lets be a good arx app and remove the menus before unloading.
    acedRemoveDefaultContextMenu(gpDefContext);
    if (acedRemoveObjectContextMenu(AcDbEntity::desc(), gpEntMC) == Adesk::kFalse)
        acutPrintf(str);
    if (acedRemoveObjectContextMenu(AcDbCircle::desc(), gpCircMC) == Adesk::kFalse)
        acutPrintf(str);
    if (acedRemoveObjectContextMenu(AcDbLine::desc(), gpLineMC) == Adesk::kFalse)
        acutPrintf(str);

    delete gpCmCommand;
    delete gpDefContext;
    delete gpEntMC;
    delete gpCircMC;
    delete gpLineMC;

    AfxSetResourceHandle(hInst);
}