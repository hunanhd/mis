#include "StdAfx.h"
#include "SQLiteHelper.h"
#include "DataBaseName.h"
#include "ParamManager.h"
#include "Tool/HelperClass.h"
#include "KompexSQLitePrerequisites.h"
#include "KompexSQLiteDatabase.h"
#include "KompexSQLiteStatement.h"
#include "KompexSQLiteException.h"
#include "KompexSQLiteStreamRedirection.h"
#include "KompexSQLiteBlob.h"
#include <set>
#include <algorithm>
void SQLiteHelper::GetStringListNames( MFCStringArray& names )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    typedef std::set<CString> StringSet;
    StringSet all_names;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        SQL.Format( _T( "SELECT * FROM %s;" ), STR_LIST_TABLE );
        pStmt->Sql( SQL );
        while( pStmt->FetchRow() )
        {
            CString field = U2W( pStmt->GetColumnString( 1 ).c_str() );
            // 添加到set中，避免重复
            all_names.insert( field );
        }
        // do not forget to clean-up
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        all_names.clear();
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
    // 从set中提取出来添加到names数组
    for( StringSet::iterator itr = all_names.begin(); itr != all_names.end(); itr++ )
    {
        names.push_back( *itr );
    }
}
void SQLiteHelper::GetStringList( const CString& name, MFCStringArray& strList )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        SQL.Format( _T( "SELECT * FROM %s where field=@field;" ), STR_LIST_TABLE );
        pStmt->Sql( SQL );
        pStmt->BindString( 1, W2U( ( LPCTSTR )name ) );
        while( pStmt->FetchRow() )
        {
            CString strValue = U2W( pStmt->GetColumnString( 2 ).c_str() );
            strList.push_back( strValue );
        }
        // do not forget to clean-up
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        strList.clear();
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
}
void SQLiteHelper::GetIntStrListNames( MFCStringArray& names )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    typedef std::set<CString> StringSet;
    StringSet all_names;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        SQL.Format( _T( "SELECT * FROM %s;" ), INT_LIST_TABLE );
        pStmt->Sql( SQL );
        while( pStmt->FetchRow() )
        {
            CString field = U2W( pStmt->GetColumnString( 1 ).c_str() );
            // 添加到set中，避免重复
            all_names.insert( field );
        }
        // do not forget to clean-up
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        all_names.clear();
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
    // 从set中提取出来添加到names数组
    for( StringSet::iterator itr = all_names.begin(); itr != all_names.end(); itr++ )
    {
        names.push_back( *itr );
    }
}
void SQLiteHelper::GetIntStrList( const CString& name, IntArray& intList, MFCStringArray& strList )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        SQL.Format( _T( "SELECT * FROM %s;" ), INT_LIST_TABLE );
        pStmt->Sql( SQL );
        while( pStmt->FetchRow() )
        {
            CString intValue = U2W( pStmt->GetColumnString( 2 ).c_str() );
            CString strValue = U2W( pStmt->GetColumnString( 3 ).c_str() );
            intList.push_back( _ttoi( intValue ) );
            strList.push_back( strValue );
        }
        // do not forget to clean-up
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        intList.clear();
        strList.clear();
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
}
void SQLiteHelper::ClearAllFields()
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        SQL.Format( _T( "delete from %s;" ), FIELD_INFO_TABLE ); //清空数据
        pStmt->Sql( SQL );
        pStmt->ExecuteAndFree();
        SQL.Format( _T( "update sqlite_sequence SET seq = 0 where name='%s';" ), FIELD_INFO_TABLE ); //自增长ID为0
        pStmt->Sql( SQL );
        pStmt->ExecuteAndFree();
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
}
void SQLiteHelper::GetTypes( MFCStringArray& types )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    typedef std::set<CString> StringSet;
    StringSet all_types;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        SQL.Format( _T( "SELECT * FROM %s;" ), FIELD_INFO_TABLE );
        pStmt->Sql( SQL );
        while( pStmt->FetchRow() )
        {
            CString getype = U2W( pStmt->GetColumnString( "getype" ).c_str() );
            all_types.insert( getype );
        }
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        all_types.clear();
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
    // 从set中提取出来添加到names数组
    for( StringSet::iterator itr = all_types.begin(); itr != all_types.end(); itr++ )
    {
        types.push_back( *itr );
    }
}
void SQLiteHelper::GetFuncs( MFCStringArray& funcs )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    typedef std::set<CString> StringSet;
    StringSet all_funcs;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        SQL.Format( _T( "SELECT * FROM %s;" ), FIELD_INFO_TABLE );
        pStmt->Sql( SQL );
        while( pStmt->FetchRow() )
        {
            CString func = U2W( pStmt->GetColumnString( "func" ).c_str() );
            all_funcs.insert( func );
        }
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        all_funcs.clear();
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
    for( StringSet::iterator itr = all_funcs.begin(); itr != all_funcs.end(); itr++ )
    {
        funcs.push_back( *itr );
    }
}
void SQLiteHelper::AddField( const CString& getype, const CString& hybirdField, const FieldInfo& info )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    // 提取功能类型和字段的名称
    CString func, field;
    FieldHelper::SplitHybirdField( hybirdField, func, field );
    CString fieldtype, defaultvalue, tole, flag, tooltips;
    FieldInfo::Get( info, fieldtype, defaultvalue, tole, flag, tooltips );
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        if( func == _T( "" ) )
        {
            SQL.Format( _T( "INSERT INTO %s (id, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);" ), FIELD_INFO_TABLE );
            pStmt->Sql( SQL );
            pStmt->BindNull( 1 );
            pStmt->BindString( 2, W2U( ( LPCTSTR )getype ) );
            pStmt->BindString( 3, W2U( ( LPCTSTR )field ) );
            pStmt->BindString( 4, W2U( ( LPCTSTR )fieldtype ) );
            pStmt->BindString( 5, W2U( ( LPCTSTR )defaultvalue ) );
            pStmt->BindString( 6, W2U( ( LPCTSTR )tole ) );
            pStmt->BindString( 7, W2U( ( LPCTSTR )flag ) );
            pStmt->BindString( 8, W2U( ( LPCTSTR )tooltips ) );
        }
        else
        {
            SQL.Format( _T( "INSERT INTO %s (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);" ), FIELD_INFO_TABLE );
            pStmt->Sql( SQL );
            pStmt->BindNull( 1 );
            pStmt->BindString( 2, W2U( ( LPCTSTR )func ) );
            pStmt->BindString( 3, W2U( ( LPCTSTR )getype ) );
            pStmt->BindString( 4, W2U( ( LPCTSTR )field ) );
            pStmt->BindString( 5, W2U( ( LPCTSTR )fieldtype ) );
            pStmt->BindString( 6, W2U( ( LPCTSTR )defaultvalue ) );
            pStmt->BindString( 7, W2U( ( LPCTSTR )tole ) );
            pStmt->BindString( 8, W2U( ( LPCTSTR )flag ) );
            pStmt->BindString( 9, W2U( ( LPCTSTR )tooltips ) );
        }
        pStmt->Execute();
        pStmt->Reset();
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
}
void SQLiteHelper::UpdateField( const CString& type, const CString& hybirdField, const FieldInfo& info )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    // 提取功能类型和字段的名称
    CString func, field;
    FieldHelper::SplitHybirdField( hybirdField, func, field );
    CString fieldtype, defaultvalue, tole, flag, tooltips;
    FieldInfo::Get( info, fieldtype, defaultvalue, tole, flag, tooltips );
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        // 构造sq语句
        CString SQL;
        if( func != _T( "" ) )
        {
            SQL.Format( _T( "UPDATE %s SET fieldtype=@fieldtype,defaultvalue=@defaultvalue, flag=@flag, tooltips=@tooltips WHERE getype=@getype and field=@field and func=@func ;" ), FIELD_INFO_TABLE );
        }
        else
        {
            SQL.Format( _T( "UPDATE %s SET fieldtype=@fieldtype,defaultvalue=@defaultvalue, flag=@flag, tooltips=@tooltips WHERE getype=@getype and field=@field ;" ), FIELD_INFO_TABLE );
        }
        pStmt->Sql( SQL );
        pStmt->BindString( 1, W2U( ( LPCTSTR )fieldtype ) );
        pStmt->BindString( 2, W2U( ( LPCTSTR )defaultvalue ) );
        pStmt->BindString( 3, W2U( ( LPCTSTR )flag ) );
        pStmt->BindString( 4, W2U( ( LPCTSTR )tooltips ) );
        pStmt->BindString( 5, W2U( ( LPCTSTR )type ) );
        pStmt->BindString( 6, W2U( ( LPCTSTR )field ) );
        if( func != _T( "" ) )
        {
            pStmt->BindString( 7, W2U( ( LPCTSTR )func ) );
        }
        pStmt->Execute();
        pStmt->Reset();
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
}
void SQLiteHelper::RemoveField( const CString& type, const CString& hybirdField )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        // 提取功能类型和字段的名称
        CString func, field;
        FieldHelper::SplitHybirdField( hybirdField, func, field );
        // 构造sq语句
        CString SQL;
        if( func != _T( "" ) )
        {
            SQL.Format( _T( "DELETE FROM %s WHERE getype=@getype and field=@field and func=@func;" ), FIELD_INFO_TABLE );
        }
        else
        {
            SQL.Format( _T( "DELETE FROM %s WHERE getype=@getype and field=@field;" ), FIELD_INFO_TABLE );
        }
        pStmt->Sql( SQL );
        pStmt->BindString( 1, W2U( ( LPCTSTR )type ) );
        pStmt->BindString( 2, W2U( ( LPCTSTR )field ) );
        if( func != _T( "" ) )
        {
            pStmt->BindString( 3, W2U( ( LPCTSTR )func ) );
        }
        pStmt->Execute();
        pStmt->Reset();
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
}
void SQLiteHelper::GetFieldInfo( const CString& type, const CString& hybirdField, FieldInfo& info )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    // 提取功能类型和字段的名称
    CString func, field;
    FieldHelper::SplitHybirdField( hybirdField, func, field );
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        SQL.Format( _T( "SELECT * FROM %s where getype=@getype and field=@field and func=@func;" ), FIELD_INFO_TABLE );
        pStmt->Sql( SQL );
        pStmt->BindString( 1, W2U( ( LPCTSTR )type ) );
        pStmt->BindString( 2, W2U( ( LPCTSTR )field ) );
        pStmt->BindString( 3, W2U( ( LPCTSTR )func ) );
        while( pStmt->FetchRow() )
        {
            CString fieldType = U2W( pStmt->GetColumnString( 4 ).c_str() );
            CString defaultValue = U2W( pStmt->GetColumnString( 5 ).c_str() );
            CString tole = U2W( pStmt->GetColumnString( 6 ).c_str() );
            CString flag = U2W( pStmt->GetColumnString( 7 ).c_str() );
            CString toolTips = U2W( pStmt->GetColumnString( 8 ).c_str() );
            FieldInfo::Set( info, fieldType, defaultValue, tole, flag, toolTips );
            break;
        }
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        info.initDefault();
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
}
void SQLiteHelper::GetAllFields( const CString& type, MFCStringArray& hybirdFields )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        SQL.Format( _T( "SELECT * FROM %s where getype=@getype;" ), FIELD_INFO_TABLE );
        pStmt->Sql( SQL );
        pStmt->BindString( 1, W2U( ( LPCTSTR )type ) );
        while( pStmt->FetchRow() )
        {
            CString field = U2W( pStmt->GetColumnString( "field" ).c_str() );
            CString func = U2W( pStmt->GetColumnString( "func" ).c_str() );
            hybirdFields.push_back( FieldHelper::MakeHybirdField( func, field ) );
        }
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        hybirdFields.clear();
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
}
void SQLiteHelper::AddMoreFields( const CString& type, const MFCStringArray& hybirdFields )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    // 构造一个默认的字段对象并提取字段信息
    FieldInfo info;
    CString fieldtype, defaultvalue, tole, flag, tooltips;
    FieldInfo::Get( info, fieldtype, defaultvalue, tole, flag, tooltips );
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        SQL.Format( _T( "INSERT INTO %s (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);" ), FIELD_INFO_TABLE );
        pStmt->Sql( SQL );
        for( int i = 0; i < hybirdFields.size(); i++ )
        {
            CString hybirdField = hybirdFields[i];
            // 提取功能类型和字段的名称
            CString func, field;
            FieldHelper::SplitHybirdField( hybirdField, func, field );
            pStmt->BindNull( 1 );
            pStmt->BindString( 2, W2U( ( LPCTSTR )func ) );
            pStmt->BindString( 3, W2U( ( LPCTSTR )type ) );
            pStmt->BindString( 4, W2U( ( LPCTSTR )field ) );
            pStmt->BindString( 5, W2U( ( LPCTSTR )fieldtype ) );
            pStmt->BindString( 6, W2U( ( LPCTSTR )defaultvalue ) );
            pStmt->BindString( 7, W2U( ( LPCTSTR )tole ) );
            pStmt->BindString( 8, W2U( ( LPCTSTR )flag ) );
            pStmt->BindString( 9, W2U( ( LPCTSTR )tooltips ) );
            pStmt->Execute();
            pStmt->Reset();
        }
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
}
void SQLiteHelper::RemoveMoreFields( const CString& type, const MFCStringArray& hybirdFields )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        // 构造sq语句
        CString SQL;
        SQL.Format( _T( "DELETE FROM %s WHERE getype=@getype and field=@field and func=@func;" ), FIELD_INFO_TABLE );
        pStmt->Sql( SQL );
        for( int i = 0; i < hybirdFields.size(); i++ )
        {
            CString hybirdField = hybirdFields[i];
            // 提取功能类型和字段的名称
            CString func, field;
            FieldHelper::SplitHybirdField( hybirdField, func, field );
            pStmt->BindString( 1, W2U( ( LPCTSTR )type ) );
			pStmt->BindString( 2, W2U( ( LPCTSTR )field ) );
            pStmt->BindString( 3, W2U( ( LPCTSTR )func ) );
            pStmt->Execute();
            pStmt->Reset();
        }
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
}
void SQLiteHelper::RemoveType( const CString& type )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        // 构造sq语句
        CString SQL;
        SQL.Format( _T( "DELETE FROM %s WHERE getype=@getype" ), FIELD_INFO_TABLE );
        pStmt->Sql( SQL );
        pStmt->BindString( 1, W2U( ( LPCTSTR )type ) );
        pStmt->Execute();
        pStmt->Reset();
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
}
void SQLiteHelper::RemoveFunc( const CString& func )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        // 构造sq语句
        CString SQL;
        SQL.Format( _T( "DELETE FROM %s WHERE func=@func" ), FIELD_INFO_TABLE );
        pStmt->Sql( SQL );
        pStmt->BindString( 1, W2U( ( LPCTSTR )func ) );
        pStmt->Execute();
        pStmt->Reset();
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
}
void SQLiteHelper::RemoveFuncOfType( const CString& type, const CString& func )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        // 构造sq语句
        CString SQL;
        SQL.Format( _T( "DELETE FROM %s WHERE getype=@getype and func=@func" ), FIELD_INFO_TABLE );
        pStmt->Sql( SQL );
        pStmt->BindString( 1, W2U( ( LPCTSTR )type ) );
        pStmt->BindString( 2, W2U( ( LPCTSTR )func ) );
        pStmt->Execute();
        pStmt->Reset();
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
}
void SQLiteHelper::GetTypeByDXFName( const CString& dxfName, CString& type )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        SQL.Format( _T( "SELECT * FROM %s;" ), TYPE_NAME_TABLE );
        pStmt->Sql( SQL );
        while( pStmt->FetchRow() )
        {
            CString dxfName0 = U2W( pStmt->GetColumnString( "dxfname" ).c_str() );
            if( dxfName == dxfName0 )
            {
                type = U2W( pStmt->GetColumnString( "getype" ).c_str() );
                break;
            }
        }
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
}
void SQLiteHelper::GetDXFNameByType( const CString& type, CString& dxfName )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        SQL.Format( _T( "SELECT * FROM %s;" ), TYPE_NAME_TABLE );
        pStmt->Sql( SQL );
        while( pStmt->FetchRow() )
        {
            CString type0 = U2W( pStmt->GetColumnString( "getype" ).c_str() );
            if( type == type0 )
            {
                dxfName = U2W( pStmt->GetColumnString( "dxfname" ).c_str() );
                break;
            }
        }
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        CString msg;
        msg.Format( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
        AfxMessageBox( msg );
    }
}
