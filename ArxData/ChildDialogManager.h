#pragma once
#include <vector>
typedef std::vector<CDialog*> DialogArray;
typedef std::vector<long> IDArray;
/*
 * 子对话框管理器
 * 用于管理嵌入到窗口中的多个子对话框
 */
class ChildDialogManager
{
public:
	ChildDialogManager();
	~ChildDialogManager();
	void add(CDialog* dlg, long idd);
	void create(CWnd* pParent);
	void activate(int di);
	void move(const CRect& rect);
	void sizeChange(CWnd* pParent, CRect* lpRect);
	void onSize(int cx, int cy);
	CDialog* get(int di) const;
	CDialog* getCurrent() const;
private:
	DialogArray m_dlgs;
	IDArray m_ids;
	int m_di;  // 默认DI_HELP
};