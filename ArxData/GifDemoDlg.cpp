#include "stdafx.h"
#include "GifDemoDlg.h"
IMPLEMENT_DYNAMIC( GifDemoDlg, CDialog )
GifDemoDlg::GifDemoDlg( CWnd* pParent /*=NULL*/ )
    : CDialog( GifDemoDlg::IDD, pParent )
{
    m_hIcon = AfxGetApp()->LoadIcon( IDR_MAINFRAME );
    m_nTextX = 95;
    m_nTextY = 10;
    m_pOldFont = NULL;
}
GifDemoDlg::~GifDemoDlg()
{
}
void GifDemoDlg::DoDataExchange( CDataExchange* pDX )
{
    CDialog::DoDataExchange( pDX );
    DDX_Control( pDX, IDC_GIF, m_GifPic );
}
BEGIN_MESSAGE_MAP( GifDemoDlg, CDialog )
    //ON_WM_CLOSE()
    ON_WM_PAINT()
    ON_WM_TIMER()
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()
BOOL GifDemoDlg::OnInitDialog()
{
    CDialog::OnInitDialog();
    if ( m_GifPic.Load( MAKEINTRESOURCE( IDR_GIFROARING ), _T( "Gif" ) ) )
    {
        //CRect rect;
        //rect.SetRect(0, 0, 80, 80);
        //m_GifPic.SetPaintRect(&rect);
        m_GifPic.Draw();
    }
    SetIcon( m_hIcon, TRUE ); // Set big icon
    SetIcon( m_hIcon, FALSE ); // Set small icon
    // 创建一种新的字体（18点，隶书）
    m_newFont.CreatePointFont( 200, _T( "隶书" ) );
    // 设置定时器，定时时间为200ms
    SetTimer( 1, 200, NULL );
    CBitmap bmp;
    bmp.LoadBitmap( IDB_BG2 );
    m_brBk.CreatePatternBrush( &bmp );
    bmp.DeleteObject();
    return TRUE;  // return TRUE unless you set the focus to a control
    // 异常: OCX 属性页应返回 FALSE
}
void GifDemoDlg::OnClose()
{
    //CDialog::OnClose();
    // 非模态对话框关闭的时候CDialog默认只能隐藏窗口
    // 但窗口资源和窗口对象内存并没有收回
    // 因此调用DestroyWindow()，触发WM_DESTROY消息
    // 从而调用PostNcDestrory()虚函数
    DestroyWindow();
}
void GifDemoDlg::PostNcDestroy()
{
    CDialog::PostNcDestroy();
    delete this;
}
void GifDemoDlg::OnPaint()
{
    if ( IsIconic() )
    {
        CPaintDC dc( this ); // device context for painting
        SendMessage( WM_ICONERASEBKGND, reinterpret_cast<WPARAM>( dc.GetSafeHdc() ), 0 );
        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics( SM_CXICON );
        int cyIcon = GetSystemMetrics( SM_CYICON );
        CRect rect;
        GetClientRect( &rect );
        int x = ( rect.Width() - cxIcon + 1 ) / 2;
        int y = ( rect.Height() - cyIcon + 1 ) / 2;
        // Draw the icon
        dc.DrawIcon( x, y, m_hIcon );
    }
    else
    {
        CPaintDC dc( this ); // device context for painting
        // 设置m_newFont对象的字体为当前字体，并将之前的字体指针保存到m_pOldFont
        m_pOldFont = ( CFont* )dc.SelectObject( &m_newFont );
        // 设置
        dc.SetBkMode( TRANSPARENT ); //设置背景为透明！
        // 设置文本颜色为红色
        dc.SetTextColor( RGB( 0, 255, 0 ) );
        // 在指定位置输出文本
        dc.TextOut( m_nTextX, 10, _T( "保存数据需要一段时间，请耐心等待！" ) );
        //// 设置文本颜色为绿色
        //dc.SetTextColor(RGB(0,255,0));
        //// 在指定位置输出文本
        //dc.TextOut(10,m_nTextY,_T("谢谢关注www.jizhuomi.com"));
        // 恢复以前的字体
        dc.SelectObject( m_pOldFont );
        CDialog::OnPaint();
    }
}
void GifDemoDlg::OnTimer( UINT_PTR nIDEvent )
{
    LOGFONT logFont;
    // 获取m_newFont字体的LOGFONT结构
    m_newFont.GetLogFont( &logFont );
    // 将m_nTextX的值减5
    m_nTextX -= 5;
    // 如果m_nTextX小于10，则文本“欢迎来到鸡啄米”回到起始位置
    if ( m_nTextX < 10 )
        m_nTextX = 260;
    //// 将m_nTextY的值加一个字符高度
    //m_nTextY += abs(logFont.lfHeight);
    //// 如果m_nTextY大于260，则文本“谢谢关注www.jizhuomi.com”回到起始位置
    //if (m_nTextY >260)
    //	m_nTextY = 10;
    // 使窗口客户区无效，之后就会重绘
    Invalidate();
    CDialog::OnTimer( nIDEvent );
}
HBRUSH GifDemoDlg::OnCtlColor( CDC* pDC, CWnd* pWnd, UINT nCtlColor )
{
    HBRUSH hbr = CDialog::OnCtlColor( pDC, pWnd, nCtlColor );
    if ( pWnd == this )
    {
        return m_brBk;
    }
    return hbr;
}
