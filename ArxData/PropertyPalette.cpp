#include "StdAfx.h"
#include "PropertyPalette.h"
#include "PropertyPaletteSet.h"
//#include "XmlHelper.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
#define DATA_PALETTE_FILENAME _T("TestDataPalette.xml")
IMPLEMENT_DYNCREATE(PropertyPalette, CAdUiPalette)
BEGIN_MESSAGE_MAP(PropertyPalette, CAdUiPalette)
	ON_MESSAGE(WM_CTLCOLORSTATIC, OnCtlColorStatic)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
END_MESSAGE_MAP()
PropertyPalette::PropertyPalette()
{
	cdm.add(&m_propHelpDlg, IDD_HELP_CHILD_DLG);
	cdm.add(&m_propDataDlg, IDD_PROPERTY_CHILD_DLG);
	// 连接信号槽
	DataObserverManager::getSingletonPtr()->signalExtDictModify.connect(this, &PropertyPalette::slotExtDictModify);
}
PropertyPalette::~PropertyPalette()
{
}
// Load the data from xml.
BOOL PropertyPalette::Load(IUnknown* pUnk) 
{	
	// Call base class first 
	CAdUiPalette::Load(pUnk);
	
	//CComQIPtr<MSXML::IXMLDOMNode> pNode(pUnk);
	//MSXML::IXMLDOMNodePtr pChild, pChild1;
	//BSTR text;
	//////////////// Item 1 ///////////////////////
	//GetChildNode(pNode, _T("Item1"), &pChild);
	//GetChildNode(pChild, _T("String"), &pChild1);
	//pChild1->get_text(&text);
	////str[0] = CString(text);
	//GetChildNode(pChild, _T("Integer"), &pChild1);
	//pChild1->get_text(&text);
	////integer[0] = _ttoi(CString(text));
	//GetChildNode(pChild, _T("Real"), &pChild1);
	//pChild1->get_text(&text);
	////real[0] = _tstof(CString(text));
	/////////////////// Item 2 ///////////////////
	//GetChildNode(pNode, _T("Item2"), &pChild);
	//GetChildNode(pChild, _T("String"), &pChild1);
	//pChild1->get_text(&text);
	////str[1] = CString(text);
	//GetChildNode(pChild, _T("Integer"), &pChild1);
	//pChild1->get_text(&text);
	////integer[1] = _ttoi(CString(text));
	//GetChildNode(pChild, _T("Real"), &pChild1);
	//pChild1->get_text(&text);
	////real[1] = _tstof(CString(text));
	return TRUE;
}
// Save the data to xml.
BOOL PropertyPalette::Save(IUnknown* pUnk) 
{
	// Call base class first 
	CAdUiPalette::Save(pUnk);
	
	//CComQIPtr<MSXML::IXMLDOMNode> pNode(pUnk);
	//MSXML::IXMLDOMNodePtr pChild, pChild1;
	//CString *pText=new CString;
	////int  n = m_Combo.GetCount();
	/////////////////// Item 1 ///////////////////////
	//AddChildNode(pNode,_T("Item1"),MSXML::NODE_ELEMENT, &pChild);	
	//AddChildNode(pChild, _T("String"), MSXML::NODE_ELEMENT, &pChild1);
	////pChild1->put_text(CString(str[0]).AllocSysString());
	//AddChildNode(pChild, _T("Integer"), MSXML::NODE_ELEMENT, &pChild1);
	////pChild1->put_text(bstr_t(integer[0]));
	//AddChildNode(pChild, _T("Real"), MSXML::NODE_ELEMENT, &pChild1);
	////pChild1->put_text(bstr_t(real[0]));
	//
	//////////////////// Item 2 /////////////////////////
	//AddChildNode(pNode,_T("Item2"),MSXML::NODE_ELEMENT, &pChild);	
	//AddChildNode(pChild, _T("String"), MSXML::NODE_ELEMENT, &pChild1);
	////pChild1->put_text(CString(str[1]).AllocSysString());
	//AddChildNode(pChild, _T("Integer"), MSXML::NODE_ELEMENT, &pChild1);
	////pChild1->put_text(bstr_t(integer[1]));
	//AddChildNode(pChild, _T("Real"), MSXML::NODE_ELEMENT, &pChild1);
	////pChild1->put_text(bstr_t(real[1]));
	return TRUE;
}

// Called by the palette set when the palette is made active
void PropertyPalette::OnSetActive() 
{
	CString s;
	//if(m_Combo.GetCurSel() == 0) {
	//	m_StringEdit.SetWindowText(str[0]);
	//	s.Format(_T("%d"), integer[0]);
	//	m_IntEdit.SetWindowText(s);
	//	s.Format(_T("%.3f"), real[0]);
	//	m_RealEdit.SetWindowText(s);
	//	m_TextLabel.SetWindowText(_T("Item 1"));
	//} else if (m_Combo.GetCurSel() == 1) {
	//	m_StringEdit.SetWindowText(str[1]);
	//	s.Format(_T("%d"), integer[1]);
	//	m_IntEdit.SetWindowText(s);
	//	s.Format(_T("%.3f"), real[1]);
	//	m_RealEdit.SetWindowText(s);
	//	m_TextLabel.SetWindowText(_T("Item 2"));
	//}
	// Create a brush if not already created
	if(m_Brush.m_hObject == NULL)
		m_Brush.CreateSolidBrush(GetPaletteSet()->GetTheme()->GetColor(kPaletteBackground));
	return CAdUiPalette::OnSetActive();
}
LRESULT PropertyPalette::OnCtlColorStatic(WPARAM wParam, LPARAM lParam)
{
	HDC hDC = (HDC)wParam;
	// match the background of all static controls to the palette's background color 
	CDC *pDC = CDC::FromHandle(hDC);
	pDC->SetBkMode(TRANSPARENT);
	return (LRESULT)HBRUSH(m_Brush);
}
int PropertyPalette::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CAdUiPalette::OnCreate(lpCreateStruct) == -1)
		return -1;
	CAcModuleResourceOverride myResources;
	// 创建子对话框
	cdm.create(this);
	//- Move the window over so we can see the control lines
	CRect rect ;
	GetWindowRect ( &rect ) ;
	// 移动对话框
	cdm.move(rect);
	// 默认激活第1个子对话框
	cdm.activate( 0 );
	// Load Palette's persistent data
	LoadPalette();
	// Set the Palette's title
	SetName(_T("属性数据"));
	return 0;
}
void PropertyPalette::OnSize(UINT nType, int cx, int cy)
{
	CAdUiPalette::OnSize(nType, cx, cy);
	CAcModuleResourceOverride myResources;
	cdm.onSize(cx, cy);
}
void PropertyPalette::OnDestroy()
{
	// Save Palette's persistent data
	SavePalette();
	CAdUiPalette::OnDestroy();
}
//Called by AutoCAD to steal focus from the palette
bool PropertyPalette::CanFrameworkTakeFocus()
{
	// not simply calling IsFloating() (a BOOL) avoids warning C4800
	//return ( GetPaletteSet()->IsFloating() == TRUE ? true : false );
	if( GetPaletteSet()->IsFloating() == TRUE )
	{
		return true;
	}
	else
	{
		// 根据鼠标是否位于对话框内, 判断是否保持焦点
		POINT pt;
		GetCursorPos( &pt );
		CRect rect;
		GetWindowRect( rect );
		return ( PtInRect( &rect, pt ) == TRUE ) ? false : true;
	}
}
void PropertyPalette::LoadPalette()
{
	//MSXML::IXMLDOMDocumentPtr pDoc = NULL;
	//HRESULT hr = pDoc.CreateInstance(CLSID_DOMDocumentAcad);
	//const TCHAR *pRoam;
	//TCHAR paletteBuffer[MAX_PATH];
	//BOOL bResult=acdbHostApplicationServices()->getRoamableRootFolder(pRoam);
	//_tcscpy(paletteBuffer, pRoam);
	//_tcscat(paletteBuffer, DATA_PALETTE_FILENAME);
	//CComVariant var(paletteBuffer);
	//VARIANT_BOOL bReturn = pDoc->load(var);
	//if (bReturn == VARIANT_TRUE) // success!
	//{
	//	MSXML::IXMLDOMNodePtr pNode;
	//	GetChildNode(pDoc->documentElement,_T("Palette"),&pNode);
	//	Load(pNode);
	//}
}
void PropertyPalette::SavePalette()
{
	//MSXML::IXMLDOMDocumentPtr pDoc = NULL;
	//HRESULT hr = pDoc.CreateInstance(CLSID_DOMDocumentAcad);
	//const TCHAR *pRoam;
	//TCHAR paletteBuffer[MAX_PATH];
	//BOOL bResult=acdbHostApplicationServices()->getRoamableRootFolder(pRoam);
	//_tcscpy(paletteBuffer, pRoam);
	//_tcscat(paletteBuffer, DATA_PALETTE_FILENAME);
	//CComVariant var(paletteBuffer);
	//// create a root element
	//MSXML::IXMLDOMNodePtr pNode;
	//if (AddChildNode(pDoc, _T("Palette"), MSXML::NODE_ELEMENT, &pNode) == FALSE)
	//	return;
	//Save(pNode);
	//hr = pDoc->save(var);
}
void PropertyPalette::ShowChildDlg(const AcDbObjectId& objId)
{
	if(objId.isNull())
	{
		m_propDataDlg.SetEntity( AcDbObjectId::kNull );
		cdm.activate(0);
	}
	else
	{
		m_propDataDlg.ShowAll( true );
		m_propDataDlg.SetEntity( objId );
		if( m_propDataDlg.Init() )
		{
			cdm.activate( 1 );
		}
		else
		{
			cdm.activate( 0 );
		}
	}
}
void PropertyPalette::slotExtDictModify( const AcDbObjectId& objId )
{
	// 非模态对话框要锁定文档才能访问cad数据库
	ArxDocLockSwitch lock_switch;
	// 切换对话框或更新对话框内容
	CAcModuleResourceOverride resourceOverride;
	if( this->IsWindowVisible() )
	{
		// 更新子对话框
		this->ShowChildDlg( objId );
	}
}
