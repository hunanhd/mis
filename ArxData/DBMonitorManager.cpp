#include "StdAfx.h"
#include "DBMonitorManager.h"
#include "ParamManager.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
// 初始化静态成员变量
template<> DBMonitorManager* Singleton<DBMonitorManager>::ms_Singleton = 0;
DBMonitorManager::DBMonitorManager() : m_use_db_monitor( true )
{
}
DBMonitorManager::~DBMonitorManager()
{
    for( DocDBMonitorMap::iterator itr = m_monitorMap.begin(); itr != m_monitorMap.end(); ++itr )
    {
        delete itr->second;
    }
    m_monitorMap.clear();
}
bool DBMonitorManager::startMonitor( AcApDocument* pDoc )
{
    if( !m_use_db_monitor ) return true;
    if( pDoc == 0 ) return false;
    // 删除原来的监视器
    DocDBMonitorMap::iterator itr = m_monitorMap.find( pDoc );
    if( itr != m_monitorMap.end() )
    {
        delete itr->second;
        m_monitorMap.erase( itr );
    }
    CString dbFile = ParamManager::getSingletonPtr()->getDbFile( pDoc );
    if( dbFile == _T( "" ) ) return false;
    // 设置新的监视器
    m_monitorMap[pDoc] = new DBMonitor( pDoc, dbFile );
    return true;
}
bool DBMonitorManager::closeMonitor( AcApDocument* pDoc )
{
    if( !m_use_db_monitor ) return true;
    if( pDoc == 0 ) return false;
    DocDBMonitorMap::iterator itr = m_monitorMap.find( pDoc );
    if( itr == m_monitorMap.end() ) return false;
    delete itr->second;
    m_monitorMap.erase( itr );
    return true;
}
void DBMonitorManager::Init()
{
    DBMonitorManager* dbm = new DBMonitorManager();
}
void DBMonitorManager::UnInit()
{
    delete DBMonitorManager::getSingletonPtr();
}
