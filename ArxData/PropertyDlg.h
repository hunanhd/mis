#pragma once
#include "resource.h"
#include "afxpropertygridctrl.h"
// 属性数据对话框
class PropertyDlg : public CDialog
{
    DECLARE_DYNAMIC( PropertyDlg )
public:
    //PropertyDataDlg(CWnd* pParent = NULL);   // 标准构造函数
    PropertyDlg( CWnd* pParent = NULL, CString func = _T( "" ) ); //带分组的构造函数
    virtual ~PropertyDlg();
    // 对话框数据
    enum { IDD = IDD_PROPERTY_DLG };
	// 初始化
	bool Init();
    // 关联图元
    void SetEntity( const AcDbObjectId& objId );
    // 添加字段
    void AddField( const CString& field );
    // 是否显示全部数据
    void ShowAll( bool bFlag );
	// 保存数据
	void Save();
protected:
    virtual void DoDataExchange( CDataExchange* pDX );  // DDX/DDV 支持
    DECLARE_MESSAGE_MAP()
public:
    virtual BOOL OnInitDialog();
    afx_msg void OnBnClickedOk();
private:
    CMFCPropertyGridCtrl m_propertyDataList;
    AcDbObjectId m_objId;          // 图元id
    AcStringArray m_fields;        // 要显示的字段
    bool m_showAll;                // 是否显示全部数据(默认true)
    CString m_func;				   // 分组名称，若给定分组名则只显示这组，否则显示全部
};
