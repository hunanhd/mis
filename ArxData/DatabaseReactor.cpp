#include "StdAfx.h"
#include "DatabaseReactor.h"

#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"

DatabaseReactor::DatabaseReactor ( AcDbDatabase* pDb ) : AcDbDatabaseReactor(), mpDatabase( pDb )
{
    if ( pDb )
    {
        //acutPrintf(_T("\nMineGEErase_DbReactor : %ld"), (long)pDb);
        pDb->addReactor ( this ) ;
    }
}

DatabaseReactor::~DatabaseReactor ()
{
    Detach () ;
}

void DatabaseReactor::Attach ( AcDbDatabase* pDb )
{
    Detach () ;
    if ( mpDatabase == NULL )
    {
        if ( ( mpDatabase = pDb ) != NULL )
            pDb->addReactor ( this ) ;
    }
}

void DatabaseReactor::Detach ()
{
    if ( mpDatabase )
    {
        mpDatabase->removeReactor ( this ) ;
        mpDatabase = NULL ;
    }
}

AcDbDatabase* DatabaseReactor::Subject () const
{
    return ( mpDatabase ) ;
}

bool DatabaseReactor::IsAttached () const
{
    return ( mpDatabase != NULL ) ;
}

// 在objectModified中无法启动事务, 必须使用open/close机制
void DatabaseReactor::objectModified( const AcDbDatabase* dwg, const AcDbObject* dbObj )
{
	return;

    AcDbDatabaseReactor::objectModified ( dwg, dbObj );

    // 判断是否扩展词典被修改(扩展词典一般是和AcDbObject绑定在一起)
    // 如果词典的owner是AcDbEntity, 表明它是一个图元的扩展词典,而不是一个普通词典
    // 此时需要更新图元的数据以及绘制效果!!!
    //if( dbObj->isKindOf( AcDbDictionary::desc() ) )
    //{
    //    AcDbDictionary* pDict = AcDbDictionary::cast( dbObj );
    //    AcDbObjectId objId = dbObj->ownerId();
    //}
    // 如果图元的几何参数被修改,同步修改图元的属性数据里对应的字段
    //else if( dbObj->isKindOf( MineGE::desc() ) )
    //{
        //MineGE* pGE = MineGE::cast( dbObj );
    //}
}