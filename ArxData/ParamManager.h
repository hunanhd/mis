#pragma once
#include "Tool/Singleton.h"
#include <map>
class ParamManager : public Singleton<ParamManager>
{
public:
    static void Init();
    static void UnInit();
public:
    // 构造函数
    ParamManager();
    // 析构函数
    ~ParamManager();
    // 初始化数据库文件,并记录
    bool initDbFile( AcApDocument* pDoc );
    // 获取数据库文件的路径
    CString getDbFile( AcApDocument* pDoc ) const;
    // 移除数据库文件记录
    bool removeDbFile( AcApDocument* pDoc );
private:
    typedef std::map<AcApDocument*, CString> DBMap;
    DBMap m_dbMap;
};
#define DB_FILE ParamManager::getSingletonPtr()->getDbFile( curDoc() )