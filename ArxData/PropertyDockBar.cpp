#include "StdAfx.h"
#include "resource.h"
#include "PropertyDockBar.h"

IMPLEMENT_DYNAMIC ( PropertyDockBar, DockBar )

BEGIN_MESSAGE_MAP( PropertyDockBar, DockBar )
END_MESSAGE_MAP()

//-----------------------------------------------------------------------------
PropertyDockBar::PropertyDockBar () : DockBar()
{
	cdm.add(&m_propHelpDlg, IDD_HELP_CHILD_DLG);
	cdm.add(&m_propDataDlg, IDD_PROPERTY_CHILD_DLG);
}

//-----------------------------------------------------------------------------
PropertyDockBar::~PropertyDockBar ()
{

}

static CLSID DockBar_GUID = {0x9dd9391a, 0xc2da, 0x4d2e, { 0x8e, 0x27, 0x95, 0xad, 0xe, 0x8b, 0xba, 0xc8 }} ;

CLSID* PropertyDockBar::GetCLSID() const
{
	return &DockBar_GUID;
}

void PropertyDockBar::ShowChildDlg(const AcDbObjectId& objId)
{
	if(objId.isNull())
	{
		m_propDataDlg.SetEntity( AcDbObjectId::kNull );

		cdm.activate(0);
	}
	else
	{
		m_propDataDlg.ShowAll( true );
		m_propDataDlg.SetEntity( objId );
		m_propDataDlg.Init();

		cdm.activate(1);
	}
}
