#pragma once
// 保存字段消息
#define WM_USER_SAVE_FIELD WM_USER + 200
// 线程参数
struct ThreadParam
{
    HWND hFieldDlg;
    HWND hGifDlg;
};
// 全局线程参数变量
extern ThreadParam global_param;