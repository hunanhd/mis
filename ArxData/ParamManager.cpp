#include "StdAfx.h"
#include "ParamManager.h"
#include "config.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
// 初始化静态成员变量
template<> ParamManager* Singleton<ParamManager>::ms_Singleton = 0;
// 从文件对话框中选择db文件
static bool GetDbFileFromDialog( CString& dbFile )
{
    CFileDialog openDialog(
        TRUE,
        _T( "db" ),
        NULL, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST,
        _T( "db文件(*.db)|*.db||" ) );
    // 设置对话框的默认路径
    CString exeDir = PathHelper::GetExeDir();
    // 不能直接将CString赋给lpstrInitialDir
    // lpstrInitialDir要求一个以\0结尾的字符串!!!!要使用GetBuffer()方法
    openDialog.m_ofn.lpstrInitialDir = exeDir.GetBuffer();
    // 对话框标题
    openDialog.m_ofn.lpstrTitle = _T( "请选择数据库文件" );
    if( IDOK == openDialog.DoModal() )
    {
        dbFile = openDialog.GetPathName();
        exeDir.ReleaseBuffer();
        return true;
    }
    else
    {
        return false;
    }
}
// 查找与dwg文件名相同的数据库文件
static bool GetDbFile( AcApDocument* pDoc, CString& outDbFile, bool useFileDialog = true )
{
    // 当前dwg文件名(绝对路径)
    // 如果dwg文件已保存,则显示完整路径,比如C:\Users\dlj\Desktop\Drawing1.dwg
    // 如果是一个新建的dwg文件,则显示文件名,比如Drawing1.dwg
    CString dwgFile = pDoc->fileName();
    //acutPrintf(_T("\n--->%s"), dwgFile);
    // 拆分路径、文件名、扩展名
    // 如果得到的目录路径dir与文件路径相同,则表示使用的是相对路径
    // 同时也表明dwg文件是新建的, 尚未保存
    CString dir = PathHelper::GetDirectory( dwgFile );
    CString name = PathHelper::GetFileNameWithoutExtension( dwgFile );
    CString ext = PathHelper::GetExtension( dwgFile );
    // 如果是新建dwg, 在系统的临时文件夹下保存db文件
    bool isNewDwg = false;
    if( ArxDwgHelper::IsNewDwg( pDoc ) )
    {
        isNewDwg = true;
        dir = PathHelper::GetTempPath();
        name = PathHelper::GetTimeStampFileName( _T( "arx" ) ); // 使用当前时间生成临时文件名
    }
    // 数据库文件的名称与dwg文件名相同, 扩展名为*.db
    CString dbName = name + _T( ".db" );
    CString dbFile = PathHelper::BuildPath( dir, dbName );
    //acutPrintf( _T( "\n%s" ), dbFile );
    // 新建dwg
    if( isNewDwg )
    {
        // 如果有同名的db,直接使用
        if( PathHelper::IsFileExist( dbFile ) )
        {
            outDbFile = dbFile;
            return true;
        }
        // 如果没有, 则从arx程序的目录下将db模板复制过来
        else
        {
            // 数据库模板(template)
            CString tplDbFile = ARX_FILE_PATH( DEFAULT_DB_NAME );
            // 调用win32 api复制文件
            bool ret = ( TRUE == CopyFile( tplDbFile, dbFile, FALSE ) );
            if( ret )
            {
                outDbFile = dbFile;
            }
            return ret;
        }
    }
    // dwg已存在
    else
    {
        // 如果有同名的db,直接使用
        if( PathHelper::IsFileExist( dbFile ) )
        {
            outDbFile = dbFile;
            return true;
        }
        // 如果没有, db文件被重命名、删除、或移动到其它位置
        // 弹出文件选择对话框, 要求用户指定db文件
        else
        {
            if( !useFileDialog ) return false;
            AfxMessageBox( _T( "原始数据库文件丢失或损坏,请选择新的数据库文件!" ) );
            return GetDbFileFromDialog( outDbFile );
        }
    }
}
static void QuitCAD()
{
    HWND cadMainWnd = acedGetAcadFrame()->GetSafeHwnd();
    CString cmd = _T( "quit" );
    CADHelper::SendCommandToAutoCAD( cadMainWnd, cmd, true );
}
ParamManager::ParamManager()
{
}
ParamManager::~ParamManager()
{
    for( DBMap::iterator itr = m_dbMap.begin(); itr != m_dbMap.end(); ++itr )
    {
        CString dbFile = itr->second;
        // 如果db文件在临时文件夹里,删除掉!!!
        if( PathHelper::IsFileInTempPath( dbFile ) )
        {
            DeleteFile( dbFile );
        }
    }
    m_dbMap.clear();
}
bool ParamManager::initDbFile( AcApDocument* pDoc )
{
    if( pDoc == 0 ) return false;
    CString dbFile;
    bool ret = GetDbFile( pDoc, dbFile, false );
    if( ret )
    {
        m_dbMap[pDoc] = dbFile;
    }
    return ret;
}
CString ParamManager::getDbFile( AcApDocument* pDoc ) const
{
    if( pDoc == 0 ) return _T( "" );
    DBMap::const_iterator itr = m_dbMap.find( pDoc );
    if( itr == m_dbMap.end() )
    {
        return _T( "" );
    }
    else
    {
        return itr->second;
    }
}
bool ParamManager::removeDbFile( AcApDocument* pDoc )
{
    if( pDoc == 0 ) return false;
    DBMap::iterator itr = m_dbMap.find( pDoc );
    bool ret = ( itr != m_dbMap.end() );
    if( ret )
    {
        CString dbFile = itr->second;
        // 如果db文件在临时文件夹里,删除掉!!!
        if( PathHelper::IsFileInTempPath( dbFile ) )
        {
            DeleteFile( dbFile );
        }
        m_dbMap.erase( itr );
    }
    return ret;
}

void ParamManager::Init()
{
    // 构造ParamManager单件对象
    ParamManager* pm = new ParamManager();
}
void ParamManager::UnInit()
{
    // 销毁ParamManager单例对象
    delete ParamManager::getSingletonPtr();
}
