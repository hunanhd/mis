#include "StdAfx.h"
#include "TrayItemHelper.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"

#include "trayitem.h"

// We can't rely on the index of the tray items to be constant, so we
// need to keep an independent map of the first tray item for each document.
CMap<AcApDocument*,AcApDocument*,CTrayItem*,CTrayItem*> documentTrayMap;

// Create the tray items for the passed document.
void TrayItemHelper::CreateDocumentWorkspace(AcApDocument* pDoc)
{
	// Create the tray items:
	CTrayItem *pTray=new CTrayItem;
	// Create World Tray Item:
	HICON hIcon = (HICON)::LoadImage(_hdllInstance, MAKEINTRESOURCE(IDI_WORLD), IMAGE_ICON, 16, 16, 0);
	int result=pTray->SetIcon(hIcon);
	result=pTray->SetToolTipText(_T("World Icon ToolTip"));
	pTray->Enable(TRUE);
	pTray->SetVisible(TRUE);
	result=pDoc->drawingStatusBar()->Add(pTray);

	// Set this document to map to the first tray item.
	documentTrayMap.SetAt(pDoc,pTray);

	// Create Pie Tray Item:
	pTray=new CTrayItem;
	hIcon = (HICON)::LoadImage(_hdllInstance, MAKEINTRESOURCE(IDI_PIE), IMAGE_ICON, 16, 16, 0);
	result=pTray->SetIcon(hIcon);
	result=pTray->SetToolTipText(_T("Pie Icon ToolTip"));
	pTray->Enable(TRUE);
	pTray->SetVisible(TRUE);
	result=pDoc->drawingStatusBar()->Add(pTray);

	// Create Tree Tray Item:
	pTray=new CTrayItem;
	hIcon = (HICON)::LoadImage(_hdllInstance, MAKEINTRESOURCE(IDI_TREE), IMAGE_ICON, 16, 16, 0);
	result=pTray->SetIcon(hIcon);
	result=pTray->SetToolTipText(_T("Tree Icon ToolTip"));
	pTray->Enable(TRUE);
	pTray->SetVisible(TRUE);
	result=pDoc->drawingStatusBar()->Add(pTray);

	// Create a Pane Item with Text...
	CPaneItem *pPane=new CPaneItem;
	result=pPane->SetToolTipText(_T("Pane Item Tooltip"));
	pPane->Enable(TRUE);
	pPane->SetVisible(TRUE);
	result=pPane->SetStyle(ACSB_POPOUT);
	result=pPane->SetText(_T("Pane Item"));
	result=pPane->SetMinWidth(pDoc->drawingStatusBar()->GetTextWidth(_T("Pane Item")));
	result=pDoc->drawingStatusBar()->Add(pPane);

	// Create a Pane Item with an Icon...
	pPane=new CPaneItem;
	result=pPane->SetToolTipText(_T("Pane Icon Item Tooltip"));
	pPane->Enable(TRUE);
	result=pPane->SetStyle(ACSB_POPOUT);
	pPane->SetVisible(TRUE);
	hIcon = (HICON)::LoadImage(_hdllInstance, MAKEINTRESOURCE(IDI_WORLD), IMAGE_ICON, 16, 16, 0);
	result=pPane->SetIcon(hIcon);
	result=pDoc->drawingStatusBar()->Add(pPane);

	// Create a status bar menu item...this replaces the default menu item (the small black triangle on the lower right of the doc window).
	CMenuPaneItem *pMenuPane=new CMenuPaneItem;
	result=pDoc->drawingStatusBar()->SetStatusBarMenuItem(pMenuPane);
}

// Cleanup the tray items for the passed document.
void TrayItemHelper::DestroyDocumentWorkspace(AcApDocument *pDoc)
{
	if(!documentTrayMap.RemoveKey(pDoc))
		AfxMessageBox(_T("Couldn't remove tray item from the document map"));

	AcApStatusBar *pStatusBar=pDoc->drawingStatusBar();
	AcTrayItem* pTrayItem = NULL;
	if(pStatusBar)
	{
		// Remove all tray items that we added in this sample.
		int count = pStatusBar->GetTrayItemCount();	
		int iPos = 0;
		for(int i = 0; i<count; i++)	
		{
			pTrayItem = pStatusBar->GetTrayItem(iPos);
			if(pTrayItem)
			{
				CString ttt;
				pTrayItem->GetToolTipText(ttt);
				if(	ttt.Compare(_T("World Icon ToolTip")) == 0 ||
					ttt.Compare(_T("Pie Icon ToolTip")) == 0 ||
					ttt.Compare(_T("Tree Icon ToolTip")) == 0 
					)
				{
					pStatusBar->Remove(pTrayItem);
					delete pTrayItem;
				}
				else
					iPos++;
			}
		}

		// Remove all pane items from the status bar.
		AcPane *pPane=NULL;
		count = pStatusBar->GetPaneCount(); 
		iPos = 0;
		for(int i=0;i<count; i++)
		{
			pPane=pStatusBar->GetPane(iPos);
			if(pPane)
			{
				CString ttt;
				pPane->GetToolTipText(ttt);
				if(	ttt.Compare(_T("Pane Item Tooltip")) == 0 ||
					ttt.Compare(_T("Pane Icon Item Tooltip")) == 0 
					)
				{
					pStatusBar->Remove(pPane);
					delete pPane;
				}
				else
					iPos++;
			}
		}
		// Remove the context menu pane.
		// Note this will make the context menu empty and there is no way to recover the previous 
		// default menu. Therefore, a better way is to make this app as unloadable (lock it). 
		// If the app is locked, there is no need to remove it with the line below as
		// AutoCAD will clean up when it quits.
		pStatusBar->SetStatusBarMenuItem(NULL);
	}
}

// Called when a bubble notification is closed.  
// The return code indicates the close condition.  
// You can customize the pData argument to suit your needs as well.
static void BubbleWindowCallback(void *pData,int nReturnCode)
{
	CString strMsg;
	switch(nReturnCode)
	{
	case AcTrayItemBubbleWindowControl::BUBBLE_WINDOW_ERROR_NO_CREATE:
		AfxMessageBox(_T("Couldn't Create the Bubble Window!"));
		break;
	case AcTrayItemBubbleWindowControl::BUBBLE_WINDOW_ERROR_NO_ICONS:
		AfxMessageBox(_T("No Tray Area for the Bubble Window!"));
		break;
	case AcTrayItemBubbleWindowControl::BUBBLE_WINDOW_ERROR_NO_NOTIFICATIONS:
		AfxMessageBox(_T("No Bubble Window Notifications!"));
		break;
	case AcTrayItemBubbleWindowControl::BUBBLE_WINDOW_CLOSE:
		acutPrintf(_T("\nNotification Closed...\n"));
		break;
	case AcTrayItemBubbleWindowControl::BUBBLE_WINDOW_TIME_OUT:
		acutPrintf(_T("\nNotification Timed Out...\n"));
		break;
	case AcTrayItemBubbleWindowControl::BUBBLE_WINDOW_HYPERLINK_CLICK:
		acutPrintf(_T("\nHyperlink Selected...\n"));
		break;
	}
	acedPostCommandPrompt();
}

// This is the command 'NOTIFY'
void TrayItemHelper::BubbleTest()
{
	// Create the bubble notification message, and callbacks.
	int result;
	AcApDocument *pDoc=acDocManager->curDocument();
	CString strMsg((CString)_T("Document ")+pDoc->docTitle()+_T(" Notification"));
	AcTrayItemBubbleWindowControl bwControl(_T("Attention!"), strMsg, _T("HyperText Here"), _T("www.autodesk.com"));
	bwControl.SetIconType(AcTrayItemBubbleWindowControl::BUBBLE_WINDOW_ICON_INFORMATION);
	bwControl.SetCallback(BubbleWindowCallback, pDoc);

	// Display the notification at the first of our icons - kept as a pointer in the map.
	CTrayItem *pFirstTrayItem=NULL;
	documentTrayMap.Lookup(acDocManager->curDocument(),pFirstTrayItem);
	if(pFirstTrayItem)
		result=pFirstTrayItem->ShowBubbleWindow(&bwControl);
}