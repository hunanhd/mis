#pragma once
#include "resource.h"
#include "afxpropertygridctrl.h"
#include "DockBarChildDlg.h"
// 属性数据对话框
class PropertyDataChildDlg : public DockBarChildDlg
{
    DECLARE_DYNAMIC( PropertyDataChildDlg )
public:
    //PropertyDataChildDlg(CWnd* pParent = NULL);   // 标准构造函数
    PropertyDataChildDlg( CWnd* pParent = NULL, CString func = _T( "" ) ); //带分组的构造函数
    virtual ~PropertyDataChildDlg();
    // 对话框数据
    enum { IDD = IDD_PROPERTY_DLG };
	bool Init();
    // 关联图元
    void SetEntity( const AcDbObjectId& objId );
    // 添加字段
    void AddField( const CString& field );
    // 是否显示全部数据
    void ShowAll( bool bFlag );
	// 保存数据
	virtual void Save();
protected:
    virtual void DoDataExchange( CDataExchange* pDX );  // DDX/DDV 支持
    DECLARE_MESSAGE_MAP()
public:
	afx_msg LRESULT OnPropertyChanged(WPARAM,LPARAM);

public:
    virtual BOOL OnInitDialog();
private:
    CMFCPropertyGridCtrl m_propertyDataList;
    AcDbObjectId m_objId;          // 图元id
    AcStringArray m_fields;        // 要显示的字段
    bool m_showAll;                // 是否显示全部数据(默认true)
    CString m_func;				   // 分组名称，若给定分组名则只显示这组，否则显示全部
};
