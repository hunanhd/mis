#pragma once
/*
 * 可视化界面辅助类
 * 放在一个类中便于管理
 * 也可使得代码看起来简洁一些
 */
class TrayItemHelper
{
public:
    static void CreateDocumentWorkspace(AcApDocument* pDoc);
    static void DestroyDocumentWorkspace(AcApDocument *pDoc);

    static void BubbleTest();
};
