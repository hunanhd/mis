#pragma once
#include "afxctl.h"
#include "aduiPalette.h"
#include "resource.h"
#include "ChildDialogManager.h"
#include "PropertyDataChildDlg.h"
#include "PropertyHelpChildDlg.h"
#include "ArxHelper/DataObserverManager.h"
class PropertyPalette : public CAdUiPalette, public has_slots<>
{ 
	DECLARE_DYNCREATE(PropertyPalette)
public:
	PropertyPalette();
	virtual ~PropertyPalette();
	
	void ShowChildDlg(const AcDbObjectId& objId);
public:
    // Load the data from xml.
    virtual BOOL Load(IUnknown* pUnk);
    // Save the data to xml.
    virtual BOOL Save(IUnknown* pUnk);
    // Called by the palette set when the palette is made active
    virtual void OnSetActive();
	//Called by AutoCAD to steal focus from the palette
	virtual bool CanFrameworkTakeFocus();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate (LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize (UINT nType, int cx, int cy);
	afx_msg void OnDestroy(); 
	afx_msg LRESULT OnCtlColorStatic(WPARAM wParam, LPARAM lParam);
private:
	// Load and save palette information
	void LoadPalette();
	void SavePalette();
private:
	// 扩展数据被修改后更新界面(响应DataObserverManager的signalExtDictModify信号)
	void slotExtDictModify( const AcDbObjectId& objId );
private:
	// Brush to paint static control background color
	CBrush m_Brush;
	// 子对话框
	PropertyDataChildDlg m_propDataDlg ;
	PropertyHelpChildDlg m_propHelpDlg;
	// 子对话框管理器
	ChildDialogManager cdm;
};
