#include "StdAfx.h"
#include "ReactorHelper.h"
#include "EditorReactor.h"
#include "DocManagerReactor.h"
EditorReactor* pEditorReactor = 0;
DocManagerReactor* pDocManagerReactor = 0;
void ReactorHelper::CreateEditorReactor()
{
    if( pEditorReactor == 0 )
    {
        pEditorReactor = new EditorReactor( true );
    }
}
void ReactorHelper::RemoveEditorReactor()
{
    delete pEditorReactor;
    pEditorReactor = 0;
}
void ReactorHelper::CreateDocManagerReactor()
{
    //if( pDocManagerReactor == 0 )
    //{
    //    pDocManagerReactor = new DocManagerReactor();
    //    acDocManager->addReactor( pDocManagerReactor );
    //}
}
void ReactorHelper::RemoveDocManagerReactor()
{
    //if( pDocManagerReactor != 0 )
    //{
    //    acDocManager->removeReactor( pDocManagerReactor );
    //    delete pDocManagerReactor;
    //    pDocManagerReactor = 0;
    //}
}