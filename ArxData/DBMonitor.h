#pragma once
#include "FileSystemWatcher.h"
// 数据库文件修改监视器
class DBMonitor
{
public:
    DBMonitor( AcApDocument* pDoc, const CString& dbFile );
    ~DBMonitor();
    AcApDocument* getDoc() const;
    CString getDbFile() const;
private:
    FileSystemWatcher fsw;
    AcApDocument* m_pDoc;
    CString m_dbFile;
};
