#pragma once 

#include "Tween/Easing.h"
#include "Tween/TweenManager.h"
#include "Tween/Tween.h"
#include "Tween/Timeline.h"
#include "Tween/TweenableValue.h"
#include "Tween/Path.h"

#include <vector>

class Animation
{
public:
	Animation(const AcDbObjectId& objId, const CString& field, bool isProperty = false);
	virtual ~Animation();
	AcDbObjectId getID() const;
	void start();
	void stop();

public:
	virtual void update() = 0;

protected:
	AcDbObjectId m_objId;
	CString m_field;
	bool m_isProperty;
};

class DoubleAnimation : public Animation
{
public:
	DoubleAnimation(const AcDbObjectId& objId, const CString& field, bool isProperty = false);
	void init(double begin, double end, double v, double k);

public:
	virtual void update();

private:
	Tween::Tween tweenX;
	Tween::TweenableFloat X;
};

//class PointAnimation : public Animation
//{
//public:
//	PointAnimation(const AcDbObjectId& objId, const CString& field, bool isProperty = false);
//
//public:
//	virtual void update();
//	virtual void init(const AcGePoint3d& begin, const AcGePoint3d& end, double v, double k);
//
//private:
//	void set_x(double v);
//	void set_y(double v);
//	void set_z(double v);
//	AcGePoint3d pt;
//	claw::tween::tweener_group t;
//};

namespace Tween
{
	class TweenManager;
}

class AnimationManager
{
public:
	static void Init();
	static void UnInit();
	static AnimationManager& getSingleton( void );
	static AnimationManager* getSingletonPtr( void );

public:
	AnimationManager( void );
	virtual ~AnimationManager();

	Tween::TweenManager* getManager() const;

	void add(Animation* ani);
	void update();

private:
	static AnimationManager* ms_Singleton;

	Tween::TweenManager* manager;
	typedef std::vector<Animation*> AnimationArray;
	AnimationArray m_animations;
	double time;
	float fDTFactor;
};