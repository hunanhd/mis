#include "StdAfx.h"
#include "EditorReactor.h"
#include "ParamManager.h"
#include "DBMonitorManager.h"
#include "DataHelper.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
#include "PropertyPalette.h"
#include "PropertyPaletteSet.h"
extern PropertyPaletteSet *pPropertyPaletteSet;
#include "UIHelper.h"
EditorReactor::EditorReactor ( const bool autoInitAndRelease ) : AcEditorReactor2(), mbAutoInitAndRelease( autoInitAndRelease )
{
    if( autoInitAndRelease )
    {
        if( acedEditor )
        {
            //acutPrintf(_T("\nEdgeGE_EditorReactor2 : %ld"), (long)acedEditor);
            acedEditor->addReactor ( this ) ;
        }
        else
            mbAutoInitAndRelease = false ;
    }
}
EditorReactor::~EditorReactor ()
{
    Detach () ;
}
void EditorReactor::Attach ()
{
    Detach () ;
    if ( !mbAutoInitAndRelease )
    {
        if ( acedEditor )
        {
            acedEditor->addReactor ( this ) ;
            mbAutoInitAndRelease = true ;
        }
    }
}
void EditorReactor::Detach ()
{
    if ( mbAutoInitAndRelease )
    {
        if ( acedEditor )
        {
            acedEditor->removeReactor ( this ) ;
            mbAutoInitAndRelease = false ;
        }
    }
}
AcEditor* EditorReactor::Subject () const
{
    return ( acedEditor ) ;
}
bool EditorReactor::IsAttached () const
{
    return ( mbAutoInitAndRelease ) ;
}
//void EditorReactor::commandWillStart( const ACHAR* cmdStr )
//{
//	CString msg;
//	msg.Format(_T("EditorReactor::commandWillStart() --> %s"), cmdStr);
//	AfxMessageBox(msg);
//
//	AcEditorReactor::commandWillStart ( cmdStr ) ;
//}
//void EditorReactor::commandEnded( const ACHAR* cmdStr )
//{
//	// If AutoCAD is shutting down, then do nothing.
//	if ( !acdbHostApplicationServices()->workingDatabase() )
//		return;
//
//	CString msg;
//	msg.Format(_T("EditorReactor::commandEnded() --> %s"), cmdStr);
//	AfxMessageBox(msg);
//
//	AcEditorReactor::commandEnded ( cmdStr ) ;
//}
void EditorReactor::saveComplete( AcDbDatabase* pDwg, const ACHAR* pActualName )
{
    AcEditorReactor::saveComplete( pDwg, pActualName );
    //acutPrintf(_T("\n保存的文件名称:%s"), pActualName);
    AcApDocument* pDoc = acDocManager->document( pDwg );
    if( pDoc == 0 ) return;
    //acutPrintf(_T("\n----保存的文件名称: %s"), pDoc->fileName());
	// 如果在临时文件夹, 则不进行复制操作
	if(PathHelper::IsFileInTempPath(pActualName)) return;

    CString old_dbFile = ParamManager::getSingletonPtr()->getDbFile( pDoc );
    if( old_dbFile == _T( "" ) ) return;
    CString old_dir = PathHelper::GetDirectory( old_dbFile );
    CString old_name = PathHelper::GetFileNameWithoutExtension( old_dbFile );
    CString dir = PathHelper::GetDirectory( pActualName );
    CString name = PathHelper::GetFileNameWithoutExtension( pActualName );
    bool ret = true;
    // 路径或文件名发生改变
    if( old_dir.CompareNoCase( dir ) != 0 || old_name.CompareNoCase( name ) )
    {
        CString dbName = name + _T( ".db" );
        CString dbFile = PathHelper::BuildPath( dir, dbName );
        // 移动db文件(若是要在不同的volume下移动文件,需要此项 COPY_ALLOWED)
        //if( FALSE == MoveFileEx( old_dbFile, dbFile, MOVEFILE_REPLACE_EXISTING | MOVEFILE_COPY_ALLOWED ) )
        if( FALSE == CopyFile( old_dbFile, dbFile, FALSE ) )
        {
            ret = false;
            // 关闭数据库文件监视器
            DBMonitorManager::getSingletonPtr()->closeMonitor( pDoc );
            CString msg;
            msg.Format( _T( "复制文件 %s \n-->\n%s\n失败!" ), old_dbFile, dbFile );
            AfxMessageBox( msg );
        }
        else
        {
            // 删除临时目录下的db文件
			if( PathHelper::IsFileInTempPath( old_dbFile ) )
			{
				DeleteFile( old_dbFile );
			}
            // 更新数据库文件记录
            ParamManager::getSingletonPtr()->initDbFile( pDoc );
        }
    }
    if( ret )
    {
        // 关闭数据库文件监视器
        DBMonitorManager::getSingletonPtr()->closeMonitor( pDoc );
        // 写入到数据库
        DataHelper::ExportAllData();
        // 重启数据库文件监视器
        DBMonitorManager::getSingletonPtr()->startMonitor( pDoc );
        AfxMessageBox( _T( "同步更新数据库!" ) );
    }
}
//void EditorReactor::docCloseWillStart( AcDbDatabase* pDwg )
//{
//
//	AcApDocument* pDoc = acDocManager->document( pDwg );
//	if( pDoc == 0 ) return;
//}
static void NotifyPropertyPalette()
{
	// 当前选择集中的图元id
	AcDbObjectIdArray objIds;
	ArxUtilHelper::GetPickSetEntity( objIds );
	if( objIds.length() == 1 )
	{
		UIHelper::UpdatePropertyPalette(objIds[0]);
	}
	else
	{
		UIHelper::UpdatePropertyPalette(AcDbObjectId::kNull);
	}
}
void EditorReactor::pickfirstModified( void )
{
	NotifyPropertyPalette();
	//AcEditorReactor::pickfirstModified();
}

//参见: http://bbs.xdcad.net/thread-705681-1-1.html
//void EditorReactor::beginDoubleClick(const AcGePoint3d& clickPoint)
//{
//    //ArxSysVarSwitch()
//AfxMessageBox(_T("这是一个测试程序!"));
//}
