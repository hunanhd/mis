#include "StdAfx.h"
#include "XmlHelper.h"

BOOL GetChildNode(MSXML::IXMLDOMNode* pNode, LPCTSTR pszNodeName, MSXML::IXMLDOMNode** ppChild)
{
	try {
		ASSERT(ppChild != NULL);
		if (ppChild == NULL)
			return FALSE;

		*ppChild = NULL;

		ASSERT(pNode != NULL && pszNodeName != NULL);
		if (pNode == NULL || pszNodeName == NULL)
			return FALSE;

		USES_CONVERSION;

		CComBSTR bstrName;
		pNode->get_nodeName(&bstrName);

		CString sName = OLE2T(bstrName);
		if (sName.CompareNoCase(pszNodeName) == 0) {
			*ppChild = pNode;
			// AddRef before returning
			if (*ppChild != NULL)
				(*ppChild)->AddRef();
			return TRUE;
		}
		// Get the named child element from the list of children
		//
		MSXML::IXMLDOMNodeListPtr pChildren; 
		pNode->get_childNodes(&pChildren);
		ASSERT(pChildren != NULL);
		if (pChildren == NULL) {
			ASSERT(FALSE);
			return FALSE;
		}

		int nCount = pChildren->Getlength();
		if (nCount < 1) {
			// No children
			return FALSE;
		}

		for (long i=0; i<nCount; i++) {
			MSXML::IXMLDOMNodePtr pNode;
			pChildren->get_item(i, &pNode);
			ASSERT(pNode != NULL);

			CComBSTR bstrName;
			pNode->get_nodeName(&bstrName);

			CString sName = OLE2T(bstrName);
			if (sName.CompareNoCase(pszNodeName) == 0) {
				*ppChild = pNode.GetInterfacePtr();
				// AddRef before returning
				if (*ppChild != NULL)
					(*ppChild)->AddRef();
				return TRUE;
			}
		}
	} catch (_com_error  &e) {
		ASSERT(FALSE);
		SetLastError(e.Error());
		return FALSE;
	} catch (...) {
		TRACE(_T("Exception\n"));        
		return FALSE;
	}

	// Named child not found
	return FALSE;
}

BOOL AddChildNode(MSXML::IXMLDOMNode* pNode, LPCTSTR pszChildNodeName, short nNodeType, MSXML::IXMLDOMNode** ppChild)
{
	USES_CONVERSION;

	try {
		ASSERT(ppChild != NULL);
		if (ppChild == NULL)
			return FALSE;

		*ppChild = NULL;

		ASSERT(pNode != NULL && pszChildNodeName != NULL );
		if (pNode == NULL || pszChildNodeName == NULL)
			return FALSE;

		CString strName(pszChildNodeName);

		MSXML::IXMLDOMDocumentPtr pDoc;
		MSXML::DOMNodeType type;
		pNode->get_nodeType(&type);
		if (type == MSXML::NODE_DOCUMENT)
			pDoc = pNode;
		else {
			pNode->get_ownerDocument(&pDoc);
			ASSERT( pDoc != NULL);
			if (pDoc == NULL)
				return FALSE;
		}

		_variant_t varType(nNodeType);
		_bstr_t varName(strName);//pszChildNodeName);

		MSXML::IXMLDOMNodePtr pNewNode = pDoc->createNode(varType, varName, _T(""));;
		*ppChild = pNewNode.GetInterfacePtr();
		// AddRef before returning
		if (*ppChild != NULL)
			(*ppChild)->AddRef();

		pNode->appendChild(*ppChild);
	} catch (_com_error  &e) {
		ASSERT(FALSE);
		SetLastError(e.Error());
		return FALSE;
	} catch (...) {
		TRACE(_T("Exception\n"));        
		return FALSE;
	}

	return TRUE;
}