#include "StdAfx.h"
#include "AnimationManager.h"

#include <boost/bind.hpp>

#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"

Animation::Animation(const AcDbObjectId& objId, const CString& field, bool isProperty)
: m_objId(objId), m_field(field), m_isProperty(isProperty)
{

}

Animation::~Animation()
{
	//stop();
}

AcDbObjectId Animation::getID() const
{
	return m_objId;
}

void Animation::start()
{
	// 关闭undo记录
	ArxEntityHelper::ToggleUndoRecording(m_objId, true);

	AnimationManager::getSingletonPtr()->add(this);
}

void Animation::stop()
{
	// 重新打开undo记录
	ArxEntityHelper::ToggleUndoRecording(m_objId, false);

	//AnimationManager::getSingletonPtr()->remove(this);
	//delete this;
}

DoubleAnimation::DoubleAnimation(const AcDbObjectId& objId, const CString& field, bool isProperty)
: Animation(objId, field, isProperty)
{

}

void DoubleAnimation::update()
{
	if( !tweenX.IsFinished() )
	{
		acutPrintf(_T("\nX=%.3f"), (float)X);
		if(m_isProperty)
		{
			ExtDictData::SetDouble(m_objId, m_field, (float)X);
		}
		else
		{
			EntityData::SetDouble(m_objId, m_field, (float)X);
		}
	}
}

void DoubleAnimation::init(double begin, double end, double v, double k)
{
	X = (float)begin;
	//tweenX.Setup(&X, Tween::InvalidTweenType , 2.0f)->Ease(&(Tween::Easing::BOUNCE_OUT))->Target((float)end)->Repeat(Tween::Infinity, 0.5f, true)->Start(manager);
	double duration = (end-begin)/(v*k);

	Tween::TweenManager* manager = AnimationManager::getSingletonPtr()->getManager();
	tweenX.Setup(&X, Tween::InvalidTweenType, (float)duration)->Ease(&(Tween::Easing::BOUNCE_OUT))->Target((float)end)->Repeat(Tween::Infinity, 0.5f, true)->Start(manager);
}

//
//PointAnimation::PointAnimation(const AcDbObjectId& objId, const CString& field, bool isProperty)
//: Animation(objId, field, isProperty)
//{
//
//}
//void PointAnimation::update()
//{
//	if( !t.is_finished() )
//	{
//		t.update(1);
//	}
//}
//
//void PointAnimation::init(const AcGePoint3d& begin, const AcGePoint3d& end, double v, double k)
//{
//	// 清空所有的单变量动画
//	t.clear();
//
//	EntityData::GetPoint(m_objId, m_field, pt);
//	double duration_x = (end.x-begin.x)/(v*k);
//	double duration_y = (end.x-begin.y)/(v*k);
//	double duration_z = (end.x-begin.z)/(v*k);
//	t.insert
//		( claw::tween::single_tweener
//		( begin.x, end.x, duration_x, boost::bind( &PointAnimation::set_x, this, _1 ),
//		claw::tween::easing_linear::ease_in ) );
//	t.insert
//		( claw::tween::single_tweener
//		( begin.y, end.y, duration_y, boost::bind( &PointAnimation::set_y, this, _1 ),
//		claw::tween::easing_linear::ease_in ) );
//	t.insert
//		( claw::tween::single_tweener
//		( begin.z, end.z, duration_z, boost::bind( &PointAnimation::set_z, this, _1 ),
//		claw::tween::easing_linear::ease_in ) );
//}
//void PointAnimation::set_x(double v)
//{
//	pt.x = v;
//	if(m_isProperty)
//	{
//		ExtDictData::SetPoint(m_objId, m_field, pt);
//	}
//	else
//	{
//		EntityData::SetPoint(m_objId, m_field, pt);
//	}
//}
//void PointAnimation::set_y(double v)
//{
//	pt.y = v;
//	if(m_isProperty)
//	{
//		ExtDictData::SetPoint(m_objId, m_field, pt);
//	}
//	else
//	{
//		EntityData::SetPoint(m_objId, m_field, pt);
//	}
//}
//void PointAnimation::set_z(double v)
//{
//	pt.z = v;
//	if(m_isProperty)
//	{
//		ExtDictData::SetPoint(m_objId, m_field, pt);
//	}
//	else
//	{
//		EntityData::SetPoint(m_objId, m_field, pt);
//	}
//}

// 实例化静态成员变量
AnimationManager* AnimationManager::ms_Singleton = 0;

AnimationManager::AnimationManager( void )
{
	// 单件模式
	assert( !ms_Singleton );
	ms_Singleton = static_cast<AnimationManager*>( this );

	manager = new Tween::TweenManager();
	time = -1.0;
	fDTFactor = 1.0f;
}

AnimationManager::~AnimationManager()
{
	// 单件模式
	assert( ms_Singleton );
	ms_Singleton = 0;

	delete manager;
	manager = 0;
	time = -1.0;
}

AnimationManager& AnimationManager::getSingleton( void )
{
	assert( ms_Singleton );
	return ( *ms_Singleton );
}

AnimationManager* AnimationManager::getSingletonPtr( void )
{
	return ( ms_Singleton );
}

void AnimationManager::Init()
{
	AnimationManager* console = new AnimationManager();
}

void AnimationManager::UnInit()
{
	delete AnimationManager::getSingletonPtr();
}

void AnimationManager::add(Animation* ani)
{
	if(ani == 0) return;
	AcDbObjectId objId = ani->getID();
	if(objId.isNull()) return;

	m_animations.push_back(ani);
}

void AnimationManager::update()
{
	if(time < 0)
	{
		time = (double)kNet::Clock::Time();
	}

	float fDt = (float) ((double)kNet::Clock::Time() - time );
	fDt *= fDTFactor;
	time = (double)kNet::Clock::Time();

	manager->Update(fDt);

	for(AnimationArray::iterator itr=m_animations.begin();itr!=m_animations.end();++itr)
	{
		Animation* ani = *itr;
		ani->update();
	}
}

Tween::TweenManager* AnimationManager::getManager() const
{
	return manager;
}
