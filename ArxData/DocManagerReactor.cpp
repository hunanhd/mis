#include "StdAfx.h"
#include "DocManagerReactor.h"
#include "ArxHelper/HelperClass.h"

DocManagerReactor::DocManagerReactor() : AcApDocManagerReactor()
{
}
void DocManagerReactor::documentToBeActivated( AcApDocument* pDoc )
{
    AcApDocManagerReactor::documentActivated( pDoc );
}
void DocManagerReactor::documentCreated( AcApDocument* pDoc )
{
    AcApDocManagerReactor::documentCreated( pDoc );
    //acutPrintf( _T( "\nDocManagerReactor::documentCreated()" ) );
}
void DocManagerReactor::documentToBeDestroyed( AcApDocument* pDoc )
{
    AcApDocManagerReactor::documentToBeDestroyed( pDoc );
    //acutPrintf( _T( "\nDocManagerReactor::documentToBeDestroyed()" ) );
}
void DocManagerReactor::documentLockModeWillChange( AcApDocument* pDoc, AcAp::DocLockMode myCurrentMode, AcAp::DocLockMode myNewMode, AcAp::DocLockMode currentMode, const ACHAR* pGlobalCmdName )
{
    AcApDocManagerReactor::documentLockModeWillChange( pDoc, myCurrentMode, myNewMode, currentMode, pGlobalCmdName );
    //acutPrintf( _T( "\nDocManagerReactor::documentLockModeWillChange() --> %d %d %d %s" ), ( int )myCurrentMode, ( int )myNewMode, ( int )currentMode, pGlobalCmdName );
}
void DocManagerReactor::documentLockModeChangeVetoed( AcApDocument* pDoc, const ACHAR* pGlobalCmdName )
{
    AcApDocManagerReactor::documentLockModeChangeVetoed( pDoc, pGlobalCmdName );
    //acutPrintf( _T( "\nDocManagerReactor::documentLockModeChangeVetoed() --> %s" ), pGlobalCmdName );
}

// 参考: arxdoc.chm --> Selection Set Manipulation
static bool BuildSelectionSet(const AcDbObjectIdArray& objIds, ads_name& ss)
{
	int k = 0;
	for(int i=0;i<objIds.length();i++)
	{
		ads_name ename;
		if( Acad::eOk != acdbGetAdsName( ename, objIds[i] ) ) continue;
		if( k == 0)
		{
			if( RTNORM != acedSSAdd( ename, NULL, ss ) )
			{
				continue;
			}
			else
			{
				k++;
			}
		}
		
		if( RTNORM != acedSSAdd( ename, ss, ss ) ) continue;
		k++;
	}
	return (k != 0);
}
/**
参见Data/DocManagerReactor::documentLockModeChanged()方法的实现

对某类图元取消操作的方法：
1、使用AcApDocManagerReactor反应器
// http://forums.autodesk.com/t5/objectarx/canceling-a-command-at-commandwillstart/td-p/1379290
// http://through-the-interface.typepad.com/through_the_interface/2006/10/blocking_autoca.html
// http://forums.autodesk.com/t5/visual-lisp-autolisp-and-general/objectarx-disable-and-redefine-a-buildin-command/td-p/888298

重载AcApDocManagerReactor::documentLockModeChanged( AcApDocument* pDoc, AcAp::DocLockMode myPreviousMode, AcAp::DocLockMode myCurrentMode, AcAp::DocLockMode currentMode, const ACHAR* pGlobalCmdName )
根据命令名称pGlobalCmdName来判断是否需要取消某个命令，如果需要取消，则调用this->veto();

ps：取消命令的其它方法还有，给cad命令行强制发送esc按键

2、取消close()
很多cad的操作都是通过close完成的，可以尝试重载subClose()
Called from within close() before anything else is done. The default implementation is to return Acad::eOk. However, when overridden in custom classes, it provides a hook into the close operation. If it returns anything other than Acad::eOk, then the close operation is immediately terminated. 

3、锁定对象，类似图层的锁定功能
尝试无果，没有找到lock object的函数，只有图层可以锁定

4、过滤选择集，排除掉不需要的图形
*/
void DocManagerReactor::documentLockModeChanged( AcApDocument* pDoc, AcAp::DocLockMode myPreviousMode, AcAp::DocLockMode myCurrentMode, AcAp::DocLockMode currentMode, const ACHAR* pGlobalCmdName )
{
	// 取消命令
	// http://forums.autodesk.com/t5/objectarx/canceling-a-command-at-commandwillstart/td-p/1379290
	// http://through-the-interface.typepad.com/through_the_interface/2006/10/blocking_autoca.html
	// http://forums.autodesk.com/t5/visual-lisp-autolisp-and-general/objectarx-disable-and-redefine-a-buildin-command/td-p/888298
	CString cmd(pGlobalCmdName);
	if(cmd.CompareNoCase(_T("ERASE")) == 0)
	{
		// 获取当前已选择的图形
		AcDbObjectIdArray allObjIds;
		ArxUtilHelper::GetPickSetEntity2(allObjIds);

		AcDbObjectIdArray objIds;
		ArxDataTool::FilterEntsByType(_T("AcDbLine"), allObjIds, objIds);

		ads_name ss;
		if( BuildSelectionSet(objIds, ss) )
		{
			acedSSSetFirst( ss, NULL ); // 高亮选中
			acedSSFree( ss );           // 释放选择集

		}
		else
		{
			acedSSSetFirst( NULL, NULL ); // 高亮选中
			this->veto();
			AfxMessageBox(_T("xxxx"));
		}
	}
	else
	{
		AcApDocManagerReactor::documentLockModeChanged( pDoc, myPreviousMode, myCurrentMode, currentMode, pGlobalCmdName );
	}
    //acutPrintf( _T( "\nDocManagerReactor::documentLockModeChanged() --> %d %d %d %s" ), ( int )myPreviousMode, ( int )myCurrentMode, ( int )currentMode, pGlobalCmdName );
}
