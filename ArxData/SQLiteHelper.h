#pragma once
#include "../ArxHelper/HelperClass.h"

class SQLiteHelper
{
public:
    // 列表查找操作
    static void GetStringListNames( MFCStringArray& names );
    static void GetStringList( const CString& name, MFCStringArray& strList );
    static void GetIntStrListNames( MFCStringArray& names );
    static void GetIntStrList( const CString& name, IntArray& intList, MFCStringArray& strList );
    // 字段"增删查改"操作
    static void ClearAllFields();
    static void GetTypes( MFCStringArray& types );
    static void GetFuncs( MFCStringArray& funcs );
    static void RemoveType( const CString& type );
    static void RemoveFunc( const CString& func );
    static void RemoveFuncOfType( const CString& type, const CString& func );
    static void AddField( const CString& getype, const CString& hybirdField, const FieldInfo& info );
    static void UpdateField( const CString& getype, const CString& hybirdField, const FieldInfo& info );
    static void RemoveField( const CString& getype, const CString& hybirdField );
    static void GetFieldInfo( const CString& type, const CString& hybirdField, FieldInfo& info );
    static void GetAllFields( const CString& type, MFCStringArray& hybirdFields );
    static void AddMoreFields( const CString& type, const MFCStringArray& hybirdFields );
    static void RemoveMoreFields( const CString& type, const MFCStringArray& hybirdFields );
    //图元DXF名翻译
    static void GetTypeByDXFName( const CString& dxfName, CString& type );
    static void GetDXFNameByType( const CString& type, CString& dxfName );
};