/*
* 监视文件变化.
* 来源：http://blog.163.com/wslngcjsdxdr@126/blog/static/162196230201231921323351/
*        http://blog.csdn.net/tangyin025/article/details/44959117
*        http://www.cnblogs.com/doublesnke/archive/2011/08/16/2141374.html
*/
#ifndef __FILESYSTEMWATCHER_HPP__
#define __FILESYSTEMWATCHER_HPP__
#if(_WIN32_WINNT < 0x0400)
#define _WIN32_WINNT 0x0400
#endif
#include <windows.h>
/*
* 当FileSystemWatcher发出通知时(文件被修改modify)
* 就会调用注册到FileSystemWatcher中的FileWatcherAction对象
* 以及相应的虚函数onModify()
*/
class FileWatcherAction
{
public:
    virtual ~FileWatcherAction() {}
    // 文件被修改时的动作
    virtual void onModify( const CString& filename ) = 0;
    // 还可以增加其他的一些响应虚函数
    // 比如增加、删除、重命名等
};
class FileSystemWatcher
{
public:
    enum Filter
    {
        FILTER_FILE_NAME        = 0x00000001, // add/remove/rename
        FILTER_DIR_NAME         = 0x00000002, // add/remove/rename
        FILTER_ATTR_NAME        = 0x00000004,
        FILTER_SIZE_NAME        = 0x00000008,
        FILTER_LAST_WRITE_NAME  = 0x00000010, // timestamp
        FILTER_LAST_ACCESS_NAME = 0x00000020, // timestamp
        FILTER_CREATION_NAME    = 0x00000040, // timestamp
        FILTER_SECURITY_NAME    = 0x00000100
    };
    enum ACTION
    {
        ACTION_ERRSTOP          = -1,
        ACTION_ADDED            = 0x00000001,
        ACTION_REMOVED          = 0x00000002,
        ACTION_MODIFIED         = 0x00000003,
        ACTION_RENAMED_OLD      = 0x00000004,
        ACTION_RENAMED_NEW      = 0x00000005
    };
    typedef void ( __stdcall* LPDEALFUNCTION )( ACTION act, LPCWSTR filename, LPVOID lParam );
    FileSystemWatcher();
    ~FileSystemWatcher();
    void RegAction( FileWatcherAction* action );
    bool Run( LPCTSTR dir, bool bWatchSubtree, DWORD dwNotifyFilter, LPVOID lParam );
    void Close( DWORD dwMilliseconds = INFINITE );
private: // no-impl
    FileSystemWatcher( const FileSystemWatcher& );
    FileSystemWatcher operator=( const FileSystemWatcher );
private:
    HANDLE m_hDir;
    DWORD m_dwNotifyFilter;
    bool m_bWatchSubtree;
    HANDLE m_hThread;
    volatile bool m_bRequestStop;
    LPDEALFUNCTION m_DealFun;
    LPVOID m_DealFunParam;
    static DWORD WINAPI Routine( LPVOID lParam );
    struct FileSystemWatcherImpl* d;
};
#endif // __FILESYSTEMWATCHER_HPP__
