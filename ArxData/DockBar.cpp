#include "StdAfx.h"
#include "resource.h"
#include "DockBar.h"
#include "DockBarChildDlg.h"

IMPLEMENT_DYNAMIC ( DockBar, CAcUiDockControlBar )

BEGIN_MESSAGE_MAP( DockBar, CAcUiDockControlBar )
    ON_WM_CREATE()
    ON_WM_SYSCOMMAND()
    ON_WM_SIZE()
END_MESSAGE_MAP()

//-----------------------------------------------------------------------------
//----- DataList_DockBar *pInstance = new DataList_DockBar;
//----- pInstance->Create (acedGetAcadFrame (), "My title bar") ;
//----- pInstance->EnableDocking (CBRS_ALIGN_ANY) ;
//----- pInstance->RestoreControlBar () ;

//-----------------------------------------------------------------------------
DockBar::DockBar ()
	: CAcUiDockControlBar()
{
}

//-----------------------------------------------------------------------------
DockBar::~DockBar ()
{

}

//-----------------------------------------------------------------------------
#ifdef _DEBUG
//- Please uncomment the 2 following lines to avoid linker error when compiling
//- in release mode. But make sure to uncomment these lines only once per project
//- if you derive multiple times from CAdUiDockControlBar/CAcUiDockControlBar
//- classes.

//void CAdUiDockControlBar::AssertValid () const {
//}
#endif

//-----------------------------------------------------------------------------
BOOL DockBar::Create ( CWnd* pParent, LPCTSTR lpszTitle )
{
    CString strWndClass ;
    strWndClass = AfxRegisterWndClass ( CS_DBLCLKS, LoadCursor ( NULL, IDC_ARROW ) ) ;
    CRect rect ( 0, 0, 300, 600 ) ;
    if ( !CAcUiDockControlBar::Create (
                strWndClass, lpszTitle, WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN,
                rect, pParent, 0
            )
       )
        return ( FALSE ) ;

	// 子类负责构造一个全局的GUID
	CLSID* DockBar_GUID = this->GetCLSID();
	if( DockBar_GUID == 0 || *DockBar_GUID == CLSID_NULL ) return FALSE;
	
	SetToolID ( DockBar_GUID ) ;
    return ( TRUE ) ;
}

//-----------------------------------------------------------------------------
//----- This member function is called when an application requests the window be
//----- created by calling the Create or CreateEx member function
int DockBar::OnCreate ( LPCREATESTRUCT lpCreateStruct )
{
    if ( CAcUiDockControlBar::OnCreate ( lpCreateStruct ) == -1 )
        return ( -1 ) ;

	CAcModuleResourceOverride myResources;

	// 创建子对话框
	cdm.create(this);

	//- Move the window over so we can see the control lines
    CRect rect ;
    GetWindowRect ( &rect ) ;
	// 移动对话框
	cdm.move(rect);

	// 默认激活第1个子对话框
	cdm.activate( 0 );

    return ( 0 ) ;
}

//-----------------------------------------------------------------------------
void DockBar::SizeChanged ( CRect* lpRect, BOOL bFloating, int flags )
{
	CAcModuleResourceOverride myResources;
	cdm.sizeChange(this, lpRect);
}

//-----------------------------------------------------------------------------
//-----  Function called when user selects a command from Control menu or when user
//----- selects the Maximize or the Minimize button.
void DockBar::OnSysCommand ( UINT nID, LPARAM lParam )
{
    CAcUiDockControlBar::OnSysCommand ( nID, lParam ) ;
}

//-----------------------------------------------------------------------------
//----- The framework calls this member function after the window's size has changed
void DockBar::OnSize ( UINT nType, int cx, int cy )
{
    CAcUiDockControlBar::OnSize ( nType, cx, cy ) ;

	CAcModuleResourceOverride myResources;
	cdm.onSize(cx, cy);
}

bool DockBar::OnClosing()
{
    bool ret = CAdUiDockControlBar::OnClosing();

	this->Save();

	return ret;
}

void DockBar::Save()
{
	if(cdm.getCurrent() != 0)
	{
		DockBarChildDlg* dlg = (DockBarChildDlg*)cdm.getCurrent();
		if(dlg != 0)
		{
			dlg->Save();
		}
	}
}

/*
http://www.xuebuyuan.com/770576.html
在AutoCAD2004+ObjectARX2004环境下，使用DockControlBar建立了一个停靠的工具栏，上面显示一个自定义的对话框，其中有一个Treelist控件。
我已经给Treelist定义了右键菜单，但是每次点击右键并选择自己的菜单之后，AutoCAD自定义的菜单：“允许固定”“隐藏”还是会跳出来。

解决方法：CAdUiDockControlBar默认了两个菜单项，可以通过自定义菜单来取消这两项：
继承CAdUiDockControlBar类，重载AddCustomMenuItems函数如下：
*/
BOOL DockBar::AddCustomMenuItems(LPARAM hMenu)
{
	HMENU hPopMenu;
	hPopMenu = (HMENU)(hMenu & 0xffff);
	CMenu* pop_menu = CMenu::FromHandle(hPopMenu);

	// 去掉弹出菜单上默认的
	pop_menu->RemoveMenu(ID_ADUI_HIDEBAR, MF_BYCOMMAND);
	pop_menu->RemoveMenu(ID_ADUI_ALLOWDOCK, MF_BYCOMMAND);
	return true;
}