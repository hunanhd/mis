#pragma once
#include "resource.h"
#include "DockBarChildDlg.h"
class PropertyHelpChildDlg : public DockBarChildDlg
{
	DECLARE_DYNAMIC(PropertyHelpChildDlg)
public:
	PropertyHelpChildDlg(CWnd *pParent =NULL, HINSTANCE hInstance =NULL);   // 标准构造函数
	virtual ~PropertyHelpChildDlg();
// 对话框数据
	enum { IDD = IDD_HELP_CHILD_DLG };
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor( CDC* pDC, CWnd* pWnd, UINT nCtlColor );
	CBrush m_brBk;
};
