#include "stdafx.h"
#include "PropertyHelpChildDlg.h"
IMPLEMENT_DYNAMIC( PropertyHelpChildDlg, DockBarChildDlg )
PropertyHelpChildDlg::PropertyHelpChildDlg( CWnd* pParent, HINSTANCE hInstance )
    : DockBarChildDlg( PropertyHelpChildDlg::IDD, pParent, hInstance )
{
}
PropertyHelpChildDlg::~PropertyHelpChildDlg()
{
}
void PropertyHelpChildDlg::DoDataExchange( CDataExchange* pDX )
{
    DockBarChildDlg::DoDataExchange( pDX );
}

BEGIN_MESSAGE_MAP( PropertyHelpChildDlg, DockBarChildDlg )
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

// HelpDlg 消息处理程序
BOOL PropertyHelpChildDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	CBitmap bmp;
	bmp.LoadBitmap( IDB_BG );
	m_brBk.CreatePatternBrush( &bmp );
	bmp.DeleteObject();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

HBRUSH PropertyHelpChildDlg::OnCtlColor( CDC* pDC, CWnd* pWnd, UINT nCtlColor )
{
	HBRUSH hbr = CDialog::OnCtlColor( pDC, pWnd, nCtlColor );
	if ( pWnd == this )
	{
		return m_brBk;
	}
	return hbr;
}
