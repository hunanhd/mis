#ifndef __CONTEXTMENU_DEFAULTARX__H__
#define __CONTEXTMENU_DEFAULTARX__H__

#include "stdafx.h"
#include "aced.h"   // AcEdUIContext

class defaultContext : public  AcEdUIContext
{
public:
    defaultContext();
    ~defaultContext();
    virtual void* getMenuContext(const AcRxClass *pClass, const AcDbObjectIdArray& ids) ;
    virtual void  onCommand(Adesk::UInt32 cmdIndex);
    virtual void OnUpdateMenu();
private:
    CMenu *m_pMenu;
    HMENU m_tempHMenu;
};

#endif // __CONTEXTMENU_DEFAULTARX__H__