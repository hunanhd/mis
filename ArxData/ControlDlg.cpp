#include "stdafx.h"
#include "ControlDlg.h"
#include "atlimage.h"

#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"

//#include "AnimationManager.h"

static void DrawQ()
{
	//static MyRandom rng;
	static int n = 1;
	static AcDbObjectId textObjId;

	ArxDocLockSwitch lock_switch;
	if(!textObjId.isNull())
	{
		CString msg;
		//msg.Format(_T("风量: %.2f m3/s"), rng.randomInt(0, 1000));
		if( n > 100) n = 1;
		msg.Format(_T("风量: %.2f m3/s"), (double)n++);
		EntityData::SetString(textObjId, _T("内容"), msg);
	}
	else
	{
		// 绘制文字
		textObjId = ArxDrawHelper::DrawMText(AcGePoint3d(100,100,0), 0, _T("  "), 100);
	}
}

//static void CreateAnimations()
//{
//	static AcDbObjectId circleObjId1;
//	static AcDbObjectId circleObjId2;
//	ArxDocLockSwitch lock_switch;
//	if(!circleObjId1.isNull())
//	{
//		// fix: 存在内存泄漏!!!
//		DoubleAnimation* ani1 = new DoubleAnimation(circleObjId1, _T("半径"), false);
//		ani1->init(10, 100, 10, 1);
//		ani1->start();
//	}
//	else
//	{
//		// 绘制半径等于10的圆
//		circleObjId1 = ArxDrawHelper::DrawCircle(AcGePoint3d(100,100,0), 10);
//	}
//
//	if(!circleObjId2.isNull())
//	{
//		// fix: 存在内存泄漏!!!
//		DoubleAnimation* ani2 = new DoubleAnimation(circleObjId2, _T("半径"), false);
//		ani2->init(10, 100, 1, 1);
//		ani2->start();
//	}
//	else
//	{
//		// 绘制半径等于10的圆
//		circleObjId2 = ArxDrawHelper::DrawCircle(AcGePoint3d(500,100,0), 10);
//	}
//}
//
// 图片的宽和高
#define IMAGE_WIDTH 32
#define IMAGE_HEIGHT 32
#define TIMER_ID1 10

IMPLEMENT_DYNAMIC(ControlDlg, AcadDialog)

ControlDlg::ControlDlg(CWnd* pParent, BOOL bModal)
	: AcadDialog(ControlDlg::IDD, pParent, bModal)
{
}

ControlDlg::~ControlDlg()
{
}

void ControlDlg::DoDataExchange(CDataExchange* pDX)
{
	AcadDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(ControlDlg, AcadDialog)
	ON_WM_PAINT()
	ON_WM_NCHITTEST()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_TIMER()
END_MESSAGE_MAP()

BOOL ControlDlg::OnInitDialog()
{
	AcadDialog::OnInitDialog();

	// 绘制不规则窗口
    UpdateDisplay(m_pPrime);

	// 设置定时器(可以设置多个定时器,不同定时器的刷新频率不一样)
	// fps大约等于30,即1秒钟刷新30次
    //SetTimer(TIMER_ID1, 30, NULL);

	// 创建动画
	//CreateAnimations();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

int ControlDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (AcadDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
    hFuncInst = LoadLibrary(_T("User32.DLL")); 
	BOOL bRet=FALSE;
	if(hFuncInst) 
	{
		UpdateLayeredWindow=(MYFUNC)GetProcAddress(hFuncInst, "UpdateLayeredWindow");
	}
	else
	{
		AfxMessageBox(_T("User32.dll ERROR!"));
		exit(0);
	}
	
	// 加载图片资源到m_pPrime变量
	// Initialize GDI+.
	m_Blend.BlendOp=0; //theonlyBlendOpdefinedinWindows2000
	m_Blend.BlendFlags=0; //nothingelseisspecial...
	m_Blend.AlphaFormat=1; //...
	m_Blend.SourceConstantAlpha=255;//AC_SRC_ALPHA
    ImageFromIDResource(IDB_TOUCH_BALL,_T("PNG"),m_pPrime);
	// 设置窗口大小
	CRect rect;
	GetClientRect(&rect);
	::SetWindowPos(m_hWnd, HWND_TOPMOST,0,0,rect.Width(),rect.Height(),SWP_NOSIZE|SWP_NOMOVE);

	return 0;
}

void ControlDlg::OnDestroy()
{
    if(m_pPrime)
    {
	    delete m_pPrime;
        m_pPrime = NULL;
    }
	FreeLibrary(hFuncInst); 
	AcadDialog::OnDestroy();
}

LRESULT ControlDlg::OnNcHitTest(CPoint pt)
{
    LRESULT nHitTest = AcadDialog::OnNcHitTest(pt);
    if(nHitTest == HTCLIENT)
    {
        nHitTest = HTCAPTION;
    }
    return nHitTest;
}

void ControlDlg::OnTimer(UINT_PTR nIDEvent)
{
	if(nIDEvent == TIMER_ID1)
	{
		//ArxDocLockSwitch lock_switch;
		//AnimationManager::getSingletonPtr()->update();
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
void ControlDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting
		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);
		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;
		// Draw the icon
		//dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
        AcadDialog::OnPaint();
	}
}

BOOL ControlDlg::ImageFromIDResource(UINT resurceID,LPCTSTR imgType,Image * &pImg)
{
	HINSTANCE hInst = AfxGetResourceHandle();
	HRSRC hRsrc = ::FindResource (hInst,MAKEINTRESOURCE(resurceID),imgType); // type
	if (hRsrc)
    {
	    // load resource into memory
	    DWORD len = SizeofResource(hInst, hRsrc);
	    BYTE* lpRsrc = (BYTE*)LoadResource(hInst, hRsrc);
	    if (lpRsrc)
        {
            // Allocate global memory on which to create stream
	        HGLOBAL m_hMem = GlobalAlloc(GMEM_FIXED, len);
	        BYTE* pmem = (BYTE*)GlobalLock(m_hMem);
	        memcpy(pmem,lpRsrc,len);
	        IStream* pstm;
	        CreateStreamOnHGlobal(m_hMem,FALSE,&pstm);
	
	        // load from stream
	        pImg=Gdiplus::Image::FromStream(pstm);
	        // free/release stuff
	        GlobalUnlock(m_hMem);
	        pstm->Release();
	        FreeResource(lpRsrc);
            return TRUE;
        }
    }
    return FALSE;
}

BOOL ControlDlg::UpdateDisplay(Image *image, int Transparent)
{
    int imageWidth = IMAGE_WIDTH;
    int imageHeight = IMAGE_HEIGHT;//magic number of my image width and height
	HDC hdcTemp=GetDC()->m_hDC;
	m_hdcMemory=CreateCompatibleDC(hdcTemp);
	HBITMAP hBitMap=CreateCompatibleBitmap(hdcTemp,imageWidth,imageHeight);
	SelectObject(m_hdcMemory,hBitMap);
	if(Transparent<0||Transparent>100)	Transparent=100;
	m_Blend.SourceConstantAlpha=int(Transparent*2.55);//1~255, if you want to change transparent, modify this.
	HDC hdcScreen=::GetDC (m_hWnd);
	RECT rct;
	GetWindowRect(&rct);
	POINT ptWinPos={rct.left,rct.top};
	Graphics graph(m_hdcMemory);
	Point points[] = { Point(0, 0), 
		               Point(imageWidth, 0), //width
					   Point(0, imageHeight)//height
					};
    graph.DrawImage(image,points,3);//Do it!
	SIZE sizeWindow={imageWidth,imageHeight};
	POINT ptSrc={0,0};
	DWORD dwExStyle=GetWindowLong(m_hWnd,GWL_EXSTYLE);
	if((dwExStyle&0x80000)!=0x80000)
		SetWindowLong(m_hWnd,GWL_EXSTYLE,dwExStyle^0x80000);
	BOOL bRet=FALSE;
	bRet= UpdateLayeredWindow( m_hWnd,hdcScreen,&ptWinPos,
				&sizeWindow,m_hdcMemory,&ptSrc,0,&m_Blend,2);
	graph.ReleaseHDC(m_hdcMemory);
	::ReleaseDC(m_hWnd,hdcScreen);
	hdcScreen=NULL;
	::ReleaseDC(m_hWnd,hdcTemp);
	hdcTemp=NULL;
	DeleteObject(hBitMap);
	DeleteDC(m_hdcMemory);
	m_hdcMemory=NULL;
	return bRet;
}
