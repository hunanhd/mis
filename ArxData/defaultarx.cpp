#include "StdAfx.h"

#include <aced.h>       // AcEdUIContext
#include <rxmfcapi.h>
#include "defaultarx.h" // default mode.
#include <acdocman.h>
#include "resource.h"

//extern "C" HINSTANCE _hdllInstance;
extern HINSTANCE _hdllInstance;

defaultContext::defaultContext()
{
    acDocManager->pushResourceHandle(_hdllInstance);

    m_pMenu = new CMenu;
    m_pMenu->LoadMenu(IDR_DEFAULT_MENU); 

	acDocManager->popResourceHandle();
}

defaultContext::~defaultContext()
{
    if (m_pMenu) 
        delete m_pMenu;
}

// This function should return pointer to the HMENU that is to
// be added to AutoCAD context menu.
void* defaultContext::getMenuContext(const AcRxClass*, const AcDbObjectIdArray&)
{ 
    m_tempHMenu = m_pMenu->GetSubMenu(0)->GetSafeHmenu();   
    return &m_tempHMenu;
}

// This function is called when user selects a ARX added item
// The cmdIndex is the ID of the menu item as supplied by ARX app.
void defaultContext::onCommand(Adesk::UInt32 cmdIndex)
{
    acDocManager->pushResourceHandle(_hdllInstance);

    CString str1, str2;
    m_pMenu->GetMenuString(cmdIndex,str1,MF_BYCOMMAND);
    str2.Format(IDS_DEFAULTARX_SELECTION,str1);
    acutPrintf(str2);

    // display the command prompt again.
    acedPostCommandPrompt();

    acDocManager->popResourceHandle();
}

// Change the state of the menu items here. This function is
// called by AutoCAD just before querying the app for the context menu
void defaultContext::OnUpdateMenu()
{
    m_pMenu->EnableMenuItem(ID_DEFAULT_ITEM1,MF_ENABLED);
    m_pMenu->EnableMenuItem(ID_DEFAULT_SUB_ITEM1,MF_GRAYED);
    
}
