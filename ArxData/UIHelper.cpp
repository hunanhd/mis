#include "StdAfx.h"
#include "UIHelper.h"
#include "ParamManager.h"

#include "PropertyDlgHelper.h"
#include "PropertyPalette.h"
#include "PropertyPaletteSet.h"

#include "FieldManagerDlg.h"

#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"

PropertyPaletteSet *pPropertyPaletteSet = 0;

void UIHelper::ShowFieldManagerDlg()
{
    // 切换资源
    CAcModuleResourceOverride myResources;
    FieldManagerDlg dlg;
    dlg.DoModal(); // 属性数据字段管理
    //// 启动字段管理程序
    //CString exePath = ARX_FILE_PATH( _T( "FieldManager.exe" ) );
    //CString dbFile = CURRENT_DB_FILE;
    //CString cmdLine = dbFile;
    //CString cwdPath = PathHelper::GetDirectory( dbFile );
    //HANDLE hProcess, hThread;
    //ThreadHelper::RunProecess( exePath, cmdLine, cwdPath, hProcess, hThread, true );
}
void UIHelper::DisplayData()
{
    // 显示图元的属性数据
    AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一个图元:" ) );
    if( objId.isNull() ) return;
    PropertyDlgHelper::DisplayPropertyDataDlg( objId );
}
void UIHelper::DisplayDataByDoubleClick()
{
    //acutPrintf(_T("\n双击自定义..."));
    AcDbObjectIdArray objIds;
    ArxUtilHelper::GetPickSetEntity( objIds );
    //if( objIds.length() != 1 ) return;
    // 显示属性对话框
    PropertyDlgHelper::DisplayPropertyDataDlg( objIds[0] );
}
void UIHelper::ShowPropertyPalette()
{
	CAcModuleResourceOverride myResources;
	CMDIFrameWnd* pAcadFrame = acedGetAcadFrame();
	// If the paletteset is already created, show it
	if(pPropertyPaletteSet != 0)
	{
		pAcadFrame->ShowControlBar(pPropertyPaletteSet, TRUE, FALSE);
		return;
	}
	// If paletteset not created, create new palette set
	PropertyPalette *pPalette1;
	pPropertyPaletteSet = new PropertyPaletteSet;
	CRect rect(0, 0, 260, 500);
	pPropertyPaletteSet->Create(_T("属性数据"),
		WS_OVERLAPPED | WS_DLGFRAME, 
		rect, 
		acedGetAcadFrame(), 
		PSS_EDIT_NAME | 
		PSS_PROPERTIES_MENU | 
		PSS_AUTO_ROLLUP |
		PSS_CLOSE_BUTTON
		);
	// Instantiate Palettes
	pPalette1 = new PropertyPalette();  // data palette
	//-------------Palette1-----------------------------------------
	pPalette1->Create(WS_CHILD | WS_VISIBLE,
		_T("Palette 1"),
		pPropertyPaletteSet,
		PS_EDIT_NAME);
	// Load Palette1's persistent data
	// LoadPalette(pPalette1);
	// Add palette to palette set
	pPropertyPaletteSet->AddPalette(pPalette1);
	// Finally show the palette set
	pPropertyPaletteSet->EnableDocking(CBRS_ALIGN_LEFT | CBRS_ALIGN_RIGHT);
	pPropertyPaletteSet->RestoreControlBar(AFX_IDW_DOCKBAR_RIGHT);
	// Load PaletteSet pesistent data
	pPropertyPaletteSet->LoadPaletteSet();
	pAcadFrame->ShowControlBar(pPropertyPaletteSet, TRUE, FALSE);
	if (pPropertyPaletteSet->GetOpacity() !=100)
		pPropertyPaletteSet->SetOpacity(100);
	// Change the title of the paletteset
	pPropertyPaletteSet->SetName(_T("属性数据"));
}
void UIHelper::DestroyPropertyPalette()
{
	CAcModuleResourceOverride myResources;
	// Destroy the paletteset
	if(pPropertyPaletteSet)
	{
		pPropertyPaletteSet->DestroyWindow();
		delete pPropertyPaletteSet;
		pPropertyPaletteSet = 0;
	}
}
void UIHelper::UpdatePropertyPalette(const AcDbObjectId& objId)
{
	if( pPropertyPaletteSet == 0 ) return;
	// 切换对话框或更新对话框内容
	CAcModuleResourceOverride resourceOverride;
	if( pPropertyPaletteSet->IsWindowVisible() )
	{
		PropertyPalette* palette = (PropertyPalette*)pPropertyPaletteSet->GetPalette(0);
		if( palette == 0 ) return;
		// 非模态对话框要锁定文档才能访问cad数据库
		ArxDocLockSwitch lock_switch;
		// 更新子对话框
		palette->ShowChildDlg(objId);
	}
}
