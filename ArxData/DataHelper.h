#pragma once
class DataHelper
{
public:
    // 从数据库读取字段和列表数据并添加到arx词典中
    static void InitFields();
    static void InitFields2();
    static void InitStrList();
    static void InitIntStrList();
    static void WriteFieldToJson();
    // 初始化/反初始化字段词典等
    static void InitAllData();
    static void UnInitAllData();
    // 更新块定义
    static void UpdateDwgBlock();
    /*
     * 导出所有图元的数据
     * 说明:
     *    (1) 所有的图元数据都放在一张数据库表中
     *    (2) 表有4列：id、类型名称、几何参数(json字符串)、属性数据(json字符串)、备注
     * use_memory_model -- 表示是否使用sqlite3的内存模式(某些情况下可以提升sql的操作性能!!!)
     */
    static void ExportAllData( bool use_memory_model = true );
    /*
    * 导入所有图元的数据
    */
    static void ImportAllData();
};