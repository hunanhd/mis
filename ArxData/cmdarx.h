#ifndef __CONTEXTMENU_CMDARX__H__
#define __CONTEXTMENU_CMDARX__H__

#include "stdafx.h"
#include "aced.h"   // AcEdUiContext

extern void cm();

class cmCommandContext : public  AcEdUIContext
{
public:
    cmCommandContext();
    ~cmCommandContext();
    virtual void* getMenuContext(const AcRxClass *pClass, const AcDbObjectIdArray& ids) ;
    virtual void  onCommand(Adesk::UInt32 cmdIndex);
    virtual void  OnUpdateMenu();
private:
    CMenu *m_pMenu;
    HMENU m_tempHMenu;
};

#endif // __CONTEXTMENU_CMDARX__H__