#include "StdAfx.h"
#include "DataHelper.h"
#include "DataBaseName.h"
#include "ParamManager.h"
#include "config.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
#include "KompexSQLitePrerequisites.h"
#include "KompexSQLiteDatabase.h"
#include "KompexSQLiteStatement.h"
#include "KompexSQLiteException.h"
#include "KompexSQLiteStreamRedirection.h"
#include "KompexSQLiteBlob.h"
#include <json/json.h>
void DataHelper::InitStrList()
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        SQL.Format( _T( "SELECT * FROM %s;" ), STR_LIST_TABLE );
        pStmt->Sql( SQL );
        while( pStmt->FetchRow() )
        {
            CString field = U2W( pStmt->GetColumnString( 1 ).c_str() );
            CString strValue = U2W( pStmt->GetColumnString( 2 ).c_str() );
            //acutPrintf(_T("\nfield:%s\tintValue:%d\tstrValue:%s"),field, intValue, strValue);
            StringListHelper::AddString( field, strValue );
        }
        // do not forget to clean-up
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        acutPrintf( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
    }
}
// 初始化"字符串-整数"列表
void DataHelper::InitIntStrList()
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        SQL.Format( _T( "SELECT * FROM %s;" ), INT_LIST_TABLE );
        pStmt->Sql( SQL );
        while( pStmt->FetchRow() )
        {
            CString field = U2W( pStmt->GetColumnString( 1 ).c_str() );
            int intValue = pStmt->GetColumnInt( 2 );
            CString strValue = U2W( pStmt->GetColumnString( 3 ).c_str() );
            //acutPrintf(_T("\nfield:%s\tintValue:%d\tstrValue:%s"),field, intValue, strValue);
            IntStrListHelper::AddIntStrPair( field, intValue, strValue );
        }
        // do not forget to clean-up
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        acutPrintf( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
    }
}
void DataHelper::InitFields()
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        SQL.Format( _T( "SELECT * FROM %s;" ), FIELD_INFO_TABLE );
        pStmt->Sql( SQL );

        // process all results
        while( pStmt->FetchRow() )
        {
            CString func = U2W( pStmt->GetColumnString( "func" ).c_str() );
            CString getype = U2W( pStmt->GetColumnString( "getype" ).c_str() );
            CString field = U2W( pStmt->GetColumnString( 3 ).c_str() );
            CString fieldType = U2W( pStmt->GetColumnString( 4 ).c_str() );
            CString defaultValue = U2W( pStmt->GetColumnString( 5 ).c_str() );
            CString tole = U2W( pStmt->GetColumnString( 6 ).c_str() );
            CString editable = U2W( pStmt->GetColumnString( 7 ).c_str() );
            CString toolTips = U2W( pStmt->GetColumnString( 8 ).c_str() );
            //acutPrintf(_T("\n\nGetDataCount: %d" ), pStmt->GetDataCount() );
            //acutPrintf(_T("\nSQL query - GetColumnDouble(0): %.1lf" ), pStmt->GetColumnDouble(0) );
            //acutPrintf(_T("\nSQL query - func: %s" ), func);
            //acutPrintf(_T("\nSQL query - getype: %s" ), getype);
            //acutPrintf(_T("\nSQL query - field: %s" ), field);
            //acutPrintf(_T("\nSQL query - fieldType: %s" ), fieldType);
            //acutPrintf(_T("\nSQL query - defaultValue: %s" ), defaultValue);
            //acutPrintf(_T("\nSQL query - tole: %s" ), tole);
            //acutPrintf(_T("\nSQL query - editable: %s" ), editable);
            //acutPrintf(_T("\nSQL query - toolTips: %s" ), toolTips);
            FieldInfo info;
            FieldInfo::Set( info, fieldType, defaultValue, tole, editable, toolTips );
            // 特殊处理: 字段名(field)
            CString hybirdField = FieldHelper::MakeHybirdField( func, field );
            // 添加字段
            bool ret = FieldHelper::AddField( getype, hybirdField );
            /**
            * 修改字段信息
            */
            if( true )
            {
                // 更新已有的字段信息
                FieldInfoHelper::RemoveFieldInfo( getype, hybirdField );
                FieldInfoHelper::AddFieldInfo( getype, hybirdField, info );
            }
        }
        // do not forget to clean-up
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        acutPrintf( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
    }
}
void DataHelper::InitFields2()
{
    std::ifstream infile("db.json");
    if(!infile) return;

    Json::Value root;
    Json::Reader reader;
    if(!reader.parse(infile, root, false)) 
    {
        infile.close();
        return;
    }
    if(!root.isMember("fileds"))
    {
        infile.close();
        return;
    }

    Json::Value child = root["fields"];
    // 循环访问fields下的key-value对
    //
    Json::Value::Members mem = child.getMemberNames();
    Json::Value::Members::iterator itr;
    for ( itr = mem.begin(); itr != mem.end(); itr++ )
    {
        std::string sGeType = *itr;
        Json::Value fields = child[sGeType];
        
        CString field = C2W( sGeType.c_str() );
        if ( fields.type() == Json::arrayValue )
        {
            //sValue = root[sKey].asString();
            for(Json::Value::iterator itr2=fields.begin();itr2!=fields.end();++itr2)
            {
                Json::Value info = *itr2;
                CString func = C2W (info["func"].asCString() );
                acutPrintf(_T("\n类型:%s 分组:%s"), field, func);
            }
        }
    }
}

// 初始化字段和数据列表
void DataHelper::InitAllData()
{
    //ArxDataTool::RegAppName( acdbHostApplicationServices()->workingDatabase(), DRAW_PARAMS_XDATA_GROUP );
    // 注册dictionary和regAppName
    ArxDictTool::RegDict( PROPERTY_DATA_FIELD_DICT );
    ArxDictTool::RegDict( PROPERTY_DATA_FIELD_INFO_DICT );
    ArxDictTool::RegDict( STRING_LIST_DICT );
    ArxDictTool::RegDict( INT_LIST_DICT );
	ArxDictTool::RegDict( PARAM_DICT );
    //创建标注样式
    ArxEntityHelper::CreateDimStyle( DIM_STYLE_NAME1, false, 1 );
    ArxEntityHelper::CreateDimStyle( DIM_STYLE_NAME2, false, 0.5 );
    ArxEntityHelper::CreateDimStyle( DIM_STYLE_NAME3, false, 0.5 );
    // 从数据中读取字段
    DataHelper::InitFields();
    DataHelper::InitIntStrList();
    DataHelper::InitStrList();
}
void DataHelper::UnInitAllData()
{
    // 删除词典
    ArxDictTool::RemoveDict( PROPERTY_DATA_FIELD_DICT );
    ArxDictTool::RemoveDict( PROPERTY_DATA_FIELD_INFO_DICT );
    ArxDictTool::RemoveDict( STRING_LIST_DICT );
    ArxDictTool::RemoveDict( INT_LIST_DICT );
}
// 更新瓦斯地质图块定义
void DataHelper::UpdateDwgBlock()
{
    //CString dwgFilePath = _T("C:\\Users\\anheihb03dlj\\Desktop\\瓦斯地质图块定义.dwg");
    CString dwgFile = ARX_FILE_PATH( BLOCK_DWG_NAME );
    if( ArxDwgHelper::MergeBlocks( dwgFile ) )
    {
        acutPrintf( _T( "\n更新块成功!\n" ) );
        ArxDwgHelper::UpdateDwg(); // 更新图形
    }
    else
    {
        acutPrintf( _T( "\n更新块失败!\n" ) );
    }
}
static void ExportLayerDatas( Kompex::SQLiteStatement* pStmt )
{
    // (1-1) 清空LayerData表
    CString SQL;
    SQL.Format( _T( "delete from %s;" ), LAYER_DATA_TABLE ); //清空数据
    pStmt->Sql( SQL );
    pStmt->ExecuteAndFree();
    // (1-2) LayerData表的自增长id改成从0开始
    SQL.Format( _T( "update sqlite_sequence SET seq = 0 where name='%s';" ), LAYER_DATA_TABLE ); //自增长ID为0
    pStmt->Sql( SQL );
    pStmt->ExecuteAndFree();
    // 查找所有的图层
    AcStringArray names;
    AcDbObjectIdArray layers;
    ArxLayerHelper::GetAllLayers( names, layers );
    // (1-3) 编译sql语句,插入数据到LayerData表中
    SQL.Format( _T( "INSERT INTO %s (id, name, geo_datas) VALUES (?, ?, ?);" ), LAYER_DATA_TABLE );
    pStmt->Sql( SQL );
    for( int i = 0; i < layers.length(); i++ )
    {
        AcDbObjectId objId = layers[i]; // id
        CString name = names[i].kACharPtr(); // 图层名称
        // 导出几何参数
        CString geoDatas;
        EntityData::ExportToJsonString( objId, geoDatas );
        //acutPrintf( _T( "\nhandle:%s  type:%s\ngeo_datas:%s\nprop_datas:%s" ), ArxUtilHelper::ObjectIdToStr( objId ), type, geoDatas, propDatas );
        // 写入到数据库
        pStmt->BindNull( 1 );
        pStmt->BindString( 2, W2U( ( LPCTSTR )name ) );
        pStmt->BindString( 3, W2U( ( LPCTSTR )geoDatas ) );
        pStmt->Execute();
        pStmt->Reset();
    }
    pStmt->FreeQuery();
}
static void ExportEntityDatas( Kompex::SQLiteStatement* pStmt )
{
    // (2-1) 清空EntityData表
    CString SQL;
    SQL.Format( _T( "delete from %s;" ), ENTITY_DATA_TABLE ); //清空数据
    pStmt->Sql( SQL );
    pStmt->ExecuteAndFree();
    // (2-2) EntityData表的自增长id改成从0开始
    SQL.Format( _T( "update sqlite_sequence SET seq = 0 where name='%s';" ), ENTITY_DATA_TABLE ); //自增长ID为0
    pStmt->Sql( SQL );
    pStmt->ExecuteAndFree();
    // (2-3) 编译sql语句,插入数据到EntityData表中
    SQL.Format( _T( "INSERT INTO %s (id, handle, type, geo_datas, prop_datas) VALUES (?, ?, ?, ?, ?);" ), ENTITY_DATA_TABLE );
    pStmt->Sql( SQL );
    // 查找所有Entity类派生的图元
    AcDbObjectIdArray ents;
    ArxDataTool::GetEntsByType( _T( "BaseEntity" ), ents, true );
    for( int i = 0; i < ents.length(); i++ )
    {
        AcDbObjectId objId = ents[i]; // id
        CString type;
        ArxDataTool::GetTypeName( objId, type ); // 类型名称
        // 导出几何参数
        CString geoDatas;
        EntityData::ExportToJsonString( objId, geoDatas );
        // 导出属性数据
        CString propDatas;
        ExtDictData::ExportToJsonString( objId, propDatas );
        //acutPrintf( _T( "\nhandle:%s  type:%s\ngeo_datas:%s\nprop_datas:%s" ), ArxUtilHelper::ObjectIdToStr( objId ), type, geoDatas, propDatas );
        // 写入到数据库
        pStmt->BindNull( 1 );
        pStmt->BindString( 2, W2U( ( LPCTSTR )ArxUtilHelper::ObjectIdToStr( objId ) ) );
        pStmt->BindString( 3, W2U( ( LPCTSTR )type ) );
        pStmt->BindString( 4, W2U( ( LPCTSTR )geoDatas ) );
        pStmt->BindString( 5, W2U( ( LPCTSTR )propDatas ) );
        pStmt->Execute();
        pStmt->Reset();
    }
    pStmt->FreeQuery();
}
void DataHelper::ExportAllData( bool use_memory_model )
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    //// 查找所有Entity类派生的图元
    //AcDbObjectIdArray ents;
    //ArxDataTool::GetEntsByType( _T( "BaseEntity" ), ents, true );
    //// 如果图元个数小于100个,则不使用内存模式
    //// 数据量较小的情况下使用内存模式反倒效率会更低!!!
    //// todo: 100这个数是随便写的,没有依据,应通过大量的测试来确定这个值!!!
    //if( ents.length() < 100 )
    //{
    //    use_memory_model = false;
    //}
    // 将db文件复制到系统临时目录下
    CString tempDbFile;
    if( use_memory_model )
    {
        tempDbFile = PathHelper::BuildPath( PathHelper::GetTempPath(), PathHelper::GetTimeStampFileName( _T( "mem" ) ) + _T( ".db" ) );
        if( FALSE == CopyFile( dbFile, tempDbFile, FALSE ) ) return;
    }
    try
    {
        Kompex::SQLiteDatabase* pDatabase;
        if( use_memory_model )
        {
            pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )tempDbFile ), SQLITE_OPEN_READWRITE, 0 );
            // move database to memory, so that we are work on the memory database hence
            // 转换成内存数据库模型
            pDatabase->MoveDatabaseToMemory();
        }
        else
        {
            pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, 0 );
        }
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        // 导出图层数据
        ExportLayerDatas( pStmt );
        // 导出图元数据
        ExportEntityDatas( pStmt );
        if( use_memory_model )
        {
            // save the memory database to a file
            // if you don't do it, all database changes will be lost after closing the memory database
            // 将内存数据库保存到磁盘中
            pDatabase->SaveDatabaseFromMemoryToFile( W2C( ( LPCTSTR )dbFile ) );
        }
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        acutPrintf( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
    }
    // 删除临时数据库文件
    DeleteFile( tempDbFile );
}
static void ImportLayerDatas( Kompex::SQLiteStatement* pStmt )
{
    // (1 )编译sql语句,从LayerData表中读取数据
    CString SQL;
    SQL.Format( _T( "SELECT * FROM %s;" ), LAYER_DATA_TABLE );
    pStmt->Sql( SQL );
    while( pStmt->FetchRow() )
    {
        CString name = U2W( pStmt->GetColumnString( 1 ).c_str() );
        CString geo_datas = U2W( pStmt->GetColumnString( 2 ).c_str() );
        // 根据图层名称得到图层id
        AcDbObjectId objId = ArxLayerHelper::GetLayerId( name );
        if( objId.isNull() )
        {
            // 新增图层
            objId = ArxLayerHelper::AddLayer( name );
        }
        // 更新图层的几何参数
        EntityData::ImportFromJsonString( objId, geo_datas );
    }
    pStmt->FreeQuery();
}
static void ImportEntityDatas( Kompex::SQLiteStatement* pStmt )
{
    //// 删除所有的Entity图元
    //AcDbObjectIdArray objIds;
    //ArxDataTool::GetEntsByType( _T( "BaseEntity" ), objIds, true );
    //ArxEntityHelper::EraseObjects2( objIds, Adesk::kTrue );
    // (2) 编译sql语句,从EntityData表中读取数据
    CString SQL;
    SQL.Format( _T( "SELECT * FROM %s;" ), ENTITY_DATA_TABLE );
    pStmt->Sql( SQL );
    while( pStmt->FetchRow() )
    {
        CString handle = U2W( pStmt->GetColumnString( 1 ).c_str() );
        CString type = U2W( pStmt->GetColumnString( 2 ).c_str() );
        CString geo_datas = U2W( pStmt->GetColumnString( 3 ).c_str() );
        CString prop_datas = U2W( pStmt->GetColumnString( 4 ).c_str() );
        // 非Entity派生的图元类
        if( !ArxClassHelper::IsDerivedFrom( type, _T( "BaseEntity" ) ) ) continue;
        //acutPrintf(_T("\nhandle:--%s--"), handle);
        // 图元id
        AcDbObjectId objId;
        // 如果handle为空,表示要增加图元
        if( handle == _T( "" ) )
        {
            // 动态创建图元并提交到cad图形数据库
            objId = ArxUtilHelper::CreateEntity( type );
            if( objId.isNull() ) continue;
        }
        else
        {
            // 如果handle不为空,但cad中找不到对应的object id,那么cad中的图元可能已被删除了
            objId = ArxUtilHelper::StrtoObjectId( handle );
            if( objId.isNull() ) continue;
        }
        // 更新图元的几何参数 和 属性数据
        EntityData::ImportFromJsonString( objId, geo_datas );
        ExtDictData::ImportFromJsonString( objId, prop_datas );
    }
    pStmt->FreeQuery();
}
void DataHelper::ImportAllData()
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    try
    {
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        // 导入图层数据
        ImportLayerDatas( pStmt );
        // 导入图元数据
        ImportEntityDatas( pStmt );
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;
    }
    catch( Kompex::SQLiteException& exception )
    {
        acutPrintf( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
    }
}

void DataHelper::WriteFieldToJson()
{
    CString dbFile = DB_FILE;
    if( dbFile == _T( "" ) ) return;
    try
    {
        Json::Value root;
        Json::Value child_fields;
        Kompex::SQLiteDatabase* pDatabase = new Kompex::SQLiteDatabase( W2U( ( LPCTSTR )dbFile ), SQLITE_OPEN_READONLY | SQLITE_OPEN_NOMUTEX, 0 );
        Kompex::SQLiteStatement* pStmt = new Kompex::SQLiteStatement( pDatabase );
        CString SQL;
        SQL.Format( _T( "SELECT * FROM %s;" ), FIELD_INFO_TABLE );
        pStmt->Sql( SQL );
        // process all results
        while( pStmt->FetchRow() )
        {
            CString func = U2W( pStmt->GetColumnString( "func" ).c_str() );
            CString getype = U2W( pStmt->GetColumnString( "getype" ).c_str() );
            CString field = U2W( pStmt->GetColumnString( 3 ).c_str() );
            CString fieldType = U2W( pStmt->GetColumnString( 4 ).c_str() );
            CString defaultValue = U2W( pStmt->GetColumnString( 5 ).c_str() );
            CString tole = U2W( pStmt->GetColumnString( 6 ).c_str() );
            CString editable = U2W( pStmt->GetColumnString( 7 ).c_str() );
            CString toolTips = U2W( pStmt->GetColumnString( 8 ).c_str() );

            Json::Value child;
            child["func"] = W2C( (LPCTSTR)func );
            child["field"] = W2C( (LPCTSTR)field );
            child["fieldType"] = W2C( (LPCTSTR)fieldType );
            child["defaultValue"] = W2C( (LPCTSTR)defaultValue );
            child["tole"] = W2C( (LPCTSTR)tole );
            child["editable"] = W2C( (LPCTSTR)editable );
            child["toolTips"] = W2C( (LPCTSTR)toolTips );

            std::string sGeType = W2C( (LPCTSTR)getype );
            child_fields[sGeType].append(child);

        }
        root["fields"] = child_fields;

        // do not forget to clean-up
        pStmt->FreeQuery();
        pDatabase->Close();
        // clean-up
        delete pStmt;
        delete pDatabase;

        // 输出无格式json字符串
        Json::FastWriter writer;
        std::string str = writer.write( root );

        std::ofstream outfile("test.json");
        outfile << str;
        outfile.close();
    }
    catch( Kompex::SQLiteException& exception )
    {
        acutPrintf( _T( "\n%s" ), C2W( exception.GetString().c_str() ) );
    }
}
