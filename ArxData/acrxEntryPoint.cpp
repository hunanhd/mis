#include "StdAfx.h"
#include "resource.h"
#include "DataHelper.h"
#include "ReactorHelper.h"
#include "DwgHelper.h"
#include "UIHelper.h"
#include "ParamManager.h"
#include "DBMonitorManager.h"
#include "ArxHelper/HelperClass.h"

// 定义注册服务名称
#ifndef ARXDATA_SERVICE_NAME
#define ARXDATA_SERVICE_NAME _T("ARXDATA_SERVICE_NAME")
#endif

ULONG_PTR m_gdiplusToken; 
class CArxDataApp : public AcRxArxApp
{
public:
    CArxDataApp () : AcRxArxApp () {}
    virtual AcRx::AppRetCode On_kInitAppMsg ( void* pkt )
    {
        AcRx::AppRetCode retCode = AcRxArxApp::On_kInitAppMsg ( pkt ) ;
        acrxRegisterAppMDIAware( pkt );
        // 注册服务
        acrxRegisterService( ARXDATA_SERVICE_NAME );
		// 初始化GDI+环境
        //use GDIplus begin
        GdiplusStartupInput gdiplusStartupInput;    
        GdiplusStartup(&m_gdiplusToken, &gdiplusStartupInput, NULL); 
        //use GDIplus end
        ReactorHelper::CreateEditorReactor();
        ReactorHelper::CreateDocManagerReactor();
        // 初始化ParamManager单例对象
        ParamManager::Init();
        // 初始化DBMonitorManager单例对象
        DBMonitorManager::Init();
        acutPrintf( _T( "\nArxData::On_kLoadAppMsg\n" ) );

        return ( retCode ) ;
    }
    virtual AcRx::AppRetCode On_kUnloadAppMsg ( void* pkt )
    {
        AcRx::AppRetCode retCode = AcRxArxApp::On_kUnloadAppMsg ( pkt ) ;
        UIHelper::DestroyPropertyPalette();
        // 销毁ParamManager单例对象
        ParamManager::UnInit();
        // 销毁DBMonitorManager单例对象
        DBMonitorManager::UnInit();
        ReactorHelper::RemoveEditorReactor();
        ReactorHelper::RemoveDocManagerReactor();
		// 关闭GDI+环境
        //close gdiplus environment
        GdiplusShutdown(m_gdiplusToken);

        delete acrxServiceDictionary->remove( ARXDATA_SERVICE_NAME );
        acutPrintf( _T( "\nArxData::On_kUnloadAppMsg\n" ) );
        return ( retCode ) ;
    }
    virtual AcRx::AppRetCode On_kLoadDwgMsg( void* pkt )
    {
        AcRx::AppRetCode retCode = AcRxArxApp::On_kLoadDwgMsg ( pkt ) ;
        // 初始化数据库并加载数据
        if( DwgHelper::Load( curDoc() ) )
		{
			acutPrintf( _T( "\nArxData::On_kLoadDwgMsg\n" ) );
			return retCode;
		}
		else
		{
			// 关闭文档(不提示)
			ArxUtilHelper::CloseDocument( curDoc(), false );
			return AcRx::kRetError;
		}
    }
    virtual AcRx::AppRetCode On_kUnloadDwgMsg( void* pkt )
    {
        AcRx::AppRetCode retCode = AcRxArxApp::On_kUnloadDwgMsg ( pkt ) ;
        // 清理数据
        DwgHelper::UnLoad( curDoc() );
        acutPrintf( _T( "\nArxData::On_kUnloadDwgMsg\n" ) );
        return retCode;
    }
    virtual void RegisterServerComponents ()
    {
    }
    static void JL_UpdateData( void )
    {
        //if( IDOK == AfxMessageBox( _T( "系统监测到数据库被修改,是否更新?" ), MB_OKCANCEL ) )
        {
            // 删除所有的数据
            DataHelper::UnInitAllData();
            // 重新读取数据库并导入字段
            DataHelper::InitAllData();
        }
    }
    static void JL_ImportData( void )
    {
        DataHelper::ImportAllData();
    }
    static void JL_ExportData( void )
    {
        AcApDocument* pDoc = curDoc();
        // 关闭数据库文件监视器
        DBMonitorManager::getSingletonPtr()->closeMonitor( pDoc );
        // 写入到数据库
        DataHelper::ExportAllData();
        // 重启数据库文件监视器
        DBMonitorManager::getSingletonPtr()->startMonitor( pDoc );
    }
    //字段信息管理对话框
    static void JL_ConfigDataField()
    {
        UIHelper::ShowFieldManagerDlg();
    }
    //显示属性数据
    static void JL_DisplayData()
    {
        UIHelper::DisplayData();
    }
    //双击显示属性数据
    static void JL_DisplayDataByDoubleClick()
    {
        UIHelper::DisplayDataByDoubleClick();
    }
    
    static void JL_ShowPropertyPalette( void )
    {
        UIHelper::ShowPropertyPalette();
    }
    static void JL_WriteToJson()
    {
        //DataHelper::WriteFieldToJson();
        DataHelper::InitFields2();
    }
} ;
IMPLEMENT_ARX_ENTRYPOINT( CArxDataApp )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxDataApp, JL, _WriteToJson, WriteToJson, ACRX_CMD_TRANSPARENT, NULL )

ACED_ARXCOMMAND_ENTRY_AUTO( CArxDataApp, JL, _UpdateData, UpdateData, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxDataApp, JL, _ImportData, ImportData, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxDataApp, JL, _ExportData, ExportData, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxDataApp, JL, _ConfigDataField, ConfigDataField, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxDataApp, JL, _DisplayData, DisplayData, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxDataApp, JL, _DisplayDataByDoubleClick, DisplayDataByDoubleClick, ACRX_CMD_TRANSPARENT | ACRX_CMD_USEPICKSET | ACRX_CMD_REDRAW, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxDataApp, JL, _ShowPropertyPalette, ShowPropertyPalette, ACRX_CMD_TRANSPARENT | ACRX_CMD_USEPICKSET | ACRX_CMD_REDRAW, NULL )