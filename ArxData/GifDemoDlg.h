#pragma once
#include "Resource.h"
#include "PictureEx.h"
#include "afxwin.h"
/*
关键词: vc gif 动画
参考代码: http://www.cnblogs.com/sankye/archive/2012/12/30/2839631.html
           http://blog.csdn.net/justin_bkdrong/article/details/5935138
滚动文字：http://www.jizhuomi.com/software/241.html
绘制背景图片：http://jingyan.baidu.com/article/2d5afd69e84cd285a2e28ef9.html
*/
class GifDemoDlg : public CDialog
{
    DECLARE_DYNAMIC( GifDemoDlg )
public:
    GifDemoDlg( CWnd* pParent = NULL ); // 标准构造函数
    virtual ~GifDemoDlg();
// 对话框数据
    enum { IDD = IDD_GIF_DLG };
protected:
    virtual void DoDataExchange( CDataExchange* pDX );  // DDX/DDV 支持
    DECLARE_MESSAGE_MAP()
public:
    CPictureEx m_GifPic;
    virtual BOOL OnInitDialog();
    afx_msg void OnClose();
protected:
    virtual void PostNcDestroy();
public:
    afx_msg void OnPaint();
public:
    int m_nTextX;   // 水平滚动文本的起始点的x坐标
    int m_nTextY;   // 垂直滚动文本的起始点的y坐标
    CFont m_newFont;   // 新字体
    CFont* m_pOldFont; // 选择新字体之前的字体
    HICON m_hIcon;
    CBrush m_brBk;
    afx_msg void OnTimer( UINT_PTR nIDEvent );
    afx_msg HBRUSH OnCtlColor( CDC* pDC, CWnd* pWnd, UINT nCtlColor );
};
