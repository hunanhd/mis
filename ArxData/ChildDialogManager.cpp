#include "StdAfx.h"
#include "ChildDialogManager.h"
ChildDialogManager::ChildDialogManager() : m_di(-1)
{
}
ChildDialogManager::~ChildDialogManager()
{
}
void ChildDialogManager::add(CDialog* dlg, long idd)
{
	if(dlg != 0)
	{
		m_dlgs.push_back(dlg);
		m_ids.push_back(idd);
	}
}
void ChildDialogManager::create(CWnd* pParent)
{
	for(int i=0;i<m_dlgs.size();i++)
	{
		m_dlgs[i]->Create( m_ids[i], pParent );
	}
}
void ChildDialogManager::move(const CRect& rect)
{
	for(int i=0;i<m_dlgs.size();i++)
	{
		if ( ::IsWindow ( m_dlgs[i]->GetSafeHwnd () ) )
		{
			m_dlgs[i]->MoveWindow( 0, 0, rect.Width (), rect.Height (), TRUE );
		}
	}
}
void ChildDialogManager::sizeChange(CWnd* pParent, CRect* lpRect)
{
	for(int i=0;i<m_dlgs.size();i++)
	{
		if ( ::IsWindow ( m_dlgs[i]->GetSafeHwnd () ) )
		{
			m_dlgs[i]->SetWindowPos ( pParent, lpRect->left , lpRect->top , lpRect->Width (), lpRect->Height (), SWP_NOZORDER ) ;
		}
	}
}
void ChildDialogManager::onSize(int cx, int cy)
{
	for(int i=0;i<m_dlgs.size();i++)
	{
		if ( ::IsWindow ( m_dlgs[i]->GetSafeHwnd () ) )
		{
			m_dlgs[i]->MoveWindow ( 0, 0 , cx, cy ) ;
		}
	}
}
void ChildDialogManager::activate(int di)
{
	for( int i = 0; i < m_dlgs.size(); i++ )
	{
		int nCmdShow = ( ( di == i ) ? SW_SHOW : SW_HIDE );
		m_dlgs[i]->ShowWindow( nCmdShow );
	}
	m_di = di;
}
CDialog* ChildDialogManager::get(int di) const
{
	if( di < 0 || di >= m_dlgs.size() )
	{
		return 0;
	}
	else
	{
		return m_dlgs[di];
	}
}
CDialog* ChildDialogManager::getCurrent() const
{
	return this->get( m_di );
}
