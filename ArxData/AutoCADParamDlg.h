#pragma once

#include "Resource.h"

class AutoCADParamDlg : public CDialog
{
	DECLARE_DYNAMIC(AutoCADParamDlg)

public:
	AutoCADParamDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~AutoCADParamDlg();

// 对话框数据
	enum { IDD = IDD_CAD_SET_DLG };

public:
	virtual BOOL OnInitDialog();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();

private:
	BOOL m_single_select;
};
