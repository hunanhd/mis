#pragma once
#include "Tool/Singleton.h"
#include "DBMonitor.h"
#include <map>
class DBMonitorManager : public Singleton<DBMonitorManager>
{
public:
    static void Init();
    static void UnInit();
public:
    DBMonitorManager();
    ~DBMonitorManager();
    bool startMonitor( AcApDocument* pDoc );
    bool closeMonitor( AcApDocument* pDoc );
private:
    typedef std::map<AcApDocument*, DBMonitor*> DocDBMonitorMap;
    DocDBMonitorMap m_monitorMap;
    bool m_use_db_monitor; // 是否监视数据库db文件(默认true)
};