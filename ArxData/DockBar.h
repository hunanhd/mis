#pragma once

#include "acui.h"
#include "ChildDialogManager.h"

class DockBar : public CAcUiDockControlBar
{
	DECLARE_DYNAMIC (DockBar)

	// 重载CAcUiDockControlBar基类虚函数
public:
	virtual ~DockBar ();
	virtual bool OnClosing (); // 工具栏关闭的时候做一些操作,默认情况下调用虚函数OnSave()
	virtual void Save();     // 保存数据

	// 窗口消息映射
public:
	afx_msg int OnCreate (LPCREATESTRUCT lpCreateStruct) ;
	afx_msg void OnSysCommand (UINT nID, LPARAM lParam) ;
	afx_msg void OnSize (UINT nType, int cx, int cy) ;

	DECLARE_MESSAGE_MAP()

	// 重载窗口基类虚函数
public:
	virtual BOOL Create (CWnd *pParent, LPCTSTR lpszTitle) ;
	virtual void SizeChanged (CRect *lpRect, BOOL bFloating, int flags) ;

protected:
	DockBar();
	// 取消CAdUiDockControlBar的2个右键菜单
	virtual BOOL AddCustomMenuItems(LPARAM hMenu);
	// 由派生类提供CLSID
	virtual CLSID* GetCLSID() const = 0;

protected:
	// 子对话框管理器
	ChildDialogManager cdm;
} ;
