#include "stdafx.h"
#include "FieldManagerDlg.h"
#include "InputDlg.h"
#include "SQLiteHelper.h"
#include "ParamManager.h"
#include "GifDemoDlg.h"
#include "ThreadParam.h"
#include "config.h"
#include <algorithm>
#include <cassert>
IMPLEMENT_DYNAMIC( FieldManagerDlg, CDialog )
FieldManagerDlg::FieldManagerDlg( CWnd* pParent /*=NULL*/ )
    : CDialog( FieldManagerDlg::IDD, pParent )
{
}
FieldManagerDlg::~FieldManagerDlg()
{
}
void FieldManagerDlg::DoDataExchange( CDataExchange* pDX )
{
    CDialog::DoDataExchange( pDX );
    DDX_Control( pDX, IDC_TYPE_COMBO2, m_typeCombo );
    DDX_Control( pDX, IDC_FIELD_LIST, m_fieldListBox );
}
BEGIN_MESSAGE_MAP( FieldManagerDlg, CDialog )
    ON_BN_CLICKED( IDC_ADD_BTN, &FieldManagerDlg::OnBnClickedAddBtn )
    ON_BN_CLICKED( IDC_DEL_BTN, &FieldManagerDlg::OnBnClickedDelBtn )
    ON_CBN_SELCHANGE( IDC_TYPE_COMBO2, &FieldManagerDlg::OnCbnSelchangeTypeCombo2 )
    ON_BN_CLICKED( IDC_APPLY_BTN, &FieldManagerDlg::OnBnClickedApplyBtn )
    ON_LBN_SELCHANGE( IDC_FIELD_LIST, &FieldManagerDlg::OnLbnSelchangeFieldList )
    ON_WM_CLOSE()
    ON_WM_PAINT()
    ON_MESSAGE( WM_USER_SAVE_FIELD, OnFieldSave )
END_MESSAGE_MAP()
BOOL FieldManagerDlg::OnInitDialog()
{
    CDialog::OnInitDialog();
    // 设置标题
    CString title;
    title.Format( _T( "字段管理【数据库: %s】" ), DB_FILE ); //在标题栏显示db文件的路径
    this->SetWindowText( title );
    // 显示字段信息子对话框
    CRect rect;
    GetDlgItem( IDC_FIELD_INFO_POS )->GetWindowRect( &rect );
    ScreenToClient( &rect );
    fidlg.Create( IDD_FIELD_INFO_DLG, this );
    fidlg.MoveWindow( rect.left, rect.top, rect.Width(), rect.Height() );
    fidlg.ShowWindow( SW_SHOW );
    fillContent(); // 填充数据
    return TRUE;
}
void FieldManagerDlg::OnClose()
{
    clearFieldInfoArray();
    CDialog::OnClose();
}
void FieldManagerDlg::OnBnClickedAddBtn()
{
    InputDlg dlg;
    if( dlg.DoModal() != IDOK ) return;
    // 注意m_lastSel的更新问题
    if( m_fieldListBox.GetCount() == 0 ) setLastSelIndex( 0 );
    // 对话框得到的字段名
    // 接下来进行验证和分析
    CString field = dlg.m_str;
    field.Trim();
    // 字段名是或否有效
    if( !isValidField( field ) )
    {
        CString msg;
        msg.Format( _T( "非法的字段\n[%s]" ), field );
        MessageBox( msg );
        return;
    }
    // 字段名特殊处理
    if( field.Find( SEPARATOR ) < 0 )
    {
        field = FieldHelper::MakeHybirdField( DEFAULT_GROUP, field );
    }
    // 字段名是否重复
    if( isFieldExistInListBox( field ) )
    {
        CString msg;
        msg.Format( _T( "字段[%s]已存在!" ), field );
        MessageBox( msg );
        return;
    }
    // 添加字段到listbox中
    int index = m_fieldListBox.AddString( field );
    if( index != LB_ERR )
    {
        m_fieldListBox.SetCurSel( index );
        // ***创建新的字段信息，并记录***
        m_infoes.push_back( new FieldInfo() );
        // 切换selection, 保存上次selection所在位置的字段信息
        updateFieldInfo();

		fim[field] = 1; // 增加新字段
    }
}
void FieldManagerDlg::OnBnClickedDelBtn()
{
    if( m_fieldListBox.GetCount() == 0 ) return;
    if( IDCANCEL == MessageBox(
                _T( "删除字段的同时也会删除所有相应类型图元的字段和数据\n\n确定要删除字段吗？【请慎重操作!!!】" ),
                _T( "提示框" ),
                MB_ICONEXCLAMATION | MB_ICONWARNING | MB_OKCANCEL ) ) return;
    int index = m_fieldListBox.GetCurSel();
    if( index == LB_ERR ) return;

	CString field = getSelField();
	if(fim.find(field) != fim.end() && fim[field] == 1)
	{
		fim.erase(field);
	}
	else
	{
		fim[field] = 0; // 删除字段
	}
    m_fieldListBox.DeleteString( index );
    // ****同时删除对应的字段信息****
    delete m_infoes[index];
    m_infoes.erase( m_infoes.begin() + index );
    // 注意m_lastSel的更新问题
    if( m_fieldListBox.GetCount() == 0 )
    {
        setLastSelIndex( LB_ERR );
    }
    if( index == 0 )
    {
        m_fieldListBox.SetCurSel( 0 );
    }
    else
    {
        m_fieldListBox.SetCurSel( index - 1 );
    }
    showFieldInfo();
}
void FieldManagerDlg::OnCbnSelchangeTypeCombo2()
{
    // 不保存之前所做的修改
    // 清空字段列表
    clearFieldListBox();
    // 填充字段
    fillFieldListBox( getSelType() );
    // 更新字段信息对话框的显示
    showFieldInfo();
    // Invalidate()将整个对话框都失效
    // 而InvalidateRect仅将IDC_DRAW_POS所在矩形块失效
    // 从而提高了效率
    //CRect rect;
    //GetDlgItem( IDC_DRAW_POS )->GetWindowRect( &rect );
    //ScreenToClient( &rect );
    //InvalidateRect( &rect );
    // 更新绘图区的显示
    // 不调用UpdateWindow()也能够触发WM_PAINT消息，从而调用OnPaint()方法
    //UpdateWindow();
}
void FieldManagerDlg::OnLbnSelchangeFieldList()
{
    // 切换之前更新上次选择的字段信息
    // 并检查字段信息的有效性
    updateFieldInfo();
}
/*
 * 方法：
 *	  1) 得到原始的字段链表L1和字段列表框中的字段链表L2
 *    2) 遍历链表L2,判断字段是否存在于L1中
 *       a) 如果存在，则该字段保持不变，并从L1中移除该字段；
 *       b) 如果不存在，则转第(3)步
 *    3) 该字段是新添加的字段，则执行"增加字段"操作Add
 *    4) 完成遍历L2，最后剩下的L1的元素就是需要删除的字段
 *    5) 执行"删除字段"操作Remove
 */
static UINT ThreadFunc( LPVOID pParam )
{
    ThreadParam* param = ( ThreadParam* )pParam;
    Sleep( 1000 );
    PostMessage( param->hFieldDlg, WM_USER_SAVE_FIELD, 0, 0 );
    SendMessage( param->hGifDlg, WM_CLOSE, 0, 0 );
    return  0;
}
void FieldManagerDlg::OnBnClickedApplyBtn()
{
    //saveModifInfo();
    //ShowWindow(SW_HIDE);
    // 模态对话框
    //GifDemoDlg dlg;
    //dlg.DoModal();
    //ShowWindow(SW_SHOW);
    // 非模态对话框(仅做测试!)
    GifDemoDlg* dlg = new GifDemoDlg();
    dlg->Create( IDD_GIF_DLG );
    CRect rect;
    this->GetWindowRect( &rect );
    dlg->MoveWindow( &rect );
    dlg->ShowWindow( SW_SHOW );
    // 启动线程
    // 注:线程参数必须是全局变量,否则会出现很多诡异的错误!!!
    global_param.hFieldDlg = this->GetSafeHwnd();
    global_param.hGifDlg = dlg->GetSafeHwnd();
    CWinThread* pThread = AfxBeginThread( ThreadFunc, &global_param );
}
bool FieldManagerDlg::updateFieldInfo()
{
    fidlg.UpdateData( TRUE );
    bool ret = fidlg.validateFieldInfo();
    if( ret )
    {
        if( getLastSelIndex() != LB_ERR )
        {
			CString json_str1 = FieldInfo::ToJson(*m_infoes[getLastSelIndex()]);
            fidlg.writeToFieldInfo( *m_infoes[getLastSelIndex()] );
			CString json_str2 = FieldInfo::ToJson(*m_infoes[getLastSelIndex()]);
			if(json_str2.CompareNoCase(json_str1) != 0)
			{
				CString field;
				m_fieldListBox.GetText( getLastSelIndex(), field );
				if(fim.find(field) == fim.end() || fim[field] != 1)
				{
					fim[field] = 2; // 修改字段
				}
			}
        }
        showFieldInfo();
    }
    else
    {
        m_fieldListBox.SetCurSel( getLastSelIndex() );
    }
    return ret;
}
CString FieldManagerDlg::getSelType()
{
    CString selDXFName;
    int sel = m_typeCombo.GetCurSel();
    if( sel != CB_ERR )
    {
        m_typeCombo.GetLBText( sel, selDXFName );
    }
    return getTypeByDXFName( selDXFName );
}
CString FieldManagerDlg::getSelField()
{
    CString selField;
    int sel = m_fieldListBox.GetCurSel();
    if( sel != LB_ERR )
    {
        m_fieldListBox.GetText( sel, selField );
    }
    return selField;
}
void FieldManagerDlg::clearFieldInfoArray()
{
    // 清空字段信息(注意释放内存)
    for( int i = 0; i < m_infoes.size(); i++ )
    {
        delete m_infoes[i];
    }
    m_infoes.clear();
}
void FieldManagerDlg::clearFieldListBox()
{
    m_fieldListBox.ResetContent();
    // 清空字段信息
    clearFieldInfoArray();
	// 清空fim
	fim.clear();
}
void FieldManagerDlg::fillFieldListBox( const CString& type )
{
    setLastSelIndex( LB_ERR ); // 记录listbox切换之前的索引位置
    MFCStringArray fields;
    SQLiteHelper::GetAllFields( type, fields );
    if( fields.empty() ) return;
    for( int i = 0; i < fields.size(); i++ )
    {
        CString field = fields[i];
        m_fieldListBox.AddString( field );
        // 初始为默认设置
        FieldInfo* pInfo = new FieldInfo();
        // 读取字段信息
        SQLiteHelper::GetFieldInfo( type, field, *pInfo );
        m_infoes.push_back( pInfo );
    }
    m_fieldListBox.SetCurSel( 0 );
}
bool FieldManagerDlg::isFieldExistInListBox( const CString& field )
{
    return ( m_fieldListBox.FindString( 0, field ) != LB_ERR );
}
bool FieldManagerDlg::isValidField( const CString& field )
{
    CString tempStr( field );
    tempStr.Trim();
    // 排除空字符串
    if( tempStr.GetLength() == 0 ) return false;
    // 排除非法字符，例如
    // {. * & @ # $ % ...}
    //if( tempStr.Find( _T( '.' ) ) >= 0 ) return false;
    return true;
}
void FieldManagerDlg::showFieldInfo()
{
    int sel = m_fieldListBox.GetCurSel();
    if( sel != LB_ERR )
    {
        FieldInfo* pInfo = m_infoes[sel];
        fidlg.readFromFieldInfo( *pInfo );
        fidlg.UpdateData( FALSE );
        setLastSelIndex( sel ); // 记录"上一次"的索引位置
    }
}
void FieldManagerDlg::fillContent()
{
    // 填充所有的图元及数据类型
    m_typeCombo.ResetContent();
    m_fieldListBox.ResetContent();
    MFCStringArray types;
    SQLiteHelper::GetTypes( types );
    if( types.empty() ) return;
    //acutPrintf(_T("\n总个数:%d"), len);
    for( int i = 0; i < types.size(); i++ )
    {
        //m_typeCombo.AddString( types[i] );
        CString type = types[i];
        CString name = getDXFNameByType( type );
        if( !name.IsEmpty() )
        {
            m_typeCombo.AddString( name );
        }
        else
        {
            m_typeCombo.AddString( type );
        }
    }
    // 定位第1个图元
    m_typeCombo.SetCurSel( 0 );
    // 填充第1个类型所包含的所有字段
    fillFieldListBox( types[0] );
    // 显示第1个字段信息
    showFieldInfo();
}
void FieldManagerDlg::OnPaint()
{
    CPaintDC dc( this ); // device context for painting
    //CRect rect;
    //GetDlgItem( IDC_DRAW_POS )->GetWindowRect( &rect );
    //ScreenToClient( &rect );
    //CString type = getSelType();
    //if( type.GetLength() != 0 )
    //{
    //    CString name( type );
    //    //TypeNameMapHelper::GetTypeNameMap(type, name);
    //    ArxClassHelper::GetDxfName( type, name );
    //    CString text;
    //    text.Format( _T( "DXF名称: %s" ), name );
    //    dc.DrawText( text, rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE );
    //}
    // 后续可以参考CustomProperties.cpp中的
    // void DateTimeProperty::OnDrawDescription( CDC* pDC, CRect rect ) 代码实现
    // 要求：字体清晰，醒目(对比效果好)
}
void FieldManagerDlg::setLastSelIndex( int sel )
{
    m_lastSel = sel;
}
int FieldManagerDlg::getLastSelIndex() const
{
    return m_lastSel;
}
void FieldManagerDlg::saveModifInfo()
{
    if( m_fieldListBox.GetCount() > 0 )
    {
        // 切换之前更新上次选择的字段信息
        // 并检查字段信息的有效性
        if( !updateFieldInfo() ) return;
    }
    // 选择的图元类型
    CString selType = getSelType();
    if( selType.GetLength() == 0 ) return;
    // 数据库中删除所有type类型的字段
    //SQLiteHelper::RemoveType( selType );
    assert( m_fieldListBox.GetCount() == m_infoes.size() );
    // "剩余"字段 与 m_infoes应该是一一对应的
    MFCStringArray leftFields;
    for( int i = 0; i < m_fieldListBox.GetCount(); i++ )
    {
        CString text;
        m_fieldListBox.GetText( i, text );
        leftFields.push_back( text );
    }
	// (1)数据库操作-删除字段
	for( int i = 0; i < leftFields.size(); i++ )
	{
		CString field = leftFields[i];
		if(fim.find(field) != fim.end() && fim[field] == 0)
		{
			fim.erase(field);
		}
	}
	MFCStringArray remove_fields;
	for(FieldInfoMap::iterator itr=fim.begin();itr!=fim.end();++itr)
	{
		if(itr->second == 0)
		{
			remove_fields.push_back(itr->first);
		}
	}
	SQLiteHelper::RemoveMoreFields(selType, remove_fields);

	// (2) 数据库操作-增加/修改字段
	for( int i = 0; i < leftFields.size(); i++ )
	{
		CString field = leftFields[i];
		if(fim.find(field) != fim.end())
		{
			if(fim[field] == 1)
			{
				// (2) 数据库操作-增加字段
				SQLiteHelper::AddField( selType, field, *m_infoes[i] );
			}
			else
			{
				// (3) 数据库操作-修改字段
				SQLiteHelper::UpdateField( selType, field, *m_infoes[i] );
			}
		}
	}

	fim.clear();

    //for( int i = 0; i < leftFields.size(); i++ )
    //{
    //    CString hybirdField = leftFields[i];
    //    SQLiteHelper::AddField( selType, hybirdField, *m_infoes[i] );
    //}
}
CString FieldManagerDlg::getTypeByDXFName( const CString& dxfName )
{
    CString type;
    SQLiteHelper::GetTypeByDXFName( dxfName, type );
    return type;
    //MFCStringArray types;
    //SQLiteHelper::GetTypes( types );
    //if( types.empty() ) return _T("");
    //for( int i = 0; i < types.size(); i++ )
    //{
    //	CString type = types[i];
    //	CString name( type );
    //	//ArxClassHelper::GetDxfName( type, name );
    //	//acutPrintf(_T("\ntype:%s"),type);
    //	//if (name.IsEmpty()) return dxfName;
    //	if (name == dxfName) return type;
    //	emptyDfx = dxfName;
    //}
    //return emptyDfx;
}

CString FieldManagerDlg::getDXFNameByType( const CString& type )
{
    CString dxfName;
    SQLiteHelper::GetDXFNameByType( type, dxfName );
    return dxfName;
}
// 自定义消息WM_USER_SAVE_FIELD 消息处理函数
LRESULT FieldManagerDlg::OnFieldSave( WPARAM wParam, LPARAM lParam )
{
    saveModifInfo();
    MessageBox( _T( "字段信息更新成功!" ) );
    // 最小化
    //ShowWindow( SW_MINIMIZE );
    return 0;
}
