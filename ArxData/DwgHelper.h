#pragma once
class DwgHelper
{
public:
    // 新建或打开dwg文件时数据初始化
    static bool Load( AcApDocument* pDoc );
    // 关闭dwg文件时的数据清理工作
    static bool UnLoad( AcApDocument* pDoc );
};