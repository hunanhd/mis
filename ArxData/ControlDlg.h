#pragma once
#include "Resource.h"
#include "AcadDialog.h"

/*
MFC 不规则窗口(使用GDI+和PNG图片)
http://blog.csdn.net/lincyang/article/details/39078295
http://blog.csdn.net/elaine_bao/article/details/51586688
*/
class ControlDlg : public AcadDialog
{
	DECLARE_DYNAMIC(ControlDlg)
public:
	ControlDlg(CWnd* pParent = NULL, BOOL bModal = FALSE);   // 标准构造函数
	virtual ~ControlDlg();
// 对话框数据
	enum { IDD = IDD_CONTROL_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

public:
	//对话框初始化
	virtual BOOL OnInitDialog();
	//WM_CREATE消息响应(非模态对话框)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//WM_PAINT消息响应
	afx_msg void OnPaint();
	//实现鼠标点击客户区移动窗口的功能
	afx_msg LRESULT OnNcHitTest(CPoint pt);
	//WM_DESTROY消息响应(非模态对话框销毁窗口)
	afx_msg void OnDestroy();
	//用计时器实现动画效果
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	DECLARE_MESSAGE_MAP()

	// 实现不规则窗口
private:
	BLENDFUNCTION m_Blend;//important struct
	HDC m_hdcMemory;
	HINSTANCE hFuncInst ;
	typedef BOOL (WINAPI *MYFUNC)(HWND,HDC,POINT*,SIZE*,HDC,POINT*,COLORREF,BLENDFUNCTION*,DWORD);
	MYFUNC UpdateLayeredWindow;
	Image *m_pPrime;
	BOOL ImageFromIDResource(UINT nID,LPCTSTR sTR,Image * &pImg);
	// 利用UpdateLayeredWindow实现不规则窗口
	BOOL UpdateDisplay(Image *image, int Transparent=255);//param is transparent, you known it.
};
