#include "StdAfx.h"
#include "DBMonitor.h"
#include "DataHelper.h"
#include "DataBaseName.h"
#include "ParamManager.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
// 使用外部的send.exe程序: 创建进程，传递命令行参数等
static void SendCmd1()
{
    CString exePath = ARX_FILE_PATH( _T( "send.exe" ) );
    bool switch_to_cad = true;
    CString cmdLine;
    cmdLine.Format( _T( "%d %s" ), ( int )switch_to_cad, _T( "JL.UpdateData" ) );
    CString cwdPath = ARX_FILE_PATH( _T( "" ) );
    HANDLE hProcess, hThread;
    ThreadHelper::RunProecess( exePath, cmdLine, cwdPath, hProcess, hThread, false );
}
// 使用sendmessage: 向CAD窗口发送命令消息
// 使用方便一些
static void SendCmd2()
{
    HWND cadMainWnd = acedGetAcadFrame()->GetSafeHwnd();
    CString cmd = _T( "JL.UpdateData" );
    //打印窗口的标题
    //TCHAR title[80];
    //GetWindowText(cadMainWnd, title, 80);
    //CString msg;
    //msg.Format(_T("句柄:%ld  窗口标题:%s  命令:%s"), cadMainWnd, title, cmd);
    //AfxMessageBox(msg);
    CADHelper::SendCommandToAutoCAD( cadMainWnd, cmd, true );
}
// 使用AcApDocManager::sendStringToExecute(): 向CAD窗口发送命令消息
// 注: 在monitor中使用该函数会崩溃!!!(放在这里仅作学习和备份用!)
static void SendCmd3( AcApDocument* pDoc )
{
    CString cmd = _T( "\003\003JL.UpdateData " ); // \003表示esc
    acDocManager->sendStringToExecute( pDoc, cmd, true, false, false );
}
/*
http://blog.csdn.net/lvwx369/article/details/42102123
http://bbs.csdn.net/topics/390947473?page=1
http://blog.csdn.net/tangyin025/article/details/44959117
使用ReadDirectoryChangesW会遇到文件被修改多次触发modify通知
这里使用一个简单的技巧,每2次通知视为1次(根据观察发现,多数都是2次通知,较少有3次以上的)
*/
// 文件变化响应动作(类似于消息映射)
class DBFileWachterAction : public FileWatcherAction
{
public:
    DBFileWachterAction( AcApDocument* pDoc, const CString& dbFile )
        : m_pDoc( pDoc ), m_dbFile( dbFile ), m_edit_count( 0 )
    {
    }
    // 文件被修改时的动作
    virtual void onModify( const CString& filename )
    {
        if( m_dbFile.CompareNoCase( filename ) == 0 )
        {
            //CString msg;
            //msg.Format(_T("%s被修改"), filename);
            //AfxMessageBox(msg);
            m_edit_count++;
            if( m_edit_count == 1 )
            {
                //SendCmd1();
                SendCmd2();
            }
            else if( m_edit_count == 2 )
            {
                m_edit_count = 0;
            }
        }
    }
    // 要监视的数据库文件
    CString m_dbFile;
    AcApDocument* m_pDoc;
    int m_edit_count;
};
DBMonitor::DBMonitor( AcApDocument* pDoc, const CString& dbFile )
    : m_pDoc( pDoc ), m_dbFile( dbFile )
{
    if( m_pDoc == 0 || m_dbFile == _T( "" ) ) return;
    // 要监视的目录以及文件名
    CString dir = PathHelper::GetDirectory( dbFile );
    CString file = PathHelper::GetFileName( dbFile );
    // 当文件的修改时间发生变化时候发出通知
    DWORD dwNotifyFilter = FileSystemWatcher::FILTER_LAST_WRITE_NAME;
    //DWORD dwNotifyFilter = FileSystemWatcher::FILTER_SIZE_NAME;
    // 注册响应动作(模仿windows的消息映射)
    fsw.RegAction( new DBFileWachterAction( pDoc, file ) );
    // 开始监视(启动线程后台监视)
    bool r = fsw.Run( dir, false, dwNotifyFilter, 0 );
}
DBMonitor::~DBMonitor()
{
    if( m_pDoc == 0 || m_dbFile == _T( "" ) ) return;
    fsw.Close( 1000 ); // 等待1秒后结束线程
}
AcApDocument* DBMonitor::getDoc() const
{
    return m_pDoc;
}
CString DBMonitor::getDbFile() const
{
    return m_dbFile;
}
