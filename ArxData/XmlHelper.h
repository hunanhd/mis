#pragma once

#include "AcImportXml.h"
using namespace MSXML;

//Utility functions to add and get the child node from the parent XML-DOM node
extern BOOL GetChildNode(MSXML::IXMLDOMNode* pNode, LPCTSTR pszNodeName, MSXML::IXMLDOMNode** ppChild);
extern BOOL AddChildNode(MSXML::IXMLDOMNode* pNode, LPCTSTR pszChildNodeName, short nNodeType, MSXML::IXMLDOMNode** ppChild);
