#pragma once
/*
 * 可视化界面辅助类
 * 放在一个类中便于管理
 * 也可使得代码看起来简洁一些
 */
class UIHelper
{
public:
    static void ShowFieldManagerDlg();   // 显示字段管理对话框
    static void DisplayData();            // 显示属性对话框
    static void DisplayDataByDoubleClick(); // 双击显示属性对话框(pickset)
	static void ShowPropertyPalette();
	static void DestroyPropertyPalette();
	static void UpdatePropertyPalette( const AcDbObjectId& objId);
};
