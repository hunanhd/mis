#include "StdAfx.h"
#include "DwgHelper.h"
#include "ParamManager.h"
#include "DBMonitorManager.h"
#include "DataHelper.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
bool DwgHelper::Load( AcApDocument* pDoc )
{
    if( pDoc == 0 ) return false;
    // 删除词典及相关数据
    DataHelper::UnInitAllData();
    // 初始化数据库文件
    if( !ParamManager::getSingletonPtr()->initDbFile( pDoc ) )
    {
        AfxMessageBox( _T( "数据库文件初始化失败!程序退出" ) );
        return false;
    }
    // 导入数据字段
    DataHelper::InitAllData();
    // 导入图块定义
    DataHelper::UpdateDwgBlock();
    if( !DBMonitorManager::getSingletonPtr()->startMonitor( pDoc ) )
    {
        AfxMessageBox( _T( "启动数据库文件线监视器失败!" ) );
        return false;
    }
    return true;
}
bool DwgHelper::UnLoad( AcApDocument* pDoc )
{
    if( pDoc == 0 ) return false;
    // 关闭数据库监视器
    DBMonitorManager::getSingletonPtr()->closeMonitor( pDoc );
    // 删除词典及相关数据
    DataHelper::UnInitAllData();
    // 移除数据库文件记录
    ParamManager::getSingletonPtr()->removeDbFile( pDoc );
    return true;
}
