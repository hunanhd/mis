#include "stdafx.h"
#include "AutoCADParamDlg.h"
#include "MineGE/HelperClass.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"

IMPLEMENT_DYNAMIC(AutoCADParamDlg, CDialog)

AutoCADParamDlg::AutoCADParamDlg(CWnd* pParent /*=NULL*/)
	: CDialog(AutoCADParamDlg::IDD, pParent)
{

}

AutoCADParamDlg::~AutoCADParamDlg()
{
}

void AutoCADParamDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Check( pDX, IDC_SINGLE_SELECT, m_single_select );
}

BEGIN_MESSAGE_MAP(AutoCADParamDlg, CDialog)
	ON_BN_CLICKED(IDOK, &AutoCADParamDlg::OnBnClickedOk)
END_MESSAGE_MAP()

void AutoCADParamDlg::OnBnClickedOk()
{
	UpdateData(TRUE);

	// 非模态对话框下要锁定中文档
	//ArxDocLockSwitch lock_switch;
	// 修改系统变量
	ArxSysVarHelper::Set(_T("PICKADD"), (m_single_select==0)?1:0);

	OnOK();
}

BOOL AutoCADParamDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 获取系统变量PICKADD的值
	ArxSysVarHelper::Get(_T("PICKADD"), m_single_select);
	m_single_select = (m_single_select==0)?1:0;
	// 更新到界面
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}
