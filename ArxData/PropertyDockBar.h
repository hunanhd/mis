#pragma once

#include "DockBar.h"
#include "PropertyDataChildDlg.h"
#include "PropertyHelpChildDlg.h"

class PropertyDockBar : public DockBar
{
	DECLARE_DYNAMIC (PropertyDockBar)

public:
	// 构造函数中调用ChildDialogManager::add()方法将子对话框添加到管理器中
	PropertyDockBar ();
	virtual ~PropertyDockBar ();

	void ShowChildDlg(const AcDbObjectId& objId);

private:
	virtual CLSID* GetCLSID() const;

private:
	// 子对话框
	PropertyDataChildDlg m_propDataDlg ;
	PropertyHelpChildDlg m_propHelpDlg;

	DECLARE_MESSAGE_MAP()
} ;
