#include "StdAfx.h"
#include "JsonFieldDict.h"
#include "HelperClass.h"
#include "FieldInfoHelper.h"
static void BuildDefaultFieldInfoObject( Json::Value& info )
{
    info["data_type"] = Json::Value( ( int )FieldInfo::DT_STRING );
    info["int_min"] = Json::Value( 0 );
    info["int_max"] = Json::Value( 100 );
    info["double_min"] = Json::Value( 0.0 );
    info["double_max"] = Json::Value( 99.9 );
    info["list_type"] = Json::Value( ( int )FieldInfo::LT_STRING );
    info["var_name"] = Json::Value( "" );
    info["enable"] = Json::Value( true );
    info["descr"] = Json::Value( "" );
    info["tolerance"] = Json::Value( ( int )2 );
    info["showInTooltip"] = Json::Value( true );
}
Adesk::UInt32 JsonFieldDict::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    JsonFieldDict, AcDbObject,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation, JSONFIELDDICT,
    ARXHELPERAPP )
JsonFieldDict::JsonFieldDict () : AcDbObject ()
{
}
JsonFieldDict::~JsonFieldDict ()
{
}
Acad::ErrorStatus JsonFieldDict::dwgOutFields ( AcDbDwgFiler* pFiler ) const
{
    assertReadEnabled () ;
    //----- Save parent class information first.
    Acad::ErrorStatus es = AcDbObject::dwgOutFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    //----- Object version number needs to be saved first
    if ( ( es = pFiler->writeUInt32 ( JsonFieldDict::kCurrentVersionNumber ) ) != Acad::eOk )
        return ( es ) ;
    // 输出无格式json字符串
    Json::FastWriter writer;
    std::string json_str = writer.write( root );
    // 转换为Unicode字符串并写入到dwg文件
    pFiler->writeString( C2W( json_str.c_str() ) );
    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus JsonFieldDict::dwgInFields ( AcDbDwgFiler* pFiler )
{
    assertWriteEnabled () ;
    //----- Read parent class information first.
    Acad::ErrorStatus es = AcDbObject::dwgInFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    //----- Object version number needs to be read first
    Adesk::UInt32 version = 0 ;
    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
        return ( es ) ;
    if ( version > JsonFieldDict::kCurrentVersionNumber )
        return ( Acad::eMakeMeProxy ) ;
    AcString json_str;
    pFiler->readString( json_str );
    // 清空
    root.clear();
    // 从字符串中解析json数据
    Json::Reader reader;
    reader.parse( W2C( json_str.kACharPtr() ), root );
    return ( pFiler->filerStatus () ) ;
}
void JsonFieldDict::print()
{
    assertReadEnabled();
    Json::StyledWriter sw;
    acutPrintf( _T( "\n%s" ), C2W( sw.write( root ).c_str() ) );
}
bool JsonFieldDict::addField( const CString& type, const CString& field, const CString& func )
{
    assertWriteEnabled();
    // 转换ansi格式字符串
    std::string sType = W2C( ( LPCTSTR )type );
    std::string sFunc = W2C( ( LPCTSTR )func );
    std::string sField = W2C( ( LPCTSTR )field );
    // 查找图元类型节点
    if( !root.isMember( sType ) )
    {
        root[sType] = Json::Value( Json::objectValue );;
    }
    // 查找图元类型节点下的分组节点
    if( !root[sType].isMember( sFunc ) )
    {
        root[sType][sFunc] = Json::Value( Json::objectValue );
    }
    // 查找分组节点下的字段节点
    if( !root[sType][sFunc].isMember( sField ) )
    {
        Json::Value obj_value( Json::objectValue );
        // 构造基本的字段属性(字符串类型)
        BuildDefaultFieldInfoObject( obj_value );
        root[sType][sFunc][sField] = obj_value;
    }
    return true;
}
bool JsonFieldDict::removeField( const CString& type, const CString& field, const CString& func )
{
    assertWriteEnabled();
    // 转换ansi格式字符串
    std::string sType = W2C( ( LPCTSTR )type );
    std::string sFunc = W2C( ( LPCTSTR )func );
    std::string sField = W2C( ( LPCTSTR )field );
    // 查找图元类型节点
    if( !root.isMember( sType ) ) return false;
    // 查找图元类型节点下的分组节点
    if( !root[sType].isMember( sFunc ) ) return false;
    //  删除分组下的字段
    root[sType][sFunc].removeMember( sField );
    return true;
}
void JsonFieldDict::removeAllFields( const CString& type )
{
    assertWriteEnabled();
    // 转换ansi格式字符串
    std::string sType = W2C( ( LPCTSTR )type );
    // 删除图元类型
    root.removeMember( sType );
}
