#include "StdAfx.h"
#include "Dao.h"
#include "config.h"
#include "DataObserverManager.h"
#include "BuiltinDataManager.h"
#include "AcDbObjectData.h"
#include "HelperClass.h"
#define NULL_JSON_STRING _T("{}")
#define XDATA_PROPERTY_APPNAME _T("扩展属性数据")
DictDao::DictDao( const AcDbObjectId& dictId ) : m_pDict( 0 ), m_createNewKey( true ), m_dictId( dictId )
{
	if( dictId.isNull() ) return;
	AcTransaction* pTrans = actrTransactionManager->startTransaction();
	if( pTrans == 0 ) return;
	//AcDb::OpenMode om = ( save ? ( AcDb::kForWrite ) : ( AcDb::kForRead ) );
	AcDbObject* pObj;
	if( Acad::eOk != pTrans->getObject( pObj, dictId, AcDb::kForWrite ) )
	{
		actrTransactionManager->abortTransaction();
		m_pDict = 0;
		return;
	}
	m_pDict = AcDbDictionary::cast( pObj );
	if( m_pDict == 0 )
	{
		actrTransactionManager->abortTransaction();
		return;
	}
}
DictDao::~DictDao()
{
	if( m_pDict != 0 )
	{
		actrTransactionManager->endTransaction();
		// 触发信号槽,通知词典数据已被修改
		if( BaseDao::isModified() )
		{
			DataObserverManager::getSingletonPtr()->signalDictModify( m_dictId );
		}
	}
}
void DictDao::clear()
{
	//清空
	//pDict->erase();
	AcStringArray names;
	AcDbDictionaryIterator* itr = m_pDict->newIterator();
	while( !itr->done() )
	{
		names.append( itr->name() );
		itr->next();
	}
	delete itr;
	for( int i = 0; i < names.length(); i++ )
	{
		m_pDict->remove( names[i].kACharPtr() );
	}
}
bool DictDao::set( const CString& key, const CString& value )
{
	if( m_pDict == 0 ) return false;
	if( key.IsEmpty() ) return false;
	AcDbXrecord* pXrec = 0;
	// key不存在或者其它原因
	Acad::ErrorStatus es = m_pDict->getAt( key, ( AcDbObject*& ) pXrec, AcDb::kForWrite );
	if( Acad::eOk != es && Acad::eKeyNotFound != es )
	{
		return false;
	}
	if( Acad::eKeyNotFound == es )
	{
		if( m_createNewKey )
		{
			pXrec = new AcDbXrecord();
			AcDbObjectId xrecObjId;
			if( Acad::eOk != m_pDict->setAt( key, pXrec, xrecObjId ) )
			{
				delete pXrec;
				return false;
			}
			else
			{
				// 在事务里新增的对象,必须要绑定到当前的transaction中,否则得到的指针无效!!!
				// 参见: ObjectARX Developer's Guide -> Advance Topics -> Transaction Management -> Newly Created Objects and Transactions
				if( Acad::eOk != actrTransactionManager->addNewlyCreatedDBRObject( pXrec ) )
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}
	ArxXRecordManager dem( pXrec, true );
	CString temp_value;
	if( !dem.getEntry( FIRST_ENTRY, temp_value ) )
	{
		return ( dem.addEntry( value ) != INVALID_ENTRY );
	}
	else
	{
		return dem.modifyEntry( FIRST_ENTRY, value ); // 每个XRECORD只存放一个数据
	}
}
bool DictDao::get( const CString& key, CString& value ) const
{
	if( m_pDict == 0 ) return false;
	AcDbXrecord* pXrec = 0;
	// key不存在或者其它原因
	Acad::ErrorStatus es = m_pDict->getAt( key, ( AcDbObject*& ) pXrec, AcDb::kForWrite );
	if( Acad::eOk != es && Acad::eKeyNotFound != es )
	{
		return false;
	}
	if( pXrec == 0 )
	{
		return false;
	}
	else
	{
		ArxXRecordManager dem( pXrec, true );
		return dem.getEntry( FIRST_ENTRY, value ); // 每个XRECORD只存放一个数据
	}
}
bool DictDao::add( const CString& key )
{
	if( m_pDict == 0 ) return false;
	AcDbXrecord* pXrec = 0;
	// key不存在或者其它原因
	Acad::ErrorStatus es = m_pDict->getAt( key, ( AcDbObject*& ) pXrec, AcDb::kForWrite );
	if( Acad::eOk != es && Acad::eKeyNotFound != es )
	{
		return false;
	}
	if( Acad::eKeyNotFound == es )
	{
		pXrec = new AcDbXrecord();
		AcDbObjectId xrecObjId;
		if( Acad::eOk != m_pDict->setAt( key, pXrec, xrecObjId ) )
		{
			delete pXrec;
			pXrec = 0;
			return false;
		}
		else
		{
			// 在事务里新增的对象,必须要绑定到当前的transaction中,否则得到的指针无效!!!
			// 参见: ObjectARX Developer's Guide -> Advance Topics -> Transaction Management -> Newly Created Objects and Transactions
			if( Acad::eOk != actrTransactionManager->addNewlyCreatedDBRObject( pXrec ) )
			{
				return false;
			}
			else
			{
				ArxXRecordManager dem( pXrec, true );
				CString temp_value;
				return ( dem.addEntry( temp_value ) != INVALID_ENTRY );// 每个XRECORD只存放一个数据
			}
		}
	}
	else
	{
		// key存在,且打开ArxXRecord成功
		// 必须要close否则会有eWasOpenForWrite错误!!!
		pXrec->close();
	}
	return true;
}
bool DictDao::remove( const CString& key )
{
	if( m_pDict == 0 ) return false;
	AcDbObjectId objId;
	if( Acad::eOk != m_pDict->remove( key, objId ) )
	{
		return false;
	}
	else
	{
		//ArxEntityHelper::EraseObject2( objId, true ); // 删除对象
		return true;
	}
}
ExtDictDao::ExtDictDao( const AcDbObjectId& objId ) : m_objId( objId ), DictDao( ArxDictTool::GetExtensionDict( objId ) )
{
}
ExtDictDao::~ExtDictDao()
{
	// 触发信号槽,通知图元的扩展词典数据已被修改
	if( BaseDao::isModified() )
	{
		DataObserverManager::getSingletonPtr()->signalExtDictModify( m_objId );
	}
}
JsonDictDao::JsonDictDao( const AcDbObjectId& dictId, const CString& user_key )
: m_createNewKey( true ), m_pXrec( 0 ), m_pDem( 0 ), m_user_key( user_key ), m_dictId( dictId )
{
	if( dictId.isNull() ) return;
	AcTransaction* pTrans = actrTransactionManager->startTransaction();
	if( pTrans == 0 ) return;
	//AcDb::OpenMode om = ( save ? ( AcDb::kForWrite ) : ( AcDb::kForRead ) );
	AcDbObject* pObj;
	if( Acad::eOk != pTrans->getObject( pObj, dictId, AcDb::kForWrite ) )
	{
		actrTransactionManager->abortTransaction();
		return;
	}
	AcDbDictionary* pDict = AcDbDictionary::cast( pObj );
	if( pDict == 0 )
	{
		actrTransactionManager->abortTransaction();
		return;
	}
	// key不存在或者其它原因
	Acad::ErrorStatus es = pDict->getAt( m_user_key, ( AcDbObject*& ) m_pXrec, AcDb::kForWrite );
	if( Acad::eOk != es && Acad::eKeyNotFound != es )
	{
		m_pXrec = 0;
		actrTransactionManager->abortTransaction();
		return;
	}
	if( Acad::eKeyNotFound == es )
	{
		if( m_createNewKey )
		{
			m_pXrec = new AcDbXrecord();
			AcDbObjectId xrecObjId;
			if( Acad::eOk != pDict->setAt( m_user_key, m_pXrec, xrecObjId ) )
			{
				actrTransactionManager->abortTransaction();
				delete m_pXrec;
				m_pXrec = 0;
				return;
			}
			else
			{
				// 在事务里新增的对象,必须要绑定到当前的transaction中,否则得到的指针无效!!!
				// 参见: ObjectARX Developer's Guide -> Advance Topics -> Transaction Management -> Newly Created Objects and Transactions
				if( Acad::eOk != actrTransactionManager->addNewlyCreatedDBRObject( m_pXrec ) )
				{
					m_pXrec = 0;
					m_pDem = 0;
					actrTransactionManager->abortTransaction();
					return;
				}
				else
				{
					//ArxXRecordManager dem( m_pXrec, false );
					m_pDem = new ArxXRecordManager( m_pXrec, false );
					if ( m_pDem->addEntry( NULL_JSON_STRING ) == INVALID_ENTRY )
					{
						delete m_pDem;
						m_pXrec = 0;
						m_pDem = 0;
						actrTransactionManager->abortTransaction();
						return;
					}
					else
					{
						// 初始化为空对象,即 "{}"
						// 注意:不是空值nullValue,空值的json字符串是不存在的!!!
						m_root = Json::Value( Json::objectValue );
					}
				}
			}
		}
		else
		{
			m_pXrec = 0;
			m_pDem = 0;
			actrTransactionManager->abortTransaction();
			return;
		}
	}
	else
	{
		// 尝试获取第1项entry
		CString entry;
		//ArxXRecordManager dem( m_pXrec, false );
		m_pDem = new ArxXRecordManager( m_pXrec, false );
		if( !m_pDem->getEntry( FIRST_ENTRY, entry ) )
		{
			// 扩展记录XRecord中没有数据, 增加一个
			if ( m_pDem->addEntry( NULL_JSON_STRING ) == INVALID_ENTRY )
			{
				delete m_pDem;
				m_pDem = 0;
				m_pXrec = 0;
				actrTransactionManager->abortTransaction();
				return;
			}
			else
			{
				// 初始化为空对象,即 "{}"
				// 注意:不是空值nullValue,空值的json字符串是不存在的!!!
				m_root = Json::Value( Json::objectValue );
			}
		}
		else
		{
			// 尝试解析成json数据
			Json::Reader reader;
			if( !reader.parse( W2C( ( LPCTSTR )entry ), m_root ) )
			{
				m_root = Json::Value( Json::objectValue );
			}
		}
	}
}
JsonDictDao::~JsonDictDao()
{
	if( m_pXrec != 0 && m_pDem != 0 )
	{
		// 输出无格式json字符串
		Json::FastWriter writer;
		std::string json_str = writer.write( m_root );
		// 将json数据写入到XRecord中
		//ArxXRecordManager dem( m_pXrec, false );
		m_pDem->modifyEntry( FIRST_ENTRY, C2W( json_str.c_str() ) );
		delete m_pDem;
		m_pXrec->close();
		actrTransactionManager->endTransaction();
		// 触发信号槽,通知json词典数据已被修改
		if( BaseDao::isModified() )
		{
			DataObserverManager::getSingletonPtr()->signalJsonDictModify( m_dictId );
		}
	}
}
void JsonDictDao::clear()
{
	if( m_pXrec == 0 || m_pDem == 0 ) return;
	// 清空json数据
	m_root.clear();
}
bool JsonDictDao::set( const CString& key, const CString& value )
{
	if( m_pXrec == 0 || m_pDem == 0 ) return false;
	if( key.IsEmpty() ) return false;
	// 修改数据
	std::string sKey = W2C( ( LPCTSTR )key );
	std::string sValue = W2C( ( LPCTSTR )value );
	m_root[sKey] = Json::Value( sValue );
	return true;
}
bool JsonDictDao::get( const CString& key, CString& value ) const
{
	if( m_pXrec == 0 || m_pDem == 0 ) return false;
	// 提取数据
	std::string sKey = W2C( ( LPCTSTR )key );
	if( !m_root.isMember( sKey ) ) return false;
	value = C2W( m_root[sKey].asString().c_str() );
	return true;
}
bool JsonDictDao::add( const CString& key )
{
	if( m_pXrec == 0 || m_pDem == 0 ) return false;
	// 提取数据
	std::string sKey = W2C( ( LPCTSTR )key );
	if( m_root.isMember( sKey ) ) return false;
	// 增加一个新的字段
	m_root[sKey] = "";
	return true;
}
bool JsonDictDao::remove( const CString& key )
{
	if( m_pXrec == 0 || m_pDem == 0 ) return false;
	// 提取数据
	std::string sKey = W2C( ( LPCTSTR )key );
	if( m_root.isMember( sKey ) )
	{
		m_root.removeMember( sKey );
	}
	return true;
}

XDataDao::XDataDao( const AcDbObjectId& objId ) : m_pObj( 0 ), m_objId( objId )
{
	if( objId.isNull() ) return;
	AcTransaction* pTrans = actrTransactionManager->startTransaction();
	if( pTrans == 0 ) return;
	//AcDb::OpenMode om = ( save ? ( AcDb::kForWrite ) : ( AcDb::kForRead ) );
	if( Acad::eOk != pTrans->getObject( m_pObj, objId, AcDb::kForWrite ) )
	{
		actrTransactionManager->abortTransaction();
		m_pObj = 0;
		return;
	}
}
XDataDao::~XDataDao()
{
	if( m_pObj != 0 )
	{
		actrTransactionManager->endTransaction();
		// 触发信号槽,通知扩展数据已被修改
		if( BaseDao::isModified() )
		{
			DataObserverManager::getSingletonPtr()->signalXDataModify( m_objId );
		}
	}
}
void XDataDao::clear()
{
	ArxDbgAppXdata xdata( XDATA_PROPERTY_APPNAME, acdbHostApplicationServices()->workingDatabase() );
	xdata.getXdata( m_pObj );
	xdata.removeAll();
	xdata.setXdata( m_pObj );
}
bool XDataDao::get( const CString& key, CString& value ) const
{
	int pos = -1;
	if( !StringHelper::StringToInt( key, pos ) ) return false;
	if( pos < 0 ) return false;
	ArxDbgAppXdata xdata( XDATA_PROPERTY_APPNAME, acdbHostApplicationServices()->workingDatabase() );
	xdata.getXdata( m_pObj );
	if( xdata.isEmpty() ) return false;
	return xdata.getString( pos, value );
}
bool XDataDao::set( const CString& key, const CString& value )
{
	int pos = -1;
	if( !StringHelper::StringToInt( key, pos ) ) return false;
	if( pos < 0 ) return false;
	ArxDbgAppXdata xdata( XDATA_PROPERTY_APPNAME, acdbHostApplicationServices()->workingDatabase() );
	xdata.getXdata( m_pObj );
	//if(xdata.isEmpty()) return false;
	xdata.setString( pos, value );
	return ( Acad::eOk == xdata.setXdata( m_pObj ) );
}
bool XDataDao::add( const CString& key )
{
	return this->set( key, _T( "" ) );
}
bool XDataDao::remove( const CString& key )
{
	int pos = -1;
	if( !StringHelper::StringToInt( key, pos ) ) return false;
	if( pos < 0 ) return false;
	ArxDbgAppXdata xdata( XDATA_PROPERTY_APPNAME, acdbHostApplicationServices()->workingDatabase() );
	xdata.getXdata( m_pObj );
	//if(xdata.isEmpty()) return false;
	// 删除位置标签tag
	if( !xdata.remove( pos ) )
	{
		return false;
	}
	else
	{
		return ( Acad::eOk == xdata.setXdata( m_pObj ) );
	}
}

EntityDataDao::EntityDataDao( const CString& clsname, AcDbObjectId& objId )
: m_pData( 0 ), m_newEntity( true ), m_pObj( 0 ), m_pObjId( &objId )
{
	// 根据类型名称动态创建图元
	m_pObj = ArxClassHelper::CreateObjectByType( clsname );
	if( m_pObj == 0 ) return;
	// 必须是图元类型!!
	if( !m_pObj->isKindOf( AcDbEntity::desc() ) )
	{
		acutPrintf( _T( "\n不支持动态创建非图元类%s(要求必须从AcDbEntity派生)!!!" ), clsname );
		delete m_pObj;
		m_pObj = 0;
		return;
	}
	AcDbObjectData* pObjectData = BuiltinDataManager::getSingletonPtr()->getData( m_pObj->isA()->name() );
	if( pObjectData != 0 )
	{
		pObjectData->setObject( m_pObj );
		m_pData = pObjectData;
	}
	else
	{
		acutPrintf( _T( "\n%s图元目前还不支持字段功能!!!" ), clsname );
		delete m_pObj;
		m_pObj = 0;
	}
}
EntityDataDao::EntityDataDao( const AcDbObjectId& objId )
: m_pData( 0 ), m_newEntity( false ), m_pObj( 0 ), m_pObjId( 0 ), m_objId( objId )
{
	if( objId.isNull() ) return;
	AcTransaction* pTrans = actrTransactionManager->startTransaction();
	if( pTrans == 0 ) return;
	//AcDbObject* m_pObj;
	if( Acad::eOk != pTrans->getObject( m_pObj, objId, AcDb::kForWrite ) )
	{
		actrTransactionManager->abortTransaction();
		m_pData = 0;
		m_pObj = 0;
		return;
	}
	/*
	* 图元分为两类:
	* (1) cad内置图元(直线、圆、圆弧等,支持一部分)
	* (2) 从Entity基类派生(支持)
	* (3) 其它arx的自定义类(不支持)
	*/
	BaseEntity* pEnt = BaseEntity::cast( m_pObj );
	if( pEnt != 0 )
	{
		m_pData = pEnt;
	}
	else
	{
		AcDbObjectData* pData = BuiltinDataManager::getSingletonPtr()->getData( m_pObj->isA()->name() );
		if( pData == 0 )
		{
			actrTransactionManager->abortTransaction();
			m_pData = 0;
			m_pObj = 0;
			return;
		}
		else
		{
			pData->setObject( m_pObj );
			m_pData = pData;
		}
	}
}
EntityDataDao::~EntityDataDao()
{
	if( m_pData != 0 )
	{
		// 打开的是数据库中已存在的图元对象,结束事务操作并提交到cad数据库
		if( !m_newEntity )
		{
			actrTransactionManager->endTransaction();
			// 触发信号槽,通知图元的扩展词典数据已被修改
			if( BaseDao::isModified() )
			{
				DataObserverManager::getSingletonPtr()->signalEntityDataModify( m_objId );
			}
		}
		// 新建图元,提交到cad数据库中
		else if( m_pObj != 0 )
		{
			AcDbEntity* pEnt = AcDbEntity::cast( m_pObj );
			if( pEnt != 0 && ArxUtilHelper::PostToModelSpace( pEnt ) )
			{
				*m_pObjId = pEnt->objectId(); // 获取id
			}
			else
			{
				m_pObjId->setNull();
				delete m_pObj;
			}
		}
	}
}
void EntityDataDao::clear()
{
}
bool EntityDataDao::set( const CString& key, const CString& value )
{
	if( m_pData == 0 ) return false;
	if( key.IsEmpty() ) return false;
	return m_pData->setString( key, value );
}
bool EntityDataDao::get( const CString& key, CString& value ) const
{
	if( m_pData == 0 ) return false;
	return m_pData->getString( key, value );
}
bool EntityDataDao::add( const CString& key )
{
	return false;
}
bool EntityDataDao::remove( const CString& key )
{
	return false;
}