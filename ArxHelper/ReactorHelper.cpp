#include "StdAfx.h"
#include "ReactorHelper.h"
//#include "EditorReactor.h"
//#include "DocManagerReactor.h"
//#include "DatabaseReactor.h"
#include "InputPointMonitor.h"
/*
 * 用于记录与Document相关的reactor
 * 例如AcDbDataBaseReactor、AcEdInputPointMonitor等都是与文档相关的
 * 一个文档对象对应一个reactor，可以看做是"局部的"
 * 而AcEditorReactor属于AcEditor，可以看做是"全局的"
 * 从arx的消息流程可以看出，在kLoadAppMsg()中AcEditor有效，
 * 而其他的例如文档对象(AcApDocument)、数据库对象(AcDbDatabase)还是无效的
 * 只有在kLoadDwgMsg()中的文档和数据库对象才开始有效
 */
#include <map>
struct DocumentReactor
{
    DocumentReactor() : pInputPointMonitor( 0 ) {}
    InputPointMonitor* pInputPointMonitor;
    // 还可以添加其它的 AcDbDatabaseReactor、
    // AcEdInputPointMonitor等属于ApApDocument类别的reactor
    // ...
    // 集中在一起便于管理，也解决了多文档的问题
    // 1) 新建文档，发送kLoadDwgMsg消息
    // 2) 已有文档，加载arx程序，也会向所有的文档发送kLoadDwgMsg消息
    // 3) 关闭文档， 发送kUnloadDwgMsg消息
    // 4) 卸载arx程序，也会向所有的文档发送kUnloadDwgMsg消息
};
/*
 * long类型表示AcApDocument*的指针地址
 */
typedef std::map<long, DocumentReactor> DocumentReactorMap;
//EditorReactor* pEditorReactor = 0;
DocumentReactorMap* pDocumentReactorMap = 0;
//DocManagerReactor* pDocManagerReactor = 0;
void ReactorHelper::CreateEditorReactor()
{
    //if( pEditorReactor == 0 )
    //{
    //    pEditorReactor = new EditorReactor( true );
    //}
}
void ReactorHelper::RemoveEditorReactor()
{
    //delete pEditorReactor;
    //pEditorReactor = 0;
}
void ReactorHelper::CreateDocManagerReactor()
{
    //if( pDocManagerReactor == 0 )
    //{
    //    pDocManagerReactor = new DocManagerReactor();
    //    acDocManager->addReactor( pDocManagerReactor );
    //}
}
void ReactorHelper::RemoveDocManagerReactor()
{
    //if( pDocManagerReactor != 0 )
    //{
    //    acDocManager->removeReactor( pDocManagerReactor );
    //    delete pDocManagerReactor;
    //    pDocManagerReactor = 0;
    //}
}
void ReactorHelper::CreateDocumentReactorMap()
{
    if( pDocumentReactorMap == 0 )
    {
        pDocumentReactorMap = new DocumentReactorMap();
    }
}
void ReactorHelper::RemoveDocumentReactorMap()
{
    delete pDocumentReactorMap;
    pDocumentReactorMap = 0;
}
void ReactorHelper::AddDocumentReactor( AcApDocument* pDoc )
{
    if( pDoc == 0 ) return;
    if( pDocumentReactorMap == 0 ) return;
    long id = ( long )pDoc;
    DocumentReactorMap::iterator itr = pDocumentReactorMap->find( id );
    if( itr != pDocumentReactorMap->end() ) return;
    DocumentReactor dr;
    dr.pInputPointMonitor = new InputPointMonitor( pDoc );
    pDocumentReactorMap->insert( DocumentReactorMap::value_type( id, dr ) );
}
void ReactorHelper::RemoveDocumentReactor( AcApDocument* pDoc )
{
    if( pDoc == 0 ) return;
    if( pDocumentReactorMap == 0 ) return;
    long id = ( long )pDoc;
    DocumentReactorMap::iterator itr = pDocumentReactorMap->find( id );
    if( itr == pDocumentReactorMap->end() ) return;
    // 解除绑定
    DocumentReactor dr = itr->second;
    delete dr.pInputPointMonitor;
    //delete dr.pTooltipMonitor;
    pDocumentReactorMap->erase( id );
}

AcDbObjectId ReactorHelper::GetApertureEntity()
{
    long id = ( long )curDoc();
    DocumentReactorMap::iterator itr = pDocumentReactorMap->find(id);
    if( itr == pDocumentReactorMap->end() ) 
    {
        return AcDbObjectId::kNull;
    }
    else
    {
        return itr->second.pInputPointMonitor->getApertureEntity();
    }
}
