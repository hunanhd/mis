#include "StdAfx.h"
#include "ObjectData.h"
#include "_InternalDataHelper.h"
static bool UpdateDataHelper( BaseDao* dao, resbuf* pTemp, bool save )
{
    while( pTemp != 0 )
    {
        CString field = pTemp->resval.rstring;
        if( save )
        {
            CString value;
            pTemp = GetData( pTemp, value );
            dao->setString( field, value );
        }
        else
        {
            CString value;
            bool ret = dao->getString( field, value );
            if( !ret )
            {
                for( int i = 0; i < 3; i++ ) pTemp = pTemp->rbnext;
            }
            else
            {
                pTemp = SetData( pTemp, value );
            }
        }
    }
    return true;
}
ObjectData::ObjectData() : BaseData()
{
}
void ObjectData::setDataSource( const AcDbObjectId& objId )
{
    // 只允许关联一次
    if( !m_objId.isNull() | objId.isNull() ) return;
    m_objId = objId;
}
AcDbObjectId ObjectData::getDataSource() const
{
    return m_objId;
}
bool ObjectData::update( bool save/*=false*/ )
{
    /*
     * 特殊说明,arx的对象在操作的时候要先标记(读或写)
     * 需要在函数开头调用assertReadEnable()或者assertWriteEnable()函数
     * 为了解决这个问题,定义并调用虚函数来解决!
     */
    if( save )
    {
        this->onBeforeSetData( _T( "" ), _T( "" ) ); //随便传入一个参数,触发set操作响应事件
    }
    else
    {
        this->onBeforeGetData( _T( "" ) ); //随便传入一个参数,触发get操作响应事件
    }
    // 如果字段没有初始化，则初始化
    if( m_pDatasToFileds == 0 )
    {
        this->regDatas();
        if( m_pDatasToFileds == 0 ) return false;
    }
    // 创建数据操作接口
    BaseDao* dao = this->createDao();
    if( dao == 0 ) return false;
    // 更新或读取数据
    bool ret = UpdateDataHelper( dao, m_pDatasToFileds, save );
    // 销毁dao指针内存
    delete dao;
    return ret;
}
