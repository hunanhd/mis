#include "StdAfx.h"
#include "DataListHelper.h"
#include "HelperClass.h"
#include "config.h"
void StringListHelper::GetAllNames( AcStringArray& names )
{
    ArxDictHelper::GetAllKeys( STRING_LIST_DICT, names );
}
bool StringListHelper::AddString( const CString& name, const CString& strValue )
{
    return ArxDictHelper::AddEntry( STRING_LIST_DICT, name, strValue );
}
bool StringListHelper::AddStringList( const CString& name, const AcStringArray& strList )
{
    if( ( name.GetLength() == 0 ) || strList.isEmpty() ) return false;
    ArxDictTool* pDictTool = ArxDictTool::GetDictTool( STRING_LIST_DICT );
    bool ret = pDictTool->findKey( name );
    if( !ret ) // 如果变量name已存在，则不进行修改
    {
        int len = strList.length();
        for( int i = 0; i < len; i++ )
        {
            pDictTool->addEntry( name, strList[i].kACharPtr() );
        }
    }
    delete pDictTool;
    return !ret;
}
bool StringListHelper::GetStringList( const CString& name, AcStringArray& strList )
{
    return ArxDictHelper::GetAllEntries( STRING_LIST_DICT, name, strList );
}
void StringListHelper::RemoveStringList( const CString& name )
{
    ArxDictHelper::RemoveAllEntries( STRING_LIST_DICT, name );
}
void IntStrListHelper::GetAllNames( AcStringArray& names )
{
    ArxDictHelper::GetAllKeys( INT_LIST_DICT, names );
}
bool IntStrListHelper::AddIntStrPair( const CString& name, int intValue, const CString& strValue )
{
    if( ( name.GetLength() == 0 ) || strValue.GetLength() == 0 ) return false;
    AcStringArray strList;
    AcDbIntArray intList;
    GetIntStrList( name, intList, strList );
    if( intList.contains( intValue ) || strList.contains( strValue ) ) return false;
    intList.append( intValue );
    strList.append( strValue );
    RemoveIntStrList( name );
    return AddIntStrList( name, intList, strList );
}
bool IntStrListHelper::AddIntStrList( const CString& name, const AcDbIntArray& intList, const AcStringArray& strList )
{
    if( ( name.GetLength() == 0 ) || strList.isEmpty() || intList.isEmpty() ) return false;
    if( intList.length() != strList.length() ) return false;
    ArxDictTool* pDictTool = ArxDictTool::GetDictTool( INT_LIST_DICT );
    bool ret = pDictTool->findKey( name );
    if( !ret )
    {
        int len = intList.length();
        for( int i = 0; i < len; i++ )
        {
            CString intValue;
            intValue.Format( _T( "%d" ), intList[i] );
            pDictTool->addEntry( name, intValue );             // 偶数位置的元素为整数
            pDictTool->addEntry( name, strList[i].kACharPtr() ); // 奇数位置的元素为字符串
            //acutPrintf(_T("\n设置整数:%d,字符串:%s"),intValue,strList[i].kACharPtr());
        }
    }
    delete pDictTool;
    return !ret;
}
bool IntStrListHelper::GetIntStrList( const CString& name, AcDbIntArray& intList, AcStringArray& strList )
{
    AcStringArray entries;
    if( !ArxDictHelper::GetAllEntries( INT_LIST_DICT, name, entries ) ) return false;
    int len = entries.length();
    bool ret = ( len > 0 && len % 2 == 0 );
    if( ret ) // 长度必须为偶数
    {
        intList.removeAll();
        strList.removeAll();
        for( int i = 0; i < len; i++ )
        {
            if( i % 2 == 0 ) // 偶数位置的元素为整数
            {
                intList.append( _ttoi( entries[i].kACharPtr() ) );
                //acutPrintf(_T("\n获取整数:%d"),_ttoi( entries[i].kACharPtr() ));
            }
            else       // 奇数位置的元素为字符串
            {
                strList.append( entries[i] );
                //acutPrintf(_T("\n获取字符串:%s"), entries[i].kACharPtr() );
            }
        }
    }
    return ret;
}
void IntStrListHelper::RemoveIntStrList( const CString& name )
{
    ArxDictHelper::RemoveAllEntries( INT_LIST_DICT, name );
}