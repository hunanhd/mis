#include "StdAfx.h"
#include "resource.h"
#include "HelperClass.h"
#include "JsonFieldDict.h"
#include "BuiltinDataManager.h"
#include "DataObserverManager.h"
#include "ReactorHelper.h"
// 定义注册服务名称
#ifndef ARXHELPER_SERVICE_NAME
#define ARXHELPER_SERVICE_NAME _T("ARXHELPER_SERVICE_NAME")
#endif

// 系统精度设置器全局变量
ArxTolSetter* _pTolSetter = 0;
// 构建Console日志系统
ConsoleStdSwitch* _pConsoleStdSwitch = 0;
ConsoleFileSwitch* _pConsoleFileSwitch = 0;
// 重定向C++标准输出(不好使!!!)
//StdOutSwitch* _pStdOutSwitch = 0;

class CArxHelperApp : public AcRxArxApp
{
public:
    CArxHelperApp () : AcRxArxApp () {}
    virtual AcRx::AppRetCode On_kInitAppMsg ( void* pkt )
    {
        AcRx::AppRetCode retCode = AcRxArxApp::On_kInitAppMsg ( pkt ) ;
        acrxRegisterAppMDIAware( pkt );
        // 注册服务
        acrxRegisterService( ARXHELPER_SERVICE_NAME );

        // 修改cad系统全局精度
        _pTolSetter = new ArxTolSetter( NUM_TOLERANCE );
        // 初始化Console系统
        Console::Init();
        // 输出到cad的命令行
        _pConsoleStdSwitch = new ConsoleStdSwitch;
        // 输出到arx.log文件
        _pConsoleFileSwitch = new ConsoleFileSwitch(_T("arx.log"));
        // 重定向C++标准流输出(无效)
        //_pStdOutSwitch = new StdOutSwitch;

        // 注册内置图元数据管理器
        BuiltinDataManager::Init();
		// 初始化DataObserverManager单例对象
		DataObserverManager::Init();
        ReactorHelper::CreateEditorReactor();
        ReactorHelper::CreateDocManagerReactor();
        ReactorHelper::CreateDocumentReactorMap();
        return retCode;
    }
    virtual AcRx::AppRetCode On_kUnloadAppMsg ( void* pkt )
    {
        AcRx::AppRetCode retCode = AcRxArxApp::On_kUnloadAppMsg ( pkt ) ;
        // 删除AcEditorReactor
        ReactorHelper::RemoveEditorReactor();
        ReactorHelper::RemoveDocManagerReactor();
        ReactorHelper::RemoveDocumentReactorMap();
        // 删除内置图元数据管理器
        BuiltinDataManager::UnInit();
		// 销毁DataObserverManager单例对象
		DataObserverManager::UnInit();

        // 恢复cad系统的全局精度
        delete _pTolSetter;
        _pTolSetter = 0;
        // 恢复C++标准流输出(无效)
        //delete _pStdOutSwitch;
        //_pStdOutSwitch = 0;
        // 卸载Console系统
        delete _pConsoleStdSwitch;
        _pConsoleStdSwitch = 0;
        delete _pConsoleFileSwitch;
        _pConsoleFileSwitch = 0;
        Console::UnInit();

        // 删除服务
        delete acrxServiceDictionary->remove( ARXHELPER_SERVICE_NAME );
        return retCode;
    }
    virtual AcRx::AppRetCode On_kLoadDwgMsg( void* pkt )
    {
        AcRx::AppRetCode retCode = AcRxArxApp::On_kLoadDwgMsg ( pkt ) ;
        // 添加反应器
        ReactorHelper::AddDocumentReactor( curDoc() );
        return retCode;
    }
    virtual AcRx::AppRetCode On_kUnloadDwgMsg( void* pkt )
    {
        AcRx::AppRetCode retCode = AcRxArxApp::On_kUnloadDwgMsg ( pkt ) ;
        ReactorHelper::RemoveDocumentReactor( curDoc() );
        return retCode;
    }
    virtual void RegisterServerComponents ()
    {
    }
    static void JL_JsonFieldDict()
    {
        ArxDictTool::RegDict( _T( "json测试" ) );
        ArxDictTool2* pDictTool2 = ArxDictTool2::GetDictTool( _T( "json测试" ) );
        JsonFieldDict* pDO = new JsonFieldDict();
        bool ret = pDictTool2->addEntry( _T( "json" ), pDO );
        if( !ret ) delete pDO; // 添加Object失败
        delete pDictTool2;
        //ArxDictTool::RemoveDict( _T( "json测试" ) );
    }
    static void JL_JsonTest()
    {
        // 获取id
        AcDbObjectId objId;
        ArxDictTool2* pDictTool2 = ArxDictTool2::GetDictTool( _T( "Json词典测试" ) );
        bool ret = pDictTool2->findEntry( _T( "json" ), objId );
        delete pDictTool2;
        AcTransaction* pTrans = actrTransactionManager->startTransaction();
        if( pTrans == 0 ) return /*false*/;
        AcDbObject* pObj;
        if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForWrite ) )
        {
            actrTransactionManager->abortTransaction();
            return /*false*/;
        }
        JsonFieldDict* pJson = JsonFieldDict::cast( pObj );
        if( pJson == 0 )
        {
            actrTransactionManager->abortTransaction();
            return /*false*/;
        }
        // 操作json数据
        pJson->addField( _T( "Tunnel" ), _T( "风量" ), _T( "常用" ) );
        actrTransactionManager->endTransaction();
    }
    static void JL_JsonPrint()
    {
        // 获取id
        AcDbObjectId objId;
        ArxDictTool2* pDictTool2 = ArxDictTool2::GetDictTool( _T( "Json词典测试" ) );
        bool ret = pDictTool2->findEntry( _T( "json" ), objId );
        delete pDictTool2;
        AcTransaction* pTrans = actrTransactionManager->startTransaction();
        if( pTrans == 0 ) return /*false*/;
        AcDbObject* pObj;
        if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForRead ) )
        {
            actrTransactionManager->abortTransaction();
            return /*false*/;
        }
        JsonFieldDict* pJson = JsonFieldDict::cast( pObj );
        if( pJson == 0 )
        {
            actrTransactionManager->abortTransaction();
            return /*false*/;
        }
        // 操作打印
        pJson->print();
        actrTransactionManager->endTransaction();
    }
    static void JL_JsonDictDataTest1()
    {
        // 注册词典
        AcDbObjectId dictId = ArxDictTool::RegDict( _T( "Json数据测试" ) );
        // 测试JsonDictData以及Dao
        BaseDao* dao = JsonDictData::CreateDao( dictId, _T( "我的数据" ) );
        if( dao == 0 ) return;
        dao->setString( _T( "总风量" ), _T( "1500" ) );
        dao->setString( _T( "瓦斯涌出量" ), _T( "10.5" ) );
        delete dao;
        // 测试JsonDictData以及Dao
        /*Dao* */
        dao = JsonDictData::CreateDao( dictId, _T( "他的数据" ) );
        if( dao == 0 ) return;
        dao->setString( _T( "总风量" ), _T( "2000" ) );
        dao->setString( _T( "瓦斯涌出量" ), _T( "3.8" ) );
        dao->setString( _T( "风阻" ), _T( "0.0456" ) );
        delete dao;
    }
    static void JL_JsonDictDataTest2()
    {
        // 注册词典
        AcDbObjectId dictId = ArxDictTool::RegDict( _T( "Json数据测试" ) );
        // 测试JsonDictData以及Dao
        BaseDao* dao = JsonDictData::CreateDao( dictId, _T( "我的数据" ) );
        if( dao == 0 ) return;
        CString q, w;
        dao->getString( _T( "总风量" ), q );
        dao->getString( _T( "瓦斯涌出量" ), w );
        delete dao;
        acutPrintf( _T( "\n%s --> %s" ), q, w );
    }
    static void JL_SomeTest()
    {
        AcGePoint3d pt( 10, 20, 100 );
        CString str = ArxUtilHelper::Point3dToString( pt );
        acutPrintf( _T( "\n转化后的字符串:%s" ), str );
        AcGePoint3d pt2;
        ArxUtilHelper::StringToPoint3d( str, pt2 );
        acutPrintf( _T( "\n转换后的点坐标:(%.3f, %.3f, %.3f)" ), pt2.x, pt2.y, pt2.z );
    }
    static void JL_EntityDataTest()
    {
        AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "\n请选择一条直线" ) );
        if( objId.isNull() ) return;
        BaseDao* dao = EntityData::CreateDao( objId );
        CString value;
        dao->getString( _T( "起点" ), value );
        AcGePoint3d pt;
        ArxUtilHelper::StringToPoint3d( value, pt );
        pt.x += 200;
        pt.y -= 300;
        dao->setString( _T( "起点" ), ArxUtilHelper::Point3dToString( pt ) );
        delete dao;
    }
    static void JL_ColorTest()
    {
        AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "\n请选择一个图元:" ) );
        if( objId.isNull() ) return;
        Adesk::UInt16 colorIndex;
        if( !ArxEntityHelper::GetEntityColor( objId, colorIndex ) ) return;
        acutPrintf( _T( "\n所选图元颜色的ACI值:%d" ), ( int )colorIndex );
        Adesk::UInt8 r, g, b;
        ArxUtilHelper::ACI_2_RGB( colorIndex, r, g, b );
        acutPrintf( _T( "\n所选图元颜色的rgb值:(%d, %d, %d)" ), r, g, b );
    }
} ;
IMPLEMENT_ARX_ENTRYPOINT( CArxHelperApp )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxHelperApp, JL, _SomeTest, SomeTest, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxHelperApp, JL, _ColorTest, ColorTest, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxHelperApp, JL, _EntityDataTest, EntityDataTest, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxHelperApp, JL, _JsonTest, JsonTest, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxHelperApp, JL, _JsonPrint, JsonPrint, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxHelperApp, JL, _JsonDictDataTest1, jddt1, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxHelperApp, JL, _JsonDictDataTest2, jddt2, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxHelperApp, JL, _JsonFieldDict, jfd, ACRX_CMD_TRANSPARENT, NULL )
