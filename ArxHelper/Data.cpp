#include "StdAfx.h"
#include "Data.h"
#include "HelperClass.h"
#include "_InternalDataHelper.h"
BaseDao::BaseDao() : m_isModified( false )
{
}
BaseDao::~BaseDao()
{
	if( m_isModified )
	{
		//DataObserverManager::getSingletonPtr()->signalExtDictModify(
	}
}
bool BaseDao::getString(const CString& field, CString& value) const
{
	return this->get( field, value );
}
bool BaseDao::getInt( const CString& field, int& value ) const
{
	CString str;
	if( !this->getString( field, str ) ) return false;
	return StringHelper::StringToInt( str, value );
}
bool BaseDao::getDouble( const CString& field, double& value ) const
{
	CString str;
	if( !this->getString( field, str ) ) return false;
	return StringHelper::StringToDouble( str, value );
}
bool BaseDao::getBool( const CString& field, bool& value ) const
{
	CString str;
	if( !this->getString( field, str ) ) return false;
	int v = 0;
	if( !StringHelper::StringToInt( str, v ) ) return false;
	value = ( v != 0 );
	return true;
}
bool BaseDao::getPoint( const CString& field, AcGePoint3d& value ) const
{
	CString str;
	if( !this->getString( field, str ) ) return false;
	return ArxUtilHelper::StringToPoint3d( str, value );
}
bool BaseDao::getVector( const CString& field, AcGeVector3d& value ) const
{
	AcGePoint3d pt;
	if( !this->getPoint( field, pt ) ) return false;
	value = pt.asVector();
	return true;
}
bool BaseDao::getDateTime( const CString& field, COleDateTime& value ) const
{
	CString str;
	if( !this->getString( field, str ) ) return false;
	return value.ParseDateTime( str );
}
bool BaseDao::getObjectId( const CString& field, AcDbObjectId& value ) const
{
	CString str;
	if( !this->getString( field, str ) ) return false;
	// 字符串转换成id
	value = ArxUtilHelper::StrtoObjectId( str );
	return true;
}
bool BaseDao::setString(const CString& field, const CString& value)
{
	bool ret = this->set( field, value );
	if( ret )
	{
		// 标记为修改状态
		m_isModified = true;
	}
	return ret;
}
bool BaseDao::setInt( const CString& field, int value )
{
	CString str;
	StringHelper::IntToString( value, str );
	return this->setString( field, str );
}
bool BaseDao::setDouble( const CString& field, double value )
{
	CString str;
	StringHelper::DoubleToString( value, str );
	return this->setString( field, str );
}
bool BaseDao::setBool( const CString& field, bool value )
{
	return this->setInt( field, value ? 1 : 0 );
}
bool BaseDao::setPoint( const CString& field, const AcGePoint3d& value )
{
	CString str = ArxUtilHelper::Point3dToString( value );
	return this->setString( field, str );
}
bool BaseDao::setVector( const CString& field, const AcGeVector3d& value )
{
	return this->setPoint( field, AcGePoint3d::kOrigin + value );
}
bool BaseDao::setDateTime( const CString& field, const COleDateTime& value )
{
	CString str = value.Format( _T( "%Y-%m-%d %H:%M:%S" ) );
	return this->setString( field, str );
}
bool BaseDao::setObjectId( const CString& field, const AcDbObjectId& value )
{
	CString str = ArxUtilHelper::ObjectIdToStr( value );
	return this->setString( field, str );
}
bool BaseDao::addField(const CString& key)
{
	return this->add( key );
}
bool BaseDao::removeField(const CString& key)
{
	return this->remove( key );
}
void BaseDao::clearAllFields()
{
	this->clear();
}
bool BaseDao::isModified() const
{
	return m_isModified;
}
BaseData::BaseData(): m_pDatasToFileds( 0 )
{
}
BaseData::~BaseData( void )
{
    if( m_pDatasToFileds != NULL )
    {
        acutRelRb( m_pDatasToFileds );
    }
}
void BaseData::mapByField( const CString& field, short rtype, void* pValue )
{
    MapGenericDataByField( m_pDatasToFileds, getRealField( field ), rtype, pValue );
}
void BaseData::map( const CString& field, CString* pValue )
{
    mapByField( field, BaseData::DT_STRING, pValue );
}
void BaseData::map( const CString& field, int* pValue )
{
    mapByField( field, BaseData::DT_INT, pValue );
}
void BaseData::map( const CString& field, double* pValue )
{
    mapByField( field, BaseData::DT_NUMERIC, pValue );
}
void BaseData::map( const CString& field, bool* pValue )
{
    mapByField( field, BaseData::DT_BOOL, pValue );
}
void BaseData::map( const CString& field, COleDateTime* pValue )
{
    mapByField( field, BaseData::DT_DATE, pValue );
}
void BaseData::map( const CString& field, AcGePoint3d* pValue )
{
    mapByField( field, BaseData::DT_POINT, pValue );
}
void BaseData::map( const CString& field, AcGeVector3d* pValue )
{
    mapByField( field, BaseData::DT_VECTOR, pValue );
}
void BaseData::map( const CString& field, AcDbObjectId* pValue )
{
    mapByField( field, BaseData::DT_HANDLE, pValue );
}
void BaseData::getAllFields( AcStringArray& fields ) const
{
    GetAllFields( m_pDatasToFileds, fields );
}
void BaseData::getAllDatas( AcStringArray& fields, AcStringArray& values )
{
    GetAllDatas( m_pDatasToFileds, fields, values );
}
void BaseData::getAllFieldTypes( AcStringArray& fields, AcDbIntArray& types )
{
    GetAllFieldTypes( m_pDatasToFileds, fields, types );
}
bool BaseData::get( const CString& field, CString& value ) const
{
    this->onBeforeGetData( field );
    resbuf* pStart = GoToListEnd( m_pDatasToFileds, getRealField( field ) );
    // 该字段不存在
    if( 0 == pStart )
    {
        return false;
    }
    else
    {
        GetData( pStart, value );
        this->onAfterGetData( field );
        return true;
    }
}
bool BaseData::set( const CString& field, const CString& value )
{
    this->onBeforeSetData( field, value );
    resbuf* pStart = GoToListEnd( m_pDatasToFileds, getRealField( field ) );
    // 该字段不存在
    if( 0 == pStart )
    {
        return false;
    }
    else
    {
        SetData( pStart, value );
        this->onAfterSetData( field, value );
        return true;
    }
}
CString BaseData::getRealField( const CString& field ) const
{
    return field;
}
//void Data::hackField(const CString& field, const CString& new_field)
//{
//	if(m_pDatasToFileds != 0)
//	{
//		resbuf* pStart = GoToListEnd(m_pDatasToFileds, field);
//		if(pStart != 0)
//		{
//			if ( pStart->resval.rstring != NULL )
//			{
//				acutDelString( pStart->resval.rstring );
//			}
//			acutNewString( new_field, pStart->resval.rstring );
//		}
//	}
//}
