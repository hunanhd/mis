#include "StdAfx.h"
#include "Entity.h"
#include "HelperClass.h"
// Entity作为抽象类
ACRX_NO_CONS_DEFINE_MEMBERS ( BaseEntity, AcDbEntity )
BaseEntity::BaseEntity()
{
    // 关联图元的基本几何特性，包括图层、颜色、线型、线宽
    map( _T( "图层" ), &m_layer );
    map( _T( "颜色" ), &m_colorIndex );
    map( _T( "线型" ), &m_lineType );
    map( _T( "线宽" ), &m_lineWeight );
}
BaseEntity::~BaseEntity()
{
}
//CString Entity::getRealField(const CString& field) const
//{
//	return FieldHelper::GuessRealField(this->getTypeName(), field);
//}
void BaseEntity::onBeforeGetData( const CString& field ) const
{
    AcDbEntity* pEnt = AcDbEntity::cast( this );
    if( pEnt == 0 ) return;
    if( field == _T( "图层" ) )
    {
        assign_const( this->m_layer, CString( pEnt->layer() ) );
    }
    else if( field == _T( "颜色" ) )
    {
        assign_const( this->m_colorIndex, ( int )( pEnt->colorIndex() ) );
    }
    else if( field == _T( "线型" ) )
    {
        assign_const( this->m_lineType, CString( pEnt->linetype() ) );
    }
    else if( field == _T( "线宽" ) )
    {
        assign_const( this->m_lineWeight, ArxUtilHelper::LineWeightToStr( pEnt->lineWeight() ) );
    }
    else
    {
        assertReadEnabled();
    }
}
void BaseEntity::onBeforeSetData( const CString& field, const CString& value )
{
    if( field == _T( "图层" ) )
    {
    }
    else if( field == _T( "颜色" ) )
    {
    }
    else if( field == _T( "线型" ) )
    {
    }
    else if( field == _T( "线宽" ) )
    {
    }
    else
    {
        assertWriteEnabled();
    }
}
void BaseEntity::onAfterSetData( const CString& field, const CString& value )
{
    AcDbEntity* pEnt = AcDbEntity::cast( this );
    if( pEnt == 0 ) return;
    // 调用基类的虚函数
    BaseData::onAfterSetData( field, value );
    if( field == _T( "图层" ) )
    {
        pEnt->setLayer( ( LPCTSTR )( this->m_layer ) );
    }
    else if( field == _T( "颜色" ) )
    {
        pEnt->setColorIndex( this->m_colorIndex );
    }
    else if( field == _T( "线型" ) )
    {
        pEnt->setLinetype( ( LPCTSTR )( this->m_lineType ) );
    }
    else if( field == _T( "线宽" ) )
    {
        pEnt->setLineWeight( ArxUtilHelper::StrToLineWeight( this->m_lineWeight ) );
    }
}
