#pragma once
#include "Data.h"
// 圆周率
#ifndef PI
#define PI 3.1415926535897932384626433832795
#endif
/**
 * 带有数据字段功能的图元基类
 * Entity的派生类在构造函数中调用Data::map()方法定义字段,并将字段与成员变量关联在一起
 * 例如:
 * Entity* pEnt = GetEntity(); // 假设得到派生类图元指针
 * pEnt->set(_T("半径"), 50); // "半径" 与 派生类的成员变量 double m_radius 关联
 * AcGePoint3d pt;
 * pEnt->get(_T("坐标"), pt); // "坐标" 与 派生类的成员变量 double m_insertPt 关联
 * 参考: MineGE/JointGE类
 */
class ARXHELPER_API BaseEntity : public AcDbEntity, public BaseData
{
public:
    ACRX_DECLARE_MEMBERS( BaseEntity ) ;
public:
    // 虚拟析构函数
    virtual ~BaseEntity();
    // 重载Data类的虚函数
protected:
    // 获取真实的字段名(考虑分组的情况,字段名还包括了分组名)
    //virtual CString getRealField(const CString& field) const;
    // get函数调用前触发该消息
    virtual void onBeforeGetData( const CString& field ) const;
    // set函数调用前触发该消息
    virtual void onBeforeSetData( const CString& field, const CString& value );
    // set函数调用后触发该消息
    virtual void onAfterSetData( const CString& field, const CString& value );
protected:
    // 构造函数
    BaseEntity();
private:
    CString m_layer;       // 图层
    int m_colorIndex;      // 颜色(ACI格式)
    CString m_lineType;    // 线型
    CString m_lineWeight;  // 线宽
};
#ifdef ARXHELPER_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( BaseEntity )
#endif