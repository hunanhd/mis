#include "StdAfx.h"
#include "PropertyDataUpdater.h"
#include "MFCPropertyGridCtrlHelper.h"
#include "CustomProperties.h"
#include "HelperClass.h"
/*
static void BuildPropList2(MFCPropertyGridCtrlHelper& pgch,
                           const CString& name, const CString& value,
						   ClickPropModifyCallBack cb,
						   const FieldInfo& info)
{
	if(cb == 0) return;
	// 做一些限制，只可用于FieldInfo::DT_STRING, m_useList=false, bEnable=true的info
	DATA_TYPE dt = info.getDataType();
	bool m_useList = info.isDataListUse();
	bool bEnable = info.isEnable();
	if((dt == FieldInfo::DT_STRING) && !m_useList && bEnable)
	{
		// 添加点击类属性
		pgch.addClickProperty(name, value, cb);
	}
}
// 回调信息映射表
// 在"属性对话框"显示前构造映射表
// 然后传递给readDataFromGE()方法
// 由它负责根据字段名称查找对应的ClickPropModifyCallBackInfo
// 并传递给BuildPropList2()方法
std::map<CString, ClickPropModifyCallBack> cbInfoMaps;
bool ShaftPropertySet(const CString& oldValue, CString& newValue)
{
	ShaftExtraPropertyDlg sepd;
	if(sepd.DoModal() == IDOK)
	{
		// 使用oldValue做一些事情
		// ......
		// 返回新的结果
		newValue = xxx;
		return true;
	}
	else
	{
		return false;
	}
}
// 例如
// 传递ShaftPropertySet函数的地址(全局函数，静态函数，类静态成员函数)
// "井筒附加属性" --> {&ShaftPropertySet, "对话框设置井筒附加属性"}
// "xxx"            --> {cb2, "xdddsf"}
*/
static void BuildPropList( MFCPropertyGridCtrlHelper& pgch, const CString& name, const CString& value, const FieldInfo& info )
{
    FieldInfo::DATA_TYPE dt       = info.m_dt;
    int nMinValue      = info.m_minValue2;
    int nMaxValue      = info.m_maxValue2;
    double dMinValue   = info.m_minValue;
    double dMaxValue   = info.m_maxValue;
    int tole	       = info.m_tolrance;
    FieldInfo::LIST_TYPE lt       = info.m_lt;
    CString varName    = info.m_varName;
    unsigned int flag       = info.m_flag;
    bool bEnable = ( flag & FieldInfo::FT_EDIT ) ? true : false;
    bool bShow = ( flag & FieldInfo::FT_SHOW ) ? true : false;
    CString m_descr    = info.m_tooltips;
    LPCTSTR descr = NULL;
    if( m_descr.GetLength() != 0 ) descr = ( LPCTSTR )m_descr;
    switch( dt )
    {
    case FieldInfo::DT_STRING:
        pgch.addStringProperty( name, value, descr, bEnable, bShow );
        break;
    case FieldInfo::DT_INT:
        pgch.addIntProperty( name, _ttoi( value ), nMinValue, nMaxValue, descr, bEnable, bShow );
        break;
    case FieldInfo::DT_NUMERIC:
        //pgch.addDoubleProperty( name, _tstof( value ), dMinValue, dMaxValue, tole, descr, bEnable, bShow );
		pgch.addDoubleProperty( name, _tstof( value ), dMinValue, dMaxValue, 8, descr, bEnable, bShow );
        break;
    case FieldInfo::DT_BOOL:
        pgch.addBoolProperty( name, StringHelper::StringToBool( value ), descr, bEnable, bShow );
        break;
    case FieldInfo::DT_DATE:
    {
        COleDateTime dt;
        StringHelper::StringToDataTime( value, dt );
        pgch.addDateTimeProperty( name, dt, descr, bEnable, bShow );
    }
    break;
    case FieldInfo::DT_LIST:
        if( lt == FieldInfo::LT_STRING )
        {
            AcStringArray strList;
            if( StringListHelper::GetStringList( varName, strList ) )
            {
                pgch.addStringPropertyList( name, value, strList, descr, bEnable, bShow );
            }
            else
            {
                pgch.addStringProperty( name, value, descr, bEnable, bShow );
            }
        }
        else if( lt == FieldInfo::LT_INT )
        {
            AcDbIntArray intList;
            AcStringArray strList;
            if( IntStrListHelper::GetIntStrList( varName, intList, strList ) )
            {
                pgch.addIntPropertList( name, _ttoi( value ), intList, strList, descr, bEnable, bShow );
            }
            else
            {
                pgch.addStringProperty( name, value, descr, bEnable, bShow );
            }
        }
        break;
    }
}
static void BuildPropList( MFCPropertyGridCtrlHelper2& pgch, CMFCPropertyGridProperty* pGroup, const CString& name, const CString& value, const FieldInfo& info )
{
    FieldInfo::DATA_TYPE dt       = info.m_dt;
    int nMinValue      = info.m_minValue2;
    int nMaxValue      = info.m_maxValue2;
    double dMinValue   = info.m_minValue;
    double dMaxValue   = info.m_maxValue;
    FieldInfo::LIST_TYPE lt       = info.m_lt;
    CString varName    = info.m_varName;
    int tole	   = info.m_tolrance;
    unsigned int flag  = info.m_flag;
    bool bEnable = ( flag & FieldInfo::FT_EDIT ) ? true : false;
    bool bShow = ( flag & FieldInfo::FT_SHOW ) ? true : false;
    CString m_descr    = info.m_tooltips;
    LPCTSTR descr = NULL;
    if( m_descr.GetLength() != 0 ) descr = ( LPCTSTR )m_descr;
    switch( dt )
    {
    case FieldInfo::DT_STRING:
        pgch.addStringProperty( pGroup, name, value, descr, bEnable, bShow );
        break;
    case FieldInfo::DT_INT:
        pgch.addIntProperty( pGroup, name, _ttoi( value ), nMinValue, nMaxValue, descr, bEnable, bShow );
        break;
    case FieldInfo::DT_NUMERIC:
        //pgch.addDoubleProperty( pGroup, name, _tstof( value ), dMinValue, dMaxValue, tole, descr, bEnable, bShow );
		pgch.addDoubleProperty( pGroup, name, _tstof( value ), dMinValue, dMaxValue, 8, descr, bEnable, bShow );
        break;
    case FieldInfo::DT_BOOL:
        pgch.addBoolProperty( pGroup, name, StringHelper::StringToBool( value ), descr, bEnable, bShow );
        break;
    case FieldInfo::DT_DATE:
    {
        COleDateTime dt;
        StringHelper::StringToDataTime( value, dt );
        pgch.addDateTimeProperty( pGroup, name, dt, descr, bEnable, bShow );
    }
    break;
    case FieldInfo::DT_LIST:
        if( lt == FieldInfo::LT_STRING )
        {
            AcStringArray strList;
            if( StringListHelper::GetStringList( varName, strList ) )
            {
                pgch.addStringPropertyList( pGroup, name, value, strList, descr, bEnable, bShow );
            }
            else
            {
                pgch.addStringProperty( pGroup, name, value, descr, bEnable, bShow );
            }
        }
        else if( lt == FieldInfo::LT_INT )
        {
            AcDbIntArray intList;
            AcStringArray strList;
            if( IntStrListHelper::GetIntStrList( varName, intList, strList ) )
            {
                pgch.addIntPropertList( pGroup, name, _ttoi( value ), intList, strList, descr, bEnable, bShow );
            }
            else
            {
                pgch.addStringProperty( pGroup, name, value, descr, bEnable, bShow );
            }
        }
        break;
    }
}

static void PrintPropList( CMFCPropertyGridCtrl* pPropDataList )
{
    int count = pPropDataList->GetPropertyCount();
    acutPrintf( _T( "\n属性数据控件包含的数据个数:%d" ), count );
    for( int i = 0; i < count; i++ )
    {
        CMFCPropertyGridProperty* pProp = pPropDataList->GetProperty( i );
        acutPrintf( _T( "\name=%s\tvalue=%s" ), pProp->GetName(), pProp->GetValue() );
    }
    acutPrintf( _T( "\n" ) );
}
static void SetPropValue( CMFCPropertyGridProperty* pProp, const FieldInfo& info, const CString& value )
{
    switch( info.m_dt )
    {
    case FieldInfo::DT_STRING:
        pProp->SetValue( ( COleVariant )value );
        break;
    case FieldInfo::DT_INT:
        if ( _ttoi( value ) < info.m_minValue2 || _ttoi( value ) > info.m_maxValue2 )
        {
            pProp->SetValue( ( long ) info.m_minValue2 );
        }
        else
        {
            pProp->SetValue( ( long )_ttoi( value ) );
        }
        break;
    case FieldInfo::DT_NUMERIC:
        if ( _ttoi( value ) < info.m_minValue || _ttoi( value ) > info.m_maxValue )
        {
            pProp->SetValue( ( long ) info.m_minValue );
        }
        else
        {
            pProp->SetValue( _tstof( value ) );
        }
        break;
    case FieldInfo::DT_BOOL:
        pProp->SetValue( ( long )( StringHelper::StringToBool( value ) ? 1 : 0 ) );
        break;
    case FieldInfo::DT_DATE:
    {
        COleDateTime dt;
        StringHelper::StringToDataTime( value, dt );
        pProp->SetValue( ( COleVariant )dt );
    }
    break;
    case FieldInfo::DT_LIST:
        if( info.m_lt == FieldInfo::LT_STRING )
        {
            pProp->SetValue( ( COleVariant )value );
        }
        else if( info.m_lt == FieldInfo::LT_INT )
        {
            pProp->SetValue( ( long )_ttoi( value ) );
        }
        break;
    default:
        pProp->SetValue( ( COleVariant )value ); // 默认为字符串类型
        break;
    }
	/**
	 * bug: 属性对话框中数据设为0或空值,保存不上!(fixed by dlj, 2017/1/7)
	 * 问题:  
	 * BuildPropList( pgch, pGroup, field, _T( "" ), info ); // 赋予空字符串
	 * 传入的是一个空值,导致CMFCPropertyGridProperty对象的originValue是0或空值
	 * 如果我们输入0或空值,则表示属性没有被modified,也就不会更新和保存了!
	 */
	pProp->SetOriginalValue(pProp->GetValue());
}
bool PropertyDataUpdater::BuildPropGridCtrlByFunc( CMFCPropertyGridCtrl* pPropDataList, const CString& type, const AcStringArray& funcFieldsInfo )
{
    if( pPropDataList == NULL ) return false;
    if( type.GetLength() == 0 ) return false;
    // 清空已添加的属性
    pPropDataList->RemoveAll();
    //acutPrintf(_T("\n清空所有属性..."));
    int funcCount = 0;
    AcStringArray funcs;
    for ( int i = 0; i < funcFieldsInfo.length(); i++ )
    {
        if ( _T( "$" ) == funcFieldsInfo[i] )
        {
            funcCount += 1;
            funcs.append( funcFieldsInfo[i + 1] );
        }
    }
    for( int i = 0; i < funcCount; i++ )
    {
        CString func = funcs[i].kACharPtr();
        CMFCPropertyGridProperty* pGroup = new CMFCPropertyGridProperty( func );
        pPropDataList->AddProperty( pGroup );
    }
    MFCPropertyGridCtrlHelper2 pgch( pPropDataList );
    for( int i = 0; i < funcCount; i++ )
    {
        CString func = funcs[i].kACharPtr();
        int indx = funcFieldsInfo.find( func );
        CMFCPropertyGridProperty* pGroup = pPropDataList->GetProperty( i );
        for( int j = indx + 1; j < funcFieldsInfo.length(); j++ )
        {
            if( _T( "$" ) == funcFieldsInfo[j] ) break;
            CString field = funcFieldsInfo[j].kACharPtr();
            // 构造真实的字段名
            CString hybirdField = FieldHelper::MakeHybirdField( func, field );
            // 读取字段信息
            FieldInfo info; // 默认设置(FieldInfo::DT_STRING, m_enable=true, m_descr =_T(""))
            FieldInfoHelper::GetFieldInfo( type, hybirdField, info );
            // 构建PropertyList(注意:字段名显示的显示用的是field，而不是hybirdField)
            BuildPropList( pgch, pGroup, field, _T( "" ), info ); // 赋予空字符串
        }
    }
    return true;
}
bool PropertyDataUpdater::ReadDataFromGEByFunc( CMFCPropertyGridCtrl* pPropDataList, const AcDbObjectId& objId )
{
    if( pPropDataList == NULL ) return false;
    if( objId.isNull() ) return false;
    //PrintPropList(pPropDataList);
    // 获取图元类型
    CString type;
    ArxDataTool::GetTypeName( objId, type );
    // 读取属性数据并填充内容
    BaseDao* dao = ExtDictData::CreateDao( objId );
    if( dao == 0 ) return false;
    //经过测试，这个地方的的个数是群组的个数，就是树根的个数
    int nCount = pPropDataList->GetPropertyCount();
    for ( int i = 0; i < nCount; i++ )
    {
        CMFCPropertyGridProperty* pGroup = pPropDataList->GetProperty( i );
        //acutPrintf(_T("\n属性个数:%d"),pGroup->GetSubItemsCount());
        for( int j = 0; j < pGroup->GetSubItemsCount(); j++ )
        {
            CMFCPropertyGridProperty* pProp = pGroup->GetSubItem( j );
			//if( pProp->IsModified() )
            // 构造字段的实际名称
            CString hybirdField = FieldHelper::MakeHybirdField( pGroup->GetName(), pProp->GetName() );
            //acutPrintf(_T("\n功能:%s->字段:%s->值:%s"),pGroup->GetName(),pProp->GetName(),value);
            FieldInfo info; // 默认设置(FieldInfo::DT_STRING, m_enable=true, m_descr =_T(""))
            FieldInfoHelper::GetFieldInfo( type, hybirdField, info );
            CString value;
            dao->getString( hybirdField, value ); // 读取属性数据
            SetPropValue( pProp, info, value ); // 设置属性值
        }
    }
    // 析构dao指针
    delete dao;
    return true;
}
bool PropertyDataUpdater::WriteDataToGEByFunc( CMFCPropertyGridCtrl* pPropDataList, const AcDbObjectId& objId )
{
    if( pPropDataList == NULL ) return false;
    if( objId.isNull() ) return false;
    // 没有属性数据可更新
    if( pPropDataList->GetPropertyCount() == 0 ) return false;
    // 获取图元类型
    CString type;
    ArxDataTool::GetTypeName( objId, type );
    // 创建dao指针用于操作扩展数据
    BaseDao* dao = ExtDictData::CreateDao( objId );
    if( dao == 0 ) return false;
    int nCount = pPropDataList->GetPropertyCount();
    for ( int i = 0; i < nCount; i++ )
    {
        CMFCPropertyGridProperty* pGroup = pPropDataList->GetProperty( i );
        //acutPrintf(_T("\n属性个数:%d"),pGroup->GetSubItemsCount());
        for( int j = 0; j < pGroup->GetSubItemsCount(); j++ )
        {
            CMFCPropertyGridProperty* pProp = pGroup->GetSubItem( j );
			CString value = pProp->GetValue();
			if(-1 == value.Find(_T("/")))
			{
				if( !pProp->IsModified() ) continue;
			}
            // 构造字段的实际名称
            CString hybirdField = FieldHelper::MakeHybirdField( pGroup->GetName(), pProp->GetName() );
            
            /* 特殊处理浮点数属性,内部仍然是按照小数点6位存储(%f格式) */
            NumericProp* fProp = DYNAMIC_DOWNCAST( NumericProp, pProp );
            if( fProp != 0 )
            {
                double v = 0;
                StringHelper::StringToDouble( value, v );
                value.Format( _T( "%f" ), v );
            }
            //acutPrintf(_T("\n字段:%s"),value);
            //acutPrintf(_T("\n写数据功能:%s->字段:%s->值:%s"),pGroup->GetName(),pProp->GetName(),value);
            //FieldInfo info; // 默认设置(FieldInfo::DT_STRING, m_enable=true, m_descr =_T(""))
            //FieldInfoHelper::GetFieldInfo( type, hybirdField, info );
            bool ret = dao->setString( hybirdField, value ); // 更新属性数据
        }
    }
    // 析构dao指针
    delete dao;
    return true;
}

bool PropertyDataUpdater_NoGroup::BuildPropGridCtrl( CMFCPropertyGridCtrl* pPropDataList, const CString& type, const AcStringArray& fields )
{
    if( pPropDataList == NULL ) return false;
    if( type.GetLength() == 0 ) return false;
    if( fields.isEmpty() ) return false;
    // 清空已添加的属性
    pPropDataList->RemoveAll();
    //acutPrintf(_T("\n清空所有属性..."));
    // 创建MFCPropertyGridCtrlHelper对象
    // 同时初始化m_propertyDataList的属性
    // 参见MFCPropertyGridCtrlHelper的构造函数
    MFCPropertyGridCtrlHelper pgch( pPropDataList );
    int len = fields.length();
    for( int i = 0; i < len; i++ )
    {
        CString hybirdField = fields[i].kACharPtr();
        FieldInfo info; // 默认设置(FieldInfo::DT_STRING, m_enable=true, m_descr =_T(""))
        FieldInfoHelper::GetFieldInfo( type, hybirdField, info );
		CString name,func;
		FieldHelper::SplitHybirdField(hybirdField,func,name);
        // 构建PropertyList
        BuildPropList( pgch, name, _T( "" ), info ); // 赋予空字符串
    }
    //PrintPropList(pPropDataList);
    return true;
}
bool PropertyDataUpdater_NoGroup::ReadDataFromGE( CMFCPropertyGridCtrl* pPropDataList, const AcDbObjectId& objId )
{
    if( pPropDataList == NULL ) return false;
    if( objId.isNull() ) return false;
    //PrintPropList(pPropDataList);
	// 获取图元类型
	CString type;
	ArxDataTool::GetTypeName( objId, type );
    // 填充内容
    BaseDao* dao = ExtDictData::CreateDao( objId );
    if( dao == 0 ) return false;
    int nCount = pPropDataList->GetPropertyCount();
    for ( int i = 0; i < nCount; i++ )
    {
        CMFCPropertyGridProperty* pProp = pPropDataList->GetProperty( i );
        FieldInfo info; // 默认设置(FieldInfo::DT_STRING, m_enable=true, m_descr =_T(""))
		CString field = pProp->GetName();
		CString hybirdField = FieldHelper::GuessRealField(type,field);       
		FieldInfoHelper::GetFieldInfo( type, hybirdField, info );
        CString value;
        dao->getString( hybirdField, value ); // 读取属性数据
        SetPropValue( pProp, info, value ); // 设置属性值
    }
    delete dao;
    //PrintPropList(pPropDataList);
    return true;
}
bool PropertyDataUpdater_NoGroup::WriteDataToGE( CMFCPropertyGridCtrl* pPropDataList, const AcDbObjectId& objId )
{
    if( pPropDataList == NULL ) return false;
    if( objId.isNull() ) return false;
    // 没有属性数据可更新
    if( pPropDataList->GetPropertyCount() == 0 ) return false;
    // 获取图元类型
    CString type;
    ArxDataTool::GetTypeName( objId, type );
    BaseDao* dao = ExtDictData::CreateDao( objId );
    if( dao == 0 ) return false;
    int nCount = pPropDataList->GetPropertyCount();
    for ( int i = 0; i < nCount; i++ )
    {
        CMFCPropertyGridProperty* pProp = pPropDataList->GetProperty( i );
		if( !pProp->IsModified() ) continue;
        // 扩展数据中默认采用"字符串"类型保存数据
        // COleVariant会自动转换成CString
        CString value = pProp->GetValue();
        /* 特殊处理浮点数属性!!! */
        NumericProp* fProp = DYNAMIC_DOWNCAST( NumericProp, pProp );
        if( fProp != 0 )
        {
            double v = 0;
            StringHelper::StringToDouble( value, v );
            value.Format( _T( "%f" ), v );
        }
        CString field = pProp->GetName();
		CString hybirdField = FieldHelper::GuessRealField(type,field);
        //acutPrintf(_T("\nFieldName:%s"),field);
        //FieldInfo info; // 默认设置(FieldInfo::DT_STRING, m_enable=true, m_descr =_T(""))
        //FieldInfoHelper::GetFieldInfo( pDO->getType(), pProp->GetName(), info );
        bool ret = dao->setString( hybirdField, value ); // 更新属性数据
        //acutPrintf(_T("\n返回值:%s"),ret?_T("成功"):_T("失败"));
    }
    delete dao;
    return true;
}
