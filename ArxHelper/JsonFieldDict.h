#pragma once
/*
这个类仅用于测试json的用法,原本计划是用来替代基于arx词典的字段管理
但考虑到json字符串过大(图元和字段会很多),可能会导致一些额外的问题,
后续就不在继续实现和完善了,测试参见:
ArxHelper/acrxEntryPoint.cpp/JL_JsonTest()函数
json-cpp参考例子：
http://www.cppblog.com/wanghaiguang/archive/2013/12/26/205020.aspx
http://my.oschina.net/Tsybius2014/blog/289527
*/
#include <json/json.h>
/*
 * json格式的字段管理词典
 *
	 {
		 "巷道": {
			 "常用": {
				 "风量": {},
				 "风速": {},
				 "风阻": {}
			 },
			 "网络解算": {},
			 "瓦斯": {}
		 }
	 }
 */
class JsonFieldDict : public AcDbObject
{
public:
    ACRX_DECLARE_MEMBERS( JsonFieldDict ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    JsonFieldDict () ;
    virtual ~JsonFieldDict ();
    bool addField( const CString& type, const CString& field, const CString& func );
    bool removeField( const CString& type, const CString& field, const CString& func );
    void removeAllFields( const CString& type );
    void print();
    virtual Acad::ErrorStatus dwgOutFields ( AcDbDwgFiler* pFiler ) const ;
    virtual Acad::ErrorStatus dwgInFields ( AcDbDwgFiler* pFiler ) ;
public:
    Json::Value root;
} ;

#ifdef ARXHELPER_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( JsonFieldDict )
#endif
