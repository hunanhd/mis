#pragma once
#include "Data.h"
#include "ArxXRecordManager.h"
#include <json/json.h>
class DictDao : public BaseDao
{
public:
	DictDao( const AcDbObjectId& dictId );
	virtual ~DictDao();
protected:
	virtual bool set( const CString& key, const CString& value );
	virtual bool get( const CString& key, CString& value ) const;
	virtual bool add( const CString& key );
	virtual bool remove( const CString& key );
	virtual void clear();
private:
	AcDbObjectId m_dictId;
	AcDbDictionary* m_pDict;
	bool m_createNewKey;
};
class ExtDictDao : public DictDao
{
public:
	ExtDictDao( const AcDbObjectId& objId );
	virtual ~ExtDictDao();
private:
	AcDbObjectId m_objId;
};
class JsonDictDao : public BaseDao
{
public:
	JsonDictDao( const AcDbObjectId& dictId, const CString& user_key );
	virtual ~JsonDictDao();
protected:
	virtual bool set( const CString& key, const CString& value );
	virtual bool get( const CString& key, CString& value ) const;
	virtual bool add( const CString& key );
	virtual bool remove( const CString& key );
	virtual void clear();
private:
	AcDbObjectId m_dictId;
	AcDbXrecord* m_pXrec;
	ArxXRecordManager* m_pDem;
	Json::Value m_root;
	bool m_createNewKey;
	CString m_user_key;
};
class XDataDao : public BaseDao
{
public:
	XDataDao( const AcDbObjectId& objId );
	virtual ~XDataDao();
protected:
	virtual bool get( const CString& key, CString& value ) const;
	virtual bool set( const CString& key, const CString& value );
	virtual bool add( const CString& key );
	virtual bool remove( const CString& key );
	virtual void clear();
private:
	AcDbObjectId m_objId;
	AcDbObject* m_pObj;
};
class EntityDataDao : public BaseDao
{
public:
    // 通过类型名称动态创建图元
    EntityDataDao( const CString& clsname, AcDbObjectId& objId );
    EntityDataDao( const AcDbObjectId& objId );
    virtual ~EntityDataDao();
protected:
    virtual bool set( const CString& key, const CString& value );
    virtual bool get( const CString& key, CString& value ) const;
    virtual bool add( const CString& key );
    virtual bool remove( const CString& key );
	virtual void clear();
private:
	AcDbObjectId m_objId;
    BaseData* m_pData;
    AcDbObject* m_pObj; // 图元对象指针(可能是打开数据库得到的,也可能是新建的尚未添加到数据库中的图元指针)
    bool m_newEntity; // 是否新创建的图元(默认false)
    AcDbObjectId* m_pObjId; // 用于存储新建图元的id
};