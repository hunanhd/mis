#pragma once
#include "dlimexp.h"
#include "config.h"
#include <vector>
typedef std::vector<int> IntArray;
// 属性字段管理辅助类
class ARXHELPER_API FieldHelper
{
public:
    // 分组+字段名拼接、拆分操作
    static CString MakeHybirdField( const CString& func, const CString& field );            // 构造混合字段名
    static bool SplitHybirdField( const CString& hybirdField, CString& func, CString& field ); // 提取分组和字段名
    //字段操作
    static bool AddField( const CString& type, const CString& hybirdField );      // 添加字段
    static bool RemoveField( const CString& type, const CString& hybirdField );   // 删除字段
    static void RemoveAllFields( const CString& type );                           // 删除所有字段
    static void GetAllFields( const CString& type, AcStringArray& hybirdFields ); // 获取图元所有的字段
    static int CountFields( const CString& type );                                // 统计图元的字段个数
    static void GetAllRegTypes( AcStringArray& types );		                    // 获取当前已注册的图元类型
    static bool AddMoreFields( const CString& type, const AcStringArray& hybirdFields );      // 添加多个字段
    static bool RemoveMoreFields( const CString& type, const AcStringArray& hybirdFields );   // 删除多个字段
    // 根据简写的字段猜测包含分组的完整字段名
    static void GuessRealFields( const CString& type, const AcStringArray& fields, AcStringArray& hybirdFields );
    static CString GuessRealField( const CString& type, const CString& field );
};
