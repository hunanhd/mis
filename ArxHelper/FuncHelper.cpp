#include "StdAfx.h"
#include "FuncHelper.h"
#include "HelperClass.h"
//bool FuncHelper::RemoveFunction( const CString& func )
//{
//    if( StringHelper::IsEmptyString( f ) ) return false;
//
//    ArxDictHelper::RemoveKey( FUNC_FIELD_DICT, f );
//    return true;
//}
//bool FuncHelper::RemoveType( const CString& func, const CString& type )
//{
//    if( StringHelper::IsEmptyString( f ) || StringHelper::IsEmptyString( type ) ) return false;
//
//    AcStringArray allEntries;
//    ArxDictHelper::GetAllEntries( FUNC_FIELD_DICT, f, allEntries );
//
//    AcStringArray entries;
//    for( int i = 0; i < allEntries.length(); i++ )
//    {
//        int pos = allEntries[i].find( SEPARATOR );
//        if( pos == -1 ) continue;
//
//        if( allEntries[i].substr( 0, pos ).compareNoCase( type ) == 0 )
//        {
//            entries.append( allEntries[i] );
//        }
//    }
//
//    for( int i = 0; i < entries.length(); i++ )
//    {
//        ArxDictHelper::RemoveEntry( FUNC_FIELD_DICT, f, entries[i].kACharPtr() );
//    }
//
//    return true;
//}
bool FuncHelper::GetFuncs( const CString& type, AcStringArray& funcs )
{
    // FieldHelper::GetAllFields得到的字段名 包含了 分组
    AcStringArray hybirdFields;
    FieldHelper::GetAllFields( type, hybirdFields );
    int n = hybirdFields.length();
    for( int i = 0; i < n; i++ )
    {
        // 提取功能类型和字段
        CString func, field;
        FieldHelper::SplitHybirdField( hybirdFields[i].kACharPtr(), func, field );
        if( funcs.find( func ) < 0 )
        {
            funcs.append( func );
        }
    }
    return !funcs.isEmpty();
}
bool FuncHelper::GetTypes( const CString& func, AcStringArray& types )
{
    if( StringHelper::IsEmptyString( func ) ) return false;
    AcStringArray allTypes;
    FieldHelper::GetAllRegTypes( allTypes );
    int n = allTypes.length();
    for( int i = 0; i < n; i++ )
    {
        AcStringArray funcs;
        FuncHelper::GetFuncs( allTypes[i].kACharPtr(), funcs );
        if( funcs.find( func ) > -1 )
        {
            types.append( allTypes[i] );
        }
    }
    return !types.isEmpty();
}
bool FuncHelper::GetFields( const CString& type, const CString& func, AcStringArray& fields )
{
    if( StringHelper::IsEmptyString( func ) || StringHelper::IsEmptyString( type ) ) return false;
    // FieldHelper::GetAllFields得到的字段名 包含了 分组
    AcStringArray hybirdFields;
    FieldHelper::GetAllFields( type, hybirdFields );
    int n = hybirdFields.length();
    for( int i = 0; i < n; i++ )
    {
        // 提取功能类型和字段
        CString geFunc, geField;
        FieldHelper::SplitHybirdField( hybirdFields[i].kACharPtr(), geFunc, geField );
        if( geFunc == func )
        {
            fields.append( geField );
            //acutPrintf(_T("\n%s-->%s"), hybirdFields[i].kACharPtr(), geField);
        }
    }
    return !fields.isEmpty();
}