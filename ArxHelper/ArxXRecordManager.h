#pragma once
#include "dlimexp.h"
class ARXHELPER_API ArxXRecordManager
{
public:
    ArxXRecordManager( AcDbXrecord* pXrec, bool auto_close = true );
    ~ArxXRecordManager();
    int addEntry( const CString& entry );
    int removeEntry( const CString& entry );
    bool modifyEntry( int index, const CString& newEntry );
    bool getEntry( int index, CString& entry );
    int findEntry( const CString& entry );
    int countEntries();
    void getAllEntries( AcStringArray& entries );
private:
    AcDbXrecord* m_pXrec;
    resbuf* m_pHead;
    bool m_auto_close; // 是否自动关闭扩展记录对象m_pXrec(默认true)
};