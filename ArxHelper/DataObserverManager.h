#pragma once
#include "dlimexp.h"
// sigslot不使用多线程(arx中使用多线程就会崩溃!)
#ifndef SIGSLOT_PURE_ISO
#define SIGSLOT_PURE_ISO
#endif // SIGSLOT_PURE_ISO
#include "Tool/sigslot.h"
using namespace sigslot;
class ARXHELPER_API DataObserverManager
{
public:
	signal1<const AcDbObjectId&> signalDictModify; // 词典被修改
	signal1<const AcDbObjectId&> signalExtDictModify; // 扩展词典被修改
	signal1<const AcDbObjectId&> signalJsonDictModify; // json词典被修改
	signal1<const AcDbObjectId&> signalXDataModify; // 扩展数据被修改
	signal1<const AcDbObjectId&> signalEntityDataModify; // 图元几何参数被修改
public:
	static void Init();
	static void UnInit();
	static DataObserverManager& getSingleton( void );
	static DataObserverManager* getSingletonPtr( void );
public:
	DataObserverManager();
	~DataObserverManager();
private:
	static DataObserverManager* ms_Singleton;
};