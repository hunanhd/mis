#pragma once
#include "dlimexp.h"
#include <vector>
#include <string>
typedef std::vector<std::string> StringArray;
typedef std::vector<CString> MFCStringArray;
class ARXHELPER_API AcStringHelper
{
public:
    static void R2S( const AcStringArray& a, StringArray& v );
    static void S2R( const StringArray& v, AcStringArray& a );
    static void R2CS( const AcStringArray& a, MFCStringArray& v );
    static void CS2R( const MFCStringArray& v, AcStringArray& a );
    static void R2C( const AcStringArray& a, CStringArray& v );
    static void C2R( const CStringArray& v, AcStringArray& a );
};
