#include "StdAfx.h"
#include "ArxDwgHelper.h"
#include "HelperClass.h"
#include "Tool/HelperClass.h"
// insert函数的bug???
// http://forums.autodesk.com/t5/Autodesk-ObjectARX/Problem-with-inserting-blocks-from-other-DWG/m-p/2846814#M27764
// http://bbs.mjtd.com/thread-27378-1-1.html
// http://www.mouldsky.com/archiver/tid-66211.html
// 1) 它把原来的块复制了一份，尽管指定了新的块名，但新的块定义中根本就没有数据!!!
// 2) 每执行一次，它都会将所有的块复制1份???
// http://hi.baidu.com/slyrabbit/blog/item/447b2aff0b69623b5c600883.html (解决!!!)
// 从dwg文件中读取所有的块
static void ReadAllBlocks( AcDbDatabase* pDB, AcStringArray& allBlocks )
{
    AcDbBlockTable* pBlkTbl;
    if( Acad::eOk != pDB->getBlockTable( pBlkTbl, AcDb::kForRead ) ) return;
    AcDbBlockTableIterator* pBlockTableIterator;
    pBlkTbl->newIterator( pBlockTableIterator );
    for ( ; !pBlockTableIterator->done(); pBlockTableIterator->step() )
    {
        AcDbBlockTableRecord* pRecord;
        if( Acad::eOk == pBlockTableIterator->getRecord( pRecord, AcDb::kForRead ) )
        {
            AcString str;
            if( Acad::eOk == pRecord->getName( str ) )
            {
                // 排除所有*开头的块定义
                //    1) 系统自带的3个默认块(模型空间、图纸空间)
                //    2) 使用标注的时候，也会生成带有*号的块
                if( str.mid( 0, 1 ) == _T( "*" ) ) continue;
                if( str.mid( 0, 1 ) == _T( "$" ) ) continue;
                if( str.mid( 0, 2 ) == _T( "A$" ) ) continue;
                allBlocks.append( str );
            }
            pRecord->close();
        }
    }
    delete pBlockTableIterator;
    pBlkTbl->close();
}
static void GetBlcokIds( AcDbDatabase* pDB, const AcStringArray& allBlocks, AcDbObjectIdArray& blockIds )
{
    int n = allBlocks.length();
    for( int i = 0; i < n; i++ )
    {
        blockIds.append( AcDbObjectId::kNull );
    }
    AcDbBlockTable* pBlkTbl;
    if( Acad::eOk != pDB->getBlockTable( pBlkTbl, AcDb::kForRead ) ) return;
    for( int i = 0; i < n; i++ )
    {
        AcDbBlockTableRecord* pRecord;
        if( Acad::eOk == pBlkTbl->getAt( allBlocks[i], pRecord, AcDb::kForRead ) )
        {
            AcString blkName;
            pRecord->getName( blkName );
            int pos = allBlocks.find( blkName );
            if( pos != -1 )
            {
                blockIds[i] = pRecord->objectId();
            }
            pRecord->close();
        }
    }
    pBlkTbl->close();
}
static void ReadIcons( AcDbDatabase* pDB, IconMap& icons )
{
    AcDbBlockTable* pBlkTbl;
    if( Acad::eOk != pDB->getBlockTable( pBlkTbl, AcDb::kForRead ) ) return;
    AcDbBlockTableIterator* pBlockTableIterator;
    pBlkTbl->newIterator( pBlockTableIterator );
    for ( ; !pBlockTableIterator->done(); pBlockTableIterator->step() )
    {
        AcDbBlockTableRecord* pRecord;
        if( Acad::eOk == pBlockTableIterator->getRecord( pRecord, AcDb::kForRead ) )
        {
            AcString str;
            if( Acad::eOk == pRecord->getName( str ) )
            {
                // 排除所有*开头的块定义
                //    1) 系统自带的3个默认块(模型空间、图纸空间)
                //    2) 使用标注的时候，也会生成带有*号的块
                if( str.mid( 0, 1 ) != _T( "*" ) && pRecord->hasPreviewIcon() )
                {
                    AcDbBlockTableRecord::PreviewIcon previewIcon;
                    pRecord->getPreviewIcon( previewIcon );
                    icons[str.kACharPtr()] = previewIcon;
                }
            }
            pRecord->close();
        }
    }
    delete pBlockTableIterator;
    pBlkTbl->close();
}
bool ArxDwgHelper::MergeBlocks( const CString& dwgFilePath )
{
    // 读取模板dwg文件到一个空数据库
    AcDbDatabase db( false );
    if( Acad::eOk != db.readDwgFile( dwgFilePath ) ) return false;
    // 读取模板dwg文件中所有的不是*开头的块定义
    AcStringArray allBlocks;
    ReadAllBlocks( &db, allBlocks );
    // 读取模板dwg文件中的块定义id
    AcDbObjectIdArray oldBlockIds;
    GetBlcokIds( &db, allBlocks, oldBlockIds );
    // 当前打开的数据库
    AcDbDatabase* pCurDB = acdbHostApplicationServices()->workingDatabase();
    AcDbIdMapping idMap;
    idMap.setDestDb( pCurDB );
    return ( Acad::eOk ==
             pCurDB->wblockCloneObjects( oldBlockIds, pCurDB->blockTableId(), idMap, AcDb::kDrcReplace ) );
}
void ArxDwgHelper::UpdateDwg()
{
    acedCommand( RTSTR, _T( "REGEN" ), 0 ); // 执行regen命令
}
bool ArxDwgHelper::GetAllBlockNames( const CString& dwgFilePath, AcStringArray& allBlocks )
{
    // 读取模板dwg文件到一个空数据库
    AcDbDatabase db( false );
    if( Acad::eOk != db.readDwgFile( dwgFilePath ) ) return false;
    // 读取模板dwg文件中所有的不是*开头的块定义
    ReadAllBlocks( &db, allBlocks );
    return true;
}
bool ArxDwgHelper::GetBlockIcons( const CString& dwgFilePath, IconMap& icons )
{
    // 读取模板dwg文件到一个空数据库
    AcDbDatabase db( false );
    if( Acad::eOk != db.readDwgFile( dwgFilePath ) ) return false;
    // 读取模板dwg文件中所有的不是*开头的块定义
    ReadIcons( &db, icons );
    return true;
}
CString ArxDwgHelper::GetDwgFileName( AcApDocument* pDoc )
{
    return CString( pDoc->fileName() );
}
bool ArxDwgHelper::IsNewDwg( AcApDocument* pDoc )
{
    /*
     * 通过文件名的路径来判断是否新建dwg文件
     * (1) 如果是新建的dwg,尚未保存,那么buffer路径是一个*.dwt模板文件
     * (2) 如果是一个已保存的dwg文件,那么buffer路径是一个dxf文件
     */
    const TCHAR* buffer = 0;
    pDoc->database()->getFilename( buffer );
    //acutPrintf( _T( "\nArxDwgHelper::IsNewDwg() --> %s" ), buffer );
    CString ext = PathHelper::GetExtension( buffer );
    return ( 0 == ext.CompareNoCase( _T( "dwt" ) ) );
}
CString ArxDwgHelper::GetDwgDirectory( AcApDocument* pDoc )
{
    if( ArxDwgHelper::IsNewDwg( pDoc ) )
    {
        //return PathHelper::GetTempPath();
        //return PathHelper::GetModuleDir(_hdllInstance);
        return _T( "" );
    }
    else
    {
        return PathHelper::GetDirectory( pDoc->fileName() );
    }
}
static bool IsBlock( const CString& strBlkName, AcDbObjectId& blkDefId )
{
    // 获得块定义名称
    CString strBlkDef = strBlkName;
    // 获得当前数据库的块表
    AcDbBlockTable* pBlkTbl;
    acdbHostApplicationServices()->workingDatabase()->getBlockTable( pBlkTbl, AcDb::kForWrite );
    // 查找用户指定的块定义是否存在
    if ( !pBlkTbl->has( strBlkDef ) )
    {
        //acutPrintf(_T("\n 当前图形中未包含指定名称的块定义！"));
        pBlkTbl->close();
        return false;
    }
    else
    {
        acutPrintf( _T( "\n 当前图形中找到！" ) );
        // 获得用户指定的块表记录
        pBlkTbl->getAt( strBlkDef, blkDefId );
        acutPrintf( _T( "\n ID = %x！" ), blkDefId );
        pBlkTbl->close();
        return true;
    }
}
// ObjectARX 删除块以及块参照
// 参考: http://blog.csdn.net/zsq597695/article/details/7800746
static void DeleteBlockRefByBlockName( const CString& strBlockName )
{
    AcDbObjectId blkId;
    bool bBlock = IsBlock( strBlockName, blkId );
    if( !bBlock ) return;
    //acutPrintf(_T("\n bBlock = %d"),bBlock);
    // 获得当前图形数据库的块表
    AcDbBlockTable* pBlkTbl;
    acdbHostApplicationServices()->workingDatabase() ->getBlockTable( pBlkTbl, AcDb::kForWrite );
    // 打开模型空间
    AcDbBlockTableRecord* pBlkTblRcd = NULL;
    pBlkTbl->getAt( ACDB_MODEL_SPACE, pBlkTblRcd, kForRead );
    pBlkTbl->close();
    // 遍历所有的图元
    AcDbBlockTableRecordIterator* pIte = NULL;
    Acad::ErrorStatus es = pBlkTblRcd->newIterator( pIte );
    AcDbEntity* pEnt = NULL;
    int i = 0;
    for( pIte->start(); !pIte->done(); pIte->step() )
    {
        i++;
        //acutPrintf(_T("\n i = %d"),i);
        pIte->getEntity( pEnt, AcDb::kForWrite );
        if( pEnt->isKindOf( AcDbBlockReference::desc() ) )
        {
            AcDbBlockReference* pRef = AcDbBlockReference::cast( pEnt );
            if( pRef->blockTableRecord() == blkId )
            {
                //es = pEnt->setColorIndex(1);
                pEnt->erase();
            }
        }
        pEnt->close();
    }
    delete pIte;
    pBlkTblRcd->close();
}
void ArxDwgHelper::EraseBlocks( const AcStringArray& allBlocks )
{
    // 删除块有风险!!!
}

// 选择dwg图形文件
bool ArxDwgHelper::SelectDwg( const CString& msg, CString& dwgFilePath )
{
	struct resbuf* rb = acutNewRb( RTSTR );
	bool ret = ( RTNORM == acedGetFileD( msg, NULL, _T( "dwg" ), 0, rb ) );
	if( ret )
	{
		dwgFilePath = rb->resval.rstring;
	}
	acutRelRb( rb ); // 必须要释放占用的内存
	return ret;
}

// 从dwg文件中的"巷道"图层读取所有的直线始末坐标
bool ArxDwgHelper::ReadAllLinesFromDwg( const CString& dwgFilePath, const CString& layerName, AcGePoint3dArray& sptArray, AcGePoint3dArray& eptArray )
{
	AcDbDatabase db( false );
	if( Acad::eOk != db.readDwgFile( dwgFilePath ) ) return false;

	// 获得名称为Blk的块表记录
	AcDbBlockTable* pBlkTbl;
	if ( Acad::eOk != db.getBlockTable( pBlkTbl, AcDb::kForRead ) )
	{
		acedAlert( _T( "获得块表失败!" ) );
		return false;
	}
	AcDbBlockTableRecord* pBlkTblRcd;
	Acad::ErrorStatus es = pBlkTbl->getAt( ACDB_MODEL_SPACE, pBlkTblRcd, AcDb::kForRead );
	pBlkTbl->close();
	if ( Acad::eOk != es )
	{
		acedAlert( _T( "打开模型空间块表记录失败!" ) );
		return false;
	}

	// 创建块表记录遍历器
	AcDbBlockTableRecordIterator* pItr;
	if ( Acad::eOk != pBlkTblRcd->newIterator( pItr ) )
	{
		acedAlert( _T( "创建遍历器失败!" ) );
		pBlkTblRcd->close();
		return false;
	}

	for ( pItr->start(); !pItr->done(); pItr->step() )
	{
		AcDbEntity* pEnt;
		if( Acad::eOk != pItr->getEntity( pEnt, AcDb::kForRead ) ) continue;

		if( layerName.CompareNoCase( pEnt->layer() ) == 0 ) // 只选择指定图层
		{
			AcDbLine* pLine = AcDbLine::cast( pEnt );   // 只选择直线
			if( pLine != 0 )
			{
				// 获取直线的始末点坐标
				AcGePoint3d spt, ept;
				pLine->getStartPoint( spt );
				pLine->getEndPoint( ept );

				// !!![修正]!!!
				//		目前不考虑3维情况，强制所有坐标z=0
				spt.z = 0;
				ept.z = 0;

				if( spt != ept ) // 排除始末点相同的直线
				{
					sptArray.append( spt );
					eptArray.append( ept );
				}
			}
		}
		pEnt->close();
	}
	delete pItr;
	pBlkTblRcd->close();

	//acutPrintf(_T("\n坐标个数:%d"), sptArray.length());
	return true;
}