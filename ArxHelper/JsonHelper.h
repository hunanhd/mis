#pragma once
#include "dlimexp.h"
#define NULL_JSON_STRING _T("{}")
class ARXHELPER_API JsonHelper
{
public:
    // 将字段和值格式化成json字符串
    static bool Format( const AcStringArray& fields, const AcStringArray& values, CString& json_str );
    // 从json字符串中解析字段和值
    static bool Parse( const CString& json_str, AcStringArray& fields, AcStringArray& values );
};