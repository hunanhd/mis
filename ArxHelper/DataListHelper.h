#pragma once
#include "dlimexp.h"
class ARXHELPER_API StringListHelper
{
public:
    static void GetAllNames( AcStringArray& names );
    static bool AddString( const CString& name, const CString& strValue );	// 实现效率较低
    static bool AddStringList( const CString& name, const AcStringArray& strList );
    static bool GetStringList( const CString& name, AcStringArray& strList );
    static void RemoveStringList( const CString& name );
};
class ARXHELPER_API IntStrListHelper
{
public:
    static void GetAllNames( AcStringArray& names );
    static bool AddIntStrPair( const CString& name, int intValue, const CString& strValue );	// 实现效率较低
    static bool AddIntStrList( const CString& name, const AcDbIntArray& intList, const AcStringArray& strList );
    static bool GetIntStrList( const CString& name, AcDbIntArray& intList, AcStringArray& strList );
    static void RemoveIntStrList( const CString& name );
};
