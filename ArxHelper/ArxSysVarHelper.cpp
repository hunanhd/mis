#include "StdAfx.h"
#include "ArxSysVarHelper.h"
#include "RawResbufHelper.h"
int ArxSysVarHelper::Get(LPCTSTR varName, int& val)
{
	resbuf rb;
	if (acedGetVar(varName, &rb)== RTNORM) {
		ASSERT(rb.restype == RTSHORT);
		val = rb.resval.rint;
		return(Acad::eOk);
	}
	else
		return(Acad::eInvalidInput);
}
int ArxSysVarHelper::Get(LPCTSTR varName, double& val)
{
	resbuf rb;
	if (acedGetVar(varName, &rb) == RTNORM) {
		ASSERT(rb.restype == RTREAL);
		val = rb.resval.rreal;
		return(Acad::eOk);
	}
	else
		return(Acad::eInvalidInput);
}
int ArxSysVarHelper::Get(LPCTSTR varName, CString& val)
{
	resbuf rb;
	if (acedGetVar(varName, &rb) == RTNORM) {
		ASSERT(rb.restype == RTSTR);
		val = rb.resval.rstring;
		free(rb.resval.rstring);
		return(Acad::eOk);
	}
	else
		return(Acad::eInvalidInput);
}
int ArxSysVarHelper::Get(LPCTSTR varName, AcGePoint2d& val)
{
	resbuf rb;
	if (acedGetVar(varName, &rb) == RTNORM) {
		ASSERT(rb.restype == RTPOINT);
		val.set(rb.resval.rpoint[X], rb.resval.rpoint[Y]);
		return(Acad::eOk);
	}
	else
		return(Acad::eInvalidInput);
}
int ArxSysVarHelper::Get(LPCTSTR varName, AcGePoint3d& val)
{
	resbuf rb;
	if (acedGetVar(varName, &rb) == RTNORM) {
		ASSERT(rb.restype == RT3DPOINT);
		val.set(rb.resval.rpoint[X], rb.resval.rpoint[Y], rb.resval.rpoint[Z]);
		return(Acad::eOk);
	}
	else
		return(Acad::eInvalidInput);
}
int ArxSysVarHelper::Set(LPCTSTR varName, int val)
{
	ASSERT(varName != NULL);
	resbuf rb;
	rb.restype = RTSHORT;
	rb.resval.rint = val;
	return (acedSetVar(varName, &rb));
}
int ArxSysVarHelper::Set(LPCTSTR varName, double val)
{
	ASSERT(varName != NULL);
	resbuf rb;
	rb.restype = RTREAL;
	rb.resval.rreal = val;
	return (acedSetVar(varName, &rb));
}
int ArxSysVarHelper::Set(LPCTSTR varName, LPCTSTR val)
{
	ASSERT(varName != NULL);
	resbuf rb;
	rb.restype = RTSTR;
	rb.resval.rstring = const_cast<TCHAR*>(val);
	return (acedSetVar(varName, &rb));
}
int ArxSysVarHelper::Set(LPCTSTR varName, const AcGePoint3d& val)
{
	ASSERT(varName != NULL);
	resbuf rb;
	rb.restype = RT3DPOINT;
	copyAdsPt(rb.resval.rpoint, asDblArray(val));
	return (acedSetVar(varName, &rb));
}
int ArxSysVarHelper::Set(LPCTSTR varName, const AcGePoint2d& val)
{
	ASSERT(varName != NULL);
	resbuf rb;
	rb.restype = RTPOINT;
	copyAdsPt(rb.resval.rpoint, asDblArray(val));
	return (acedSetVar(varName, &rb));
}