#include "StdAfx.h"
#include "BuiltinDataManager.h"
#include "AcDbObjectData.h"
#include "BuiltinData.h"
// 初始化静态成员变量
template<> BuiltinDataManager* Singleton<BuiltinDataManager>::ms_Singleton = 0;
BuiltinDataManager::BuiltinDataManager()
{
}
BuiltinDataManager::~BuiltinDataManager()
{
    for( BuiltinDataMap::iterator itr = m_dataMap.begin(); itr != m_dataMap.end(); ++itr )
    {
        delete itr->second;
    }
    m_dataMap.clear();
}
void BuiltinDataManager::regData( const CString& clsname, AcDbObjectData* pData )
{
    m_dataMap.insert( BuiltinDataMap::value_type( clsname, pData ) );
}
AcDbObjectData* BuiltinDataManager::getData( const CString& clsname )
{
    BuiltinDataMap::iterator itr = m_dataMap.find( clsname );
    if( itr == m_dataMap.end() )
    {
        return 0;
    }
    else
    {
        return itr->second;
    }
}
// 注册所有的内置图元数据对象
extern void BuiltinDataReg();
void BuiltinDataManager::Init()
{
    // 注册内置图元数据管理器
    BuiltinDataManager* bdm = new BuiltinDataManager();
    // 注册所有的内置图元数据
    BuiltinDataReg();
}
void BuiltinDataManager::UnInit()
{
    // 删除内置图元数据管理器
    delete BuiltinDataManager::getSingletonPtr();
}