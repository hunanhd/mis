#pragma once
#include "dlimexp.h"
// ARX工具类
class ARXHELPER_API ArxUtilHelper
{
public:
    // 将新建的图元添加到CAD图形数据库
    static bool PostToModelSpace( AcDbEntity* pEnt );
	// 关闭文档(不提示)
	static void CloseDocument( AcApDocument* pDoc, bool needPrompt = false );
    // 命令交互选择一个实体
    static AcDbObjectId SelectEntity( const CString& msg );
    // 命令交互选择多个实体
    static void SelectMoreEntity( const CString& msg, AcDbObjectIdArray& objIds );
    // 获取当前已选择的图元(PickSet)
    static void GetPickSetEntity( AcDbObjectIdArray& objIds );
    static void GetPickSetEntity2( AcDbObjectIdArray& objIds );
    // 计算两点构成的向量夹角
    static double AngleOfTwoPoint( const AcGePoint3d& startPt, const AcGePoint3d& endPt );
    // 计算向量v与x轴逆时针旋转的角度
    static double AngleToXAxis( const AcGeVector3d& v );
    // 交互选择
    static bool GetPoint( const CString& msg, AcGePoint3d& pt, const AcGePoint3d& basePt = AcGePoint3d::kOrigin );
    static bool GetTwoPoint( AcGePoint3d& startPt, AcGePoint3d& endPt );
    static bool GetString( const CString& msg, CString& str );
    static bool GetInt( const CString& msg, int& v );
    static bool GetReal( const CString& msg, double& v );
    static bool GetAngle( const AcGePoint3d& pt, const CString& msg, double& angle );
    // 暂停
    static void Pause( const CString& msg = _T( "\n请按回车或空格键结束..." ) );
	// 按n次ESC键
	static void Esc(int n=1);
    // 是否指定类型的图元
    static bool IsEqualType( const CString& type, const AcDbObjectId& objId, bool isDerivedFromParent = true );
    // 临时用一种颜色显示一个图元
    // 会话结束后，恢复原有颜色
    static void ShowEntityWithColor( const AcDbObjectId& objId, unsigned short colorIndex );
    // 临时用一种颜色显示多个图元
    // 会话结束后，恢复原有颜色
    static void ShowEntitiesWithColor( AcDbObjectIdArray& objIds, unsigned short colorIndex );
    // 临时用多种颜色显示多个图元
    // 会话结束后，恢复原有颜色
    static void ShowEntitiesWithColor2( AcDbObjectIdArray& objIds, const AcArray<Adesk::UInt16>& colors );
    // 命令行交互遍历多个图元
    static void BrowserEntities( const AcDbObjectIdArray& objIds );
    // 调整角度(正值，[0, 2*PI])
    // 可以输入任意大小的角度(不论正负，输入值可以超出[0, 2*PI]区间范围)
    static double AdjustAngle( double angle );
    //CString <--> AcGePoint3d之间的转换
    static CString Point3dToString( const AcGePoint3d& pt );
    static bool StringToPoint3d( const CString& value, AcGePoint3d& pt );
    //CString <--> AcGeVector3d之间的转换
    static CString Vector3dToString( const AcGeVector3d& v );
    static bool StringToVector3d( const CString& value, AcGeVector3d& v );
    // 颜色转换(RGB、RGB long、ACI)
    static void LONG_2_RGB( Adesk::UInt32 rgbColor, Adesk::UInt8& r, Adesk::UInt8& g, Adesk::UInt8& b );
    static Adesk::UInt32 ACI_2_LONG( Adesk::UInt16 colorIndex );
    static void ACI_2_RGB( Adesk::UInt16 colorIndex, Adesk::UInt8& r, Adesk::UInt8& g, Adesk::UInt8& b );
    static Adesk::UInt16 RGB_2_ACI( Adesk::UInt8 r, Adesk::UInt8 g, Adesk::UInt8 b );
    static Adesk::UInt32 RGB_2_LONG( Adesk::UInt8 r, Adesk::UInt8 g, Adesk::UInt8 b );
    // 获取cad绘图窗口背景色
    static bool GetBackgroundColor( Adesk::UInt8& r, Adesk::UInt8& g, Adesk::UInt8& b );
#ifndef _WIN64
    static AcDbObjectId GetObjectId( long id );
    static long GetLongId( const AcDbObjectId& objId );
#else
    static AcDbObjectId GetObjectId( unsigned long id );
    static unsigned long GetLongId( const AcDbObjectId& objId );
#endif // _WIN64
    // 句柄与字符串之间的转换
    static CString HandleToStr( const AcDbHandle& handle );
    static AcDbHandle StrToHandle( const CString& str );
    // id与字符串之间的转换
    static CString ObjectIdToStr( const AcDbObjectId& objId );
    static AcDbObjectId StrtoObjectId( const CString& str );
    // 线宽、颜色方法 与 字符串 之间的转换
    static CString LineWeightToStr( AcDb::LineWeight type );
    static CString ColorMethodToStr( AcCmEntityColor::ColorMethod eColorMethod );
    static AcDb::LineWeight StrToLineWeight( const CString& str );
    static AcCmEntityColor::ColorMethod StrToColorMethod( const CString& str );
    // 根据类型名称动态创建一个图元并提交到数据库(从AcDbEntity派生)
    static AcDbObjectId CreateEntity( const CString& type );
    // 获取鼠标下的图元
    static AcDbObjectId GetApertureEntity();
};
