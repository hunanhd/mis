#pragma once
#include "dlimexp.h"
// 数据操作接口
class ARXHELPER_API BaseDao
{
public:
	// 构造函数
	BaseDao();
	// 析构函数
	virtual ~BaseDao();
	// 获取几何参数的值
	bool getString( const CString& field, CString& value ) const;
	bool getInt( const CString& field, int& value ) const;
	bool getDouble( const CString& field, double& value ) const;
	bool getBool( const CString& field, bool& value ) const;
	bool getPoint( const CString& field, AcGePoint3d& value ) const;
	bool getVector( const CString& field, AcGeVector3d& value ) const;
	bool getDateTime( const CString& field, COleDateTime& value ) const;
	bool getObjectId( const CString& field, AcDbObjectId& value ) const;
	// 设置几何参数的值
	bool setString( const CString& field, const CString& value );
	bool setInt( const CString& field, int value );
	bool setDouble( const CString& field, double value );
	bool setBool( const CString& field, bool value );
	bool setPoint( const CString& field, const AcGePoint3d& value );
	bool setVector( const CString& field, const AcGeVector3d& value );
	bool setDateTime( const CString& field, const COleDateTime& value );
	bool setObjectId( const CString& field, const AcDbObjectId& value );
	bool addField( const CString& key );
	bool removeField( const CString& key );
	void clearAllFields();
	bool isModified() const;
	// 派生类必须要重载的虚函数
protected:
	virtual bool set( const CString& key, const CString& value ) = 0;
	virtual bool get( const CString& key, CString& value ) const = 0;
    virtual bool add( const CString& key ) = 0;
    virtual bool remove( const CString& key ) = 0;
	virtual void clear() = 0;
protected:
	bool m_isModified; // 是否被修改过?(默认false,调用setxxx函数时标记为true)
};
// 数据基类
class ARXHELPER_API BaseData : public BaseDao
{
public:
    virtual ~BaseData();
    // 几何参数类型
public:
    enum DATA_TYPE
    {
        DT_STRING   = 0, // 字符串
        DT_INT      = 1, // 整数
        DT_NUMERIC  = 2, // 浮点数
        DT_BOOL     = 3, // 布尔类型
        DT_DATE     = 4, // 日期类型
        DT_POINT    = 5, // 坐标点
        DT_VECTOR   = 6, // 向量
        DT_HANDLE   = 7  // 句柄(dwg和dxf中存储的是句柄,而不是id)
    };
public:
    // 获取所有注册的几何参数名称
    void getAllFields( AcStringArray& fields ) const;
    // 获取所有的几何参数名称及值
    void getAllDatas( AcStringArray& fields, AcStringArray& values );
    // 获取所有字段的类型(类型用枚举DATA_TYPE的整数值表示)
    void getAllFieldTypes( AcStringArray& fields, AcDbIntArray& types );
	// 重载基类Dao的虚函数
protected:
	// 获取几何参数的值
	virtual bool get( const CString& field, CString& value ) const;
	// 设置几何参数的值
	virtual bool set( const CString& field, const CString& value );
	// 增加字段(do nothing)
	virtual bool add( const CString& key ) { return false; }
	// 删除字段(do nothing)
	virtual bool remove( const CString& key ) { return false; }
	// 清空所有的数据与字段(do nothing)
	virtual void clear() {}
protected:
    void map( const CString& field, CString* pValue );
    void map( const CString& field, int* pValue );
    void map( const CString& field, double* pValue );
    void map( const CString& field, bool* pValue );
    void map( const CString& field, COleDateTime* pValue );
    void map( const CString& field, AcGePoint3d* pValue );
    void map( const CString& field, AcGeVector3d* pValue );
    void map( const CString& field, AcDbObjectId* pValue );
protected:
    // 构造函数
    BaseData();
    // 修改字段名称(这个方法原则上不应该设计,仅供MineGE类特殊用途!!!)
    //void hackField(const CString& field, const CString& new_field);
    /**
    * 下面这5个虚函数派生类根据需要进行重载!!!
    */
    // 获取真实字段名称(派生类如果有需要可以重载该函数,改写字段名)
    virtual CString getRealField( const CString& field ) const;
    // bool get( const CString& field, ... )函数调用前触发该消息
    virtual void onBeforeGetData( const CString& field ) const {}
    // bool set( const CString& field, ... )函数调用前触发该消息
    virtual void onBeforeSetData( const CString& field, const CString& value ) {}
    // bool get( const CString& field, ... )函数调用后触发该消息
    virtual void onAfterGetData( const CString& field ) const {}
    // bool set( const CString& field, ... )函数调用后触发该消息
    virtual void onAfterSetData( const CString& field, const CString& value ) {}
private:
    // 将字段名称与成员变量关联
    void mapByField( const CString& field, short rtype, void* pValue );
protected:
    // 存储几何参数信息(实质就是一个链表)
    // 格式: 字段名称->字段类型->映射的变量指针->...
    resbuf* m_pDatasToFileds;
};