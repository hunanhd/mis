#pragma once
#include "DictData.h"
/*
 * 数据类(基于Json+词典实现)
 */
class ARXHELPER_API JsonDictData : public DictData
{
public:
    // 获取词典的所有字段
    static bool GetAllFields( const AcDbObjectId& dictId, const CString& user_key, AcStringArray& fields );
    // 获取词典的所有字段以及值
    static bool GetAllDatas( const AcDbObjectId& dictId, const CString& user_key, AcStringArray& fields, AcStringArray& values );
    // 导出图元的字段和值为json格式(JsonDictData数据的值本身即为json格式,进行Export/Import没有必要!!!)
    //static bool ExportToJsonString( const AcDbObjectId& objId, CString& json_str);
    // 从json字符串中导入数据(JsonDictData数据的值本身即为json格式,进行Export/Import没有必要!!!)
    //static bool ImportFromJsonString( const AcDbObjectId& objId, const CString& json_str );
    //初始化数据
    static bool Init( const AcDbObjectId& dictId, const CString& user_key, const AcStringArray& fields, const AcStringArray& values );
    // 获取属性数据
    // 如果字段不存在，返回false
    static bool GetString( const AcDbObjectId& dictId, const CString& user_key, const CString& field, CString& value );
	static bool GetInt( const AcDbObjectId& dictId, const CString& user_key, const CString& field, int& value );
	static bool GetDouble( const AcDbObjectId& dictId, const CString& user_key, const CString& field, double& value );
	static bool GetBool( const AcDbObjectId& dictId, const CString& user_key, const CString& field, bool& value );
	static bool GetPoint( const AcDbObjectId& dictId, const CString& user_key, const CString& field, AcGePoint3d& value );
	static bool GetVector( const AcDbObjectId& dictId, const CString& user_key, const CString& field, AcGeVector3d& value );
	static bool GetDateTime( const AcDbObjectId& dictId, const CString& user_key, const CString& field, COleDateTime& value );
	static bool GetObjectId( const AcDbObjectId& dictId, const CString& user_key, const CString& field, AcDbObjectId& value );
    // 修改属性数据
    // 如果字段不存在，返回false
    static bool SetString( const AcDbObjectId& dictId, const CString& user_key, const CString& field, const CString& value );
	static bool SetInt( const AcDbObjectId& dictId, const CString& user_key, const CString& field, int value );
	static bool SetDouble( const AcDbObjectId& dictId, const CString& user_key, const CString& field, double value );
	static bool SetBool( const AcDbObjectId& dictId, const CString& user_key, const CString& field, bool value );
	static bool SetPoint( const AcDbObjectId& dictId, const CString& user_key, const CString& field, const AcGePoint3d& value );
	static bool SetVector( const AcDbObjectId& dictId, const CString& user_key, const CString& field, const AcGeVector3d& value );
	static bool SetDateTime( const AcDbObjectId& dictId, const CString& user_key, const CString& field, const COleDateTime& value );
	static bool SetObjectId( const AcDbObjectId& dictId, const CString& user_key, const CString& field, const AcDbObjectId& value );
    // 复制属性数据(未实现)
    static void Copy( const AcDbObjectId& sourceObjId, const AcDbObjectId& targetObjId );
    // 创建扩展词典操作dao
    static BaseDao* CreateDao( const AcDbObjectId& dictId, const CString& user_key );
protected:
    // 构造函数
    JsonDictData( const CString& user_key );
    // 重载Data基类的虚函数
    virtual BaseDao* createDao();
private:
    CString m_user_key; // 用自定义的key(类似于命名空间、包、模块的概念)
};
