#include "StdAfx.h"
#include "HelperClass.h"
#include <set>
#include <map>
#include <fstream>
// 需要包含<fstream>才能使用
#include "AcFStream.h"
static bool FindKey_Helper( const CString& dictName, const CString& type )
{
    ArxDictTool* pDictTool = ArxDictTool::GetDictTool( dictName );
    bool ret = pDictTool->findKey( type );
    delete pDictTool;
    return ret;
}
static void RemoveKey_Helper( const CString& dictName, const CString& type )
{
    ArxDictTool* pDictTool = ArxDictTool::GetDictTool( dictName );
    pDictTool->removeKey( type );
    delete pDictTool;
}
static void GetAllKeys_Helper( const CString& dictName, AcStringArray& keys )
{
    ArxDictTool* pDictTool = ArxDictTool::GetDictTool( dictName );
    pDictTool->getAllKeys( keys );
    delete pDictTool;
}
static bool AddField_Helper( const CString& dictName, const CString& type, const CString& hybirdField )
{
    ArxDictTool* pDictTool = ArxDictTool::GetDictTool( dictName );
    int index = pDictTool->addEntry( type, hybirdField );
    delete pDictTool;
    return ( index != INVALID_ENTRY );
}
static int RemoveField_Helper( const CString& dictName, const CString& type, const CString& hybirdField )
{
    ArxDictTool* pDictTool = ArxDictTool::GetDictTool( dictName );
    int index = pDictTool->removeEntry( type, hybirdField );
    delete pDictTool;
    return index;
}
static int CountFields_Helper( const CString& dictName, const CString& type )
{
    ArxDictTool* pDictTool = ArxDictTool::GetDictTool( dictName );
    int count = pDictTool->countEntries( type );
    delete pDictTool;
    return count;
}
static void GetAllFields_Helper( const CString& dictName, const CString& type, AcStringArray& hybirdFields )
{
    ArxDictTool* pDictTool = ArxDictTool::GetDictTool( dictName );
    pDictTool->getAllEntries( type, hybirdFields );
    delete pDictTool;
}
static void AddMoreFields_Helper( const CString& dictName, const CString& type, const AcStringArray& hybirdFields )
{
    ArxDictTool* pDictTool = ArxDictTool::GetDictTool( dictName );
    int n = hybirdFields.length();
    for( int i = 0; i < n; i++ )
    {
        int index = pDictTool->addEntry( type, hybirdFields[i].kACharPtr() );
    }
    delete pDictTool;
}
static void RemoveMoreFields_Helper( const CString& dictName, const CString& type, const AcStringArray& hybirdFields )
{
    ArxDictTool* pDictTool = ArxDictTool::GetDictTool( dictName );
    int n = hybirdFields.length();
    for( int i = 0; i < n; i++ )
    {
        int index = pDictTool->removeEntry( type, hybirdFields[i].kACharPtr() );
    }
    delete pDictTool;
}
static void AddObjectFieldsHelper( const AcDbObjectIdArray& objIds, const AcStringArray& hybirdFields )
{
    int len = objIds.length();
    for( int i = 0; i < len; i++ )
    {
        BaseDao* dao = ExtDictData::CreateDao( objIds[i] );
        if( dao == 0 ) continue;
        for( int j = 0; j < hybirdFields.length(); j++ )
        {
            dao->addField( hybirdFields[j].kACharPtr() );
        }
        delete dao;
    }
}
static void RemoveObjectFieldsHelper( const AcDbObjectIdArray& objIds, const AcStringArray& hybirdFields )
{
    int len = objIds.length();
    for( int i = 0; i < len; i++ )
    {
        BaseDao* dao = ExtDictData::CreateDao( objIds[i] );
        if( dao == 0 ) continue;
        for( int j = 0; j < hybirdFields.length(); j++ )
        {
            dao->removeField( hybirdFields[j].kACharPtr() );
        }
        delete dao;
    }
}
static void CleanAllObjectFieldsHelper( const AcDbObjectIdArray& objIds )
{
    int len = objIds.length();
    for( int i = 0; i < len; i++ )
    {
        BaseDao* dao = ExtDictData::CreateDao( objIds[i] );
        if( dao == 0 ) continue;
        dao->clearAllFields();
        delete dao;
    }
}
CString FieldHelper::MakeHybirdField( const CString& func, const CString& field )
{
#ifdef USE_GROUP
    CString hybirdField;
    if( func == _T( "" ) )
    {
        hybirdField.Format( _T( "%s%s%s" ), DEFAULT_GROUP, SEPARATOR, field );
    }
    else
    {
        hybirdField.Format( _T( "%s%s%s" ), func, SEPARATOR, field );
    }
    return hybirdField;
#else
    return field;
#endif
}
bool FieldHelper::SplitHybirdField( const CString& hybirdField, CString& func, CString& field )
{
    CStringArray values;
    StringHelper::SplitCString( hybirdField, SEPARATOR, values );
    if( values.GetCount() < 1 ) return false;
    if( values.GetCount() == 1 )
    {
        func = _T( "" );
        field = values[0];
    }
    else
    {
        func = values[0];
        field = StringHelper::JoinCString( values, 1, SEPARATOR );
    }
    //acutPrintf(_T("\n从 %s 提取[%s] --> [%s]"), hybirdField, func, field);
    return true;
}
bool FieldHelper::AddField( const CString& type, const CString& hybirdField )
{
    if( StringHelper::IsEmptyString( type ) || StringHelper::IsEmptyString( hybirdField ) ) return false;
    bool ret = AddField_Helper( PROPERTY_DATA_FIELD_DICT, type, hybirdField );
    if( ret )
    {
        // 向所有的图元中增加一个字段
        if( true )
        {
            AcDbObjectIdArray objIds;
            ArxDataTool::GetEntsByType( type, objIds, true );
            if( !objIds.isEmpty() )
            {
                AcStringArray hybirdFields;
                hybirdFields.append( hybirdField );
                AddObjectFieldsHelper( objIds, hybirdFields ); // 增加一个数据
            }
        }
    }
    return ret;
}
bool FieldHelper::RemoveField( const CString& type, const CString& hybirdField )
{
    if( StringHelper::IsEmptyString( type ) || StringHelper::IsEmptyString( hybirdField ) ) return false;
    int index = RemoveField_Helper( PROPERTY_DATA_FIELD_DICT, type, hybirdField );
    bool ret = ( INVALID_ENTRY != index );
    if( ret )
    {
        // 删除字段信息
        FieldInfoHelper::RemoveFieldInfo( type, hybirdField );
        if( true )
        {
            // 查找类型为type的图元, 并删除图元中的字段
            AcDbObjectIdArray objIds;
            ArxDataTool::GetEntsByType( type, objIds, true );
            if( !objIds.isEmpty() )
            {
                AcStringArray hybirdFields;
                hybirdFields.append( hybirdField );
                RemoveObjectFieldsHelper( objIds, hybirdFields );
            }
        }
    }
    return ret;
}
bool FieldHelper::AddMoreFields( const CString& type, const AcStringArray& hybirdFields )
{
    if( StringHelper::IsEmptyString( type ) ) return false;
    AddMoreFields_Helper( PROPERTY_DATA_FIELD_DICT, type, hybirdFields );
    if( true )
    {
        AcDbObjectIdArray objIds;
        ArxDataTool::GetEntsByType( type, objIds, true );
        AddObjectFieldsHelper( objIds, hybirdFields ); // 增加多个数据
    }
    return true;
}
bool FieldHelper::RemoveMoreFields( const CString& type, const AcStringArray& hybirdFields )
{
    if( StringHelper::IsEmptyString( type ) ) return false;
    // 删除字段
    RemoveMoreFields_Helper( PROPERTY_DATA_FIELD_DICT, type, hybirdFields );
    // 删除字段信息
    FieldInfoHelper::RemoveMoreFieldsInfo( type, hybirdFields );
    if( true )
    {
        // 删除图元中的字段
        AcDbObjectIdArray objIds;
        ArxDataTool::GetEntsByType( type, objIds, true );
        RemoveObjectFieldsHelper( objIds, hybirdFields );
    }
    return true;
}
// 实现有问题???
void FieldHelper::RemoveAllFields( const CString& type )
{
    // 删除词典中的type类型下的所有字段
    AcStringArray hybirdFields;
    GetAllFields( type, hybirdFields );
    int len = hybirdFields.length();
    for( int i = 0; i < len; i++ )
    {
        RemoveField( type, hybirdFields[i].kACharPtr() );
    }
    RemoveKey_Helper( PROPERTY_DATA_FIELD_DICT, type );
    // 删除type类型图元的所有数据
    AcDbObjectIdArray objIds;
    ArxDataTool::GetEntsByType( type, objIds, true );
    CleanAllObjectFieldsHelper( objIds );
}
int FieldHelper::CountFields( const CString& type )
{
    return CountFields_Helper( PROPERTY_DATA_FIELD_DICT, type );
}
void FieldHelper::GetAllFields( const CString& type, AcStringArray& hybirdFields )
{
    GetAllFields_Helper( PROPERTY_DATA_FIELD_DICT, type, hybirdFields );
}
void FieldHelper::GetAllRegTypes( AcStringArray& types )
{
    // 类型包括3部分：
    //   1) arx中派生于MineGE类型的图元
    ArxClassHelper::GetAllTopParentClass( _T( "MineGE" ), types );
    //   2) arx中派生与ModelGE类型的图元
    //ArxClassHelper::GetAllTopParentClass( _T( "ModelGE" ), types );
    //   3.1) 在词典OBJECT_LIST_DICT中注册的类型
    //GetAllKeys_Helper( OBJECT_LIST_DICT, types );
    //   3.2) 在词典GLOBAL_SINGLE_INFO_DICT中注册的类型
    //GetAllKeys_Helper( GLOBAL_SINGLE_INFO_DICT, types );
}
void FieldHelper::GuessRealFields( const CString& type, const AcStringArray& fields, AcStringArray& hybirdFields )
{
    AcStringArray allFields;
    FieldHelper::GetAllFields( type, allFields );
    typedef std::set<CString> StringSet;
    StringSet field_set;
    // 构造" 字段名->分组 "词典
    typedef std::map<CString, CString> StringMap;
    StringMap field_map;
    for( int i = 0; i < allFields.length(); i++ )
    {
        // 构造集合
        field_set.insert( allFields[i].kACharPtr() );
        // 构造词典
        CString func, field;
        FieldHelper::SplitHybirdField( allFields[i].kACharPtr(), func, field );
        field_map[field] = func;
    }
    // 猜测
    for( int i = 0; i < fields.length(); i++ )
    {
        CString field = fields[i].kACharPtr();
        if( field_set.find( field ) != field_set.end() )
        {
            hybirdFields.append( field );
        }
        else if( field_map.find( field ) != field_map.end() )
        {
            hybirdFields.append( FieldHelper::MakeHybirdField( field_map[field], field ) );
        }
        else
        {
            hybirdFields.append( field );
        }
    }
}
CString FieldHelper::GuessRealField( const CString& type, const CString& field )
{
    AcStringArray fields;
    fields.append( field );
    AcStringArray hybirdFields;
    FieldHelper::GuessRealFields( type, fields, hybirdFields );
    return hybirdFields[0].kACharPtr();
}
