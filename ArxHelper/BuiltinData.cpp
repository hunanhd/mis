#include "StdAfx.h"
#include "BuiltinData.h"
#include "HelperClass.h"
AcDbLayerTableRecordData::AcDbLayerTableRecordData()
{
    map( _T( "冻结" ), &m_isFrozen );
    map( _T( "关闭" ), &m_isOff );
    map( _T( "锁定" ), &m_isLocked );
    map( _T( "颜色" ), &m_colorIndex );
    map( _T( "线型" ), &m_lineType );
    map( _T( "线宽" ), &m_lineWeight );
}
void AcDbLayerTableRecordData::onBeforeGetData( const CString& field ) const
{
    AcDbLayerTableRecord* pEnt = AcDbLayerTableRecord::cast( m_pObj );
    if( pEnt == 0 ) return;
    if( field == _T( "冻结" ) )
    {
        assign_const( this->m_isFrozen, pEnt->isFrozen() );
    }
    else if( field == _T( "关闭" ) )
    {
        assign_const( this->m_isOff, pEnt->isOff() );
    }
    else if( field == _T( "锁定" ) )
    {
        assign_const( this->m_isLocked, pEnt->isLocked() );
    }
    else if( field == _T( "颜色" ) )
    {
        assign_const( this->m_colorIndex, ( int )( pEnt->color().colorIndex() ) );
    }
    else if( field == _T( "线型" ) )
    {
        CString name = ArxEntityHelper::GetLineTypeName( pEnt->linetypeObjectId() );
        if( !name.IsEmpty() )
        {
            assign_const( this->m_lineType, name );
        }
    }
    else if( field == _T( "线宽" ) )
    {
        assign_const( this->m_lineWeight, ArxUtilHelper::LineWeightToStr( pEnt->lineWeight() ) );
    }
}
void AcDbLayerTableRecordData::onAfterSetData( const CString& field, const CString& value )
{
    AcDbLayerTableRecord* pEnt = AcDbLayerTableRecord::cast( m_pObj );
    if( pEnt == 0 ) return;
    if( field == _T( "冻结" ) )
    {
        pEnt->setIsFrozen( this->m_isFrozen );
    }
    else if( field == _T( "关闭" ) )
    {
        pEnt->setIsOff( this->m_isOff );
    }
    else if( field == _T( "锁定" ) )
    {
        pEnt->setIsLocked( this->m_isLocked );
    }
    else if( field == _T( "颜色" ) )
    {
        AcCmColor color;
        color.setColorIndex( ( Adesk::UInt16 )( this->m_colorIndex ) );
        pEnt->setColor( color );
    }
    else if( field == _T( "线型" ) )
    {
        AcDbObjectId objId = ArxEntityHelper::GetLineTypeId( this->m_lineType );
        if( !objId.isNull() )
        {
            pEnt->setLinetypeObjectId( objId );
        }
    }
    else if( field == _T( "线宽" ) )
    {
        pEnt->setLineWeight( ArxUtilHelper::StrToLineWeight( this->m_lineWeight ) );
    }
}
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
AcDbEntityData::AcDbEntityData()
{
    // 关联图元的基本几何特性，包括图层、颜色、线型、线宽
    map( _T( "图层" ), &m_layer );
    map( _T( "颜色" ), &m_colorIndex );
    map( _T( "线型" ), &m_lineType );
    map( _T( "线宽" ), &m_lineWeight );
}
void AcDbEntityData::onBeforeGetData( const CString& field ) const
{
    AcDbEntity* pEnt = AcDbEntity::cast( m_pObj );
    if( pEnt == 0 ) return;
    if( field == _T( "图层" ) )
    {
        assign_const( this->m_layer, CString( pEnt->layer() ) );
    }
    else if( field == _T( "颜色" ) )
    {
        assign_const( this->m_colorIndex, ( int )( pEnt->colorIndex() ) );
    }
    else if( field == _T( "线型" ) )
    {
        assign_const( this->m_lineType, CString( pEnt->linetype() ) );
    }
    else if( field == _T( "线宽" ) )
    {
        assign_const( this->m_lineWeight, ArxUtilHelper::LineWeightToStr( pEnt->lineWeight() ) );
    }
}
void AcDbEntityData::onAfterSetData( const CString& field, const CString& value )
{
    AcDbEntity* pEnt = AcDbEntity::cast( m_pObj );
    if( pEnt == 0 ) return;
    if( field == _T( "图层" ) )
    {
        pEnt->setLayer( ( LPCTSTR )( this->m_layer ) );
    }
    else if( field == _T( "颜色" ) )
    {
        pEnt->setColorIndex( this->m_colorIndex );
    }
    else if( field == _T( "线型" ) )
    {
        pEnt->setLinetype( ( LPCTSTR )( this->m_lineType ) );
    }
    else if( field == _T( "线宽" ) )
    {
        pEnt->setLineWeight( ArxUtilHelper::StrToLineWeight( this->m_lineWeight ) );
    }
}
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
AcDbLineData::AcDbLineData() : AcDbEntityData()
{
    map( _T( "起点" ), &m_spt );
    map( _T( "终点" ), &m_ept );
}
void AcDbLineData::onBeforeGetData( const CString& field ) const
{
    AcDbLine* pEnt = AcDbLine::cast( m_pObj );
    if( pEnt == 0 ) return;
    // 调用基类方法
    AcDbEntityData::onBeforeGetData( field );
    if( field == _T( "起点" ) )
    {
        AcGePoint3d pt;
        pEnt->getStartPoint( pt );
        assign_const( this->m_spt, pt );
    }
    else if( field == _T( "终点" ) )
    {
        AcGePoint3d pt;
        pEnt->getEndPoint( pt );
        assign_const( this->m_ept, pt );
    }
}
void AcDbLineData::onAfterSetData( const CString& field, const CString& value )
{
    AcDbLine* pEnt = AcDbLine::cast( m_pObj );
    if( pEnt == 0 ) return;
    // 调用基类方法
    AcDbEntityData::onAfterSetData( field, value );
    if( field == _T( "起点" ) )
    {
        pEnt->setStartPoint( this->m_spt );
    }
    else if( field == _T( "终点" ) )
    {
        pEnt->setEndPoint( this->m_ept );
    }
}
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
AcDbCircleData::AcDbCircleData() : AcDbEntityData()
{
    map( _T( "中心点" ), &m_cnt );
    map( _T( "半径" ), &m_radius );
}
void AcDbCircleData::onBeforeGetData( const CString& field ) const
{
    AcDbCircle* pEnt = AcDbCircle::cast( m_pObj );
    if( pEnt == 0 ) return;
    // 调用基类方法
    AcDbEntityData::onBeforeGetData( field );
    if( field == _T( "中心点" ) )
    {
        AcGePoint3d pt = pEnt->center();
        assign_const( this->m_cnt, pt );
    }
    else if( field == _T( "半径" ) )
    {
        assign_const( this->m_radius, pEnt->radius() );
    }
}
void AcDbCircleData::onAfterSetData( const CString& field, const CString& value )
{
    AcDbCircle* pEnt = AcDbCircle::cast( m_pObj );
    if( pEnt == 0 ) return;
    // 调用基类方法
    AcDbEntityData::onAfterSetData( field, value );
    if( field == _T( "中心点" ) )
    {
        pEnt->setCenter( this->m_cnt );
    }
    else if( field == _T( "半径" ) )
    {
        pEnt->setRadius( this->m_radius );
    }
}
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
AcDbArcData::AcDbArcData() : AcDbEntityData()
{
    map( _T( "中心点" ), &m_cnt );
    map( _T( "半径" ), &m_radius );
    map( _T( "起始角度" ), &m_startAngle );
    map( _T( "终止角度" ), &m_endAngle );
}
void AcDbArcData::onBeforeGetData( const CString& field ) const
{
    AcDbArc* pEnt = AcDbArc::cast( m_pObj );
    if( pEnt == 0 ) return;
    // 调用基类方法
    AcDbEntityData::onBeforeGetData( field );
    if( field == _T( "中心点" ) )
    {
        AcGePoint3d pt = pEnt->center();
        assign_const( this->m_cnt, pt );
    }
    else if( field == _T( "半径" ) )
    {
        assign_const( this->m_radius, pEnt->radius() );
    }
    else if( field == _T( "起始角度" ) )
    {
        assign_const( this->m_startAngle, pEnt->startAngle() );
    }
    else if( field == _T( "终止角度" ) )
    {
        assign_const( this->m_endAngle, pEnt->endAngle() );
    }
}
void AcDbArcData::onAfterSetData( const CString& field, const CString& value )
{
    AcDbArc* pEnt = AcDbArc::cast( m_pObj );
    if( pEnt == 0 ) return;
    // 调用基类方法
    AcDbEntityData::onAfterSetData( field, value );
    if( field == _T( "中心点" ) )
    {
        pEnt->setCenter( this->m_cnt );
    }
    else if( field == _T( "半径" ) )
    {
        pEnt->setRadius( this->m_radius );
    }
    else if( field == _T( "起始角度" ) )
    {
        pEnt->setStartAngle( this->m_startAngle );
    }
    else if( field == _T( "终止角度" ) )
    {
        pEnt->setEndAngle( this->m_endAngle );
    }
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
AcDbEllipseData::AcDbEllipseData() : AcDbEntityData()
{
    map( _T( "中心点" ), &m_cnt );
    map( _T( "长轴向量" ), &m_majorAxis );
    map( _T( "长短轴比" ), &m_radiusRatio );
}
void AcDbEllipseData::onBeforeGetData( const CString& field ) const
{
    AcDbEllipse* pEnt = AcDbEllipse::cast( m_pObj );
    if( pEnt == 0 ) return;
    // 调用基类方法
    AcDbEntityData::onBeforeGetData( field );
    if( field == _T( "中心点" ) )
    {
        AcGePoint3d pt = pEnt->center();
        assign_const( this->m_cnt, pt );
    }
    else if( field == _T( "长轴向量" ) )
    {
        assign_const( this->m_majorAxis, pEnt->majorAxis() );
    }
    else if( field == _T( "长短轴比" ) )
    {
        assign_const( this->m_radiusRatio, pEnt->radiusRatio() );
    }
}
void AcDbEllipseData::onAfterSetData( const CString& field, const CString& value )
{
    AcDbEllipse* pEnt = AcDbEllipse::cast( m_pObj );
    if( pEnt == 0 ) return;
    // 调用基类方法
    AcDbEntityData::onAfterSetData( field, value );
    if( field == _T( "中心点" ) )
    {
        pEnt->setCenter( this->m_cnt );
    }
    else if( field == _T( "长轴向量" ) )
    {
        pEnt->set( this->m_cnt, AcGeVector3d::kZAxis, this->m_majorAxis, this->m_radiusRatio );
    }
    else if( field == _T( "长短轴比" ) )
    {
        pEnt->set( this->m_cnt, AcGeVector3d::kZAxis, this->m_majorAxis, this->m_radiusRatio );
    }
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
AcDbMTextData::AcDbMTextData() : AcDbEntityData()
{
    map( _T( "位置" ), &m_location );
    map( _T( "方向向量" ), &m_direction );
    map( _T( "高度" ), &m_textHeight );
    map( _T( "内容" ), &m_contents );
}
void AcDbMTextData::onBeforeGetData( const CString& field ) const
{
    AcDbMText* pEnt = AcDbMText::cast( m_pObj );
    if( pEnt == 0 ) return;
    // 调用基类方法
    AcDbEntityData::onBeforeGetData( field );
    if( field == _T( "位置" ) )
    {
        AcGePoint3d pt = pEnt->location();
        assign_const( this->m_location, pt );
    }
    else if( field == _T( "方向向量" ) )
    {
        assign_const( this->m_direction, pEnt->direction() );
    }
    else if( field == _T( "高度" ) )
    {
        assign_const( this->m_textHeight, pEnt->textHeight() );
    }
    else if( field == _T( "内容" ) )
    {
        assign_const( this->m_contents, CString( pEnt->contents() ) );
    }
}
void AcDbMTextData::onAfterSetData( const CString& field, const CString& value )
{
    AcDbMText* pEnt = AcDbMText::cast( m_pObj );
    if( pEnt == 0 ) return;
    // 调用基类方法
    AcDbEntityData::onAfterSetData( field, value );
    if( field == _T( "位置" ) )
    {
        pEnt->setLocation( this->m_location );
    }
    else if( field == _T( "方向向量" ) )
    {
        pEnt->setDirection( this->m_direction );
    }
    else if( field == _T( "高度" ) )
    {
        pEnt->setTextHeight( this->m_textHeight );
    }
    else if( field == _T( "内容" ) )
    {
        pEnt->setContents( this->m_contents );
    }
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
AcDbTextData::AcDbTextData() : AcDbEntityData()
{
    map( _T( "位置" ), &m_position );
    map( _T( "角度" ), &m_rotation );
    map( _T( "高度" ), &m_height );
    map( _T( "内容" ), &m_text );
}
void AcDbTextData::onBeforeGetData( const CString& field ) const
{
    AcDbText* pEnt = AcDbText::cast( m_pObj );
    if( pEnt == 0 ) return;
    // 调用基类方法
    AcDbEntityData::onBeforeGetData( field );
    if( field == _T( "位置" ) )
    {
        AcGePoint3d pt = pEnt->position();
        assign_const( this->m_position, pt );
    }
    else if( field == _T( "角度" ) )
    {
        assign_const( this->m_rotation, pEnt->rotation() );
    }
    else if( field == _T( "高度" ) )
    {
        assign_const( this->m_height, pEnt->height() );
    }
    else if( field == _T( "内容" ) )
    {
        assign_const( this->m_text, CString( pEnt->textStringConst() ) );
    }
}
void AcDbTextData::onAfterSetData( const CString& field, const CString& value )
{
    AcDbText* pEnt = AcDbText::cast( m_pObj );
    if( pEnt == 0 ) return;
    // 调用基类方法
    AcDbEntityData::onAfterSetData( field, value );
    if( field == _T( "位置" ) )
    {
        pEnt->setPosition( this->m_position );
    }
    else if( field == _T( "角度" ) )
    {
        pEnt->setRotation( this->m_rotation );
    }
    else if( field == _T( "高度" ) )
    {
        pEnt->setHeight( this->m_height );
    }
    else if( field == _T( "内容" ) )
    {
        pEnt->setTextString( ( LPCTSTR )this->m_text );
    }
}
