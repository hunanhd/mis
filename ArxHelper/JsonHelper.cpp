#include "StdAfx.h"
#include "JsonHelper.h"
#include "HelperClass.h"
#include <json/json.h>
bool JsonHelper::Format( const AcStringArray& fields, const AcStringArray& values, CString& json_str )
{
    if( fields.length() != values.length() ) return false;
    Json::Value root;
    for( int i = 0; i < fields.length(); i++ )
    {
        std::string sKey = W2C( fields[i].kACharPtr() );
        std::string sValue = W2C( values[i].kACharPtr() );
        root[sKey] = Json::Value( sValue );
    }
    // 输出无格式json字符串
    Json::FastWriter writer;
    std::string str = writer.write( root );
    json_str = C2W( str.c_str() );
    return true;
}
/*
* 参考: JsonCpp遍历json
* http://blog.csdn.net/yuanxiaobo007/article/details/26229615
*/
bool JsonHelper::Parse( const CString& json_str, AcStringArray& fields, AcStringArray& values )
{
    Json::Value root;
    Json::Reader reader;
    if( !reader.parse( W2C( ( LPCTSTR )json_str ), root ) )
    {
        return false;
    }
    else
    {
        Json::Value::Members mem = root.getMemberNames();
        Json::Value::Members::iterator itr;
        //遍历json节点下的所有key
        for ( itr = mem.begin(); itr != mem.end(); itr++ )
        {
            std::string sKey = *itr;
            std::string sValue;
            if ( root[sKey].type() == Json::stringValue )
            {
                sValue = root[sKey].asString();
            }
            fields.append( C2W( sKey.c_str() ) );
            values.append( C2W( sValue.c_str() ) );
        }
        return true;
    }
}
