#include "StdAfx.h"
#include "_InternalDataHelper.h"
#include "HelperClass.h"
#include "Data.h"
resbuf* GoToListEnd( resbuf* pHead, const CString& field )
{
    resbuf* pTemp = pHead;
    while( pTemp != 0 )
    {
        // 存在相同的字段名称
        if( field.CompareNoCase( pTemp->resval.rstring ) == 0 ) break;
        bool isBreak = false;
        // 循环3次，跳到下一个字段节点
        for( int i = 0; i < 3; i++ )
        {
            if( pTemp->rbnext == 0 )
            {
                isBreak = true;
                break;
            }
            pTemp = pTemp->rbnext;
        }
        if( isBreak ) break;
    }
    return pTemp;
}
resbuf* SetData( resbuf* pStart, const CString& value )
{
    resbuf* pTypeNode = pStart->rbnext;
    resbuf* pValueNode = pTypeNode->rbnext;
    short rtype = pTypeNode->resval.rint;
    long ptr = pValueNode->resval.mnLongPtr;
    switch( rtype )
    {
    case BaseData::DT_STRING:
    {
        CString* pValue = ( CString* )ptr;
        *pValue = value;
    }
    break;
    case BaseData::DT_INT:
    {
        int iv = 0;
        StringHelper::StringToInt( value, iv );
        int* pValue = ( int* )ptr;
        *pValue = iv;
    }
    break;
    case BaseData::DT_NUMERIC:
    {
        double dv = 0;
        StringHelper::StringToDouble( value, dv );
        double* pValue = ( double* )ptr;
        *pValue = dv;
    }
    break;
    case BaseData::DT_BOOL:
    {
        int iv = 0;
        bool bv = false;
        if( StringHelper::StringToInt( value, iv ) )
        {
            bv = ( iv != 0 ); // 等于0为假，不等于0为真
        }
        bool* pValue = ( bool* )ptr;
        *pValue = bv;
    }
    break;
    case BaseData::DT_DATE:
    {
        COleDateTime dt;
        if( ( value.GetLength() == 0 ) || !dt.ParseDateTime( value ) )
        {
            // 当前时间
            dt = COleDateTime::GetCurrentTime();
        }
        COleDateTime* pValue = ( COleDateTime* )ptr;
        *pValue = dt;
    }
    break;
    case BaseData::DT_POINT:
    {
        AcGePoint3d pt;
        ArxUtilHelper::StringToPoint3d( value, pt );
        AcGePoint3d* pValue = ( AcGePoint3d* )ptr;
        *pValue = pt;
    }
    break;
    case BaseData::DT_VECTOR:
    {
        AcGeVector3d v;
        ArxUtilHelper::StringToVector3d( value, v );
        AcGeVector3d* pValue = ( AcGeVector3d* )ptr;
        *pValue = v;
    }
    break;
    case BaseData::DT_HANDLE:
    {
        // 字符串转换成句柄
        AcDbHandle handle = ArxUtilHelper::StrToHandle( value );
        // 句柄转换成AcDbObjectId
        AcDbObjectId objId;
        acdbHostApplicationServices()->workingDatabase()->getAcDbObjectId( objId, false, handle, 0 );
        AcDbObjectId* pValue = ( AcDbObjectId* )ptr;
        *pValue = objId;
    }
    break;
    }
    return pValueNode->rbnext;
}
resbuf* GetData( resbuf* pStart, CString& value )
{
    resbuf* pTypeNode = pStart->rbnext;
    resbuf* pValueNode = pTypeNode->rbnext;
    short rtype = pTypeNode->resval.rint;
    long ptr = pValueNode->resval.mnLongPtr;
    switch( rtype )
    {
    case BaseData::DT_STRING:
    {
        value = *( ( CString* )ptr );
    }
    break;
    case BaseData::DT_INT:
    {
        StringHelper::IntToString( *( ( int* )ptr ), value );
    }
    break;
    case BaseData::DT_NUMERIC:
    {
        StringHelper::DoubleToString( *( ( double* )ptr ), value );
    }
    break;
    case BaseData::DT_BOOL:
    {
        bool bv = *( bool* )ptr;
        value.Format( _T( "%d" ), ( bv ? -1 : 0 ) ); // -1表示真, 0表示假
    }
    break;
    case BaseData::DT_DATE:
    {
        COleDateTime dt = *( COleDateTime* )ptr;
        value = ( COleVariant )dt;
    }
    break;
    case BaseData::DT_POINT:
    {
        value = ArxUtilHelper::Point3dToString( *( ( AcGePoint3d* )ptr ) );
    }
    break;
    case BaseData::DT_VECTOR:
    {
        value = ArxUtilHelper::Vector3dToString( *( ( AcGeVector3d* )ptr ) );
    }
    break;
    case BaseData::DT_HANDLE:
    {
        AcDbObjectId* objId = ( AcDbObjectId* )ptr;
        value = ArxUtilHelper::HandleToStr( objId->handle() );
    }
    break;
    }
    return pValueNode->rbnext;
}
void GetAllFields( resbuf* pHead, AcStringArray& fields )
{
    resbuf* pTemp = pHead;
    while( pTemp != 0 )
    {
        fields.append( pTemp->resval.rstring );
        bool isBreak = false;
        // 循环3次，跳到下一个字段节点
        for( int i = 0; i < 3; i++ )
        {
            if( pTemp->rbnext == 0 )
            {
                isBreak = true;
                break;
            }
            pTemp = pTemp->rbnext;
        }
        if( isBreak ) break;
    }
}
void GetAllDatas( resbuf* pHead, AcStringArray& fields, AcStringArray& values )
{
    resbuf* pTemp = pHead;
    while( pTemp != 0 )
    {
        CString field = pTemp->resval.rstring;
        fields.append( field );
        // 获取数据值并跳转到下一个节点的位置
        CString value;
        pTemp = GetData( pTemp, value );
        values.append( value );
    }
}
void GetAllFieldTypes( resbuf* pHead, AcStringArray& fields, AcDbIntArray& types )
{
    resbuf* pTemp = pHead;
    while( pTemp != 0 )
    {
        CString field = pTemp->resval.rstring;
        fields.append( field );
        resbuf* pTypeNode = pTemp->rbnext;
        types.append( pTypeNode->resval.rint );
        // 获取数据值并跳转到下一个节点的位置
        CString value;
        pTemp = GetData( pTemp, value );
    }
}
void MapGenericDataByField( resbuf*& pHead, const CString& field, short rtype, void* pValue )
{
    if( field.GetLength() == 0 ) return;
    resbuf* pTemp = 0;
    if( pHead == 0 )
    {
        pHead = acutBuildList( RTSTR, field, RTSHORT, rtype, RTLONG_PTR, ( long )pValue, 0 );
    }
    else
    {
        pTemp = GoToListEnd( pHead, field );
        if( pTemp->rbnext == 0 )
        {
            pTemp->rbnext = acutBuildList( RTSTR, field, RTSHORT, rtype, RTLONG_PTR, ( long )pValue, 0 );
        }
    }
}
void MapGenericDataByPos( resbuf*& pHead, int pos, short rtype, void* pValue )
{
    if( pos < 0 ) return;
    CString field;
    StringHelper::IntToString( pos, field );
    MapGenericDataByField( pHead, field, rtype, pValue );
}