#include "StdAfx.h"
#include "ExtDictData.h"
#include "Dao.h"
#include "HelperClass.h"
BaseDao* ExtDictData::createDao()
{
    return ExtDictData::CreateDao( getDataSource() );
}
bool ExtDictData::GetAllFields( const AcDbObjectId& objId, AcStringArray& fields )
{
    if( objId.isNull() ) return false;
    AcDbObjectId dictId = ArxDictTool::GetExtensionDict( objId );
    return DictData::GetAllFields( dictId, fields );
}
bool ExtDictData::GetAllDatas( const AcDbObjectId& objId, AcStringArray& fields, AcStringArray& values )
{
    if( objId.isNull() ) return false;
    if( !ExtDictData::GetAllFields( objId, fields ) ) return false;
	return ExtDictData::GetDatas(objId, fields, values);
}
bool ExtDictData::GetDatas(const AcDbObjectId& objId, const AcStringArray& fields, AcStringArray& values)
{
	if( objId.isNull() ) return false;

	BaseDao* dao = ExtDictData::CreateDao( objId );
	if( dao == 0 ) return false;
	for( int i = 0; i < fields.length(); i++ )
	{
		CString value;
		bool ret = dao->getString( fields[i].kACharPtr(), value );
		values.append( value );
	}
	delete dao;
	return true;
}
bool ExtDictData::ExportToJsonString( const AcDbObjectId& objId, CString& json_str )
{
    AcStringArray fields, values;
    if( !ExtDictData::GetAllDatas( objId, fields, values ) )
    {
        json_str = NULL_JSON_STRING;
        return false;
    }
    else
    {
        // 将字段和值格式化成json字符串
        return JsonHelper::Format( fields, values, json_str );
    }
}
bool ExtDictData::ImportFromJsonString( const AcDbObjectId& objId, const CString& json_str )
{
    // 从json字符串中解析字段和值
    AcStringArray fields, values;
    if( !JsonHelper::Parse( json_str, fields, values ) ) return false;
    // 初始化数据
    return ExtDictData::Init( objId, fields, values );
}
bool ExtDictData::Init( const AcDbObjectId& objId, const AcStringArray& fields, const AcStringArray& values )
{
    if( objId.isNull() ) return false;
    BaseDao* dao = ExtDictData::CreateDao( objId );
    if( dao == 0 ) return false;
    dao->clearAllFields(); // 清空
    for( int i = 0; i < values.length(); i++ )
    {
        dao->setString( fields[i].kACharPtr(), values[i].kACharPtr() );
    }
    delete dao;
    return true;
}
bool ExtDictData::GetString( const AcDbObjectId& objId, const CString& field, CString& value )
{
    if( objId.isNull() ) return false;
    BaseDao* dao = ExtDictData::CreateDao( objId );
    if( dao == 0 ) return false;
    bool ret = dao->getString( field, value );
    delete dao;
    return ret;
}
bool ExtDictData::GetInt( const AcDbObjectId& objId, const CString& field, int& value )
{
	CString str;
	if( !ExtDictData::GetString( objId, field, str ) ) return false;
	if( !StringHelper::StringToInt( str, value ) ) return false;
	return true;
}
bool ExtDictData::GetDouble( const AcDbObjectId& objId, const CString& field, double& value )
{
	CString str;
	if( !ExtDictData::GetString( objId, field, str ) ) return false;
	if( !StringHelper::StringToDouble( str, value ) ) return false;
	return true;
}
bool ExtDictData::GetBool( const AcDbObjectId& objId, const CString& field, bool& value )
{
	CString str;
	if( !ExtDictData::GetString( objId, field, str ) ) return false;
	int i = 0;
	if( !StringHelper::StringToInt( str, i ) ) return false;
	value = ( i != 0 );
	return true;
}
bool ExtDictData::GetPoint( const AcDbObjectId& objId, const CString& field, AcGePoint3d& value )
{
	CString str;
	if( !ExtDictData::GetString( objId, field, str ) ) return false;
	if( !ArxUtilHelper::StringToPoint3d( str, value ) ) return false;
	return true;
}
bool ExtDictData::GetVector( const AcDbObjectId& objId, const CString& field, AcGeVector3d& value )
{
	CString str;
	if( !ExtDictData::GetString( objId, field, str ) ) return false;
	AcGePoint3d pt;
	if( !ArxUtilHelper::StringToPoint3d( str, pt ) ) return false;
	value = pt.asVector();
	return true;
}
bool ExtDictData::GetDateTime( const AcDbObjectId& objId, const CString& field, COleDateTime& value )
{
	CString str;
	if( !ExtDictData::GetString( objId, field, str ) ) return false;
	return value.ParseDateTime( str );
}
bool ExtDictData::GetObjectId( const AcDbObjectId& objId, const CString& field, AcDbObjectId& value )
{
	CString str;
	if( !ExtDictData::GetString( objId, field, str ) ) return false;
	value = ArxUtilHelper::StrtoObjectId( str );
	return !value.isNull();
}
bool ExtDictData::SetInt( const AcDbObjectId& objId, const CString& field, int value )
{
	CString str;
	StringHelper::IntToString( value, str );
	return ExtDictData::SetString( objId, field, str );
}
bool ExtDictData::SetDouble( const AcDbObjectId& objId, const CString& field, double value )
{
	CString str;
	StringHelper::DoubleToString( value, str );
	return ExtDictData::SetString( objId, field, str );
}
bool ExtDictData::SetBool( const AcDbObjectId& objId, const CString& field, bool value )
{
	CString str;
	StringHelper::IntToString( value ? 1 : 0, str );
	return ExtDictData::SetString( objId, field, str );
}
bool ExtDictData::SetPoint( const AcDbObjectId& objId, const CString& field, const AcGePoint3d& value )
{
	CString str = ArxUtilHelper::Point3dToString( value );
	return ExtDictData::SetString( objId, field, str );
}
bool ExtDictData::SetVector( const AcDbObjectId& objId, const CString& field, const AcGeVector3d& value )
{
	return ExtDictData::SetPoint( objId, field, AcGePoint3d::kOrigin + value );
}
bool ExtDictData::SetDateTime( const AcDbObjectId& objId, const CString& field, const COleDateTime& value )
{
	CString str = value.Format( _T( "%Y-%m-%d %H:%M:%S" ) );
	return ExtDictData::SetString( objId, field, str );
}
bool ExtDictData::SetObjectId( const AcDbObjectId& objId, const CString& field, const AcDbObjectId& value )
{
	CString str = ArxUtilHelper::ObjectIdToStr( value );
	return ExtDictData::SetString( objId, field, str );
}
bool ExtDictData::SetString( const AcDbObjectId& objId, const CString& field, const CString& value )
{
    if( objId.isNull() ) return false;
    BaseDao* dao = ExtDictData::CreateDao( objId );
    if( dao == 0 ) return false;
    bool ret = dao->setString( field, value );
    delete dao;
    return ret;
}
bool XData::SetInt( const AcDbObjectId& objId, int pos, int value )
{
	CString str;
	StringHelper::IntToString( value, str );
	return XData::SetString( objId, pos, str );
}
bool XData::SetDouble( const AcDbObjectId& objId, int pos, double value )
{
	CString str;
	StringHelper::DoubleToString( value, str );
	return XData::SetString( objId, pos, str );
}
bool XData::SetBool( const AcDbObjectId& objId, int pos, bool value )
{
	CString str;
	StringHelper::IntToString( value ? 1 : 0, str );
	return XData::SetString( objId, pos, str );
}
bool XData::SetPoint( const AcDbObjectId& objId, int pos, const AcGePoint3d& value )
{
	CString str = ArxUtilHelper::Point3dToString( value );
	return XData::SetString( objId, pos, str );
}
bool XData::SetVector( const AcDbObjectId& objId, int pos, const AcGeVector3d& value )
{
	return XData::SetPoint( objId, pos, AcGePoint3d::kOrigin + value );
}
bool XData::SetDateTime( const AcDbObjectId& objId, int pos, const COleDateTime& value )
{
	CString str = value.Format( _T( "%Y-%m-%d %H:%M:%S" ) );
	return XData::SetString( objId, pos, str );
}
bool XData::SetObjectId( const AcDbObjectId& objId, int pos, const AcDbObjectId& value )
{
	CString str = ArxUtilHelper::ObjectIdToStr( value );
	return XData::SetString( objId, pos, str );
}
bool ExtDictData::Copy( const AcDbObjectId& sourceObjId, const AcDbObjectId& targetObjId )
{
	CString SourceType,targetType;
	ArxDataTool::GetTypeName( sourceObjId, SourceType );
	ArxDataTool::GetTypeName( targetObjId, targetType );

	if ( SourceType != targetType ) return false;

	AcStringArray hybirdFields;
	FieldHelper::GetAllFields( SourceType, hybirdFields );
	if( hybirdFields.isEmpty() ) return false;
	BaseDao* sourceDao = ExtDictData::CreateDao( sourceObjId );
	if( sourceDao == 0 ) return false;
	BaseDao* targetDao = ExtDictData::CreateDao( targetObjId );
	if( targetDao == 0 ) return false;

	for( int i = 0; i < hybirdFields.length(); i++ )
	{
		CString field = hybirdFields[i].kACharPtr();
		CString value;
		sourceDao->getString( field, value );
		targetDao->setString( field, value );	
	}
	delete sourceDao;
	delete targetDao;
	return true;
}

BaseDao* ExtDictData::CreateDao( const AcDbObjectId& objId )
{
    if( objId.isNull() ) return 0;
	return new ExtDictDao( objId );
    ////获取扩展词典id
    //AcDbObjectId dictId = ArxDictTool::GetExtensionDict( objId );
    //if( dictId.isNull() ) return 0;
    //// 调用DictData基类的CreateDao方法
    //return DictData::CreateDao( dictId );
}
ExtDictData::ExtDictData() : DictData()
{
}
