#include "StdAfx.h"
#include "AcStringHelper.h"
#include "HelperClass.h"
void AcStringHelper::R2S( const AcStringArray& a, StringArray& v )
{
    for( int i = 0; i < a.length(); i++ )
    {
        v.push_back( W2C( a[i].kACharPtr() ) );
    }
}
void AcStringHelper::S2R( const StringArray& v, AcStringArray& a )
{
    StringArray::const_iterator itr = v.begin();
    for( ; itr != v.end(); ++itr )
    {
        a.append( C2W( itr->c_str() ) );
    }
}
void AcStringHelper::R2CS( const AcStringArray& a, MFCStringArray& v )
{
    for( int i = 0; i < a.length(); i++ )
    {
        v.push_back( a[i].kACharPtr() );
    }
}
void AcStringHelper::CS2R( const MFCStringArray& v, AcStringArray& a )
{
    MFCStringArray::const_iterator itr = v.begin();
    for( ; itr != v.end(); ++itr )
    {
        a.append( *itr );
    }
}
void AcStringHelper::R2C( const AcStringArray& a, CStringArray& v )
{
    for( int i = 0; i < a.length(); i++ )
    {
        v.Add( a[i].kACharPtr() );
    }
}
void AcStringHelper::C2R( const CStringArray& v, AcStringArray& a )
{
    for( int i = 0; i < v.GetSize(); i++ )
    {
        a.append( v.GetAt( i ) );
    }
}
