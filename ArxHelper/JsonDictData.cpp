#include "StdAfx.h"
#include "JsonDictData.h"
#include "HelperClass.h"
#include "ArxXRecordManager.h"
#include "Dao.h"
//////////////////////////////////////////////////////////////////
///////////////////// 成员函数实现 /////////////////////////////
/////////////////////////////////////////////////////////////////
JsonDictData::JsonDictData( const CString& user_key ) : DictData(), m_user_key( user_key )
{
}
BaseDao* JsonDictData::createDao()
{
    return JsonDictData::CreateDao( getDataSource(), m_user_key );
}
//////////////////////////////////////////////////////////////////
///////////////// 静态成员函数实现 ////////////////////////////
/////////////////////////////////////////////////////////////////
bool JsonDictData::GetAllFields( const AcDbObjectId& dictId, const CString& user_key, AcStringArray& fields )
{
    if( dictId.isNull() ) return false;
    AcStringArray entries;
    ArxDictHelper::GetAllEntries( dictId, user_key, entries );
    if( entries.isEmpty() ) return false;
    // 取第1个entry
    CString json_str = entries[0].kACharPtr();
    // 从json字符串中解析字段和值
    AcStringArray values;
    JsonHelper::Parse( json_str, fields, values );
    return !fields.isEmpty();
}
bool JsonDictData::GetAllDatas( const AcDbObjectId& dictId, const CString& user_key, AcStringArray& fields, AcStringArray& values )
{
    if( dictId.isNull() ) return false;
    AcStringArray entries;
    ArxDictHelper::GetAllEntries( dictId, user_key, entries );
    if( entries.isEmpty() ) return false;
    // 取第1个entry
    CString json_str = entries[0].kACharPtr();
    // 从json字符串中解析字段和值
    JsonHelper::Parse( json_str, fields, values );
    return true;
}
bool JsonDictData::Init( const AcDbObjectId& dictId, const CString& user_key, const AcStringArray& fields, const AcStringArray& values )
{
    if( dictId.isNull() ) return false;
    if( fields.length() != values.length() ) return false;
    BaseDao* dao = JsonDictData::CreateDao( dictId, user_key );
    if( dao == 0 ) return false;
    dao->clearAllFields(); // 清空
    for( int i = 0; i < values.length(); i++ )
    {
        dao->setString( fields[i].kACharPtr(), values[i].kACharPtr() );
    }
    delete dao;
    return true;
}
bool JsonDictData::GetString( const AcDbObjectId& dictId, const CString& user_key, const CString& field, CString& value )
{
    if( dictId.isNull() ) return false;
    BaseDao* dao = JsonDictData::CreateDao( dictId, user_key );
    if( dao == 0 ) return false;
    bool ret = dao->getString( field, value );
    delete dao;
    return ret;
}
bool JsonDictData::GetInt( const AcDbObjectId& dictId, const CString& user_key, const CString& field, int& value )
{
	CString str;
	if( !JsonDictData::GetString( dictId, user_key, field, str ) ) return false;
	if( !StringHelper::StringToInt( str, value ) ) return false;
	return true;
}
bool JsonDictData::GetDouble( const AcDbObjectId& dictId, const CString& user_key, const CString& field, double& value )
{
	CString str;
	if( !JsonDictData::GetString( dictId, user_key, field, str ) ) return false;
	if( !StringHelper::StringToDouble( str, value ) ) return false;
	return true;
}
bool JsonDictData::GetBool( const AcDbObjectId& dictId, const CString& user_key, const CString& field, bool& value )
{
	CString str;
	if( !JsonDictData::GetString( dictId, user_key, field, str ) ) return false;
	int i = 0;
	if( !StringHelper::StringToInt( str, i ) ) return false;
	value = ( i != 0 );
	return true;
}
bool JsonDictData::GetPoint( const AcDbObjectId& dictId, const CString& user_key, const CString& field, AcGePoint3d& value )
{
	CString str;
	if( !JsonDictData::GetString( dictId, user_key, field, str ) ) return false;
	if( !ArxUtilHelper::StringToPoint3d( str, value ) ) return false;
	return true;
}
bool JsonDictData::GetVector( const AcDbObjectId& dictId, const CString& user_key, const CString& field, AcGeVector3d& value )
{
	CString str;
	if( !JsonDictData::GetString( dictId, user_key, field, str ) ) return false;
	AcGePoint3d pt;
	if( !ArxUtilHelper::StringToPoint3d( str, pt ) ) return false;
	value = pt.asVector();
	return true;
}
bool JsonDictData::GetDateTime( const AcDbObjectId& dictId, const CString& user_key, const CString& field, COleDateTime& value )
{
	CString str;
	if( !JsonDictData::GetString( dictId, user_key, field, str ) ) return false;
	return value.ParseDateTime( str );
}
bool JsonDictData::GetObjectId( const AcDbObjectId& dictId, const CString& user_key, const CString& field, AcDbObjectId& value )
{
	CString str;
	if( !JsonDictData::GetString( dictId, user_key, field, str ) ) return false;
	value = ArxUtilHelper::StrtoObjectId( str );
	return !value.isNull();
}
bool JsonDictData::SetString( const AcDbObjectId& dictId, const CString& user_key, const CString& field, const CString& value )
{
    if( dictId.isNull() ) return false;
    BaseDao* dao = JsonDictData::CreateDao( dictId, user_key );
    if( dao == 0 ) return false;
    bool ret = dao->setString( field, value );
    delete dao;
    return ret;
}
bool JsonDictData::SetInt( const AcDbObjectId& dictId, const CString& user_key, const CString& field, int value )
{
	CString str;
	StringHelper::IntToString( value, str );
	return JsonDictData::SetString( dictId, user_key, field, str );
}
bool JsonDictData::SetDouble( const AcDbObjectId& dictId, const CString& user_key, const CString& field, double value )
{
	CString str;
	StringHelper::DoubleToString( value, str );
	return JsonDictData::SetString( dictId, user_key, field, str );
}
bool JsonDictData::SetBool( const AcDbObjectId& dictId, const CString& user_key, const CString& field, bool value )
{
	CString str;
	StringHelper::IntToString( value ? 1 : 0, str );
	return JsonDictData::SetString( dictId, user_key, field, str );
}
bool JsonDictData::SetPoint( const AcDbObjectId& dictId, const CString& user_key, const CString& field, const AcGePoint3d& value )
{
	CString str = ArxUtilHelper::Point3dToString( value );
	return JsonDictData::SetString( dictId, user_key, field, str );
}
bool JsonDictData::SetVector( const AcDbObjectId& dictId, const CString& user_key, const CString& field, const AcGeVector3d& value )
{
	return JsonDictData::SetPoint( dictId, user_key, field, AcGePoint3d::kOrigin + value );
}
bool JsonDictData::SetDateTime( const AcDbObjectId& dictId, const CString& user_key, const CString& field, const COleDateTime& value )
{
	CString str = value.Format( _T( "%Y-%m-%d %H:%M:%S" ) );
	return JsonDictData::SetString( dictId, user_key, field, str );
}
bool JsonDictData::SetObjectId( const AcDbObjectId& dictId, const CString& user_key, const CString& field, const AcDbObjectId& value )
{
	CString str = ArxUtilHelper::ObjectIdToStr( value );
	return JsonDictData::SetString( dictId, user_key, field, str );
}
void JsonDictData::Copy( const AcDbObjectId& sourceObjId, const AcDbObjectId& targetObjId )
{
}
BaseDao* JsonDictData::CreateDao( const AcDbObjectId& dictId, const CString& user_key )
{
    if( dictId.isNull() ) return 0;
    if( user_key.IsEmpty() ) return 0;
    return new JsonDictDao( dictId, user_key );
}
