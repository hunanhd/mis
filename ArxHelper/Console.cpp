#include "stdafx.h"
#include "Console.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
#include <set>
#include <iostream>
struct ConsoleSingletonImpl
{
	// observer list
	std::set<ConsoleObserver* > _aclObservers;
};
// 实例化静态成员变量
Console* Console::ms_Singleton = 0;
Console::Console( void ) : _bVerbose( false )
{
	// 单件模式
	assert( !ms_Singleton );
	ms_Singleton = static_cast<Console*>( this );
	 d = new struct ConsoleSingletonImpl;
}
Console::~Console()
{
    for( std::set<ConsoleObserver* >::iterator Iter = d->_aclObservers.begin(); Iter != d->_aclObservers.end(); ++Iter )
	{
		delete ( *Iter );
	}
	delete d;
	// 单件模式
	assert( ms_Singleton );
	ms_Singleton = 0;
}
Console& Console::getSingleton( void )
{
	assert( ms_Singleton );
	return ( *ms_Singleton );
}
Console* Console::getSingletonPtr( void )
{
	return ( ms_Singleton );
}
void Console::Init()
{
	Console* console = new Console();
}
void Console::UnInit()
{
	delete Console::getSingletonPtr();
}
//**************************************************************************
// methods
/**
 *  sets the console in a special mode
 */
void Console::SetMode( ConsoleMode m )
{
    if( m && Verbose )
        _bVerbose = true;
}
/**
 *  unsets the console from a special mode
 */
void Console::UnsetMode( ConsoleMode m )
{
    if( m && Verbose )
        _bVerbose = false;
}
/**
 * \a type can be OR'ed with any of the FreeCAD_ConsoleMsgType flags to enable -- if \a b is true --
 * or to disable -- if \a b is false -- a console observer with name \a sObs.
 * The return value is an OR'ed value of all message types that have changed their state. For example
 * @code
 * // switch off warnings and error messages
 * ConsoleMsgFlags ret = Console().SetEnabledMsgType("myObs",
 *                             ConsoleMsgType::MsgType_Wrn|ConsoleMsgType::MsgType_Err, false);
 * // do something without notifying observer myObs
 * ...
 * // restore the former configuration again
 * Console().SetEnabledMsgType("myObs", ret, true);
 * @endcode
 * switches off warnings and error messages and restore the state before the modification.
 * If the observer \a sObs doesn't exist then nothing happens.
 */
int Console::SetEnabledMsgType( LPCTSTR sObs, int type, bool b )
{
    ConsoleObserver* pObs = Get( sObs );
    if ( pObs )
    {
        int flags = 0;
        if ( type & MsgType_Err )
        {
            if ( pObs->bErr != b )
                flags |= MsgType_Err;
            pObs->bErr = b;
        }
        if ( type & MsgType_Wrn )
        {
            if ( pObs->bWrn != b )
                flags |= MsgType_Wrn;
            pObs->bWrn = b;
        }
        if ( type & MsgType_Txt )
        {
            if ( pObs->bMsg != b )
                flags |= MsgType_Txt;
            pObs->bMsg = b;
        }
        if ( type & MsgType_Log )
        {
            if ( pObs->bLog != b )
                flags |= MsgType_Log;
            pObs->bLog = b;
        }
        return flags;
    }
    else
    {
        return 0;
    }
}
bool Console::IsMsgTypeEnabled( LPCTSTR sObs, Arx_ConsoleMsgType type ) const
{
    ConsoleObserver* pObs = Get( sObs );
    if ( pObs )
    {
        switch ( type )
        {
        case MsgType_Txt:
            return pObs->bMsg;
        case MsgType_Log:
            return pObs->bLog;
        case MsgType_Wrn:
            return pObs->bWrn;
        case MsgType_Err:
            return pObs->bErr;
        default:
            return false;
        }
    }
    else
    {
        return false;
    }
}
/** Prints a Message
 *  This method issues a Message.
 *  Messages are used show some non vital information. That means in the
 *  case FreeCAD running with GUI a Message in the status Bar apear. In console
 *  mode a message comes out.
 *  \par
 *  You can use a printf like interface like:
 *  \code
 *  Console().Message("Doing somthing important %d times\n",i);
 *  \endcode
 *  @see Warning
 *  @see Error
 *  @see Log
 */
void Console::Message( LPCTSTR pMsg, ... )
{
    TCHAR szFormat[4024];
    va_list namelessVars;
    va_start( namelessVars, pMsg ); // Get the "..." vars
    _vsntprintf( szFormat, sizeof(szFormat)/sizeof(TCHAR), pMsg, namelessVars );
    va_end( namelessVars );
    NotifyMessage( szFormat );
}
/** Prints a Message
 *  This method issues a Warning.
 *  Messages are used to get the users attantion. That means in the
 *  case FreeCAD running with GUI a Message Box is poping up. In console
 *  mode a colored message comes out! So dont use careless. For information
 *  purpose the Log or Message method is more aprobiated.
 *  \par
 *  You can use a printf like interface like:
 *  \code
 *  Console().Warning("Some defects in %s, loading anyway\n",str);
 *  \endcode
 *  @see Message
 *  @see Error
 *  @see Log
 */
void Console::Warning( LPCTSTR pMsg, ... )
{
    TCHAR szFormat[4024];
    va_list namelessVars;
    va_start( namelessVars, pMsg ); // Get the "..." vars
    _vsntprintf( szFormat, sizeof(szFormat)/sizeof(TCHAR), pMsg, namelessVars );
    va_end( namelessVars );
    NotifyWarning( szFormat );
}
/** Prints a Message
 *  This method issues an Error which makes some execution imposible.
 *  Errors are used to get the users attantion. That means in the
 *  case FreeCAD running with GUI a Error Message Box is poping up. In console
 *  mode a colored message comes out! So dont use this careless. For information
 *  purpose the Log or Message method is more aprobiated.
 *  \par
 *  You can use a printf like interface like:
 *  \code
 *  Console().Error("Somthing realy bad in %s happend\n",str);
 *  \endcode
 *  @see Message
 *  @see Warning
 *  @see Log
 */
void Console::Error( LPCTSTR pMsg, ... )
{
    TCHAR szFormat[4024];
    va_list namelessVars;
    va_start( namelessVars, pMsg ); // Get the "..." vars
    _vsntprintf( szFormat, sizeof(szFormat)/sizeof(TCHAR), pMsg, namelessVars );
    va_end( namelessVars );
    NotifyError( szFormat );
}

/** Prints a Message
 *  this method is more for devlopment and tracking purpos.
 *  It can be used to track execution of algorithms and functions
 *  and put it in files. The normal user dont need to see it, its more
 *  for developers and experinced users. So in normal user modes the
 *  logging is switched of.
 *  \par
 *  You can use a printf like interface like:
 *  \code
 *  Console().Log("Exectue part %d in algorithem %s\n",i,str);
 *  \endcode
 *  @see Message
 *  @see Warning
 *  @see Error
 */

void Console::Log( LPCTSTR pMsg, ... )
{
    TCHAR szFormat[4024];
    if ( !_bVerbose )
    {
        va_list namelessVars;
        va_start( namelessVars, pMsg ); // Get the "..." vars
        _vsntprintf( szFormat, sizeof(szFormat)/sizeof(TCHAR), pMsg, namelessVars );
        va_end( namelessVars );
        NotifyLog( szFormat );
    }
}
//**************************************************************************
// Observer stuff
/** Attaches an Observer to Console
 *  Use this method to attach a ConsoleObserver derived class to
 *  the Console. After the observer is attached all messages will also
 *  forwardet to it.
 *  @see ConsoleObserver
 */
void Console::AttachObserver( ConsoleObserver* pcObserver )
{
    // double insert !!
    assert( d->_aclObservers.find( pcObserver ) == d->_aclObservers.end() );
    d->_aclObservers.insert( pcObserver );
}
/** Detaches an Observer from Console
 *  Use this method to detach a ConsoleObserver derived class.
 *  After detaching you can destruct the Observer or reinsert it later.
 *  @see ConsoleObserver
 */
void Console::DetachObserver( ConsoleObserver* pcObserver )
{
    d->_aclObservers.erase( pcObserver );
}
void Console::NotifyMessage( LPCTSTR sMsg )
{
    for( std::set<ConsoleObserver* >::iterator Iter = d->_aclObservers.begin(); Iter != d->_aclObservers.end(); ++Iter )
    {
        if( ( *Iter )->bMsg )
            ( *Iter )->Message( sMsg ); // send string to the listener
    }
}
void Console::NotifyWarning( LPCTSTR sMsg )
{
    for( std::set<ConsoleObserver* >::iterator Iter = d->_aclObservers.begin(); Iter != d->_aclObservers.end(); ++Iter )
    {
        if( ( *Iter )->bWrn )
            ( *Iter )->Warning( sMsg ); // send string to the listener
    }
}
void Console::NotifyError( LPCTSTR sMsg )
{
    for( std::set<ConsoleObserver* >::iterator Iter = d->_aclObservers.begin(); Iter != d->_aclObservers.end(); ++Iter )
    {
        if( ( *Iter )->bErr )
            ( *Iter )->Error( sMsg ); // send string to the listener
    }
}
void Console::NotifyLog( LPCTSTR sMsg )
{
    for( std::set<ConsoleObserver* >::iterator Iter = d->_aclObservers.begin(); Iter != d->_aclObservers.end(); ++Iter )
    {
        if( ( *Iter )->bLog )
            ( *Iter )->Log( sMsg ); // send string to the listener
    }
}
ConsoleObserver* Console::Get( LPCTSTR Name ) const
{
    LPCTSTR OName;
    for( std::set<ConsoleObserver* >::const_iterator Iter = d->_aclObservers.begin(); Iter != d->_aclObservers.end(); ++Iter )
    {
        OName = ( *Iter )->Name(); // get the name
        if( OName && _tcscmp( OName, Name ) == 0 )
            return *Iter;
    }
    return 0;
}
//=========================================================================
// some special observers
ConsoleObserverFile::ConsoleObserverFile( LPCTSTR sFileName )
: cFileStream() // can be in UTF8
{
	cFileStream.open( sFileName, std::ios::out | std::ios::app);
    if ( !cFileStream.is_open() )
        Console().Warning( _T("Cannot open log file '%s'.\n"), sFileName );
    // mark the file as a UTF-8 encoded file
    //unsigned char bom[3] = {0xef, 0xbb, 0xbf};
    //cFileStream.write( C2W( ( const char* )bom ), 3 * sizeof( TCHAR ) );
}
ConsoleObserverFile::~ConsoleObserverFile()
{
    cFileStream.close();
}
void ConsoleObserverFile::Warning( LPCTSTR sWarn )
{
    cFileStream << _T("\n[") << (LPCTSTR)StringHelper::TimeStamp() << _T("]")
		        << _T( "[WARN]" ) << sWarn;
    cFileStream.flush();
}
void ConsoleObserverFile::Message( LPCTSTR sMsg )
{
    cFileStream << _T("\n[") << (LPCTSTR)StringHelper::TimeStamp() << _T("]")
				<< _T( "[INFO]" ) << sMsg;
    cFileStream.flush();
}
void ConsoleObserverFile::Error  ( LPCTSTR sErr )
{
    cFileStream << _T("\n[") << (LPCTSTR)StringHelper::TimeStamp() << _T("]")
				<< _T( "[ERROR]" ) << sErr;
    cFileStream.flush();
}
void ConsoleObserverFile::Log    ( LPCTSTR sLog )
{
    cFileStream << _T("\n[") << (LPCTSTR)StringHelper::TimeStamp() << _T("]")
				<< _T( "[LOG]" ) << sLog;
    cFileStream.flush();
}

ConsoleObserverStd::ConsoleObserverStd()
{
    bLog = false;
}
ConsoleObserverStd::~ConsoleObserverStd()
{
}
void ConsoleObserverStd::Message( LPCTSTR sMsg )
{
    acutPrintf( _T( "\n[%s][INFO]%s" ), (LPCTSTR)StringHelper::TimeStamp(), sMsg );
}
void ConsoleObserverStd::Warning( LPCTSTR sWarn )
{
    acutPrintf( _T( "\n[%s][WARN]%s" ), (LPCTSTR)StringHelper::TimeStamp(), sWarn );
}
void ConsoleObserverStd::Error  ( LPCTSTR sErr )
{
    acutPrintf( _T( "\n[%s][ERROR]%s" ), (LPCTSTR)StringHelper::TimeStamp(), sErr );
}
void ConsoleObserverStd::Log    ( LPCTSTR sErr )
{
    acutPrintf( _T( "\n[%s][LOG]%s" ), (LPCTSTR)StringHelper::TimeStamp(), sErr );
}
RedirectStdOutput::RedirectStdOutput()
{
    buffer.reserve( 80 );
}
int RedirectStdOutput::overflow( int c )
{
    if ( c != EOF )
        buffer.push_back( ( char )c );
    return c;
}
int RedirectStdOutput::sync()
{
    // Print as log as this might be verbose
    if ( !buffer.empty() )
    {
		Console::getSingletonPtr()->Log( _T("%s"), C2W( buffer.c_str() ) );
		buffer.clear();
    }
    return 0;
}
RedirectStdLog::RedirectStdLog() : buffer()
{
    buffer.reserve( 80 );
}
int RedirectStdLog::overflow( int c )
{
    if ( c != EOF )
        buffer.push_back( ( char )c );
    return c;
}
int RedirectStdLog::sync()
{
    // Print as log as this might be verbose
    if ( !buffer.empty() )
    {
        Console::getSingletonPtr()->Log( _T("%s"), C2W( buffer.c_str() ) );
        buffer.clear();
    }
    return 0;
}
RedirectStdError::RedirectStdError()
{
    buffer.reserve( 80 );
}
int RedirectStdError::overflow( int c )
{
    if ( c != EOF )
        buffer.push_back( ( char )c );
    return c;
}
int RedirectStdError::sync()
{
    if ( !buffer.empty() )
    {
        Console::getSingletonPtr()->Error( _T("%s"), C2W( buffer.c_str() ) );
        buffer.clear();
    }
    return 0;
}

StdOutSwitch::StdOutSwitch() : oldcout(0), oldclog(0), oldcerr(0)
{
	oldcout = std::cout.rdbuf(&newcout);
	oldclog = std::clog.rdbuf(&newclog);
	oldcerr = std::cerr.rdbuf(&newcerr);
}
StdOutSwitch::~StdOutSwitch()
{
	std::cout.rdbuf(oldcout);
	std::clog.rdbuf(oldclog);
	std::cerr.rdbuf(oldcerr);
}
ConsoleStdSwitch::ConsoleStdSwitch()
{
	Console::getSingletonPtr()->AttachObserver(&console_out);
}
ConsoleStdSwitch::~ConsoleStdSwitch()
{
	Console::getSingletonPtr()->DetachObserver(&console_out);
}
ConsoleFileSwitch::ConsoleFileSwitch(LPCTSTR sFileName) : file_out(sFileName)
{
	Console::getSingletonPtr()->AttachObserver(&file_out);
}
ConsoleFileSwitch::~ConsoleFileSwitch()
{
	Console::getSingletonPtr()->DetachObserver(&file_out);
}
