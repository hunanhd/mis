#include "StdAfx.h"
#include "DataObserverManager.h"
// 实例化静态成员变量
DataObserverManager* DataObserverManager::ms_Singleton = 0;
DataObserverManager::DataObserverManager( void )
{
	// 单件模式
	assert( !ms_Singleton );
	ms_Singleton = static_cast<DataObserverManager*>( this );
}
DataObserverManager::~DataObserverManager()
{
	// 断开所有的信号槽
	signalDictModify.disconnect_all();
	signalExtDictModify.disconnect_all();
	signalJsonDictModify.disconnect_all();
	signalXDataModify.disconnect_all();
	signalEntityDataModify.disconnect_all();
	// 单件模式
	assert( ms_Singleton );
	ms_Singleton = 0;
}
DataObserverManager& DataObserverManager::getSingleton( void )
{
	assert( ms_Singleton );
	return ( *ms_Singleton );
}
DataObserverManager* DataObserverManager::getSingletonPtr( void )
{
	return ( ms_Singleton );
}
void DataObserverManager::Init()
{
	DataObserverManager* console = new DataObserverManager();
}
void DataObserverManager::UnInit()
{
	delete DataObserverManager::getSingletonPtr();
}