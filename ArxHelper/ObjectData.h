#pragma once
#include "Data.h"
/*
 * 对象数据基类
 * 从图元或词典(DictData)中获取的数据称为"对象数据"
 * 其中图元中的数据有2种存储方法: 扩展词典ExtDictData、扩展数据XData
 */
class ARXHELPER_API ObjectData : public BaseData
{
public:
    /*
     * 设置数据源
     */
    void setDataSource( const AcDbObjectId& objId );
    /*
     * 获取数据源
     */
    AcDbObjectId getDataSource() const;
    /*
     * 读取或更新数据
     * 参数：
     *    save -- false表示将数据更新到变量
     *            true表示将变量更新到数据对象
     */
    bool update( bool save = false );
    /* 如果需要使用dao接口操作数据,请重载实现下面的2个虚函数. */
protected:
    // 注册字段并关联变量
    virtual void regDatas() = 0;
    // 创建dao接口
    virtual BaseDao* createDao() = 0;
protected:
    ObjectData();
protected:
    // 对象id(图元或词典)
    AcDbObjectId m_objId;
};
