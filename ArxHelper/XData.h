#pragma once
#include "ObjectData.h"
/*
 * 数据类(基于扩展数据实现)
 * 注: XData类是一个这里抽象类,因为它没重载虚函数createDao()
 *     这是特意设计的,因为扩展数据的数据量较少,使用的也不多, 对性能要求也不高,
 *     强制开发人员只能通过Dao指针或者静态成员函数操纵扩展数据
 */
class ARXHELPER_API XData : public ObjectData
{
public:
    //初始化扩展数据
    static bool Init( const AcDbObjectId& objId, int n );
    static bool Init( const AcDbObjectId& objId, const AcStringArray& values );
    // 获取属性数据
    // 如果索引位置pos不存在，返回false
    static bool GetString( const AcDbObjectId& objId, int pos, CString& value );
	static bool GetInt( const AcDbObjectId& objId, int pos, int& value );
	static bool GetDouble( const AcDbObjectId& objId, int pos, double& value );
	static bool GetBool( const AcDbObjectId& objId, int pos, bool& value );
	static bool GetPoint( const AcDbObjectId& objId, int pos, AcGePoint3d& value );
	static bool GetVector( const AcDbObjectId& objId, int pos, AcGeVector3d& value );
	static bool GetDateTime( const AcDbObjectId& objId, int pos, COleDateTime& value );
	static bool GetObjectId( const AcDbObjectId& objId, int pos, AcDbObjectId& value );
    // 修改属性数据
    // 如果索引位置pos不存在，返回false
    static bool SetString( const AcDbObjectId& objId, int pos, const CString& value );
	static bool SetInt( const AcDbObjectId& objId, int pos, int value );
	static bool SetDouble( const AcDbObjectId& objId, int pos, double value );
	static bool SetBool( const AcDbObjectId& objId, int pos, bool value );
	static bool SetPoint( const AcDbObjectId& objId, int pos, const AcGePoint3d& value );
	static bool SetVector( const AcDbObjectId& objId, int pos, const AcGeVector3d& value );
	static bool SetDateTime( const AcDbObjectId& objId, int pos, const COleDateTime& value );
	static bool SetObjectId( const AcDbObjectId& objId, int pos, const AcDbObjectId& value );
    // 复制属性数据(未实现)
    static void Copy( const AcDbObjectId& sourceObjId, const AcDbObjectId& targetObjId );
    // 创建扩展数据操作dao
    static BaseDao* CreateDao( const AcDbObjectId& objId );
protected:
	// 重载虚函数
    //virtual Dao* createDao();
};
