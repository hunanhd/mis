#pragma once
/*
* Console类的使用参见VVLoader/acrxEntryPoint.cpp中的VVTest_testOut()函数
*/
#include "dlimexp.h"
#include <iostream>
#include <string>
#include <fstream>
#include "AcFStream.h"
/** The console observer class
*  This class distribute the Messages issued to the FCConsole class.
*  If you need to catch some of the Messages you need to inherit from
*  this class and implement some of the methods.
*  @see Console
*/
class ARXHELPER_API ConsoleObserver
{
public:
	ConsoleObserver() : bErr( true ), bMsg( true ), bLog( true ), bWrn( true ) {}
	virtual ~ConsoleObserver() {}
	/// get calles when a Warning is issued
	virtual void Warning( LPCTSTR ) {}
	/// get calles when a Message is issued
	virtual void Message( LPCTSTR ) {}
	/// get calles when a Error is issued
	virtual void Error  ( LPCTSTR ) = 0;
	/// get calles when a Log Message is issued
	virtual void Log    ( LPCTSTR ) {}
	virtual LPCTSTR Name( void ) { return 0L; }
	bool bErr, bMsg, bLog, bWrn;
};

/** The console class
*  This class manage all the stdio stuff. This includes
*  Messages, Warnings, Log entries and Errors. The incomming
*  Messages are distributed with the FCConsoleObserver. The
*  FCConsole class itself makes no IO, it's more like a manager.
*  \par
*  ConsoleSingleton is a singleton! That means you can access the only
*  instance of the class from every where in c++ by simply using:
*  \code
*  #include <Base/Console.h>
*  Console().Log("Stage: %d",i);
*  \endcode
*  \par
*  ConsoleSingleton is abel to switch between several modes to, e.g. switch
*  the logging on or off, or treat Warnings as Errors, and so on...
*  @see ConsoleObserver
*/
class ARXHELPER_API Console
{
public:
	static void Init();
	static void UnInit();
	static Console& getSingleton( void );
	static Console* getSingletonPtr( void );
public:
	Console( void );
	virtual ~Console();
	// exported functions goes here +++++++++++++++++++++++++++++++++++++++
	/// Prints a Message
	virtual void Message ( LPCTSTR pMsg, ... ) ;
	/// Prints a warning Message
	virtual void Warning ( LPCTSTR pMsg, ... ) ;
	/// Prints a error Message
	virtual void Error   ( LPCTSTR pMsg, ... ) ;
	/// Prints a log Message
	virtual void Log     ( LPCTSTR pMsg, ... ) ;
	/// Attaches an Observer to FCConsole
	void AttachObserver( ConsoleObserver* pcObserver );
	/// Detaches an Observer from FCConsole
	void DetachObserver( ConsoleObserver* pcObserver );
	/// enumaration for the console modes
	enum ConsoleMode
	{
		Verbose = 1,	// supress Log messages
	};
	enum Arx_ConsoleMsgType
	{
		MsgType_Txt = 1,
		MsgType_Log = 2,
		MsgType_Wrn = 4,
		MsgType_Err = 8
	} ;
	/// Change mode
	void SetMode( ConsoleMode m );
	/// Change mode
	void UnsetMode( ConsoleMode m );
	/// Enables or disables message types of a cetain console observer
	int SetEnabledMsgType( LPCTSTR sObs, int type, bool b );
	/// Enables or disables message types of a cetain console observer
	bool IsMsgTypeEnabled( LPCTSTR sObs, Arx_ConsoleMsgType type ) const;
	// retrieval of an observer by name
	ConsoleObserver* Get( LPCTSTR Name ) const;
protected:
	bool _bVerbose;
private:
	// observer processing
	void NotifyMessage( LPCTSTR sMsg );
	void NotifyWarning( LPCTSTR sMsg );
	void NotifyError  ( LPCTSTR sMsg );
	void NotifyLog    ( LPCTSTR sMsg );
	struct ConsoleSingletonImpl* d;
	static Console* ms_Singleton;
};
// vs2005版本号是1400
#if  _MSC_VER > 1400
#define ARX_INFO(msg, ...) Console::getSingletonPtr()->Message(msg, __VA_ARGS__)
#define ARX_LOG(msg, ...) Console::getSingletonPtr()->Log(msg, __VA_ARGS__)
#define ARX_WARN(msg, ...) Console::getSingletonPtr()->Warning(msg, __VA_ARGS__)
#define ARX_ERROR(msg, ...) Console::getSingletonPtr()->Error(msg, __VA_ARGS__)
#else
#define ARX_INFO(msg) Console::getSingletonPtr()->Message(msg)
#define ARX_LOG(msg) Console::getSingletonPtr()->Log(msg)
#define ARX_WARN(msg) Console::getSingletonPtr()->Warning(msg)
#define ARX_ERROR(msg) Console::getSingletonPtr()->Error(msg)
#endif
//=========================================================================
// some special observers
/** The LoggingConsoleObserver class
*  This class is used by the main modules to write Console messages and logs to a file
*/
class ARXHELPER_API ConsoleObserverFile : public ConsoleObserver
{
public:
	ConsoleObserverFile( LPCTSTR sFileName );
	virtual ~ConsoleObserverFile();
	virtual void Warning( LPCTSTR sWarn );
	virtual void Message( LPCTSTR sMsg );
	virtual void Error  ( LPCTSTR sErr );
	virtual void Log    ( LPCTSTR sLog );
	LPCTSTR Name( void ) { return _T("File"); }
protected:
	AcOfstream cFileStream;
};
/** The CmdConsoleObserver class
*  This class is used by the main modules to write Console messages and logs the system con.
*/
class ARXHELPER_API ConsoleObserverStd: public ConsoleObserver
{
public:
	ConsoleObserverStd();
	virtual ~ConsoleObserverStd();
	virtual void Warning( LPCTSTR sWarn );
	virtual void Message( LPCTSTR sMsg );
	virtual void Error  ( LPCTSTR sErr );
	virtual void Log    ( LPCTSTR sErr );
	LPCTSTR Name( void ) { return _T("Console"); }
};
/*
C++标准输出流重定向,代码来自于FreeCAD,但在arx里试验不成功!!!!
参考资料: 
(1) c++里关于cerr,clog,cout三者的区别
http://blog.csdn.net/zx824/article/details/6644455
(2) 用streambuf简单封装socket
http://blog.csdn.net/gjw198276/article/details/2077274
*/
class ARXHELPER_API RedirectStdOutput : public std::streambuf
{
public:
	RedirectStdOutput();
protected:
	int overflow( int c = EOF );
	int sync();
private:
	std::string buffer;
};
class ARXHELPER_API RedirectStdError : public std::streambuf
{
public:
	RedirectStdError();
protected:
	int overflow( int c = EOF );
	int sync();
private:
	std::string buffer;
};
class ARXHELPER_API RedirectStdLog : public std::streambuf
{
public:
	RedirectStdLog();
protected:
	int overflow( int c = EOF );
	int sync();
private:
	std::string buffer;
};
class ARXHELPER_API StdOutSwitch
{
public:
	StdOutSwitch();
	~StdOutSwitch();
private:
	RedirectStdOutput newcout;
	RedirectStdLog    newclog;
	RedirectStdError  newcerr;
	// 当前C++的标准输出缓存
	std::streambuf* oldcout;
	std::streambuf* oldclog;
	std::streambuf* oldcerr;
};
// 使用该类Console必须先初始化!!!
class ARXHELPER_API ConsoleStdSwitch
{
public:
	ConsoleStdSwitch();
	~ConsoleStdSwitch();
private:
	ConsoleObserverStd console_out;
};
class ARXHELPER_API ConsoleFileSwitch
{
public:
	ConsoleFileSwitch(LPCTSTR sFileName);
	~ConsoleFileSwitch();
private:
	ConsoleObserverFile file_out;
};
