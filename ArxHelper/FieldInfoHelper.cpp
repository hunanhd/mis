#include "StdAfx.h"
#include "FieldInfoHelper.h"
#include "HelperClass.h"
#include "config.h"
static CString MakeFieldInfoKey( const CString& type, const CString& field )
{
    CString key;
    key.Format( _T( "%s_%s" ), type, field );
    return key;
}
bool FieldInfoHelper::GetFieldInfo( const CString& type, const CString& field, FieldInfo& info )
{
    // 读取字段信息数据
    ArxDictTool* pDictTool = ArxDictTool::GetDictTool( PROPERTY_DATA_FIELD_INFO_DICT );
 	CString json_str;
	bool ret = pDictTool->getEntry(MakeFieldInfoKey( type, field ), 1, json_str);    
	if( ret ) 
	{
		info = FieldInfo::FromJson(json_str);
	}
	delete pDictTool;
	return ret;
}
bool FieldInfoHelper::UpdateFieldInfo( const CString& type, const CString& field, const FieldInfo& info )
{
    // 读取字段信息数据
    ArxDictTool* pDictTool = ArxDictTool::GetDictTool( PROPERTY_DATA_FIELD_INFO_DICT );
    CString json_str;
    bool ret = pDictTool->getEntry( MakeFieldInfoKey( type, field ), 1, json_str );
    if( ret )
	{
		json_str = FieldInfo::ToJson(info);
		pDictTool->modifyEntry( MakeFieldInfoKey( type, field ), 1, json_str );
	}
	delete pDictTool;
	return ret;
}
bool FieldInfoHelper::RemoveFieldInfo( const CString& type, const CString& field )
{
    ArxDictTool* pDictTool = ArxDictTool::GetDictTool( PROPERTY_DATA_FIELD_INFO_DICT );
    bool ret = pDictTool->removeKey( MakeFieldInfoKey( type, field ) );
    delete pDictTool;
	return ret;
}
bool FieldInfoHelper::AddFieldInfo( const CString& type, const CString& field, const FieldInfo& info )
{
	// 读取字段信息数据
	ArxDictTool* pDictTool = ArxDictTool::GetDictTool( PROPERTY_DATA_FIELD_INFO_DICT );
	bool ret = pDictTool->findKey( MakeFieldInfoKey( type, field ) );
	CString json_str = FieldInfo::ToJson(info);
	if( ret )
	{
		ret = pDictTool->modifyEntry( MakeFieldInfoKey( type, field ), 1, json_str );
	}
	else
	{
		ret = pDictTool->addEntry( MakeFieldInfoKey( type, field ), json_str );
	}
	delete pDictTool;
	if(!ret)
	{
		acutPrintf( _T( "\n增加%s字段信息失败" ), field );
	}
	return ret;
}
void FieldInfoHelper::RemoveMoreFieldsInfo( const CString& type, const AcStringArray& fields )
{
    ArxDictTool* pDictTool = ArxDictTool::GetDictTool( PROPERTY_DATA_FIELD_INFO_DICT );
    int n = fields.length();
    for( int i = 0; i < n; i++ )
    {
        pDictTool->removeKey( MakeFieldInfoKey( type, fields[i].kACharPtr() ) );
    }
    delete pDictTool;
}
