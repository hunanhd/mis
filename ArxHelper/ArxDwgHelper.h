#pragma once
#include "dlimexp.h"
#include <map>
typedef std::map<CString, AcDbBlockTableRecord::PreviewIcon> IconMap;
/**
* dwg数据库辅助操作类.
*/
class ARXHELPER_API ArxDwgHelper
{
public:
    // 将另一个dwg中的块定义合并到当前数据库中
    static bool MergeBlocks( const CString& dwgFilePath );
    // 获取dwg文件中的所有块名称
    static bool GetAllBlockNames( const CString& dwgFilePath, AcStringArray& allBlocks );
    // 删除块定义(未实现,删除块有较大风险!!!)
    static void EraseBlocks( const AcStringArray& allBlocks );
    // 获取块略缩图
    static bool GetBlockIcons( const CString& dwgFilePath, IconMap& icons );
    // 更新图形
    static void UpdateDwg();
    // 获取dwg所在路径
    static CString GetDwgDirectory( AcApDocument* pDoc );
    // 获取dwg文件名
    static CString GetDwgFileName( AcApDocument* pDoc );
    // dwg是否新建的?
    static bool IsNewDwg( AcApDocument* pDoc );
	// 弹出对话框,要求用户选择dwg文件
	static bool SelectDwg( const CString& msg, CString& dwgFilePath );
	// 从dwg文件中的某个图层中读取所有的直线
	static bool ReadAllLinesFromDwg( const CString& dwgFilePath, const CString& layerName, AcGePoint3dArray& sptArray, AcGePoint3dArray& eptArray );
};
