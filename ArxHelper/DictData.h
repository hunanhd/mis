#pragma once
#include "ObjectData.h"
/*
 * 数据类(基于命名对象词典实现)
 */
class ARXHELPER_API DictData : public ObjectData
{
public:
    // 获取词典的所有字段
    static bool GetAllFields( const AcDbObjectId& dictId, AcStringArray& fields );
    // 获取词典的所有字段以及值
    static bool GetAllDatas( const AcDbObjectId& dictId, AcStringArray& fields, AcStringArray& values );
	static bool GetDatas( const AcDbObjectId& dictId, const AcStringArray& fields, AcStringArray& values );
    // 导出词典的字段和值为json格式
    static bool ExportToJsonString( const AcDbObjectId& dictId, CString& json_str );
    // 从json字符串中导入数据
    static bool ImportFromJsonString( const AcDbObjectId& dictId, const CString& json_str );
    //初始化数据
    static bool Init( const AcDbObjectId& dictId, const AcStringArray& fields, const AcStringArray& values );
    // 获取属性数据
    // 如果字段不存在，返回false
    static bool GetString( const AcDbObjectId& dictId, const CString& field, CString& value );
	static bool GetInt( const AcDbObjectId& dictId, const CString& field, int& value );
	static bool GetDouble( const AcDbObjectId& dictId, const CString& field, double& value );
	static bool GetBool( const AcDbObjectId& dictId, const CString& field, bool& value );
	static bool GetPoint( const AcDbObjectId& dictId, const CString& field, AcGePoint3d& value );
	static bool GetVector( const AcDbObjectId& dictId, const CString& field, AcGeVector3d& value );
	static bool GetDateTime( const AcDbObjectId& dictId, const CString& field, COleDateTime& value );
	static bool GetObjectId( const AcDbObjectId& dictId, const CString& field, AcDbObjectId& value );
    // 修改属性数据
    static bool SetString( const AcDbObjectId& dictId, const CString& field, const CString& value );
	static bool SetInt( const AcDbObjectId& dictId, const CString& field, int value );
	static bool SetDouble( const AcDbObjectId& dictId, const CString& field, double value );
	static bool SetBool( const AcDbObjectId& dictId, const CString& field, bool value );
	static bool SetPoint( const AcDbObjectId& dictId, const CString& field, const AcGePoint3d& value );
	static bool SetVector( const AcDbObjectId& dictId, const CString& field, const AcGeVector3d& value );
	static bool SetDateTime( const AcDbObjectId& dictId, const CString& field, const COleDateTime& value );
	static bool SetObjectId( const AcDbObjectId& dictId, const CString& field, const AcDbObjectId& value );
    // 复制属性数据(未实现)
    static void Copy( const AcDbObjectId& sourceObjId, const AcDbObjectId& targetObjId );
    // 创建词典操作dao
    static BaseDao* CreateDao( const AcDbObjectId& dictId );
protected:
    // 构造函数
    DictData();
    // 重载Data基类的虚函数
    virtual BaseDao* createDao();
};
