#pragma once
#include "DictData.h"
/*
 * 数据类(基于扩展词典实现)
 */
class ARXHELPER_API ExtDictData : public DictData
{
public:
    // 获取图元注册的所有字段
    static bool GetAllFields( const AcDbObjectId& objId, AcStringArray& fields );
    // 获取图元的所有字段以及值
    static bool GetAllDatas( const AcDbObjectId& objId, AcStringArray& fields, AcStringArray& values );
	static bool GetDatas(const AcDbObjectId& objId, const AcStringArray& fields, AcStringArray& values);
	// 导出图元的字段和值为json格式
    static bool ExportToJsonString( const AcDbObjectId& objId, CString& json_str );
    // 从json字符串中导入数据
    static bool ImportFromJsonString( const AcDbObjectId& objId, const CString& json_str );
    //初始化数据
    static bool Init( const AcDbObjectId& objId, const AcStringArray& fields, const AcStringArray& values );
    // 获取属性数据
    // 如果字段不存在，返回false
    static bool GetString( const AcDbObjectId& objId, const CString& field, CString& value );
	static bool GetInt( const AcDbObjectId& objId, const CString& field, int& value );
	static bool GetDouble( const AcDbObjectId& objId, const CString& field, double& value );
	static bool GetBool( const AcDbObjectId& objId, const CString& field, bool& value );
	static bool GetPoint( const AcDbObjectId& objId, const CString& field, AcGePoint3d& value );
	static bool GetVector( const AcDbObjectId& objId, const CString& field, AcGeVector3d& value );
	static bool GetDateTime( const AcDbObjectId& objId, const CString& field, COleDateTime& value );
	static bool GetObjectId( const AcDbObjectId& objId, const CString& field, AcDbObjectId& value );
    // 修改属性数据
    // 如果字段不存在，返回false
    static bool SetString( const AcDbObjectId& objId, const CString& field, const CString& value );
	static bool SetInt( const AcDbObjectId& objId, const CString& field, int value );
	static bool SetDouble( const AcDbObjectId& objId, const CString& field, double value );
	static bool SetBool( const AcDbObjectId& objId, const CString& field, bool value );
	static bool SetPoint( const AcDbObjectId& objId, const CString& field, const AcGePoint3d& value );
	static bool SetVector( const AcDbObjectId& objId, const CString& field, const AcGeVector3d& value );
	static bool SetDateTime( const AcDbObjectId& objId, const CString& field, const COleDateTime& value );
	static bool SetObjectId( const AcDbObjectId& objId, const CString& field, const AcDbObjectId& value );
    // 复制属性数据
    static bool Copy( const AcDbObjectId& sourceObjId, const AcDbObjectId& targetObjId );
    // 创建扩展词典操作dao
    static BaseDao* CreateDao( const AcDbObjectId& objId );
protected:
    // 构造函数
    ExtDictData();
    // 重载Data基类的虚函数
    virtual BaseDao* createDao();
};
