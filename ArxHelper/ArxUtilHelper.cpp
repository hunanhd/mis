#include "StdAfx.h"
#include "ArxUtilHelper.h"
#include "ArxDataTool.h"
#include "ArxEntityHelper.h"
#include "ArxClassHelper.h"
#include "Tool/HelperClass.h"
#include <cmath>
#include "ReactorHelper.h"
static AcDbBlockTableRecord* openCurrentSpaceBlock( AcDb::OpenMode mode, AcDbDatabase* db )
{
    AcDbBlockTableRecord* blkRec;
    Acad::ErrorStatus es = acdbOpenObject( blkRec, db->currentSpaceId(), mode );
    if ( es != Acad::eOk )
        return NULL;
    else
        return blkRec;
}
bool ArxUtilHelper::PostToModelSpace( AcDbEntity* pEnt )
{
    AcDbBlockTableRecord* blkRec = openCurrentSpaceBlock(
                                       AcDb::kForWrite,
                                       acdbHostApplicationServices()->workingDatabase() );
    if ( blkRec == NULL ) return false;
    bool ret = ( Acad::eOk == blkRec->appendAcDbEntity( pEnt ) );
    blkRec->close();
    // 注意关闭顺序(块表记录-->实体指针)
    // 最保险的做法：打开之后立即关闭!!!
    if( ret )
    {
        pEnt->close();
    }
    return ret;
}
AcDbObjectId ArxUtilHelper::SelectEntity( const CString& msg )
{
    ads_name en;
    ads_point pt;
    CString fmsg;
    fmsg.Format( _T( "\n%s" ), msg ); // 添加一个换行符
    if( RTNORM != acedEntSel( fmsg, en, pt ) ) return AcDbObjectId::kNull;
    acutPrintf( _T( "\n" ) );
    AcDbObjectId eId;
    if ( acdbGetObjectId( eId, en ) != Acad::eOk ) return AcDbObjectId::kNull;
    return eId;
}
void ArxUtilHelper::SelectMoreEntity( const CString& msg, AcDbObjectIdArray& objIds )
{
    // 打印提示信息
    acutPrintf( msg );
    ads_name ss;
    if( RTNORM != acedSSGet( NULL, NULL, NULL, NULL, ss ) ) return;
    long n = 0;
    acedSSLength ( ss, &n );
    for( long i = 0; i < n; i++ )
    {
        ads_name ename;
        if( RTNORM != acedSSName ( ss, i, ename ) ) continue;
        AcDbObjectId objId;
        if( Acad::eOk != acdbGetObjectId( objId, ename ) ) continue;
        objIds.append( objId );
    }
    acedSSFree( ss );
}
void ArxUtilHelper::GetPickSetEntity2( AcDbObjectIdArray& objIds )
{
    resbuf* pset;  // standard out of mem handler??
    if( RTNORM != acedSSGetFirst( NULL, &pset ) ) return;
    ads_name ss;
    ads_name_set( pset->resval.rlname, ss );
    //acutPrintf( _T( "\nseett   type:%d" ), pset->restype );
    acutRelRb( pset );
    //acutPrintf( _T( "\n遍历选择集.." ) );
    long n = 0;
    acedSSLength ( ss, &n );
    for( long i = 0; i < n; i++ )
    {
        ads_name ename;
        if( RTNORM != acedSSName ( ss, i, ename ) ) continue;
        AcDbObjectId objId;
        if( Acad::eOk != acdbGetObjectId( objId, ename ) ) continue;
        objIds.append( objId );
    }
    acedSSFree( ss );
    //acutPrintf( _T( "\n释放选择集.." ) );
}
void ArxUtilHelper::GetPickSetEntity( AcDbObjectIdArray& objIds )
{
    ads_name ss;
    int ret = acedSSGet( _T( "I" ), NULL, NULL, NULL, ss );
    if( RTNORM != ret ) return;
    long n = 0;
    acedSSLength ( ss, &n );
    for( long i = 0; i < n; i++ )
    {
        ads_name ename;
        if( RTNORM != acedSSName ( ss, i, ename ) ) continue;
        AcDbObjectId objId;
        if( Acad::eOk != acdbGetObjectId( objId, ename ) ) continue;
        objIds.append( objId );
    }
    acedSSFree( ss );
}
void ArxUtilHelper::Pause( const CString& msg )
{
    // 中断
    ACHAR tt[100];
    acedGetString( 0, msg, tt );
}
bool ArxUtilHelper::IsEqualType( const CString& type, const AcDbObjectId& objId, bool isDerivedFromParent/*=true*/ )
{
    return ArxDataTool::IsEqualType( type, objId, true );
}
void ArxUtilHelper::ShowEntityWithColor( const AcDbObjectId& objId, unsigned short colorIndex )
{
    AcDbObjectIdArray objIds;
    objIds.append( objId );
    ArxUtilHelper::ShowEntitiesWithColor( objIds, colorIndex );
}
void ArxUtilHelper::ShowEntitiesWithColor( AcDbObjectIdArray& objIds, unsigned short colorIndex )
{
    // 记录图元的原颜色
    AcArray<Adesk::UInt16> originColors;
    if( !ArxEntityHelper::GetEntitiesColor( objIds, originColors ) ) return;
    // 高亮显示图元
    ArxEntityHelper::SetEntitiesColor( objIds, colorIndex );
    // 暂停
    ArxUtilHelper::Pause();
    // 恢复图元原有颜色
    ArxEntityHelper::SetEntitiesColor2( objIds, originColors );
}
void ArxUtilHelper::ShowEntitiesWithColor2( AcDbObjectIdArray& objIds, const AcArray<Adesk::UInt16>& colors )
{
    // 记录图元的原颜色
    AcArray<Adesk::UInt16> originColors;
    if( !ArxEntityHelper::GetEntitiesColor( objIds, originColors ) ) return;
    // 高亮显示图元
    ArxEntityHelper::SetEntitiesColor2( objIds, colors );
    // 暂停
    ArxUtilHelper::Pause();
    // 恢复图元原有颜色
    ArxEntityHelper::SetEntitiesColor2( objIds, originColors );
}
double ArxUtilHelper::AdjustAngle( double angle )
{
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( angle, AcGeVector3d::kZAxis );
    return v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
}
bool ArxUtilHelper::GetPoint( const CString& msg, AcGePoint3d& pt, const AcGePoint3d& basePt )
{
    ads_point _pt;
    if( basePt == AcGePoint3d::kOrigin )
    {
        if( acedGetPoint( NULL, msg, _pt ) != RTNORM ) return false;
    }
    else
    {
        if( acedGetPoint( asDblArray( basePt ), msg, _pt ) != RTNORM ) return false;
    }
    pt = asPnt3d( _pt );
    return true;
}
double ArxUtilHelper::AngleOfTwoPoint( const AcGePoint3d& startPt, const AcGePoint3d& endPt )
{
    AcGeVector3d v = endPt - startPt;
    return v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
}
bool ArxUtilHelper::GetTwoPoint( AcGePoint3d& startPt, AcGePoint3d& endPt )
{
    ads_point start_pt, end_pt;
    if( acedGetPoint( NULL, _T( "\n请选择第一点: " ), start_pt ) != RTNORM )
    {
        acutPrintf( _T( "\n选择点坐标失败" ) );
        return false;
    }
    if( acedGetPoint( start_pt, _T( "\n请选择第二点：" ), end_pt ) != RTNORM )
    {
        acutPrintf( _T( "\n选择点坐标失败" ) );
        return false;
    }
    // 返回结果
    startPt = asPnt3d( start_pt );
    endPt = asPnt3d( end_pt );
    return true;
}
double ArxUtilHelper::AngleToXAxis( const AcGeVector3d& v )
{
    return v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
}
void ArxUtilHelper::BrowserEntities( const AcDbObjectIdArray& objIds )
{
    int len = objIds.length();
    for( int i = 0; i < len; i++ )
    {
        ArxEntityHelper::ZoomToEntity( objIds[i] );
        ArxUtilHelper::Pause(); // 暂停
    }
}
CString ArxUtilHelper::Point3dToString( const AcGePoint3d& pt )
{
    CString value;
    value.Format( _T( "(%.3f,%.3f,%.3f)" ), pt.x * 1.0, pt.y * 1.0, pt.z * 1.0 );
    return value;
}
bool ArxUtilHelper::StringToPoint3d( const CString& value, AcGePoint3d& pt )
{
    CStringArray values;
    StringHelper::SplitCString( value, _T( " (\t\n,;_)" ), values );
    if( values.GetCount() < 3 ) return false;
    double x = 0, y = 0, z = 0;
    if( !StringHelper::StringToDouble( values[0], x ) ) return false;
    if( !StringHelper::StringToDouble( values[1], y ) ) return false;
    if( !StringHelper::StringToDouble( values[2], z ) ) return false;
    pt.set( x, y, z );
    return true;
}
CString ArxUtilHelper::Vector3dToString( const AcGeVector3d& v )
{
    AcGePoint3d pt;
    pt += v;
    return ArxUtilHelper::Point3dToString( pt );
}
bool ArxUtilHelper::StringToVector3d( const CString& value, AcGeVector3d& v )
{
    AcGePoint3d pt;
    pt += v;
    if( StringToPoint3d( value, pt ) )
    {
        v = pt.asVector();
        return true;
    }
    else
    {
        return false;
    }
}
bool ArxUtilHelper::GetString( const CString& msg, CString& str )
{
    ACHAR _str[50];
    if( acedGetString( Adesk::kFalse, ( LPCTSTR )msg, _str ) != RTNORM )
    {
        return false;
    }
    str.Format( _T( "%s" ), _str );
    return true;
}
bool ArxUtilHelper::GetInt( const CString& msg, int& v )
{
    return RTNORM == acedGetInt( ( LPCTSTR )msg, &v );
}
bool ArxUtilHelper::GetReal( const CString& msg, double& v )
{
    return RTNORM == acedGetReal( ( LPCTSTR )msg, &v );
}
bool ArxUtilHelper::GetAngle( const AcGePoint3d& pt, const CString& msg, double& angle )
{
    ads_point _pt = {pt.x, pt.y, pt.z};
    return RTNORM == acedGetAngle( _pt, ( LPCTSTR )msg, &angle );
}
// DWORD <==> Adesk::UInt32 <==> unsigned long
// Adesk::UInt8 <==> unsigned char
void ArxUtilHelper::LONG_2_RGB( Adesk::UInt32 rgbColor, Adesk::UInt8& r, Adesk::UInt8& g, Adesk::UInt8& b )
{
    // 下面2段代码是等价的
    /*r = ( rgbColor & 0xffL );
    g = ( rgbColor & 0xff00L ) >> 8;
    b = rgbColor >> 16;*/
    r = GetRValue( rgbColor );
    g = GetGValue( rgbColor );
    b = GetBValue( rgbColor );
}
// 获取cad绘图窗口背景色
bool ArxUtilHelper::GetBackgroundColor( Adesk::UInt8& r, Adesk::UInt8& g, Adesk::UInt8& b )
{
    // 获取cad当前的所有颜色设置
    // 右键"选项"->"显示"->"颜色"
    AcColorSettings cs;
    if( !acedGetCurrentColors( &cs ) ) return false;
    // 读取背景颜色
    DWORD rgbColor = cs.dwGfxModelBkColor;
    ArxUtilHelper::LONG_2_RGB( rgbColor, r, g, b );
    return true;
}
// 从CAD的颜色(ACI)得到RGB
Adesk::UInt32 ArxUtilHelper::ACI_2_LONG( Adesk::UInt16 colorIndex )
{
    /* Looks up the RGB value.
     * colorIndex is an ACI that is to be mapped to an RGB value,
     * where the low byte is blue, the second byte is green, and the third byte is red.
     * The high byte is the color method.
     * lookUpRGB函数返回的颜色顺序: blue->green->red, 与正常的是相反的!!!
     */
    Adesk::UInt32 rgb = AcCmEntityColor::lookUpRGB( ( Adesk::UInt8 )colorIndex );
    Adesk::UInt8 b = GetRValue( rgb );
    Adesk::UInt8 g = GetGValue( rgb );
    Adesk::UInt8 r = GetBValue( rgb );
    return ( Adesk::UInt32 )RGB( r, g, b );
}
// 从rgb颜色得到CAD颜色(ACI)
Adesk::UInt16 ArxUtilHelper::RGB_2_ACI( Adesk::UInt8 r, Adesk::UInt8 g, Adesk::UInt8 b )
{
    return ( Adesk::UInt16 )AcCmEntityColor::lookUpACI( r, g, b );
}
void ArxUtilHelper::ACI_2_RGB( Adesk::UInt16 colorIndex, Adesk::UInt8& r, Adesk::UInt8& g, Adesk::UInt8& b )
{
    Adesk::UInt32 rgb = ArxUtilHelper::ACI_2_LONG( colorIndex );
    LONG_2_RGB( rgb, r, g, b );
}
Adesk::UInt32 ArxUtilHelper::RGB_2_LONG( Adesk::UInt8 r, Adesk::UInt8 g, Adesk::UInt8 b )
{
    return ( Adesk::UInt32 )RGB( r, g, b );
}
#ifndef _WIN64
AcDbObjectId ArxUtilHelper::GetObjectId( long id )
{
    AcDbObjectId objId;
    objId.setFromOldId( ( Adesk::LongPtr )id );
    return objId;
}
long ArxUtilHelper::GetLongId( const AcDbObjectId& objId )
{
    return ( long )objId.asOldId();
}
#else
AcDbObjectId ArxUtilHelper::GetObjectId( unsigned long id )
{
    AcDbObjectId objId;
    objId.setFromOldId( ( Adesk::UIntPtr )id );
    return objId;
}
unsigned long ArxUtilHelper::GetLongId( const AcDbObjectId& objId )
{
    return ( unsigned long )objId.asOldId();
}
#endif // _WIN64

CString ArxUtilHelper::HandleToStr( const AcDbHandle& handle )
{
    // 句柄字符串最大长度17个字符
    ACHAR buf[17];
    handle.getIntoAsciiBuffer( buf );
    return CString( buf );
}
AcDbHandle ArxUtilHelper::StrToHandle( const CString& str )
{
    return AcDbHandle( ( LPCTSTR )str );
}
AcDbObjectId ArxUtilHelper::StrtoObjectId( const CString& str )
{
    AcDbHandle handle = ArxUtilHelper::StrToHandle( str );
    AcDbObjectId objId;
    acdbHostApplicationServices()->workingDatabase()->getAcDbObjectId( objId, false, handle, 0 );
    return objId;
}
CString ArxUtilHelper::ObjectIdToStr( const AcDbObjectId& objId )
{
    return ArxUtilHelper::HandleToStr( objId.handle() );
}
// 废弃代码
/*
Adesk::UInt32 rgbColor = AcCmEntityColor::lookUpRGB(cl.colorIndex()); // 转换成rgb颜色
AcCmEntityColor bgColor(255-r, 255-g, 255-b); // RGB颜色
AcCmEntityColor bgColor;
bgColor.setRGB(0, 0, 0);
if(layerColor.colorIndex() == 7) bgColor.setRGB(r, g, b); // RGB颜色
else bgColor.setRGB(255-r, 255-g, 255-b);
*/
/*
* 在绘制闭合图形时，AcGiFillType默认为kAcGiFillAlways (始终填充)
* 闭合图形包括：圆、多边形、网格等
* 参见：AcGiSubEntityTraits::fillType()方法说明
* 例如，绘制一个圆，当前颜色是黑底白色，那么采用自定义实体绘制的圆有2种情况:
*	    1) arx程序加载的情况下-- 白边+黑底填充(正常效果，和cad的圆是一样的)
*		2) arx程序卸载，cad采用代理实体模式显示图元 -- 白边+白底填充
* 具体参见：绘制填充圆的一些说明.doc
*/
// 慎用AcCmColor::colorIndex()方法，因为color index总共只有256种，且白/黑都使用7表示，无法区分
// 如果要使用rgb颜色，应使用AcCmEntityColor或AcCmColor对象
CString ArxUtilHelper::LineWeightToStr( AcDb::LineWeight type )
{
    static AcDb::LineWeight lts[] =
    {
        AcDb::kLnWt000,	AcDb::kLnWt005,	AcDb::kLnWt009,
        AcDb::kLnWt013,	AcDb::kLnWt015,	AcDb::kLnWt018,
        AcDb::kLnWt020,	AcDb::kLnWt025,	AcDb::kLnWt030,
        AcDb::kLnWt035,	AcDb::kLnWt040,	AcDb::kLnWt050,
        AcDb::kLnWt053,	AcDb::kLnWt060,	AcDb::kLnWt070,
        AcDb::kLnWt080,	AcDb::kLnWt090,	AcDb::kLnWt100,
        AcDb::kLnWt106,	AcDb::kLnWt120,	AcDb::kLnWt140,
        AcDb::kLnWt158,	AcDb::kLnWt200,	AcDb::kLnWt211,
        AcDb::kLnWtByLayer,	AcDb::kLnWtByBlock,	AcDb::kLnWtByLwDefault
    };
    static CString lt_names[] =
    {
        _T( "0.00 mm" ), _T( "0.05 mm" ), _T( "0.09 mm" ),
        _T( "0.13 mm" ), _T( "0.15 mm" ), _T( "0.18 mm" ),
        _T( "0.20 mm" ), _T( "0.25 mm" ), _T( "0.30 mm" ),
        _T( "0.35 mm" ), _T( "0.40 mm" ), _T( "0.50 mm" ),
        _T( "0.53 mm" ), _T( "0.60 mm" ), _T( "0.70 mm" ),
        _T( "0.80 mm" ), _T( "0.90 mm" ), _T( "1.00 mm" ),
        _T( "1.06 mm" ), _T( "1.20 mm" ), _T( "1.40 mm" ),
        _T( "1.58 mm" ), _T( "2.00 mm" ), _T( "2.11 mm" ),
        _T( "ByLayer" ), _T( "ByBlock" ), _T( "ByLwDefault" )
    };
    CString str;
    for( int i = 0; i < sizeof( lts ) / sizeof( AcCmEntityColor::ColorMethod ); i++ )
    {
        if( type == lts[i] )
        {
            str = lt_names[i];
        }
    }
    return str;
}
CString ArxUtilHelper::ColorMethodToStr( AcCmEntityColor::ColorMethod eColorMethod )
{
    static AcCmEntityColor::ColorMethod cms[] =
    {
        AcCmEntityColor::kByLayer,
        AcCmEntityColor::kByBlock,
        AcCmEntityColor::kByColor,
        AcCmEntityColor::kByACI,
        AcCmEntityColor::kByPen,
        AcCmEntityColor::kForeground,
        AcCmEntityColor::kLayerOff,
        AcCmEntityColor::kLayerFrozen,
        AcCmEntityColor::kNone
    };
    static CString cm_names[] =
    {
        _T( "kByLayer" ),
        _T( "kByBlock" ),
        _T( "kByColor" ),
        _T( "kByACI" ),
        _T( "kByPen" ),
        _T( "kForeground" ),
        _T( "kLayerOff" ),
        _T( "kLayerFrozen" ),
        _T( "kNone" )
    };
    CString str;
    for( int i = 0; i < sizeof( cms ) / sizeof( AcCmEntityColor::ColorMethod ); i++ )
    {
        if( eColorMethod == cms[i] )
        {
            str = cm_names[i];
        }
    }
    return str;
}
AcDb::LineWeight ArxUtilHelper::StrToLineWeight( const CString& str )
{
    static AcDb::LineWeight lts[] =
    {
        AcDb::kLnWt000,	AcDb::kLnWt005,	AcDb::kLnWt009,
        AcDb::kLnWt013,	AcDb::kLnWt015,	AcDb::kLnWt018,
        AcDb::kLnWt020,	AcDb::kLnWt025,	AcDb::kLnWt030,
        AcDb::kLnWt035,	AcDb::kLnWt040,	AcDb::kLnWt050,
        AcDb::kLnWt053,	AcDb::kLnWt060,	AcDb::kLnWt070,
        AcDb::kLnWt080,	AcDb::kLnWt090,	AcDb::kLnWt100,
        AcDb::kLnWt106,	AcDb::kLnWt120,	AcDb::kLnWt140,
        AcDb::kLnWt158,	AcDb::kLnWt200,	AcDb::kLnWt211,
        AcDb::kLnWtByLayer,	AcDb::kLnWtByBlock,	AcDb::kLnWtByLwDefault
    };
    static CString lt_names[] =
    {
        _T( "0.00 mm" ), _T( "0.05 mm" ), _T( "0.09 mm" ),
        _T( "0.13 mm" ), _T( "0.15 mm" ), _T( "0.18 mm" ),
        _T( "0.20 mm" ), _T( "0.25 mm" ), _T( "0.30 mm" ),
        _T( "0.35 mm" ), _T( "0.40 mm" ), _T( "0.50 mm" ),
        _T( "0.53 mm" ), _T( "0.60 mm" ), _T( "0.70 mm" ),
        _T( "0.80 mm" ), _T( "0.90 mm" ), _T( "1.00 mm" ),
        _T( "1.06 mm" ), _T( "1.20 mm" ), _T( "1.40 mm" ),
        _T( "1.58 mm" ), _T( "2.00 mm" ), _T( "2.11 mm" ),
        _T( "ByLayer" ), _T( "ByBlock" ), _T( "ByLwDefault" )
    };
    AcDb::LineWeight lt = AcDb::kLnWtByLayer;
    for( int i = 0; i < sizeof( lts ) / sizeof( AcCmEntityColor::ColorMethod ); i++ )
    {
        if( str.CompareNoCase( lt_names[i] ) == 0 )
        {
            lt = lts[i];
            break;
        }
    }
    return lt;
}
AcCmEntityColor::ColorMethod ArxUtilHelper::StrToColorMethod( const CString& str )
{
    static AcCmEntityColor::ColorMethod cms[] =
    {
        AcCmEntityColor::kByLayer,
        AcCmEntityColor::kByBlock,
        AcCmEntityColor::kByColor,
        AcCmEntityColor::kByACI,
        AcCmEntityColor::kByPen,
        AcCmEntityColor::kForeground,
        AcCmEntityColor::kLayerOff,
        AcCmEntityColor::kLayerFrozen,
        AcCmEntityColor::kNone
    };
    static CString cm_names[] =
    {
        _T( "kByLayer" ),
        _T( "kByBlock" ),
        _T( "kByColor" ),
        _T( "kByACI" ),
        _T( "kByPen" ),
        _T( "kForeground" ),
        _T( "kLayerOff" ),
        _T( "kLayerFrozen" ),
        _T( "kNone" )
    };
    AcCmEntityColor::ColorMethod cm = AcCmEntityColor::kByLayer;
    for( int i = 0; i < sizeof( cms ) / sizeof( AcCmEntityColor::ColorMethod ); i++ )
    {
        if( str.CompareNoCase( cm_names[i] ) )
        {
            cm = cms[i];
            break;
        }
    }
    return cm;
}
AcDbObjectId ArxUtilHelper::CreateEntity( const CString& type )
{
    if( !ArxClassHelper::IsDerivedFrom( type, _T( "AcDbEntity" ) ) ) return AcDbObjectId::kNull;
    AcDbObject* pObj = ArxClassHelper::CreateObjectByType( type );
    if( pObj == 0 ) return AcDbObjectId::kNull;
    AcDbEntity* pEnt = AcDbEntity::cast( pObj );
    if( pEnt == 0 )
    {
        delete pObj;
        return AcDbObjectId::kNull;
    }
    if( !ArxUtilHelper::PostToModelSpace( pEnt ) )
    {
        delete pEnt;
        return AcDbObjectId::kNull;
    }
    return pEnt->objectId();
}
void ArxUtilHelper::Esc( int n )
{
	HWND cadMainWnd = acedGetAcadFrame()->GetSafeHwnd();
	CString cmd;
	for( int i=0;i<n;i++ )
	{
		cmd.AppendFormat( _T( "\003" ) );
	}
	CADHelper::SendCommandToAutoCAD( cadMainWnd, cmd, false );
}
/**
参考:
http://blog.sina.com.cn/s/blog_62c2373a0100fqfs.html
http://www.cnblogs.com/lihao102/archive/2013/04/14/3020222.html
*/
extern long acdbSetDbmod(AcDbDatabase* pDb, long newVal); // 添加acdbSetDbmod的引用
void ArxUtilHelper::CloseDocument( AcApDocument* pDoc, bool needPrompt )
{
	if( !needPrompt )
	{
		acdbSetDbmod(curDoc()->database(), 0); // 就是這句代碼起了決定性作用
	}
	// 关闭文档
	::acDocManager->closeDocument( curDoc() );
}

AcDbObjectId ArxUtilHelper::GetApertureEntity()
{
    return ReactorHelper::GetApertureEntity();
}
