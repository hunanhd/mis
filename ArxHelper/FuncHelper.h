#pragma once
#include "dlimexp.h"
class ARXHELPER_API FuncHelper
{
public:
    static bool GetFuncs( const CString& type, AcStringArray& funcs );                  // 获取所有功能
    static bool GetTypes( const CString& func, AcStringArray& types );                      // 获取功能下所有的类型
    static bool GetFields( const CString& type, const CString& func, AcStringArray& fields ); // 获取字段
    //static bool RemoveFunction( const CString& func );                                    // 删除功能
    //static bool RemoveType( const CString& func, const CString& type );                   // 删除类型
};