#include "StdAfx.h"
#include "EntityData.h"
#include "Entity.h"
#include "Dao.h"
#include "HelperClass.h"
#include "BuiltinDataManager.h"
#include "AcDbObjectData.h"
//////////////////////////////////////////////////////////////////
///////////////////// 成员函数实现 /////////////////////////////
/////////////////////////////////////////////////////////////////
BaseDao* EntityData::createDao()
{
    return EntityData::CreateDao( getDataSource() );
}
//////////////////////////////////////////////////////////////////
///////////////// 静态成员函数实现 ////////////////////////////
/////////////////////////////////////////////////////////////////
bool EntityData::GetAllFields( const AcDbObjectId& objId, AcStringArray& fields )
{
    AcTransaction* pTrans = acTransactionManagerPtr()->startTransaction();
    AcDbObject* pObj = 0;
    if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForRead ) )
    {
        acTransactionManagerPtr()->abortTransaction();
        return false;
    }
    BaseData* pData = 0;
    BaseEntity* pEnt = BaseEntity::cast( pObj );
    if( pEnt != 0 )
    {
        pData = pEnt;
    }
    else
    {
        AcDbObjectData* pObjectData = BuiltinDataManager::getSingletonPtr()->getData( pObj->isA()->name() );
        if( pObjectData == 0 )
        {
            actrTransactionManager->abortTransaction();
            return false;
        }
        else
        {
            pObjectData->setObject( pObj );
            pData = pObjectData;
        }
    }
    if( pData == 0 )
    {
        actrTransactionManager->abortTransaction();
        return false;
    }
    pData->getAllFields( fields );
    acTransactionManagerPtr()->endTransaction();
    return true;
}
bool EntityData::GetAllDatas( const AcDbObjectId& objId, AcStringArray& fields, AcStringArray& values )
{
    if( !EntityData::GetAllFields( objId, fields ) ) return false;
    BaseDao* dao = EntityData::CreateDao( objId );
    if( dao == 0 ) return false;
    for( int i = 0; i < fields.length(); i++ )
    {
        CString value;
        bool ret = dao->getString( fields[i].kACharPtr(), value );
        values.append( value );
    }
    delete dao;
    return true;
}
bool EntityData::ExportToJsonString( const AcDbObjectId& objId, CString& json_str )
{
    AcStringArray fields, values;
    if( !EntityData::GetAllDatas( objId, fields, values ) )
    {
        json_str = NULL_JSON_STRING;
        return false;
    }
    else
    {
        // 将字段和值格式化成json字符串
        return JsonHelper::Format( fields, values, json_str );
    }
}
bool EntityData::ImportFromJsonString( const AcDbObjectId& objId, const CString& json_str )
{
    // 从json字符串中解析字段和值
    AcStringArray fields, values;
    if( !JsonHelper::Parse( json_str, fields, values ) ) return false;
    // 初始化数据
    return EntityData::Init( objId, fields, values );
}
bool EntityData::Init( const AcDbObjectId& objId, const AcStringArray& fields, const AcStringArray& values )
{
    if( objId.isNull() ) return false;
    if( fields.length() != values.length() ) return false;
    BaseDao* dao = EntityData::CreateDao( objId );
    if( dao == 0 ) return false;
    dao->clearAllFields(); // 清空
    for( int i = 0; i < values.length(); i++ )
    {
        dao->setString( fields[i].kACharPtr(), values[i].kACharPtr() );
    }
    delete dao;
    return true;
}
bool EntityData::SetString( const AcDbObjectId& objId, const CString& field, const CString& value )
{
    if( field.IsEmpty() ) return false;
    BaseDao* dao = EntityData::CreateDao( objId );
    if( dao == 0 ) return false;
    dao->setString( field, value );
    delete dao;
    return true;
}
bool EntityData::SetInt( const AcDbObjectId& objId, const CString& field, int value )
{
    CString str;
    StringHelper::IntToString( value, str );
    return EntityData::SetString( objId, field, str );
}
bool EntityData::SetDouble( const AcDbObjectId& objId, const CString& field, double value )
{
    CString str;
    StringHelper::DoubleToString( value, str );
    return EntityData::SetString( objId, field, str );
}
bool EntityData::SetBool( const AcDbObjectId& objId, const CString& field, bool value )
{
    CString str;
    StringHelper::IntToString( value ? 1 : 0, str );
    return EntityData::SetString( objId, field, str );
}
bool EntityData::SetPoint( const AcDbObjectId& objId, const CString& field, const AcGePoint3d& value )
{
    CString str = ArxUtilHelper::Point3dToString( value );
    return EntityData::SetString( objId, field, str );
}
bool EntityData::SetVector( const AcDbObjectId& objId, const CString& field, const AcGeVector3d& value )
{
    return EntityData::SetPoint( objId, field, AcGePoint3d::kOrigin + value );
}
bool EntityData::SetDateTime( const AcDbObjectId& objId, const CString& field, const COleDateTime& value )
{
    CString str = value.Format( _T( "%Y-%m-%d %H:%M:%S" ) );
    return EntityData::SetString( objId, field, str );
}
bool EntityData::SetObjectId( const AcDbObjectId& objId, const CString& field, const AcDbObjectId& value )
{
    CString str = ArxUtilHelper::ObjectIdToStr( value );
    return EntityData::SetString( objId, field, str );
}
bool EntityData::GetString( const AcDbObjectId& objId, const CString& field, CString& value )
{
    if( field.IsEmpty() ) return false;
    BaseDao* dao = EntityData::CreateDao( objId );
    if( dao == 0 ) return false;
    bool ret = dao->getString( field, value );
    delete dao;
    return ret;
}
bool EntityData::GetInt( const AcDbObjectId& objId, const CString& field, int& value )
{
    CString str;
    if( !EntityData::GetString( objId, field, str ) ) return false;
    if( !StringHelper::StringToInt( str, value ) ) return false;
    return true;
}
bool EntityData::GetDouble( const AcDbObjectId& objId, const CString& field, double& value )
{
    CString str;
    if( !EntityData::GetString( objId, field, str ) ) return false;
    if( !StringHelper::StringToDouble( str, value ) ) return false;
    return true;
}
bool EntityData::GetBool( const AcDbObjectId& objId, const CString& field, bool& value )
{
    CString str;
    if( !EntityData::GetString( objId, field, str ) ) return false;
    int i = 0;
    if( !StringHelper::StringToInt( str, i ) ) return false;
    value = ( i != 0 );
    return true;
}
bool EntityData::GetPoint( const AcDbObjectId& objId, const CString& field, AcGePoint3d& value )
{
    CString str;
    if( !EntityData::GetString( objId, field, str ) ) return false;
    if( !ArxUtilHelper::StringToPoint3d( str, value ) ) return false;
    return true;
}
bool EntityData::GetVector( const AcDbObjectId& objId, const CString& field, AcGeVector3d& value )
{
    CString str;
    if( !EntityData::GetString( objId, field, str ) ) return false;
    AcGePoint3d pt;
    if( !ArxUtilHelper::StringToPoint3d( str, pt ) ) return false;
    value = pt.asVector();
    return true;
}
bool EntityData::GetDateTime( const AcDbObjectId& objId, const CString& field, COleDateTime& value )
{
    CString str;
    if( !EntityData::GetString( objId, field, str ) ) return false;
    return value.ParseDateTime( str );
}
bool EntityData::GetObjectId( const AcDbObjectId& objId, const CString& field, AcDbObjectId& value )
{
    CString str;
    if( !EntityData::GetString( objId, field, str ) ) return false;
    value = ArxUtilHelper::StrtoObjectId( str );
    return !value.isNull();
}
void EntityData::Copy( const AcDbObjectId& sourceObjId, const AcDbObjectId& targetObjId )
{
}
EntityData::EntityData() : ObjectData()
{
}
BaseDao* EntityData::CreateDao( const AcDbObjectId& objId )
{
    if( objId.isNull() ) return 0;
    return new EntityDataDao( objId );
}
BaseDao* EntityData::CreateDao( const CString& type, AcDbObjectId& objId )
{
    return new EntityDataDao( type, objId );
}
