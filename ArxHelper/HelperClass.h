#pragma once
/* 下面所有的辅助类均可导出(export)使用 */
// ARX辅助方法类
#include "ArxDbgXdata.h"
#include "ArxXRecordManager.h"
#include "XDataStream.h"
#include "ArxDataTool.h"
#include "ArxDictTool.h"
#include "ArxClassHelper.h"
#include "ArxEntityHelper.h"
#include "ArxUtilHelper.h"
#include "ArxDwgHelper.h"
#include "ArxLayerHelper.h"
#include "ArxDrawHelper.h"
#include "ArxGroupHelper.h"
#include "ArxSwitchHelper.h"
#include "ArxUcsHelper.h"
#include "ArxDwgHelper.h"
#include "AcStringHelper.h"
#include "ArxSysVarHelper.h"
// 属性数据字段管理辅助类
#include "FieldHelper.h"
#include "FieldInfoHelper.h"
#include "FuncHelper.h"
// 列表管理辅助类
#include "DataListHelper.h"
// 扩展数据和词典操作函数
#include "Data.h"
#include "ObjectData.h"
#include "XData.h"
#include "DictData.h"
#include "ExtDictData.h"
#include "JsonDictData.h"
#include "Entity.h"
#include "EntityData.h"
#include "JsonHelper.h"
//日志和消息打印控制系统类
#include "Console.h"
// CMFCPropertyGridCtrl控件的辅助类
#include "MFCPropertyGridCtrlHelper.h"
#include "PropertyDataUpdater.h"

#include "Tool/HelperClass.h"

// 定义一个帮助宏,简化arx模块的路径操作
#define ARX_DIR PathHelper::GetModuleDir(_hdllInstance)
#define ARX_FILE_PATH(file) PathHelper::BuildPath(PathHelper::GetModuleDir(_hdllInstance),file)
