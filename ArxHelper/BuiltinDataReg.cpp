#include "StdAfx.h"
#include "BuiltinData.h"
#include "BuiltinDataManager.h"
void BuiltinDataReg()
{
    REG_BUILTIN_DATA( AcDbLine );
    REG_BUILTIN_DATA( AcDbCircle );
    REG_BUILTIN_DATA( AcDbArc );
    REG_BUILTIN_DATA( AcDbEllipse );
    REG_BUILTIN_DATA( AcDbMText );
    REG_BUILTIN_DATA( AcDbText );
    REG_BUILTIN_DATA( AcDbLayerTableRecord );
}
