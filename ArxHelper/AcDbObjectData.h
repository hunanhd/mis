#pragma once
#include "Data.h"
/*
 * cad内置图元的数据基类
 * 设计这个类的目的是为了使cad内置图元(直线、圆等)和Entity派生的图元一样可以通过dao修改图元的几何参数
 * 参考: 派生类AcDbLineData的实现
 */
class AcDbObjectData : public BaseData
{
public:
    void setObject( AcDbObject* pObj );
protected:
    // 当dao->get(field, ...)调用前触发该虚函数,cad内置图元可以在这里修改关联的成员变量
    //virtual void onBeforeGetData( const CString& field ) const {}
    // 当dao->set(field, ...)调用后触发该虚函数,cad内置图元利用关联的成员变量修改自身的几何参数
    //virtual void onAfterSetData( const CString& field, const CString& value ) {}
protected:
    AcDbObjectData();
    AcDbObject* m_pObj;
};
