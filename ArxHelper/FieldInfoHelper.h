#pragma once
#include "FieldInfo.h"
class ARXHELPER_API FieldInfoHelper
{
public:
    static bool AddFieldInfo( const CString& type, const CString& field, const FieldInfo& info );
    static bool RemoveFieldInfo( const CString& type, const CString& field );
    static void RemoveMoreFieldsInfo( const CString& type, const AcStringArray& fields );
    static bool GetFieldInfo( const CString& type, const CString& field, FieldInfo& info );
    static bool UpdateFieldInfo( const CString& type, const CString& field, const FieldInfo& info );
};
