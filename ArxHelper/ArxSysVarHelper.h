#pragma once
#include "dlimexp.h"
/* 
 * AutoCAD系统变量辅助类
 * 常用CAD变量:
 *      OSMODE(对象捕捉)
 *      PICKADD(选择图形的个数)
 */
class ARXHELPER_API ArxSysVarHelper
{
public:
	static int Get(LPCTSTR varName, int& val);
	static int Get(LPCTSTR varName, double& val);
	static int Get(LPCTSTR varName, CString& val);
	static int Get(LPCTSTR varName, AcGePoint2d& val);
	static int Get(LPCTSTR varName, AcGePoint3d& val);
	static int Set(LPCTSTR varName, int val);
	static int Set(LPCTSTR varName, double val);
	static int Set(LPCTSTR varName, LPCTSTR val);
	static int Set(LPCTSTR varName, const AcGePoint3d& val);
	static int Set(LPCTSTR varName, const AcGePoint2d& val);
};
// 系统变量切换
template<typename T>
class ArxSysVarSwitch
{
public:
	ArxSysVarSwitch(const CString& _name, const T& new_val) : name(_name), es(Acad::eOk)
	{
		es = ArxSysVarHelper::Get(name, old_val);
		if(es == Acad::eOk)
		{
			ArxSysVarHelper::Set(name, new_val);
		}
	}
	~ArxSysVarSwitch()
	{
		if(es == Acad::eOk)
		{
			ArxSysVarHelper::Set(name, old_val);
		}
	}
private:
	int es;
	CString name;
	T old_val;
};