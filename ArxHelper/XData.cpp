#include "StdAfx.h"
#include "XData.h"
#include "Dao.h"
#include "HelperClass.h"
//////////////////////////////////////////////////////////////////
///////////////////// 成员函数实现 /////////////////////////////
/////////////////////////////////////////////////////////////////
//Dao* XData::createDao()
//{
//    return XData::CreateDao( getDataSource() );
//}
//////////////////////////////////////////////////////////////////
///////////////// 静态成员函数实现 ////////////////////////////
/////////////////////////////////////////////////////////////////
bool XData::Init( const AcDbObjectId& objId, int n )
{
    if( objId.isNull() ) return false;
    if( n < 1 ) return false;
    AcStringArray values;
    for( int i = 0; i < n; i++ )
    {
        values.append( _T( "" ) );
    }
    return XData::Init( objId, values );
}
bool XData::Init( const AcDbObjectId& objId, const AcStringArray& values )
{
    if( objId.isNull() ) return false;
    if( values.isEmpty() ) return false;
    BaseDao* dao = XData::CreateDao( objId );
    if( dao == 0 ) return false;
    dao->clearAllFields(); // 清空
    for( int i = 0; i < values.length(); i++ )
    {
        CString field;
        StringHelper::IntToString( i, field );
        dao->setString( field, values[i].kACharPtr() );
    }
    delete dao;
    return true;
}
bool XData::GetString( const AcDbObjectId& objId, int pos, CString& value )
{
    if( objId.isNull() ) return false;
    BaseDao* dao = XData::CreateDao( objId );
    if( dao == 0 ) return false;
    CString field;
    StringHelper::IntToString( pos, field );
    bool ret = dao->getString( field, value );
    delete dao;
    return ret;
}
bool XData::GetInt( const AcDbObjectId& objId, int pos, int& value )
{
	CString str;
	if( !XData::GetString( objId, pos, str ) ) return false;
	if( !StringHelper::StringToInt( str, value ) ) return false;
	return true;
}
bool XData::GetDouble( const AcDbObjectId& objId, int pos, double& value )
{
	CString str;
	if( !XData::GetString( objId, pos, str ) ) return false;
	if( !StringHelper::StringToDouble( str, value ) ) return false;
	return true;
}
bool XData::GetBool( const AcDbObjectId& objId, int pos, bool& value )
{
	CString str;
	if( !XData::GetString( objId, pos, str ) ) return false;
	int i = 0;
	if( !StringHelper::StringToInt( str, i ) ) return false;
	value = ( i != 0 );
	return true;
}
bool XData::GetPoint( const AcDbObjectId& objId, int pos, AcGePoint3d& value )
{
	CString str;
	if( !XData::GetString( objId, pos, str ) ) return false;
	if( !ArxUtilHelper::StringToPoint3d( str, value ) ) return false;
	return true;
}
bool XData::GetVector( const AcDbObjectId& objId, int pos, AcGeVector3d& value )
{
	CString str;
	if( !XData::GetString( objId, pos, str ) ) return false;
	AcGePoint3d pt;
	if( !ArxUtilHelper::StringToPoint3d( str, pt ) ) return false;
	value = pt.asVector();
	return true;
}
bool XData::GetDateTime( const AcDbObjectId& objId, int pos, COleDateTime& value )
{
	CString str;
	if( !XData::GetString( objId, pos, str ) ) return false;
	return value.ParseDateTime( str );
}
bool XData::GetObjectId( const AcDbObjectId& objId, int pos, AcDbObjectId& value )
{
	CString str;
	if( !XData::GetString( objId, pos, str ) ) return false;
	value = ArxUtilHelper::StrtoObjectId( str );
	return !value.isNull();
}
bool XData::SetString( const AcDbObjectId& objId, int pos, const CString& value )
{
    if( objId.isNull() ) return false;
    BaseDao* dao = XData::CreateDao( objId );
    if( dao == 0 ) return false;
    CString field;
    StringHelper::IntToString( pos, field );
    bool ret = dao->setString( field, value );
    delete dao;
    return ret;
}
void XData::Copy( const AcDbObjectId& sourceObjId, const AcDbObjectId& targetObjId )
{
}
BaseDao* XData::CreateDao( const AcDbObjectId& objId )
{
    return new XDataDao( objId );
}