#include "StdAfx.h"
#include "FieldInfo.h"
#include "JsonHelper.h"
#include "Tool/HelperClass.h"
void FieldInfo::Set( FieldInfo& info, const CString& fieldType, const CString& defaultValue, const CString& tole, const CString& editable, const CString& toolTips )
{
    info.m_flag = _ttoi( editable );
    info.m_tooltips = toolTips;
    if ( _T( "DT_STRING" ) == fieldType )
    {
        info.m_dt = FieldInfo::DT_STRING;
    }
    if ( _T( "DT_INT" ) == fieldType )
    {
        info.m_dt = FieldInfo::DT_INT;
        int pos = defaultValue.Find( _T( "," ) );
        int minInt = _ttoi( defaultValue.Left( pos ) );
        int maxInt = _ttoi( defaultValue.Mid( pos + 1, defaultValue.GetLength() ) );
        //acutPrintf(_T("\n最小值:%d->最大值:%d"),minInt,maxInt);
        info.m_minValue2 = minInt;
        info.m_maxValue2 = maxInt;
    }
    if ( _T( "DT_NUMERIC" ) == fieldType )
    {
        info.m_dt = FieldInfo::DT_NUMERIC;
        int pos = defaultValue.Find( _T( "," ) );
        double minDouble = _tstof( defaultValue.Left( pos ) );
        double maxDouble = _tstof( defaultValue.Mid( pos + 1, defaultValue.GetLength() ) );
        //acutPrintf(_T("\n最小值:%lf->最大值:%lf"),minDouble,maxDouble);
        info.m_minValue = minDouble;
        info.m_maxValue = maxDouble;
        info.m_tolrance = _ttoi( tole );
    }
    if ( _T( "DT_LIST" ) == fieldType )
    {
        info.m_dt = FieldInfo::DT_LIST;
        int pos = defaultValue.Find( _T( "," ) );
        CString ltType = defaultValue.Left( pos );
        CString ltValue = defaultValue.Mid( pos + 1, defaultValue.GetLength() );
        if( _T( "LT_INT" ) == ltType )	info.m_lt = FieldInfo::LT_INT;
        if( _T( "LT_STRING" ) == ltType )	info.m_lt = FieldInfo::LT_STRING;
        info.m_varName = ltValue;
    }
    if ( _T( "DT_BOOL" ) == fieldType )
    {
        info.m_dt = FieldInfo::DT_BOOL;
    }
    if ( _T( "DT_DATE" ) == fieldType )
    {
        info.m_dt = FieldInfo::DT_DATE;
    }
}
void FieldInfo::Get( const FieldInfo& info, CString& fieldType, CString& defaultValue, CString& tole, CString& editable, CString& toolTips )
{
    editable.Format( _T( "%d" ), info.m_flag );
    toolTips = info.m_tooltips;
    CString str;
    switch( info.m_dt )
    {
    case FieldInfo::DT_STRING:
        fieldType = _T( "DT_STRING" );
        tole = _T( "0" );
        defaultValue = _T( "" );
        break;
    case FieldInfo::DT_INT:
        fieldType = _T( "DT_INT" );
        tole = _T( "0" );
        defaultValue.Format( _T( "%d,%d" ), info.m_minValue2, info.m_maxValue2 );
        break;
    case FieldInfo::DT_NUMERIC:
        fieldType = _T( "DT_NUMERIC" );
        tole = _T( "2" );
        str = _T( "%." ) + tole + _T( "lf" );
        str = str + _T( "," ) + str;
        defaultValue.Format( str, info.m_minValue, info.m_maxValue );
        break;
    case FieldInfo::DT_BOOL:
        fieldType = _T( "DT_BOOL" );
        tole = _T( "0" );
        defaultValue = _T( "1" );
        break;
    case FieldInfo::DT_DATE:
        fieldType = _T( "DT_DATE" );
        tole = _T( "0" );
        defaultValue = _T( "" );
        break;
    case FieldInfo::DT_LIST:
        fieldType = _T( "DT_LIST" );
        tole = _T( "0" );
        CString ltType, ltValue;
        if( info.m_lt == FieldInfo::LT_INT )	ltType = _T( "LT_INT" );
        if( info.m_lt == FieldInfo::LT_STRING )	ltType = _T( "LT_STRING" );
        ltValue = info.m_varName;
        defaultValue.Format( _T( "%s,%s" ), ltType, ltValue );
        break;
    }
}
FieldInfo::FieldInfo()
{
    initDefault();
}
void FieldInfo::initDefault()
{
    m_dt = FieldInfo::DT_STRING;
    m_flag = FT_EDIT | FT_SHOW;
    m_tooltips = _T( "" );
    m_tolrance = 2;
    initListType();
    initIntMinMaxValue();
    initNumericMinMaxValue();
}
void FieldInfo::copyFrom( const FieldInfo& info )
{
    m_dt = info.m_dt;
    m_minValue2 = info.m_minValue2;
    m_maxValue2 = info.m_maxValue2;
    m_minValue = info.m_minValue;
    m_maxValue = info.m_maxValue;
    m_flag = info.m_flag;
    m_tooltips = info.m_tooltips;
    m_lt = info.m_lt;
    m_varName = info.m_varName;
    m_tolrance = info.m_tolrance;
}
void FieldInfo::initNumericMinMaxValue()
{
    m_minValue = 0;
    m_maxValue = 99.9;
}
void FieldInfo::initIntMinMaxValue()
{
    m_minValue2 = 0;
    m_maxValue2 = 100;
}
void FieldInfo::initListType()
{
    m_lt = FieldInfo::LT_STRING;
    m_varName = _T( "" );
}
void FieldInfo::revise()
{
    switch( m_dt )
    {
    case FieldInfo::DT_STRING:
    case FieldInfo::DT_BOOL:
    case FieldInfo::DT_DATE:
        initListType();
        initIntMinMaxValue();
        initNumericMinMaxValue();
        break;
    case FieldInfo::DT_INT:
        initListType();
        initNumericMinMaxValue();
        break;
    case FieldInfo::DT_NUMERIC:
        initListType();
        initIntMinMaxValue();
        break;
    case FieldInfo::DT_LIST:
        initIntMinMaxValue();
        initNumericMinMaxValue();
        break;
    }
}
bool FieldInfo::isDefault() const
{
    return ( ( m_dt == FieldInfo::DT_STRING ) && ( m_flag == ( FT_EDIT | FT_SHOW ) ) && ( m_tooltips.GetLength() == 0 ) );
}

CString FieldInfo::ToJson(const FieldInfo& info)
{
	AcStringArray fields, values;
	fields.append(_T("data_type"));
	fields.append(_T("int_min_value"));
	fields.append(_T("int_max_value"));
	fields.append(_T("double_min_value"));
	fields.append(_T("double_max_value"));
	fields.append(_T("list_type"));
	fields.append(_T("var_name"));
	fields.append(_T("flag"));
	fields.append(_T("tooltips"));
	fields.append(_T("tolrance"));
	
	CString str;
	StringHelper::IntToString((int)info.m_dt, str);
	values.append(str);

	str = _T("");
	StringHelper::IntToString(info.m_minValue2, str);
	values.append(str);
	str = _T("");
	StringHelper::IntToString(info.m_maxValue2, str);
	values.append(str);

	str = _T("");
	StringHelper::DoubleToString(info.m_minValue, str);
	values.append(str);
	str = _T("");
	StringHelper::DoubleToString(info.m_maxValue, str);
	values.append(str);

	str = _T("");
	StringHelper::IntToString((int)info.m_lt, str);
	values.append(str);

	values.append(info.m_varName);

	str = _T("");
	StringHelper::IntToString((int)info.m_flag, str);
	values.append(str);

	values.append(info.m_tooltips);

	str = _T("");
	StringHelper::IntToString(info.m_tolrance, str);
	values.append(str);

	str = _T("{}");
	JsonHelper::Format(fields, values, str);
	return str;
}

FieldInfo FieldInfo::FromJson(const CString& json_str)
{
	AcStringArray fields, values;
	if(!JsonHelper::Parse(json_str, fields, values) || fields.length() != values.length())
	{
		return FieldInfo();
	}
	else
	{
		FieldInfo info;
		int n = fields.length();
		for(int i=0;i<n;i++)
		{
			CString field = fields[i].kACharPtr();
			CString value = values[i].kACharPtr();
			field.MakeLower();

			if(field == _T("data_type"))
			{
				int i = 0;
				StringHelper::StringToInt(value, i);
				info.m_dt = (FieldInfo::DATA_TYPE)i;
			}
			else if(field == _T("int_min_value"))
			{
				int i = 0;
				StringHelper::StringToInt(value, i);
				info.m_minValue2 = i;
			}
			else if(field == _T("int_max_value"))
			{
				int i = 0;
				StringHelper::StringToInt(value, i);
				info.m_maxValue2 = i;
			}
			else if(field == _T("double_min_value"))
			{
				double i = 0;
				StringHelper::StringToDouble(value, i);
				info.m_minValue = i;
			}
			else if(field == _T("double_max_value"))
			{
				double i = 0;
				StringHelper::StringToDouble(value, i);
				info.m_maxValue = i;
			}
			else if(field == _T("list_type"))
			{
				int i = 0;
				StringHelper::StringToInt(value, i);
				info.m_lt = (FieldInfo::LIST_TYPE)i;
			}
			else if(field == _T("var_name"))
			{
				info.m_varName = value;
			}
			else if(field == _T("flag"))
			{
				int i = 0;
				StringHelper::StringToInt(value, i);
				info.m_flag = (unsigned int)i;
			}
			else if(field == _T("tooltips"))
			{
				info.m_varName = value;
			}
			else if(field == _T("tolrance"))
			{
				double i = 0;
				StringHelper::StringToDouble(value, i);
				info.m_tolrance = i;
			}
		}
		// 校正
		info.revise();
		return info;
	}
}
