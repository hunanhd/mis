#pragma once
#include <map>
#include "Tool/Singleton.h"
class AcDbObjectData;
class BuiltinDataManager : public Singleton<BuiltinDataManager>
{
public:
    static void Init();
    static void UnInit();
public:
    BuiltinDataManager();
    ~BuiltinDataManager();
    void regData( const CString& clsname, AcDbObjectData* pData );
    AcDbObjectData* getData( const CString& clsname );
private:
    typedef std::map<CString, AcDbObjectData*> BuiltinDataMap;
    BuiltinDataMap m_dataMap;
};
#define REG_BUILTIN_DATA(clsname) BuiltinDataManager::getSingletonPtr()->regData(_T(#clsname), new clsname##Data())
