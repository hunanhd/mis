#pragma once
extern resbuf* GoToListEnd( resbuf* pHead, const CString& field );
extern resbuf* SetData( resbuf* pStart, const CString& value );
extern resbuf* GetData( resbuf* pStart, CString& value );
extern void GetAllFields( resbuf* pHead, AcStringArray& fields );
extern void GetAllDatas( resbuf* pHead, AcStringArray& fields, AcStringArray& values );
extern void GetAllFieldTypes( resbuf* pHead, AcStringArray& fields, AcDbIntArray& types );
extern void MapGenericDataByPos( resbuf*& pHead, int pos, short rtype, void* pValue );
extern void MapGenericDataByField( resbuf*& pHead, const CString& field, short rtype, void* pValue );
