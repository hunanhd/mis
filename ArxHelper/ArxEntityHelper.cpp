#include "StdAfx.h"
#include "ArxEntityHelper.h"
/* 全局函数(实现在ZoomHelper.cpp中) */
extern void ExecuteZoomCommand1( const AcGePoint3d& minPt, const AcGePoint3d& maxPt );
extern void ExecuteZoomCommand2( const AcGePoint3d& minPt, const AcGePoint3d& maxPt );
//extern void ZommWindowUseCom(const AcGePoint3d& minPt, const AcGePoint3d& maxPt);
extern void ZoomWindowUseView( const AcGePoint3d& minPt, const AcGePoint3d& maxPt );
// 无效
extern void ZoomWindowUseGSView( const AcGePoint3d& minPt, const AcGePoint3d& maxPt );

void ArxEntityHelper::EraseObject( const AcDbObjectId& objId, Adesk::Boolean erasing )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    AcDbObject* pObj;
    if( Acad::eOk == pTrans->getObject( pObj, objId, AcDb::kForWrite, !erasing ) )
    {
        pObj->erase( erasing ); // (反)删除图元
    }
    actrTransactionManager->endTransaction();
}
void ArxEntityHelper::EraseObject2( const AcDbObjectId& objId, Adesk::Boolean erasing )
{
    AcDbObject* pObj;
    if( Acad::eOk == acdbOpenAcDbObject( pObj, objId, AcDb::kForWrite, !erasing ) )
    {
        pObj->erase( erasing );
        pObj->close();
        //acutPrintf(_T("\n使用Open/close机制删除对象成功"));
    }
}
void ArxEntityHelper::EraseObjects( const AcDbObjectIdArray& objIds, Adesk::Boolean erasing )
{
    if( objIds.isEmpty() ) return;
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    int len = objIds.length();
    for( int i = 0; i < len; i++ )
    {
        AcDbObject* pObj;
        if( Acad::eOk != pTrans->getObject( pObj, objIds[i], AcDb::kForWrite, !erasing ) ) continue;
        pObj->erase( erasing ); // (反)删除图元
    }
    actrTransactionManager->endTransaction();
}
void ArxEntityHelper::EraseObjects2( const AcDbObjectIdArray& objIds, Adesk::Boolean erasing )
{
    if( objIds.isEmpty() ) return;
    int len = objIds.length();
    for( int i = 0; i < len; i++ )
    {
        EraseObject2( objIds[i], erasing );
    }
}
void ArxEntityHelper::TransformEntities2( const AcDbObjectIdArray& objIds, const AcGeMatrix3d& xform )
{
    if( objIds.isEmpty() ) return;
    int len = objIds.length();
    for( int i = 0; i < len; i++ )
    {
        AcDbEntity* pEnt;
        if( Acad::eOk == acdbOpenAcDbEntity( pEnt, objIds[i], AcDb::kForWrite ) )
        {
            pEnt->transformBy( xform );
            pEnt->close();
        }
    }
}
void ArxEntityHelper::TransformEntities( const AcDbObjectIdArray& objIds, const AcGeMatrix3d& xform )
{
    if( objIds.isEmpty() ) return;
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    int len = objIds.length();
    for( int i = 0; i < len; i++ )
    {
        AcDbObject* pObj;
        if( Acad::eOk != pTrans->getObject( pObj, objIds[i], AcDb::kForWrite ) ) continue;
        AcDbEntity* pEnt = AcDbEntity::cast( pObj );
        if( pEnt == 0 ) continue;
        pEnt->transformBy( xform );
    }
    actrTransactionManager->endTransaction();
}
void ArxEntityHelper::DrawEntities( const AcDbObjectIdArray& objIds, AcGiWorldDraw* mode )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    int len = objIds.length();
    for( int i = 0; i < len; i++ )
    {
        AcDbObject* pObj;
        if( Acad::eOk != pTrans->getObject( pObj, objIds[i], AcDb::kForRead ) ) continue;
        AcDbEntity* pEnt = AcDbEntity::cast( pObj );
        if( pEnt == 0 ) continue;
        //acutPrintf(_T("\n绘制第%d个图元"), i);
        pEnt->worldDraw( mode );
    }
    actrTransactionManager->endTransaction();
}
void ArxEntityHelper::ShowEntities( const AcDbObjectIdArray& objIds, bool isVisible )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    int len = objIds.length();
    for( int i = 0; i < len; i++ )
    {
        AcDbObject* pObj = 0;
        if( Acad::eOk != pTrans->getObject( pObj, objIds[i], AcDb::kForWrite ) ) continue;;
        AcDbEntity* pEnt = AcDbEntity::cast( pObj );
        if( pEnt == 0 ) continue;
        pEnt->setVisibility( isVisible ? AcDb::kVisible : AcDb::kInvisible ); // 显示或隐藏图元(修改)
    }
    actrTransactionManager->endTransaction();
}
void ArxEntityHelper::ShowEntity( const AcDbObjectId& objId, bool isVisible )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    AcDbObject* pObj = 0;
    if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForWrite ) )
    {
        actrTransactionManager->abortTransaction();
        return;
    }
    AcDbEntity* pEnt = AcDbEntity::cast( pObj );
    if( pEnt == 0 )
    {
        actrTransactionManager->abortTransaction();
        return;
    }
    pEnt->setVisibility( isVisible ? AcDb::kVisible : AcDb::kInvisible ); // 显示或隐藏图元(修改)
    actrTransactionManager->endTransaction();
}
// 使用事务
static void UpdateEntity1( const AcDbObjectId& objId )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    AcDbObject* pObj = 0;
    if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForWrite ) ) // 打开图元失败
    {
        actrTransactionManager->abortTransaction();
        return;
    }
    AcDbEntity* pEnt = AcDbEntity::cast( pObj );
    if( pEnt == 0 )
    {
        actrTransactionManager->abortTransaction();
        return;
    }
    pEnt->recordGraphicsModified( true ); // 标签图元状态已修改，需要更新图形
    actrTransactionManager->endTransaction();
}
// 使用常规的Open/Close机制
static void UpdateEntity2( const AcDbObjectId& objId )
{
    AcDbEntity* pEnt = 0;
    if( Acad::eOk != acdbOpenObject( pEnt, objId, AcDb::kForWrite ) ) return;
    pEnt->recordGraphicsModified( true ); // 标签图元状态已修改，需要更新图形
    pEnt->close();
}
void ArxEntityHelper::Update( const AcDbObjectId& objId )
{
    if( objId.isNull() ) return;
    /*
     * 使用常规的Open/Close机制更新实体
     * 注：使用事务机制更新实体有时候好使, 有时候不好使
     *      原因暂时不明
     */
    UpdateEntity2( objId );
}
static void ForceUpdate()
{
    actrTransactionManager->queueForGraphicsFlush();
    actrTransactionManager->flushGraphics();
    acedUpdateDisplay();
}
static void ZoomEntity_Helper( const AcGePoint3d& minPt, const AcGePoint3d& maxPt )
{
    //acutPrintf(_T("\n最小点:(%.3f,%.3f,%.3f)\t最大点:(%.3f,%.3f,%.3f)\n"), minPt.x, minPt.y, minPt.z, maxPt.x, maxPt.y, maxPt.z);
    AcGeVector3d v = maxPt - minPt;
    // 沿着对角线由内向外放大(最小点变小，最大点变大)
    double c = 0.618; // 黄金比例值
    // 1) 使用sendStringToExecute方法
    // 缺点:在命令行显示一些提示字符串，有点烦人(例如: "命令：zoom w 指定第一个位置 ... ")
    //ExecuteZoomCommand1(minPt-c*v, maxPt+c*v);
    // 2) 使用acedCommand方法
    // 缺点: 在modelss dialog中无法使用(具体的说是在application context下无法执行)
    // 参见：arxdoc.chm->Advanced Topics->The Multiple Document Interface
    //        ->Application Execution Context
    //        ->Code Differences under the Application Execution Context
    //ExecuteZoomCommand2(minPt-c*v, maxPt+c*v);
    // 3) 使用com
    // 缺点: 显示一些空的命令提示(例如"命令: ")
    //ZommWindowUseCom(minPt-c*v, maxPt+c*v);
    // 4) 使用view
    // 缺点：如果在缩放的时候，对图元进行了修改，例如修改颜色，并不会马上更新
    //        只有当前焦点在cad的绘图窗口时，才会更新
    // 参见：<<ObjectARX开发实例教程-20090826>>中的"4.4 视图"小节
    ZoomWindowUseView( minPt - c * v, maxPt + c * v );
    acedGetAcadFrame()->SetFocus(); // 切换焦点(解决图形修改的问题)
    // 5) 使用AcGsView(失败)
    // AcGsView只能用于3d模式
    //ZoomWindowUseGSView(minPt-c*v, maxPt+c*v);
}
static bool IsValidExtent( const AcDbExtents& ext )
{
    AcGeVector3d v = ext.maxPoint() - ext.minPoint();
    //acutPrintf(_T("\nextents:x=%.3f,%y=%.3f,z=%.3f\n"), v.x, v.y, v.z);
    return ( v.x >= 0 && v.y >= 0 && v.z >= 0 ); // x,y,z的差值必须>=0
}
void ArxEntityHelper::ZoomToEntityForModeless( const AcDbObjectId& objId )
{
    // 在非模态对话框下无法使用
    //ads_name en;
    //if(Acad::eOk != acdbGetAdsName(en, objId)) return;
    //acedCommand(RTSTR, _T("ZOOM"), RTSTR, _T("O"), RTENAME, en, RTSTR, _T(""), 0);
    // 临时使用sendStringToExecute解决缩放定位问题
    CString cmd;
    cmd.Format( _T( "ZOOM O \003" ) ); // 按空格结束选择对象，然后esc(防止多余的空格重复执行命令)
    acDocManager->sendStringToExecute( curDoc(), cmd, true, false, false );
}
void ArxEntityHelper::ZoomToEntity( const AcDbObjectId& objId )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    AcDbObject* pObj;
    if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForRead ) )
    {
        actrTransactionManager->abortTransaction();
        return;
    }
    AcDbEntity* pEnt = AcDbEntity::cast( pObj );
    if( pEnt == 0 )
    {
        actrTransactionManager->abortTransaction();
        return;
    }
    AcDbExtents extents;
    bool ret = ( Acad::eOk == pEnt->getGeomExtents( extents ) );
    actrTransactionManager->endTransaction();
    if( ret && IsValidExtent( extents ) )
    {
        ZoomEntity_Helper( extents.minPoint(), extents.maxPoint() );
    }
}
void ArxEntityHelper::ZoomToEntities( const AcDbObjectIdArray& objIds )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    AcDbExtents exts;
    int len = objIds.length();
    for( int i = 0; i < len; i++ )
    {
        AcDbObject* pObj;
        if( Acad::eOk != pTrans->getObject( pObj, objIds[i], AcDb::kForRead ) ) continue;
        AcDbEntity* pEnt = AcDbEntity::cast( pObj );
        if( pEnt == 0 ) continue;
        AcDbExtents extents;
        if( Acad::eOk != pEnt->getGeomExtents( extents ) ) continue;
        exts.addPoint( extents.minPoint() );
        exts.addPoint( extents.maxPoint() );
    }
    actrTransactionManager->endTransaction();
    AcGePoint3d minPt = exts.minPoint();
    AcGePoint3d maxPt = exts.maxPoint();
    if( minPt.x <= maxPt.x && minPt.y <= maxPt.y && minPt.z <= maxPt.z )
    {
        ZoomEntity_Helper( minPt, maxPt );
    }
}
bool ArxEntityHelper::GetEntityColor( const AcDbObjectId& objId, Adesk::UInt16& colorIndex )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return false;
    AcDbObject* pObj;
    if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForRead ) )
    {
        actrTransactionManager->abortTransaction();
        return false;
    }
    AcDbEntity* pEnt = AcDbEntity::cast( pObj );
    if( pEnt == 0 )
    {
        actrTransactionManager->abortTransaction();
        return false;
    }
    colorIndex = pEnt->colorIndex();
    actrTransactionManager->endTransaction();
    return true;
}
bool ArxEntityHelper::GetEntitiesColor( const AcDbObjectIdArray& objIds, AcArray<Adesk::UInt16>& colors )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return false;
    bool ret = true;  // 默认返回true
    int len = objIds.length();
    for( int i = 0; i < len; i++ )
    {
        AcDbObject* pObj;
        if( Acad::eOk != pTrans->getObject( pObj, objIds[i], AcDb::kForRead ) )
        {
            actrTransactionManager->abortTransaction();
            ret = false;
            colors.removeAll(); // 清空
            break;
        }
        AcDbEntity* pEnt = AcDbEntity::cast( pObj );
        if( pEnt == 0 )
        {
            actrTransactionManager->abortTransaction();
            ret = false;
            colors.removeAll(); // 清空
            break;
        }
        Adesk::UInt16 ci = pEnt->colorIndex();
        colors.append( ci ); // 记录原有的颜色
    }
    actrTransactionManager->endTransaction();
    return ret;
}
bool ArxEntityHelper::SetEntityColor( const AcDbObjectId& objId, Adesk::UInt16 colorIndex )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return false;
    AcDbObject* pObj;
    if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForWrite ) )
    {
        actrTransactionManager->abortTransaction();
        return false;
    }
    AcDbEntity* pGE = AcDbEntity::cast( pObj );
    if( pGE == 0 )
    {
        actrTransactionManager->abortTransaction();
        return false;
    }
    // 设置颜色
    pGE->setColorIndex( colorIndex );
    actrTransactionManager->endTransaction();
    return true;
}
bool ArxEntityHelper::SetEntitiesColor( AcDbObjectIdArray& objIds, Adesk::UInt16 colorIndex )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return false;
    bool ret = true; // 默认返回true
    int len = objIds.length();
    for( int i = 0; i < len; i++ )
    {
        AcDbObject* pObj;
        if( Acad::eOk != pTrans->getObject( pObj, objIds[i], AcDb::kForWrite ) )
        {
            actrTransactionManager->abortTransaction();
            ret = false;
            break;
        }
        AcDbEntity* pGE = AcDbEntity::cast( pObj );
        if( pGE == 0 )
        {
            actrTransactionManager->abortTransaction();
            ret = false;
            break;
        }
        // 设置颜色
        pGE->setColorIndex( colorIndex );
    }
    actrTransactionManager->endTransaction();
    return ret;
}
bool ArxEntityHelper::SetEntitiesColor2( const AcDbObjectIdArray& objIds, const AcArray<Adesk::UInt16>& colors )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return false;
    bool ret = true; // 默认返回true
    int len = objIds.length();
    for( int i = 0; i < len; i++ )
    {
        AcDbObject* pObj;
        if( Acad::eOk != pTrans->getObject( pObj, objIds[i], AcDb::kForWrite ) )
        {
            actrTransactionManager->abortTransaction();
            ret = false;
            break;
        }
        AcDbEntity* pEnt = AcDbEntity::cast( pObj );
        if( pEnt == 0 )
        {
            actrTransactionManager->abortTransaction();
            ret = false;
            break;
        }
        pEnt->setColorIndex( colors[i] ); // 恢复原先的颜色
    }
    actrTransactionManager->endTransaction();
    return ret;
}

/*
 * 帮助文档中acedSSSetFirst以及acedSSGetFirst
 * 要求使用它的命令应该开启ACRX_CMD_USEPICKSET和ACRX_CMD_REDRAW选项
 * 但测试结果显示，貌似不使用也可以??????
 */
bool ArxEntityHelper::HighlightEntity( const AcDbObjectId& objId )
{
    //acedSSSetFirst(NULL, NULL);
    if( objId.isNull() ) return false;
    ads_name ename;
    if( Acad::eOk != acdbGetAdsName( ename, objId ) ) return false;;
    ads_name ss;
    if( RTNORM != acedSSAdd( ename, NULL, ss ) ) return false; // 创建选择集
    acedSSSetFirst( ss, NULL ); // 高亮选中
    acedSSFree( ss );           // 释放选择集
    return true;
}
bool ArxEntityHelper::HighlightEntities( const AcDbObjectIdArray& objIds )
{
    //acedSSSetFirst(NULL, NULL);
    if( objIds.isEmpty() ) return false;
    ads_name ss;
    //创建一个空的选择集
    if( RTNORM != acedSSAdd( NULL, NULL, ss ) ) return false;
    bool ret = true;
    for( int i = 0; i < objIds.length(); i++ )
    {
        ads_name ename;
        if( Acad::eOk != acdbGetAdsName( ename, objIds[i] ) )
        {
            ret = false;;
            break;
        }
        if( RTNORM != acedSSAdd( ename, ss, ss ) )  // 添加到选择集
        {
            ret = false;;
            break;
        }
    }
    if( ret )
    {
        acedSSSetFirst( ss, NULL ); // 高亮选中
    }
    acedSSFree( ss );           // 释放选择集
    return ret;
}

//AcDbObjectId createTextStyle(CString fontName,CString bigFontName,CString textStyleName)
//{
//	AcGiTextStyle *TextStyle=new AcGiTextStyle
//		(fontName,
//		bigFontName,
//		0,
//		0.67,
//		0,
//		0,
//		Adesk::kFalse,
//		Adesk::kFalse,
//		Adesk::kFalse,
//		Adesk::kFalse,
//		Adesk::kFalse,
//		textStyleName); //字体名
//	AcDbObjectId textStyleId;
//	toAcDbTextStyle(*TextStyle,textStyleId);
//	return textStyleId;
//}
//
//AcDbObjectId createMutiText(AcGePoint3d BasePoint,AcDb::TextHorzMode hMode,CString Text,double texthight,double widthfactor,double angle,int color,CString smallFontName,CString bigFontName)
//{
//	AcDbMText *pMText=new AcDbMText();
//	AcDbObjectId TextStyleId;
//	TextStyleId=createTextStyle(smallFontName,bigFontName,"xianlu");
//	pMText->setTextStyle(TextStyleId);
//	pMText->setContents(Text.GetBuffer(Text.GetLength()));
//	pMText->setTextHeight(texthight);
//	pMText->setRotation(angle);
//	pMText->setLineSpacingFactor(0.8);
//	pMText->setColorIndex(color);
//	return TextStyleId;
//}
/*
 参考资料
 https://msdn.microsoft.com/zh-cn/library/ms858335.aspx
 https://msdn.microsoft.com/en-us/library/cc194829.aspx
 http://bbs.mjtd.com/thread-1560-1-1.html
 http://blog.csdn.net/sw283632534/article/details/5401999
 http://blog.csdn.net/jiangdong2007/article/details/39637369
 */
AcDbObjectId ArxEntityHelper::CreateTextStyle( const CString& style, const CString& winFont, Adesk::Boolean bold, Adesk::Boolean italic, int charset, int pitchAndFamily )
{
    /*
    //pTextStyleTblRcd->setFileName(_T("simfang.ttf"));
    //pTextStyleTblRcd->setBigFontFileName("hzdx");
    pTextStyleTblRcd->setXScale(0.8);
    //pTextStyleTblRcd->setFont("_T(楷体_GB2312"),Adesk::kFalse,Adesk::kFalse,GB2312_CHARSET,49);
    //pTextStyleTblRcd->setFont(_T("宋体"),Adesk::kFalse,Adesk::kFalse,GB2312_CHARSET,2);
    pTextStyleTblRcd->setFont(winFont,Adesk::kFalse,Adesk::kFalse,GB2312_CHARSET,16);
    */
    AcDbTextStyleTable* pTextStyleTbl;
    acdbHostApplicationServices()->workingDatabase()->getSymbolTable( pTextStyleTbl, AcDb::kForWrite );
    AcDbObjectId fontId;
    if( pTextStyleTbl->getAt( style, fontId ) == Acad::eKeyNotFound )
    {
        AcDbTextStyleTableRecord* pTextStyleTblRcd = new AcDbTextStyleTableRecord;
        pTextStyleTblRcd->setName( style );
        pTextStyleTblRcd->setFont( winFont, bold, italic, charset, pitchAndFamily );
        pTextStyleTbl->add( fontId, pTextStyleTblRcd );
        pTextStyleTblRcd->close();
    }
    pTextStyleTbl->close();
    return fontId;
    //acdbHostApplicationServices()->workingDatabase()->setTextstyle(fontId);
}
//http://blog.csdn.net/foreverling/article/details/28267307
/*
static void qxzyOperateLayer_AddStyle(void)
{
AcDbTextStyleTable *pTextStyleTbl;
acdbHostApplicationServices()->workingDatabase()
->getTextStyleTable(pTextStyleTbl, AcDb::kForWrite);
AcDbTextStyleTableRecord *pTextStyleTblRcd;
pTextStyleTblRcd = new AcDbTextStyleTableRecord();
pTextStyleTblRcd->setName(_T("仿宋体"));
pTextStyleTblRcd->setFileName(_T("simfang.ttf"));
pTextStyleTblRcd->setXScale(0.7);
pTextStyleTbl->add(pTextStyleTblRcd);
pTextStyleTblRcd->close();
pTextStyleTbl->close();
}
字体的名称不一定与字体文件的名称相同。打开控制面板，进入“字体”文件夹，右键单击“仿宋体”图标，从弹出的快捷菜单中选择【属性】菜单项，系统会弹出对话框显示字体文件的名称。
*/
AcDbObjectId ArxEntityHelper::CreateTextstyle( const CString& textStyleName, bool modifyExistStyle )
{
    AcDbTextStyleTable* pTextStyleTbl;
    if( Acad::eOk != acdbHostApplicationServices()->workingDatabase()->getTextStyleTable( pTextStyleTbl, AcDb::kForRead ) )
        return AcDbObjectId::kNull;
    AcDbTextStyleTableRecord* pTextStyleTblRcd;
    AcDbObjectId textRecordId;
    if ( !pTextStyleTbl->has( textStyleName ) )
    {
        pTextStyleTblRcd = new AcDbTextStyleTableRecord();
        pTextStyleTbl->add( textRecordId, pTextStyleTblRcd );
    }
    else
    {
        if( !modifyExistStyle )
        {
            pTextStyleTbl->getAt( textStyleName, textRecordId );
            pTextStyleTbl->close();
            return textRecordId;
        }
        else
        {
            pTextStyleTbl->getAt( textStyleName, pTextStyleTblRcd, AcDb::kForWrite );
        }
    }
    pTextStyleTbl->close();
    //设置文字样式的特性
    pTextStyleTblRcd->setName( textStyleName );
    pTextStyleTblRcd->setFileName( _T( "romans" ) );
    pTextStyleTblRcd->setBigFontFileName( _T( "hzdx" ) );
    pTextStyleTblRcd->setXScale( 0.8 );
    pTextStyleTblRcd->setFont( _T( "楷体_GB2312" ), 0, 0, 134, 49 );
    pTextStyleTblRcd->close();
    //acdbHostApplicationServices()->workingDatabase()->setTextstyle(fontId);
    return pTextStyleTblRcd->objectId();
}
AcDbObjectId ArxEntityHelper::GetTextStyleId( const CString& name )
{
    AcDbTextStyleTable* pTable;
    if( Acad::eOk != acdbHostApplicationServices()->workingDatabase()->getTextStyleTable( pTable, AcDb::kForRead ) )
        return AcDbObjectId::kNull;
    AcDbObjectId objId;
    pTable->getAt( name, objId );
    pTable->close();
    return objId;
}
//http://blog.csdn.net/foreverling/article/details/28268811
/*
static void qxzyAddDimStyle_AddDimStyle(void)
{
ACHAR styleName[50];
if(acedGetString(Adesk::kFalse, _T("请输入样式名称:"), styleName) != RTNORM)
{
return;
}
AcDbDimStyleTable *pDimStyleTbl;
acdbHostApplicationServices()->workingDatabase()
->getDimStyleTable(pDimStyleTbl, AcDb::kForWrite);
if(pDimStyleTbl->has(styleName))
{
pDimStyleTbl->close();
return;
}
AcDbDimStyleTableRecord *pDimStyleTblRcd;
pDimStyleTblRcd = new AcDbDimStyleTableRecord();
pDimStyleTblRcd->setName(styleName);//样式名称
pDimStyleTblRcd->setDimasz(3);//箭头长度
pDimStyleTblRcd->setDimexe(3);//尺寸界线与标注
pDimStyleTblRcd->setDimtad(1);//文字位于标注线的上方
pDimStyleTblRcd->setDimtxt(3);//标注文字的高度
pDimStyleTbl->add(pDimStyleTblRcd);
pDimStyleTblRcd->close();
pDimStyleTbl->close();
}
*/
AcDbObjectId ArxEntityHelper::CreateDimStyle( const CString& dimStyleName, bool modifyExistStyle, double bili )
{
    AcDbDimStyleTable* pDimStyleTbl;
    if( Acad::eOk != acdbHostApplicationServices()->workingDatabase()->getSymbolTable( pDimStyleTbl, AcDb::kForWrite ) )
        return AcDbObjectId::kNull;
    AcDbDimStyleTableRecord* pDimStyleTblRcd = 0;
    AcDbObjectId dimRecordId;
    if ( !pDimStyleTbl->has( dimStyleName ) )
    {
        pDimStyleTblRcd = new AcDbDimStyleTableRecord();
        pDimStyleTbl->add( dimRecordId, pDimStyleTblRcd );
    }
    else
    {
        if( !modifyExistStyle )
        {
            pDimStyleTbl->getAt( dimStyleName, dimRecordId );
            pDimStyleTbl->close();
            return dimRecordId;
        }
        else
        {
            pDimStyleTbl->getAt( dimStyleName, pDimStyleTblRcd, AcDb::kForWrite );
        }
    }
    pDimStyleTbl->close();
    //设置标注样式的特性
    pDimStyleTblRcd->setName( dimStyleName ); // 样式名称
    pDimStyleTblRcd->setDimasz( 5 * bili ); // 箭头长度
    //pDimStyleTblRcd->setDimblk("_Oblique");//设置箭头的形状为建筑标记
    pDimStyleTblRcd->setDimexe( 10 * bili ); // 指定尺寸界线超出尺寸线的距离
    pDimStyleTblRcd->setDimlfac( 1 ); //比例因子
    pDimStyleTblRcd->setDimdec( 0 ); //设置标注主单位显示的小数位位数，0为byblock、256为bylayer
    AcCmColor clr;
    clr.setColorIndex( 256 );
    pDimStyleTblRcd->setDimclrd( clr ); //为尺寸线、箭头和标注引线指定颜色，0为byblock、256为bylayer
    pDimStyleTblRcd->setDimclre( clr ); //为尺寸界线指定颜色。此颜色可以是任意有效的颜色编号
    pDimStyleTblRcd->setDimclrt( clr ); //为标注文字指定颜色，0为byblock、256为bylayer
    pDimStyleTblRcd->setDimexo( 1 * bili ); //指定尺寸界线偏移原点的距离
    pDimStyleTblRcd->setDimgap( 2 * bili ); //文字从尺寸线偏移 '当尺寸线分成段以在两段之间放置标注文字时，设置标注文字周围的距离
    pDimStyleTblRcd->setDimjust( 0 ); //控制标注文字的水平位置
    pDimStyleTblRcd->setDimtix( 1 ); //设置标注文字始终绘制在尺寸界线之间
    //'.SetVariable "DimJust", 0       '控制标注文字的水平位置
    // '0  将文字置于尺寸线之上，并在尺寸界线之间置中对正
    // '1  紧邻第一条尺寸界线放置标注文字
    // '2  紧邻第二条尺寸界线放置标注文字
    // '3  将标注文字放在第一条尺寸界线以上，并与之对齐
    //'4  将标注文字放在第二条尺寸界线以上，并与之对齐
    pDimStyleTblRcd->setDimtmove( 0 ); //设置标注文字的移动规则
    //'0  尺寸线和标注文字一起移动
    //'1  在移动标注文字时添加一条引线
    // '2  允许标注文字自由移动而不用添加引线
    //AcDbObjectId textStyleId = ArxEntityHelper::GetTextStyleId(textStyleName);
    //if(!textStyleId.isNull())
    //{
    //	pDimStyleTblRcd->setDimtxsty(textStyleId);//指定标注的文字样式
    //}
    pDimStyleTblRcd->setDimtxt( 5 * bili ); //指定标注文字的高度，除非当前文字样式具有固定的高度
    pDimStyleTblRcd->setDimtad( 1 * bili ); // 文字位于标注线的上方
    pDimStyleTblRcd->close();
    return pDimStyleTblRcd->objectId();
}
AcDbObjectId ArxEntityHelper::GetDimStyleId( const CString& name )
{
    AcDbDimStyleTable* pTable;
    if( Acad::eOk != acdbHostApplicationServices()->workingDatabase()->getDimStyleTable( pTable, AcDb::kForRead ) )
        return AcDbObjectId::kNull;
    AcDbObjectId objId;
    pTable->getAt( name, objId );
    pTable->close();
    return objId;
}
void ArxEntityHelper::SetLineType( const AcDbObjectId& objId, const CString& ltName )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    //AcDb::OpenMode om = ( save ? ( AcDb::kForWrite ) : ( AcDb::kForRead ) );
    AcDbObject* pObj;
    if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForWrite ) )
    {
        actrTransactionManager->abortTransaction();
        return;
    }
    AcDbEntity* pEnt = AcDbEntity::cast( pObj );
    if( pEnt == 0 )
    {
        actrTransactionManager->abortTransaction();
        return;
    }
    pEnt->setLinetype( ltName );
    actrTransactionManager->endTransaction();
}
void ArxEntityHelper::SetLineType( const AcDbObjectIdArray& objIds, const CString& ltName )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    for( int i = 0; i < objIds.length(); i++ )
    {
        //AcDb::OpenMode om = ( save ? ( AcDb::kForWrite ) : ( AcDb::kForRead ) );
        AcDbObject* pObj;
        if( Acad::eOk != pTrans->getObject( pObj, objIds[i], AcDb::kForWrite ) ) continue;
        AcDbEntity* pEnt = AcDbEntity::cast( pObj );
        if( pEnt == 0 ) continue;;
        pEnt->setLinetype( ltName );
    }
    actrTransactionManager->endTransaction();
}
AcDbObjectId ArxEntityHelper::GetLineTypeId( const CString& name )
{
    AcDbLinetypeTable* pTable;
    if( Acad::eOk != acdbHostApplicationServices()->workingDatabase()->getLinetypeTable( pTable, AcDb::kForRead ) )
        return AcDbObjectId::kNull;
    AcDbObjectId objId;
    pTable->getAt( name, objId );
    pTable->close();
    return objId;
}
CString ArxEntityHelper::GetLineTypeName( const AcDbObjectId& objId )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return _T( "" );
    AcDbObject* pObj;
    if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForRead ) )
    {
        actrTransactionManager->abortTransaction();
        return _T( "" );
    }
    AcDbLinetypeTableRecord* pEnt = AcDbLinetypeTableRecord::cast( pObj );
    if( pEnt == 0 )
    {
        actrTransactionManager->abortTransaction();
        return _T( "" );
    }
    AcString name;
    if( Acad::eOk != pEnt->getName( name ) )
    {
        actrTransactionManager->abortTransaction();
        return _T( "" );
    }
    actrTransactionManager->endTransaction();
    return name.kACharPtr();
}
CString ArxEntityHelper::GetTextStyleName( const AcDbObjectId& objId )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return _T( "" );
    AcDbObject* pObj;
    if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForRead ) )
    {
        actrTransactionManager->abortTransaction();
        return _T( "" );
    }
    AcDbTextStyleTableRecord* pEnt = AcDbTextStyleTableRecord::cast( pObj );
    if( pEnt == 0 )
    {
        actrTransactionManager->abortTransaction();
        return _T( "" );
    }
    AcString name;
    if( Acad::eOk != pEnt->getName( name ) )
    {
        actrTransactionManager->abortTransaction();
        return _T( "" );
    }
    actrTransactionManager->endTransaction();
    return name.kACharPtr();
}
CString ArxEntityHelper::GetDimStyleName( const AcDbObjectId& objId )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return _T( "" );
    AcDbObject* pObj;
    if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForRead ) )
    {
        actrTransactionManager->abortTransaction();
        return _T( "" );
    }
    AcDbDimStyleTableRecord* pEnt = AcDbDimStyleTableRecord::cast( pObj );
    if( pEnt == 0 )
    {
        actrTransactionManager->abortTransaction();
        return _T( "" );
    }
    AcString name;
    if( Acad::eOk != pEnt->getName( name ) )
    {
        actrTransactionManager->abortTransaction();
        return _T( "" );
    }
    actrTransactionManager->endTransaction();
    return name.kACharPtr();
}

void ArxEntityHelper::ToggleUndoRecording( const AcDbObjectId& objId, bool disable )
{
	AcDbEntity* pEnt = 0;
	if( Acad::eOk != acdbOpenObject( pEnt, objId, AcDb::kForWrite ) ) return;
	pEnt->disableUndoRecording( disable ? Adesk::kTrue : Adesk::kFalse ); // 打开或关闭undo记录
	pEnt->close();
}
