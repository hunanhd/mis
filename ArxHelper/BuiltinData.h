#pragma once
#include "AcDbObjectData.h"
/**
* 给CAD内置的图元和对象附加字段管理功能,比如
* 图层、直线、圆、圆弧、椭圆、多行文字等
* todo: 其它的类还需要慢慢的增加!
*/
// AcDbLayerTableRecord -- 图层
class AcDbLayerTableRecordData : public AcDbObjectData
{
public:
    AcDbLayerTableRecordData();
    virtual void onBeforeGetData( const CString& field ) const;
    virtual void onAfterSetData( const CString& field, const CString& value );
private:
    bool m_isFrozen;        // 冻结
    bool m_isOff;           // 关闭
    bool m_isLocked;        // 锁定
    int m_colorIndex;       // 颜色(ACI格式)
    CString m_lineType;     // 线型
    CString m_lineWeight;   // 线宽
};
// AcDbEntity -- 图元基类
class AcDbEntityData : public AcDbObjectData
{
public:
    // 派生类应重载这2个虚函数,同时也要在函数实现中调用该基类的虚函数
    virtual void onBeforeGetData( const CString& field ) const;
    virtual void onAfterSetData( const CString& field, const CString& value );
protected:
    AcDbEntityData();
private:
    CString m_layer;       // 图层
    int m_colorIndex;      // ACI颜色
    CString m_lineType;    // 线型
    CString m_lineWeight;  // 线宽
};
// AcDbLine -- 直线
class AcDbLineData : public AcDbEntityData
{
public:
    AcDbLineData();
    virtual void onBeforeGetData( const CString& field ) const;
    virtual void onAfterSetData( const CString& field, const CString& value );
private:
    AcGePoint3d m_spt, m_ept; // 起点、终点
};
// AcDbCircle -- 圆
class AcDbCircleData : public AcDbEntityData
{
public:
    AcDbCircleData();
    virtual void onBeforeGetData( const CString& field ) const;
    virtual void onAfterSetData( const CString& field, const CString& value );
private:
    AcGePoint3d m_cnt; // 中心点
    double m_radius;   // 半径
};
// AcDbArc -- 圆弧
class AcDbArcData : public AcDbEntityData
{
public:
    AcDbArcData();
    virtual void onBeforeGetData( const CString& field ) const;
    virtual void onAfterSetData( const CString& field, const CString& value );
private:
    AcGePoint3d m_cnt; // 中心点
    double m_radius;   // 半径
    double m_startAngle; // 起始角度
    double m_endAngle;   // 终止角度
};
// AcDbEllipse -- 椭圆
class AcDbEllipseData : public AcDbEntityData
{
public:
    AcDbEllipseData();
    virtual void onBeforeGetData( const CString& field ) const;
    virtual void onAfterSetData( const CString& field, const CString& value );
private:
    AcGePoint3d m_cnt;          // 中心点
    AcGeVector3d m_majorAxis;   // 长轴向量
    double m_radiusRatio;       // 长短轴比
};
// AcDbMText -- 多行文本
class AcDbMTextData : public AcDbEntityData
{
public:
    AcDbMTextData();
    virtual void onBeforeGetData( const CString& field ) const;
    virtual void onAfterSetData( const CString& field, const CString& value );
private:
    AcGePoint3d m_location;      // 位置
    AcGeVector3d m_direction;    // 方向向量
    double m_textHeight;         // 高度
    CString m_contents;          // 内容
};
// AcDbText -- 单行文本
class AcDbTextData : public AcDbEntityData
{
public:
    AcDbTextData();
    virtual void onBeforeGetData( const CString& field ) const;
    virtual void onAfterSetData( const CString& field, const CString& value );
private:
    AcGePoint3d m_position;   // 位置
    double m_rotation;        // 角度
    double m_height;          // 高度
    CString m_text;           // 内容
};