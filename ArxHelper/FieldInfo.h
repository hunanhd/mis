#pragma once
#include "dlimexp.h"
// 字段信息
class ARXHELPER_API FieldInfo
{
public:
    static void Get( const FieldInfo& info, CString& fieldType, CString& defaultValue, CString& tole, CString& editable, CString& toolTips );
    static void Set( FieldInfo& info, const CString& fieldType, const CString& defaultValue, const CString& tole, const CString& editable, const CString& toolTips );
	static CString ToJson(const FieldInfo& info); // 转换成json格式
	static FieldInfo FromJson(const CString& json_str); // 从json字符串中解析

public:
    FieldInfo();
    /*
     * 默认设置
     * 以下3个条件必须同时满足
     *     1) m_dt = DT_STRING
     *     2) m_enable = FT_EDIT | FT_SHOW
     *     3) m_descr = _T("")
     */
    bool isDefault() const;
    void initDefault();                // 恢复默认设置
    void revise();                     //  校正属性设置
    void copyFrom( const FieldInfo& info );
public:
    // 数据类型枚举
    enum DATA_TYPE
    {
        DT_STRING   = 0, // 字符串
        DT_INT      = 1, // 整数
        DT_NUMERIC  = 2, // 浮点数
        DT_BOOL     = 3, // 布尔类型
        DT_DATE     = 4, // 日期类型
        DT_LIST     = 5  // 列表类型(特殊的离散类型)
    };
    // 列表数据类型
    enum LIST_TYPE
    {
        LT_STRING = 0, // 字符串列表
        LT_INT    = 1  // 整数列表
    };
    enum FLAG_TYPE
    {
        FT_EDIT = 1, // 属性可编辑
        FT_SHOW = 2  // 属性可显示
    };
public:
    DATA_TYPE m_dt;                    // 默认为字符串类型(DT_STRING)
    int m_minValue2, m_maxValue2;      // 整数区间
    double m_minValue, m_maxValue;     // 浮点数区间
    LIST_TYPE m_lt;                    // 列表类型
    CString m_varName;                 // 列表变量名称
    unsigned int m_flag;               // 默认属性可编辑、可显示(FT_EDIT | FT_SHOW)
    CString m_tooltips;                // 简要说明
    int m_tolrance;						//精度
private:
    void initIntMinMaxValue();         // 设置整型数默认值m_minValue2=m_maxValue2=0
    void initNumericMinMaxValue();     // 设置浮点数默认值m_minValue=m_maxValue=0
    void initListType();               // 设置列表默认设置m_lt=DT_LIST m_varName=_T("")
};