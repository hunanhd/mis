#include "StdAfx.h"
#include "DictData.h"
#include "HelperClass.h"
#include "Dao.h"
//////////////////////////////////////////////////////////////////
///////////////////// 成员函数实现 /////////////////////////////
/////////////////////////////////////////////////////////////////
BaseDao* DictData::createDao()
{
    return DictData::CreateDao( getDataSource() );
}
//////////////////////////////////////////////////////////////////
///////////////// 静态成员函数实现 ////////////////////////////
/////////////////////////////////////////////////////////////////
bool DictData::GetAllFields( const AcDbObjectId& dictId, AcStringArray& fields )
{
    if( dictId.isNull() ) return false;
    ArxDictHelper::GetAllKeys( dictId, fields );
    return !fields.isEmpty();
}
bool DictData::GetAllDatas( const AcDbObjectId& dictId, AcStringArray& fields, AcStringArray& values )
{
    if( dictId.isNull() ) return false;
    if( !DictData::GetAllFields( dictId, fields ) ) return false;
	return DictData::GetDatas(dictId, fields, values);
}

bool DictData::GetDatas(const AcDbObjectId& dictId, const AcStringArray& fields, AcStringArray& values)
{
	if( dictId.isNull() ) return false;

	BaseDao* dao = DictData::CreateDao( dictId );
	if( dao == 0 ) return false;
	for( int i = 0; i < fields.length(); i++ )
	{
		CString value;
		bool ret = dao->getString( fields[i].kACharPtr(), value );
		values.append( value );
	}
	delete dao;
	return true;
}

bool DictData::ExportToJsonString( const AcDbObjectId& dictId, CString& json_str )
{
    AcStringArray fields, values;
    if( !DictData::GetAllDatas( dictId, fields, values ) )
    {
        json_str = NULL_JSON_STRING;
        return false;
    }
    else
    {
        // 将字段和值格式化成json字符串
        return JsonHelper::Format( fields, values, json_str );
    }
}
bool DictData::ImportFromJsonString( const AcDbObjectId& dictId, const CString& json_str )
{
    // 从json字符串中解析字段和值
    AcStringArray fields, values;
    if( !JsonHelper::Parse( json_str, fields, values ) ) return false;
    // 初始化数据
    return DictData::Init( dictId, fields, values );
}
bool DictData::Init( const AcDbObjectId& dictId, const AcStringArray& fields, const AcStringArray& values )
{
    if( dictId.isNull() ) return false;
    if( fields.length() != values.length() ) return false;
    if( fields.isEmpty() ) return false;
    BaseDao* dao = DictData::CreateDao( dictId );
    if( dao == 0 ) return false;
    dao->clearAllFields(); // 清空
    for( int i = 0; i < values.length(); i++ )
    {
        dao->setString( fields[i].kACharPtr(), values[i].kACharPtr() );
    }
    delete dao;
    return true;
}
bool DictData::GetString( const AcDbObjectId& dictId, const CString& field, CString& value )
{
    if( dictId.isNull() ) return false;
    BaseDao* dao = DictData::CreateDao( dictId );
    if( dao == 0 ) return false;
    bool ret = dao->getString( field, value );
    delete dao;
    return ret;
}
bool DictData::GetInt( const AcDbObjectId& dictId, const CString& field, int& value )
{
	CString str;
	if( !DictData::GetString( dictId, field, str ) ) return false;
	if( !StringHelper::StringToInt( str, value ) ) return false;
	return true;
}
bool DictData::GetDouble( const AcDbObjectId& dictId, const CString& field, double& value )
{
	CString str;
	if( !DictData::GetString( dictId, field, str ) ) return false;
	if( !StringHelper::StringToDouble( str, value ) ) return false;
	return true;
}
bool DictData::GetBool( const AcDbObjectId& dictId, const CString& field, bool& value )
{
	CString str;
	if( !DictData::GetString( dictId, field, str ) ) return false;
	int i = 0;
	if( !StringHelper::StringToInt( str, i ) ) return false;
	value = ( i != 0 );
	return true;
}
bool DictData::GetPoint( const AcDbObjectId& dictId, const CString& field, AcGePoint3d& value )
{
	CString str;
	if( !DictData::GetString( dictId, field, str ) ) return false;
	if( !ArxUtilHelper::StringToPoint3d( str, value ) ) return false;
	return true;
}
bool DictData::GetVector( const AcDbObjectId& dictId, const CString& field, AcGeVector3d& value )
{
	CString str;
	if( !DictData::GetString( dictId, field, str ) ) return false;
	AcGePoint3d pt;
	if( !ArxUtilHelper::StringToPoint3d( str, pt ) ) return false;
	value = pt.asVector();
	return true;
}
bool DictData::GetDateTime( const AcDbObjectId& dictId, const CString& field, COleDateTime& value )
{
	CString str;
	if( !DictData::GetString( dictId, field, str ) ) return false;
	return value.ParseDateTime( str );
}
bool DictData::GetObjectId( const AcDbObjectId& dictId, const CString& field, AcDbObjectId& value )
{
	CString str;
	if( !DictData::GetString( dictId, field, str ) ) return false;
	value = ArxUtilHelper::StrtoObjectId( str );
	return !value.isNull();
}
bool DictData::SetString( const AcDbObjectId& dictId, const CString& field, const CString& value )
{
    if( dictId.isNull() ) return false;
    BaseDao* dao = DictData::CreateDao( dictId );
    if( dao == 0 ) return false;
    bool ret = dao->setString( field, value );
    delete dao;
    return ret;
}
bool DictData::SetInt( const AcDbObjectId& dictId, const CString& field, int value )
{
	CString str;
	StringHelper::IntToString( value, str );
	return DictData::SetString( dictId, field, str );
}
bool DictData::SetDouble( const AcDbObjectId& dictId, const CString& field, double value )
{
	CString str;
	StringHelper::DoubleToString( value, str );
	return DictData::SetString( dictId, field, str );
}
bool DictData::SetBool( const AcDbObjectId& dictId, const CString& field, bool value )
{
	CString str;
	StringHelper::IntToString( value ? 1 : 0, str );
	return DictData::SetString( dictId, field, str );
}
bool DictData::SetPoint( const AcDbObjectId& dictId, const CString& field, const AcGePoint3d& value )
{
	CString str = ArxUtilHelper::Point3dToString( value );
	return DictData::SetString( dictId, field, str );
}
bool DictData::SetVector( const AcDbObjectId& dictId, const CString& field, const AcGeVector3d& value )
{
	return DictData::SetPoint( dictId, field, AcGePoint3d::kOrigin + value );
}
bool DictData::SetDateTime( const AcDbObjectId& dictId, const CString& field, const COleDateTime& value )
{
	CString str = value.Format( _T( "%Y-%m-%d %H:%M:%S" ) );
	return DictData::SetString( dictId, field, str );
}
bool DictData::SetObjectId( const AcDbObjectId& dictId, const CString& field, const AcDbObjectId& value )
{
	CString str = ArxUtilHelper::ObjectIdToStr( value );
	return DictData::SetString( dictId, field, str );
}
void DictData::Copy( const AcDbObjectId& sourceObjId, const AcDbObjectId& targetObjId )
{
}
BaseDao* DictData::CreateDao( const AcDbObjectId& dictId )
{
    if( dictId.isNull() ) return 0;
    return new DictDao( dictId );
}
DictData::DictData() : ObjectData()
{
}
