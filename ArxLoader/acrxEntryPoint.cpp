#include "StdAfx.h"
#include "resource.h"
#include "config.h"
#include "ArxHelper/HelperClass.h"

static CString BuildArxFileName( const CString& arxModuleName, const CString& ext = _T( "arx" ) )
{
    CString str;
    str.Format( _T( "%s.%s" ), arxModuleName, ext );
    return str;
}
static CString BuildServiceName( const CString& arxModuleName )
{
    CString str;
    str.Format( _T( "%s_SERVICE_NAME" ), arxModuleName );
    return str.MakeUpper();
}
class CArxLoaderApp : public AcRxArxApp
{
public:
    CArxLoaderApp () : AcRxArxApp () {}
    virtual AcRx::AppRetCode On_kInitAppMsg ( void* pkt )
    {
        AcRx::AppRetCode retCode = AcRxArxApp::On_kInitAppMsg ( pkt ) ;
        acrxRegisterAppMDIAware( pkt );
        // 加载模块
        if( !loadModules() ) return AcRx::kRetError;
        acutPrintf( _T( "\nArxLoader::On_kInitAppMsg\n" ) );
        return ( retCode ) ;
    }
    virtual AcRx::AppRetCode On_kUnloadAppMsg ( void* pkt )
    {
        AcRx::AppRetCode retCode = AcRxArxApp::On_kUnloadAppMsg ( pkt ) ;
        // 卸载模块
        unloadModules();
        acutPrintf( _T( "\nArxLoader::On_kUnloadAppMsg\n" ) );
        return ( retCode ) ;
    }
    virtual AcRx::AppRetCode On_kLoadDwgMsg( void* pkt )
    {
        AcRx::AppRetCode retCode = AcRxArxApp::On_kLoadDwgMsg ( pkt ) ;       
        ArxSysVarHelper::Set(_T("WSCURRENT"), _T("JLCAD"));
        return (retCode);
    }
    virtual AcRx::AppRetCode On_kUnloadDwgMsg( void* pkt )
    {
        AcRx::AppRetCode retCode = AcRxArxApp::On_kUnloadDwgMsg ( pkt ) ;       
        ArxSysVarHelper::Set(_T("WSCURRENT"), _T("AutoCAD 经典"));
        return (retCode);
    }
    virtual void RegisterServerComponents ()
    {
    }
    bool loadArxModule( const CString& arxModuleName, const CString& ext = _T( "arx" ) )
    {
        CString arxName = BuildArxFileName( arxModuleName, ext );
        CString serviceName = BuildServiceName( arxModuleName );
        AcRxObject* pSvc = acrxServiceDictionary->at ( serviceName );
        if( pSvc == NULL )
        {
            if( !acrxDynamicLinker->loadModule( arxName, false ) )
            {
                acutPrintf ( _T( "\n加载%s模块失败...\n" ), arxName ) ;
                return false;
            }
            acutPrintf ( _T( "\n成功加载%s模块...\n" ), arxName ) ;
        }
        else
        {
            acutPrintf ( _T( "\n%s模块已经加载过了...\n" ), arxName ) ;
        }
        return true;
    }
    void unloadArxModule( const CString& arxModuleName, const CString& ext = _T( "arx" ) )
    {
        acrxDynamicLinker->unloadModule( BuildArxFileName( arxModuleName, ext ), 0 );
        acutPrintf( _T( "\n卸载模块:%s\n" ), arxModuleName );
    }
    // 加载图元模块
    bool loadModules()
    {
        acutPrintf( _T( "\n-------- 加载模块 ------------\n" ) );
        if( !loadArxModule( _T( "ArxHelper" ) ) ) return false;
        if( !loadArxModule( _T( "MineGE" ) ) ) return false;
        if( !loadArxModule( _T( "VentGE" ) ) ) return false;
        if( !loadArxModule( _T( "GasGE" ) ) ) return false;
        if( !loadArxModule( _T( "MGSGE" ) ) ) return false;
		if( !loadArxModule( _T( "VngGE" ) ) ) return false;
        if( !loadArxModule( _T( "ArxData" ) ) ) return false;
        if( !loadArxModule( _T( "ArxCmds" ) ) ) return false;
        //if( !loadArxModule( _T( "LuaScript" ) ) ) return false;
        //if( !loadArxModule( _T( "arx" ), _T( "pyd" ) ) ) return false;
        //if( !loadArxModule( _T( "ArxVno" ) ) ) return false;
        //if( !loadArxModule( _T( "ArxVng" ) ) ) return false;

        return true;
    }
    void unloadModules()
    {
        acutPrintf( _T( "\n-------- 卸载模块 ------------" ) );
        //unloadArxModule( _T( "ArxVng" ) );
        //unloadArxModule( _T( "ArxVno" ) );
        //unloadArxModule( _T( "arx" ), _T( "pyd" ) );
        //unloadArxModule( _T( "LuaScript" ) );
        unloadArxModule( _T( "ArxCmds" ) );
        unloadArxModule( _T( "ArxData" ) );
        unloadArxModule( _T( "VngGE" ) );
		unloadArxModule( _T( "GasGE" ) );
        unloadArxModule( _T( "MGSGE" ) );
        unloadArxModule( _T( "VentGE" ) );
        unloadArxModule( _T( "MineGE" ) );
        unloadArxModule( _T( "ArxHelper" ) );
    }
} ;
IMPLEMENT_ARX_ENTRYPOINT( CArxLoaderApp )
//ACED_ARXCOMMAND_ENTRY_AUTO( CArxLoaderApp, VVTest, _testVector, tvec, ACRX_CMD_TRANSPARENT, NULL )