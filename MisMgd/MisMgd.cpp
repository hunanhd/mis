#include "StdAfx.h"
#include "MisMgd.h"

#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"

using namespace System::Collections::Generic;
using namespace System::Runtime::InteropServices;
using namespace arx;

static void ObjectIdArray_2_Collection(const AcDbObjectIdArray& objIds, ObjectIdCollection^ lists)
{
	//ObjectIdCollection^ lists = gcnew ObjectIdCollection();
	for(int i=0;i<objIds.length();i++)
	{
		lists->Add(ToObjectId(objIds[i]));
	}
	//return lists;
}

static void ObjectIdCollection_2_Array( ObjectIdCollection^ lists, AcDbObjectIdArray& objIds )
{
	//ObjectIdCollection^ lists = gcnew ObjectIdCollection();
	for(int i=0;i<lists->Count;i++)
	{
		objIds.append(GETOBJECTID(lists[i]));
	}
}

static void AcStringArray_2_StringList(const AcStringArray& values, List<String^>^ lists)
{
	//List<String^>^ lists = gcnew List<String^>();
	for(int i=0;i<values.length();i++)
	{
		lists->Add(WcharToString(values[i].kACharPtr()));
	}
	//return lists;
}

static void StringList_2_AcStringArray(List<String^>^ lists, AcStringArray& values)
{
	//List<String^>^ lists = gcnew List<String^>();
	for(int i=0;i<lists->Count;i++)
	{
		values.append((const wchar_t*)StringToWchar(lists[i]));
	}
	//return lists;
}

static void Point3dArray_2_Collection(const AcGePoint3dArray& values, Point3dCollection^ lists)
{
	//Point3dCollection^ lists = gcnew Point3dCollection();
	for(int i=0;i<values.length();i++)
	{
		lists->Add(ToPoint3d(values[i]));
	}
	//return lists;
}

static void Point3dCollection_2_Array(Point3dCollection^ lists, AcGePoint3dArray& values )
{
	//Point3dCollection^ lists = gcnew Point3dCollection();
	for(int i=0;i<lists->Count;i++)
	{
		values.append(GETPOINT3D(lists[i]));
	}
	//return lists;
}

static long DateTimeToTime_t(DateTime^ dateTime)
{
	long time_t;
	DateTime^ dt1 = gcnew DateTime(1970, 1, 1,0,0,0);
	TimeSpan ts = dateTime->Subtract(*dt1);
	time_t = ts.Ticks/10000000-28800;      
	return time_t;
}
void MUtil::Printf( String^ msg )
{
	acutPrintf( (const wchar_t*)StringToWchar(msg) );
}

#ifndef _WIN64
ObjectId MUtil::GetObjectId( long id )
{
	return ToObjectId(ArxUtilHelper::GetObjectId( id ));
}

long MUtil::GetLongId( ObjectId objId )
{
	return ArxUtilHelper::GetLongId( GETOBJECTID(objId) );
}
#else

ObjectId MUtil::GetObjectId( unsigned long id )
{
	return ToObjectId(ArxUtilHelper::GetObjectId( id ));
}

unsigned long MUtil::GetLongId( ObjectId objId )
{
	return ArxUtilHelper::GetLongId( GETOBJECTID(objId) );
}
#endif

String^ arx::MUtil::HandleToStr(Handle handle)
{
    return handle.ToString();
    //ArxUtilHelper::HandleToStr()
}

Handle arx::MUtil::StrToHandle(String^ str)
{
    return MUtil::StrtoObjectId(str).Handle;
}

String^ arx::MUtil::ObjectIdToStr(ObjectId objId)
{
    return WcharToString(( LPCTSTR )ArxUtilHelper::ObjectIdToStr(GETOBJECTID(objId)));
}

ObjectId arx::MUtil::StrtoObjectId(String^ str)
{
    return ToObjectId(ArxUtilHelper::StrtoObjectId((const wchar_t*)StringToWchar(str)));
}

Autodesk::AutoCAD::DatabaseServices::ObjectId arx::MUtil::GetApertureEntity()
{
    return ToObjectId(ArxUtilHelper::GetApertureEntity());
}

ObjectId MUtil::SelectEntity(String^ msg)
{
	return ToObjectId(ArxUtilHelper::SelectEntity( (const wchar_t*)StringToWchar(msg) ));
}

ObjectIdCollection^ MUtil::SelectMoreEntity( String^ msg )
{
	AcDbObjectIdArray objIds;
	ArxUtilHelper::SelectMoreEntity( (const wchar_t*)StringToWchar(msg), objIds );
	ObjectIdCollection^ _objIds = gcnew ObjectIdCollection();
	ObjectIdArray_2_Collection( objIds, _objIds );
	return _objIds;
}

ObjectIdCollection^ MUtil::GetPickSetEntity()
{
	AcDbObjectIdArray objIds;
	ArxUtilHelper::GetPickSetEntity( objIds );
	ObjectIdCollection^ _objIds = gcnew ObjectIdCollection();
	ObjectIdArray_2_Collection( objIds, _objIds );
	return _objIds;
}

ObjectIdCollection^ MUtil::GetPickSetEntity2()
{
	AcDbObjectIdArray objIds;
	ArxUtilHelper::GetPickSetEntity2( objIds );
	ObjectIdCollection^ _objIds = gcnew ObjectIdCollection();
	ObjectIdArray_2_Collection( objIds, _objIds );
	return _objIds;
}

bool MUtil::GetPoint( String^ msg, Point3d basePt, Point3d% pt)
{
	AcGePoint3d _pt;
	bool ret = ArxUtilHelper::GetPoint( (const wchar_t*)StringToWchar(msg), _pt, GETPOINT3D(basePt) );
	if(ret)
	{
		pt = ToPoint3d(_pt);
	}
	return ret;
}

bool MUtil::GetTwoPoint(Point3d% spt, Point3d% ept)
{
	AcGePoint3d _spt, _ept;
	bool ret = ArxUtilHelper::GetTwoPoint( _spt, _ept );
	if(ret)
	{
		spt = ToPoint3d(_spt);
		ept = ToPoint3d(_ept);
	}
	return ret;
}
bool MUtil::GetString( String^ msg, String^% str )
{
	CString _str;
	bool ret = ArxUtilHelper::GetString( (const wchar_t*)StringToWchar( msg ), _str );
	if(ret)
	{
		str = WcharToString((LPCTSTR)_str);
	}
	return ret;
}
bool MUtil::GetInt( String^ msg, int% i )
{
	int _i = 0;
	bool ret = ArxUtilHelper::GetInt( (const wchar_t*)StringToWchar( msg ), _i );
	if(ret)
	{
		i = _i;
	}
	return ret;
}
bool MUtil::GetReal( String^ msg, double% v )
{
	double _v = 0.0;
	bool ret = ArxUtilHelper::GetReal( (const wchar_t*)StringToWchar( msg ), _v );
	if(ret)
	{
		v = _v;
	}
	return v;
}
bool MUtil::GetAngle( String^ msg, Point3d pt, double% angle )
{
	double _angle = 0.0;
	bool ret = ArxUtilHelper::GetAngle( GETPOINT3D(pt), (const wchar_t*)StringToWchar( msg ), _angle );
	if(ret)
	{
		angle = _angle;
	}
	return ret;
}
void MUtil::Pause( String^ msg )
{
	ArxUtilHelper::Pause( (const wchar_t*)StringToWchar( msg ) );
}
double MUtil::AngleOfTwoPoint( Point3d spt, Point3d ept )
{
	return ArxUtilHelper::AngleOfTwoPoint( GETPOINT3D(spt), GETPOINT3D(ept) );
}
double MUtil::AngleToXAxis( Vector3d v )
{
	return ArxUtilHelper::AngleToXAxis( GETVECTOR3D(v) );
}
double MUtil::AdjustAngle( double angle )
{
	return ArxUtilHelper::AdjustAngle( angle );
}
String^ MUtil::Point3dToString( Point3d pt )
{
	CString str = ArxUtilHelper::Point3dToString( GETPOINT3D(pt) );
	return WcharToString( ( LPCTSTR )str );
}
bool MUtil::StringToPoint3d( String^ str, Point3d% pt )
{
	AcGePoint3d _pt;
	bool ret = ArxUtilHelper::StringToPoint3d( (const wchar_t*)StringToWchar( str ), _pt );
	if(ret)
	{
		pt = ToPoint3d(_pt);
	}
	return ret;
}
String^ MUtil::Vector3dToString( Vector3d v )
{
	CString str = ArxUtilHelper::Vector3dToString( GETVECTOR3D(v) );
	return WcharToString( ( LPCTSTR )str );
}
bool MUtil::StringToVector3d( String^ str, Vector3d% v )
{
	AcGeVector3d _v;
	bool ret = ArxUtilHelper::StringToVector3d( (const wchar_t*)StringToWchar( str ), _v );
	if(ret)
	{
		v = ToVector3d(_v);
	}
	return ret;
}
void MUtil::LONG_2_RGB( unsigned long rgbColor, unsigned int% r, unsigned int% g, unsigned int% b )
{
	Adesk::UInt8 _r, _g, _b;
	ArxUtilHelper::LONG_2_RGB( ( Adesk::UInt32 )rgbColor, _r, _g, _b );
	r = _r; g = _g; b = _b;
}
unsigned long MUtil::ACI_2_LONG( unsigned int colorIndex )
{
	Adesk::UInt32 rgb = ArxUtilHelper::ACI_2_LONG( ( Adesk::UInt16 )colorIndex );
	return ( unsigned long )rgb;
}
unsigned int MUtil::RGB_2_ACI( unsigned int r, unsigned int g, unsigned int b )
{
	return ( unsigned int )( ArxUtilHelper::RGB_2_ACI( ( Adesk::UInt8 )r, ( Adesk::UInt8 )g, ( Adesk::UInt8 )b ) );
}
void MUtil::ACI_2_RGB( unsigned int colorIndex, unsigned int% r, unsigned int% g, unsigned int% b )
{
	Adesk::UInt8 _r, _g, _b;
	ArxUtilHelper::ACI_2_RGB( ( Adesk::UInt16 )colorIndex, _r, _g, _b );
	r = _r; g = _g; b = _b;
}
unsigned long MUtil::RGB_2_LONG( unsigned int r, unsigned int g, unsigned int b )
{
	return ( unsigned long )( ArxUtilHelper::RGB_2_LONG( ( Adesk::UInt8 )r, ( Adesk::UInt8 )g, ( Adesk::UInt8 )b ) );
}
ObjectId MUtil::CreateEntity( String^ type )
{
	AcDbObjectId objId = ArxUtilHelper::CreateEntity( (const wchar_t*)StringToWchar( type ) );
	return ToObjectId(objId);
}

bool MUtil::GetTypeName(ObjectId objId, String^% type)
{
	CString _type;
	bool ret = ArxDataTool::GetTypeName( GETOBJECTID(objId), _type);
	if(ret)
	{
		type = WcharToString((LPCTSTR)_type);
	}
	return ret;
}

bool MUtil::IsEqualType(String^ type, ObjectId objId, bool isDerivedFromParent)
{
	return ArxDataTool::IsEqualType((const wchar_t*)StringToWchar(type), GETOBJECTID(objId), isDerivedFromParent);
}

bool MUtil::IsEqualType_NoTrans(String^ type, ObjectId objId, bool isDerivedFromParent)
{
	return ArxDataTool::IsEqualType_NoTrans((const wchar_t*)StringToWchar(type), GETOBJECTID(objId), isDerivedFromParent);
}

void MUtil::GetEntsByType(String^ type, ObjectIdCollection^% objIds, bool isDerivedFromParent)
{
	AcDbObjectIdArray _objIds;
	ArxDataTool::GetEntsByType((const wchar_t*)StringToWchar(type), _objIds, isDerivedFromParent);
	ObjectIdArray_2_Collection(_objIds, objIds);
}

void MUtil::GetErasedEntsByType(String^ type, ObjectIdCollection^% objIds, bool isDerivedFromParent)
{
	AcDbObjectIdArray _objIds;
	ArxDataTool::GetErasedEntsByType((const wchar_t*)StringToWchar(type), _objIds, isDerivedFromParent);
	ObjectIdArray_2_Collection(_objIds, objIds);
}

void MUtil::FilterEntsByType(String^ type, ObjectIdCollection^ allObjIds, ObjectIdCollection^% objIds)
{
	AcDbObjectIdArray _allObjIds;
	ObjectIdCollection_2_Array(allObjIds, _allObjIds);
	AcDbObjectIdArray _objIds;
	ArxDataTool::FilterEntsByType((const wchar_t*)StringToWchar(type), _allObjIds, _objIds);
	ObjectIdArray_2_Collection(_objIds, objIds);
}

void MUtil::Update( ObjectId objId )
{
	ArxEntityHelper::Update( GETOBJECTID(objId) );
}
void MUtil::ZoomToEntity( ObjectId objId )
{
	ArxEntityHelper::ZoomToEntity( GETOBJECTID(objId) );
}
void MUtil::ZoomToEntityForModeless( ObjectId objId )
{
	ArxEntityHelper::ZoomToEntityForModeless( GETOBJECTID(objId) );
}
void MUtil::ZoomToEntities( ObjectIdCollection^ objIds )
{
	AcDbObjectIdArray _objIds;
	ObjectIdCollection_2_Array( objIds, _objIds );
	ArxEntityHelper::ZoomToEntities( _objIds );
}
bool MUtil::HighlightEntity( ObjectId objId )
{
	return ArxEntityHelper::HighlightEntity( GETOBJECTID(objId) );
}
bool MUtil::HighlightEntities( ObjectIdCollection^ objIds )
{
	AcDbObjectIdArray _objIds;
	ObjectIdCollection_2_Array( objIds, _objIds );
	return ArxEntityHelper::HighlightEntities( _objIds );
}
ObjectId MUtil::GetTextStyleId( String^ name )
{
	return ToObjectId(ArxEntityHelper::GetTextStyleId( (const wchar_t*)StringToWchar( name ) ));
}
String^ MUtil::GetTextStyleName( ObjectId objId )
{
	CString name = ArxEntityHelper::GetTextStyleName( GETOBJECTID(objId) );
	return WcharToString( ( LPCTSTR )name );
}
ObjectId MUtil::GetDimStyleId( String^ name )
{
	return ToObjectId(ArxEntityHelper::GetDimStyleId( (const wchar_t*)StringToWchar( name ) ));
}
String^ MUtil::GetDimStyleName( ObjectId objId )
{
	CString name = ArxEntityHelper::GetDimStyleName( GETOBJECTID(objId) );
	return WcharToString( ( LPCTSTR )name );
}
ObjectId MUtil::GetLineTypeId( String^ name )
{
	return ToObjectId(ArxEntityHelper::GetLineTypeId( (const wchar_t*)StringToWchar( name ) ));
}
String^ MUtil::GetLineTypeName( ObjectId objId )
{
	CString name = ArxEntityHelper::GetLineTypeName( GETOBJECTID(objId) );
	return WcharToString( ( LPCTSTR )name );
}
unsigned int MUtil::GetEntityColor( ObjectId objId )
{
	Adesk::UInt16 colorIndex = 0;
	bool ret = ArxEntityHelper::GetEntityColor( GETOBJECTID(objId), colorIndex );
	return ( unsigned int )colorIndex;
}
bool MUtil::SetEntityColor( ObjectId objId, unsigned int colorIndex )
{
	return ArxEntityHelper::SetEntityColor( GETOBJECTID(objId), ( Adesk::UInt16 )colorIndex );
}

Point3d MDraw::MidPoint( Point3d pt1, Point3d pt2 )
{
	return ToPoint3d(ArxDrawHelper::MidPoint( GETPOINT3D(pt1), GETPOINT3D(pt2) ));
}
String^ MDraw::MakeUpperText( String^ inStr )
{
	CString str = ArxDrawHelper::MakeUpperText( (const wchar_t*)StringToWchar( inStr ) );
	return WcharToString( ( LPCTSTR )str );
}
String^ MDraw::MakeLowerText( String^ inStr )
{
	CString str = ArxDrawHelper::MakeLowerText( (const wchar_t*)StringToWchar( inStr ) );
	return WcharToString( ( LPCTSTR )str );
}
ObjectId MDraw::DrawMText( Point3d pt, double angle, String^ text, double height )
{
	return ToObjectId(ArxDrawHelper::DrawMText( GETPOINT3D(pt), angle, (const wchar_t*)StringToWchar( text ), height ));
}
ObjectId MDraw::DrawArc( Point3d spt, Point3d pt, Point3d ept )
{
	return ToObjectId(ArxDrawHelper::DrawArc( GETPOINT3D(spt), GETPOINT3D(pt), GETPOINT3D(ept) ));
}
ObjectId MDraw::DrawCircle( Point3d pt, double radius )
{
	return ToObjectId(ArxDrawHelper::DrawCircle( GETPOINT3D(pt), radius ));
}
ObjectId MDraw::DrawEllipse( Point3d pt, double width, double height )
{
	return ToObjectId(ArxDrawHelper::DrawEllipse( GETPOINT3D(pt), width, height ));
}
ObjectId MDraw::DrawLine1( Point3d spt, Point3d ept )
{
	return ToObjectId(ArxDrawHelper::DrawLine1( GETPOINT3D(spt), GETPOINT3D(ept) ));
}
ObjectId MDraw::DrawLine2( Point3d pt, double angle, double length )
{
	return ToObjectId(ArxDrawHelper::DrawLine1( GETPOINT3D(pt), angle, length ));
}
ObjectId MDraw::DrawPolyLine1( Point3d spt, Point3d ept, double width )
{
	return ToObjectId(ArxDrawHelper::DrawPolyLine( GETPOINT3D(spt), GETPOINT3D(ept), width ));
}
ObjectId MDraw::DrawPolyLine2( Point3d pt, double angle, double length, double width )
{
	return ToObjectId(ArxDrawHelper::DrawPolyLine( GETPOINT3D(pt), angle, length, width ));
}
ObjectId MDraw::DrawPolyLine3( Point3dCollection^ pts, bool isClosed )
{
	AcGePoint3dArray _pts;
	Point3dCollection_2_Array(pts, _pts);
	return ToObjectId(ArxDrawHelper::DrawPolyLine( _pts, isClosed ));
}
ObjectId MDraw::DrawSpline( Point3dCollection^ pts )
{
	AcGePoint3dArray _pts;
	Point3dCollection_2_Array( pts, _pts );
	return ToObjectId(ArxDrawHelper::DrawSpline( _pts ));
}
ObjectId MDraw::DrawRect1( Point3d cnt, double angle, double width, double height )
{
	return ToObjectId(ArxDrawHelper::DrawRect1( GETPOINT3D(cnt), angle, width, height ));
}
ObjectId MDraw::DrawRect2( Point3d pt, double angle, double width, double height )
{
	return ToObjectId(ArxDrawHelper::DrawRect2( GETPOINT3D(pt), angle, width, height ));
}
ObjectId MDraw::DrawPolygonHatch( Point3dCollection^ pts, String^ patName, double scale )
{
	AcGePoint3dArray _pts;
	Point3dCollection_2_Array( pts, _pts );
	return ToObjectId(ArxDrawHelper::DrawPolygonHatch( _pts, (const wchar_t*)StringToWchar( patName ), scale ));
}
ObjectId MDraw::Make2LineAngularDim( Point3d pt1, Point3d pt2, Point3d pt3, Point3d pt4, Point3d textPt )
{
	return ToObjectId(ArxDrawHelper::Make2LineAngularDim( GETPOINT3D(pt1), GETPOINT3D(pt2), GETPOINT3D(pt3), GETPOINT3D(pt4), GETPOINT3D(textPt) ));
}
ObjectId MDraw::Make3PointAngularDim( Point3d centerPt, Point3d pt1, Point3d pt2, Point3d textPt )
{
	return ToObjectId(ArxDrawHelper::Make3PointAngularDim( GETPOINT3D(centerPt), GETPOINT3D(pt1), GETPOINT3D(pt2), GETPOINT3D(textPt) ));
}
ObjectId MDraw::MakeAlignedDim( Point3d pt1, Point3d pt2, String^ text, double offset, bool clockwise )
{
	//return ArxDrawHelper::MakeAlignedDim(pt1, pt2, C2W( text ), offset, clockwise, C2W( dimStyle ));
	return ToObjectId(ArxDrawHelper::MakeAlignedDim( GETPOINT3D(pt1), GETPOINT3D(pt2), (const wchar_t*)StringToWchar( text ), offset, clockwise, DIM_STYLE_NAME1 ));
}
ObjectId MDraw::MakeDiametricDim( Point3d centerPt, double radius, Point3d textPt )
{
	return ToObjectId(ArxDrawHelper::MakeDiametricDim( GETPOINT3D(centerPt), radius, GETPOINT3D(textPt) ));
}
ObjectId MDraw::MakeOrdinateDim( bool useXAxis, Point3d featurePt, Point3d leaderPt )
{
	return ToObjectId(ArxDrawHelper::MakeOrdinateDim( ( Adesk::Boolean )useXAxis, GETPOINT3D(featurePt), GETPOINT3D(leaderPt) ));
}
ObjectId MDraw::MakeRadialDim( Point3d centerPt, double radius, Point3d textPt )
{
	return ToObjectId(ArxDrawHelper::MakeRadialDim( GETPOINT3D(centerPt), radius, GETPOINT3D(textPt) ));
}
ObjectId MDraw::MakeRotatedDim( Point3d pt1, Point3d pt2, double ang, Point3d textPt )
{
	return ToObjectId(ArxDrawHelper::MakeRotatedDim( GETPOINT3D(pt1), GETPOINT3D(pt2), ang, GETPOINT3D(textPt) ));
}

String^ MField::MakeHybirdField( String^ func, String^ field )
{
	CString str = FieldHelper::MakeHybirdField((const wchar_t*)StringToWchar( func ), (const wchar_t*)StringToWchar( field ));
	return WcharToString((LPCTSTR)str);
}
bool MField::SplitHybirdField( String^ hybirdField, String^% func, String^% field )
{
	CString _func, _field;
	bool ret = FieldHelper::SplitHybirdField((const wchar_t*)StringToWchar( hybirdField ), _func, _field );
	if(ret)
	{
		func = WcharToString((LPCTSTR)_func);
		field = WcharToString((LPCTSTR)_field);
	}
	return ret;
}
bool MField::AddField( String^ type, String^ hybirdField )
{
	return FieldHelper::AddField((const wchar_t*)StringToWchar( type ), (const wchar_t*)StringToWchar( hybirdField ) );
}
bool MField::RemoveField( String^ type, String^ hybirdField )
{
	return FieldHelper::RemoveField((const wchar_t*)StringToWchar( type ), (const wchar_t*)StringToWchar( hybirdField ) );
}
void MField::RemoveAllFields( String^ type )
{
	FieldHelper::RemoveAllFields((const wchar_t*)StringToWchar( type ) );
}
int MField::CountFields( String^ type )
{
	return FieldHelper::CountFields((const wchar_t*)StringToWchar( type ) );
}
bool MField::AddMoreFields( String^ type, List<String^>^ hybirdFields )
{
	AcStringArray _hybirdFields;
	StringList_2_AcStringArray(hybirdFields, _hybirdFields);
	return FieldHelper::AddMoreFields((const wchar_t*)StringToWchar( type ), _hybirdFields );
}
bool MField::RemoveMoreFields( String^ type, List<String^>^ hybirdFields )
{
	AcStringArray _hybirdFields;
	StringList_2_AcStringArray(hybirdFields, _hybirdFields);
	return FieldHelper::RemoveMoreFields((const wchar_t*)StringToWchar( type ), _hybirdFields );
}
List<String^>^ MField::GuessRealFields( String^ type, List<String^>^ fields )
{
	AcStringArray _fields, _hybirdFields;
	StringList_2_AcStringArray(fields, _fields);
	FieldHelper::GuessRealFields((const wchar_t*)StringToWchar( type ), _fields, _hybirdFields );
	List<String^>^ hybirdFields = gcnew List<String^>();
	AcStringArray_2_StringList(_hybirdFields, hybirdFields);
	return hybirdFields;
}
String^ MField::GuessRealField( String^ type, String^ field )
{
	CString str = FieldHelper::GuessRealField((const wchar_t*)StringToWchar( type ), (const wchar_t*)StringToWchar( field ) );
	return WcharToString((LPCTSTR)str);
}

List<String^>^ MField::GetAllFields( String^ type )
{
	AcStringArray _hybirdFields;
	FieldHelper::GetAllFields( (const wchar_t*)StringToWchar( type ), _hybirdFields );
	List<String^>^ hybirdFields = gcnew List<String^>();
	AcStringArray_2_StringList( _hybirdFields, hybirdFields );
	return hybirdFields;
}
List<String^>^ MField::GetAllRegTypes()
{
	AcStringArray _types;
	FieldHelper::GetAllRegTypes( _types );
	List<String^>^ types = gcnew List<String^>();
	AcStringArray_2_StringList( _types, types );
	return types;
}
List<String^>^ MField::GetFuncs( String^ type )
{
	AcStringArray _funcs;
	bool ret = FuncHelper::GetFuncs( (const wchar_t*)StringToWchar( type ), _funcs );
	List<String^>^ funcs = gcnew List<String^>();
	AcStringArray_2_StringList( _funcs, funcs );
	return funcs;
}
List<String^>^ MField::GetTypesOfFunc( String^ func )
{
	AcStringArray _types;
	bool ret = FuncHelper::GetTypes( (const wchar_t*)StringToWchar( func ), _types );
	List<String^>^ types = gcnew List<String^>();
	AcStringArray_2_StringList( _types, types );
	return types;
}
List<String^>^ MField::GetFieldsOfFunc( String^ type, String^ func )
{
	AcStringArray _fields;
	bool ret = FuncHelper::GetFields( (const wchar_t*)StringToWchar( type ), (const wchar_t*)StringToWchar( func ), _fields );
	List<String^>^ fields = gcnew List<String^>();
	AcStringArray_2_StringList( _fields, fields );
	return fields;
}

MDao::MDao()
{
}
MDao::~MDao()
{
}
String^ MDao::getString(String^ field)
{
	BaseDao* dao = getDao();
	if(dao == 0) return WcharToString(_T(""));

	CString value;
	dao->getString((const wchar_t*)StringToWchar(field), value);
	return WcharToString((LPCTSTR)value);
}
int MDao::getInt( String^ field )
{
	BaseDao* dao = getDao();
	if(dao == 0) return 0;

	int value = 0;
	dao->getInt((const wchar_t*)StringToWchar(field), value);
	return value;
}
double MDao::getDouble( String^ field )
{
	BaseDao* dao = getDao();
	if(dao == 0) return 0;

	double value = 0;
	dao->getDouble((const wchar_t*)StringToWchar(field), value);
	return value;
}
bool MDao::getBool( String^ field )
{
	BaseDao* dao = getDao();
	if(dao == 0) return false;

	bool value = false;
	dao->getBool((const wchar_t*)StringToWchar(field), value);
	return value;
}
Point3d MDao::getPoint( String^ field )
{
	BaseDao* dao = getDao();
	if(dao == 0) return ToPoint3d(AcGePoint3d());

	AcGePoint3d value;
	dao->getPoint((const wchar_t*)StringToWchar(field), value);
	return ToPoint3d(value);
}
Vector3d MDao::getVector( String^ field )
{
	BaseDao* dao = getDao();
	if(dao == 0) return ToVector3d(AcGeVector3d());

	AcGeVector3d value;
	dao->getVector((const wchar_t*)StringToWchar(field), value);
	return ToVector3d(value);
}
DateTime MDao::getDateTime( String^ field )
{
	BaseDao* dao = getDao();
	if(dao == 0) return DateTime::FromOADate( (DATE)COleDateTime() );

	COleDateTime value;
	dao->getDateTime((const wchar_t*)StringToWchar(field), value);
	return DateTime::FromOADate( (DATE)value );
}
ObjectId MDao::getObjectId( String^ field )
{
	BaseDao* dao = getDao();
	if(dao == 0) return ToObjectId(AcDbObjectId::kNull);

	 AcDbObjectId value;
	dao->getObjectId((const wchar_t*)StringToWchar(field), value);
	return ToObjectId(value);
}
bool MDao::setString(String^ field, String^ value)
{
	BaseDao* dao = getDao();
	if(dao == 0) return false;
	return dao->setString((const wchar_t*)StringToWchar(field), (const wchar_t*)StringToWchar(value));
}
bool MDao::setInt( String^ field, int value )
{
	BaseDao* dao = getDao();
	if(dao == 0) return false;
	return dao->setInt((const wchar_t*)StringToWchar(field), value);
}
bool MDao::setDouble( String^ field, double value )
{
	BaseDao* dao = getDao();
	if(dao == 0) return false;
	return dao->setDouble((const wchar_t*)StringToWchar(field), value);
}
bool MDao::setBool( String^ field, bool value )
{
	BaseDao* dao = getDao();
	if(dao == 0) return false;
	return dao->setBool((const wchar_t*)StringToWchar(field), value);
}
bool MDao::setPoint( String^ field, Point3d value )
{
	BaseDao* dao = getDao();
	if(dao == 0) return false;
	return dao->setPoint((const wchar_t*)StringToWchar(field), GETPOINT3D(value));
}
bool MDao::setVector( String^ field, Vector3d value )
{
	BaseDao* dao = getDao();
	if(dao == 0) return false;
	return dao->setVector((const wchar_t*)StringToWchar(field), GETVECTOR3D(value));
}
bool MDao::setDateTime( String^ field, DateTime value )
{
	BaseDao* dao = getDao();
	if(dao == 0) return false;
	return dao->setDateTime((const wchar_t*)StringToWchar(field), DateTimeToTime_t(value));
}
bool MDao::setObjectId( String^ field, ObjectId value )
{
	BaseDao* dao = getDao();
	if(dao == 0) return false;
	return dao->setObjectId((const wchar_t*)StringToWchar(field), GETOBJECTID(value));
}
bool MDao::addField(String^ field)
{
	BaseDao* dao = getDao();
	if(dao == 0) return false;
	return dao->addField( (const wchar_t*)StringToWchar(field) );
}
bool MDao::removeField(String^ field)
{
	BaseDao* dao = getDao();
	if(dao == 0) return false;
	return dao->removeField( (const wchar_t*)StringToWchar(field) );
}
void MDao::clearAllFields()
{
	BaseDao* dao = getDao();
	if(dao == 0) return;
	dao->clearAllFields();
}
bool MDao::isModified()
{
	BaseDao* dao = getDao();
	if(dao == 0) return false;
	return dao->isModified();
}

ObjectId MDict::Tool::GetExtensionDict( ObjectId objId )
{
	return ToObjectId(ArxDictTool::GetExtensionDict( GETOBJECTID(objId) ));
}
bool MDict::Tool::IsDictExist( String^ dictName )
{
	return ArxDictTool::IsDictExist( (const wchar_t*)StringToWchar( dictName ) );
}
ObjectId MDict::Tool::RegDict( String^ dictName )
{
	return ToObjectId(ArxDictTool::RegDict( (const wchar_t*)StringToWchar( dictName ) ));
}
void MDict::Tool::RemoveDict( String^ dictName )
{
	ArxDictTool::RemoveDict( (const wchar_t*)StringToWchar( dictName ) );
}
ObjectId MDict::Tool::GetDict( String^ dictName )
{
	return ToObjectId(ArxDictTool::GetDict( (const wchar_t*)StringToWchar( dictName ) ));
}

MDict::Dao::Dao( ObjectId dictId ) : m_dictId( dictId )
{
	dao = DictData::CreateDao( GETOBJECTID(dictId) );
}
MDict::Dao::~Dao()
{
	delete dao; dao = 0;
}
ObjectId MDict::Dao::release()
{
	delete dao;	dao = 0;
	return m_dictId;
}
BaseDao* MDict::Dao::getDao()
{
	return dao;
}

List<String^>^ MDict::Data::GetAllFields( ObjectId dictId )
{
	AcStringArray _fields;
	bool ret = DictData::GetAllFields( GETOBJECTID(dictId), _fields );
	List<String^>^ fields = gcnew List<String^>();
	if(ret)
	{
		AcStringArray_2_StringList(_fields, fields);
	}
	return fields;
}
bool MDict::Data::GetAllDatas( ObjectId dictId, List<String^>^% fields, List<String^>^% values )
{
	AcStringArray _fields, _values;
	bool ret = DictData::GetAllDatas( GETOBJECTID(dictId), _fields, _values );
	if(ret)
	{
		AcStringArray_2_StringList(_values, values);
		AcStringArray_2_StringList(_fields, fields);
	}
	return ret;
}
bool MDict::Data::GetDatas(ObjectId dictId, List<String^>^ fields, List<String^>^% values)
{
	AcStringArray _fields, _values;
	StringList_2_AcStringArray( fields, _fields );
	bool ret = DictData::GetDatas( GETOBJECTID(dictId), _fields, _values );
	if(ret)
	{
		AcStringArray_2_StringList(_values, values);
	}
	return ret;
}

String^ MDict::Data::ExportToJsonString( ObjectId dictId )
{
	CString _json_str = _T("{}");
	bool ret = DictData::ExportToJsonString( GETOBJECTID(dictId), _json_str );
	return WcharToString( ( LPCTSTR )_json_str );
}
bool MDict::Data::ImportFromJsonString( ObjectId dictId, String^ json_str )
{
	return DictData::ImportFromJsonString( GETOBJECTID(dictId), (const wchar_t*)StringToWchar( json_str ) );
}
bool MDict::Data::Init( ObjectId dictId, List<String^>^ fields, List<String^>^ values )
{
	AcStringArray _fields, _values;
	StringList_2_AcStringArray( fields, _fields );
	StringList_2_AcStringArray( values, _values );
	return DictData::Init( GETOBJECTID(dictId), _fields, _values );
}
String^ MDict::Data::GetString( ObjectId dictId, String^ field )
{
	CString value;
	bool ret = DictData::GetString( GETOBJECTID(dictId), (const wchar_t*)StringToWchar(field), value );
	return WcharToString((LPCTSTR)value);
}

int MDict::Data::GetInt( ObjectId dictId, String^ field )
{
	int value = 0;
	bool ret = DictData::GetInt( GETOBJECTID(dictId), (const wchar_t*)StringToWchar(field), value );
	return value;
}
double MDict::Data::GetDouble( ObjectId dictId, String^ field )
{
	double value = 0;
	bool ret = DictData::GetDouble( GETOBJECTID(dictId), (const wchar_t*)StringToWchar(field), value );
	return value;
}
bool MDict::Data::GetBool( ObjectId dictId, String^ field )
{
	bool value = false;
	bool ret = DictData::GetBool( GETOBJECTID(dictId), (const wchar_t*)StringToWchar(field), value );
	return value;
}
Point3d MDict::Data::GetPoint( ObjectId dictId, String^ field )
{
	AcGePoint3d value;
	bool ret = DictData::GetPoint( GETOBJECTID(dictId), (const wchar_t*)StringToWchar(field), value );
	return ToPoint3d(value);
}
Vector3d MDict::Data::GetVector( ObjectId dictId, String^ field )
{
	AcGeVector3d value;
	bool ret = DictData::GetVector( GETOBJECTID(dictId), (const wchar_t*)StringToWchar(field), value );
	return ToVector3d(value);
}
DateTime MDict::Data::GetDateTime( ObjectId dictId, String^ field )
{
	COleDateTime value;
	bool ret = DictData::GetDateTime( GETOBJECTID(dictId), (const wchar_t*)StringToWchar(field), value );
	return DateTime::FromOADate( (DATE)value );
}
ObjectId MDict::Data::GetObjectId( ObjectId dictId, String^ field )
{
	AcDbObjectId value;
	bool ret = DictData::GetObjectId( GETOBJECTID(dictId), (const wchar_t*)StringToWchar(field), value );
	return ToObjectId(value);
}

bool MDict::Data::SetString( ObjectId dictId, String^ field, String^ value )
{
	return DictData::SetString( GETOBJECTID(dictId), (const wchar_t*)StringToWchar( field ), (const wchar_t*)StringToWchar( value ) );
}
bool MDict::Data::SetInt( ObjectId dictId, String^ field, int value )
{
	return DictData::SetInt( GETOBJECTID(dictId), (const wchar_t*)StringToWchar( field ), value );
}
bool MDict::Data::SetDouble( ObjectId dictId, String^ field, double value )
{
	return DictData::SetDouble( GETOBJECTID(dictId), (const wchar_t*)StringToWchar( field ), value );
}
bool MDict::Data::SetBool( ObjectId dictId, String^ field, bool value )
{
	return DictData::SetBool( GETOBJECTID(dictId), (const wchar_t*)StringToWchar( field ), value );
}
bool MDict::Data::SetPoint( ObjectId dictId, String^ field, Point3d value )
{
	return DictData::SetPoint( GETOBJECTID(dictId), (const wchar_t*)StringToWchar( field ), GETPOINT3D(value) );
}
bool MDict::Data::SetVector( ObjectId dictId, String^ field, Vector3d value )
{
	return DictData::SetVector( GETOBJECTID(dictId), (const wchar_t*)StringToWchar( field ), GETVECTOR3D(value) );
}
bool MDict::Data::SetDateTime( ObjectId dictId, String^ field, DateTime value )
{
	return DictData::SetDateTime( GETOBJECTID(dictId), (const wchar_t*)StringToWchar( field ), DateTimeToTime_t(value) );
}
bool MDict::Data::SetObjectId( ObjectId dictId, String^ field, ObjectId value )
{
	return DictData::SetObjectId( GETOBJECTID(dictId), (const wchar_t*)StringToWchar( field ), GETOBJECTID(value) );
}

MExtDict::Dao::Dao( ObjectId objId ) : m_objId( objId )
{
	dao = ExtDictData::CreateDao( GETOBJECTID(objId) );
}
MExtDict::Dao::~Dao()
{
	delete dao; dao = 0;
}
ObjectId MExtDict::Dao::release()
{
	delete dao; dao = 0;
	return m_objId;
}
BaseDao* MExtDict::Dao::getDao()
{
	return dao;
}

List<String^>^ MExtDict::Data::GetAllFields( ObjectId objId )
{
	AcStringArray _fields;
	bool ret = ExtDictData::GetAllFields( GETOBJECTID(objId), _fields );
	List<String^>^ fields = gcnew List<String^>();
	if(ret)
	{
		AcStringArray_2_StringList(_fields, fields);
	}
	return fields;
}
bool MExtDict::Data::GetAllDatas( ObjectId objId, List<String^>^% fields, List<String^>^% values )
{
	AcStringArray _fields, _values;
	bool ret = ExtDictData::GetAllDatas( GETOBJECTID(objId), _fields, _values );
	if(ret)
	{
		AcStringArray_2_StringList(_values, values);
		AcStringArray_2_StringList(_fields, fields);
	}
	return ret;
}
bool MExtDict::Data::GetDatas(ObjectId objId, List<String^>^ fields, List<String^>^% values)
{
	AcStringArray _fields, _values;
	StringList_2_AcStringArray( fields, _fields );
	bool ret = ExtDictData::GetDatas( GETOBJECTID(objId), _fields, _values );
	if(ret)
	{
		AcStringArray_2_StringList(_values, values);
	}
	return ret;
}
String^ MExtDict::Data::ExportToJsonString( ObjectId objId )
{
	CString _json_str = _T("{}");
	bool ret = ExtDictData::ExportToJsonString( GETOBJECTID(objId), _json_str );
	return WcharToString( ( LPCTSTR )_json_str );
}
bool MExtDict::Data::ImportFromJsonString( ObjectId objId, String^ json_str )
{
	return ExtDictData::ImportFromJsonString( GETOBJECTID(objId), (const wchar_t*)StringToWchar( json_str ) );
}
bool MExtDict::Data::Init( ObjectId objId, List<String^>^ fields, List<String^>^ values )
{
	AcStringArray _fields, _values;
	StringList_2_AcStringArray( fields, _fields );
	StringList_2_AcStringArray( values, _values );
	return ExtDictData::Init( GETOBJECTID(objId), _fields, _values );
}
String^ MExtDict::Data::GetString( ObjectId objId, String^ field )
{
	CString value;
	bool ret = ExtDictData::GetString( GETOBJECTID(objId), (const wchar_t*)StringToWchar(field), value );
	return WcharToString((LPCTSTR)value);
}

int MExtDict::Data::GetInt( ObjectId objId, String^ field )
{
	int value = 0;
	bool ret = ExtDictData::GetInt( GETOBJECTID(objId), (const wchar_t*)StringToWchar(field), value );
	return value;
}
double MExtDict::Data::GetDouble( ObjectId objId, String^ field )
{
	double value = 0;
	bool ret = ExtDictData::GetDouble( GETOBJECTID(objId), (const wchar_t*)StringToWchar(field), value );
	return value;
}
bool MExtDict::Data::GetBool( ObjectId objId, String^ field )
{
	bool value = false;
	bool ret = ExtDictData::GetBool( GETOBJECTID(objId), (const wchar_t*)StringToWchar(field), value );
	return value;
}
Point3d MExtDict::Data::GetPoint( ObjectId objId, String^ field )
{
	AcGePoint3d value;
	bool ret = ExtDictData::GetPoint( GETOBJECTID(objId), (const wchar_t*)StringToWchar(field), value );
	return ToPoint3d(value);
}
Vector3d MExtDict::Data::GetVector( ObjectId objId, String^ field )
{
	AcGeVector3d value;
	bool ret = ExtDictData::GetVector( GETOBJECTID(objId), (const wchar_t*)StringToWchar(field), value );
	return ToVector3d(value);
}
DateTime MExtDict::Data::GetDateTime( ObjectId objId, String^ field )
{
	COleDateTime value;
	bool ret = ExtDictData::GetDateTime( GETOBJECTID(objId), (const wchar_t*)StringToWchar(field), value );
	return DateTime::FromOADate( (DATE)value );
}
ObjectId MExtDict::Data::GetObjectId( ObjectId objId, String^ field )
{
	AcDbObjectId value;
	bool ret = ExtDictData::GetObjectId( GETOBJECTID(objId), (const wchar_t*)StringToWchar(field), value );
	return ToObjectId(value);
}

bool MExtDict::Data::SetString( ObjectId objId, String^ field, String^ value )
{
	return ExtDictData::SetString( GETOBJECTID(objId), (const wchar_t*)StringToWchar( field ), (const wchar_t*)StringToWchar( value ) );
}
bool MExtDict::Data::SetInt( ObjectId objId, String^ field, int value )
{
	return ExtDictData::SetInt( GETOBJECTID(objId), (const wchar_t*)StringToWchar( field ), value );
}
bool MExtDict::Data::SetDouble( ObjectId objId, String^ field, double value )
{
	return ExtDictData::SetDouble( GETOBJECTID(objId), (const wchar_t*)StringToWchar( field ), value );
}
bool MExtDict::Data::SetBool( ObjectId objId, String^ field, bool value )
{
	return ExtDictData::SetBool( GETOBJECTID(objId), (const wchar_t*)StringToWchar( field ), value );
}
bool MExtDict::Data::SetPoint( ObjectId objId, String^ field, Point3d value )
{
	return ExtDictData::SetPoint( GETOBJECTID(objId), (const wchar_t*)StringToWchar( field ), GETPOINT3D(value) );
}
bool MExtDict::Data::SetVector( ObjectId objId, String^ field, Vector3d value )
{
	return ExtDictData::SetVector( GETOBJECTID(objId), (const wchar_t*)StringToWchar( field ), GETVECTOR3D(value) );
}
bool MExtDict::Data::SetDateTime( ObjectId objId, String^ field, DateTime value )
{
	return ExtDictData::SetDateTime( GETOBJECTID(objId), (const wchar_t*)StringToWchar( field ), DateTimeToTime_t(value) );
}
bool MExtDict::Data::SetObjectId( ObjectId objId, String^ field, ObjectId value )
{
	return ExtDictData::SetObjectId( GETOBJECTID(objId), (const wchar_t*)StringToWchar( field ), GETOBJECTID(value) );
}


MXData::Dao::Dao( ObjectId objId ) : m_objId( objId )
{
	dao = XData::CreateDao( GETOBJECTID(objId) );
}
MXData::Dao::~Dao()
{
	delete dao; dao = 0;
}
ObjectId MXData::Dao::release()
{
	delete dao; dao = 0;
	return m_objId;
}
BaseDao* MXData::Dao::getDao()
{
	return dao;
}

bool MXData::Data::Init( ObjectId objId, List<String^>^ values )
{
	AcStringArray _fields, _values;
	StringList_2_AcStringArray( values, _values );
	return XData::Init( GETOBJECTID(objId), _values );
}
String^ MXData::Data::GetString( ObjectId objId, int pos )
{
	CString value;
	bool ret = XData::GetString( GETOBJECTID(objId), pos, value );
	return WcharToString((LPCTSTR)value);
}

int MXData::Data::GetInt( ObjectId objId, int pos )
{
	int value = 0;
	bool ret = XData::GetInt( GETOBJECTID(objId), pos, value );
	return value;
}
double MXData::Data::GetDouble( ObjectId objId, int pos )
{
	double value = 0;
	bool ret = XData::GetDouble( GETOBJECTID(objId), pos, value );
	return value;
}
bool MXData::Data::GetBool( ObjectId objId, int pos )
{
	bool value = false;
	bool ret = XData::GetBool( GETOBJECTID(objId), pos, value );
	return value;
}
Point3d MXData::Data::GetPoint( ObjectId objId, int pos )
{
	AcGePoint3d value;
	bool ret = XData::GetPoint( GETOBJECTID(objId), pos, value );
	return ToPoint3d(value);
}
Vector3d MXData::Data::GetVector( ObjectId objId, int pos )
{
	AcGeVector3d value;
	bool ret = XData::GetVector( GETOBJECTID(objId), pos, value );
	return ToVector3d(value);
}
DateTime MXData::Data::GetDateTime( ObjectId objId, int pos )
{
	COleDateTime value;
	bool ret = XData::GetDateTime( GETOBJECTID(objId), pos, value );
	return DateTime::FromOADate( (DATE)value );
}
ObjectId MXData::Data::GetObjectId( ObjectId objId, int pos )
{
	AcDbObjectId value;
	bool ret = XData::GetObjectId( GETOBJECTID(objId), pos, value );
	return ToObjectId(value);
}

bool MXData::Data::SetString( ObjectId objId, int pos, String^ value )
{
	return XData::SetString( GETOBJECTID(objId), pos, (const wchar_t*)StringToWchar( value ) );
}
bool MXData::Data::SetInt( ObjectId objId, int pos, int value )
{
	return XData::SetInt( GETOBJECTID(objId), pos, value );
}
bool MXData::Data::SetDouble( ObjectId objId, int pos, double value )
{
	return XData::SetDouble( GETOBJECTID(objId), pos, value );
}
bool MXData::Data::SetBool( ObjectId objId, int pos, bool value )
{
	return XData::SetBool( GETOBJECTID(objId), pos, value );
}
bool MXData::Data::SetPoint( ObjectId objId, int pos, Point3d value )
{
	return XData::SetPoint( GETOBJECTID(objId), pos, GETPOINT3D(value) );
}
bool MXData::Data::SetVector( ObjectId objId, int pos, Vector3d value )
{
	return XData::SetVector( GETOBJECTID(objId), pos, GETVECTOR3D(value) );
}
bool MXData::Data::SetDateTime( ObjectId objId, int pos, DateTime value )
{
	return XData::SetDateTime( GETOBJECTID(objId), pos, DateTimeToTime_t(value) );
}
bool MXData::Data::SetObjectId( ObjectId objId, int pos, ObjectId value )
{
	return XData::SetObjectId( GETOBJECTID(objId), pos, GETOBJECTID(value) );
}

MJsonDict::Dao::Dao( ObjectId dictId, String^ user_key ) : m_dictId( dictId )
{
	dao = JsonDictData::CreateDao( GETOBJECTID(dictId), (const wchar_t*)StringToWchar(user_key) );
}
MJsonDict::Dao::~Dao()
{
	delete dao; dao = 0;
}
ObjectId MJsonDict::Dao::release()
{
	delete dao; dao = 0;
	return m_dictId;
}
BaseDao* MJsonDict::Dao::getDao()
{
	return dao;
}

List<String^>^ MJsonDict::Data::GetAllFields( ObjectId dictId, String^ user_key )
{
	AcStringArray _fields;
	bool ret = JsonDictData::GetAllFields( GETOBJECTID(dictId), user_key, _fields );
	List<String^>^ fields = gcnew List<String^>();
	if(ret)
	{
		AcStringArray_2_StringList(_fields, fields);
	}
	return fields;
}
bool MJsonDict::Data::GetAllDatas( ObjectId dictId, String^ user_key, List<String^>^% fields, List<String^>^% values )
{
	AcStringArray _fields, _values;
	bool ret = JsonDictData::GetAllDatas( GETOBJECTID(dictId), user_key, _fields, _values );
	if(ret)
	{
		AcStringArray_2_StringList(_values, values);
		AcStringArray_2_StringList(_fields, fields);
	}
	return ret;
}

bool MJsonDict::Data::Init( ObjectId dictId, String^ user_key, List<String^>^ fields, List<String^>^ values )
{
	AcStringArray _fields, _values;
	StringList_2_AcStringArray( fields, _fields );
	StringList_2_AcStringArray( values, _values );
	return JsonDictData::Init( GETOBJECTID(dictId), user_key, _fields, _values );
}
String^ MJsonDict::Data::GetString( ObjectId dictId, String^ user_key, String^ field )
{
	CString value;
	bool ret = JsonDictData::GetString( GETOBJECTID(dictId), user_key, (const wchar_t*)StringToWchar(field), value );
	return WcharToString((LPCTSTR)value);
}

int MJsonDict::Data::GetInt( ObjectId dictId, String^ user_key, String^ field )
{
	int value = 0;
	bool ret = JsonDictData::GetInt( GETOBJECTID(dictId), user_key, (const wchar_t*)StringToWchar(field), value );
	return value;
}
double MJsonDict::Data::GetDouble( ObjectId dictId, String^ user_key, String^ field )
{
	double value = 0;
	bool ret = JsonDictData::GetDouble( GETOBJECTID(dictId), user_key, (const wchar_t*)StringToWchar(field), value );
	return value;
}
bool MJsonDict::Data::GetBool( ObjectId dictId, String^ user_key, String^ field )
{
	bool value = false;
	bool ret = JsonDictData::GetBool( GETOBJECTID(dictId), user_key, (const wchar_t*)StringToWchar(field), value );
	return value;
}
Point3d MJsonDict::Data::GetPoint( ObjectId dictId, String^ user_key, String^ field )
{
	AcGePoint3d value;
	bool ret = JsonDictData::GetPoint( GETOBJECTID(dictId), user_key, (const wchar_t*)StringToWchar(field), value );
	return ToPoint3d(value);
}
Vector3d MJsonDict::Data::GetVector( ObjectId dictId, String^ user_key, String^ field )
{
	AcGeVector3d value;
	bool ret = JsonDictData::GetVector( GETOBJECTID(dictId), user_key, (const wchar_t*)StringToWchar(field), value );
	return ToVector3d(value);
}
DateTime MJsonDict::Data::GetDateTime( ObjectId dictId, String^ user_key, String^ field )
{
	COleDateTime value;
	bool ret = JsonDictData::GetDateTime( GETOBJECTID(dictId), user_key, (const wchar_t*)StringToWchar(field), value );
	return DateTime::FromOADate( (DATE)value );
}
ObjectId MJsonDict::Data::GetObjectId( ObjectId dictId, String^ user_key, String^ field )
{
	AcDbObjectId value;
	bool ret = JsonDictData::GetObjectId( GETOBJECTID(dictId), user_key, (const wchar_t*)StringToWchar(field), value );
	return ToObjectId(value);
}

bool MJsonDict::Data::SetString( ObjectId dictId, String^ user_key, String^ field, String^ value )
{
	return JsonDictData::SetString( GETOBJECTID(dictId), user_key, (const wchar_t*)StringToWchar( field ), (const wchar_t*)StringToWchar( value ) );
}
bool MJsonDict::Data::SetInt( ObjectId dictId, String^ user_key, String^ field, int value )
{
	return JsonDictData::SetInt( GETOBJECTID(dictId), user_key, (const wchar_t*)StringToWchar( field ), value );
}
bool MJsonDict::Data::SetDouble( ObjectId dictId, String^ user_key, String^ field, double value )
{
	return JsonDictData::SetDouble( GETOBJECTID(dictId), user_key, (const wchar_t*)StringToWchar( field ), value );
}
bool MJsonDict::Data::SetBool( ObjectId dictId, String^ user_key, String^ field, bool value )
{
	return JsonDictData::SetBool( GETOBJECTID(dictId), user_key, (const wchar_t*)StringToWchar( field ), value );
}
bool MJsonDict::Data::SetPoint( ObjectId dictId, String^ user_key, String^ field, Point3d value )
{
	return JsonDictData::SetPoint( GETOBJECTID(dictId), user_key, (const wchar_t*)StringToWchar( field ), GETPOINT3D(value) );
}
bool MJsonDict::Data::SetVector( ObjectId dictId, String^ user_key, String^ field, Vector3d value )
{
	return JsonDictData::SetVector( GETOBJECTID(dictId), user_key, (const wchar_t*)StringToWchar( field ), GETVECTOR3D(value) );
}
bool MJsonDict::Data::SetDateTime( ObjectId dictId, String^ user_key, String^ field, DateTime value )
{
	return JsonDictData::SetDateTime( GETOBJECTID(dictId), user_key, (const wchar_t*)StringToWchar( field ), DateTimeToTime_t(value) );
}
bool MJsonDict::Data::SetObjectId( ObjectId dictId, String^ user_key, String^ field, ObjectId value )
{
	return JsonDictData::SetObjectId( GETOBJECTID(dictId), user_key, (const wchar_t*)StringToWchar( field ), GETOBJECTID(value) );
}



MEntity::Dao::Dao( ObjectId objId ) : m_objId( objId ), m_pObjId(0)
{
	dao = EntityData::CreateDao( GETOBJECTID(objId) );
}
MEntity::Dao::Dao( String^ type ): m_pObjId(0)
{
	m_pObjId = new AcDbObjectId();
	dao = EntityData::CreateDao( (const wchar_t*)StringToWchar(type), *m_pObjId );
}
MEntity::Dao::~Dao()
{	
	delete m_pObjId; m_pObjId = 0;
	delete dao; dao = 0;
}
ObjectId MEntity::Dao::release()
{
	ObjectId objId = m_objId;
	if(m_pObjId != 0)
	{
		objId = ToObjectId(*m_pObjId);
		delete m_pObjId; m_pObjId = 0;
	}
	delete dao; dao = 0;
	return objId;
}
BaseDao* MEntity::Dao::getDao()
{
	return dao;
}

List<String^>^ MEntity::Data::GetAllFields( ObjectId objId )
{
	AcStringArray _fields;
	bool ret = EntityData::GetAllFields( GETOBJECTID(objId), _fields );
	List<String^>^ fields = gcnew List<String^>();
	if(ret)
	{
		AcStringArray_2_StringList(_fields, fields);
	}
	return fields;
}
bool MEntity::Data::GetAllDatas( ObjectId objId, List<String^>^% fields, List<String^>^% values )
{
	AcStringArray _fields, _values;
	bool ret = EntityData::GetAllDatas( GETOBJECTID(objId), _fields, _values );
	if(ret)
	{
		AcStringArray_2_StringList(_values, values);
		AcStringArray_2_StringList(_fields, fields);
	}
	return ret;
}
String^ MEntity::Data::ExportToJsonString( ObjectId objId )
{
	CString _json_str = _T("{}");
	bool ret = EntityData::ExportToJsonString( GETOBJECTID(objId), _json_str );
	return WcharToString( ( LPCTSTR )_json_str );
}
bool MEntity::Data::ImportFromJsonString( ObjectId objId, String^ json_str )
{
	return EntityData::ImportFromJsonString( GETOBJECTID(objId), (const wchar_t*)StringToWchar( json_str ) );
}
bool MEntity::Data::Init( ObjectId objId, List<String^>^ fields, List<String^>^ values )
{
	AcStringArray _fields, _values;
	StringList_2_AcStringArray( fields, _fields );
	StringList_2_AcStringArray( values, _values );
	return EntityData::Init( GETOBJECTID(objId), _fields, _values );
}
String^ MEntity::Data::GetString( ObjectId objId, String^ field )
{
	CString value;
	bool ret = EntityData::GetString( GETOBJECTID(objId), (const wchar_t*)StringToWchar(field), value );
	return WcharToString((LPCTSTR)value);
}

int MEntity::Data::GetInt( ObjectId objId, String^ field )
{
	int value = 0;
	bool ret = EntityData::GetInt( GETOBJECTID(objId), (const wchar_t*)StringToWchar(field), value );
	return value;
}
double MEntity::Data::GetDouble( ObjectId objId, String^ field )
{
	double value = 0;
	bool ret = EntityData::GetDouble( GETOBJECTID(objId), (const wchar_t*)StringToWchar(field), value );
	return value;
}
bool MEntity::Data::GetBool( ObjectId objId, String^ field )
{
	bool value = false;
	bool ret = EntityData::GetBool( GETOBJECTID(objId), (const wchar_t*)StringToWchar(field), value );
	return value;
}
Point3d MEntity::Data::GetPoint( ObjectId objId, String^ field )
{
	AcGePoint3d value;
	bool ret = EntityData::GetPoint( GETOBJECTID(objId), (const wchar_t*)StringToWchar(field), value );
	return ToPoint3d(value);
}
Vector3d MEntity::Data::GetVector( ObjectId objId, String^ field )
{
	AcGeVector3d value;
	bool ret = EntityData::GetVector( GETOBJECTID(objId), (const wchar_t*)StringToWchar(field), value );
	return ToVector3d(value);
}
DateTime MEntity::Data::GetDateTime( ObjectId objId, String^ field )
{
	COleDateTime value;
	bool ret = EntityData::GetDateTime( GETOBJECTID(objId), (const wchar_t*)StringToWchar(field), value );
	return DateTime::FromOADate( (DATE)value );
}
ObjectId MEntity::Data::GetObjectId( ObjectId objId, String^ field )
{
	AcDbObjectId value;
	bool ret = EntityData::GetObjectId( GETOBJECTID(objId), (const wchar_t*)StringToWchar(field), value );
	return ToObjectId(value);
}

bool MEntity::Data::SetString( ObjectId objId, String^ field, String^ value )
{
	return EntityData::SetString( GETOBJECTID(objId), (const wchar_t*)StringToWchar( field ), (const wchar_t*)StringToWchar( value ) );
}
bool MEntity::Data::SetInt( ObjectId objId, String^ field, int value )
{
	return EntityData::SetInt( GETOBJECTID(objId), (const wchar_t*)StringToWchar( field ), value );
}
bool MEntity::Data::SetDouble( ObjectId objId, String^ field, double value )
{
	return EntityData::SetDouble( GETOBJECTID(objId), (const wchar_t*)StringToWchar( field ), value );
}
bool MEntity::Data::SetBool( ObjectId objId, String^ field, bool value )
{
	return EntityData::SetBool( GETOBJECTID(objId), (const wchar_t*)StringToWchar( field ), value );
}
bool MEntity::Data::SetPoint( ObjectId objId, String^ field, Point3d value )
{
	return EntityData::SetPoint( GETOBJECTID(objId), (const wchar_t*)StringToWchar( field ), GETPOINT3D(value) );
}
bool MEntity::Data::SetVector( ObjectId objId, String^ field, Vector3d value )
{
	return EntityData::SetVector( GETOBJECTID(objId), (const wchar_t*)StringToWchar( field ), GETVECTOR3D(value) );
}
bool MEntity::Data::SetDateTime( ObjectId objId, String^ field, DateTime value )
{
	return EntityData::SetDateTime( GETOBJECTID(objId), (const wchar_t*)StringToWchar( field ), DateTimeToTime_t(value) );
}
bool MEntity::Data::SetObjectId( ObjectId objId, String^ field, ObjectId value )
{
	return EntityData::SetObjectId( GETOBJECTID(objId), (const wchar_t*)StringToWchar( field ), GETOBJECTID(value) );
}