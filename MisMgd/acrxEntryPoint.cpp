#include "StdAfx.h"
#include "resource.h"

// cli模块用到了这个宏,普通arx模块没有
AC_DECLARE_EXTENSION_MODULE(MisMgdDLL) ;

class CMisMgdApp : public AcRxArxApp {

public:
	CMisMgdApp () : AcRxArxApp () {}

	virtual AcRx::AppRetCode On_kInitAppMsg (void *pkt) {
		// Save critical data pointers before running the constructors (see afxdllx.h for details)
		// cli相关的初始化
		AFX_MODULE_STATE* pModuleState = AfxGetModuleState();
		pModuleState->m_pClassInit = pModuleState->m_classList;
		pModuleState->m_pFactoryInit = pModuleState->m_factoryList;
		pModuleState->m_classList.m_pHead = NULL;
		pModuleState->m_factoryList.m_pHead = NULL;

		MisMgdDLL.AttachInstance (_hdllInstance) ;
		InitAcUiDLL () ;

		AcRx::AppRetCode retCode =AcRxArxApp::On_kInitAppMsg (pkt) ;
		
		return (retCode) ;
	}

	virtual AcRx::AppRetCode On_kUnloadAppMsg (void *pkt) {
		AcRx::AppRetCode retCode =AcRxArxApp::On_kUnloadAppMsg (pkt) ;

		MisMgdDLL.DetachInstance () ;

		return (retCode) ;
	}

	virtual void RegisterServerComponents () {
	}

} ;

IMPLEMENT_ARX_ENTRYPOINT(CMisMgdApp)
