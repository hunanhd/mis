#pragma once

// arx自带的一个头文件,用于arx c++类与cli托管类之间的转换
#include "mgdinterop.h"
// 从网上找的,暂时没有用到
#include "AutoNativePtr.h"

#include "ArxHelper/HelperClass.h"

using namespace System;
using namespace System::Collections::Generic;
using namespace System::Runtime::InteropServices;

using namespace Autodesk::AutoCAD::DatabaseServices;
using namespace Autodesk::AutoCAD::Geometry;
using namespace Autodesk::AutoCAD::Runtime;

namespace arx {

	// 托管类ObjectId、Point3d、Vector3d类是struct类型
	// 在C#中struct代表值类型,因此不需要使用 ^
	public ref class MUtil
	{
	public:
		// 打印字符串
		static void Printf( String^ msg );
		// AcDbObjectId与long之间的转换
#ifndef _WIN64
		static ObjectId GetObjectId( long id );
		static long GetLongId( ObjectId^ objId );
#else
		static ObjectId GetObjectId( unsigned long id );
		static unsigned long GetLongId( ObjectId objId );
#endif // _WIN64

        // 句柄与字符串之间的转换
        static String^ HandleToStr( Handle handle );
        static Handle StrToHandle( String^ str );
        // id与字符串之间的转换
        static String^ ObjectIdToStr( ObjectId objId );
        static ObjectId StrtoObjectId( String^ str );

		/***************  封装ArxUtilHelper  *****************/
		/*
		 * 命令交互选择一个实体
		 * @example C#用法示例:
		 * objId = arx.select_entity("\n请选择一个图元:");
		 */
		static ObjectId SelectEntity( String^ msg );

		/*
		 * 命令交互选择多个实体
		 * @example C#用法示例:
		 * objIds = arx.select_more_entity("\n请选择多个图元:")
		 * arx.printf("\n选择的图元个数:" .. len(objIds))
		 * for objId in objIds:
		 *      arx.printf("\n%ld" % arx.long_id(objId))
		 */
		static ObjectIdCollection^ SelectMoreEntity( String^ msg );
		// 获取当前已选择的图元(用法同上)
		static ObjectIdCollection^ GetPickSetEntity();
		static ObjectIdCollection^ GetPickSetEntity2();

		/*
		 * 交互选择一个点坐标(返回值包括2个元素:第1个是状态,第2个点坐标)
		 * @example C#用法示例:
		 * # kOrigin是一个常量(C++注册给C#使用的全局变量),表示Point3d::kOrigin(坐标原点)
		 * ret, pt = arx.get_point("\n请选择第1点:", arx.kOrigin)
		 */
		static bool GetPoint( String^ msg, Point3d basePt, Point3d% pt);

		/*
		 * 命令行交互获取2点坐标.
		 * @example python用法示例:
		 * ret, spt, ept = arx.get_two_point();
		 */
		static bool GetTwoPoint(Point3d% spt, Point3d% ept);
		/*
		 * cad交互输入方法.
		 * @example python用法示例:
		 * ret, i = arx.get_int("\n请输入一个整数:")
		 * if ret:
		 *      arx.printf("\n用户输入的整数:%d" % i)
		 * ret, f = arx.get_real("\n请输入一个浮点数:")
		 * if ret:
		 *      arx.printf("\n用户输入的浮点数:%f" % f)
		 * ret, s = arx.get_string("\n请输入一个字符串:")
		 * if ret:
		 *      arx.printf("\n用户输入的字符串:%s" % s)
		 */
		static bool GetString( String^ msg, String^% str );
		static bool GetInt( String^ msg, int% i );
		static bool GetReal( String^ msg, double% d );
		static bool GetAngle( String^ msg, Point3d pt, double% angle );
		// 暂停
		static void Pause( String^ msg );
		// 计算两点构成的夹角
		static double AngleOfTwoPoint( Point3d startPt, Point3d endPt );
		// 计算向量v与x轴逆时针旋转的角度
		static double AngleToXAxis( Vector3d v );
		// 调整角度(正值，[0, 2*PI])
		// 可以输入任意大小的角度(不论正负，输入值可以超出[0, 2*PI]区间范围)
		static double AdjustAngle( double angle );
		/*
		 * CString < Point3d之间的转换.
		 * @example python用法示例:
		 * s = arx.point3d_to_string(pt)
		 * arx.printf("\n用户选择的点坐标:%s" % s)
		 * ret, pt = arx.string_to_point3d(s)
		 * if ret:
		 *      arx.printf("\n字符串转换成坐标:(%.3f, %.3f, %.3f)" % (pt.x, pt.y, pt.z))
		 * end
		 */
		static String^ Point3dToString( Point3d pt );
		static bool StringToPoint3d( String^ str, Point3d% pt );
		static String^ Vector3dToString( Vector3d v );
		static bool StringToVector3d( String^ str, Vector3d% v );
		// RGB和ACI颜色转换
		static void LONG_2_RGB( unsigned long rgbColor, unsigned int% r, unsigned int% g, unsigned int% b );
		static unsigned long ACI_2_LONG( unsigned int colorIndex );
		static void ACI_2_RGB( unsigned int colorIndex, unsigned int% r, unsigned int% g, unsigned int% b );
		static unsigned int RGB_2_ACI( unsigned int r, unsigned int g, unsigned int b );
		static unsigned long RGB_2_LONG( unsigned int r, unsigned int g, unsigned int b );

		// 根据类型名称动态创建图元
		static ObjectId CreateEntity( String^ type );

		static bool GetTypeName( ObjectId objId, String^% type );
		/* 判断图元是否与类型type匹配 */
		static bool IsEqualType( String^ type, ObjectId objId, bool isDerivedFromParent );
		/* 判断图元是否与类型type匹配 */
		static bool IsEqualType_NoTrans( String^ type, ObjectId objId, bool isDerivedFromParent );
		/* 在模型空间的块表记录中查找所有指定类型的图元(屏幕上可见的) */
		static void GetEntsByType( String^ type, ObjectIdCollection^% objIds, bool isDerivedFromParent );

		/* 在模型空间的块表记录中查找所有指定类型的图元(屏幕上不可见的，已被删除的) */
		static void GetErasedEntsByType( String^ type, ObjectIdCollection^% objIds, bool isDerivedFromParent );
		/* 过滤图元 */
		static void FilterEntsByType( String^ type, ObjectIdCollection^ allObjIds, ObjectIdCollection^% objIds );
	
		// 立即更新图元效果
		static void Update( ObjectId objId );
		// 缩放到图元实体
		static void ZoomToEntity( ObjectId objId );
		//非模态对话框下缩放图元实体
		static void ZoomToEntityForModeless( ObjectId objId );
		// 缩放多个图元的近似中心
		static void ZoomToEntities( ObjectIdCollection^ objIds );
		//高亮选中图元
		static bool HighlightEntity( ObjectId objId );
		//高亮选中多个图元
		static bool HighlightEntities( ObjectIdCollection^ objIds );
		//获取文字样式id
		static ObjectId GetTextStyleId( String^ name );
		// 获取文字样式的名称
		static String^ GetTextStyleName( ObjectId objId );
		//获取标注样式id
		static ObjectId GetDimStyleId( String^ name );
		// 获取标注样式的名称
		static String^ GetDimStyleName( ObjectId objId );
		// 获取线型id
		static ObjectId GetLineTypeId( String^ name );
		// 获取线型样式的名称
		static String^ GetLineTypeName( ObjectId objId );
		// 获取图元的颜色(使用ACI,取值0~256)
		static unsigned int GetEntityColor( ObjectId objId );
		// 设置图元的颜色(使用ACI,取值0~256)
		static bool SetEntityColor( ObjectId objId, unsigned int colorIndex );
        // 获取鼠标的当前图元(只有1个)
        static ObjectId GetApertureEntity();
	};

	public ref class MDraw 
	{
	public:
		/************* 封装ArxDrawHelper *********************/
		/*
		 * todo: 还差一些Point3d、AcGeVector3d的基本运算没封装
		 *       这2个C++类大量使用运算符重载来完成 坐标点 和 向量 之间的几何运算
		 *       但是lua不支持操作符重载,为了方便在lua中进行几何运算,就需要额外
		 *       提供一些几何运算函数给python使用!!!
		 */
		// (1) 点和向量相加(比如: pt + v)
		// (2) 向量和数相乘(比如: 0.5*v)
		// (3) 向量和向量相加(比如: v1+v2)
		// (4) 向量点乘(比如: v1*v2 )
		// (5) 向量长度(比如: v.lenght())
		// (6) 两点相减得到一个向量(比如: v = p2 - p1)
		// (7) 向量标准化(比如: v.normalize())
		// (8) 向量旋转(比如: v.rotateBy())
		// (9) 向量的角度(比如: v.angleTo())
		// 计算两点的中点
		static Point3d MidPoint( Point3d pt1, Point3d pt2 );
		// 构造上标文字(cad里的上标、下标要使用特殊的符号)
		static String^ MakeUpperText( String^ inStr );
		// 构造下标文字(cad里的上标、下标要使用特殊的符号)
		static String^ MakeLowerText( String^ inStr );
		// 绘制文字
		static ObjectId DrawMText( Point3d pt, double angle, String^ text, double height );
		// 绘制圆弧
		static ObjectId DrawArc( Point3d spt, Point3d pt, Point3d ept );
		// 绘制圆
		static ObjectId DrawCircle( Point3d pt, double radius );
		// 绘制椭圆
		static ObjectId DrawEllipse( Point3d pt, double width, double height );
		// 绘制直线
		static ObjectId DrawLine1( Point3d spt, Point3d ept );
		// 绘制直线
		static ObjectId DrawLine2( Point3d pt, double angle, double length );
		// 绘制多段线
		static ObjectId DrawPolyLine1( Point3d spt, Point3d ept, double width );
		// 绘制多段线
		static ObjectId DrawPolyLine2( Point3d pt, double angle, double length, double width );
		// 绘制多段线
		static ObjectId DrawPolyLine3( Point3dCollection^ pts, bool isClosed );
		// 绘制样条曲线
		static ObjectId DrawSpline( Point3dCollection^ pts );
		// 绘制矩形(用多段线绘制)
		static ObjectId DrawRect1( Point3d cnt, double angle, double width, double height );
		// 绘制矩形(用多段线绘制)
		static ObjectId DrawRect2( Point3d pt, double angle, double width, double height );
		// 绘制多边形填充
		static ObjectId DrawPolygonHatch( Point3dCollection^ pts, String^ patName, double scale );
		// 绘制渐变颜色填充
		//static ObjectId DrawCircleGradient( Point3d pt, double radius, String^ gradName, const AcCmColor& c1, const AcCmColor& c2 );
		// 两条直线夹角的角度标注
		static ObjectId Make2LineAngularDim( Point3d pt1, Point3d pt2, Point3d pt3, Point3d pt4, Point3d textPt );
		//圆或圆弧的角度标注
		static ObjectId Make3PointAngularDim( Point3d centerPt, Point3d pt1, Point3d pt2, Point3d textPt );
		//对齐标注(lua tinker不支持6个以上的参数!!!)
		static ObjectId MakeAlignedDim( Point3d pt1, Point3d pt2, String^ text, double offset, bool clockwise/*, String^ dimStyle*/ );
		//直径标注
		static ObjectId MakeDiametricDim( Point3d centerPt, double radius, Point3d textPt );
		//坐标/基线/连续标注
		static ObjectId MakeOrdinateDim( bool useXAxis, Point3d featurePt, Point3d leaderPt );
		//半径/折弯标注
		static ObjectId MakeRadialDim( Point3d centerPt, double radius, Point3d textPt );
		static ObjectId MakeRotatedDim( Point3d pt1, Point3d pt2, double ang, Point3d textPt );
	};

	public ref class MField 
	{
	public:
		// 分组+字段名拼接、拆分操作
		static String^ MakeHybirdField( String^ func, String^ field );            // 构造混合字段名
		static bool SplitHybirdField( String^ hybirdField, String^% func, String^% field ); // 提取分组和字段名
		//字段操作
		static bool AddField( String^ type, String^ hybirdField );      // 添加字段
		static bool RemoveField( String^ type, String^ hybirdField );   // 删除字段
		static void RemoveAllFields( String^ type );                           // 删除所有字段
		static int CountFields( String^ type );                                // 统计图元的字段个数
		static bool AddMoreFields( String^ type, List<String^>^ hybirdFields );      // 添加多个字段
		static bool RemoveMoreFields( String^ type, List<String^>^ hybirdFields );   // 删除多个字段
		// 根据简写的字段猜测包含分组的完整字段名
		static List<String^>^ GuessRealFields( String^ type, List<String^>^ fields );
		static String^ GuessRealField( String^ type, String^ field );

		// 获取图元所有的字段
		static List<String^>^ GetAllFields( String^ type );
		// 获取当前已注册的图元类型
		static List<String^>^ GetAllRegTypes();
		/************* 封装FuncHelper *********************/
		// 获取所有功能
		static List<String^>^ GetFuncs( String^ type );
		// 获取功能下所有的类型
		static List<String^>^ GetTypesOfFunc( String^ func );
		// 获取字段
		static List<String^>^ GetFieldsOfFunc( String^ type, String^ func );
	};

	public ref class MDao abstract
	{
	public:
		// 构造函数
		MDao();
		// 析构函数
		virtual ~MDao();
		// 释放资源
		virtual ObjectId release() = 0; // 代替析构函数~DictDataDao(), 要手动调用该函数进行析构!!!

		// 获取几何参数的值
		String^ getString( String^ field );
		int getInt( String^ field );
		double getDouble( String^ field );
		bool getBool( String^ field );
		Point3d getPoint( String^ field );
		Vector3d getVector( String^ field );
		DateTime getDateTime( String^ field );
		ObjectId getObjectId( String^ field );

		// 设置几何参数的值
		bool setString( String^ field, String^ value );
		bool setInt( String^ field, int value );
		bool setDouble( String^ field, double value );
		bool setBool( String^ field, bool value );
		bool setPoint( String^ field, Point3d value );
		bool setVector( String^ field, Vector3d value );
		bool setDateTime( String^ field, DateTime value );
		bool setObjectId( String^ field, ObjectId value );

		// 字段操作
		bool addField( String^ key );
		bool removeField( String^ key );
		void clearAllFields();
		// 数据是否被修改
		bool isModified();

	protected:
		virtual BaseDao* getDao() = 0;
	};

	namespace MDict	{
		public ref class Tool
		{
		public:
			//获取扩展词典
			static ObjectId GetExtensionDict( ObjectId objId );
			// 判断词典是否存在
			static bool IsDictExist( String^ dictName );
			// 注册添加词典
			static ObjectId RegDict( String^ dictName );
			// 删除词典
			static void RemoveDict( String^ dictName );
			// 获取词典id
			static ObjectId GetDict( String^ dictName );
		};

		// 封装DictData的dao给C#使用
		public ref class Dao : public MDao
		{
		public:
			Dao( ObjectId dictId );
			virtual ~Dao();
			virtual ObjectId release() override;
		protected:
			virtual BaseDao* getDao() override;
		private:
			ObjectId m_dictId;
			BaseDao* dao;
		};

		public ref class Data
		{
		public:
			// 获取词典的所有字段
			static List<String^>^ GetAllFields( ObjectId dictId );
			// 获取词典的所有字段以及值
			static bool GetAllDatas( ObjectId dictId, List<String^>^% fields, List<String^>^% values );
			static bool GetDatas( ObjectId dictId, List<String^>^ fields, List<String^>^% values );
			// 导出词典的字段和值为json格式
			static String^ ExportToJsonString( ObjectId dictId );
			// 从json字符串中导入数据
			static bool ImportFromJsonString( ObjectId dictId, String^ json_str );
			//初始化数据
			static bool Init( ObjectId dictId, List<String^>^ fields, List<String^>^ values );
			// 获取属性数据
			// 如果字段不存在，返回false
			static String^ GetString( ObjectId dictId, String^ field );
			static int GetInt( ObjectId dictId, String^ field );
			static double GetDouble( ObjectId dictId, String^ field );
			static bool GetBool( ObjectId dictId, String^ field );
			static Point3d GetPoint( ObjectId dictId, String^ field );
			static Vector3d GetVector( ObjectId dictId, String^ field );
			static DateTime GetDateTime( ObjectId dictId, String^ field );
			static ObjectId GetObjectId( ObjectId dictId, String^ field );
			// 修改属性数据
			static bool SetString( ObjectId dictId, String^ field, String^ value );
			static bool SetInt( ObjectId dictId, String^ field, int value );
			static bool SetDouble( ObjectId dictId, String^ field, double value );
			static bool SetBool( ObjectId dictId, String^ field, bool value );
			static bool SetPoint( ObjectId dictId, String^ field, Point3d value );
			static bool SetVector( ObjectId dictId, String^ field, Vector3d value );
			static bool SetDateTime( ObjectId dictId, String^ field, DateTime value );
			static bool SetObjectId( ObjectId dictId, String^ field, ObjectId value );
			//// 复制属性数据(未实现)
			//static void Copy( ObjectId sourceObjId, ObjectId targetObjId );
		};
	}
	namespace MExtDict {
		// 封装DictData的dao给C#使用
		public ref class Dao : public MDao
		{
		public:
			Dao( ObjectId objId );
			virtual ~Dao();
			virtual ObjectId release() override;
		protected:
			virtual BaseDao* getDao() override;
		private:
			ObjectId m_objId;
			BaseDao* dao;
		};

		public ref class Data
		{
		public:
			// 获取词典的所有字段
			static List<String^>^ GetAllFields( ObjectId objId );
			// 获取词典的所有字段以及值
			static bool GetAllDatas( ObjectId objId, List<String^>^% fields, List<String^>^% values );
			static bool GetDatas( ObjectId objId, List<String^>^ fields, List<String^>^% values );
			// 导出词典的字段和值为json格式
			static String^ ExportToJsonString( ObjectId objId );
			// 从json字符串中导入数据
			static bool ImportFromJsonString( ObjectId objId, String^ json_str );
			//初始化数据
			static bool Init( ObjectId objId, List<String^>^ fields, List<String^>^ values );
			// 获取属性数据
			// 如果字段不存在，返回false
			static String^ GetString( ObjectId objId, String^ field );
			static int GetInt( ObjectId objId, String^ field );
			static double GetDouble( ObjectId objId, String^ field );
			static bool GetBool( ObjectId objId, String^ field );
			static Point3d GetPoint( ObjectId objId, String^ field );
			static Vector3d GetVector( ObjectId objId, String^ field );
			static DateTime GetDateTime( ObjectId objId, String^ field );
			static ObjectId GetObjectId( ObjectId objId, String^ field );
			// 修改属性数据
			static bool SetString( ObjectId objId, String^ field, String^ value );
			static bool SetInt( ObjectId objId, String^ field, int value );
			static bool SetDouble( ObjectId objId, String^ field, double value );
			static bool SetBool( ObjectId objId, String^ field, bool value );
			static bool SetPoint( ObjectId objId, String^ field, Point3d value );
			static bool SetVector( ObjectId objId, String^ field, Vector3d value );
			static bool SetDateTime( ObjectId objId, String^ field, DateTime value );
			static bool SetObjectId( ObjectId objId, String^ field, ObjectId value );
			//// 复制属性数据(未实现)
			//static void Copy( ObjectId sourceObjId, ObjectId targetObjId );
		};
	}

	namespace MXData {
		// 封装XData的dao给C#使用
		public ref class Dao : public MDao
		{
		public:
			Dao( ObjectId objId );
			virtual ~Dao();
			virtual ObjectId release() override;
		protected:
			virtual BaseDao* getDao() override;
		private:
			ObjectId m_objId;
			BaseDao* dao;
		};

		public ref class Data
		{
		public:
			//初始化数据
			//static bool Init( ObjectId objId, int n );
			static bool Init( ObjectId objId, List<String^>^ values );
			// 获取属性数据
			// 如果字段不存在，返回false
			static String^ GetString( ObjectId objId, int pos );
			static int GetInt( ObjectId objId, int pos );
			static double GetDouble( ObjectId objId, int pos );
			static bool GetBool( ObjectId objId, int pos );
			static Point3d GetPoint( ObjectId objId, int pos );
			static Vector3d GetVector( ObjectId objId, int pos );
			static DateTime GetDateTime( ObjectId objId, int pos );
			static ObjectId GetObjectId( ObjectId objId, int pos );
			// 修改属性数据
			static bool SetString( ObjectId objId, int pos, String^ value );
			static bool SetInt( ObjectId objId, int pos, int value );
			static bool SetDouble( ObjectId objId, int pos, double value );
			static bool SetBool( ObjectId objId, int pos, bool value );
			static bool SetPoint( ObjectId objId, int pos, Point3d value );
			static bool SetVector( ObjectId objId, int pos, Vector3d value );
			static bool SetDateTime( ObjectId objId, int pos, DateTime value );
			static bool SetObjectId( ObjectId objId, int pos, ObjectId value );
			//// 复制属性数据(未实现)
			//static void Copy( ObjectId sourceObjId, ObjectId targetObjId );
		};
	}

	namespace MJsonDict  {
		// 封装DictData的dao给C#使用
		public ref class Dao : public MDao
		{
		public:
			Dao( ObjectId dictId, String^ user_key );
			virtual ~Dao();
			virtual ObjectId release() override;
		protected:
			virtual BaseDao* getDao() override;
		private:
			ObjectId m_dictId;
			BaseDao* dao;
		};

		public ref class Data
		{
		public:
			// 获取词典的所有字段
			static List<String^>^ GetAllFields( ObjectId dictId, String^ user_key );
			// 获取词典的所有字段以及值
			static bool GetAllDatas( ObjectId dictId, String^ user_key, List<String^>^% fields, List<String^>^% values );
			// 导出词典的字段和值为json格式
			//static String^ ExportToJsonString( ObjectId dictId, String^ user_key );
			// 从json字符串中导入数据
			//static bool ImportFromJsonString( ObjectId dictId, String^ user_key, String^ json_str );
			//初始化数据
			static bool Init( ObjectId dictId, String^ user_key, List<String^>^ fields, List<String^>^ values );
			// 获取属性数据
			// 如果字段不存在，返回false
			static String^ GetString( ObjectId dictId, String^ user_key, String^ field );
			static int GetInt( ObjectId dictId, String^ user_key, String^ field );
			static double GetDouble( ObjectId dictId, String^ user_key, String^ field );
			static bool GetBool( ObjectId dictId, String^ user_key, String^ field );
			static Point3d GetPoint( ObjectId dictId, String^ user_key, String^ field );
			static Vector3d GetVector( ObjectId dictId, String^ user_key, String^ field );
			static DateTime GetDateTime( ObjectId dictId, String^ user_key, String^ field );
			static ObjectId GetObjectId( ObjectId dictId, String^ user_key, String^ field );
			// 修改属性数据
			static bool SetString( ObjectId dictId, String^ user_key, String^ field, String^ value );
			static bool SetInt( ObjectId dictId, String^ user_key, String^ field, int value );
			static bool SetDouble( ObjectId dictId, String^ user_key, String^ field, double value );
			static bool SetBool( ObjectId dictId, String^ user_key, String^ field, bool value );
			static bool SetPoint( ObjectId dictId, String^ user_key, String^ field, Point3d value );
			static bool SetVector( ObjectId dictId, String^ user_key, String^ field, Vector3d value );
			static bool SetDateTime( ObjectId dictId, String^ user_key, String^ field, DateTime value );
			static bool SetObjectId( ObjectId dictId, String^ user_key, String^ field, ObjectId value );
			//// 复制属性数据(未实现)
			//static void Copy( ObjectId sourceObjId, ObjectId targetObjId );
		};
	}

	namespace MEntity {
		// 封装EntityData的dao给C#使用
		public ref class Dao : public MDao
		{
		public:
			Dao( ObjectId objId );
			Dao( String^ type );
			virtual ~Dao();
			virtual ObjectId release() override;
		protected:
			virtual BaseDao* getDao() override;
		private:
			AcDbObjectId* m_pObjId;
			ObjectId m_objId;
			BaseDao* dao;
		};

		public ref class Data
		{
		public:
			// 获取词典的所有字段
			static List<String^>^ GetAllFields( ObjectId objId );
			// 获取词典的所有字段以及值
			static bool GetAllDatas( ObjectId objId, List<String^>^% fields, List<String^>^% values );
			// 导出词典的字段和值为json格式
			static String^ ExportToJsonString( ObjectId objId );
			// 从json字符串中导入数据
			static bool ImportFromJsonString( ObjectId objId, String^ json_str );
			//初始化数据
			static bool Init( ObjectId objId, List<String^>^ fields, List<String^>^ values );
			// 获取属性数据
			// 如果字段不存在，返回false
			static String^ GetString( ObjectId objId, String^ field );
			static int GetInt( ObjectId objId, String^ field );
			static double GetDouble( ObjectId objId, String^ field );
			static bool GetBool( ObjectId objId, String^ field );
			static Point3d GetPoint( ObjectId objId, String^ field );
			static Vector3d GetVector( ObjectId objId, String^ field );
			static DateTime GetDateTime( ObjectId objId, String^ field );
			static ObjectId GetObjectId( ObjectId objId, String^ field );
			// 修改属性数据
			static bool SetString( ObjectId objId, String^ field, String^ value );
			static bool SetInt( ObjectId objId, String^ field, int value );
			static bool SetDouble( ObjectId objId, String^ field, double value );
			static bool SetBool( ObjectId objId, String^ field, bool value );
			static bool SetPoint( ObjectId objId, String^ field, Point3d value );
			static bool SetVector( ObjectId objId, String^ field, Vector3d value );
			static bool SetDateTime( ObjectId objId, String^ field, DateTime value );
			static bool SetObjectId( ObjectId objId, String^ field, ObjectId value );
			//// 复制属性数据(未实现)
			//static void Copy( ObjectId sourceObjId, ObjectId targetObjId );
		};
	}
}