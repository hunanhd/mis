#pragma once

#include "dlimexp.h"
#include "dbmain.h"

// ����ͼͼԪ����
class VNG_GE_EXPORT_API VngGE : public AcDbEntity 
{
public:
	ACRX_DECLARE_MEMBERS(VngGE) ;

protected:
	static Adesk::UInt32 kCurrentVersionNumber ;

public:
	virtual ~VngGE () ;

	//virtual Acad::ErrorStatus dwgOutFields (AcDbDwgFiler *pFiler) const;
	//virtual Acad::ErrorStatus dwgInFields (AcDbDwgFiler *pFiler);

protected:
	VngGE () ;
} ;

#ifdef VNG_GE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO(VngGE)
#endif
