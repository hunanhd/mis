#include "StdAfx.h"
#include "VngGE.h"

Adesk::UInt32 VngGE::kCurrentVersionNumber = 1 ;

ACRX_NO_CONS_DEFINE_MEMBERS( VngGE, AcDbEntity )

VngGE::VngGE () : AcDbEntity ()
{
}

VngGE::~VngGE ()
{
}

//Acad::ErrorStatus VngGE::dwgOutFields ( AcDbDwgFiler* pFiler ) const
//{
//    assertReadEnabled () ;
//    //----- Save parent class information first.
//    Acad::ErrorStatus es = AcDbEntity::dwgOutFields ( pFiler ) ;
//    if ( es != Acad::eOk )
//        return ( es ) ;
//    //----- Object version number needs to be saved first
//    if ( ( es = pFiler->writeUInt32 ( VngGE::kCurrentVersionNumber ) ) != Acad::eOk )
//        return ( es ) ;
//
//    return ( pFiler->filerStatus () ) ;
//}
//
//Acad::ErrorStatus VngGE::dwgInFields ( AcDbDwgFiler* pFiler )
//{
//    assertWriteEnabled () ;
//    //----- Read parent class information first.
//    Acad::ErrorStatus es = AcDbEntity::dwgInFields ( pFiler ) ;
//    if ( es != Acad::eOk )
//        return ( es ) ;
//    //----- Object version number needs to be read first
//    Adesk::UInt32 version = 0 ;
//    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
//        return ( es ) ;
//    if ( version > VngGE::kCurrentVersionNumber )
//        return ( Acad::eMakeMeProxy ) ;
//
//	return ( pFiler->filerStatus () ) ;
//}