#include "StdAfx.h"

#include "VngNode.h"
#include "DrawTool.h"

Adesk::UInt32 VngNode::kCurrentVersionNumber = 1 ;

ACRX_DXF_DEFINE_MEMBERS (
    VngNode, VngGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    网络-节点, VNGGENAPP
)

VngNode::VngNode () : VngGE ()
{
}

VngNode::~VngNode ()
{
}

Acad::ErrorStatus VngNode::dwgOutFields ( AcDbDwgFiler* pFiler ) const
{
    assertReadEnabled () ;
    //----- Save parent class information first.
    Acad::ErrorStatus es = VngGE::dwgOutFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    //----- Object version number needs to be saved first
    if ( ( es = pFiler->writeUInt32 ( VngNode::kCurrentVersionNumber ) ) != Acad::eOk )
        return ( es ) ;

    // 保存节点数据
    pFiler->writePoint3d( m_pt );
    pFiler->writeDouble( m_width );
    pFiler->writeDouble( m_height );
    pFiler->writeDouble( m_textHeight );

    return ( pFiler->filerStatus () ) ;
}

Acad::ErrorStatus VngNode::dwgInFields ( AcDbDwgFiler* pFiler )
{
    assertWriteEnabled () ;
    //----- Read parent class information first.
    Acad::ErrorStatus es = VngGE::dwgInFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    //----- Object version number needs to be read first
    Adesk::UInt32 version = 0 ;
    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
        return ( es ) ;
    if ( version > VngNode::kCurrentVersionNumber )
        return ( Acad::eMakeMeProxy ) ;

    // 读取节点数据
    pFiler->readPoint3d( &m_pt );
    pFiler->readDouble( &m_width );
    pFiler->readDouble( &m_height );
    pFiler->readDouble( &m_textHeight );

    return ( pFiler->filerStatus () ) ;
}

Adesk::Boolean VngNode::subWorldDraw ( AcGiWorldDraw* mode )
{
    assertReadEnabled();

    DrawEllipse( mode, m_pt, m_width, m_height, false );

    CString str;
    str.Format( _T( "v%s" ), MakeLowerText( m_name ) );
    DrawMText( mode, m_pt, 0, str, m_textHeight );

    return Adesk::kTrue;
    //return (VngGE::subWorldDraw (mode));
}

Acad::ErrorStatus VngNode::subTransformBy( const AcGeMatrix3d& xform )
{
    m_pt.transformBy( xform );

    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.transformBy( xform );

    m_width = v.length() * m_width;
    m_height = v.length() * m_height;
    m_textHeight = v.length() * m_textHeight;

    return Acad::eOk;
}

Acad::ErrorStatus VngNode::subGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    int gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;

    if( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_pt );
    }
    return Acad::eOk;
}

Acad::ErrorStatus VngNode::subGetGripPoints(
    AcGePoint3dArray& gripPoints,
    AcDbIntArray& osnapModes,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;

    gripPoints.append( m_pt );

    return Acad::eOk;
}

Acad::ErrorStatus VngNode::subMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;

    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );

        if ( idx == 0 )
        {
            m_pt += offset;
        }
    }
    return Acad::eOk;
}

void VngNode::setParam( double m_nodeWidth, double m_nodeHeight, double m_nodeTextHeight )
{
    assertWriteEnabled () ;
    this->m_width = m_nodeWidth;
    this->m_height = m_nodeHeight;
    this->m_textHeight = m_nodeTextHeight;
}

void VngNode::setName( const CString& name )
{
	assertWriteEnabled () ;
	if( name.GetLength() == 0 )
	{
		this->m_name = _T( "NULL" );
	}
	else
	{
		if(name.GetAt(0) == _T('v') || name.GetAt(0) == _T('V') )
		{
			this->m_name = name.Mid(1);
		}
		else
		{
			this->m_name = name;
		}
	}
}