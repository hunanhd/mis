﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Windows;

using AcadApplication = Autodesk.AutoCAD.ApplicationServices.Application;
using AcadDocument = Autodesk.AutoCAD.ApplicationServices.Document;
using AcadWindows = Autodesk.AutoCAD.Windows;

// 所有c#封装类都使用了M前缀
using arx;

[assembly:
    ExtensionApplication(typeof(MisFan.MyInitialization))
]
[assembly:
    CommandClass(typeof(MisFan.MyCommands))
]

namespace MisFan
{
    public class MyInitialization : Autodesk.AutoCAD.Runtime.IExtensionApplication
    {
        public void Initialize()
        {
        }
        public void Terminate()
        {
        }
    }
    public class MyCommands
    {
        [CommandMethod("xx")]
        public static void ArxRandom()
        {
        }
    }
}
