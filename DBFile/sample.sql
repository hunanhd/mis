--
-- File generated with SQLiteStudio v3.0.6 on 周日 一月 22 21:43:36 2017
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: FieldInfo
CREATE TABLE FieldInfo (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, func NVARCHAR (100), getype NVARCHAR (100), field NVARCHAR (100), fieldtype NVARCHAR (20) DEFAULT (0), defaultvalue NVARCHAR (20), tole NVARCHAR (20) DEFAULT (2), flag NVARCHAR (20) DEFAULT (3), tooltips NVARCHAR (20));
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (39, '常用', 'WindDirection', '三区', 'DT_LIST', 'LT_INT,三区', '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (97, '几何参数', 'AirSource', '坐标X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (98, '几何参数', 'AirSource', '坐标Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (99, '几何参数', 'AirSource', '坐标Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (100, '常用', 'AirSource', '风量', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (101, '常用', 'AirSource', '风压', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：Pa');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (107, '常用', 'MainTube', '名称', 'DT_STRING', NULL, '2', '3', '永久抽采管路名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (108, '常用', 'MainTube', '长度', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (109, '常用', 'MainTube', '管径', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：mm');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (110, '常用', 'MainTube', '瓦斯浓度', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：%');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (111, '常用', 'MainTube', '流量', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (112, '常用', 'BranchTube', '名称', 'DT_STRING', NULL, '2', '3', '移动泵排瓦斯管路名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (113, '常用', 'BranchTube', '长度', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (114, '常用', 'BranchTube', '管径', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：mm');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (115, '常用', 'BranchTube', '瓦斯浓度', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：%');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (116, '常用', 'BranchTube', '流量', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (117, '常用', 'TrunkTube', '名称', 'DT_STRING', NULL, '2', '3', '移动泵抽瓦斯管路名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (118, '常用', 'TrunkTube', '长度', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (119, '常用', 'TrunkTube', '管径', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：mm');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (120, '常用', 'TrunkTube', '瓦斯浓度', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：%');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (121, '常用', 'TrunkTube', '流量', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (122, '几何参数', 'DrillGE', '开孔X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '开孔坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (123, '几何参数', 'DrillGE', '开孔Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '开孔点坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (124, '几何参数', 'DrillGE', '开孔Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '开孔点坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (125, '几何参数', 'DrillGE', '终孔X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '终孔坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (126, '几何参数', 'DrillGE', '终孔Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '终孔点坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (127, '几何参数', 'DrillGE', '终孔Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '终孔点坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (128, '常用', 'DrillGE', '名称', 'DT_STRING', NULL, '2', '3', '钻孔名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (129, '几何参数', 'GasPump', '坐标X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (130, '几何参数', 'GasPump', '坐标Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (131, '几何参数', 'GasPump', '坐标Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (132, '常用', 'GasPump', '型号', 'DT_STRING', NULL, '2', '3', '永久抽采管路名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (133, '常用', 'GasPump', '负压', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：Pa');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (134, '常用', 'GasPump', '流量', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (135, '常用', 'GasPump', '功率', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：Kw');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (136, '几何参数', 'Tailrace', '坐标X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (137, '几何参数', 'Tailrace', '坐标Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (138, '几何参数', 'Tailrace', '坐标Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (139, '几何参数', 'Valve', '坐标X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (140, '几何参数', 'Valve', '坐标Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (141, '几何参数', 'Valve', '坐标Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (142, '几何参数', 'Flowmeter', '坐标X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (143, '几何参数', 'Flowmeter', '坐标Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (144, '几何参数', 'Flowmeter', '坐标Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (145, '几何参数', 'BackFire', '坐标X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (146, '几何参数', 'BackFire', '坐标Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (147, '几何参数', 'BackFire', '坐标Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (148, '几何参数', 'DetermineHole', '坐标X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (149, '几何参数', 'DetermineHole', '坐标Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (150, '几何参数', 'DetermineHole', '坐标Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (151, '常用', 'Inclined', '井口名称', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (152, '常用', 'Inclined', '井口标高', 'DT_NUMERIC', '-10000,10000', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (153, '常用', 'Inclined', '井筒倾角', 'DT_NUMERIC', '-10000,10000', '2', '3', '单位：°');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (154, '常用', 'Inclined', '名称', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (155, '常用', 'Shaft', '井口高程', 'DT_NUMERIC', '-10000,10000', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (156, '常用', 'Shaft', '井底高程', 'DT_NUMERIC', '-10000,10000', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (157, '常用', 'Shaft', '用途', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (158, '常用', 'Shaft', '孔号', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (159, '常用', 'Coalhole', '孔口高程', 'DT_NUMERIC', '-10000.00,10000.00', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (160, '常用', 'Coalhole', '底板高程', 'DT_NUMERIC', '-10000.00,10000.00', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (161, '常用', 'Coalhole', '厚度', 'DT_NUMERIC', '0,1000', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (162, '常用', 'Coalhole', '孔号', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (163, '常用', 'NoCoalhole', '孔口高程', 'DT_NUMERIC', '-10000.00,10000.00', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (164, '常用', 'NoCoalhole', '孔号', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (165, '常用', 'WaterDrill', '平距', 'DT_NUMERIC', '-10000,10000', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (166, '常用', 'WaterDrill', '垂距', 'DT_NUMERIC', '-10000,10000', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (167, '常用', 'WaterDrill', '泄水量', 'DT_NUMERIC', '0,1000', '2', '3', '单位：m3');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (168, '常用', 'WaterDrill', '孔号', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (169, '常用', 'LeakDrill', '孔口高程', 'DT_NUMERIC', '-10000.00,10000.00', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (170, '常用', 'LeakDrill', '底板高程', 'DT_NUMERIC', '-10000.00,10000.00', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (171, '常用', 'LeakDrill', '水位高程', 'DT_NUMERIC', '-10000.00,10001.00', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (172, '常用', 'LeakDrill', '水柱高度', 'DT_NUMERIC', '0.00,1000.00', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (173, '常用', 'LeakDrill', '漏失深度', 'DT_NUMERIC', '0,1000', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (174, '常用', 'LeakDrill', '漏失量', 'DT_NUMERIC', '0,1000', '2', '3', '单位：L/min');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (175, '常用', 'LeakDrill', '孔号', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (176, '常用', 'WaterPumpDrill', '孔口高程', 'DT_NUMERIC', '-10000.00,10000.00', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (177, '常用', 'WaterPumpDrill', '底板高程', 'DT_NUMERIC', '-10000.00,10000.00', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (178, '常用', 'WaterPumpDrill', '水位高程', 'DT_NUMERIC', '-10000.00,10001.00', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (179, '常用', 'WaterPumpDrill', '水柱高度', 'DT_NUMERIC', '0.00,1000.00', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (180, '常用', 'WaterPumpDrill', '单位涌水量', 'DT_NUMERIC', '0.00,1000.00', '2', '3', '单位：L/sm');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (181, '常用', 'WaterPumpDrill', '渗透系数', 'DT_NUMERIC', '0.00,1000.00', '2', '3', '单位：m/d');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (182, '常用', 'WaterPumpDrill', '是否见煤', 'DT_BOOL', '1', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (183, '常用', 'TempeSensor', '温度', 'DT_NUMERIC', '0,9999999999', '2', '3', '单位：℃');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (184, '常用', 'PressSensor', '压力', 'DT_NUMERIC', '0,9999999999', '3', '3', '单位：Pa');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (185, '常用', 'DifferPressSensor', '压差', 'DT_NUMERIC', '0,9999999999', '4', '3', '单位：Pa');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (186, '常用', 'CH4Sensor', '瓦斯浓度', 'DT_NUMERIC', '0,9999999999', '5', '3', '单位：%');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (187, '常用', 'COSensor', 'CO浓度', 'DT_NUMERIC', '0,9999999999', '6', '3', '单位：%');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (188, '常用', 'VentSensor', '风速', 'DT_NUMERIC', '0,9999999999', '7', '3', '单位：m/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (189, '常用', 'FlowSensor', '流量', 'DT_NUMERIC', '0,9999999999', '8', '3', '单位：m3/min');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (190, '常用', 'CDH', '地面标高', 'DT_NUMERIC', '-10000,10000', '1', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (191, '常用', 'CDH', '煤层底板标高', 'DT_NUMERIC', '-10000,10000', '1', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (192, '常用', 'CDH', '煤厚', 'DT_NUMERIC', '-10000,10000', '1', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (204, '几何参数', 'AirIn', '坐标X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (205, '几何参数', 'AirIn', '坐标Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (206, '几何参数', 'AirIn', '坐标Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (207, '常用', 'AirIn', '风量', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (208, '常用', 'AirIn', '风压', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：Pa');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (236, '常用', 'Joint', '名称', 'DT_STRING', NULL, '0', '3', '节点名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (237, '常用', 'Joint', '标高', 'DT_NUMERIC', '-10000.00,10000.00', '2', '3', '节点对应标准高程，单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (238, '几何参数', 'Joint', '坐标', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (239, '几何参数', 'Joint', '半径', 'DT_NUMERIC', '0.00,99.90', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (240, '通风', 'Tunnel', '始节点ID', 'DT_INT', '0,10000', '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (241, '通风', 'Tunnel', '末节点ID', 'DT_INT', '0,10000', '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (242, '常用', 'Tunnel', '名称', 'DT_STRING', NULL, '0', '3', '巷道名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (243, '常用', 'Tunnel', '简称', 'DT_STRING', NULL, '0', '3', '巷道的简称或习惯称呼');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (244, '常用', 'Tunnel', '用途', 'DT_STRING', NULL, '0', '3', '该巷道的主要用途');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (245, '常用', 'Tunnel', '风量', 'DT_NUMERIC', '-10000.00,10000.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (246, '通风', 'Tunnel', '风阻', 'DT_NUMERIC', '0.00,10000.00', '4', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (247, '通风', 'Tunnel', '调节风阻', 'DT_NUMERIC', '0.00,10000.00', '4', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (248, '通风', 'Tunnel', '解算风量', 'DT_NUMERIC', '-10000.00,10000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (249, '通风', 'Tunnel', '固定风量', 'DT_NUMERIC', '-10000.00,10000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (250, '常用', 'Tunnel', '备注', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (251, '几何参数', 'MainFan', '巷道ID', 'DT_INT', '0,999999999', '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (252, '几何参数', 'MainFan', '坐标X', 'DT_NUMERIC', '-1000000.00,1000000.00', '2', '3', '插入点坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (253, '几何参数', 'MainFan', '坐标Y', 'DT_NUMERIC', '-1000000.00,1000000.00', '2', '3', '插入点坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (254, '几何参数', 'MainFan', '坐标Z', 'DT_NUMERIC', '-1000000.00,1000000.00', '2', '3', '插入点坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (255, '常用', 'MainFan', '名称', 'DT_STRING', NULL, '0', '3', '主扇名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (256, '常用', 'MainFan', '风量', 'DT_NUMERIC', '-10000.00,10000.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (257, '常用', 'MainFan', '风压', 'DT_NUMERIC', '0.00,1000000.00', '2', '3', '单位：Pa');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (258, '通风', 'MainFan', 'a0', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (259, '通风', 'MainFan', 'a1', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (260, '通风', 'MainFan', 'a2', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (261, '通风', 'MainFan', 'a3', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (262, '通风', 'MainFan', 'a4', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (263, '通风', 'MainFan', 'a5', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (264, '常用', 'MainFan', '备注', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (271, '几何参数', 'LocalFan', '巷道ID', 'DT_INT', '0,999999999', '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (272, '几何参数', 'LocalFan', '坐标X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (273, '几何参数', 'LocalFan', '坐标Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (274, '几何参数', 'LocalFan', '坐标Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '插入点坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (275, '常用', 'LocalFan', '名称', 'DT_STRING', NULL, '0', '3', '局扇名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (276, '常用', 'LocalFan', '功率', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：Kw');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (277, '通风', 'LocalFan', 'a0', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (278, '通风', 'LocalFan', 'a1', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (279, '通风', 'LocalFan', 'a2', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (280, '通风', 'LocalFan', 'a3', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (281, '通风', 'LocalFan', 'a4', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (282, '通风', 'LocalFan', 'a5', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (283, '常用', 'LocalFan', '备注', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (284, '几何参数', 'BalanceGate', '坐标X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (285, '几何参数', 'BalanceGate', '坐标Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (286, '几何参数', 'BalanceGate', '坐标Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (287, '常用', 'BalanceGate', '名称', 'DT_STRING', NULL, '0', '3', '平衡风门名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (288, '常用', 'BalanceGate', '风量', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (289, '常用', 'BalanceGate', '压差', 'DT_NUMERIC', '0.00,9999999999.01', '2', '2', '单位：Pa');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (290, '常用', 'BalanceGate', '面积', 'DT_NUMERIC', '0.00,9999999999.02', '2', '3', '单位：m2');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (291, '通风', 'BalanceGate', '等效风阻', 'DT_NUMERIC', '0.00,100000.00', '4', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (292, '常用', 'BalanceGate', '备注', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (293, '几何参数', 'DoubleGate', '坐标X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (294, '几何参数', 'DoubleGate', '坐标Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (295, '几何参数', 'DoubleGate', '坐标Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (296, '常用', 'DoubleGate', '名称', 'DT_STRING', NULL, '0', '3', '双向风门名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (297, '常用', 'DoubleGate', '风量', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (298, '常用', 'DoubleGate', '压差', 'DT_NUMERIC', '0.00,9999999999.01', '2', '2', '单位：Pa');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (299, '常用', 'DoubleGate', '面积', 'DT_NUMERIC', '0.00,9999999999.02', '2', '3', '单位：m2');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (300, '通风', 'DoubleGate', '等效风阻', 'DT_NUMERIC', '0.00,100000.00', '4', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (301, '常用', 'DoubleGate', '备注', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (302, '几何参数', 'PermanentGate', '坐标X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (303, '几何参数', 'PermanentGate', '坐标Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (304, '几何参数', 'PermanentGate', '坐标Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (305, '常用', 'PermanentGate', '名称', 'DT_STRING', NULL, '0', '3', '永久风门名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (306, '常用', 'PermanentGate', '风量', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (307, '常用', 'PermanentGate', '压差', 'DT_NUMERIC', '0.00,9999999999.01', '2', '2', '单位：Pa');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (308, '常用', 'PermanentGate', '面积', 'DT_NUMERIC', '0.00,9999999999.02', '2', '3', '单位：m2');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (309, '通风', 'PermanentGate', '等效风阻', 'DT_NUMERIC', '0.00,100000.00', '4', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (310, '常用', 'PermanentGate', '备注', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (311, '几何参数', 'TemporaryGate', '坐标X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (312, '几何参数', 'TemporaryGate', '坐标Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (313, '几何参数', 'TemporaryGate', '坐标Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (314, '常用', 'TemporaryGate', '名称', 'DT_STRING', NULL, '0', '3', '临时风门名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (315, '常用', 'TemporaryGate', '风量', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (316, '常用', 'TemporaryGate', '压差', 'DT_NUMERIC', '0.00,9999999999.01', '2', '2', '单位：Pa');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (317, '常用', 'TemporaryGate', '面积', 'DT_NUMERIC', '0.00,9999999999.02', '2', '3', '单位：m2');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (318, '通风', 'TemporaryGate', '等效风阻', 'DT_NUMERIC', '0.00,100000.00', '4', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (319, '常用', 'TemporaryGate', '备注', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (320, '几何参数', 'PermanentCasement', '坐标X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (321, '几何参数', 'PermanentCasement', '坐标Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (322, '几何参数', 'PermanentCasement', '坐标Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (323, '常用', 'PermanentCasement', '名称', 'DT_STRING', NULL, '0', '3', '永久调节风窗名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (324, '常用', 'PermanentCasement', '风量', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (325, '常用', 'PermanentCasement', '压差', 'DT_NUMERIC', '0.00,9999999999.01', '2', '2', '单位：Pa');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (326, '常用', 'PermanentCasement', '面积', 'DT_NUMERIC', '0.00,9999999999.02', '2', '3', '单位：m2');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (327, '通风', 'PermanentCasement', '等效风阻', 'DT_NUMERIC', '0.00,100000.00', '4', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (328, '常用', 'PermanentCasement', '备注', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (329, '几何参数', 'TemporaryCasement', '坐标X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (330, '几何参数', 'TemporaryCasement', '坐标Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (331, '几何参数', 'TemporaryCasement', '坐标Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (332, '常用', 'TemporaryCasement', '名称', 'DT_STRING', NULL, '0', '3', '临时调节风窗名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (333, '常用', 'TemporaryCasement', '风量', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (334, '常用', 'TemporaryCasement', '压差', 'DT_NUMERIC', '0.00,9999999999.01', '2', '2', '单位：Pa');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (335, '常用', 'TemporaryCasement', '面积', 'DT_NUMERIC', '0.00,9999999999.02', '2', '3', '单位：m2');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (336, '通风', 'TemporaryCasement', '等效风阻', 'DT_NUMERIC', '0.00,100000.00', '4', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (337, '常用', 'TemporaryCasement', '备注', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (338, '几何参数', 'WallCasement', '坐标X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (339, '几何参数', 'WallCasement', '坐标Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (340, '几何参数', 'WallCasement', '坐标Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '位置坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (341, '常用', 'WallCasement', '名称', 'DT_STRING', NULL, '0', '3', '墙调节风窗名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (342, '常用', 'WallCasement', '风量', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (343, '常用', 'WallCasement', '压差', 'DT_NUMERIC', '0.00,9999999999.01', '2', '2', '单位：Pa');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (344, '常用', 'WallCasement', '面积', 'DT_NUMERIC', '0.00,9999999999.02', '2', '3', '单位：m2');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (345, '通风', 'WallCasement', '等效风阻', 'DT_NUMERIC', '0.00,100000.00', '4', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (346, '常用', 'WallCasement', '备注', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (347, '几何参数', 'WorkSurface', '入风端X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '入风端坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (348, '几何参数', 'WorkSurface', '入风端Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '入风端坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (349, '几何参数', 'WorkSurface', '入风端Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '入风端坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (350, '几何参数', 'WorkSurface', '回风端X', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '回风端坐标(x,y,z)中x所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (351, '几何参数', 'WorkSurface', '回风端Y', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '回风端坐标(x,y,z)中y所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (352, '几何参数', 'WorkSurface', '回风端Z', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '回风端坐标(x,y,z)中z所对应值');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (353, '常用', 'WorkSurface', '名称', 'DT_STRING', NULL, '0', '3', '回采面名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (354, '常用', 'WorkSurface', '风量', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (355, '通风', 'WorkSurface', '风阻', 'DT_NUMERIC', '0.00,100000.00', '4', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (356, '通风', 'WorkSurface', '调节风阻', 'DT_NUMERIC', '0.00,100000.00', '4', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (357, '通风', 'WorkSurface', '解算风量', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (358, '通风', 'WorkSurface', '固定风量', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (359, '常用', 'WorkSurface', '备注', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (371, '通风', 'ArcTunnel', '始节点ID', 'DT_INT', '0,999999999', '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (372, '通风', 'ArcTunnel', '末节点ID', 'DT_INT', '0,999999999', '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (373, '常用', 'ArcTunnel', '名称', 'DT_STRING', NULL, '0', '3', '巷道名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (374, '常用', 'ArcTunnel', '简称', 'DT_STRING', NULL, '0', '3', '巷道的简称或习惯称呼');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (375, '常用', 'ArcTunnel', '用途', 'DT_STRING', NULL, '0', '3', '该巷道的主要用途');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (376, '常用', 'ArcTunnel', '风量', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (377, '通风', 'ArcTunnel', '风阻', 'DT_NUMERIC', '0.00,100000.00', '4', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (378, '通风', 'ArcTunnel', '调节风阻', 'DT_NUMERIC', '0.00,100000.00', '4', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (379, '通风', 'ArcTunnel', '解算风量', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (380, '通风', 'ArcTunnel', '固定风量', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (381, '常用', 'ArcTunnel', '备注', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (382, '几何参数', 'Chimney', '巷道ID', 'DT_INT', '0,999999999', '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (383, '几何参数', 'Chimney', '掘进头ID', 'DT_INT', '0,999999999', '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (384, '常用', 'Chimney', '长度', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：m');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (385, '常用', 'Chimney', '直径', 'DT_NUMERIC', '0.00,9999999999.00', '2', '3', '单位：mm');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (386, '常用', 'Chimney', '风阻', 'DT_NUMERIC', '0.00,100000.00', '4', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (387, '常用', 'Chimney', '备注', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (388, '通风', 'TTunnel', '始节点ID', 'DT_INT', '0,10000', '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (389, '通风', 'TTunnel', '末节点ID', 'DT_INT', '0,10000', '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (390, '常用', 'TTunnel', '名称', 'DT_STRING', NULL, '0', '3', '掘进工作面名称（或编号）');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (391, '通风', 'TTunnel', '风量', 'DT_NUMERIC', '-100000.00,100000.00', '2', '3', '单位：m3/s');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (392, '通风', 'TTunnel', '风阻', 'DT_NUMERIC', '0.00,10000.00', '4', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (393, '掘进', 'TTunnel', '日进尺数', 'DT_NUMERIC', '0.00,10000.00', '2', '3', '单位：m/d');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (394, '掘进', 'TTunnel', '推进长度', 'DT_NUMERIC', '0.00,10000.01', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (395, '常用', 'TTunnel', '备注', 'DT_STRING', NULL, '0', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (396, '掘进', 'TTunnel', '更新日期', 'DT_DATE', NULL, '0', '3', '掘进面推进更新日期');
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (397, '通风', 'Tunnel', '调节风压', 'DT_NUMERIC', '-10000,10000', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (398, '通风', 'Tunnel', '空气密度', 'DT_NUMERIC', '0.8,1.5', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (399, '通风', 'Tunnel', '可调节性', 'DT_INT', '1,4', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (400, '通风', 'WorkSurface', '调节风压', 'DT_NUMERIC', '-10000,10000', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (401, '通风', 'WorkSurface', '空气密度', 'DT_NUMERIC', '0.8,1.5', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (402, '通风', 'WorkSurface', '可调节性', 'DT_INT', '1,4', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (403, '通风', 'ArcTunnel', '调节风压', 'DT_NUMERIC', '-10000,10000', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (404, '通风', 'ArcTunnel', '空气密度', 'DT_NUMERIC', '0.8,1.5', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (405, '通风', 'ArcTunnel', '可调节性', 'DT_INT', '1,4', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (406, '通风', 'Tunnel', 'ID', 'DT_INT', '0,10000', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (407, '通风', 'TTunnel', 'ID', 'DT_INT', '0,10000', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (408, '通风', 'WorkSurface', 'ID', 'DT_INT', '0,10000', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (409, '通风', 'WorkSurface', '始节点ID', 'DT_INT', '0,10000', '2', '3', NULL);
INSERT INTO FieldInfo (id, func, getype, field, fieldtype, defaultvalue, tole, flag, tooltips) VALUES (410, '通风', 'WorkSurface', '末节点ID', 'DT_INT', '0,10000', '2', '3', NULL);

-- Table: EntityData
CREATE TABLE EntityData (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, handle NVARCHAR (10), type NVARCHAR (20), geo_datas NTEXT, prop_datas NTEXT, comment NVARCHAR (50));

-- Table: IntStrList
CREATE TABLE [IntStrList] (
  [id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
  [field] NVARCHAR(20), 
  [intValue] INTEGER(20), 
  [strValue] NVARCHAR(20));
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (1, '通风方法', 0, '压入式');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (2, '通风方法', 1, '抽出式');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (3, '通风方法', 2, '长抽短压');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (4, '通风方法', 3, '长压短抽');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (5, '风筒材质', 0, '胶布风筒');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (6, '风筒材质', 1, '金属风筒');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (7, '通风方式', 0, '单机单风筒');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (8, '通风方式', 1, '辅助风筒');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (9, '通风方式', 2, '间断风筒');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (10, '通风方式', 3, '串联局扇');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (11, '通风方式', 4, '混合通风');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (12, '通风方式', 5, '风柜通风');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (13, '通风方式', 6, '风库通风');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (14, '通风方式', 7, '钻孔通风');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (15, '引爆方式', 0, '电雷管起爆');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (16, '引爆方式', 1, '火雷管起爆');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (17, '工作方式', 0, '压入式');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (18, '工作方式', 1, '抽出式');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (19, '三区', 0, '进风');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (20, '三区', 1, '回风');
INSERT INTO IntStrList (id, field, intValue, strValue) VALUES (21, '三区', 2, '用风');

-- Table: DataObjectList
CREATE TABLE [DataObjectList] (
  [id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
  [field] NVARCHAR(20), 
  [dataValue] NVARCHAR(20));
INSERT INTO DataObjectList (id, field, dataValue) VALUES (1, '安全检测仪表', '名称');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (2, '安全检测仪表', '型号');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (3, '安全检测仪表', '数量');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (4, '安全检测仪表', '生产单位');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (5, '安全检测仪表', '计量检验单位');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (6, '安全检测仪表', '计量检验结果');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (8, '矿井信息', '名称');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (9, '矿井信息', '隶属集团公司');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (10, '矿井信息', '设计日期');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (11, '矿井信息', '投产日期');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (12, '矿井信息', '年产量');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (13, '矿井信息', '总进风量');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (14, '矿井信息', '瓦斯等级');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (15, '矿井信息', '通风方式');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (16, '矿井信息', '通风系数');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (17, '矿井信息', '通风需风系数');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (19, '评价项目信息', '评价依据');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (20, '评价项目信息', '制定评价方案开始时间');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (21, '评价项目信息', '制定评价方案结束时间');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (22, '评价项目信息', '收集评价资料开始时间');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (23, '评价项目信息', '收集评价资料结束时间');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (24, '评价项目信息', '签订评价合同开始时间');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (25, '评价项目信息', '签订评价合同结束时间');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (26, '评价项目信息', '评价宣传贯彻开始时间');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (27, '评价项目信息', '评价宣传贯彻结束时间');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (28, '评价项目信息', '开展评价工作开始时间');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (29, '评价项目信息', '开展评价工作结束时间');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (30, '评价项目信息', '编制评价报告开始时间');
INSERT INTO DataObjectList (id, field, dataValue) VALUES (31, '评价项目信息', '编制评价报告结束时间');

-- Table: LayerData
CREATE TABLE LayerData (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name NVARCHAR (20) NOT NULL, geo_datas NTEXT, comment NVARCHAR (50));

-- Table: GETrans
CREATE TABLE [GETrans] (
  [id] INTEGER NOT NULL, 
  [getype] NVARCHAR(100), 
  [dxfname] NVARCHAR(100));
INSERT INTO GETrans (id, getype, dxfname) VALUES (1, 'Joint', '节点');
INSERT INTO GETrans (id, getype, dxfname) VALUES (2, 'Tunnel', '巷道');
INSERT INTO GETrans (id, getype, dxfname) VALUES (3, 'ArcTunnel', '弧线巷道');
INSERT INTO GETrans (id, getype, dxfname) VALUES (4, 'MainFan', '主扇');
INSERT INTO GETrans (id, getype, dxfname) VALUES (5, 'LocalFan', '局扇');
INSERT INTO GETrans (id, getype, dxfname) VALUES (6, 'Chimney', '风筒');
INSERT INTO GETrans (id, getype, dxfname) VALUES (7, 'TTunnel', '掘进工作面');
INSERT INTO GETrans (id, getype, dxfname) VALUES (8, 'WindDirection', '风流方向');
INSERT INTO GETrans (id, getype, dxfname) VALUES (9, 'WorkSurface', '回采工作面');
INSERT INTO GETrans (id, getype, dxfname) VALUES (10, 'WallCasement', '墙调节风窗');
INSERT INTO GETrans (id, getype, dxfname) VALUES (11, 'TemporaryCasement', '临时调节风窗');
INSERT INTO GETrans (id, getype, dxfname) VALUES (12, 'PermanentCasement', '永久调节风窗');
INSERT INTO GETrans (id, getype, dxfname) VALUES (13, 'PermanentGate', '永久风门');
INSERT INTO GETrans (id, getype, dxfname) VALUES (14, 'TemporaryGate', '临时风门');
INSERT INTO GETrans (id, getype, dxfname) VALUES (15, 'BalanceGate', '平衡风门');
INSERT INTO GETrans (id, getype, dxfname) VALUES (16, 'DoubleGate', '双向风门');
INSERT INTO GETrans (id, getype, dxfname) VALUES (17, 'AirSource', '漏风源');
INSERT INTO GETrans (id, getype, dxfname) VALUES (18, 'AirIn', '漏风汇');
INSERT INTO GETrans (id, getype, dxfname) VALUES (19, 'MainTube', '永久瓦斯抽放管路');
INSERT INTO GETrans (id, getype, dxfname) VALUES (20, 'BranchTube', '移动泵排瓦斯管路');
INSERT INTO GETrans (id, getype, dxfname) VALUES (21, 'TrunkTube', '移动泵抽瓦斯管路');
INSERT INTO GETrans (id, getype, dxfname) VALUES (22, 'DrillGE', '钻孔');
INSERT INTO GETrans (id, getype, dxfname) VALUES (23, 'GasPump', '瓦斯泵');
INSERT INTO GETrans (id, getype, dxfname) VALUES (24, 'Tailrace', '放水器');
INSERT INTO GETrans (id, getype, dxfname) VALUES (25, 'Valve', '阀门');
INSERT INTO GETrans (id, getype, dxfname) VALUES (26, 'Flowmeter', '流量计');
INSERT INTO GETrans (id, getype, dxfname) VALUES (27, 'BackFire', '阻火器');
INSERT INTO GETrans (id, getype, dxfname) VALUES (28, 'DetermineHole', '测定孔');
INSERT INTO GETrans (id, getype, dxfname) VALUES (29, 'Inclined', '斜井');
INSERT INTO GETrans (id, getype, dxfname) VALUES (30, 'Anemometer', '测风站');
INSERT INTO GETrans (id, getype, dxfname) VALUES (31, 'Firewall', '防火密闭');
INSERT INTO GETrans (id, getype, dxfname) VALUES (32, 'PermanentWall', '永久密闭');
INSERT INTO GETrans (id, getype, dxfname) VALUES (33, 'TemporaryWall', '临时密闭');
INSERT INTO GETrans (id, getype, dxfname) VALUES (34, 'WindBridge', '风桥');
INSERT INTO GETrans (id, getype, dxfname) VALUES (35, 'Shaft', '竖井');
INSERT INTO GETrans (id, getype, dxfname) VALUES (36, 'Coalhole', '见煤孔');
INSERT INTO GETrans (id, getype, dxfname) VALUES (37, 'NoCoalhole', '未见煤孔');
INSERT INTO GETrans (id, getype, dxfname) VALUES (38, 'WaterDrill', '井下探水孔');
INSERT INTO GETrans (id, getype, dxfname) VALUES (39, 'LeakDrill', '漏水钻孔');
INSERT INTO GETrans (id, getype, dxfname) VALUES (40, 'WaterPumpDrill', '抽水孔');
INSERT INTO GETrans (id, getype, dxfname) VALUES (41, 'HeatSource', '热源');
INSERT INTO GETrans (id, getype, dxfname) VALUES (42, 'MachineStation', '基站');
INSERT INTO GETrans (id, getype, dxfname) VALUES (43, 'AirCurtain', '空气幕');
INSERT INTO GETrans (id, getype, dxfname) VALUES (44, 'LocalResis', '局部阻力');
INSERT INTO GETrans (id, getype, dxfname) VALUES (45, 'AddPower', '附加动力');
INSERT INTO GETrans (id, getype, dxfname) VALUES (46, 'PitotTube', '皮托管');
INSERT INTO GETrans (id, getype, dxfname) VALUES (47, 'Hoses', '胶管');
INSERT INTO GETrans (id, getype, dxfname) VALUES (48, 'DiffPressGauge', '压差计');
INSERT INTO GETrans (id, getype, dxfname) VALUES (49, 'WaterInrushPt', '突水点');
INSERT INTO GETrans (id, getype, dxfname) VALUES (50, 'TempeSensor', '温度传感器');
INSERT INTO GETrans (id, getype, dxfname) VALUES (51, 'FlowSensor', '流量传感器');
INSERT INTO GETrans (id, getype, dxfname) VALUES (52, 'VentSensor', '风速传感器');
INSERT INTO GETrans (id, getype, dxfname) VALUES (53, 'PressSensor', '压力传感器');
INSERT INTO GETrans (id, getype, dxfname) VALUES (54, 'DifferPressSensor', '压差传感器');
INSERT INTO GETrans (id, getype, dxfname) VALUES (55, 'CH4Sensor', '瓦斯浓度传感器');
INSERT INTO GETrans (id, getype, dxfname) VALUES (56, 'COSensor', 'CO传感器');
INSERT INTO GETrans (id, getype, dxfname) VALUES (57, 'Sensor', '通用传感器');

-- Table: StrList
CREATE TABLE [StrList] (
  [id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
  [field] NVARCHAR(20), 
  [strValue] NVARCHAR(20));
INSERT INTO StrList (id, field, strValue) VALUES (1, '设计单位', '北京煤科院');
INSERT INTO StrList (id, field, strValue) VALUES (2, '设计单位', '重庆煤科院');
INSERT INTO StrList (id, field, strValue) VALUES (3, '设计单位', '沈阳煤科院');
INSERT INTO StrList (id, field, strValue) VALUES (4, '评价依据', '煤矿安全规程');
INSERT INTO StrList (id, field, strValue) VALUES (5, '评价依据', '瓦斯抽采达标暂行规定');

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
