#include "stdafx.h"
#include "FanVisualDlg.h"
// FanVisualDlg 对话框

IMPLEMENT_DYNAMIC(FanVisualDlg, CDialog)

FanVisualDlg::FanVisualDlg(CWnd* pParent /*=NULL*/)
: CDialog(FanVisualDlg::IDD, pParent)
{

}

FanVisualDlg::~FanVisualDlg()
{
}

void FanVisualDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FAN_TAB, m_fanTabCtrl);
	DDX_Control(pDX, IDC_APPLY_BUTTON, m_applyBtn);
}


BEGIN_MESSAGE_MAP(FanVisualDlg, CDialog)
	ON_NOTIFY(TCN_SELCHANGE, IDC_FAN_TAB, &FanVisualDlg::OnTcnSelchangeFanTab)
	ON_BN_CLICKED(IDOK, &FanVisualDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_APPLY_BUTTON, &FanVisualDlg::OnBnClickedApplyButton)
	ON_MESSAGE(WM_PRO_CHANGED, &FanVisualDlg::OnProChange)
END_MESSAGE_MAP()

BOOL FanVisualDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO: 在此添加额外的初始化代码
	m_fanTabCtrl.SetMinTabWidth(10);
	m_fanTabCtrl.InsertItem (0,_T("司机操作"));
	m_fanTabCtrl.InsertItem (1,_T("叶片角度参数"));
	m_fanTabCtrl.InsertItem (2,_T("测试数据处理"));
	m_fanTabCtrl.InsertItem (3,_T("特性曲线和性能分析"));
	m_driverDlg.Create(IDD_DRIVER_DIALOG,&m_fanTabCtrl);
	m_fanAngleDlg.Create(IDD_FAN_ANGLE_PRO_DIALOG,&m_fanTabCtrl);
	m_fanTestDataDlg.Create(IDD_FAN_TEST_DATAS_DIALOG,&m_fanTabCtrl);
	m_fanCurveDlg.Create(IDD_FAN_CURVE_DIALOG,&m_fanTabCtrl);
	CRect rc;
	m_fanTabCtrl.GetClientRect(&rc);
	rc.top += 20;
	rc.bottom -= 4;
	rc.left += 4;
	rc.right -= 4;
	m_driverDlg.MoveWindow(&rc);
	m_fanAngleDlg.MoveWindow(&rc);
	m_fanTestDataDlg.MoveWindow(&rc);
	m_fanCurveDlg.MoveWindow(&rc);
	m_driverDlg.ShowWindow(SW_SHOW);
	m_fanAngleDlg.ShowWindow(SW_HIDE);
	m_fanTestDataDlg.ShowWindow(SW_HIDE);
	m_fanCurveDlg.ShowWindow(SW_HIDE);
	if(m_fanAngleDlg.getFilesState() || m_fanTestDataDlg.getFilesState())
	{
		MessageBox(_T("必要文件丢失!"));
		CDialog::OnCancel();
	}
	//m_fanTabCtrl.SetCurFocus(0);

	initDriverDlg();

	return TRUE;
}

// FanVisualDlg 消息处理程序

void FanVisualDlg::OnTcnSelchangeFanTab(NMHDR *pNMHDR, LRESULT *pResult)
{
	CRect rc;
	m_fanTabCtrl.GetClientRect(&rc);
	switch(m_fanTabCtrl.GetCurSel())
	{
	case 0:
		m_driverDlg.ShowWindow(SW_SHOW);
		m_fanAngleDlg.ShowWindow(SW_HIDE);
		m_fanTestDataDlg.ShowWindow(SW_HIDE);
		m_fanCurveDlg.ShowWindow(SW_HIDE);
		modifyApplyBtnText(TRUE);
		break;
	case 1:
		m_driverDlg.ShowWindow(SW_HIDE);
		m_fanAngleDlg.ShowWindow(SW_SHOW);
		m_fanTestDataDlg.ShowWindow(SW_HIDE);
		m_fanCurveDlg.ShowWindow(SW_HIDE);
		modifyApplyBtnText(TRUE);
		break;
	case 2:
		m_driverDlg.ShowWindow(SW_HIDE);
		m_fanAngleDlg.ShowWindow(SW_HIDE);
		m_fanTestDataDlg.ShowWindow(SW_SHOW);
		m_fanCurveDlg.ShowWindow(SW_HIDE);
		modifyApplyBtnText(TRUE);
		break;
	case 3:
		m_driverDlg.ShowWindow(SW_HIDE);
		m_fanAngleDlg.ShowWindow(SW_HIDE);
		m_fanTestDataDlg.ShowWindow(SW_HIDE);
		m_fanCurveDlg.ShowWindow(SW_SHOW);
		modifyApplyBtnText(FALSE);
		m_fanCurveDlg.readDatas();
		break;
	default:
		break;
	}
	*pResult = 0;
}

void FanVisualDlg::SetEntity(const AcDbObjectId& objId)
{
	m_fanId = objId;
	m_driverDlg.SetEntity(m_fanId);
}

void FanVisualDlg::initDriverDlg()
{
	CString type;
	if( !ArxDataTool::GetTypeName( m_fanId, type ) ) return;
	AcStringArray fields;
	FieldHelper::GetAllFields( type, fields );
	if( !fields.isEmpty() )
	{
		int len = fields.length();
		for( int i = 0; i < len; i++ )
		{
			m_driverDlg.AddField( fields[i].kACharPtr() );
		}
	}
}

void FanVisualDlg::OnBnClickedOk()
{
	if(m_applyBtn.IsWindowEnabled())
	{
		saveDatas();
	}
	OnOK();
}

void FanVisualDlg::lightApplyBtn(BOOL bLight)
{
	m_applyBtn.EnableWindow(bLight);
}

void FanVisualDlg::OnBnClickedApplyButton()
{
	saveDatas();
}

LRESULT FanVisualDlg::OnProChange(WPARAM wParam, LPARAM lParam)
{
	bool light = m_driverDlg.getModifyStates() || m_fanAngleDlg.getModifyStates()
		|| m_fanTestDataDlg.getModifyStates() || m_fanCurveDlg.getModifyStates();
	this->lightApplyBtn(light);
	return 0;
}

void FanVisualDlg::saveDatas()
{
	if(m_driverDlg.getModifyStates())
	{
		m_driverDlg.save();
		m_driverDlg.setModifyStates(false);
	}
	if(m_fanAngleDlg.getModifyStates())
	{
		m_fanAngleDlg.save();
		m_fanAngleDlg.setModifyStates(false);
	}
	if(m_fanTestDataDlg.getModifyStates())
	{
		m_fanTestDataDlg.save();
		m_fanTestDataDlg.setModifyStates(false);
		m_fanTestDataDlg.activeButton(FALSE);
	}
	if(m_fanCurveDlg.getModifyStates())
	{
		m_fanCurveDlg.savePicture();
		m_fanCurveDlg.setModifyStates(false);
	}
	lightApplyBtn(!m_applyBtn.IsWindowEnabled());
}

void FanVisualDlg::modifyApplyBtnText(BOOL bApply /*= TRUE*/)
{
	if(bApply)
		m_applyBtn.SetWindowText(_T("应用"));
	else
		m_applyBtn.SetWindowText(_T("保存图形"));

}

