#pragma once
#include "resource.h"
#include "AcadDialog.h"
//#include "afxcmn.h"
#include "CGridListCtrlEx.h"
#include <vector>

typedef std::vector<AcStringArray> ArrayVector;

class ToploAnasDlg : public CDialog
{
	DECLARE_DYNAMIC(ToploAnasDlg)
public:
	ToploAnasDlg(CWnd *pParent =NULL);   // 标准构造函数
	virtual ~ToploAnasDlg();
	virtual BOOL OnInitDialog();
	// 对话框数据
	enum { IDD = IDD_TOPLO_ANAS_DLG };
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	afx_msg void OnDestroy();
	afx_msg LRESULT onAcadKeepFocus(WPARAM wParam,LPARAM lParam);
	afx_msg void OnNMDblclkTbList(NMHDR *pNMHDR, LRESULT *pResult);
	DECLARE_MESSAGE_MAP()

private:
	void initListCtrl();
	void setIterms();

public:
	ArrayVector m_listDatasVector;

private:
	CGridListCtrlEx m_tbListCtrl;
};
