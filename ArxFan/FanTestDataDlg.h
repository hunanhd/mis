#pragma once
#include "resource.h"
#include "TabCtrlBaseDlg.h"
#include "afxcmn.h"
#include "CGridListCtrlEx.h"
#include "afxwin.h"

class FanTestDataDlg : public TabCtrlBaseDlg
{
	DECLARE_DYNAMIC(FanTestDataDlg)

public:
	FanTestDataDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~FanTestDataDlg();
	virtual BOOL OnInitDialog();
	void setDatas(const ArrayVector& datas);
	void save();
	void activeButton(BOOL bActive = TRUE);
	void getListColDatas(int colIndex, vector<double>& datas);
// 对话框数据
	enum { IDD = IDD_FAN_TEST_DATAS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	afx_msg void OnBnClickedLastDatasButton();
	afx_msg void OnBnClickedClearAllButton();
	afx_msg void OnBnClickedSaveTestDatasButton();
	afx_msg void OnBnClickedFitButton();
	afx_msg void OnDeltaposNumSpin(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposHqSpin(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposNqSpin3(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposEqSpin2(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeleteItem();
	afx_msg void OnAddItem();
	afx_msg void OnNMRClickFanTestDatasList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillfocusDatasNumEdit();
	afx_msg void OnEnKillfocusHqEdit();
	afx_msg void OnEnKillfocusNqEdit();
	afx_msg void OnEnKillfocusEqEdit();
	afx_msg void OnLvnBeginlabeleditFanTestDatasList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnEndlabeleditFanTestDatasList(NMHDR *pNMHDR, LRESULT *pResult);

	DECLARE_MESSAGE_MAP()

private:
	void initListCtrl();
	void setIterms();
	void updateDatas();
	void updateList();
	void buildListCtrl();
	void saveListDatas();
	void readDatas();
	void fit(const vector<double>& v_x, const vector<double>& v_y, int n, const CString& printStr = _T(""));
private:
	CGridListCtrlEx m_testList;
	int m_num;
	int m_iHQ;
	int m_iNQ;
	int m_iEQ;
	CString m_print;
	ArrayVector m_listDatasVector;
	CButton m_saveBtn;
	CString m_oldText;
	CButton m_lastDataBtn;
	ArrayVector m_equations;
};
