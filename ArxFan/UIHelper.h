#pragma once
/*
 * 可视化界面辅助类
 * 放在一个类中便于管理
 * 也可使得代码看起来简洁一些
 */
enum ToploType
{
	VENTIN		= 0,
	VENTOUT		= 1,
	STRCUTRE	= 3,
	FANTUNNEL	= 4,
	CONNECT		= 5,
	ONEWAY		= 6,
	RESERVEDIR	= 7,
	LOOP		=8
};

class UIHelper
{
public:
	static void ShowToploAnasDlg(ToploType topType);
	static void DestroyToploAnasDlg();
	static void ShowFanVisualDlg();
};
