#include "StdAfx.h"
#include "resource.h"
#include "UIHelper.h"

#include "MineGE/HelperClass.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"

// 定义注册服务名称
#ifndef ARXFAN_SERVICE_NAME
#define ARXFAN_SERVICE_NAME _T("ARXFAN_SERVICE_NAME")
#endif
//-----------------------------------------------------------------------------
//----- ObjectARX EntryPoint
class CArxFanApp : public AcRxArxApp
{
public:
    CArxFanApp () : AcRxArxApp () {}
    virtual AcRx::AppRetCode On_kInitAppMsg ( void* pkt )
    {
        // You *must* call On_kInitAppMsg here
        AcRx::AppRetCode retCode = AcRxArxApp::On_kInitAppMsg ( pkt ) ;
        acrxRegisterAppMDIAware( pkt );
        // 注册服务
        acrxRegisterService( ARXFAN_SERVICE_NAME );
        return ( retCode ) ;
    }
    virtual AcRx::AppRetCode On_kUnloadAppMsg ( void* pkt )
    {
        // TODO: Add your code here
        // You *must* call On_kUnloadAppMsg here
        AcRx::AppRetCode retCode = AcRxArxApp::On_kUnloadAppMsg ( pkt ) ;
        // 删除服务
        delete acrxServiceDictionary->remove( ARXFAN_SERVICE_NAME );
        return ( retCode ) ;
    }
	virtual AcRx::AppRetCode On_kLoadDwgMsg( void* pkt )
	{
		AcRx::AppRetCode retCode = AcRxArxApp::On_kLoadDwgMsg ( pkt ) ;
		acutPrintf( _T( "\nArxFan::On_kLoadDwgMsg\n" ) );
		return retCode;
	}
	virtual AcRx::AppRetCode On_kUnloadDwgMsg( void* pkt )
	{
		AcRx::AppRetCode retCode = AcRxArxApp::On_kUnloadDwgMsg ( pkt ) ;
		acutPrintf( _T( "\nArxFan::On_kUnloadDwgMsg\n" ) );
		return retCode;
	}
    virtual void RegisterServerComponents ()
    {
    }

    //显示进风巷道
    static void JL_ShowVentInDlg( void )
    {
        UIHelper::ShowToploAnasDlg(VENTIN);
    }
    //显示出风口巷道
    static void JL_ShowVentOutDlg( void )
    {
        UIHelper::ShowToploAnasDlg(VENTOUT);
    }
    //显示构筑物巷道
    static void JL_ShowStructDlg( void )
    {
        UIHelper::ShowToploAnasDlg(STRCUTRE);
    }
    //显示通风动力巷道
    static void JL_ShowFantunnelDlg( void )
    {
        UIHelper::ShowToploAnasDlg(FANTUNNEL);
    }
    //图的连通性
    static void JL_ShowConnectDlg( void )
    {
        UIHelper::ShowToploAnasDlg(CONNECT);
    }
    //单向回路
    static void JL_ShowOnewayDlg( void )
    {
        UIHelper::ShowToploAnasDlg(ONEWAY);
    }
    //风流反向巷道
    static void JL_ShowReservedirDlg( void )
    {
        UIHelper::ShowToploAnasDlg(RESERVEDIR);
    }
    //自环检查
    static void JL_ShowLoopDlg( void )
    {
        UIHelper::ShowToploAnasDlg(LOOP);
    }

    //风机仿真对话框
    static void JL_FanVisual( void )
    {
        UIHelper::ShowFanVisualDlg();
    }
} ;
//-----------------------------------------------------------------------------
IMPLEMENT_ARX_ENTRYPOINT( CArxFanApp )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxFanApp, JL, _ShowVentInDlg, ShowVentInDlg, ACRX_CMD_TRANSPARENT | ACRX_CMD_USEPICKSET | ACRX_CMD_REDRAW, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxFanApp, JL, _ShowVentOutDlg, ShowVentOutDlg, ACRX_CMD_TRANSPARENT | ACRX_CMD_USEPICKSET | ACRX_CMD_REDRAW, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxFanApp, JL, _ShowStructDlg, ShowStructDlg, ACRX_CMD_TRANSPARENT | ACRX_CMD_USEPICKSET | ACRX_CMD_REDRAW, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxFanApp, JL, _ShowFantunnelDlg, ShowFantunnelDlg, ACRX_CMD_TRANSPARENT | ACRX_CMD_USEPICKSET | ACRX_CMD_REDRAW, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxFanApp, JL, _ShowConnectDlg, ShowConnectDlg, ACRX_CMD_TRANSPARENT | ACRX_CMD_USEPICKSET | ACRX_CMD_REDRAW, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxFanApp, JL, _ShowOnewayDlg, ShowOnewayDlg, ACRX_CMD_TRANSPARENT | ACRX_CMD_USEPICKSET | ACRX_CMD_REDRAW, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxFanApp, JL, _ShowReservedirDlg, ShowReservedirDlg, ACRX_CMD_TRANSPARENT | ACRX_CMD_USEPICKSET | ACRX_CMD_REDRAW, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxFanApp, JL, _ShowLoopDlg, ShowLoopDlg, ACRX_CMD_TRANSPARENT | ACRX_CMD_USEPICKSET | ACRX_CMD_REDRAW, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CArxFanApp, JL, _FanVisual, FanVisual, ACRX_CMD_TRANSPARENT, NULL )
