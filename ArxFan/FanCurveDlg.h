#pragma once
#include "resource.h"
#include "ChartCtrl.h"
#include "ChartLineSerie.h"
#include "ColourPicker.h"
#include "TabCtrlBaseDlg.h"
#include "afxwin.h"

struct LinePro
{
	int m_lineType;
	double m_lineWidth;
	CColourPicker m_lineColor;
};

class FanCurveDlg : public TabCtrlBaseDlg
{
	DECLARE_DYNAMIC(FanCurveDlg)

public:
	FanCurveDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~FanCurveDlg();
	void savePicture();
	bool readDatas();

// 对话框数据
	enum { IDD = IDD_FAN_CURVE_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedLengendvisCheck();
	afx_msg void OnBnClickedHqCheck();
	afx_msg void OnBnClickedRCheck();
	afx_msg void OnBnClickedNqCheck();
	afx_msg void OnBnClickedEqCheck();
	afx_msg void OnBnClickedAutoCheck();
	afx_msg void OnBnClickedLeftAxisRadio();
	afx_msg void OnBnClickedButtomAxisRadio();
	afx_msg void OnEnKillfocusMinValueEdit();
	afx_msg void OnEnKillfocusMaxValueEdit();
	afx_msg void OnLbnSelchangeLineProList();
	afx_msg LRESULT OnColorSelChange(WPARAM lParam, LPARAM wParam);
	afx_msg void OnEnKillfocusLineWidthEdit();
	afx_msg void OnCbnSelchangeLineTypeCombo();

	DECLARE_MESSAGE_MAP()

private:
	void creatHQLineSerie();
	void creatRLineSerie();
	void creatNQLineSerie();
	void creatEQLineSerie();
	void clearSerie(CChartSerie* serie);
	CChartAxis* GetSelectedAxis();
	bool setMinMaxValue();
	void enableMinMaxEditable(BOOL bEnable = TRUE);
	void initAxis();
	void initLineList();
	void setLinePro(const CString& lineName,CChartLineSerie* serie);
	void sendMessage();
	void enadbleDraw(BOOL bDraw = TRUE);

private:
	CChartCtrl m_chartCtrl;
	CChartLineSerie *m_HQLineSerie;
	CChartLineSerie *m_RLineSerie;
	CChartLineSerie *m_NQLineSerie;
	CChartLineSerie *m_EQLineSerie;
	LinePro m_HQLinePro;
	LinePro m_RLinePro;
	LinePro m_NQLinePro;
	LinePro m_EQLinePro;
	CButton m_legendVisBtn;
	CButton m_HQCheack;
	CButton m_RCheack;
	CButton m_NQCheack;
	CButton m_EQCheack;
	CListBox m_lineList;
	int m_lineType;
	double m_lineWidth;
	CColourPicker m_lineColor;
	CButton m_autoAxisBtn;
	double m_minValue;
	double m_maxValue;
	ArrayVector m_datas;
public:
	afx_msg void OnDeltaposSpin1(NMHDR *pNMHDR, LRESULT *pResult);
};
