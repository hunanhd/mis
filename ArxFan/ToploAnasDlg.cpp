#include "stdafx.h"
#include "ToploAnasDlg.h"
#include "CGridColumnTraitText.h"

IMPLEMENT_DYNAMIC( ToploAnasDlg, CDialog )
ToploAnasDlg::ToploAnasDlg( CWnd* pParent )
: CDialog( ToploAnasDlg::IDD, pParent )
{
}
ToploAnasDlg::~ToploAnasDlg()
{
}
void ToploAnasDlg::DoDataExchange( CDataExchange* pDX )
{
	CDialog::DoDataExchange( pDX );
	DDX_Control(pDX, IDC_TB_LIST, m_tbListCtrl);
}

// 消息处理程序
BEGIN_MESSAGE_MAP(ToploAnasDlg, CDialog)
	ON_MESSAGE(WM_ACAD_KEEPFOCUS, onAcadKeepFocus)
	ON_WM_DESTROY()
	ON_NOTIFY(NM_DBLCLK, IDC_TB_LIST, &ToploAnasDlg::OnNMDblclkTbList)
END_MESSAGE_MAP()

void ToploAnasDlg::setIterms()
{
	m_tbListCtrl.DeleteAllItems();
	int nItem = 0;
	for(size_t rowId = 1; rowId < m_listDatasVector.size() ; ++rowId)
	{
		m_tbListCtrl.InsertItem(nItem, m_listDatasVector[rowId][0]);
		m_tbListCtrl.SetItemData(nItem, rowId);
		for(int col = 0; col < m_listDatasVector[0].length() ; ++col)
		{
			int nCellCol = col+1;	// +1 because of hidden column
			const CString& strCellText = m_listDatasVector[rowId][col].kACharPtr();
			m_tbListCtrl.SetItemText(nItem, nCellCol, strCellText);
		}
		nItem++;
	}	

}

void ToploAnasDlg::initListCtrl()
{
	//m_tbListCtrl.SetExtendedStyle(m_tbListCtrl.GetExtendedStyle() |	LVS_EX_GRIDLINES );
	//m_tbListCtrl.SetCellMargin(1.0);
	////CGridRowTraitXP* pRowTrait = new CGridRowTraitXP;
	//CGridRowTrait* pRowTrait = new CGridRowTrait;
	////COLORREF textColor=RGB(0,0,0);
	////COLORREF bkColor=RGB(246,246,246);
	////pRowTrait->SetRowColor(textColor,bkColor);
	//m_tbListCtrl.SetDefaultRowTrait(pRowTrait);
	//m_tbListCtrl.EnableVisualStyles(true);

	CRect rect;
	m_tbListCtrl.GetClientRect(rect); //获得当前客户区信息  

	m_tbListCtrl.InsertHiddenLabelColumn();	// Requires one never uses column 0
	int colCount = m_listDatasVector[0].length();
	for(int col = 0; col <  colCount; ++col)
	{
		const CString& title = m_listDatasVector[0][col].kACharPtr();
		CGridColumnTraitText* pTrait = new CGridColumnTraitText;
		double colWidth = rect.Width()/colCount;
		if( colWidth < 50 ) colWidth = 50;
		m_tbListCtrl.InsertColumnTrait(col+1, title, LVCFMT_LEFT, colWidth, col, pTrait);
	}
	//CViewConfigSectionWinApp* pColumnProfile = new CViewConfigSectionWinApp(_T("Sample"));
	//m_tbListCtrl.SetupColumnConfig(pColumnProfile);
}

BOOL ToploAnasDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	if(m_listDatasVector.empty()) return TRUE;
	initListCtrl();
	setIterms();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void ToploAnasDlg::OnDestroy()
{
	CDialog::OnDestroy();
}

LRESULT ToploAnasDlg::onAcadKeepFocus(WPARAM wParam,LPARAM lParam)
{
	return TRUE;
}

void ToploAnasDlg::OnNMDblclkTbList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	int nIdx=m_tbListCtrl.GetNextItem(-1,LVIS_SELECTED);//选中的行的索引
	acutPrintf(_T("双击%s\n"),m_tbListCtrl.GetItemText(nIdx,0));
	*pResult = 0;
}
