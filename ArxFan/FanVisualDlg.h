#pragma once
#include "afxcmn.h"
#include "resource.h"
#include "DriverDlg.h"
#include "FanAngleProDlg.h"
#include "afxwin.h"
#include "FanTestDataDlg.h"
#include "FanCurveDlg.h"
// FanVisualDlg 对话框

class FanVisualDlg : public CDialog
{
	DECLARE_DYNAMIC(FanVisualDlg)

public:
	FanVisualDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~FanVisualDlg();

// 对话框数据
	enum { IDD = IDD_FAN_VISUAL_DIALOG };

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	afx_msg void OnTcnSelchangeFanTab(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedApplyButton();
	afx_msg LRESULT OnProChange(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

public:
	void SetEntity(const AcDbObjectId& objId);

private:
	void initDriverDlg();
	void lightApplyBtn(BOOL bLight = TRUE);
	void saveDatas();
	void modifyApplyBtnText(BOOL bApply = TRUE);

private:
	CTabCtrl m_fanTabCtrl;
	AcDbObjectId m_fanId;
	DriverDlg m_driverDlg;
	FanAngleProDlg m_fanAngleDlg;
	FanTestDataDlg m_fanTestDataDlg;
	FanCurveDlg m_fanCurveDlg;
	CButton m_applyBtn;

};
