#pragma once
#include "ArxHelper/HelperClass.h"

#define TEST_DATAS_FILE_NAME _T("FanTestDataDatas.txt")
#define ANGLE_DATAS_FILE_NAME _T("FanAngleDatas.txt")
typedef std::vector<AcStringArray> ArrayVector;


class TabCtrlBaseDlg : public CDialog
{
	DECLARE_DYNAMIC(TabCtrlBaseDlg)

public:
	TabCtrlBaseDlg(UINT IDD, CWnd* pParent = NULL);   // 标准构造函数
	virtual ~TabCtrlBaseDlg();
	virtual BOOL OnInitDialog();
	bool getModifyStates();
	void setModifyStates(bool bChanged);
	bool getFilesState();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	//afx_msg LRESULT OnPropertyChanged( WPARAM wParam,LPARAM lParam );
	DECLARE_MESSAGE_MAP()

	bool readDatasFromFile(const CString& fileName, ArrayVector& datas, int nCol);
	bool writeDatasToFile(const CString& fileName, const ArrayVector& datas);
protected:
	bool m_dataChanged;
	bool m_missFiels;
};
