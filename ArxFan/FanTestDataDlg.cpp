#include "stdafx.h"
#include "FanTestDataDlg.h"
#include "CGridColumnTraitText.h"
#include "CGridColumnTraitEdit.h"
#include "fit.h"

IMPLEMENT_DYNAMIC(FanTestDataDlg, TabCtrlBaseDlg)

FanTestDataDlg::FanTestDataDlg(CWnd* pParent /*=NULL*/)
	: TabCtrlBaseDlg(FanTestDataDlg::IDD, pParent)
	, m_num(0)
	, m_iHQ(2)
	, m_iNQ(2)
	, m_iEQ(2)
	, m_print(_T(""))
{
}

FanTestDataDlg::~FanTestDataDlg()
{
}

void FanTestDataDlg::DoDataExchange(CDataExchange* pDX)
{
	TabCtrlBaseDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FAN_TEST_DATAS_LIST, m_testList);
	DDX_Text(pDX, IDC_DATAS_NUM_EDIT, m_num);
	DDX_Text(pDX, IDC_HQ_EDIT, m_iHQ);
	DDX_Text(pDX, IDC_NQ_EDIT, m_iNQ);
	DDX_Text(pDX, IDC_EQ_EDIT, m_iEQ);
	DDX_Text(pDX, IDC_PRINT_EDIT, m_print);
	DDX_Control(pDX, IDC_SAVE_TEST_DATAS_BUTTON, m_saveBtn);
	DDX_Control(pDX, IDC_LAST_DATAS_BUTTON, m_lastDataBtn);
}


BEGIN_MESSAGE_MAP(FanTestDataDlg, TabCtrlBaseDlg)
	ON_BN_CLICKED(IDC_LAST_DATAS_BUTTON, &FanTestDataDlg::OnBnClickedLastDatasButton)
	ON_BN_CLICKED(IDC_CLEAR_ALL_BUTTON, &FanTestDataDlg::OnBnClickedClearAllButton)
	ON_BN_CLICKED(IDC_SAVE_TEST_DATAS_BUTTON, &FanTestDataDlg::OnBnClickedSaveTestDatasButton)
	ON_BN_CLICKED(IDC_FIT_BUTTON, &FanTestDataDlg::OnBnClickedFitButton)
	ON_NOTIFY(UDN_DELTAPOS, IDC_NUM_SPIN, &FanTestDataDlg::OnDeltaposNumSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_HQ_SPIN, &FanTestDataDlg::OnDeltaposHqSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_NQ_SPIN3, &FanTestDataDlg::OnDeltaposNqSpin3)
	ON_NOTIFY(UDN_DELTAPOS, IDC_EQ_SPIN2, &FanTestDataDlg::OnDeltaposEqSpin2)
	ON_NOTIFY(NM_RCLICK, IDC_FAN_TEST_DATAS_LIST, &FanTestDataDlg::OnNMRClickFanTestDatasList)
	ON_COMMAND(ID_CLISTMENU_DELETE, &FanTestDataDlg::OnDeleteItem)
	ON_COMMAND(ID_CLISTMENU_ADD, &FanTestDataDlg::OnAddItem)
	ON_EN_KILLFOCUS(IDC_DATAS_NUM_EDIT, &FanTestDataDlg::OnEnKillfocusDatasNumEdit)
	ON_EN_KILLFOCUS(IDC_HQ_EDIT, &FanTestDataDlg::OnEnKillfocusHqEdit)
	ON_EN_KILLFOCUS(IDC_NQ_EDIT, &FanTestDataDlg::OnEnKillfocusNqEdit)
	ON_EN_KILLFOCUS(IDC_EQ_EDIT, &FanTestDataDlg::OnEnKillfocusEqEdit)
	ON_NOTIFY(LVN_BEGINLABELEDIT, IDC_FAN_TEST_DATAS_LIST, &FanTestDataDlg::OnLvnBeginlabeleditFanTestDatasList)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_FAN_TEST_DATAS_LIST, &FanTestDataDlg::OnLvnEndlabeleditFanTestDatasList)
END_MESSAGE_MAP()

BOOL FanTestDataDlg::OnInitDialog()
{
	TabCtrlBaseDlg::OnInitDialog();
	buildListCtrl();
	activeButton(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
}

void FanTestDataDlg::readDatas()
{
	if(!readDatasFromFile(ARX_FILE_PATH(TEST_DATAS_FILE_NAME),m_listDatasVector,5))
	{
		if(!readDatasFromFile(ARX_FILE_PATH(_T("FanTestDataHeader.txt")),m_listDatasVector,5))
		{
			m_missFiels = true;
		}
	}
}

void FanTestDataDlg::buildListCtrl()
{
	readDatas();
	if(m_listDatasVector.empty()) return;
	m_num = m_listDatasVector.size()-1;
	initListCtrl();
	setIterms();
	UpdateData(FALSE);
}

void FanTestDataDlg::setDatas(const ArrayVector& datas)
{
	m_listDatasVector = datas;
}

void FanTestDataDlg::saveListDatas()
{
	updateDatas();
	writeDatasToFile(TEST_DATAS_FILE_NAME,m_listDatasVector);
}

void FanTestDataDlg::save()
{
	//saveListDatas();
	//保存回归之后的数据
	writeDatasToFile(_T("FanEquations.txt"),m_equations);
}

void FanTestDataDlg::initListCtrl()
{
	CRect rect;
	m_testList.GetClientRect(&rect); //获得当前客户区信息  
	m_testList.InsertHiddenLabelColumn();	// Requires one never uses column 0
	int colCount = m_listDatasVector[0].length();
	for(int col = 0; col <  colCount; ++col)
	{
		const CString& title = m_listDatasVector[0][col].kACharPtr();
		CGridColumnTraitText* pTrait = NULL;
		if(col > 0) pTrait = new CGridColumnTraitEdit;
		else pTrait = new CGridColumnTraitText;
		double colWidth = rect.Width()/colCount;
		if( colWidth < 50 ) colWidth = 50;
		m_testList.InsertColumnTrait(col+1, title, LVCFMT_LEFT, colWidth, col, pTrait);
	}
}

void FanTestDataDlg::setIterms()
{
	m_testList.DeleteAllItems();
	int nItem = 0;
	for(size_t rowId = 1; rowId < m_listDatasVector.size() ; ++rowId)
	{
		m_testList.InsertItem(nItem, m_listDatasVector[rowId][0]);
		m_testList.SetItemData(nItem, rowId);
		for(int col = 0; col < m_listDatasVector[0].length() ; ++col)
		{
			int nCellCol = col+1;	// +1 because of hidden column
			CString data = m_listDatasVector[rowId][col].kACharPtr();
			if(_T("NULL") == data) data = _T("");
			const CString& strCellText = data;
			m_testList.SetItemText(nItem, nCellCol, strCellText);
		}
		CString strText;
		strText.Format(_T("%d"),nItem+1);
		m_testList.SetItemText(nItem, 1, strText);
		nItem++;
	}	
}

void FanTestDataDlg::updateDatas()
{
	UpdateData(TRUE);
	m_listDatasVector.clear();
	AcStringArray headers;
	for (int i = 1; i < m_testList.GetColumnCount(); i++)
	{
		headers.append(m_testList.GetColumnHeading(i));
	}
	m_listDatasVector.push_back(headers);
	for (int i = 0; i < m_testList.GetItemCount(); i++)
	{
		AcStringArray datas;
		for (int j = 1; j < m_testList.GetColumnCount(); j++)
		{
			datas.append(m_testList.GetItemText(i,j));
		}
		m_listDatasVector.push_back(datas);
	}
}

void FanTestDataDlg::updateList()
{
	updateDatas();
	int dataSize = m_listDatasVector.size()-1;//减去表头
	if( m_num == dataSize ) return;
	if(m_num < dataSize)
	{
		for(int i = 0; i < dataSize - m_num; i++)
		{
			m_listDatasVector.pop_back();
		}
	}
	if(m_num > dataSize)
	{
		for(int i = 0; i < m_num - dataSize; i++)
		{
			AcStringArray datas;
			for (int j = 1; j < m_testList.GetColumnCount(); j++)
			{
				datas.append(_T(""));
			}
			m_listDatasVector.push_back(datas);
		}
	}
	setIterms();
	activeButton();
}

// FanTestDataDlg 消息处理程序

void FanTestDataDlg::OnBnClickedLastDatasButton()
{
	readDatas();
	if(m_listDatasVector.empty()) return;
	m_num = m_listDatasVector.size()-1;
	setIterms();
	UpdateData(FALSE);
	activeButton();
}

void FanTestDataDlg::OnBnClickedClearAllButton()
{
	if(m_testList.GetItemCount() < 1) return;
	m_testList.DeleteAllItems();
	m_num = 0;
	UpdateData(FALSE);
	activeButton();
}

void FanTestDataDlg::OnBnClickedSaveTestDatasButton()
{
	saveListDatas();
	activeButton(FALSE);
}

void FanTestDataDlg::fit(const vector<double>& v_x, const vector<double>& v_y, int n, const CString& printStr)
{
	czy::Fit fit;
	fit.polyfit(v_x,v_y,n,true);
	CString strFun(_T("y=")),strTemp(_T(""));
	AcStringArray equas;
	equas.append(printStr);
	for (int i=0;i<fit.getFactorSize();++i)
	{
		CString strEq;
		if (0 == i)
		{
			strTemp.Format(_T("%g"),fit.getFactor(i));
			strEq = strTemp;
		}
		else
		{
			double fac = fit.getFactor(i);
			if (fac<0)
			{
				strTemp.Format(_T("%gx^%d"),fac,i);
			}
			else
			{
				strTemp.Format(_T("+%gx^%d"),fac,i);
			}
			strEq.Format(_T("%g"),fac);
		}
		equas.append(strEq);
		strFun += strTemp;
	}
	m_equations.push_back(equas);
	CString str;
	str.Format(_T("%s回归方程：%s\r\n误差：ssr:%g,sse=%g,rmse:%g,确定系数:%g"),printStr,strFun
		,fit.getSSR(),fit.getSSE(),fit.getRMSE(),fit.getR_square());
	m_print = m_print + _T("\r\n------------------------\r\n")+str;
	UpdateData(FALSE);
}

void FanTestDataDlg::OnBnClickedFitButton()
{
	m_print = _T("");
	UpdateData(FALSE);
	saveListDatas();
	activeButton(FALSE);
	vector<double> v_Q, v_H, v_N, v_E;
	getListColDatas(2,v_Q);
	getListColDatas(3,v_H);
	getListColDatas(4,v_N);
	getListColDatas(5,v_E);
	m_equations.clear();
	fit(v_Q,v_H,m_iHQ,_T("H_Q"));
	fit(v_Q,v_N,m_iNQ,_T("N_Q"));
	fit(v_Q,v_E,m_iEQ,_T("E_Q"));
	m_dataChanged = true;
	GetParent()->GetParent()->SendMessage(WM_PRO_CHANGED);
}

void FanTestDataDlg::OnDeltaposNumSpin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	UpdateData(TRUE);
	if(pNMUpDown->iDelta == 1) //点击了Spin的往下箭头 
	{  
		if(m_num <= 0) return;
		m_num -= 1;
	} 
	else if(pNMUpDown->iDelta == -1) //点击了Spin的往上箭头 
	{ 
		m_num += 1;
	}
	UpdateData(FALSE);
	updateList();
	*pResult = 0;
}

void FanTestDataDlg::OnDeltaposHqSpin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	UpdateData(TRUE);
	if(pNMUpDown->iDelta == 1) //点击了Spin的往下箭头 
	{  
		if(m_iHQ <= 1) return;
		m_iHQ -= 1;
	} 
	else if(pNMUpDown->iDelta == -1) //点击了Spin的往上箭头 
	{ 
		if(m_iHQ >= 5) return;
		m_iHQ += 1;
	}
	UpdateData(FALSE);
	*pResult = 0;
}

void FanTestDataDlg::OnDeltaposNqSpin3(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	UpdateData(TRUE);
	if(pNMUpDown->iDelta == 1) //点击了Spin的往下箭头 
	{  
		if(m_iNQ <= 1) return;
		m_iNQ -= 1;
	} 
	else if(pNMUpDown->iDelta == -1) //点击了Spin的往上箭头 
	{ 
		if(m_iNQ >= 5) return;
		m_iNQ += 1;
	}
	UpdateData(FALSE);
	*pResult = 0;
}

void FanTestDataDlg::OnDeltaposEqSpin2(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	UpdateData(TRUE);
	if(pNMUpDown->iDelta == 1) //点击了Spin的往下箭头 
	{  
		if(m_iEQ <= 1) return;
		m_iEQ -= 1;
	} 
	else if(pNMUpDown->iDelta == -1) //点击了Spin的往上箭头 
	{ 
		if(m_iEQ >= 5) return;
		m_iEQ += 1;
	}
	UpdateData(FALSE);
	*pResult = 0;
}

void FanTestDataDlg::OnNMRClickFanTestDatasList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	if(pNMListView->iItem == -1) return; 
	CPoint point;
	::GetCursorPos(&point);

	CMenu menu;
	menu.LoadMenu(IDR_CLIST_MENU);
	CMenu* popup=menu.GetSubMenu(0);
	if(popup==NULL ) return;

	CBitmap m_bitmap1;//位图
	m_bitmap1.LoadBitmap(IDB_ADD);

	popup->SetMenuItemBitmaps(0,MF_BYPOSITION,&m_bitmap1,&m_bitmap1); 
	m_bitmap1.LoadBitmap(IDB_DELETE);
	popup->SetMenuItemBitmaps(1,MF_BYPOSITION,&m_bitmap1,&m_bitmap1); 

	popup->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, this );
	m_bitmap1.DeleteObject();
	*pResult = 0;
}

void FanTestDataDlg::OnDeleteItem()
{
	int nIndex;
	POSITION pos = m_testList.GetFirstSelectedItemPosition();
	nIndex = m_testList.GetNextSelectedItem(pos); 
	if(nIndex == -1) return;
	m_testList.DeleteItem(nIndex);
	UpdateData(TRUE);
	m_num -= 1;
	UpdateData(FALSE);
	for(int i = nIndex; i < m_testList.GetItemCount(); i++)
	{
		CString indexStr;
		indexStr.Format(_T("%d"),i+1);
		m_testList.SetItemText(i,1,indexStr);
	}
	activeButton();
}

void FanTestDataDlg::OnAddItem()
{
	int nItem = m_testList.GetItemCount();
	CString strText;
	strText.Format(_T("Item %d"),nItem);
	m_testList.InsertItem(nItem, strText);
	m_testList.SetItemData(nItem, nItem+2);
	for(int col = 0; col < m_testList.GetColumnCount() ; ++col)
	{
		int nCellCol = col+1;	// +1 because of hidden column
		m_testList.SetItemText(nItem, nCellCol, _T(""));
	}
	strText.Format(_T("%d"),nItem+1);
	m_testList.SetItemText(nItem, 1, strText);
	UpdateData(TRUE);
	m_num += 1;
	UpdateData(FALSE);
	activeButton();
}

void FanTestDataDlg::OnEnKillfocusDatasNumEdit()
{
	updateList();
}

void FanTestDataDlg::OnEnKillfocusHqEdit()
{
	UpdateData(TRUE);
	if(m_iHQ > 5) m_iHQ = 5;
	if(m_iHQ < 1) m_iHQ = 1;
	UpdateData(FALSE);
}

void FanTestDataDlg::OnEnKillfocusNqEdit()
{
	UpdateData(TRUE);
	if(m_iNQ > 5) m_iNQ = 5;
	if(m_iNQ < 1) m_iNQ = 1;
	UpdateData(FALSE);
}

void FanTestDataDlg::OnEnKillfocusEqEdit()
{
	UpdateData(TRUE);
	if(m_iEQ > 5) m_iEQ = 5;
	if(m_iEQ < 1) m_iEQ = 1;
	UpdateData(FALSE);
}

void FanTestDataDlg::activeButton(BOOL bActive /*= TRUE*/)
{
	m_saveBtn.EnableWindow(bActive);
	m_lastDataBtn.EnableWindow(bActive);
}

void FanTestDataDlg::OnLvnBeginlabeleditFanTestDatasList(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
	LV_ITEM* pItem= &(pDispInfo)->item;  
	CString data = m_testList.GetItemText(pItem->iItem,pItem->iSubItem);
	m_oldText = data;
	*pResult = 0;
}

void FanTestDataDlg::OnLvnEndlabeleditFanTestDatasList(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
	LV_ITEM* pItem= &(pDispInfo)->item;  
	CString data = m_testList.GetItemText(pItem->iItem,pItem->iSubItem);
	//acutPrintf(_T("\n编辑之后的数据:%s"),data);
	if(m_oldText != data)
	{
		activeButton();
	}
	*pResult = 0;
}

void FanTestDataDlg::getListColDatas(int colIndex, vector<double>& datas)
{
	datas.clear();
	for (int i = 0; i < m_testList.GetItemCount(); i++)
	{
		CString data = m_testList.GetItemText(i,colIndex);
		datas.push_back(_tstof(data));
	}
}
