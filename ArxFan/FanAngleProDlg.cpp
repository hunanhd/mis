#include "stdafx.h"
#include "FanAngleProDlg.h"
#include "CGridColumnTraitText.h"
#include "CGridColumnTraitEdit.h"
// FanAngleProDlg 对话框

IMPLEMENT_DYNAMIC(FanAngleProDlg, TabCtrlBaseDlg)

FanAngleProDlg::FanAngleProDlg(CWnd* pParent /*=NULL*/)
	: TabCtrlBaseDlg(FanAngleProDlg::IDD, pParent)
	, m_rowNum(0)
{

}

FanAngleProDlg::~FanAngleProDlg()
{
}

void FanAngleProDlg::DoDataExchange(CDataExchange* pDX)
{
	TabCtrlBaseDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FAN_ANGLE_DATAS_LIST, m_listCtrl);
	DDX_Text(pDX, IDC_ANGLE_NUM_EDIT, m_rowNum);
}


BEGIN_MESSAGE_MAP(FanAngleProDlg, TabCtrlBaseDlg)
	ON_NOTIFY(NM_RCLICK, IDC_FAN_ANGLE_DATAS_LIST, &FanAngleProDlg::OnNMRClickFanAngleDatasList)
	ON_COMMAND(ID_CLISTMENU_DELETE, &FanAngleProDlg::OnDeleteItem)
	ON_COMMAND(ID_CLISTMENU_ADD, &FanAngleProDlg::OnAddItem)
	ON_NOTIFY(LVN_BEGINLABELEDIT, IDC_FAN_ANGLE_DATAS_LIST, &FanAngleProDlg::OnLvnBeginlabeleditFanAngleDatasList)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_FAN_ANGLE_DATAS_LIST, &FanAngleProDlg::OnLvnEndlabeleditFanAngleDatasList)
	ON_EN_KILLFOCUS(IDC_ANGLE_NUM_EDIT, &FanAngleProDlg::OnEnKillfocusAngleNumEdit)
	ON_NOTIFY(UDN_DELTAPOS, IDC_NUM_SPIN, &FanAngleProDlg::OnDeltaposNumSpin)
END_MESSAGE_MAP()


// FanAngleProDlg 消息处理程序

void FanAngleProDlg::initListCtrl()
{
	CRect rect;
	m_listCtrl.GetClientRect(&rect); //获得当前客户区信息  
	m_listCtrl.InsertHiddenLabelColumn();	// Requires one never uses column 0
	int colCount = m_listDatasVector[0].length();
	for(int col = 0; col <  colCount; ++col)
	{
		const CString& title = m_listDatasVector[0][col].kACharPtr();
		CGridColumnTraitText* pTrait = NULL;
		if(col < 19) pTrait = new CGridColumnTraitEdit;
		else pTrait = new CGridColumnTraitText;
		double colWidth = rect.Width()/colCount;
		if( colWidth < 80 ) colWidth = 80;
		m_listCtrl.InsertColumnTrait(col+1, title, LVCFMT_LEFT, colWidth, col, pTrait);
	}
}

void FanAngleProDlg::setIterms()
{
	m_listCtrl.DeleteAllItems();
	int nItem = 0;
	for(size_t rowId = 1; rowId < m_listDatasVector.size() ; ++rowId)
	{
		m_listCtrl.InsertItem(nItem, m_listDatasVector[rowId][0]);
		m_listCtrl.SetItemData(nItem, rowId);
		for(int col = 0; col < m_listDatasVector[0].length() ; ++col)
		{
			int nCellCol = col+1;	// +1 because of hidden column
			CString data = m_listDatasVector[rowId][col].kACharPtr();
			if(_T("NULL") == data) data = _T("");
			const CString& strCellText = data;
			m_listCtrl.SetItemText(nItem, nCellCol, strCellText);
		}
		nItem++;
	}	
}

BOOL FanAngleProDlg::OnInitDialog()
{
	TabCtrlBaseDlg::OnInitDialog();
	if(!readDatasFromFile(ARX_FILE_PATH(ANGLE_DATAS_FILE_NAME),m_listDatasVector,28))
	{
		if(!readDatasFromFile(ARX_FILE_PATH(_T("FanAngleHeader.txt")),m_listDatasVector,28))
		{
			m_missFiels = true;
		}
	}
	if(m_listDatasVector.empty()) return TRUE;
	m_rowNum = m_listDatasVector.size()-1;
	initListCtrl();
	setIterms();
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
}

void FanAngleProDlg::setDatas(const ArrayVector& datas)
{
	m_listDatasVector = datas;
}

void FanAngleProDlg::OnNMRClickFanAngleDatasList(NMHDR *pNMHDR, LRESULT *pResult)
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	if(pNMListView->iItem == -1) return; 
	CPoint point;
	::GetCursorPos(&point);

	CMenu menu;
	menu.LoadMenu(IDR_CLIST_MENU);
	CMenu* popup=menu.GetSubMenu(0);
	if(popup==NULL ) return;

	CBitmap m_bitmap1;//位图
	m_bitmap1.LoadBitmap(IDB_ADD);

	popup->SetMenuItemBitmaps(0,MF_BYPOSITION,&m_bitmap1,&m_bitmap1); 
	m_bitmap1.LoadBitmap(IDB_DELETE);
	popup->SetMenuItemBitmaps(1,MF_BYPOSITION,&m_bitmap1,&m_bitmap1); 

	popup->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, this );
	m_bitmap1.DeleteObject();
	*pResult = 0;
}

void FanAngleProDlg::OnDeleteItem()
{
	//删除所选项
	int nIndex;
	POSITION pos = m_listCtrl.GetFirstSelectedItemPosition();
	nIndex = m_listCtrl.GetNextSelectedItem(pos); 
	if(nIndex == -1) return;
	m_listCtrl.DeleteItem(nIndex);
	UpdateData(TRUE);
	m_rowNum = m_rowNum-1;
	UpdateData(FALSE);
	m_dataChanged = true;
	GetParent()->GetParent()->SendMessage(WM_PRO_CHANGED);
}

void FanAngleProDlg::OnAddItem()
{
	int nItem = m_listCtrl.GetItemCount();
	CString strText;
	strText.Format(_T("Item %d"),nItem);
	m_listCtrl.InsertItem(nItem, strText);
	m_listCtrl.SetItemData(nItem, nItem+2);
	for(int col = 0; col < m_listCtrl.GetColumnCount() ; ++col)
	{
		int nCellCol = col+1;	// +1 because of hidden column
		m_listCtrl.SetItemText(nItem, nCellCol, _T(""));
	}
	//strText.Format(_T("%d"),nItem+1);
	//m_listCtrl.SetItemText(nItem, 1, strText);
	UpdateData(TRUE);
	m_rowNum = m_rowNum+1;
	UpdateData(FALSE);
	m_dataChanged = true;
	GetParent()->GetParent()->SendMessage(WM_PRO_CHANGED);
}

void FanAngleProDlg::OnLvnBeginlabeleditFanAngleDatasList(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
	LV_ITEM* pItem= &(pDispInfo)->item;  
	CString data = m_listCtrl.GetItemText(pItem->iItem,pItem->iSubItem);
	//acutPrintf(_T("\n编辑之前的数据:%s"),data);
	m_oldText = data;
	*pResult = 0;
}

void FanAngleProDlg::OnLvnEndlabeleditFanAngleDatasList(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
	LV_ITEM* pItem= &(pDispInfo)->item;  
	CString data = m_listCtrl.GetItemText(pItem->iItem,pItem->iSubItem);
	//acutPrintf(_T("\n编辑之后的数据:%s"),data);
	if(m_oldText != data)
	{
		m_dataChanged = true;
		GetParent()->GetParent()->SendMessage(WM_PRO_CHANGED);
	}
	*pResult = 0;
}

void FanAngleProDlg::save()
{
	updateDatas();
	writeDatasToFile(ANGLE_DATAS_FILE_NAME,m_listDatasVector);
}

void FanAngleProDlg::OnEnKillfocusAngleNumEdit()
{
	updateList();
}

void FanAngleProDlg::updateDatas()
{
	UpdateData(TRUE);
	m_listDatasVector.clear();
	AcStringArray headers;
	for (int i = 1; i < m_listCtrl.GetColumnCount(); i++)
	{
		headers.append(m_listCtrl.GetColumnHeading(i));
	}
	m_listDatasVector.push_back(headers);
	for (int i = 0; i < m_listCtrl.GetItemCount(); i++)
	{
		AcStringArray datas;
		for (int j = 1; j < m_listCtrl.GetColumnCount(); j++)
		{
			datas.append(m_listCtrl.GetItemText(i,j));
		}
		m_listDatasVector.push_back(datas);
	}
}

void FanAngleProDlg::OnDeltaposNumSpin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	UpdateData(TRUE);
	if(pNMUpDown->iDelta == 1) //点击了Spin的往下箭头 
	{  
		if(m_rowNum <= 0) return;
		m_rowNum -= 1;
	} 
	else if(pNMUpDown->iDelta == -1) //点击了Spin的往上箭头 
	{ 
		m_rowNum += 1;
	}
	UpdateData(FALSE);
	updateList();
	*pResult = 0;
}

void FanAngleProDlg::updateList()
{
	updateDatas();
	int dataSize = m_listDatasVector.size()-1;//减去表头
	if( m_rowNum == dataSize ) return;
	if(m_rowNum < dataSize)
	{
		for(int i = 0; i < dataSize - m_rowNum; i++)
		{
			m_listDatasVector.pop_back();
		}
	}
	if(m_rowNum > dataSize)
	{
		for(int i = 0; i < m_rowNum - dataSize; i++)
		{
			AcStringArray datas;
			for (int j = 1; j < m_listCtrl.GetColumnCount(); j++)
			{
				datas.append(_T(""));
			}
			m_listDatasVector.push_back(datas);
		}
	}
	setIterms();
}
