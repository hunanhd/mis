#include "stdafx.h"
#include "FanCurveDlg.h"
#include "ChartAxisLabel.h"
#include "fit.h"
#include <WindowsX.h>
#include <afx.h>

#define HQ_LINE _T("压力曲线")
#define R_LINE _T("风阻曲线")
#define NQ_LINE _T("功率曲线")
#define EQ_LINE _T("效率曲线")

IMPLEMENT_DYNAMIC(FanCurveDlg, TabCtrlBaseDlg)

FanCurveDlg::FanCurveDlg(CWnd* pParent /*=NULL*/)
: TabCtrlBaseDlg(FanCurveDlg::IDD, pParent)
, m_lineWidth(1)
, m_minValue(0)
, m_maxValue(100)
,m_lineType(0)
{
	m_HQLinePro.m_lineType = m_lineType;
	m_HQLinePro.m_lineWidth = m_lineWidth;
	m_RLinePro.m_lineType = m_lineType;
	m_RLinePro.m_lineWidth = m_lineWidth;
	m_NQLinePro.m_lineType = m_lineType;
	m_NQLinePro.m_lineWidth = m_lineWidth;
	m_EQLinePro.m_lineType = m_lineType;
	m_EQLinePro.m_lineWidth = m_lineWidth;
}

FanCurveDlg::~FanCurveDlg()
{
}

void FanCurveDlg::DoDataExchange(CDataExchange* pDX)
{
	TabCtrlBaseDlg::DoDataExchange(pDX);
	//DDX_Control(pDX, IDC_CHART, m_chartCtrl);
	DDX_Control(pDX, IDC_HQ_CHECK, m_HQCheack);
	DDX_Control(pDX, IDC_R_CHECK, m_RCheack);
	DDX_Control(pDX, IDC_NQ_CHECK, m_NQCheack);
	DDX_Control(pDX, IDC_EQ_CHECK, m_EQCheack);
	DDX_Control(pDX, IDC_LENGENDVIS_CHECK, m_legendVisBtn);
	DDX_Control(pDX, IDC_LINE_PRO_LIST, m_lineList);
	DDX_CBIndex(pDX, IDC_LINE_TYPE_COMBO, m_lineType);
	DDX_Text(pDX, IDC_LINE_WIDTH_EDIT, m_lineWidth);
	DDX_Control(pDX, IDC_TYPE_COLOR_BUTTON, m_lineColor);
	DDX_Control(pDX, IDC_AUTO_CHECK, m_autoAxisBtn);
	DDX_Text(pDX, IDC_MIN_VALUE_EDIT, m_minValue);
	DDX_Text(pDX, IDC_MAX_VALUE_EDIT, m_maxValue);
}


BEGIN_MESSAGE_MAP(FanCurveDlg, TabCtrlBaseDlg)
	ON_BN_CLICKED(IDC_HQ_CHECK, &FanCurveDlg::OnBnClickedHqCheck)
	ON_BN_CLICKED(IDC_R_CHECK, &FanCurveDlg::OnBnClickedRCheck)
	ON_BN_CLICKED(IDC_NQ_CHECK, &FanCurveDlg::OnBnClickedNqCheck)
	ON_BN_CLICKED(IDC_EQ_CHECK, &FanCurveDlg::OnBnClickedEqCheck)
	ON_BN_CLICKED(IDC_LENGENDVIS_CHECK, &FanCurveDlg::OnBnClickedLengendvisCheck)
	ON_BN_CLICKED(IDC_AUTO_CHECK, &FanCurveDlg::OnBnClickedAutoCheck)
	ON_BN_CLICKED(IDC_LEFT_AXIS_RADIO, &FanCurveDlg::OnBnClickedLeftAxisRadio)
	ON_BN_CLICKED(IDC_BUTTOM_AXIS_RADIO, &FanCurveDlg::OnBnClickedButtomAxisRadio)
	ON_EN_KILLFOCUS(IDC_MIN_VALUE_EDIT, &FanCurveDlg::OnEnKillfocusMinValueEdit)
	ON_EN_KILLFOCUS(IDC_MAX_VALUE_EDIT, &FanCurveDlg::OnEnKillfocusMaxValueEdit)
	ON_LBN_SELCHANGE(IDC_LINE_PRO_LIST, &FanCurveDlg::OnLbnSelchangeLineProList)
	ON_MESSAGE(CPN_SELENDOK, &FanCurveDlg::OnColorSelChange)
	ON_EN_KILLFOCUS(IDC_LINE_WIDTH_EDIT, &FanCurveDlg::OnEnKillfocusLineWidthEdit)
	ON_CBN_SELCHANGE(IDC_LINE_TYPE_COMBO, &FanCurveDlg::OnCbnSelchangeLineTypeCombo)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN1, &FanCurveDlg::OnDeltaposSpin1)
END_MESSAGE_MAP()

static double Randf(double min,double max)
{
	int minInteger = (int)(min*10000);
	int maxInteger = (int)(max*10000);
	int randInteger = rand()*rand();
	int diffInteger = maxInteger - minInteger;
	int resultInteger = randInteger % diffInteger + minInteger;
	return resultInteger/10000.0;
}

BOOL FanCurveDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	CRect rect,rectChart;   
	GetDlgItem(IDC_CHART)->GetWindowRect(&rect);  
	ScreenToClient(rect);  
	rectChart = rect;  
	//rectChart.top = rect.bottom + 3;  
	//rectChart.bottom = rectChart.top + rect.Height();  
	m_chartCtrl.Create(this,rectChart,IDC_CHART2);  
	m_chartCtrl.ShowWindow(SW_SHOWNORMAL);  
	initAxis();
	initLineList();
	if(!readDatas())
	{
		m_missFiels = true;
		enadbleDraw(FALSE);
	}
	//m_chartCtrl.RemoveAllSeries();//先清空
	((CButton*)GetDlgItem(IDC_LEFT_AXIS_RADIO))->SetCheck(1);
	m_autoAxisBtn.SetCheck(1);
	enableMinMaxEditable(FALSE);
	OnBnClickedLeftAxisRadio();
	return TRUE;
}

void FanCurveDlg::initAxis()
{
	CChartAxis *pAxis = NULL; 
	pAxis = m_chartCtrl.CreateStandardAxis(CChartCtrl::BottomAxis);
	pAxis->SetAutomatic(true);
	pAxis->GetLabel()->SetText(_T("Q(m^3/s)"));
	pAxis = m_chartCtrl.CreateStandardAxis(CChartCtrl::LeftAxis);
	pAxis->SetAutomatic(true);
	pAxis->GetLabel()->SetText(_T("H(Pa)"));
}

void FanCurveDlg::initLineList()
{
	m_lineList.ResetContent();
	m_lineList.AddString(HQ_LINE);
	m_lineList.AddString(R_LINE);
	m_lineList.AddString(NQ_LINE);
	m_lineList.AddString(EQ_LINE);
	m_lineList.SetCurSel(0);
}

void FanCurveDlg::creatHQLineSerie()
{
	std::vector<double> m_x,m_y;
	if(m_datas.empty()) return;
	int pointCount = m_datas.size()-1;
	m_x.resize(pointCount);
	m_y.resize(pointCount);
	for (int i = 1; i < pointCount+1; i++)
	{
		if(m_datas[i].length()<3) return;
		m_x[i-1] = _tstof(m_datas[i][1].kACharPtr());
		m_y[i-1] = _tstof(m_datas[i][2].kACharPtr());
	}
	czy::Fit fit;
	fit.polyfit(m_x,m_y,2,true);
	std::vector<double> yploy;
	fit.getFitedYs(yploy);

	m_HQLineSerie = m_chartCtrl.CreateLineSerie();	
	m_HQLineSerie->SetSeriesOrdering(poNoOrdering);//设置为无序
	m_HQLineSerie->AddPoints(&m_x[0], &yploy[0], yploy.size());
	m_HQLineSerie->SetName(_T("压力"));
	setLinePro(HQ_LINE,m_HQLineSerie);
}

void FanCurveDlg::creatRLineSerie()
{
	std::vector<double> m_x,m_y;
	double r = 16.9;
	double maxQ = -1024;
	if(m_datas.empty()) return;
	for (int i = 1; i < m_datas.size(); i++)
	{
		if(m_datas[i].length()<3) return;
		double q = _tstof(m_datas[i][1].kACharPtr());
		if(q>maxQ) maxQ = q;
	}
	int pointCount = maxQ + Randf(0,0.5);
	m_x.resize(pointCount);
	m_y.resize(pointCount);
	for(size_t i =0;i<pointCount;++i)
	{
		m_x[i] = i;
		m_y[i] = r*pow(double(i),2);
	}
	czy::Fit fit;
	fit.polyfit(m_x,m_y,2,true);
	std::vector<double> yploy;
	fit.getFitedYs(yploy);
	m_RLineSerie = m_chartCtrl.CreateLineSerie();	
	m_RLineSerie->SetSeriesOrdering(poNoOrdering);//设置为无序
	m_RLineSerie->AddPoints(&m_x[0], &yploy[0], yploy.size());
	m_RLineSerie->SetName(_T("风阻"));
	setLinePro(R_LINE,m_RLineSerie);
}

void FanCurveDlg::creatNQLineSerie()
{
	std::vector<double> m_x,m_y;
	if(m_datas.empty()) return;
	int pointCount = m_datas.size()-1;
	m_x.resize(pointCount);
	m_y.resize(pointCount);
	for (int i = 1; i < pointCount+1; i++)
	{
		if(m_datas[i].length()<4) return;
		m_x[i-1] = _tstof(m_datas[i][1].kACharPtr());
		m_y[i-1] = _tstof(m_datas[i][3].kACharPtr());
	}
	czy::Fit fit;
	fit.polyfit(m_x,m_y,2,true);
	std::vector<double> yploy;
	fit.getFitedYs(yploy);
	m_NQLineSerie = m_chartCtrl.CreateLineSerie();	
	m_NQLineSerie->SetSeriesOrdering(poNoOrdering);//设置为无序
	m_NQLineSerie->AddPoints(&m_x[0], &yploy[0], yploy.size());
	m_NQLineSerie->SetName(_T("功率"));
	setLinePro(NQ_LINE,m_NQLineSerie);
}

void FanCurveDlg::creatEQLineSerie()
{
	std::vector<double> m_x,m_y;
	if(m_datas.empty()) return;
	int pointCount = m_datas.size()-1;
	m_x.resize(pointCount);
	m_y.resize(pointCount);
	for (int i = 1; i < pointCount+1; i++)
	{
		if(m_datas[i].length()<5) return;
		m_x[i-1] = _tstof(m_datas[i][1].kACharPtr());
		double enta1 = _tstof(m_datas[i][3].kACharPtr());
		double enta2 = _tstof(m_datas[i][4].kACharPtr());
		m_y[i-1] = enta1 / enta2;
	}
	czy::Fit fit;
	fit.polyfit(m_x,m_y,2,true);
	std::vector<double> yploy;
	fit.getFitedYs(yploy);
	m_EQLineSerie = m_chartCtrl.CreateLineSerie();	
	m_EQLineSerie->SetSeriesOrdering(poNoOrdering);//设置为无序
	m_EQLineSerie->AddPoints(&m_x[0], &yploy[0], yploy.size());
	m_EQLineSerie->SetName(_T("效率"));
	setLinePro(EQ_LINE,m_EQLineSerie);
}

void FanCurveDlg::OnBnClickedHqCheck()
{
	UpdateData(TRUE);
	if(m_HQCheack.GetCheck() == 1)
	{
		creatHQLineSerie();
		sendMessage();
	}
	else
	{
		clearSerie(m_HQLineSerie);
	}
	((CButton*)GetDlgItem(IDC_LEFT_AXIS_RADIO))->SetCheck(1);
	OnBnClickedLeftAxisRadio();
}

void FanCurveDlg::OnBnClickedRCheck()
{
	UpdateData(TRUE);
	if(m_RCheack.GetCheck() == 1)
	{
		creatRLineSerie();
		sendMessage();
	}
	else
	{
		clearSerie(m_RLineSerie);
	}
	((CButton*)GetDlgItem(IDC_LEFT_AXIS_RADIO))->SetCheck(1);
	OnBnClickedLeftAxisRadio();
}

void FanCurveDlg::OnBnClickedNqCheck()
{
	UpdateData(TRUE);
	if(m_NQCheack.GetCheck() == 1)
	{
		creatNQLineSerie();
		sendMessage();
	}
	else
	{
		clearSerie(m_NQLineSerie);
	}
	((CButton*)GetDlgItem(IDC_LEFT_AXIS_RADIO))->SetCheck(1);
	OnBnClickedLeftAxisRadio();
}

void FanCurveDlg::OnBnClickedEqCheck()
{
	UpdateData(TRUE);
	if(m_EQCheack.GetCheck() == 1)
	{
		creatEQLineSerie();
		sendMessage();
	}
	else
	{
		clearSerie(m_EQLineSerie);
	}
	((CButton*)GetDlgItem(IDC_LEFT_AXIS_RADIO))->SetCheck(1);
	OnBnClickedLeftAxisRadio();
}

void FanCurveDlg::OnBnClickedLengendvisCheck()
{
	if (m_legendVisBtn.GetCheck() == 1)
		m_chartCtrl.GetLegend()->SetVisible(true);
	else
		m_chartCtrl.GetLegend()->SetVisible(false);
	m_chartCtrl.RefreshCtrl();
}

void FanCurveDlg::clearSerie(CChartSerie* serie)
{
	m_chartCtrl.RemoveSerie(serie->GetSerieId());
}

void FanCurveDlg::OnBnClickedAutoCheck()
{
	CChartAxis* pAxis = GetSelectedAxis();
	if ( m_autoAxisBtn.GetCheck() == 1 )
	{
		pAxis->SetAutomatic(true);
		enableMinMaxEditable(FALSE);
	}
	else
	{
		enableMinMaxEditable(TRUE);
		if(!setMinMaxValue())
		{
			m_autoAxisBtn.SetCheck(1);
			return;
		}
		pAxis->SetAutomatic(false);
	}
	m_chartCtrl.RefreshCtrl();
}

CChartAxis* FanCurveDlg::GetSelectedAxis()
{
	if ( ((CButton*)GetDlgItem(IDC_LEFT_AXIS_RADIO))->GetCheck() == 1 )
		return m_chartCtrl.GetLeftAxis();
	if ( ((CButton*)GetDlgItem(IDC_BUTTOM_AXIS_RADIO))->GetCheck() == 1 )
		return m_chartCtrl.GetBottomAxis();
	return NULL;
}

void FanCurveDlg::OnBnClickedLeftAxisRadio()
{
	CChartAxis* pAxis = m_chartCtrl.GetLeftAxis();
	if (pAxis->IsAutomatic())
	{
		m_autoAxisBtn.SetCheck(1);
		enableMinMaxEditable(FALSE);
	}
	else
	{
		m_autoAxisBtn.SetCheck(0);
		enableMinMaxEditable(TRUE);
	}

	pAxis->GetMinMax(m_minValue,m_maxValue);
	UpdateData(FALSE);
}

void FanCurveDlg::OnBnClickedButtomAxisRadio()
{
	CChartAxis* pAxis = m_chartCtrl.GetBottomAxis();
	if (pAxis->IsAutomatic())
	{
		m_autoAxisBtn.SetCheck(1);
		enableMinMaxEditable(FALSE);
	}
	else
	{
		m_autoAxisBtn.SetCheck(0);
		enableMinMaxEditable(TRUE);
	}

	pAxis->GetMinMax(m_minValue,m_maxValue);
	UpdateData(FALSE);
}

void FanCurveDlg::OnEnKillfocusMinValueEdit()
{
	setMinMaxValue();
}

void FanCurveDlg::OnEnKillfocusMaxValueEdit()
{
	setMinMaxValue();
}

void FanCurveDlg::OnLbnSelchangeLineProList()
{
	int index = m_lineList.GetCurSel();
	if(index < 0) return;
	int serieId = (int)m_lineList.GetItemData(index);
	CChartLineSerie *serie = (CChartLineSerie*)m_chartCtrl.GetSerie(serieId);
	CString strTmp;
	m_lineList.GetText(index,strTmp);
	if(NULL == serie) 
	{
		if( HQ_LINE == strTmp )
		{
			m_lineType = m_HQLinePro.m_lineType;
			m_lineWidth = m_HQLinePro.m_lineWidth;
			m_lineColor.SetColour(m_HQLinePro.m_lineColor.GetColour());
		}
		if( R_LINE == strTmp )
		{
			m_lineType = m_RLinePro.m_lineType;
			m_lineWidth = m_RLinePro.m_lineWidth;
			m_lineColor.SetColour(m_RLinePro.m_lineColor.GetColour());
		}
		if( NQ_LINE == strTmp )
		{
			m_lineType = m_NQLinePro.m_lineType;
			m_lineWidth = m_NQLinePro.m_lineWidth;
			m_lineColor.SetColour(m_NQLinePro.m_lineColor.GetColour());
		}
		if( EQ_LINE == strTmp )
		{
			m_lineType = m_EQLinePro.m_lineType;
			m_lineWidth = m_EQLinePro.m_lineWidth;
			m_lineColor.SetColour(m_EQLinePro.m_lineColor.GetColour());
		}
		UpdateData(FALSE);
		return;
	}
	m_lineType = serie->GetPenStyle();
	m_lineWidth = serie->GetWidth();
	m_lineColor.SetColour(serie->GetColor());
	if( HQ_LINE == strTmp )
	{
		m_HQLinePro.m_lineType = m_lineType;
		m_HQLinePro.m_lineWidth = m_lineWidth;
		m_HQLinePro.m_lineColor.SetColour(m_lineColor.GetColour());
	}
	if( R_LINE == strTmp )
	{
		m_RLinePro.m_lineType = m_lineType;
		m_RLinePro.m_lineWidth = m_lineWidth;
		m_RLinePro.m_lineColor.SetColour(m_lineColor.GetColour());
	}
	if( NQ_LINE == strTmp )
	{
		m_NQLinePro.m_lineType = m_lineType;
		m_NQLinePro.m_lineWidth = m_lineWidth;
		m_NQLinePro.m_lineColor.SetColour(m_lineColor.GetColour());
	}
	if( EQ_LINE == strTmp )
	{
		m_EQLinePro.m_lineType = m_lineType;
		m_EQLinePro.m_lineWidth = m_lineWidth;
		m_EQLinePro.m_lineColor.SetColour(m_lineColor.GetColour());
	}
	UpdateData(FALSE);
}

bool FanCurveDlg::setMinMaxValue()
{
	UpdateData(TRUE);
	CChartAxis* pAxis = GetSelectedAxis();
	if (m_minValue > m_maxValue)
	{
		MessageBox(_T("最小坐标值 > 最大坐标值"),_T("错误"),MB_OK);
		return false;
	}
	pAxis->SetMinMax(m_minValue,m_maxValue);
	m_chartCtrl.RefreshCtrl();	
	return true;
}

void FanCurveDlg::enableMinMaxEditable(BOOL bEnable)
{
	GetDlgItem(IDC_MIN_VALUE_EDIT)->EnableWindow(bEnable);
	GetDlgItem(IDC_MAX_VALUE_EDIT)->EnableWindow(bEnable);
}

void FanCurveDlg::setLinePro(const CString& lineName,CChartLineSerie* serie)
{
	int index = m_lineList.FindStringExact(-1,lineName);
	m_lineList.SetItemData(index, serie->GetSerieId());
	m_lineList.SetCurSel(index);
	if( HQ_LINE == lineName )
	{
		if(m_HQLinePro.m_lineType > -1)
			m_HQLineSerie->SetPenStyle(m_HQLinePro.m_lineType);
		if(m_HQLinePro.m_lineWidth > 0)
			m_HQLineSerie->SetWidth(m_HQLinePro.m_lineWidth);
		int colorIndex = m_HQLinePro.m_lineColor.GetColour();
		if(colorIndex != GetSysColor(COLOR_3DFACE))
			m_HQLineSerie->SetColor(m_HQLinePro.m_lineColor.GetColour());
	}
	if( R_LINE == lineName )
	{
		if(m_RLinePro.m_lineType > -1)
			m_RLineSerie->SetPenStyle(m_RLinePro.m_lineType);
		if(m_RLinePro.m_lineWidth > 0)
			m_RLineSerie->SetWidth(m_RLinePro.m_lineWidth);
		if(m_RLinePro.m_lineColor.GetColour() != GetSysColor(COLOR_3DFACE))
			m_RLineSerie->SetColor(m_RLinePro.m_lineColor.GetColour());
	}
	if( NQ_LINE == lineName )
	{
		if(m_NQLinePro.m_lineType > -1)
			m_NQLineSerie->SetPenStyle(m_NQLinePro.m_lineType);
		if(m_NQLinePro.m_lineWidth > 0)
			m_NQLineSerie->SetWidth(m_NQLinePro.m_lineWidth);
		if(m_NQLinePro.m_lineColor.GetColour() != GetSysColor(COLOR_3DFACE))
			m_NQLineSerie->SetColor(m_NQLinePro.m_lineColor.GetColour());
	}
	if( EQ_LINE == lineName )
	{
		if(m_EQLinePro.m_lineType > -1)
			m_EQLineSerie->SetPenStyle(m_EQLinePro.m_lineType);
		if(m_EQLinePro.m_lineWidth > 0)
			m_EQLineSerie->SetWidth(m_EQLinePro.m_lineWidth);
		if(m_EQLinePro.m_lineColor.GetColour() != GetSysColor(COLOR_3DFACE))
			m_EQLineSerie->SetColor(m_EQLinePro.m_lineColor.GetColour());
	}
	OnLbnSelchangeLineProList();
}

LRESULT FanCurveDlg::OnColorSelChange(WPARAM lParam, LPARAM)
{
	int index = m_lineList.GetCurSel();
	if(index < 0) return FALSE;
	int serieId = (int)m_lineList.GetItemData(index);
	CChartLineSerie *serie = (CChartLineSerie*)m_chartCtrl.GetSerie(serieId);
	CString strTmp;
	m_lineList.GetText(index,strTmp);
	if( HQ_LINE == strTmp )
	{
		m_HQLinePro.m_lineColor.SetColour(m_lineColor.GetColour());
	}
	if( R_LINE == strTmp )
	{
		m_RLinePro.m_lineColor.SetColour(m_lineColor.GetColour());
	}
	if( NQ_LINE == strTmp )
	{
		m_NQLinePro.m_lineColor.SetColour(m_lineColor.GetColour());
	}
	if( EQ_LINE == strTmp )
	{
		m_EQLinePro.m_lineColor.SetColour(m_lineColor.GetColour());
	}
	if(NULL == serie) 
	{
		return FALSE;
	}
	serie->SetColor(m_lineColor.GetColour());
	return TRUE;
}


void FanCurveDlg::OnEnKillfocusLineWidthEdit()
{
	UpdateData(TRUE);
	int index = m_lineList.GetCurSel();
	if(index < 0) return;
	int serieId = (int)m_lineList.GetItemData(index);
	CChartLineSerie *serie = (CChartLineSerie*)m_chartCtrl.GetSerie(serieId);
	CString strTmp;
	m_lineList.GetText(index,strTmp);
	if( HQ_LINE == strTmp )
	{
		m_HQLinePro.m_lineWidth = m_lineWidth;
	}
	if( R_LINE == strTmp )
	{
		m_RLinePro.m_lineWidth = m_lineWidth;
	}
	if( NQ_LINE == strTmp )
	{
		m_NQLinePro.m_lineWidth = m_lineWidth;
	}
	if( EQ_LINE == strTmp )
	{
		m_EQLinePro.m_lineWidth = m_lineWidth;
	}
	if(NULL == serie) 
	{
		return;
	}
	serie->SetWidth(m_lineWidth);
}

void FanCurveDlg::OnCbnSelchangeLineTypeCombo()
{
	UpdateData(TRUE);
	int index = m_lineList.GetCurSel();
	if(index < 0) return;
	int serieId = (int)m_lineList.GetItemData(index);
	CChartLineSerie *serie = (CChartLineSerie*)m_chartCtrl.GetSerie(serieId);
	CString strTmp;
	m_lineList.GetText(index,strTmp);
	if( HQ_LINE == strTmp )
	{
		m_HQLinePro.m_lineType = m_lineType;
	}
	if( R_LINE == strTmp )
	{
		m_RLinePro.m_lineType = m_lineType;
	}
	if( NQ_LINE == strTmp )
	{
		m_NQLinePro.m_lineType = m_lineType;
	}
	if( EQ_LINE == strTmp )
	{
		m_EQLinePro.m_lineType = m_lineType;
	}
	if(NULL == serie) 
	{
		return;
	}
	serie->SetPenStyle(m_lineType);
}

void FanCurveDlg::sendMessage()
{
	m_dataChanged = true;
	GetParent()->GetParent()->SendMessage(WM_PRO_CHANGED);
}

void FanCurveDlg::savePicture()
{
	CClientDC SHDC(m_chartCtrl.GetDlgItem(IDC_CHART2));
	CDC memDC;
	CRect rect;
	m_chartCtrl.GetClientRect(rect);
	m_chartCtrl.ClientToScreen(&rect);
	memDC.CreateCompatibleDC(&SHDC);
	CBitmap bm;
	int Width=rect.Width();
	int Height=rect.Height();
	bm.CreateCompatibleBitmap(&SHDC,Width,Height);
	CBitmap *pOld=memDC.SelectObject(&bm);
	memDC.BitBlt(-rect.left,-rect.top,Width+rect.left,Height+rect.top,&SHDC,0,0,SRCCOPY);
	memDC.SelectObject(pOld);
	BITMAP btm;
	bm.GetBitmap(&btm);
	DWORD size=btm.bmWidthBytes*btm.bmHeight;
	LPSTR lpData=(LPSTR)GlobalAllocPtr(GPTR,size);
	BITMAPFILEHEADER bfh;
	BITMAPINFOHEADER bih;
	bih.biBitCount=btm.bmBitsPixel;
	bih.biClrImportant=0;
	bih.biClrUsed=0;
	bih.biCompression=0;
	bih.biHeight=btm.bmHeight;
	bih.biPlanes=1;
	bih.biSize=sizeof(BITMAPINFOHEADER);
	bih.biSizeImage=size;
	bih.biWidth=btm.bmWidth;
	bih.biXPelsPerMeter=0;
	bih.biYPelsPerMeter=0;
	GetDIBits(SHDC,bm,0,bih.biHeight,lpData,(BITMAPINFO*)&bih,DIB_RGB_COLORS);
	bfh.bfReserved1=bfh.bfReserved2=0;
	bfh.bfType=((WORD)('M'<<8)|'B');
	bfh.bfSize=54+size;
	bfh.bfOffBits=54;
	CFileDialog dlg(FALSE,_T("*.bmp"),_T("曲线.bmp"),OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,_T("*.bmp|*.jpg|*.png|*.*||"));
	if (dlg.DoModal()!=IDOK)
	{
		return;
	}
	CFile bf;
	CString ss=dlg.GetPathName();
	if (bf.Open(ss,CFile::modeCreate|CFile::modeWrite))
	{
		bf.Write(&bfh,sizeof(BITMAPFILEHEADER));
		bf.Write(&bih,sizeof(BITMAPINFOHEADER));
		bf.Write(lpData,size);
		bf.Close();
	}
	pOld->DeleteObject();
	bm.DeleteObject();
	GlobalFreePtr(lpData);
}

void FanCurveDlg::OnDeltaposSpin1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	UpdateData(TRUE);
	if(pNMUpDown->iDelta == 1) //点击了Spin的往下箭头 
	{  
		if(m_lineWidth <= 1) return;
		m_lineWidth -= 1;
	} 
	else if(pNMUpDown->iDelta == -1) //点击了Spin的往上箭头 
	{ 
		m_lineWidth += 1;
	}
	UpdateData(FALSE);
	OnEnKillfocusLineWidthEdit();
	*pResult = 0;
}

void FanCurveDlg::enadbleDraw(BOOL bDraw /*= TRUE*/)
{
	m_EQCheack.EnableWindow(bDraw);
	m_RCheack.EnableWindow(bDraw);
	m_NQCheack.EnableWindow(bDraw);
	m_HQCheack.EnableWindow(bDraw);
}

bool FanCurveDlg::readDatas()
{
	if(!readDatasFromFile(ARX_FILE_PATH(TEST_DATAS_FILE_NAME),m_datas,5))
	{
		m_missFiels = true;
		enadbleDraw(FALSE);
		return false;
	}
	if(m_datas.size() < 2)
	{
		m_missFiels = true;
		enadbleDraw(FALSE);
		return false;
	}
	m_missFiels = false;
	enadbleDraw(TRUE);
	return true;
}
