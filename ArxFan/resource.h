//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ArxFan.rc
//
#define IDS_PROJNAME                    100
#define IDC_COMBO1                      104
#define IDB_FAN1                        105
#define IDB_FAN2                        106
#define IDB_FAN3                        107
#define IDC_FAN_TEST_DATAS_LIST         107
#define IDB_ADD                         108
#define IDC_SAVE_TEST_DATAS_BUTTON      108
#define IDB_DELETE                      109
#define IDC_LAST_DATAS_BUTTON           109
#define IDC_TB_LIST                     110
#define IDC_CLEAR_ALL_BUTTON            110
#define IDC_DATAS_NUM_EDIT              112
#define IDC_HQ_EDIT                     113
#define IDD_DRIVER_DIALOG               113
#define IDC_EQ_EDIT                     114
#define IDD_FAN_ANGLE_PRO_DIALOG        114
#define IDC_NQ_EDIT                     115
#define IDC_FAN_ANGLE_DATAS_LIST        115
#define IDC_FIT_BUTTON                  116
#define IDC_PRINT_EDIT                  118
#define IDR_CLIST_MENU                  120
#define IDC_FAN_TAB                     120
#define IDC_HQ_SPIN                     121
#define IDC_EQ_SPIN2                    122
#define IDC_ANGLE_NUM_EDIT              122
#define IDC_BOOT_BUTTON                 122
#define IDC_NQ_SPIN3                    123
#define IDC_NUM_SPIN                    124
#define IDC_SHUT_BUTTON                 125
#define IDC_CHART                       127
#define IDC_HQ_CHECK                    128
#define IDC_REVER_BUTTON                128
#define IDC_R_CHECK                     129
#define IDC_NQ_CHECK                    130
#define IDC_EQ_CHECK                    131
#define IDC_JUST_ANGLE_BUTTON           131
#define IDC_LENGENDVIS_CHECK            132
#define IDD_FAN_CURVE_DIALOG            132
#define IDC_FAN_PIC_POS                 133
#define IDC_FAN_PRO_POS                 135
#define IDC_LINE_PRO_LIST               136
#define IDC_LINE_TYPE_COMBO             137
#define IDC_APPLY_BUTTON                138
#define IDC_LINE_WIDTH_EDIT             138
#define IDC_TYPE_COLOR_BUTTON           139
#define IDC_AUTO_CHECK                  140
#define IDC_LEFT_AXIS_RADIO             141
#define IDC_BUTTOM_AXIS_RADIO           142
#define IDD_TOPLO_ANAS_DLG              143
#define IDC_MIN_VALUE_EDIT              143
#define IDC_MAX_VALUE_EDIT              144
#define IDD_FAN_VISUAL_DIALOG           145
#define IDD_FAN_TEST_DATAS_DIALOG       146
#define IDC_SHOW_IN_TOOLTIP             154
#define IDC_CHART2                      1045
#define IDC_SPIN1                       1046
#define ID_CLISTMENU_ADD                32770
#define ID_CLISTMENU_DELETE             32771

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        110
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         109
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
