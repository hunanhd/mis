#pragma once
#include "resource.h"
#include "afxpropertygridctrl.h"
#include "ArxHelper/HelperClass.h"
#include "afxwin.h"
#include "TabCtrlBaseDlg.h"

class DriverDlg : public TabCtrlBaseDlg
{
	DECLARE_DYNAMIC(DriverDlg)

public:
	DriverDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~DriverDlg();
	virtual BOOL OnInitDialog();
	virtual void OnTimer(UINT_PTR nIDEvent);
// 对话框数据
	enum { IDD = IDD_DRIVER_DIALOG };
	// 初始化
	bool Init();
	// 关联图元
	void SetEntity( const AcDbObjectId& objId );
	// 添加字段
	void AddField( const CString& field );
	// 保存数据
	void save();
	// 是否显示全部数据
	void ShowAll( bool bFlag );

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	afx_msg LRESULT OnPropertyChanged( WPARAM wParam,LPARAM lParam );
	afx_msg void OnBnClickedBootButton();
	afx_msg void OnBnClickedShutButton();
	afx_msg void OnBnClickedReverButton();
	DECLARE_MESSAGE_MAP()

private:
	CMFCPropertyGridCtrl m_propertyDataList;
	AcDbObjectId m_objId;          // 图元id
	AcStringArray m_fields;        // 要显示的字段
	bool m_showAll;
	CStatic m_fanPict;
	int m_iTimer;
	CButton m_bootCtrl;
	CButton m_shutCtrl;
	CButton m_reverCtrl;
	bool m_isReverse;
	CBitmap m_bmp;

private:
	void RunByClockwise(int i);
	void RunByAntiClockwise(int i);
};
