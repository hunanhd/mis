#include "stdafx.h"
#include "TabCtrlBaseDlg.h"
#include <fstream>
#include <ostream>
#include "AcFStream.h"

IMPLEMENT_DYNAMIC(TabCtrlBaseDlg, CDialog)

TabCtrlBaseDlg::TabCtrlBaseDlg(UINT IDD, CWnd* pParent /*=NULL*/)
: CDialog(IDD, pParent),m_dataChanged(false),m_missFiels(false)
{
}

TabCtrlBaseDlg::~TabCtrlBaseDlg()
{
}


void TabCtrlBaseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP( TabCtrlBaseDlg, CDialog )
	//ON_REGISTERED_MESSAGE(AFX_WM_PROPERTY_CHANGED, OnPropertyChanged)
END_MESSAGE_MAP()

BOOL TabCtrlBaseDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	return TRUE;  // return TRUE unless you set the focus to a control
}


//LRESULT TabCtrlBaseDlg::OnPropertyChanged(WPARAM wParam,LPARAM lParam)
//{
//	m_dataChanged = true;
//	GetParent()->GetParent()->SendMessage(WM_PRO_CHANGED);
//	return 0;
//}

bool TabCtrlBaseDlg::getModifyStates()
{
	return m_dataChanged;
}

void TabCtrlBaseDlg::setModifyStates(bool bChanged)
{
	m_dataChanged = bChanged;
}

bool TabCtrlBaseDlg::readDatasFromFile(const CString& fileName, ArrayVector& datas, int nCol)
{
	AcIfstream inFile( fileName );
	if( !inFile ) return false;
	datas.clear();
	while( !inFile.eof() )
	{
		AcStringArray rowlDatas;
		bool readFaild = false;
		for (int i = 0; i < nCol; i++)
		{
			ACHAR data[_MAX_PATH];
			inFile >> data;
			if(inFile.fail())
			{
				readFaild = true;
				break;
			}
			rowlDatas.append(data);
		}
		if(readFaild) break;
		datas.push_back(rowlDatas);
	}
	inFile.close();
	return true;
}

bool TabCtrlBaseDlg::writeDatasToFile(const CString& fileName, const ArrayVector& datas)
{
	if(datas.size() < 1) return false;
	AcOfstream outFile( fileName );
	if( !outFile ) return false;
	for (int i = 0; i < datas.size(); i++)
	{
		for(int j = 0; j < datas[0].length(); j++)
		{
			CString data = datas[i][j].kACharPtr();
			if(data.IsEmpty()) data = _T("NULL");
			outFile << (LPWSTR)(LPCTSTR)data << _T("\t");
		}
		outFile << _T("\n");
	}
	outFile.close();
	return true;
}

bool TabCtrlBaseDlg::getFilesState()
{
	return m_missFiels;
}
