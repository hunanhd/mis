#include "StdAfx.h"
#include "UIHelper.h"
#include "ToploAnasDlg.h"
#include "FanVisualDlg.h"

#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"

ToploAnasDlg* pToploAnasDlg = 0;

static ArrayVector GetListDatas(ToploType topType)
{
	AcStringArray titles;
	ArrayVector v;
	CString title;
	switch(topType)
	{
	case VENTIN:
		title.Format(_T("%d-"),VENTIN+1);
		titles.append(title+_T("x"));
		titles.append(title+_T("y"));
		titles.append(title+_T("z"));
		v.push_back(titles);
		for(int i = 0; i < 4; i++)
		{
			AcStringArray datas;
			CString tmp;
			tmp.Format(_T("%d-"),i+1);
			datas.append(title+tmp+_T("1"));
			datas.append(title+tmp+_T("2"));
			datas.append(title+tmp+_T("3"));
			v.push_back(datas);
		}
		break;
	case VENTOUT:
		title.Format(_T("%d-"),VENTOUT+1);
		titles.append(title+_T("x"));
		titles.append(title+_T("y"));
		titles.append(title+_T("z"));
		v.push_back(titles);
		for(int i = 0; i < 2; i++)
		{
			AcStringArray datas;
			CString tmp;
			tmp.Format(_T("%d-"),i+1);
			datas.append(title+tmp+_T("1"));
			datas.append(title+tmp+_T("2"));
			datas.append(title+tmp+_T("3"));
			v.push_back(datas);
		}
		break;
	case STRCUTRE:
		title.Format(_T("%d-"),STRCUTRE+1);
		titles.append(title+_T("x"));
		titles.append(title+_T("y"));
		titles.append(title+_T("z"));
		v.push_back(titles);
		acutPrintf(_T("\n构筑物巷道...."));
		break;
	case FANTUNNEL:
		acutPrintf(_T("\n通风动力巷道...."));
		break;
	case CONNECT:
		acutPrintf(_T("\n图的连通性...."));
		break;
	case ONEWAY:
		acutPrintf(_T("\n单向回路...."));
		break;
	case RESERVEDIR:
		acutPrintf(_T("\n反向巷道...."));
		break;
	case LOOP:
		acutPrintf(_T("\n自环检查...."));
		break;
	default:
		break;
	}
	return v;
}

void UIHelper::ShowToploAnasDlg(ToploType topType)
{
	CAcModuleResourceOverride myResources;
	ArrayVector v = GetListDatas(topType);
	//如果没有数据即使有表头也不显示对话框
	if(v.size() < 2) return;
	//重新创建对话框
	DestroyToploAnasDlg();
	pToploAnasDlg = new ToploAnasDlg(acedGetAcadFrame());
	pToploAnasDlg->m_listDatasVector = v;
	pToploAnasDlg->Create(IDD_TOPLO_ANAS_DLG);
	pToploAnasDlg->CenterWindow();
	pToploAnasDlg->ShowWindow(SW_SHOW);

}

void UIHelper::DestroyToploAnasDlg()
{
	CAcModuleResourceOverride myResources;
	if(pToploAnasDlg)
	{
		pToploAnasDlg->DestroyWindow();
		delete pToploAnasDlg;
		pToploAnasDlg = 0;
	}
}

void UIHelper::ShowFanVisualDlg(void)
{
	CAcModuleResourceOverride resOverride;
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一个主扇:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "MainFan" ), objId ) ) return;

	FanVisualDlg dlg;
	dlg.SetEntity(objId);
	dlg.DoModal();
}
