#include "stdafx.h"
#include "DriverDlg.h"
// DriverDlg 对话框
#define TIME_SPACE 80

IMPLEMENT_DYNAMIC(DriverDlg, TabCtrlBaseDlg)

DriverDlg::DriverDlg(CWnd* pParent /*=NULL*/)
: TabCtrlBaseDlg(DriverDlg::IDD, pParent),m_iTimer(0)
,m_isReverse(false)
{
	m_showAll = true;
}

DriverDlg::~DriverDlg()
{
}

void DriverDlg::DoDataExchange(CDataExchange* pDX)
{
	TabCtrlBaseDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FAN_PIC_POS, m_fanPict);
	DDX_Control(pDX, IDC_BOOT_BUTTON, m_bootCtrl);
	DDX_Control(pDX, IDC_SHUT_BUTTON, m_shutCtrl);
	DDX_Control(pDX, IDC_REVER_BUTTON, m_reverCtrl);
}


BEGIN_MESSAGE_MAP( DriverDlg, TabCtrlBaseDlg )
	ON_REGISTERED_MESSAGE(AFX_WM_PROPERTY_CHANGED, OnPropertyChanged)
	ON_BN_CLICKED(IDC_BOOT_BUTTON, &DriverDlg::OnBnClickedBootButton)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_SHUT_BUTTON, &DriverDlg::OnBnClickedShutButton)
	ON_BN_CLICKED(IDC_REVER_BUTTON, &DriverDlg::OnBnClickedReverButton)
END_MESSAGE_MAP()

bool DriverDlg::Init()
{
	m_propertyDataList.RemoveAll();
	if( m_objId.isNull() ) return false;
	// 获取类型名称
	CString type;
	if( !ArxDataTool::GetTypeName( m_objId, type ) ) return false;
	//获取字段
	AcStringArray fileds;
	if( m_showAll )
	{
		FieldHelper::GetAllFields( type, fileds );
	}
	else
	{
		fileds.append( m_fields );
	}
	// 创建属性数据控件
	PropertyDataUpdater_NoGroup::BuildPropGridCtrl( &m_propertyDataList, type, fileds );
	// 填充属性数据
	bool ret = PropertyDataUpdater_NoGroup::ReadDataFromGE( &m_propertyDataList, m_objId );

	// 填充数据失败
	if( !ret )
	{
		m_propertyDataList.EnableWindow( FALSE );
		m_propertyDataList.ShowWindow( SW_HIDE );
	}
	else
	{
		m_propertyDataList.ExpandAll( TRUE );
	}
	return ret;
}

void DriverDlg::SetEntity(const AcDbObjectId& objId)
{
	m_objId = objId;
}

void DriverDlg::AddField(const CString& field)
{
	m_fields.append( field );
}

void DriverDlg::save()
{
	UpdateData( TRUE ); // 更新控件
	// 更新图元的属性数据
	PropertyDataUpdater_NoGroup::WriteDataToGE( &m_propertyDataList, m_objId );
}

BOOL DriverDlg::OnInitDialog()
{
	TabCtrlBaseDlg::OnInitDialog();
	// 创建, 定位, 显示CMFCPropertyGridCtrl
	CRect rect;
	GetDlgItem( IDC_FAN_PRO_POS )->GetWindowRect( &rect );
	ScreenToClient( &rect );
	m_propertyDataList.Create( WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_BORDER, rect, this, ( UINT ) - 1 );
	m_propertyDataList.SetDescriptionRows( 2 );
	// 初始化
	this->Init();
	SetTimer(1,TIME_SPACE,NULL);
	m_bootCtrl.EnableWindow(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
}

void DriverDlg::ShowAll(bool bFlag)
{
	m_showAll = bFlag;
}

LRESULT DriverDlg::OnPropertyChanged(WPARAM wParam,LPARAM lParam)
{
	//CMFCPropertyGridProperty* pProp = (CMFCPropertyGridProperty*) lParam;
	//FanVisualDlg   *pMMD = (FanVisualDlg*)AfxGetMainWnd(); 
	//pMMD->lightApplyBtn();
	//pMMD = NULL;
	//delete pMMD;
	m_dataChanged = true;
	GetParent()->GetParent()->SendMessage(WM_PRO_CHANGED);
	return 0;
}

void DriverDlg::OnBnClickedBootButton()
{
	SetTimer(1,TIME_SPACE,NULL);
	m_bootCtrl.EnableWindow(FALSE);
	m_shutCtrl.EnableWindow(TRUE);
	m_reverCtrl.EnableWindow(TRUE);
}

void DriverDlg::OnBnClickedShutButton()
{
	KillTimer(1);
	m_bootCtrl.EnableWindow(TRUE);
	m_shutCtrl.EnableWindow(FALSE);
	m_reverCtrl.EnableWindow(FALSE);
	m_isReverse = false;
}

void DriverDlg::OnBnClickedReverButton()
{
	m_bootCtrl.EnableWindow(FALSE);
	m_shutCtrl.EnableWindow(TRUE);
	m_reverCtrl.EnableWindow(FALSE);
	m_isReverse = true;
}

void DriverDlg::OnTimer(UINT_PTR nIDEvent)
{
	HBITMAP hBmp;
	if(m_iTimer > 2) m_iTimer = 0;
	if(m_isReverse) RunByAntiClockwise(m_iTimer);
	else RunByClockwise(m_iTimer);
	hBmp = (HBITMAP)m_bmp.GetSafeHandle();
	HBITMAP hold = m_fanPict.SetBitmap(hBmp);
	DeleteObject(hBmp);
	m_bmp.DeleteObject();
	DeleteObject(hold);
	m_iTimer++;
	TabCtrlBaseDlg::OnTimer(nIDEvent);
}

void DriverDlg::RunByClockwise(int i)
{
	switch(i)
	{
	case 0:
		m_bmp.LoadBitmap(IDB_FAN1);  
		break;
	case 1:
		m_bmp.LoadBitmap(IDB_FAN2);  
		break;
	case 2:
		m_bmp.LoadBitmap(IDB_FAN3);  
		break;
	default:
		m_bmp.LoadBitmap(IDB_FAN1);  
		break;
	}

}

void DriverDlg::RunByAntiClockwise(int i)
{
	switch(i)
	{
	case 0:
		m_bmp.LoadBitmap(IDB_FAN3);  
		break;
	case 1:
		m_bmp.LoadBitmap(IDB_FAN2);  
		break;
	case 2:
		m_bmp.LoadBitmap(IDB_FAN1);  
		break;
	default:
		m_bmp.LoadBitmap(IDB_FAN3);  
		break;
	}

}
