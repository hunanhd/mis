#pragma once
#include "resource.h"
#include "afxcmn.h"
#include "CGridListCtrlEx.h"
#include "TabCtrlBaseDlg.h"

class FanAngleProDlg : public TabCtrlBaseDlg
{
	DECLARE_DYNAMIC(FanAngleProDlg)

public:
	FanAngleProDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~FanAngleProDlg();
	virtual BOOL OnInitDialog();
	void setDatas(const ArrayVector& datas);
	void save();
// 对话框数据
	enum { IDD = IDD_FAN_ANGLE_PRO_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	afx_msg void OnDeleteItem();
	afx_msg void OnAddItem();
	afx_msg void OnNMRClickFanAngleDatasList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnBeginlabeleditFanAngleDatasList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnEndlabeleditFanAngleDatasList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillfocusAngleNumEdit();
	afx_msg void OnDeltaposNumSpin(NMHDR *pNMHDR, LRESULT *pResult);

	DECLARE_MESSAGE_MAP()
private:
	void initListCtrl();
	void setIterms();
	void updateDatas();
	void updateList();

private:
	ArrayVector m_listDatasVector;
	CGridListCtrlEx m_listCtrl;
	CString m_oldText;
	int m_rowNum;
};
