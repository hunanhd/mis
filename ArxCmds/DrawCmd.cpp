#include "stdafx.h"
#include "DrawCmd.h"
#include "PolyLineJig.h"
#include "IncludeGE.h"
#include "SelBlockDlg.h"
#include "AdvanceDlg.h"
#include "config.h"
#include "MineGE/HelperClass.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
/* 全局函数(实现在PromptTool.cpp) */
extern bool PromptSEPt( const CString& name, AcGePoint3d& startPt, AcGePoint3d& endPt, double& angle );
extern bool GetClosePtAndAngle( const AcDbObjectId& objId, AcGePoint3d& pt, double& angle );
extern bool PromptInsertPt( const AcDbObjectId& objId, AcGePoint3d& pt );
extern bool PromptArcPt( const CString& name, AcGePoint3d& startPt, AcGePoint3d& endPt, AcGePoint3d& thirdPt );
// 绘制风流方向时，巷道的最小长度
#define MIN_LENGTH 150
static bool CheackTubeLenth( AcDbObjectId& objId )
{
	objId = ArxUtilHelper::SelectEntity( _T( "请选择一条瓦斯管路:" ) );
	if( objId.isNull() ) return false;
	if( !ArxUtilHelper::IsEqualType( _T( "GasTube" ), objId ) ) return false;
	AcTransaction* pTrans = actrTransactionManager->startTransaction();
	if ( 0 == pTrans ) return false;
	AcDbObject* pObj;
	if ( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForRead ) ) return false;
	GasTube* pGasTube = GasTube::cast( pObj );
	if ( 0 == pGasTube )
	{
		actrTransactionManager->abortTransaction();
		return false;
	}
	AcGePoint3d spt, ept;
	pGasTube->getSEPoint( spt, ept );
	//double angle = pGasTube->getAngle();
	actrTransactionManager->endTransaction();
	AcGeVector3d v = ept - spt;
	double tubeLenth = v.length();
	return true;
}
static void CheakLayerExit( CString layerName, int colorIndx, AcDb::LineWeight lineWeigt )
{
	//int colorIndx = 7;		//默认的颜色为白色
	//AcDb::LineWeight lineWeigt = AcDb::kLnWt000;
	//lineWeigt = AcDb::kLnWt030;
	AcDbLayerTable* pLayerTbl = NULL;
	// 获取当前的数据库
	AcDbDatabase* pDB = acdbHostApplicationServices()->workingDatabase();
	// 因为要创建新的图层，所以先要以写的方式获取图层表
	pDB->getSymbolTable( pLayerTbl, AcDb::kForWrite );
	// 检查图层是否存在
	if ( !pLayerTbl->has( layerName ) )
	{
		// 初始化一个新的对象，并且设置它的属性
		AcDbLayerTableRecord* pLayerTblRcd = new AcDbLayerTableRecord;
		pLayerTblRcd->setName( layerName );
		pLayerTblRcd->setIsFrozen( 0 ); // 图层设置为THAWED（解冻的）
		pLayerTblRcd->setIsOff( 0 ); // 图层设置为ON(开着的)
		pLayerTblRcd->setIsLocked( 0 ); // 图层 un-locked(解锁的)
		AcCmColor color;
		color.setColorIndex( colorIndx ); // 图层的颜色设置
		pLayerTblRcd->setColor( color );
		pLayerTblRcd->setLineWeight( lineWeigt );
		// 增加一个新的图层到容器(表)中
		pLayerTbl->add( pLayerTblRcd );
		// 把新建的图层关闭(不要删除它)
		pLayerTblRcd->close();
		// 关闭容器(表)
		pLayerTbl->close();
	}
	else
	{
		// 如果这个图层已经存在，仅仅需要关闭表继续就是
		AcDbLayerTableRecord* pLayerTblRcd;
		pLayerTbl->getAt( layerName, pLayerTblRcd, AcDb::kForWrite );
		AcCmColor color;
		color.setColorIndex( colorIndx ); // 图层的颜色设置
		pLayerTblRcd->setColor( color );
		pLayerTblRcd->setLineWeight( lineWeigt );
		pLayerTblRcd->close();
		pLayerTbl->close();
		//acutPrintf(_T("\nMYLAYER already exists"));
	}
}
static void CreateDirection( const AcDbObjectId& host, const AcGePoint3d& pt, double angle, double length, int colorIndx )
{
	WindDirection* pDir = new WindDirection( pt, angle );
	//设置风流方向的长度，当巷道特长时效果不理想，所以暂时固定长度
	//pDir->setLength(length);
	pDir->setRelatedGE( host ); // 关联图元
	pDir->setColorIndex( colorIndx );
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pDir ) ) delete pDir;
}
static bool SetAdvanLenth( const AcDbObjectId& objId, double advanLenth )
{
	AcTransaction* pTrans = actrTransactionManager->startTransaction();
	if( pTrans == 0 ) return false;
	AcDbObject* pObj;
	if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForWrite ) )
	{
		actrTransactionManager->abortTransaction();
		return false;
	}
	TTunnel* pTT = TTunnel::cast( pObj );
	if( pTT == 0 )
	{
		actrTransactionManager->abortTransaction();
		return false;
	}
	double oldAdvanLenth = pTT->getAdvanLenth();
	pTT->setAdvanLenth( oldAdvanLenth + advanLenth );
	actrTransactionManager->endTransaction();
	return true;
}
static void UpdateTTAdvance( const AcDbObjectId& objId )
{
	//acutPrintf(_T("\n测试....\n"));
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "TTunnel" ), objId ) ) return;
	AcGePoint3d spt,ept;
	DrawHelper::GetEdgePoint(objId,spt,ept);
	AcGeVector3d v = ept - spt;
	double speed;
	if(!ExtDictData::GetDouble(objId,_T("掘进.日进尺数"),speed)) return;
	acutPrintf(_T("\n掘进日进尺数:%.2lf"),speed);
	COleDateTime oldTime,curTime;
	COleDateTimeSpan timeSpan;
	curTime = COleDateTime::GetCurrentTime();
	if(!ExtDictData::GetDateTime(objId,_T("掘进.更新日期"),oldTime)) oldTime = curTime;
	timeSpan = curTime - oldTime;
	double days = timeSpan.GetTotalDays();
	acutPrintf(_T("\n掘进天数:%.lf"),days);
	if( days < 0.9 ) return;
	double length = speed*days;
	if(length >= v.length())
		ept = ept + v.normalize()*length;
	if(!DrawHelper::SetEdgePoint(objId,spt,ept)) return;
	if( !SetAdvanLenth(objId,length) ) return;
	ExtDictData::SetDouble(objId,_T("掘进.推进长度"),length);
	//acutPrintf(_T("\n掘进长度:%.2lf"),length);
	ExtDictData::SetDateTime(objId,_T("掘进.更新日期"),curTime);
	ExtDictData::SetDateTime(objId,_T("掘进.上次更新日期"),oldTime);
}
static void UpdateTTAdvanceByUser( const AcDbObjectId& objId, double length )
{
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "TTunnel" ), objId ) ) return;
	if( length < 0 ) return;
	AcGePoint3d spt,ept;
	DrawHelper::GetEdgePoint(objId,spt,ept);
	AcGeVector3d v = ept - spt;
	COleDateTime oldTime,curTime;
	COleDateTimeSpan timeSpan;
	curTime = COleDateTime::GetCurrentTime();
	if(!ExtDictData::GetDateTime(objId,_T("掘进.更新日期"),oldTime)) oldTime = curTime;
	double oldLength;
	if(!ExtDictData::GetDouble(objId,_T("掘进.推进长度"),oldLength)) oldLength = 0;
	double tmpLen = length-oldLength;
	if(abs(tmpLen) < 0.0001) return;
	if( !SetAdvanLenth(objId,tmpLen) ) return;
	if(tmpLen >= v.length())
		ept = ept + v.normalize()*tmpLen;
	ept = ept + v.normalize()*length;
	if(!DrawHelper::SetEdgePoint(objId,spt,ept)) return;
	ExtDictData::SetDouble(objId,_T("掘进.推进长度"),length);
	//acutPrintf(_T("\n掘进长度:%.2lf"),length);
	ExtDictData::SetDateTime(objId,_T("掘进.更新日期"),curTime);
	ExtDictData::SetDateTime(objId,_T("掘进.上次更新日期"),oldTime);
}
void DrawCmd::DrawArcTunnel( void )
{
	AcGePoint3d startPt, endPt, thirdPt;
	CString GEName = _T( "弧线巷道" );
	if( !PromptArcPt( GEName , startPt, endPt, thirdPt ) ) return;
	ArcTunnel* pArcTunnel = new ArcTunnel( startPt, endPt, thirdPt );  // 弧线巷道
	//CheakLayerExit(GEName,7,AcDb::kLnWt030);
	//pArcTunnel->setLayer(GEName);
	if( !ArxUtilHelper::PostToModelSpace( pArcTunnel ) ) delete pArcTunnel;
}
static AcDbObjectId DrawJointHelper(const AcGePoint3d& pt)
{
    JointGE* pJoint = new JointGE(pt);
    // 4、初始化并提交到数据库
    if( !ArxUtilHelper::PostToModelSpace( pJoint ) ) 
    {
        delete pJoint;
        return AcDbObjectId::kNull;
    }
    else
    {
        return pJoint->objectId();
    }
}
void DrawCmd::DrawTunnel( void )
{
	AcGePoint3d startPt, endPt;
	double angle;
	if( !PromptSEPt( _T( "巷道" ), startPt, endPt, angle ) ) return;
    
    // 查找关联的节点
    AcDbObjectId startId = DrawHelper::GetJointGEByPoint(startPt);
    AcDbObjectId endId = DrawHelper::GetJointGEByPoint(endPt);
    if(startId.isNull())
    {
        startId = DrawJointHelper(startPt);
    }
    if(endId.isNull())
    {
        endId = DrawJointHelper(endPt);
    }
    // 3、创建图元
	Tunnel* pTunnel = new Tunnel( startPt, endPt );  // 巷道
    pTunnel->setStartId(startId);
    pTunnel->setEndId(endId);
	// 4、初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pTunnel ) ) 
    {
        delete pTunnel;
    }
}
void DrawCmd::DrawWS( void )
{
	AcGePoint3d startPt, endPt;
	double angle;
	if( !PromptSEPt( _T( "回采工作面" ), startPt, endPt, angle ) ) return;
	WorkSurface* pWS = new WorkSurface( startPt, endPt );
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pWS ) ) delete pWS;
}
void DrawCmd::DrawTTunnel()
{
	AcGePoint3d startPt, endPt;
	double angle;
	if( !PromptSEPt( _T( "掘进工作面" ), startPt, endPt, angle ) ) return;
	TTunnel* pTT = new TTunnel( startPt, endPt );
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pTT ) ) delete pTT;
}
static void SetInTunnel( const AcDbObjectId& objId, const AcDbObjectId& inTunnel )
{
	AcTransaction* pTrans = actrTransactionManager->startTransaction();
	if( pTrans == 0 ) return;
	AcDbObject* pObj;
	if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForWrite ) )
	{
		actrTransactionManager->abortTransaction();
		return;
	}
	TTunnel* pTT = TTunnel::cast( pObj );
	//pTT->setInTunnel( inTunnel );
	actrTransactionManager->endTransaction();
}
static bool GetInTunnel( const AcDbObjectId& objId, AcDbObjectId& inTunnel )
{
	AcTransaction* pTrans = actrTransactionManager->startTransaction();
	if( pTrans == 0 ) return false;
	AcDbObject* pObj;
	if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForRead ) )
	{
		actrTransactionManager->abortTransaction();
		return false;
	}
	TTunnel* pTT = TTunnel::cast( pObj );
	//inTunnel = pTT->getInTunnel();
	actrTransactionManager->endTransaction();
	return true;
}
void DrawCmd::ResetInTunnel()
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "\n请选择一个掘进工作面:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "TTunnel" ), objId ) ) return;
	AcDbObjectId inTunnel = ArxUtilHelper::SelectEntity( _T( "\n请选择掘进工作面的风筒所在的进风巷道:" ) );
	if( inTunnel.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "Tunnel" ), inTunnel ) ) return;
	SetInTunnel( objId, inTunnel );
}
void DrawCmd::ShowInTunnel()
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "\n请选择一个掘进工作面:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "TTunnel" ), objId ) ) return;
	AcDbObjectId inTunnel;
	if( !GetInTunnel( objId, inTunnel ) ) return;
	ArxUtilHelper::ShowEntityWithColor( inTunnel, 2 ); // 使用黄颜色高亮显示
}
void DrawCmd::DrawMainFan()
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一条巷道:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "EdgeGE" ), objId ) ) return;
	if( ArxUtilHelper::IsEqualType( _T( "TTunnel" ), objId ) )
	{
		AfxMessageBox( _T( "掘进巷道不能设置主要通风机!" ) );
		return;
	}
	AcDbObjectIdArray objIds;
	DrawHelper::GetTagGEById2( objId, _T( "MainFan" ), objIds );
	if( !objIds.isEmpty() )
	{
		AfxMessageBox( _T( "该巷道已设置了主要通风机!" ) );
		return;
	}
	AcGePoint3d pt, insertPt;
	double angle;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定主扇的插入点坐标:" ), pt ) ) return;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	insertPt = pt;
	MainFan* pFan = new MainFan( insertPt, angle );
	if( pFan == 0 ) return;
	pFan->setRelatedGE( objId ); // 关联巷道
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pFan ) ) delete pFan;
}
void DrawCmd::DrawLocalFan()
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一条巷道:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "EdgeGE" ), objId ) ) return;
	//if( !ArxUtilHelper::IsEqualType( _T( "Chimney" ), objId ) ) return;
	//AcDbObjectIdArray objIds;
	//DrawHelper::GetTagGEById2( objId, _T( "LocalFan" ), objIds );
	//if( !objIds.isEmpty() )
	//{
	//    AfxMessageBox( _T( "该掘进工作面已设置了局部通风机!" ) );
	//    return;
	//}
	AcGePoint3d pt, insertPt;
	double angle;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定局扇的插入点坐标:" ), pt ) ) return;
	insertPt = pt;
	LocalFan* pFan = new LocalFan( insertPt, angle );
	if( pFan == 0 ) return;
	pFan->setRelatedGE( objId ); // 关联巷道
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pFan ) ) delete pFan;
}
static void FanTagGEDrawed( AcDbObjectId fanId )
{
	//AcTransaction *pTrans = actrTransactionManager->startTransaction();
	//if ( 0 == pTrans ) return;
	//AcDbObject *pObj;
	//if (Acad::eOk != pTrans->getObject(pObj,fanId,AcDb::kForRead)) return;
	//LocalFan *pFan = LocalFan::cast(pObj);
	//if ( 0 == pFan)
	//{
	//	actrTransactionManager->abortTransaction();
	//	return;
	//}
	//AcGePoint3d insertPt = 	pFan->getInsertPt();
	//actrTransactionManager->endTransaction();
	//AcDbObjectIdArray fanTags;
	//DrawHelper::GetTagGEById2( fanId, _T( "FanTagGE" ), fanTags );
	//if (!fanTags.isEmpty())
	//{
	//	ArxEntityHelper::EraseObjects( fanTags, true );
	//}
	//
	//CString name,qStr,hStr,way;
	//ExtDictData::Get(fanId,_T("风机型号"),name);
	//ExtDictData::Get(fanId,_T("工作风量(m3/s)"),qStr);
	//ExtDictData::Get(fanId,_T("工作风压(Pa)"),hStr);
	//ExtDictData::Get(fanId,_T("工作方式"),way);
	//FanTagGE *pFanTag = new FanTagGE(insertPt,name,way,_tstof(qStr),_tstof(hStr));
	//if( pFanTag == 0 ) return;
	////acutPrintf(_T("\ninsertPt:(%lf,%lf),angle:%lf"),insertPt.x,insertPt.y,angle);
	//pFanTag->setRelatedGE( fanId ); // 关联局扇
	//// 初始化并提交到数据库
	//if( !ArxUtilHelper::PostToModelSpace( pFanTag ) ) delete pFanTag;
}
static void CreateGate( int flag, const AcDbObjectId& host, const AcGePoint3d& pt, double angle )
{
	Gate* pGate;
	switch( flag )
	{
	case 1:
		pGate = new PermanentGate( pt, angle );  // 永久风门
		break;
	case 2:
		pGate = new TemporaryGate( pt, angle );  // 临时风门
		break;
	case 3:
		pGate = new DoubleGate( pt, angle );    // 双向风门
		break;
	case 4:
		pGate = new BalanceGate( pt, angle ); // 平衡风门
		break;
	default:
		pGate = 0;
	}
	if( pGate == 0 ) return;
	pGate->setRelatedGE( host );
	if( !ArxUtilHelper::PostToModelSpace( pGate ) ) delete pGate;
}
void DrawCmd::DrawGate( int flag )
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择需要添加风门的巷道或者硐室:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "Tunnel" ), objId ) &&
		!ArxUtilHelper::IsEqualType( _T( "ArcTunnel" ), objId ) &&
		!ArxUtilHelper::IsEqualType( _T( "StorageGE" ), objId ) ) return;
	AcGePoint3d pt;
	if( !PromptInsertPt( objId, pt ) ) return;
	double angle;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	CreateGate( flag, objId, pt, angle );
}
// flag = 1 --> 火药库
// flag = 2 --> 机电硐室
// flag = 3 --> 充电室
void DrawCmd::DrawStorage( int flag )
{
    CString name;
    if( flag == 1 ) name = _T( "火药库" );
    else if( flag == 2 ) name = _T( "机电硐室" );
    else if( flag == 3 ) name = _T( "充电室" );
    else return;
    AcGePoint3d startPt, endPt;
    double angle;
    if( !PromptSEPt( name, startPt, endPt, angle ) ) return;
    // 3、创建图元
    MineGE* pGE = 0;
    switch( flag )
    {
    case 1:
        pGE = new PowderStorage( startPt, endPt );  // 火药库
        break;
    case 2:
        pGE = new MachineRoom( startPt, endPt );  // 机电硐室
        break;
    case 3:
        pGE = new ChargeRoom( startPt, endPt );    // 充电室
        break;
    default:
        pGE = 0;
    }
    if( pGE != 0 )
    {
        // 4、初始化并提交到数据库
        if( !ArxUtilHelper::PostToModelSpace( pGE ) ) delete pGE;
    }
}
void DrawCmd::DrawFanTag()
{
	//AcDbObjectIdArray fanIds;
	//DrawHelper::FindMineGEs(_T("LocalFan"),fanIds);
	//int len = fanIds.length();
	//for (int i = 0; i < len; i++)
	//{
	//	FanTagGEDrawed(fanIds[i]);
	//}
}
void DrawCmd::DrawDirection( void )
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一个需要标明方向的图元:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "EdgeGE" ), objId ) ) return;
	AcDbObject* pObj;
	acdbOpenObject( pObj, objId, AcDb::kForRead );
	AcGePoint3d pt;
	if( !PromptInsertPt( objId, pt ) ) return;
	//ArcTunnel *pArcTunnel = ArcTunnel::cast(pObj);
	EdgeGE* pEdge = EdgeGE::cast( pObj );
	//TTunnel* pTTunel = TTunnel::cast(pObj);
	int colorIndx = pEdge->colorIndex();
	//acutPrintf(_T("\n颜色索引:%d"),colorIndx);
	//CheakLayerExit(_T("流动方向"),colorIndx);
	AcGePoint3d spt, ept;
	pEdge->getSEPoint( spt, ept );
    pObj->close();

	AcGeVector3d v = ept - spt;
	double angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
	double length = v.length();
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	//angle = ControlDirByMethods(objId,angle);
	CreateDirection( objId, pt, angle, length * 0.09, colorIndx );
}
void DrawCmd::DrawChimney( void )
{
	//acutPrintf( _T( "\n绘制风筒测试..." ) );
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择风筒所对应的局扇:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "LocalFan" ), objId ) ) return;
	//if( !objIds.isEmpty() )
	//{
	//    AfxMessageBox( _T( "该掘进工作面已设置了风筒!" ) );
	//    return;
	//}
	AcGePoint3dArray pts;
	PolyLineJig jig;
	if( !jig.doJig( pts ) ) return;
	int len = pts.length();
	//acutPrintf( _T( "\n点个数:%d" ), len );
	if( len < 2 ) return;
	Chimney* pChimney = new Chimney();
	pChimney->setRelatedGE( objId );
	for( int i = 0; i < len; i++ ) pChimney->addControlPoint( pts[i] );
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pChimney ) ) delete pChimney;
	AcDbObjectIdArray objIds;
	DrawHelper::GetTagGEById2( objId, _T( "Chimney" ), objIds );
	//将同一局扇相连的风筒合并成一段
	//int numChim = objIds.length();
	//for(int i = 0; i < numChim-1; i++)
	//{
	//	Merging(objIds[i],objIds[i+1]);
	//}
}
void DrawCmd::DrawWindLibrary( void )
{
	//AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一条巷道:" ) );
	//if( objId.isNull() ) return;
	//if( !ArxUtilHelper::IsEqualType( _T( "EdgeGE" ), objId ) ) return;
	//AcGePoint3d pt,insertPt;
	//double angle;
	//if( !ArxUtilHelper::PromptPt( _T( "\n请指定风库的插入点坐标:" ), pt ) ) return;
	//if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	//AcGeVector3d v = AcGeVector3d(AcGeVector3d::kXAxis);
	//v.rotateBy(angle - PI/2,AcGeVector3d::kZAxis);
	//v.normalize();
	//insertPt = pt + v * 60;
	//WindLibrary* pWindLib = new WindLibrary( insertPt, angle );
	//if( pWindLib == 0 ) return;
	//pWindLib->setRelatedGE( objId ); // 关联巷道
	//// 初始化并提交到数据库
	//if( !ArxUtilHelper::PostToModelSpace( pWindLib ) ) delete pWindLib;
}
static void CreateCasement( int flag, const AcDbObjectId& host, const AcGePoint3d& pt, double angle )
{
	Casement* pCasement;
	switch( flag )
	{
	case 1:
		pCasement = new WallCasement( pt, angle );       // 墙调节风窗
		break;
	case 2:
		pCasement = new PermanentCasement( pt, angle );  // 永久调节风窗
		break;
	case 3:
		pCasement = new TemporaryCasement( pt, angle );  // 临时调节风窗
		break;
	default:
		pCasement = 0;
	}
	if( pCasement == 0 ) return;
	// 关联到巷道上
	pCasement->setRelatedGE( host );
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pCasement ) ) delete pCasement;
}
void DrawCmd::DrawCasement( int flag )
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择需要添加调节风窗的巷道:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "EdgeGE" ), objId ) ) return;
	if( ArxUtilHelper::IsEqualType( _T( "TTunnel" ), objId ) ) return;
	AcGePoint3d pt;
	if( !PromptInsertPt( objId, pt ) ) return;
	double angle;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	CreateCasement( flag, objId, pt, angle );
}
static void EffectRanDrawed( AcDbObjectId ttunel )
{
	//AcTransaction *pTrans = actrTransactionManager->startTransaction();
	//if ( 0 == pTrans ) return;
	//AcDbObject *pObj;
	//if (Acad::eOk != pTrans->getObject(pObj,ttunel,AcDb::kForRead)) return;
	//TTunnel *pTTunnel = TTunnel::cast(pObj);
	//if ( 0 == pTTunnel)
	//{
	//	actrTransactionManager->abortTransaction();
	//	return;
	//}
	//AcGePoint3d spt,ept;
	//pTTunnel->getSEPoint(spt,ept);
	//double angle = pTTunnel->getAngle();
	//actrTransactionManager->endTransaction();
	//AcDbObjectIdArray eTags;
	//DrawHelper::GetTagGEById2( ttunel, _T( "EffectRanTagGE" ), eTags );
	//if (!eTags.isEmpty())
	//{
	//	ArxEntityHelper::EraseObjects( eTags, true );
	//}
	//AcGeVector3d v = ept - spt;
	//double diatance = v.length();
	//CString area,way;
	//if(!ExtDictData::Get(ttunel,_T("断面面积"),area)) return;
	//if(!ExtDictData::Get(ttunel,_T("通风方法"),way)) return;
	//double minDistan,maxDistan;
	//if(way.IsEmpty()) return;
	//if(area.IsEmpty()) return;
	//if (_T("压入式") == way || _T("长压短抽") == way)
	//{
	//	minDistan = 4*sqrtf(_tstof(area));
	//	maxDistan = 5*sqrtf(_tstof(area));
	//}
	//else
	//{
	//	minDistan = 0;
	//	maxDistan = 1.5*sqrtf(_tstof(area));
	//}
	//EffectRanTagGE *pTag = new EffectRanTagGE(ept,angle,minDistan,maxDistan,diatance*0.1);
	//if (0 == pTag) return;
	//pTag->setRelatedGE(ttunel);
	//if( !ArxUtilHelper::PostToModelSpace( pTag ) ) delete pTag;
}
void DrawCmd::DrawEffectRanGE()
{
	//AcDbObjectIdArray ttunels;
	//DrawHelper::FindMineGEs(_T("TTunnel"),ttunels);
	//int len = ttunels.length();
	//if(ttunels.isEmpty()) return;
	//for (int i = 0; i < len; i++)
	//{
	//	EffectRanDrawed(ttunels[i]);
	//}
}
void DrawCmd::DrawGasTube( GASTUBETYPE GEName )
{
	int colorIndx;
	CString layerName, lineTypeName;
	GasTube* pGasTube;
	AcGePoint3d startPt, endPt;
	//CString GEName =  _T( "抽采主管" );
	double angle;
	if( !PromptSEPt( layerName, startPt, endPt, angle ) ) return;
	switch( GEName )
	{
	case PD_GAS_TUBE:
		layerName = _T( "永久抽放瓦斯管路" );
		lineTypeName = _T( "FENCELINE1" );
		colorIndx = 1;	//红色
		pGasTube = new MainTube( startPt, endPt );
		break;
	case MPE_GAS_TUBE:
		layerName = _T( "移动泵排瓦斯管路" );
		lineTypeName = _T( "ACAD_ISO03W100" );
		colorIndx = 5;	//蓝色
		pGasTube = new TrunkTube( startPt, endPt );
		break;
	case MPD_GAS_TUBE:
		layerName = _T( "移动泵抽瓦斯管路" );
		lineTypeName = _T( "HOT_WATER_SUPPLY" );
		pGasTube = new BranchTube( startPt, endPt );
		colorIndx = 3;	//绿色
		break;
	default:
		colorIndx = 7;	//白色
		pGasTube = 0;
		delete pGasTube;
		break;
	}
	CheakLayerExit( layerName, colorIndx, AcDb::kLnWt030 );
	pGasTube->setColorIndex( colorIndx );
	pGasTube->setLayer( layerName );
	AddLineType( lineTypeName );
	pGasTube->setLinetype( lineTypeName );
	// 4、初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pGasTube ) ) delete pGasTube;
	//bool ret = ArxUtilHelper::PostToModelSpace( pGasTube );
	//acutPrintf(_T("\n提交状态：%s"),ret ? _T("成功"):_T("失败"));
}
void DrawCmd::DrawGasPump( void )
{
	AcDbObjectId objId;
	if( !CheackTubeLenth( objId ) ) return;
	AcGePoint3d pt;
	double angle;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定瓦斯泵的插入点坐标:" ), pt ) ) return;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	AcGePoint3d spt, ept;
	DrawHelper::GetEdgePoint( objId, spt, ept );
	//SplitByPoint(objId,spt,ept,pt);
	AcGeVector3d v( AcGeVector3d::kXAxis );
	v.rotateBy( angle, AcGeVector3d::kZAxis );
	v.normalize();
	AcGePoint3d pt2 = pt + v * 3;
	GasPump* pGasPump = new GasPump( pt, pt2 );
	if( pGasPump == 0 ) return;
	//pGasPump->
	CString GEName = _T( "瓦斯泵" );
	CheakLayerExit( GEName, 7, AcDb::kLnWt025 );
	pGasPump->setLayer( GEName );
	//pGasPump->setRelatedGE( objId );
	//// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pGasPump ) ) delete pGasPump;
}
void DrawCmd::DrawValve( void )
{
	AcDbObjectId objId;
	if( !CheackTubeLenth( objId ) ) return;
	AcGePoint3d pt, insertPt;
	double angle;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定阀的插入点坐标:" ), pt ) ) return;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	insertPt = pt;
	Valve* pValveGE = new Valve( insertPt, angle );
	if( pValveGE == 0 ) return;
	CString GEName = _T( "阀门" );
	CheakLayerExit( GEName, 7, AcDb::kLnWt025 );
	pValveGE->setLayer( GEName );
	pValveGE->setRelatedGE( objId );
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pValveGE ) ) delete pValveGE;
}
void DrawCmd::DrawTailrace( void )
{
	AcDbObjectId objId;
	if( !CheackTubeLenth( objId ) ) return;
	AcGePoint3d pt, insertPt;
	double angle;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定放水器的插入点坐标:" ), pt ) ) return;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	insertPt = pt;
	Tailrace* pTailrace = new Tailrace( insertPt, angle );
	if( pTailrace == 0 ) return;
	CString GEName = _T( "放水器" );
	CheakLayerExit( GEName, 7, AcDb::kLnWt025 );
	pTailrace->setLayer( GEName );
	pTailrace->setRelatedGE( objId );
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pTailrace ) ) delete pTailrace;
}
void DrawCmd::DrawFlowmeter( void )
{
	AcDbObjectId objId;
	if( !CheackTubeLenth( objId ) ) return;
	AcGePoint3d pt, insertPt;
	double angle;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定流量计的插入点坐标:" ), pt ) ) return;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	insertPt = pt;
	Flowmeter* pFlowmeterGE = new Flowmeter( insertPt, angle );
	if( pFlowmeterGE == 0 ) return;
	CString GEName = _T( "流量计" );
	CheakLayerExit( GEName, 7, AcDb::kLnWt025 );
	pFlowmeterGE->setLayer( GEName );
	pFlowmeterGE->setRelatedGE( objId );
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pFlowmeterGE ) ) delete pFlowmeterGE;
}
void DrawCmd::DrawBackFire( void )
{
	AcDbObjectId objId;
	if( !CheackTubeLenth( objId ) ) return;
	AcGePoint3d pt, insertPt;
	double angle;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定阻火器的插入点坐标:" ), pt ) ) return;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	insertPt = pt;
	BackFire* pBackFireGE = new BackFire( insertPt, angle );
	if( pBackFireGE == 0 ) return;
	CString GEName = _T( "阻火器" );
	CheakLayerExit( GEName, 7, AcDb::kLnWt025 );
	pBackFireGE->setLayer( GEName );
	pBackFireGE->setRelatedGE( objId );
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pBackFireGE ) ) delete pBackFireGE;
}
void DrawCmd::DrawDetermineHole( void )
{
	AcDbObjectId objId;
	if( !CheackTubeLenth( objId ) ) return;
	AcGePoint3d pt, insertPt;
	double angle;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定测定孔的插入点坐标:" ), pt ) ) return;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	insertPt = pt;
	DetermineHole* pDetermineHole = new DetermineHole( insertPt, angle );
	if( pDetermineHole == 0 ) return;
	CString GEName = _T( "测定孔" );
	CheakLayerExit( GEName, 7, AcDb::kLnWt025 );
	pDetermineHole->setLayer( GEName );
	pDetermineHole->setRelatedGE( objId );
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pDetermineHole ) ) delete pDetermineHole;
}
static void CreateGasFlow( const AcDbObjectId& host, const AcGePoint3d& pt, double angle )
{
	//GasFlowTagGE* pGasFlowTagGE = new GasFlowTagGE( pt, angle );
	//// 关联到巷道上
	//pGasFlowTagGE->setRelatedGE( host );
	//// 初始化并提交到数据库
	//if( !ArxUtilHelper::PostToModelSpace( pGasFlowTagGE ) ) delete pGasFlowTagGE;
	////acutPrintf(_T("\nobjId:%d"),pGasFlowTagGE->objectId());
	////CString vv;
	////ExtDictData::Get(pGasFlowTagGE->objectId(),_T("瓦斯流量"),vv);
	////acutPrintf(_T("\n瓦斯流量值:%s"),vv);
}
void DrawCmd::DrawGasFlow()
{
	//AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一条管路:" ) );
	//if( objId.isNull() ) return;
	//if( !ArxUtilHelper::IsEqualType( _T( "GasTube" ), objId ) ) return;
	//AcGePoint3d pt;
	//if( !PromptInsertPt( objId, pt ) ) return;
	//double angle;
	//if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	//CreateGasFlow( objId, pt, angle );
	////CString v;
	////ExtDictData::Get(objId,_T("瓦斯流量"),v);
	////acutPrintf(_T("\n值:%s"),v);
}
void DrawCmd::DrawDrill( void )
{
	AcGePoint3d startPt, endPt;
	double angle;
	CString GEName = _T( "钻孔" );
	if( !PromptSEPt( GEName, startPt, endPt, angle ) ) return;
	// 3、创建图元
	DrillGE* pDrillGE = new DrillGE( startPt, endPt );
	CheakLayerExit( GEName, 7, AcDb::kLnWt025 );
	pDrillGE->setLayer( GEName );
	AddLineType( _T( "DASHED2" ) );
	pDrillGE->setLinetype( _T( "DASHED2" ) );
	pDrillGE->setLineWeight( AcDb::kLnWt025 );
	// 4、初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pDrillGE ) ) delete pDrillGE;
}
static void CreateSensor( int flag, const AcDbObjectId& host, const AcGePoint3d& pt, double angle )
{
	Sensor* pSensor;
	switch( flag )
	{
	case 0:
		pSensor = new CommonSensor( pt, angle );       // 通用传感器
		break;
	case 1:
		pSensor = new TempeSensor( pt, angle );       // 温度传感器
		//pSensor->setColor(6);	//紫色
		break;
	case 2:
		pSensor = new FlowSensor( pt, angle );       // 流量传感器
		//pSensor->setColorIndex(6);	//紫色
		break;
	case 3:
		pSensor = new PressSensor( pt, angle );       // 压力传感器
		//pSensor->setColor(4);	//青色
		break;
	case 4:
		pSensor = new CH4Sensor( pt, angle );       // 瓦斯浓度传感器
		//pSensor->setColor(3);	//绿色
		break;
	case 5:
		pSensor = new COSensor( pt, angle );       // CO浓度传感器
		//pSensor->setColor(1);	//红色
		break;
	case 6:
		pSensor = new VentSensor( pt, angle );       // 速度传感器
		break;
	case 7:
		pSensor = new DifferPressSensor( pt, angle );       // 压差传感器
		break;
	default:
		pSensor = 0;
	}
	if( pSensor == 0 ) return;
	// 关联到巷道上
	pSensor->setRelatedGE( host );
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pSensor ) ) delete pSensor;
}
void DrawCmd::DrawSensor( int flag )
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择需要添加传感器的地点(巷道、工作面、抽采管路):" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "EdgeGE" ), objId ) ) return;
	AcGePoint3d pt, insertpt;
	if( !PromptInsertPt( objId, pt ) ) return;
	insertpt = pt;
	double angle;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	CreateSensor( flag, objId, insertpt, angle );
}
void DrawCmd::DrawAnemometer( void )
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一条巷道:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "Tunnel" ), objId ) &&
		!ArxUtilHelper::IsEqualType( _T( "ArcTunnel" ), objId ) &&
		!ArxUtilHelper::IsEqualType( _T( "TTunnel" ), objId ) ) return;
	AcGePoint3d pt, insertPt;
	double angle;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定测风站的插入点坐标:" ), pt ) ) return;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	insertPt = pt;
	Anemometer* pAnemometer = new Anemometer( insertPt, angle );
	if( pAnemometer == 0 ) return;
	pAnemometer->setRelatedGE( objId ); // 关联巷道
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pAnemometer ) ) delete pAnemometer;
}
void DrawCmd::DrawCDH()
{
	ads_point _pt;
	if( acedGetPoint( NULL, _T( "\n请选择插入点坐标：" ), _pt ) != RTNORM )
	{
		acutPrintf( _T( "\n选择点坐标失败" ) );
		return;
	}
	AcGePoint3d pt( _pt[X], _pt[Y], _pt[Z] );
	CDH* pCDH = new CDH( pt );
	if( !ArxUtilHelper::PostToModelSpace( pCDH ) ) delete pCDH;
}
void DrawCmd::DrawCDH2()
{
	ads_point _pt;
	if( acedGetPoint( NULL, _T( "\n请选择插入点坐标：" ), _pt ) != RTNORM )
	{
		acutPrintf( _T( "\n选择点坐标失败" ) );
		return;
	}
	AcGePoint3d pt( _pt[X], _pt[Y], _pt[Z] );
	AcStringArray allBlocks;
	CString dwgFile = PathHelper::BuildPath( ARX_FILE_PATH( _T( "" ) ), BLOCK_DWG_NAME );
	ArxDwgHelper::GetAllBlockNames( dwgFile, allBlocks );
	for ( int i = 0; i < allBlocks.length(); i++ )
	{
		acutPrintf( _T( "\n第%d个块名称:%s" ), i + 1, allBlocks[i].kACharPtr() );
	}
	CustomGE* pCDH = new CustomGE( allBlocks[14].kACharPtr()/*_T("CDH")*/ );
	pCDH->set( pt );
	if( !ArxUtilHelper::PostToModelSpace( pCDH ) ) delete pCDH;
}
static void CreateAirLeakage( const AcDbObjectId& host, const AcGePoint3d& pt, double angle, bool isSource )
{
	AirLeakage* pAirLeakage;
	if ( isSource )
	{
		pAirLeakage = new AirSource( pt, angle );
	}
	else
	{
		pAirLeakage = new AirIn( pt, angle );
	}
	if( pAirLeakage == 0 ) return;
	pAirLeakage->setRelatedGE( host );
	if( !ArxUtilHelper::PostToModelSpace( pAirLeakage ) ) delete pAirLeakage;
}
void DrawCmd::DrawAirLeakage( bool isSource )
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择需要添加漏风点的巷道:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "Tunnel" ), objId ) &&
		!ArxUtilHelper::IsEqualType( _T( "ArcTunnel" ), objId ) ) return;
	AcGePoint3d pt;
	if( !PromptInsertPt( objId, pt ) ) return;
	double angle;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	CreateAirLeakage( objId, pt, angle, isSource );
}
static void CreateWall( int flag, const AcDbObjectId& host, const AcGePoint3d& pt, double angle )
{
	Wall* pWall;
	switch( flag )
	{
	case 1:
		pWall = new PermanentWall( pt, angle );  // 永久挡风墙
		break;
	case 2:
		pWall = new TemporaryWall( pt, angle );  // 临时挡风墙
		break;
	case 3:
		pWall = new Firewall( pt, angle );  // 防火挡风墙
		break;
	default:
		pWall = 0;
	}
	if( pWall == 0 ) return;
	// 关联到巷道上
	pWall->setRelatedGE( host );
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pWall ) ) delete pWall;
}
void DrawCmd::DrawWall( int flag )
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择需要增加风墙（密闭）的巷道:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "EdgeGE" ), objId ) ) return;
	AcGePoint3d pt;
	if( !PromptInsertPt( objId, pt ) ) return;
	double angle;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	CreateWall( flag, objId, pt, angle );
}
static void CreateWindBridge( const AcDbObjectId& host, const AcGePoint3d& pt, double angle )
{
	WindBridge* pBridge = new WindBridge( pt, angle );
	pBridge->setRelatedGE( host ); // 关联图元
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pBridge ) ) delete pBridge;
}
void DrawCmd::DrawWindBridge()
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择需要增加风桥的巷道:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "EdgeGE" ), objId ) ) return;
	AcGePoint3d pt;
	if( !PromptInsertPt( objId, pt ) ) return;
	double angle;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	CreateWindBridge( objId, pt, angle ); // 风桥
}
static void CreateGoaf( const AcGePoint3dArray& polygon )
{
	Goaf* pGoaf = new Goaf();
	for( int i = 0; i < polygon.length(); i++ )
	{
		pGoaf->addPoint( polygon[i] );
	}
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pGoaf ) ) delete pGoaf;
}
static bool GetPolygonByJig( AcGePoint3dArray& polygon )
{
	//PolyLineJig jig;
	PolyLineJig jig;
	return jig.doJig( polygon );
}
void DrawCmd::DrawGoaf()
{
	acutPrintf( _T( "\n请指定采空区封闭区域: " ) );
	// jig获取封闭多边形
	AcGePoint3dArray polygon;
	if( !GetPolygonByJig( polygon ) ) return;
	if( polygon.length() < 3 )
	{
		acutPrintf( _T( "\n绘制采空区至少需要指定3个坐标..." ) );
	}
	else
	{
		// 创建采空区
		CreateGoaf( polygon );
	}
}
void DrawCmd::DrawInclined( void )
{
	AcGePoint3d startPt, endPt;
	double angle;
	if( !PromptSEPt( _T( "斜井" ), startPt, endPt, angle ) ) return;
	Inclined* pInclined = new Inclined( startPt, endPt );
	if( !ArxUtilHelper::PostToModelSpace( pInclined ) ) delete pInclined;
}
void DrawCmd::DrawShaft( void )
{
	ads_point _pt;
	if( acedGetPoint( NULL, _T( "\n请选择插入点坐标：" ), _pt ) != RTNORM )
	{
		acutPrintf( _T( "\n选择点坐标失败" ) );
		return;
	}
	AcGePoint3d pt( _pt[X], _pt[Y], _pt[Z] );
	Shaft* pShaft = new Shaft( pt );
	if( !ArxUtilHelper::PostToModelSpace( pShaft ) ) delete pShaft;
}
void DrawCmd::DrawCoalhole( void )
{
	ads_point _pt;
	if( acedGetPoint( NULL, _T( "\n请选择插入点坐标：" ), _pt ) != RTNORM )
	{
		acutPrintf( _T( "\n选择点坐标失败" ) );
		return;
	}
	AcGePoint3d pt( _pt[X], _pt[Y], _pt[Z] );
	Coalhole* pCoalhole = new Coalhole( pt );
	if( !ArxUtilHelper::PostToModelSpace( pCoalhole ) ) delete pCoalhole;
}
void DrawCmd::DrawNoCoalhole( void )
{
	ads_point _pt;
	if( acedGetPoint( NULL, _T( "\n请选择插入点坐标：" ), _pt ) != RTNORM )
	{
		acutPrintf( _T( "\n选择点坐标失败" ) );
		return;
	}
	AcGePoint3d pt( _pt[X], _pt[Y], _pt[Z] );
	NoCoalhole* pCoalhole = new NoCoalhole( pt );
	if( !ArxUtilHelper::PostToModelSpace( pCoalhole ) ) delete pCoalhole;
}
void DrawCmd::DrawWaterDrill( void )
{
	ads_point _pt;
	if( acedGetPoint( NULL, _T( "\n请选择插入点坐标：" ), _pt ) != RTNORM )
	{
		acutPrintf( _T( "\n选择点坐标失败" ) );
		return;
	}
	AcGePoint3d pt( _pt[X], _pt[Y], _pt[Z] );
	WaterDrill* pWaterDrill = new WaterDrill( pt );
	if( !ArxUtilHelper::PostToModelSpace( pWaterDrill ) ) delete pWaterDrill;
}
void DrawCmd::DrawLeakDrill( void )
{
	ads_point _pt;
	if( acedGetPoint( NULL, _T( "\n请选择插入点坐标：" ), _pt ) != RTNORM )
	{
		acutPrintf( _T( "\n选择点坐标失败" ) );
		return;
	}
	AcGePoint3d pt( _pt[X], _pt[Y], _pt[Z] );
	LeakDrill* pLeakDrill = new LeakDrill( pt );
	if( !ArxUtilHelper::PostToModelSpace( pLeakDrill ) ) delete pLeakDrill;
}
void DrawCmd::DrawWaterPumpDrill( void )
{
	ads_point _pt;
	if( acedGetPoint( NULL, _T( "\n请选择插入点坐标：" ), _pt ) != RTNORM )
	{
		acutPrintf( _T( "\n选择点坐标失败" ) );
		return;
	}
	AcGePoint3d pt( _pt[X], _pt[Y], _pt[Z] );
	WaterPumpDrill* pWaterPumpDrill = new WaterPumpDrill( pt );
	if( !ArxUtilHelper::PostToModelSpace( pWaterPumpDrill ) ) delete pWaterPumpDrill;
}
void DrawCmd::DrawMachineStation()
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一条巷道:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "EdgeGE" ), objId ) ) return;
	if( ArxUtilHelper::IsEqualType( _T( "TTunnel" ), objId ) )
	{
		AfxMessageBox( _T( "掘进巷道不能设置机站!" ) );
		return;
	}
	AcDbObjectIdArray objIds;
	DrawHelper::GetTagGEById2( objId, _T( "MachineStation" ), objIds );
	if( !objIds.isEmpty() )
	{
		AfxMessageBox( _T( "该巷道已设置了机站!" ) );
		return;
	}
	AcGePoint3d pt, insertPt;
	double angle;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定机站的插入点坐标:" ), pt ) ) return;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	insertPt = pt;
	MachineStation* pMachineStation = new MachineStation( insertPt, angle );
	if( pMachineStation == 0 ) return;
	pMachineStation->setRelatedGE( objId ); // 关联巷道
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pMachineStation ) ) delete pMachineStation;
}
void DrawCmd::DrawAirCurtain( void )
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一条巷道:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "EdgeGE" ), objId ) ) return;
	AcGePoint3d pt, insertPt;
	double angle;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定空气幕的插入点坐标:" ), pt ) ) return;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	insertPt = pt;
	AirCurtain* pAirC = new AirCurtain( pt, angle );
	if( 0 == pAirC ) return;
	pAirC->setRelatedGE( objId ); // 关联图元
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pAirC ) ) delete pAirC;
}
void DrawCmd::DrawWaterInrushPt( void )
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一条巷道:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "EdgeGE" ), objId ) ) return;
	AcGePoint3d pt, insertPt;
	double angle;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定突水点的插入点坐标:" ), pt ) ) return;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	insertPt = pt;
	WaterInrushPt* pWaterInrushPt = new WaterInrushPt( pt, angle );
	if( 0 == pWaterInrushPt ) return;
	pWaterInrushPt->setRelatedGE( objId ); // 关联图元
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pWaterInrushPt ) ) delete pWaterInrushPt;
}
void DrawCmd::DrawDiffPressGauge( void )
{
	ads_point _pt;
	if( acedGetPoint( NULL, _T( "\n请选择插入点坐标：" ), _pt ) != RTNORM )
	{
		acutPrintf( _T( "\n选择点坐标失败" ) );
		return;
	}
	AcGePoint3d pt( _pt[X], _pt[Y], _pt[Z] );
	DiffPressGauge* pDiffPressGauge = new DiffPressGauge( pt );
	if( !ArxUtilHelper::PostToModelSpace( pDiffPressGauge ) ) delete pDiffPressGauge;
}
void DrawCmd::DrawAddPower( void )
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一条巷道:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "EdgeGE" ), objId ) ) return;
	AcGePoint3d pt, insertPt;
	double angle;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定附加动力的插入点坐标:" ), pt ) ) return;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	insertPt = pt;
	AddPower* pAddPower = new AddPower( pt, angle );
	if( 0 == pAddPower ) return;
	pAddPower->setRelatedGE( objId ); // 关联图元
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pAddPower ) ) delete pAddPower;
}
void DrawCmd::DrawLocalResis( void )
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一条巷道:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "EdgeGE" ), objId ) ) return;
	AcGePoint3d pt, insertPt;
	double angle;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定局部阻力的插入点坐标:" ), pt ) ) return;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	insertPt = pt;
	LocalResis* pLocalResis = new LocalResis( pt, angle );
	if( 0 == pLocalResis ) return;
	pLocalResis->setRelatedGE( objId ); // 关联图元
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pLocalResis ) ) delete pLocalResis;
}
void DrawCmd::DrawHeatSource( void )
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一条巷道:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "EdgeGE" ), objId ) ) return;
	AcGePoint3d pt, insertPt;
	double angle;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定热源的插入点坐标:" ), pt ) ) return;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	insertPt = pt;
	HeatSource* pHS = new HeatSource( pt, angle );
	if( 0 == pHS ) return;
	pHS->setRelatedGE( objId ); // 关联图元
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pHS ) ) delete pHS;
}
void DrawCmd::DrawPitotTube( void )
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一条巷道:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "EdgeGE" ), objId ) ) return;
	AcGePoint3d pt, insertPt;
	double angle;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定皮托管的插入点坐标:" ), pt ) ) return;
	if( !GetClosePtAndAngle( objId, pt, angle ) ) return;
	insertPt = pt;
	PitotTube* pPitotTube = new PitotTube( pt, angle );
	if( 0 == pPitotTube ) return;
	pPitotTube->setRelatedGE( objId ); // 关联图元
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pPitotTube ) ) delete pPitotTube;
}
void DrawCmd::DrawHoses( void )
{
	AcGePoint3dArray pts;
	PolyLineJig jig;
	if( !jig.doJig( pts ) ) return;
	int len = pts.length();
	if( len < 2 ) return;
	Hoses* pHoses = new Hoses();
	for( int i = 0; i < len; i++ ) pHoses->addControlPoint( pts[i] );
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pHoses ) ) delete pHoses;
}
void DrawCmd::DrawJoint()
{
	AcGePoint3d pt;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定插入点坐标:" ), pt ) ) return;
	JointGE* pJointGE = new JointGE( pt );
	if( 0 == pJointGE ) return;
	// 初始化并提交到数据库
	if( !ArxUtilHelper::PostToModelSpace( pJointGE ) ) delete pJointGE;
}
void DrawCmd::DrawBlockGE()
{
	CAcModuleResourceOverride resOverride;    //防止资源冲突
	SelBlockDlg dlg;
	if( IDOK != dlg.DoModal() ) return;
	//CString dataDirName = _T( "" );
	// 块定义文件路径
	//CString dwgFile = PathHelper::BuildPath(ARX_FILE_PATH(dataDirName), BLOCK_DWG_NAME);
	// 获取所有的块名称
	//AcStringArray blkNames;
	//ArxDwgHelper::GetAllBlockNames(dwgFile, blkNames);
	//if(blkNames.isEmpty())
	//{
	//	acutPrintf(_T("\n没有块定义"));
	//	return;
	//}
	//// 打印所有的块名称列表
	//acutPrintf(_T("\n自定义图元列表:"));
	//int n = blkNames.length();
	//for(int i=0;i<n;i++)
	//{
	//	acutPrintf(_T("\n%d:%s"), i, blkNames[i].kACharPtr());
	//}
	//// 让用户选择哪一个
	//int k = -1;
	//if(RTNORM != acedGetInt(_T("\n请选择要绘制的图元编号:"), &k)) return;
	int k = dlg.m_blockList.GetCurSel();
	if( k < 0 /*|| k >= n*/ ) return;
	AcGePoint3d pt;
	if( !ArxUtilHelper::GetPoint( _T( "\n请指定图元插入点坐标:" ), pt ) ) return;
	// 绘制
	CustomGE* pGE = new CustomGE( /*blkNames[k].kACharPtr()*/dlg.m_curBlockName, pt );
	if( !ArxUtilHelper::PostToModelSpace( pGE ) ) delete pGE;
}
//创建一个简单的块定义(个半径为10的圆，再加上一个文字编号)
static void CreateSimpleBlock( AcDbDatabase* pDB, const CString& blkName, int num )
{
	//获得当前图形数据库的块表
	AcDbBlockTable* pBlkTbl;
	pDB->getBlockTable( pBlkTbl, AcDb::kForWrite );
	//创建新的块表记录
	AcDbBlockTableRecord* pBlkTblRcd;
	pBlkTblRcd = new AcDbBlockTableRecord();
	pBlkTblRcd->setName( blkName );
	//将块表记录添加到块表中
	AcDbObjectId blkDefId;
	pBlkTbl->add( blkDefId, pBlkTblRcd );
	pBlkTbl->close();
	//向块表记录中添加实体
	//画一个半径为10的圆，再加上一个文字编号
	AcDbCircle* pCircle = new AcDbCircle( AcGePoint3d::kOrigin, AcGeVector3d::kZAxis, 10 );
	AcDbMText* pMText = new AcDbMText;
	//AcDbObjectIdstyle;//文字样式
	//pMText->setTextStyle(style);
	pMText->setLocation( AcGePoint3d::kOrigin );
	pMText->setRotation( 0 );
	//pMText->setWidth(width);//不设置宽度，自动调整
	pMText->setTextHeight( 10 );
	pMText->setAttachment( AcDbMText::kMiddleMid ); //默认居中
	CString text;
	text.Format( _T( "%d" ), num );
	pMText->setContents( text );
	AcDbObjectId entId;
	pBlkTblRcd->appendAcDbEntity( entId, pCircle );
	pBlkTblRcd->appendAcDbEntity( entId, pMText );
	//关闭实体和块表记录
	pMText->close();
	pCircle->close();
	pBlkTblRcd->close();
}
// 连续增加多个简单的块定义
// startNum -- 起始编号
static void AddBlocksToDwgFile( const AcStringArray& blkNames, int startNum, const CString& dwgFile )
{
	// 打开dwg文件，并新建数据库
	AcDbDatabase db( false );
	if( Acad::eOk != db.readDwgFile( dwgFile ) ) return ;
	// 已有的块定义个数
	int n = blkNames.length();
	for( int i = 0; i < n; i++ )
	{
		CreateSimpleBlock( &db, blkNames[i].kACharPtr(), startNum++ );
	}
	// 保存到dwg文件
	db.saveAs( dwgFile );
}
void DrawCmd::AddNewBlock()
{
	CString dataDirName = _T( "" );
	// 块定义文件路径
	CString dwgFile = PathHelper::BuildPath( ARX_FILE_PATH( dataDirName ), BLOCK_DWG_NAME );
	// 查找已存在的块定义
	AcStringArray existBlkNames;
	ArxDwgHelper::GetAllBlockNames( dwgFile, existBlkNames );
	// 连续输入多个块名称
	AcStringArray blkNames;
	ACHAR tt[50];
	while( RTNORM == acedGetString( 0, _T( "\n新增图元类型名称:" ), tt ) )
	{
		CString name( tt );
		name.Trim();
		if( name == _T( "" ) ) break;;
		blkNames.append( tt );
	}
	if( blkNames.isEmpty() ) return;
	// 指定起始编号
	int startNum = -1;
	if( RTNORM != acedGetInt( _T( "\n指定新增图元起始编号:" ), &startNum ) ) return;
	if( startNum < 0 ) return;
	// 新增块定义到dwg文件
	AddBlocksToDwgFile( blkNames, startNum, dwgFile );
	// 将dwg的块定义合并到当前图形数据库中
	if( ArxDwgHelper::MergeBlocks( dwgFile ) )
	{
		acutPrintf( _T( "\n更新块成功!\n" ) );
		ArxDwgHelper::UpdateDwg(); // 更新图形
	}
	else
	{
		acutPrintf( _T( "\n更新块失败!\n" ) );
	}
}
void DrawCmd::AdvanceTT()
{
	AcDbObjectIdArray objIds;
	DrawHelper::FindMineGEs(_T("TTunnel"),objIds);
	for (int i = 0; i < objIds.length();i++)
	{
		UpdateTTAdvance(objIds[i]);
	}
}
void DrawCmd::AdvanceTTByUser()
{
	AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一个掘进工作面:" ) );
	if( objId.isNull() ) return;
	if( !ArxUtilHelper::IsEqualType( _T( "TTunnel" ), objId ) ) return;
	COleDateTime oldTime,oldTime1,curTime;
	curTime = COleDateTime::GetCurrentTime();
	if(!ExtDictData::GetDateTime(objId,_T("掘进.更新日期"),oldTime)) oldTime = curTime;
	if(!ExtDictData::GetDateTime(objId,_T("掘进.上次更新日期"),oldTime1)) oldTime1 = curTime;
	double oldLength;
	if(!ExtDictData::GetDouble(objId,_T("掘进.推进长度"),oldLength)) oldLength = 0;
	AdvanceDlg dlg;
	dlg.m_stTime = oldTime1.Format( _T( "%Y年%m月%d日" ) );
	dlg.m_edTime = oldTime.Format( _T( "%Y年%m月%d日" ) );
	dlg.m_length = oldLength;
	double length = 0;
	if (IDOK == dlg.DoModal())
	{
		length = dlg.m_length;
	}
	UpdateTTAdvanceByUser(objId,length);
}
void DrawCmd::CopyDatas()
{
	AcDbObjectId objId1 = ArxUtilHelper::SelectEntity( _T( "选择源图元(source):" ) );
	if( objId1.isNull() ) return;
	AcDbObjectId objId2 = ArxUtilHelper::SelectEntity( _T( "选择目标图元(target):" ) );
	if( objId2.isNull() ) return;
	acutPrintf(_T("\n数据复制%s！！！"),ExtDictData::Copy(objId1,objId2)?_T("成功"):_T("失败"));
}