#pragma once
#include "JunctionEdgeInfo.h"
#include "VentGE/Tunnel.h"
#include "VentGE/WorkSurface.h"
#include "VentGE/Goaf.h"

class AdvanceWSHelper
{
public:
    AdvanceWSHelper( const AcDbObjectId ws_objId,
             JunctionEdgeInfoArray& sptLines,
             JunctionEdgeInfoArray& eptLines,
             const AcDbObjectId& goaf_objId,
             int p1, int p2 );
    virtual ~AdvanceWSHelper();
	bool autoDo();
	bool advanceByUser(double length);
    bool update();

private:
	AcDbObjectId m_id,m_goafId;
    // 工作面
    //WorkSurface* m_pWS;
    // 始点关联的巷道
    AcDbVoidPtrArray m_pSptLines;
    // 末点关联的巷道
    AcDbVoidPtrArray m_pEptLines;
    // 关联的采空区
    //Goaf* m_pGoaf;
    // 关联巷道信息
    JunctionEdgeInfoArray& m_sptLinesInfo;
    JunctionEdgeInfoArray& m_eptLinesInfo;
    // 关联的采空区信息
    AcGePoint3dArray m_polygon;
    int m_p1, m_p2;
    AcGePoint3d m_spt, m_ept;
    AcGeVector3d m_dir;
    AcGePoint3d m_basePt;
    double m_dist;
};
