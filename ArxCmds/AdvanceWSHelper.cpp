#include "stdafx.h"
#include "AdvanceWSHelper.h"
#include "DumyJigEntity.h"
#include "ArxHelper/HelperClass.h"
#include "MineGE/HelperClass.h"

#define EPLISON 0.001

extern bool GetWSDirPt( const AcDbObjectId& objId, AcGePoint3d& dirPt );

AdvanceWSHelper::AdvanceWSHelper(
    const AcDbObjectId ws_objId,
    JunctionEdgeInfoArray& sptLinesInfo,
    JunctionEdgeInfoArray& eptLinesInfo,
    const AcDbObjectId& goaf_objId,
    int p1, int p2 )
    : m_sptLinesInfo( sptLinesInfo ),
      m_eptLinesInfo( eptLinesInfo ),
      m_dist( 0 ), m_p1( p1 ), m_p2( p2 ),
	  m_goafId( goaf_objId ),m_id( ws_objId )
{

}
AdvanceWSHelper::~AdvanceWSHelper()
{
  
}
static AcGeVector3d CaclDir( const AcGePoint3d& spt, const AcGePoint3d& ept, const AcGePoint3d& dirPt )
{
    if( dirPt == spt || dirPt == ept ) return AcGeVector3d::kIdentity;
    AcGeVector3d v = ept - spt;
    v.normalize();
    v.rotateBy( 0.5 * PI, AcGeVector3d::kZAxis );
    AcGeVector3d offset = dirPt - spt;          // 或 dirPt - ept
    double L = offset.dotProduct( v );          // 计算向量offset在向量v上的投影
    if( L == 0 ) return AcGeVector3d::kIdentity; // 返回零向量
    if( L < 0 )
    {
        v.negate();
    }
    return v;
}

static bool UpdateGoaf(const AcDbObjectId& goaf_objId, int pos1,const AcGePoint3d& spt, int pos2, const AcGePoint3d& ept)
{
	AcTransaction* pTrans = actrTransactionManager->startTransaction();
	if( pTrans == 0 ) return false;
	AcDbObject* pObj;
	if( Acad::eOk != pTrans->getObject( pObj, goaf_objId, AcDb::kForWrite ) )
	{
		actrTransactionManager->abortTransaction();
		return false;
	}
	Goaf* pGoaf = Goaf::cast( pObj );
	if( pGoaf == 0 )
	{
		actrTransactionManager->abortTransaction();
		return false;
	}
	pGoaf->setPoint( pos1, spt );
	pGoaf->setPoint( pos2, ept );
	pGoaf->update();
	actrTransactionManager->endTransaction();
	return true;
}

static bool UpdateSEptLines(JunctionEdgeInfoArray& sptLinesInfo, JunctionEdgeInfoArray& eptLinesInfo,const AcGePoint3d& newSpt,const AcGePoint3d& newEpt)
{
	int m = sptLinesInfo.length(), n = eptLinesInfo.length();

	for( int i = 0; i < m; i++ )
	{
		AcTransaction* pTrans = actrTransactionManager->startTransaction();
		if( pTrans == 0 ) return false;
		AcDbObject* pObj;
		if( Acad::eOk != pTrans->getObject( pObj, sptLinesInfo[i].objId, AcDb::kForWrite ) )
		{
			actrTransactionManager->abortTransaction();
			return false;
		}
		Tunnel* pTunnel = Tunnel::cast( pObj );
		if( pTunnel == 0 )
		{
			actrTransactionManager->abortTransaction();
			return false;
		}
		bool startOrEnd = sptLinesInfo[i].startOrEnd;
		AcGePoint3d p1, p2;
		pTunnel->getSEPoint( p1, p2 );
		if( startOrEnd )
		{
			pTunnel->setSEPoint( newSpt, p2 );
		}
		else
		{
			pTunnel->setSEPoint( p1, newSpt );
		}
		pTunnel->update();
		actrTransactionManager->endTransaction();
	}
	for( int i = 0; i < n; i++ )
	{
		AcTransaction* pTrans = actrTransactionManager->startTransaction();
		if( pTrans == 0 ) return false;
		AcDbObject* pObj;
		if( Acad::eOk != pTrans->getObject( pObj, eptLinesInfo[i].objId, AcDb::kForWrite ) )
		{
			actrTransactionManager->abortTransaction();
			return false;
		}
		Tunnel* pTunnel = Tunnel::cast( pObj );
		if( pTunnel == 0 )
		{
			actrTransactionManager->abortTransaction();
			return false;
		}
		bool startOrEnd = eptLinesInfo[i].startOrEnd;
		AcGePoint3d p1, p2;
		pTunnel->getSEPoint( p1, p2 );
		if( startOrEnd )
		{
			pTunnel->setSEPoint( newEpt, p2 );
		}
		else
		{
			pTunnel->setSEPoint( p1, newEpt );
		}
		pTunnel->update();
		actrTransactionManager->endTransaction();
	}
	return true;
}

bool AdvanceWSHelper::update()
{
    AcGePoint3d newSpt = m_spt + m_dir * m_dist, newEpt = m_ept + m_dir * m_dist;
    // 更新工作面坐标
	DrawHelper::SetEdgePoint(m_id,newSpt,newEpt);
    // 更新关联始末点的巷道
	UpdateSEptLines(m_sptLinesInfo,m_eptLinesInfo,newSpt,newEpt);
    // 更改关联的采空区
    if( !m_goafId.isNull() )
    {
        UpdateGoaf(m_goafId,m_p1,newSpt,m_p2,newEpt);
    }
    return Adesk::kTrue;
}

bool AdvanceWSHelper::autoDo()
{
	AcGePoint3d dirPt;
	GetWSDirPt(m_id,dirPt);
	DrawHelper::GetEdgePoint(m_id,m_spt,m_ept);
	// 计算方向向量
	m_dir = CaclDir( m_spt, m_ept, dirPt );
	if( m_dir.isZeroLength() ) return false;
	
	m_basePt = m_spt;
	double speed;
	if(!ExtDictData::GetDouble(m_id,_T("推进.日进尺数"),speed)) return false;
	//acutPrintf(_T("\n推进日进尺数:%.2lf"),speed);
	COleDateTime oldTime,curTime;
	COleDateTimeSpan timeSpan;
	curTime = COleDateTime::GetCurrentTime();
	if(!ExtDictData::GetDateTime(m_id,_T("推进.更新日期"),oldTime)) oldTime = curTime;
	//if(!ExtDictData::GetDateTime(m_id,_T("推进.更新日期"),tmpTime)) tmpTime = curTime;

	timeSpan = curTime - oldTime;
	double days = timeSpan.GetTotalDays();

	//days = 2;
	//acutPrintf(_T("\n推进天数:%.lf"),days);

	if( days < EPLISON ) return false;
	m_dist = speed*days;
	if(!update()) return false;
	ExtDictData::SetDouble(m_id,_T("推进.推进长度"),m_dist);
	//acutPrintf(_T("\n推进长度:%.2lf"),m_dist);
	ExtDictData::SetDateTime(m_id,_T("推进.更新日期"),curTime);
	ExtDictData::SetDateTime(m_id,_T("推进.上次更新日期"),oldTime);
	return true;
}

bool AdvanceWSHelper::advanceByUser(double length)
{
	AcGePoint3d dirPt;
	GetWSDirPt(m_id,dirPt);
	DrawHelper::GetEdgePoint(m_id,m_spt,m_ept);
	// 计算方向向量
	m_dir = CaclDir( m_spt, m_ept, dirPt );
	if( m_dir.isZeroLength() ) return false;

	m_basePt = m_spt;
	COleDateTime oldTime,curTime;
	COleDateTimeSpan timeSpan;
	curTime = COleDateTime::GetCurrentTime();
	if(!ExtDictData::GetDateTime(m_id,_T("推进.更新日期"),oldTime)) oldTime = curTime;
	double oldLength;
	if(!ExtDictData::GetDouble(m_id,_T("推进.推进长度"),oldLength)) oldLength = 0;
	m_dist = length - oldLength;
	
	if(!update()) return false;
	if(abs(m_dist) < EPLISON) return false;
	ExtDictData::SetDouble(m_id,_T("推进.推进长度"),length);
	//acutPrintf(_T("\n推进长度:%.2lf"),m_dist);
	ExtDictData::SetDateTime(m_id,_T("推进.更新日期"),curTime);
	ExtDictData::SetDateTime(m_id,_T("推进.上次更新日期"),oldTime);
	return true;
}
