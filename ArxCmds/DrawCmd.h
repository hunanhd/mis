#pragma once
enum GASTUBETYPE
{
    PD_GAS_TUBE	 = 0,	//永久抽放瓦斯管路
    MPE_GAS_TUBE = 1,	//移动泵排瓦斯管路
    MPD_GAS_TUBE = 2	//移动泵抽瓦斯管路
};
class DrawCmd
{
public:
    /* 实现在DrawCmd.cpp中 */
    static void DrawTunnel( void );
    static void DrawArcTunnel( void );
    static void DrawWS( void );
    static void DrawJoint();
    static void DrawTTunnel();
    static void ResetInTunnel();
    static void ShowInTunnel();
    static void DrawMainFan();
    static void DrawLocalFan();
    static void DrawMachineStation();//绘制机站，机站图元效果与主扇一致
    static void DrawFanTag();
    static void DrawGate( int flag );
    static void DrawCasement( int flag );
    static void DrawWindBridge();
    static void DrawWall( int flag );
    static void DrawStorage( int flag );
    static void DrawDirection( void );
    static void DrawWindLibrary( void );
    //绘制空气幕
    static void DrawAirCurtain( void );
    //绘制突水点
    static void DrawWaterInrushPt( void );
    //绘制水柱计
    static void DrawDiffPressGauge( void );
    //绘制附加动力
    static void DrawAddPower( void );
    //绘制局部阻力
    static void DrawLocalResis( void );
    //绘制热源
    static void DrawHeatSource( void );
    //绘制皮托管
    static void DrawPitotTube( void );
    //绘制采空区
    static void DrawGoaf();
    static void AdvanceWS();
	static void AutoAdvanceWS();
	static void AutoAdvanceWSByUser();

    //绘制漏风源
    static void DrawAirLeakage( bool isSource );
    //瓦斯抽采管路绘制
    static void DrawGasTube( GASTUBETYPE GEName );
    //瓦斯泵绘制
    static void DrawGasPump( void );
    //阀
    static void DrawValve( void );
    //放水器
    static void DrawTailrace( void );
    //流量计
    static void DrawFlowmeter( void );
    //阻火器
    static void DrawBackFire( void );
    //测定孔
    static void DrawDetermineHole( void );
    //钻孔
    static void DrawDrill( void );
    //传感器
    static void DrawSensor( int flag );
    //测风站
    static void DrawAnemometer( void );
    //斜井
    static void DrawInclined( void );
    //竖井
    static void DrawShaft( void );
    //见煤孔
    static void DrawCoalhole( void );
    //未见煤孔
    static void DrawNoCoalhole( void );
    //探水钻孔
    static void DrawWaterDrill( void );
    //漏水孔
    static void DrawLeakDrill( void );
    //抽水孔
    static void DrawWaterPumpDrill( void );
    //风筒
    static void DrawChimney( void );
    //胶管
    static void DrawHoses( void );
    //static void DrawCompass();
    /* 实现在AutoDraw.cpp中 */
    static void AutoDraw();
    static void AutoDraw2();
    /* 自动标注风流方向 */
    static void AutoDirection();
    static double ControlDirByMethods( AcDbObjectId objId, double angle );
    /* 自动更新瓦斯流量数据 */
    //static void UpdateAllGasFlow();
    // 通用编辑命令
    static void JunctionEnclose( void );
    static void ReverseDirection();
    static void ReBindEdge();
    static void ZoomMineGE();
    static void SplitByPoint( AcDbObjectId objId, AcGePoint3d spt, AcGePoint3d ept, AcGePoint3d pt );
    static void SplitDeal();
    static void MergeChimney();
    static void Merging( AcDbObjectId sourceId, AcDbObjectId mergeId );
    static void DrawQTagGE();
    static void DrawEffectRanGE();
    static void DrawGasFlow();
    static void AutoGasFlow();
    static void DeleteDirection();
    static void DeleteAllGasFlow();
    static void DeleteFanTag();
    static void DeleteQTag();
    static void DeleteEDTag();

	static void CopyDatas();
    //static AcDbObjectId GetRelatedTW(AcDbObjectId objId);
    static void testHD();
    static void DrawCDH();
    static void DrawCDH2();
    // 交互方式绘制BlockGE图元
    static void DrawBlockGE();
    // 向dwg文件中新增块
    static void AddNewBlock();
	//掘进工作面推进
	static void AdvanceTT();
	//输入长度推进掘进工作面
	static void AdvanceTTByUser();
};