//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ArxCmds.rc
//
#define IDS_PROJNAME                    100
#define IDD_SELBLOCK                    103
#define IDC_COMBO1                      104
#define IDD_ADVANCE_INPUT_DIALOG        104
#define IDC_BLOCK_LIST                  105
#define IDC_PREVIEW                     106
#define IDC_STTIME_EDIT                 107
#define IDC_ENTIME_EDIT                 108
#define IDC_LENGTH_EDIT                 109
#define IDC_SHOW_IN_TOOLTIP             154

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        110
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         109
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
