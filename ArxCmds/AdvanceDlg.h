#pragma once
#include "Resource.h"

// AdvanceDlg 对话框

class AdvanceDlg : public CDialog
{
	DECLARE_DYNAMIC(AdvanceDlg)

public:
	AdvanceDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~AdvanceDlg();

// 对话框数据
	enum { IDD = IDD_ADVANCE_INPUT_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	double m_length;
	CString m_stTime;
	CString m_edTime;
	afx_msg void OnBnClickedOk();
};
