#include "StdAfx.h"
#include "resource.h"
#include "MineGE/HelperClass.h"
#include "DrawCmd.h"
#include "MineGE/JointGE.h"
#include "ArxHelper/HelperClass.h"
#include "MineGE/HelperClass.h"
#include "Tool/HelperClass.h"
// 定义注册服务名称
#ifndef ARXCMDS_SERVICE_NAME
#define ARXCMDS_SERVICE_NAME _T("ARXCMDS_SERVICE_NAME")
#endif
//-----------------------------------------------------------------------------
//----- ObjectARX EntryPoint
class CDefineCmdsApp : public AcRxArxApp
{
public:
    CDefineCmdsApp () : AcRxArxApp () {}
    virtual AcRx::AppRetCode On_kInitAppMsg ( void* pkt )
    {
        // You *must* call On_kInitAppMsg here
        AcRx::AppRetCode retCode = AcRxArxApp::On_kInitAppMsg ( pkt ) ;
        acrxRegisterAppMDIAware( pkt );
        // 注册服务
        acrxRegisterService( ARXCMDS_SERVICE_NAME );
        return ( retCode ) ;
    }
    virtual AcRx::AppRetCode On_kUnloadAppMsg ( void* pkt )
    {
        // TODO: Add your code here
        // You *must* call On_kUnloadAppMsg here
        AcRx::AppRetCode retCode = AcRxArxApp::On_kUnloadAppMsg ( pkt ) ;
        // 删除服务
        delete acrxServiceDictionary->remove( ARXCMDS_SERVICE_NAME );
        return ( retCode ) ;
    }
	virtual AcRx::AppRetCode On_kLoadDwgMsg( void* pkt )
	{
		AcRx::AppRetCode retCode = AcRxArxApp::On_kLoadDwgMsg ( pkt ) ;
		DrawCmd::AdvanceTT();
		DrawCmd::AutoAdvanceWS();
		acutPrintf( _T( "\nArxCmds::On_kLoadDwgMsg\n" ) );
		return retCode;
	}
	virtual AcRx::AppRetCode On_kUnloadDwgMsg( void* pkt )
	{
		AcRx::AppRetCode retCode = AcRxArxApp::On_kUnloadDwgMsg ( pkt ) ;
		acutPrintf( _T( "\nArxCmds::On_kUnloadDwgMsg\n" ) );
		return retCode;
	}
    virtual void RegisterServerComponents ()
    {
    }
    // 绘制巷道
    static void JL_DrawTunnel( void )
    {
        DrawCmd::DrawTunnel();        // 使用普通命令行交互绘制巷道
    }
    // 绘制弧线巷道
    static void JL_DrawArcTunnel( void )
    {
        DrawCmd::DrawArcTunnel();
    }
    // 绘制风流方向
    static void JL_DrawDirection()
    {
        DrawCmd::DrawDirection();       // 使用普通命令行交互绘制风流方向
    }
    // 绘制工作面
    static void JL_DrawWorkSurface()
    {
        DrawCmd::DrawWS();       // 使用普通命令行交互绘制工作面
    }
    //绘制采空区
    static void JL_DrawGoaf()
    {
        DrawCmd::DrawGoaf();
    }
    // 绘制掘进工作面
    static void JL_DrawTTunnel()
    {
        DrawCmd::DrawTTunnel();  // 绘制掘进工作面
    }
    // 绘制主要风机
    static void JL_DrawMainFan()
    {
        DrawCmd::DrawMainFan();       // 使用普通命令行交互绘制风机
    }
    // 绘制局部扇风机
    static void JL_DrawLocalFan()
    {
        DrawCmd::DrawLocalFan();       // 使用普通命令行交互绘制风机
    }
    // 绘制机站
    static void JL_DrawMachineStation()
    {
        DrawCmd::DrawMachineStation();
    }
    // 绘制空气幕
    static void JL_DrawAirCurtain()
    {
        DrawCmd::DrawAirCurtain();
    }
    //绘制突水点
    static void JL_DrawWaterInrushPt()
    {
        DrawCmd::DrawWaterInrushPt();
    }
    //绘制水柱计
    static void JL_DrawDiffPressGauge()
    {
        DrawCmd::DrawDiffPressGauge();
    }
    //绘制附加动力
    static void JL_DrawAddPower()
    {
        DrawCmd::DrawAddPower();
    }
    //绘制局部阻力
    static void JL_DrawLocalResis()
    {
        DrawCmd::DrawLocalResis();
    }
    //绘制热源
    static void JL_DrawHeatSource()
    {
        DrawCmd::DrawHeatSource();
    }
	//绘制皮托管
    static void JL_DrawPitotTube()
    {
        DrawCmd::DrawPitotTube();
    }
    // 绘制风筒
    static void JL_DrawChimney()
    {
        DrawCmd::DrawChimney();
    }
    //绘制胶管
    static void JL_DrawHoses()
    {
        DrawCmd::DrawHoses();
    }
    // 绘制永久风门
    static void JL_DrawPermanentGate()
    {
        DrawCmd::DrawGate( 1 );
    }
    // 绘制临时风门
    static void JL_DrawTemporaryGate()
    {
        DrawCmd::DrawGate( 2 );
    }
    // 绘制双向风门
    static void JL_DrawDoubleGate()
    {
        DrawCmd::DrawGate( 3 );
    }
    // 绘制平衡风门
    static void JL_DrawBalanceGate()
    {
        DrawCmd::DrawGate( 4 );
    }
    // 绘制墙调节风窗
    static void JL_DrawWallCasement()
    {
        DrawCmd::DrawCasement( 1 );
    }
    // 绘制永久调节风窗
    static void JL_DrawPermanentCasement()
    {
        DrawCmd::DrawCasement( 2 );
    }
    // 绘制临时挡风墙
    static void JL_DrawTemporaryCasement()
    {
        DrawCmd::DrawCasement( 3 );
    }
    // 绘制火药爆炸材料库
    static void JL_DrawPowderStorage()
    {
        DrawCmd::DrawStorage( 1 );     // 使用普通命令行交互绘制火药库
    }
    // 绘制机电硐室
    static void JL_DrawMachineRoom()
    {
        DrawCmd::DrawStorage( 2 );     // 使用普通命令行交互绘制机电硐室
    }
    // 绘制井下充电室
    static void JL_DrawChargeRoom()
    {
        DrawCmd::DrawStorage( 3 );     // 使用普通命令行交互绘制充电室
    }
    // 绘制漏风源点
    static void JL_DrawAirSource()
    {
        DrawCmd::DrawAirLeakage( true );
    }
    // 绘制漏风汇点
    static void JL_DrawAirIn()
    {
        DrawCmd::DrawAirLeakage( false );
    }
    //绘制永久抽瓦斯抽采管路
    static void JL_DrawMainTube( void )
    {
        DrawCmd::DrawGasTube( PD_GAS_TUBE );
    }
    //绘制移动泵排瓦斯抽采管路
    static void JL_DrawTrunkTube( void )
    {
        DrawCmd::DrawGasTube( MPE_GAS_TUBE );
    }
    //绘制移动泵排瓦斯抽采管路
    static void JL_DrawBranchTube( void )
    {
        DrawCmd::DrawGasTube( MPD_GAS_TUBE );
    }
    //绘制钻孔
    static void JL_DrawDrill( void )
    {
        DrawCmd::DrawDrill();
    }
    //绘制瓦斯泵
    static void JL_DrawGasPump( void )
    {
        DrawCmd::DrawGasPump();
    }
    //绘制放水器
    static void JL_DrawTailrace( void )
    {
        DrawCmd::DrawTailrace();
    }
    //绘制瓦斯阀门
    static void JL_DrawValve( void )
    {
        DrawCmd::DrawValve();
    }
    //绘制流量计
    static void JL_DrawFlowmeter( void )
    {
        DrawCmd::DrawFlowmeter();
    }
    //绘制阻火器
    static void JL_DrawBackFire( void )
    {
        DrawCmd::DrawBackFire();
    }
    //绘制测定孔
    static void JL_DrawDetermineHole( void )
    {
        DrawCmd::DrawDetermineHole();
    }
    // 绘制永久挡风墙
    static void JL_DrawPermanentWall()
    {
        DrawCmd::DrawWall( 1 );
    }
    // 绘制临时挡风墙
    static void JL_DrawTemporaryWall()
    {
        DrawCmd::DrawWall( 2 );
    }
    // 绘制防火挡风墙
    static void JL_DrawFireWall()
    {
        DrawCmd::DrawWall( 3 );
    }
    //绘制通用传感器
    static void JL_DrawCommonSensor( void )
    {
        DrawCmd::DrawSensor( 0 );
    }
    //绘制温度传感器
    static void JL_DrawTempeSensor( void )
    {
        DrawCmd::DrawSensor( 1 );
    }
    //绘制流量传感器
    static void JL_DrawFlowSensor( void )
    {
        DrawCmd::DrawSensor( 2 );
    }
    //绘制压力传感器
    static void JL_DrawPressSensor( void )
    {
        DrawCmd::DrawSensor( 3 );
    }
    //绘制瓦斯传感器
    static void JL_DrawGasSensor( void )
    {
        DrawCmd::DrawSensor( 4 );
    }
    //绘制CO传感器
    static void JL_DrawCOSensor( void )
    {
        DrawCmd::DrawSensor( 5 );
    }
    //绘制风速传感器
    static void JL_DrawVentSensor( void )
    {
        DrawCmd::DrawSensor( 6 );
    }
    //绘制压差传感器
    static void JL_DrawDifferPressSensor( void )
    {
        DrawCmd::DrawSensor( 7 );
    }
    //绘制测风站
    static void JL_DrawAnemometer( void )
    {
        DrawCmd::DrawAnemometer();
    }
    // 绘制风桥
    static void JL_DrawWindBridge()
    {
        DrawCmd::DrawWindBridge();
    }
    // 绘制斜井
    static void JL_DrawInclined()
    {
        DrawCmd::DrawInclined();
    }
    // 绘制竖井
    static void JL_DrawShaft()
    {
        DrawCmd::DrawShaft();
    }
    // 绘制见煤孔
    static void JL_DrawCoalhole()
    {
        DrawCmd::DrawCoalhole();
    }
    // 绘制未见煤孔
    static void JL_DrawNoCoalhole()
    {
        DrawCmd::DrawNoCoalhole();
    }
    // 绘制探水钻孔
    static void JL_DrawWaterDrill()
    {
        DrawCmd::DrawWaterDrill();
    }
    // 绘制漏水孔
    static void JL_DrawLeakDrill()
    {
        DrawCmd::DrawLeakDrill();
    }
    // 绘制抽水孔
    static void JL_DrawWaterPumpDrill()
    {
        DrawCmd::DrawWaterPumpDrill();
    }
    //分割图元
    static void JL_SplitByPoint( void )
    {
        DrawCmd::SplitDeal();
    }
    //流动反向
    static void JL_ReverseDirection( void )
    {
        DrawCmd::ReverseDirection();
    }
    //自动绘制风流方向
    static void JL_AutoDirection( void )
    {
        DrawCmd::AutoDirection();
    }
    //推进工作面绘制采空区
    static void JL_AdvanceWS( void )
    {
        DrawCmd::AdvanceWS();
    }
	//自动推进工作面
	static void JL_AutoAdvanceWS( void )
	{
		DrawCmd::AutoAdvanceWS();
	}
	// 人为设定回采面推进长度
	static void JL_AutoAdvanceWSByUser( void )
	{
		DrawCmd::AutoAdvanceWSByUser();
	}
    static void JL_DrawCDH()
    {
        DrawCmd::DrawCDH();
    }
	 
    static void JL_DrawCDH2()
    {
        DrawCmd::DrawCDH2();
    }
	// 绘制自定义图元
    static void JL_DrawBlockGE()
    {
        DrawCmd::DrawBlockGE();
    }
	// 新增自定义图元
    static void JL_AddNewBlock()
    {
        DrawCmd::AddNewBlock();
    }

	static void JL_AdvanceTT()
	{
		DrawCmd::AdvanceTT();
	}
	//人为设定掘进面的推进长度
	static void JL_AdvanceTTByUser()
	{
		DrawCmd::AdvanceTTByUser();
	}

	//刷新所有方向
	static void JL_DeleteDirects()
	{
		DrawCmd::DeleteDirection();
	}

	static void JL_DrawJointGE(void)
	{
		DrawCmd::DrawJoint();
	}

	//属性数据复制
	static void JL_CopyDatas( void )
	{
		DrawCmd::CopyDatas();
	}

    // 测试: BaseDao基类成员函数的使用
    static void JL_ParamTest()
    {
        AcGePoint3d pt;
        if( !ArxUtilHelper::GetPoint( _T( "\n请选择一点:" ), pt ) ) return;
        JointGE* pJointGE = new JointGE( pt );
        if( !ArxUtilHelper::PostToModelSpace( pJointGE ) ) return;
        AcDbObjectId objId = pJointGE->objectId();
        if( objId.isNull() ) return;
        AcTransaction* pTrans = acTransactionManagerPtr()->startTransaction();
        AcDbObject* pObj = 0;
        if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForWrite ) )
        {
            acTransactionManagerPtr()->abortTransaction();
            return;
        }
        /*JointGE**/ pJointGE = JointGE::cast( pObj );
        if( pJointGE == 0 )
        {
            acTransactionManagerPtr()->abortTransaction();
            return;
        }
        CString value;
        pJointGE->getString( _T( "坐标" ), value );
        acutPrintf( _T( "\n获取点坐标:%s" ), value );
        acTransactionManagerPtr()->endTransaction();
    }
    // 测试: BaseDao基类成员函数的使用
    static void JL_ParamTest2()
    {
        AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "\n请选择一个节点:" ) );
        if( objId.isNull() ) return;
        AcTransaction* pTrans = acTransactionManagerPtr()->startTransaction();
        AcDbObject* pObj = 0;
        if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForWrite ) )
        {
            acTransactionManagerPtr()->abortTransaction();
            return;
        }
        JointGE* pJointGE = JointGE::cast( pObj );
        if( pJointGE == 0 )
        {
            acTransactionManagerPtr()->abortTransaction();
            return;
        }
        AcGePoint3d pt = pJointGE->getInsertPt();
        pt.x += 100;
        pt.y += 100;
        pt.z += 10;
        pJointGE->setPoint( _T( "坐标" ), pt );
        CString value;
        pJointGE->getString( _T( "坐标" ), value );
        acutPrintf( _T( "\n修改后获取点坐标:%s" ), value );
        pJointGE->getString( _T( "半径" ), value );
        double radius = _tstof( value ) + 10;
        pJointGE->setDouble( _T( "半径" ), radius );
        acTransactionManagerPtr()->endTransaction();
    }
    // 测试: EntityData类的使用
    static void JL_ParamTest3()
    {
        AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "\n请选择一个节点:" ) );
        if( objId.isNull() ) return;
        AcGePoint3d pt;
        EntityData::GetPoint( objId, _T( "坐标" ), pt );
        acutPrintf( _T( "\n节点坐标:(%.3f, %.3f, %.3f))" ), pt.x, pt.y, pt.z );
        acutPrintf( _T( "\n修改半径为50" ) );
        EntityData::SetDouble( objId, _T( "半径" ), 50.0 );
    }

    // 测试: 打印巷道所有的几何参数
    static void JL_ParamTest4()
    {
        AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "\n请选择一个分支类图元 [巷道、瓦斯管路等]：" ) );
        if( !ArxUtilHelper::IsEqualType( _T( "EdgeGE" ), objId ) ) return;

        AcStringArray fields,values;
        EntityData::GetAllDatas(objId, fields, values);
        acutPrintf(_T("\n打印巷道的所有几何参数..."));
        for(int i=0;i<fields.length();i++)
        {
            CString name = fields[i].kACharPtr();
            CString value = values[i].kACharPtr();
            acutPrintf(_T("\n-->\t%s: %s"), name, value);
        }
    }
} ;
//-----------------------------------------------------------------------------
IMPLEMENT_ARX_ENTRYPOINT( CDefineCmdsApp )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _ParamTest, ParamTest, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _ParamTest2, ParamTest2, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _ParamTest3, ParamTest3, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _ParamTest4, ParamTest4, ACRX_CMD_TRANSPARENT, NULL )

ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawTunnel, DrawTunnel, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawArcTunnel, DrawArcTunnel, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawDirection, DrawDirection, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawWorkSurface, DrawWorkSurface, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawGoaf, DrawGoaf, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawTTunnel, DrawTTunnel, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawMainFan, DrawMainFan, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawMachineStation, DrawMachineStation, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawLocalFan, DrawLocalFan, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawChimney, DrawChimney, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawAirCurtain, DrawAirCurtain, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawWaterInrushPt, DrawWaterInrushPt, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawDiffPressGauge, DrawDiffPressGauge, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawAddPower, DrawAddPower, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawLocalResis, DrawLocalResis, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawHeatSource, DrawHeatSource, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawPitotTube, DrawPitotTube, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawHoses, DrawHoses, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawPermanentGate, DrawPermanentGate, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawTemporaryGate, DrawTemporaryGate, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawDoubleGate, DrawDoubleGate, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawBalanceGate, DrawBalanceGate, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawWallCasement, DrawWallCasement, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawPermanentCasement, DrawPermanentCasement, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawTemporaryCasement, DrawTemporaryCasement, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawPowderStorage, DrawPowderStorage, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawMachineRoom, DrawMachineRoom, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawChargeRoom, DrawChargeRoom, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawAirSource, DrawAirSource, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawAirIn, DrawAirIn, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawMainTube, DrawMainTube, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawTrunkTube, DrawTrunkTube, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawBranchTube, DrawBranchTube, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawDrill, DrawDrill, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawGasPump, DrawGasPump, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawTailrace, DrawTailrace, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawValve, DrawValve, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawFlowmeter, DrawFlowmeter, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawBackFire, DrawBackFire, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawDetermineHole, DrawDetermineHole, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawPermanentWall, DrawPermanentWall, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawTemporaryWall, DrawTemporaryWall, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawFireWall, DrawFireWall, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawCommonSensor, DrawCommonSensor, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawTempeSensor, DrawTempeSensor, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawFlowSensor, DrawFlowSensor, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawCOSensor, DrawCOSensor, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawPressSensor, DrawPressSensor, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawDifferPressSensor, DrawDifferPressSensor, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawGasSensor, DrawGasSensor, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawVentSensor, DrawVentSensor, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawAnemometer, DrawAnemometer, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawWindBridge, DrawWindBridge, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawInclined, DrawInclined, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawShaft, DrawShaft, ACRX_CMD_TRANSPARENT, NULL );
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawCoalhole, DrawCoalhole, ACRX_CMD_TRANSPARENT, NULL );
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawNoCoalhole, DrawNoCoalhole, ACRX_CMD_TRANSPARENT, NULL );
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawWaterDrill, DrawWaterDrill, ACRX_CMD_TRANSPARENT, NULL );
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawLeakDrill, DrawLeakDrill, ACRX_CMD_TRANSPARENT, NULL );
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawWaterPumpDrill, DrawWaterPumpDrill, ACRX_CMD_TRANSPARENT, NULL );
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawCDH, DrawCDH, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawCDH2, DrawCDH2, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawBlockGE, DrawBlockGE, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _AddNewBlock, AddNewBlock, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _ReverseDirection, ReverseDirection, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _AdvanceWS, AdvanceWS, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _AutoAdvanceWS, AutoAdvanceWS, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _AutoAdvanceWSByUser, AutoAdvanceWSByUser, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _AutoDirection, AutoDirection, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _SplitByPoint, SplitByPoint, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _AdvanceTT, AdvanceTT, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _AdvanceTTByUser, AdvanceTTByUser, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DeleteDirects, DeleteDirects, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _DrawJointGE, DrawJoint, ACRX_CMD_TRANSPARENT, NULL )
ACED_ARXCOMMAND_ENTRY_AUTO( CDefineCmdsApp, JL, _CopyDatas, CopyDatas, ACRX_CMD_TRANSPARENT, NULL )
