#pragma once
#include "afxwin.h"
#include "Resource.h"
// SelBlockDlg 对话框
class SelBlockDlg : public CAcUiDialog
{
    DECLARE_DYNAMIC( SelBlockDlg )
public:
    SelBlockDlg( CWnd* pParent = NULL ); // 标准构造函数
    virtual ~SelBlockDlg();
// 对话框数据
    enum { IDD = IDD_SELBLOCK };
protected:
    virtual void DoDataExchange( CDataExchange* pDX );  // DDX/DDV 支持
    virtual BOOL OnInitDialog();
    DECLARE_MESSAGE_MAP()
public:
    CListBox m_blockList;
    CStatic m_bitmap;
    CString m_dwgPath;
    CString m_curBlockName;
    afx_msg void OnLbnSelchangeBlockList();
private:
    HBITMAP BlockIconToBMP( AcDbBlockTableRecord::PreviewIcon icon, HDC hdc );
    void updateBitmap();
public:
    afx_msg void OnBnClickedOk();
    afx_msg void OnLbnDblclkBlockList();
};
