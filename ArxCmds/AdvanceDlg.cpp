#include "stdafx.h"
#include "AdvanceDlg.h"

IMPLEMENT_DYNAMIC(AdvanceDlg, CDialog)

AdvanceDlg::AdvanceDlg(CWnd* pParent /*=NULL*/)
	: CDialog(AdvanceDlg::IDD, pParent)
	, m_length(0)
	, m_stTime(_T(""))
	, m_edTime(_T(""))
{

}

AdvanceDlg::~AdvanceDlg()
{
}

void AdvanceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_LENGTH_EDIT, m_length);
	DDX_Text(pDX, IDC_STTIME_EDIT, m_stTime);
	DDX_Text(pDX, IDC_ENTIME_EDIT, m_edTime);
}


BEGIN_MESSAGE_MAP(AdvanceDlg, CDialog)
	ON_BN_CLICKED(IDOK, &AdvanceDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// AdvanceDlg 消息处理程序

void AdvanceDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	OnOK();
}

BOOL AdvanceDlg::OnInitDialog()
{
	UpdateData(FALSE);
	return CDialog::OnInitDialog();
}
