#include "stdafx.h"
#include "SelBlockDlg.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
// SelBlockDlg 对话框
IMPLEMENT_DYNAMIC( SelBlockDlg, CAcUiDialog )
SelBlockDlg::SelBlockDlg( CWnd* pParent /*=NULL*/ )
    : CAcUiDialog( SelBlockDlg::IDD, pParent )
{
    CString dataDirName = _T( "" );
    // 块定义文件路径
    m_dwgPath = PathHelper::BuildPath( ARX_FILE_PATH( dataDirName ), BLOCK_DWG_NAME );
}
SelBlockDlg::~SelBlockDlg()
{
}
void SelBlockDlg::DoDataExchange( CDataExchange* pDX )
{
    CAcUiDialog::DoDataExchange( pDX );
    DDX_Control( pDX, IDC_BLOCK_LIST, m_blockList );
    DDX_Control( pDX, IDC_PREVIEW, m_bitmap );
}

BEGIN_MESSAGE_MAP( SelBlockDlg, CAcUiDialog )
    ON_LBN_SELCHANGE( IDC_BLOCK_LIST, &SelBlockDlg::OnLbnSelchangeBlockList )
    ON_BN_CLICKED( IDOK, &SelBlockDlg::OnBnClickedOk )
    ON_LBN_DBLCLK( IDC_BLOCK_LIST, &SelBlockDlg::OnLbnDblclkBlockList )
END_MESSAGE_MAP()
BOOL SelBlockDlg::OnInitDialog()
{
    CAcUiDialog::OnInitDialog();
    AcStringArray blkNames;
    ArxDwgHelper::GetAllBlockNames( m_dwgPath, blkNames );
    if( blkNames.isEmpty() )
    {
        acutPrintf( _T( "\n没有块定义" ) );
        return FALSE;
    }
    // 打印所有的块名称列表
    acutPrintf( _T( "\n自定义图元列表:" ) );
    int n = blkNames.length();
    for( int i = 0; i < n; i++ )
    {
        acutPrintf( _T( "\n%d:%s" ), i, blkNames[i].kACharPtr() );
        CString blkName = blkNames[i].kACharPtr();
        m_blockList.AddString( blkName );
    }
    m_blockList.SetCurSel( 0 );
    updateBitmap();
    return TRUE;
}
HBITMAP SelBlockDlg::BlockIconToBMP( AcDbBlockTableRecord::PreviewIcon icon, HDC hdc )
{
    //由icon数组获得可显示的位图
    BITMAPINFOHEADER ih;    //位图信息头
    memcpy( &ih, icon.asArrayPtr(), sizeof( ih ) );
    size_t memsize = sizeof( BITMAPINFOHEADER ) + ( ( 1 << ih.biBitCount ) * sizeof( RGBQUAD ) );
    LPBITMAPINFO bi = ( LPBITMAPINFO )malloc( memsize );
    //位图信息
    memcpy( bi, icon.asArrayPtr(), memsize );
    HBITMAP hbm = CreateDIBitmap( hdc, &ih, CBM_INIT, icon.asArrayPtr() + memsize, bi, DIB_RGB_COLORS );
    free( bi );
    return hbm;
}
void SelBlockDlg::OnLbnSelchangeBlockList()
{
    updateBitmap();
}
void SelBlockDlg::updateBitmap()
{
    AcDbObjectId blkTblRcdId;
    CString strBlkDefName;
    m_blockList.GetText( m_blockList.GetCurSel(), strBlkDefName );
    IconMap icons;
    ArxDwgHelper::GetBlockIcons( m_dwgPath, icons );
    if( icons.find( strBlkDefName ) == icons.end() )
    {
        return;
    }
    CPaintDC dc( this );
    HBITMAP hBitmap = BlockIconToBMP( icons[strBlkDefName], dc.GetSafeHdc() );
    m_bitmap.SetBitmap( hBitmap );
}
void SelBlockDlg::OnBnClickedOk()
{
    m_blockList.GetText( m_blockList.GetCurSel(), m_curBlockName );
    CAcUiDialog::OnOK();
}
void SelBlockDlg::OnLbnDblclkBlockList()
{
    m_blockList.GetText( m_blockList.GetCurSel(), m_curBlockName );
    CAcUiDialog::OnOK();
}
