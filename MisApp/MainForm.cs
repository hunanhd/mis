﻿using System;
using System.Collections.Generic;
//using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

using CefSharp;
using CefSharp.WinForms;

namespace MisApp
{
    public partial class MainForm : Form
    {
        public ChromiumWebBrowser browser;

        public MainForm()
        {
            InitializeComponent();
            // Start the browser after initialize global component
            InitializeChromium();
        }

        public void InitializeChromium()
        {
            // Now instead use http://mis.baidu.com as URL we'll use the "url" variable to load our local resource
            //String url = "http://www.baidu.com";
            string url = "http://mis/index.html";
            // Create a browser component
            browser = new ChromiumWebBrowser(url);
            browser.Dock = DockStyle.Fill;
            // Add it to the form and fill it to the form window.
            this.browserPanel.Controls.Add(browser);

            // 注册对象,必须在 browser 一创建后就注册            
            CefTools.RegisterJsObjects(browser);
            // 注册一些特殊url的处理handle
            //CefTools.RegisterTestResources(browser);
        }

        private void showDevToolButton_Click(object sender, EventArgs e)
        {
            browser.ShowDevTools();
        }

        private void aboutButton_Click(object sender, EventArgs e)
        {
            //string page = string.Format("{0}/mis/about.html", Environment.CurrentDirectory);
            string page = "http://mis/about.html";
            browser.Load(page);
        }

        // js调用C#
        private void button1_Click(object sender, EventArgs e)
        {
            string scripts = @"
                alert('Call JavaScript from C#');
                document.body.style.backgroundColor = 'red';
                function CallWinformFunc()
                {
                     var list = customObj.GetListOfPeople(); // Call C# Function
                     for (var nLoopCnt = 0; nLoopCnt < list.length; nLoopCnt++) {
                           var person = list[nLoopCnt];
                            console.log(person);
                     }
                }
                CallWinformFunc();
            ";
            browser.ExecuteScriptAsync(scripts);
        }

        // C#调用js并取回数据
        private void button2_Click(object sender, EventArgs e)
        {
            string script = @"
                function tempFunction() {
                    var w = window.innerWidth;
                    var h = window.innerHeight;

                    return w*h;
                }
                tempFunction();
            ";

            var task = browser.EvaluateScriptAsync(script, null);

            task.ContinueWith(t =>
            {
                if (!t.IsFaulted)
                {
                    var response = t.Result;

                    if (response.Success == true)
                    {
                        MessageBox.Show(response.Result.ToString());
                    }
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        // C#调用js并取回数据
        private void button3_Click(object sender, EventArgs e)
        {
            string htmlPage = @"
                hello world2
            ";
            //string url = string.Format(@"{0}/mis/about.html", Environment.CurrentDirectory);
            string url = "http://mis/about.html";
            browser.LoadHtml(htmlPage, url);

            string script = @"
                function tempFunction() {
                    // create a JS object
                    var person = {firstName:'John', lastName:'Maclaine', age:23, eyeColor:'blue'};
                    // Important: convert object to string before returning to C#                    
                    return JSON.stringify(person);
                }
                tempFunction();
            ";

            var task = browser.EvaluateScriptAsync(script, null);

            task.ContinueWith(t =>
            {
                if (!t.IsFaulted)
                {
                    var response = t.Result;

                    if (response.Success == true)
                    {
                        MessageBox.Show(response.Result.ToString());
                    }
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        //private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        //{
        //    Cef.Shutdown();
        //}
    }
}
