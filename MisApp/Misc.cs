﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using System.Reflection;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Windows;

using AcadApplication = Autodesk.AutoCAD.ApplicationServices.Application;
using AcadDocument = Autodesk.AutoCAD.ApplicationServices.Document;
using AcadWindows = Autodesk.AutoCAD.Windows;

namespace MisApp
{
    public static class Misc
    {
        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWow64Process([In] IntPtr hProcess, [Out] out bool lpSystemInfo);

        /// <summary>
        /// Detect Operation System is 64bit
        /// </summary>
        /// <returns></returns>
        public static bool Is64BitOS()
        {
            using (Process p = Process.GetCurrentProcess())
            {
                bool retVal;
                return (IntPtr.Size == 8 ||
                        (IntPtr.Size == 4 &&
                        IsWow64Process(p.Handle, out retVal) &&
                        retVal)
                       );
            }
        }

        /// <summary>
        /// Detect Process is 64bit
        /// </summary>
        /// <returns></returns>
        public static bool Is64BitProcess()
        {
            return (IntPtr.Size == 8);
        }

        public static Stream GetResourceStream(string fileName, bool embedded)
        {
            // 使用本地文件资源(local)
            if (!embedded)
            {
                if (File.Exists(fileName))
                {
                    Byte[] bytes = File.ReadAllBytes(fileName);
                    return new MemoryStream(bytes);
                }
                else
                {
                    return null;
                }
            }
            // 使用程序集内嵌资源(embedded)
            else
            {
                Assembly ass = Assembly.GetExecutingAssembly();
                string resourcePath = ass.GetName().Name + "." + fileName.Replace("/", ".");
                if (ass.GetManifestResourceInfo(resourcePath) != null)
                {
                    return ass.GetManifestResourceStream(resourcePath);
                }
                else
                {
                    return null;
                }
            }
        }

        static string BuildFileName(string arxName, string ext)
        {
            return String.Format("{0}.{1}", arxName, ext);
        }

        static string BuildServiceName(string arxName)
        {
            return String.Format("{0}_SERVICE_NAME", arxName.ToUpper());
        }

        public static bool LoadArx(string arxName)
        {
            Document doc = AcadApplication.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;

            string fname = BuildFileName(arxName, "arx");
            string serviceName = BuildServiceName(arxName);
            //ed.WriteMessage(serviceName);
            RXObject svc = SystemObjects.ServiceDictionary.At(serviceName);
            if (svc == null)
            {
                try
                {
                    SystemObjects.DynamicLinker.LoadModule(fname, true, false);
                    //Assembly.LoadFrom("CsBridge.dll");
                    ed.WriteMessage("\n成功加载{0}模块!", fname);
                    return true;
                }
                catch (System.Exception ex)
                {
                    ed.WriteMessage(
                      "\n加载{0}模块失败!  原因:{1}",
                      fname,
                      ex.Message
                    );
                    return false;
                }
            }
            else
            {
                ed.WriteMessage("\n{0}模块已经加载过了!", fname);
            }
            return true;
        }

        public static void UnloadArx(string arxName)
        {
            Document doc = AcadApplication.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;

            string fname = BuildFileName(arxName, "arx");

            SystemObjects.DynamicLinker.UnloadModule(fname, false);
            ed.WriteMessage("\n卸载模块:{0}", fname);
        }

        public static bool LoadAssembly(string dllName)
        {
            Document doc = AcadApplication.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;

            string fname = BuildFileName(dllName, "dll");
            try
            {
                Assembly.LoadFrom(fname);
                return true;
            }
            catch (System.Exception ex)
            {
                ed.WriteMessage(
                  "\n加载{0}模块失败!  原因:{1}",
                  fname,
                  ex.Message
                );
                return false;
            }
        }

        public static void PrintValues1(StringCollection myCol)
        {
            Document doc = AcadApplication.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;

            foreach (Object obj in myCol)
            {
                ed.WriteMessage(String.Format("\n   {0}", obj));
            }
            ed.WriteMessage("\n");
        }

        public static void PrintValues2(StringCollection myCol)
        {
            Document doc = AcadApplication.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;

            StringEnumerator myEnumerator = myCol.GetEnumerator();
            while (myEnumerator.MoveNext())
            {
                ed.WriteMessage(String.Format("\n   {0}", myEnumerator.Current));
            }
            ed.WriteMessage("\n");
        }

        public static void PrintModules()
        {
            StringCollection sc = SystemObjects.DynamicLinker.GetLoadedModules();
            PrintValues1(sc);
            //PrintValues2(sc);
        }
    }
}
