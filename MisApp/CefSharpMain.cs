﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;

using CefSharp;
//using CefSharp.WinForms;

// 所有c#封装类都使用了M前缀
using arx;

[assembly:
    ExtensionApplication( typeof(MisApp.MyInitialization) )
]
[assembly:
    CommandClass( typeof(MisApp.MyCommands) )
]

namespace MisApp
{
    public class MyInitialization : Autodesk.AutoCAD.Runtime.IExtensionApplication
    {
        public void Initialize()
        {
            InitializeCef();
        }
        public void Terminate()
        {
            UnInitializeCef();
        }

        public void InitializeCef()
        {
            // 窗口系统样式设置
            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);

            try
            {
                CefTools.Init(false, true);
            }
            catch(System.Exception ex)
            {
                MessageBox.Show(ex.Message);
                Autodesk.AutoCAD.ApplicationServices.Application.Quit();
            }
        }
       
        /*
         * cef退出不正常，有时候后台进程能正常退出，有时候不能???
         * 参考: http://www.cnblogs.com/AimeeFang/p/6088107.html
         * 组件引用后，发现关闭程序时出现卡死，后来在FormClosed事件里各种资源释放还是无解，
         * 最后研究了下程序退出的各种方法，把Environment.Exit(0);换成Application.Exit();居然完美解决？
         */
        public void UnInitializeCef()
        {
            Cef.Shutdown();
            //System.Windows.Forms.Application.Exit();
        }
    }

    public class MyCommands
    {
        [CommandMethod("TestCef")]
        public static void TestCef()
        {
            MainForm f = new MainForm();
            f.ShowDialog();
        }
    }
}