﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using CefSharp;
//using CefSharp.WinForms;
using Newtonsoft.Json;

namespace MisApp
{
    //C# 对象暴露给 JavaScript 使用
    class CefCallTest
    {
        public string StringProp { get; set; }

        public CefCallTest()
        {
            StringProp = "Hello, Cef";
        }

        public void ShowHelloCef()
        {
            MessageBox.Show("Hello, Cef!!!!");
        }
    }

    class Person
    {
        public Person()
            : this("li", "lei", DateTime.Now)
        {
        }

        public Person(string firstName, string lastName, DateTime birthDate)
        {
            FirstName = firstName;
            LastName = lastName;
            DateOfBirth = birthDate;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int SkillLevel { get; set; }
    }

    class CustomObject
    {
        private Person m_theMan = null;

        public CustomObject()
        {
            m_theMan = new Person("Bat", "Man", DateTime.Now);
        }

        public string SomeFunction()
        {
            return "yippieee";
        }

        public string GetPerson()
        {
            var p1 = new Person("Bruce", "Banner", DateTime.Now);

            string json = JsonConvert.SerializeObject(p1);
            return json;
        }

        public string ErrorFunction()
        {
            return null;
        }

        public string GetListOfPeople()
        {
            List<Person> peopleList = new List<Person>();

            peopleList.Add(new Person("Scooby", "Doo", DateTime.Now));
            peopleList.Add(new Person("Buggs", "Bunny", DateTime.Now));
            peopleList.Add(new Person("Daffy", "Duck", DateTime.Now));
            peopleList.Add(new Person("Fred", "Flinstone", DateTime.Now));
            peopleList.Add(new Person("Iron", "Man", DateTime.Now));

            string json = JsonConvert.SerializeObject(peopleList);
            return json;
        }
    }
}
