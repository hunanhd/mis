﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Windows;

using AcadApplication = Autodesk.AutoCAD.ApplicationServices.Application;
using AcadDocument = Autodesk.AutoCAD.ApplicationServices.Document;
using AcadWindows = Autodesk.AutoCAD.Windows;

// 所有c#封装类都使用了M前缀
using arx;

[assembly:
    ExtensionApplication( typeof(MisApp.MyInitialization) )
]
[assembly:
    CommandClass( typeof(MisApp.MyCommands) )
]

namespace MisApp
{
    public class MyInitialization : Autodesk.AutoCAD.Runtime.IExtensionApplication
    {
        public void LoadArxModules()
        {
            if (!Misc.LoadArx("ArxHelper")) return;
            if (!Misc.LoadArx("MineGE")) return;
            if (!Misc.LoadArx("VentGE")) return;
            if (!Misc.LoadArx("GasGE")) return;
            if (!Misc.LoadArx("MGSGE")) return;
            if (!Misc.LoadArx("VngGE")) return; 
            if (!Misc.LoadArx("ArxData")) return;
            if (!Misc.LoadArx("ArxCmds")) return;
            if (!Misc.LoadArx("ArxLoader")) return;
        }

        public void UnLoadArxModules()
        {
            Misc.UnloadArx("ArxLoader");
            Misc.UnloadArx("ArxCmds");
            Misc.UnloadArx("ArxData");
            Misc.UnloadArx("VngGE");
            Misc.UnloadArx("GasGE");
            Misc.UnloadArx("MGSGE");
            Misc.UnloadArx("VentGE");
            Misc.UnloadArx("MineGE");
            Misc.UnloadArx("ArxHelper");
        }

        public void LoadDllAssembly()
        {
            Misc.LoadAssembly("MisMgd");
            Misc.LoadAssembly("MisVno");
        }

        public void Initialize()
        {
            //加载arx模块
            //更新:仍使用ArxLoader加载arx模块!C#加载arx模块还是会出现一些问题!
            //LoadArxModules();
            //加载托管dll
            LoadDllAssembly();
            //切换工作空间
            //AcadApplication.SetSystemVariable("WSCURRENT", "JLCAD");
        }
        public void Terminate()
        {
            //恢复AutoCAD经典工作空间
            //AcadApplication.SetSystemVariable("WSCURRENT", "AutoCAD经典");
            //卸载arx模块
            //UnLoadArxModules();
            //托管dll无法卸载
        }
    }

    public class MyCommands
    {

        [CommandMethod("FirstLine")]
        public static void FirstLine()
        {
            // AutoCAD.NET 自动加载托管dll
            // http://through-the-interface.typepad.com/through_the_interface/2008/09/loading-net-mod.html
            Document doc = AcadApplication.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;

            //获取当前活动图形数据库
            Database db = HostApplicationServices.WorkingDatabase;
            Point3d startPoint = new Point3d(500, 500, 0);  //直线起点
            Point3d endPoint = new Point3d(700, 500, 0); //直线终点
            Line line = new Line(startPoint, endPoint); //新建一直线对象   
            //定义一个指向当前数据库的事务处理，以添加直线
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                BlockTable bt = (BlockTable)trans.GetObject(db.BlockTableId, OpenMode.ForRead); //以读方式打开块表.
                //以写方式打开模型空间块表记录.
                BlockTableRecord btr = (BlockTableRecord)trans.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite);
                //将图形对象的信息添加到块表记录中,并返回ObjectId对象.
                btr.AppendEntity(line);
                trans.AddNewlyCreatedDBObject(line, true); //把对象添加到事务处理中.
                trans.Commit(); //提交事务处理
            }
        }

        [CommandMethod("TestPrint")]
        public static void TestPrint()
        {
            MUtil.Printf("\n你好啊!");
        }

        [CommandMethod("TestSelect")]
        public static void TestSelect()
        {
            ObjectId objId = MUtil.SelectEntity("\n请选择一个图元:");
            MUtil.Printf(String.Format("\nlong:{0} str: {1}", MUtil.GetLongId(objId), MUtil.ObjectIdToStr(objId)));
        }

        [CommandMethod("TestSelect2")]
        public static void TestSelect2()
        {
            ObjectIdCollection objIds = MUtil.SelectMoreEntity("\n请选择多个图元:");
            foreach (ObjectId objId2 in objIds)
            {
                MUtil.Printf(String.Format("\nlong:{0} str: {1}", MUtil.GetLongId(objId2), MUtil.ObjectIdToStr(objId2)));
            }
        }

        [CommandMethod("TestPoint")]
        public static void TestPoint()
        {
            Point3d basePt = new Point3d(0, 0, 0);
            Point3d pt = new Point3d();
            bool ret = MUtil.GetPoint("\n请选择一个点:", basePt, ref pt);
            if (ret)
            {
                MUtil.Printf(String.Format("\n用户选择坐标成功!{0}", pt));
                pt = new Point3d(100, 200, 300);
                MUtil.Printf(String.Format("\n修改后的坐标!{0}", pt));
            }
            else
            {
                MUtil.Printf("\n用户取消坐标选择!");
            }
        }

        [CommandMethod("TestDictData")]
        public static void TestDictData()
        {
            MUtil.Printf("\n测试词典dao的操作...");
            MUtil.Printf("\n注册词典...");
            ObjectId dictId = arx.MDict.Tool.RegDict("py词典");
            if(dictId.IsNull)
            {
                MUtil.Printf("\n注册词典失败!");
                return;
            }
            // 使用dao操作数据
            arx.MDao dao = new arx.MDict.Dao(dictId);
            // 写入数据
            dao.setString("高数", "78.9");
            MUtil.Printf(String.Format("\n获取数据-->字段:{0} 值:{1}", "高数", dao.getDouble("高数")));
            // 读取数据
            string field = "语文";
            MUtil.Printf(String.Format("\n获取数据-->字段:{0} 值:{1}", field, dao.getString(field)));
            // 手动释放资源(不能忘了!!!)
            dao.release();

            // 使用静态成员函数操作数据
            arx.MDict.Data.SetDateTime(dictId, "时间", DateTime.Now);

            // 打印词典的所有数据
            List<string> fields = arx.MDict.Data.GetAllFields(dictId);
            foreach (string a in fields)
            {
                MUtil.Printf("\n"+a);
            }

            string json_str = arx.MDict.Data.ExportToJsonString(dictId);
            MUtil.Printf("\n"+json_str);
        }
    }
}