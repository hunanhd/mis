﻿using System;
using System.Text;
using System.IO;
using System.Web;
using System.Net;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using CefSharp;
using CefSharp.WinForms;

namespace MisApp
{
    // 参考资料:
    // https://thechriskent.com/2014/04/21/use-local-files-in-cefsharp/
    // https://thechriskent.com/2014/05/12/use-embedded-resources-in-cefsharp/
    // http://maogm.com/blog/cef-in-winform.html?utm_source=tuicool&utm_medium=referral
    // https://github.com/cefsharp/CefSharp/wiki/General-Usage "Request Handling"后面的几节内容
    // https://my.oschina.net/sitan/blog/743268
    // https://bitbucket.org/chromiumembedded/cef/wiki/GeneralUsage#markdown-header-scheme-handler
    internal class LocalSchemeHanlderFactory : ISchemeHandlerFactory
    {
        // 使用本地资源,但scheme仍然用http
        public const string SchemeName = "http";
        public const string DomainName = "mis";
        public IResourceHandler Create(IBrowser browser, IFrame frame, string schemeName, IRequest request)
        {
            if (schemeName == LocalSchemeHanlderFactory.SchemeName)
            {
                return new LocalSchemeHandler();
            }

            return null;
        }
    }

    internal class LocalSchemeHandler : IResourceHandler
    {
        private string mimeType;
        private MemoryStream stream;
        private int statusCode;
        private string statusText;

        public Stream GetResponse(IResponse response, out long responseLength, out string redirectUrl)
        {
            responseLength = stream.Length;
            redirectUrl = null;

            response.StatusCode = statusCode;
            response.StatusText = statusText;
            response.MimeType = mimeType;

            return stream;
        }

        public bool ProcessRequestAsync(IRequest request, ICallback callback)
        {
            // The 'host' portion is entirely ignored by this scheme handler.
            // uri格式: [scheme:][//host:port][path][?query][#fragment]
            // 比如:    http://www.aiaide.com:8080/Home/index.htm?a=1&b=2#search
            var uri = new Uri(request.Url);
            var host = uri.Host;
            var fileName = uri.Host + uri.AbsolutePath;

            if (string.Equals(host, "mis", StringComparison.OrdinalIgnoreCase))
            {
                Stream resourceStream = Misc.GetResourceStream(fileName, false);
                if (resourceStream != null)
                {
                    string fileExtension = Path.GetExtension(fileName);
                    var handler = ResourceHandler.FromStream(resourceStream, ResourceHandler.GetMimeType(fileExtension));
                    stream = (MemoryStream)handler.Stream;
                    statusCode = handler.StatusCode;
                    statusText = handler.StatusText;
                    mimeType = handler.MimeType;
                    callback.Continue();
                    return true;
                }
            }
            //stream = new MemoryStream();
            //statusText = "404 error";
            //statusCode = (int)HttpStatusCode.NotFound;
            //callback.Continue();
            return false;
        }
    }
}