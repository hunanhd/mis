#include "StdAfx.h"
#include "Shaft.h"
Adesk::UInt32 Shaft::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    Shaft, MgsGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    竖井, MGSGEAPP
)
Shaft::Shaft()
{
    m_outRadius = 12;
    m_inRadius = 9;
}
Shaft::Shaft( const AcGePoint3d& insrtPt ): MgsGE( insrtPt )
{
    m_outRadius = 12;
    m_inRadius = 9;
}
Adesk::Boolean Shaft::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    double c = 0.618;
    DrawCircle( mode, m_insertPt, m_outRadius, false );
    DrawCircle( mode, m_insertPt, m_inRadius, false );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( PI / 4, AcGeVector3d::kZAxis );
    DrawLine( mode, m_insertPt, m_insertPt + v * m_inRadius );
    DrawLine( mode, m_insertPt, m_insertPt - v * m_inRadius );
    v.rotateBy( PI / 4, AcGeVector3d::kZAxis );
    AcGePoint3d pt1 = m_insertPt + v * ( m_outRadius + c );
    int colorIndex = mode->subEntityTraits().color();
    mode->subEntityTraits().setColor( 1 );
    AcGePoint3d pt2 = pt1 + v * ( m_outRadius + m_inRadius );
    DrawLine( mode, pt1, pt2 );
    pt2 = pt2 + v * c;
    v.rotateBy( PI / 6, AcGeVector3d::kZAxis );
    DrawLine( mode, pt1, pt1 + v * m_inRadius * 0.5 );
    v.rotateBy( -PI / 3, AcGeVector3d::kZAxis );
    DrawLine( mode, pt1, pt1 + v * m_inRadius * 0.5 );
    mode->subEntityTraits().setColor( colorIndex );
    double textHeight = m_inRadius * 0.618;
    DrawMText( mode, pt2, 0, m_name, textHeight, AcDbMText::kBottomCenter );
    v.rotateBy( 2 * PI / 3, AcGeVector3d::kZAxis );
    AcGePoint3d pt3 = m_insertPt + v * ( m_outRadius + c );
    AcGePoint3d pt4 = m_insertPt - v * ( m_outRadius + c );
    v.rotateBy( PI / 2, AcGeVector3d::kZAxis );
    AcGePoint3d pt5 = pt3 - v * ( textHeight + 0.5 * c );
    AcGePoint3d pt6 = pt3 + v * ( textHeight + 0.5 * c );
    mode->subEntityTraits().setColor( 1 );
    if( !m_wellHeadEleve.IsEmpty() )
        DrawMText( mode, pt5, 0, m_wellHeadEleve + _T( "(m)" ), textHeight, AcDbMText::kMiddleRight );
    mode->subEntityTraits().setColor( colorIndex );
    if( !m_bottomEleve.IsEmpty() )
        DrawMText( mode, pt6, 0, m_bottomEleve + _T( "(m)" ), textHeight, AcDbMText::kMiddleRight );
    DrawMText( mode, pt4, 0, m_using, textHeight, AcDbMText::kMiddleLeft );
    return Adesk::kTrue;
}
void Shaft::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "名称" ) );
    names.append( _T( "井口高程" ) );
    names.append( _T( "井底高程" ) );
    names.append( _T( "用途" ) );
}
void Shaft::readPropertyDataFromValues( const AcStringArray& values )
{
    m_name = values[0].kACharPtr();
    m_wellHeadEleve = values[1].kACharPtr();
    m_bottomEleve = values[2].kACharPtr();
    m_using = values[3].kACharPtr();
}