#pragma once
#include "MgsGE.h"
//见煤孔
class MGSGE_EXPORT_API NoCoalhole : public MgsGE
{
public:
    ACRX_DECLARE_MEMBERS( NoCoalhole ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    virtual void regPropertyDataNames( AcStringArray& names ) const;
    virtual void readPropertyDataFromValues( const AcStringArray& values );
    // 重载AcDbEntity虚函数(图形交互操作)
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
public:
    NoCoalhole();
    NoCoalhole( const AcGePoint3d& insrtPt );

private:
    double m_outRadius;
    double m_inRadius;
    CString m_idNum;
    CString m_wellHeadEleve;	//井口高程
} ;
#ifdef MGSGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( NoCoalhole )
#endif