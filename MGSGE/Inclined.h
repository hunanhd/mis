#pragma once
#include "MineGE/EdgeGE.h"
#include "dlimexp.h"
// 斜井图元
class MGSGE_EXPORT_API Inclined : public EdgeGE
{
public:
    ACRX_DECLARE_MEMBERS( Inclined ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    Inclined();
    Inclined( const AcGePoint3d& startPt, const AcGePoint3d& endPt );
    virtual ~Inclined();
    // 重载EdgeGE类虚函数(用于实现节点闭合)
public:
    virtual void reverse();
    virtual void dealWithStartPointBoundary( const AcGeRay3d& boundaryLine );
    virtual void dealWithEndPointBoundary( const AcGeRay3d& boundaryLine );
    virtual void extendByLength( double length );
    virtual void regPropertyDataNames( AcStringArray& names ) const;
    virtual void readPropertyDataFromValues( const AcStringArray& values );
    // 重载AcDbObject的虚函数(实现dwg读写)
public:
    virtual Acad::ErrorStatus dwgOutFields ( AcDbDwgFiler* pFiler ) const;
    virtual Acad::ErrorStatus dwgInFields ( AcDbDwgFiler* pFiler );
    // 重载MineGE虚函数(图形交互操作)
protected:
    // 具体的绘制实现
    virtual Adesk::Boolean customWorldDraw( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset ) ;
    // 计算图形的最小多边形
    virtual void minPolygon( AcGePoint3dArray& pts );
private:
    void update();
    void caclStartPoint( AcGePoint3d& startPt, AcGePoint3d& endPt );
    void caclEndPoint( AcGePoint3d& startPt, AcGePoint3d& endPt );
    void DrawTag( AcGiWorldDraw* mode, const AcGePoint3d& leftPt, const AcGePoint3d& rigthPt, double angle, double width, bool ventIn );
private:
    double m_width; // 默认为30
    AcGePoint3d m_leftStartPt, m_leftEndPt;
    AcGePoint3d m_rightStartPt, m_rightEndPt;
    bool m_isIn; //是否为进风
    CString m_name;	//井口名称
    CString m_elevation; //井口标高
    CString m_bevel;	//井筒倾斜角度
} ;
#ifdef MGSGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( Inclined )
#endif
