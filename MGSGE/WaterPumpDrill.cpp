#include "StdAfx.h"
#include "WaterPumpDrill.h"
Adesk::UInt32 WaterPumpDrill::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    WaterPumpDrill, MgsGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    抽水孔, MGSGEAPP
)
WaterPumpDrill::WaterPumpDrill()
{
    m_outRadius = 10;
    m_inRadius = 7;
}
WaterPumpDrill::WaterPumpDrill( const AcGePoint3d& insrtPt ): MgsGE( insrtPt )
{
    m_outRadius = 10;
    m_inRadius = 7;
}
Adesk::Boolean WaterPumpDrill::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    double c = 0.618;
    DrawCircle( mode, m_insertPt, m_outRadius, false );
    DrawCircle( mode, m_insertPt, m_inRadius, false );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    AcGePoint3d leftPt = m_insertPt - v * ( 2 * m_outRadius - m_inRadius );
    AcGePoint3d leftTextPt = m_insertPt - v * ( 2 * m_outRadius - m_inRadius + c );
    AcGePoint3d rightPt = m_insertPt + v * ( 2 * m_outRadius - m_inRadius );
    AcGePoint3d rightTextPt = m_insertPt + v * ( 2 * m_outRadius - m_inRadius + c );
    DrawLine( mode, m_insertPt - v * m_outRadius, leftPt );
    DrawLine( mode, m_insertPt + v * m_outRadius, rightPt );
    v.rotateBy( PI / 2, AcGeVector3d::kZAxis );
    AcGePoint3d topPt = m_insertPt + v * ( 2 * m_outRadius - m_inRadius );
    AcGePoint3d topTextPt = m_insertPt + v * ( 2 * m_outRadius - m_inRadius + c );
    AcGePoint3d bottomPt = m_insertPt - v * ( 2 * m_outRadius - m_inRadius );
    AcGePoint3d bottomTextPt = m_insertPt - v * ( 2 * m_outRadius - m_inRadius + c );
    DrawLine( mode, m_insertPt + v * m_outRadius, topPt );
    DrawLine( mode, m_insertPt - v * m_outRadius, bottomPt );
    v.rotateBy( -PI / 6, AcGeVector3d::kZAxis );
    DrawLine( mode, topPt - v * ( m_outRadius - m_inRadius )*c * 0.5, topPt );
    v.rotateBy( PI / 3, AcGeVector3d::kZAxis );
    DrawLine( mode, topPt - v * ( m_outRadius - m_inRadius )*c * 0.5, topPt );
    v.rotateBy( -PI / 6, AcGeVector3d::kZAxis );
    double textHeight = m_inRadius * 0.618;
    DrawMText( mode, topTextPt, 0, m_idNum, textHeight, AcDbMText::kBottomCenter );
    AcGePoint3d pt1 = leftTextPt + v * ( textHeight + 0.5 * c );
    AcGePoint3d pt2 = leftTextPt - v * ( textHeight + 0.5 * c );
    int colorIndex = mode->subEntityTraits().color();
    mode->subEntityTraits().setColor( 1 );
    if( !m_wellHeadEleve.IsEmpty() )
        DrawMText( mode, pt1, 0, m_wellHeadEleve + _T( "(m)" ), textHeight, AcDbMText::kMiddleRight );
    mode->subEntityTraits().setColor( colorIndex );
    if( !m_bottomEleve.IsEmpty() )
        DrawMText( mode, pt2, 0, m_bottomEleve + _T( "(m)" ), textHeight, AcDbMText::kMiddleRight );
    pt1 = rightTextPt + v * ( textHeight * 0.5 + 1.5 * c );
    pt2 = pt1 + v * ( textHeight + 3 * c );
    AcGePoint3d pt3 = rightTextPt - v * ( textHeight * 0.5 + 1.5 * c );
    AcGePoint3d pt4 = pt3 - v * ( textHeight + 3 * c );
    if( !m_waterColumn.IsEmpty() )
        DrawMText( mode, pt1, 0, m_waterColumn + _T( "(m)" ), textHeight, AcDbMText::kMiddleLeft );
    if( !m_waterEleve.IsEmpty() )
        DrawMText( mode, pt2, 0, m_waterEleve + _T( "(m)" ), textHeight, AcDbMText::kMiddleLeft );
    if( !m_unitGush.IsEmpty() )
        DrawMText( mode, pt3, 0, m_unitGush + _T( "(L/s·m)" ), textHeight, AcDbMText::kMiddleLeft );
    if( !m_permeability.IsEmpty() )
        DrawMText( mode, pt4, 0, m_permeability + _T( "(m/d)" ), textHeight, AcDbMText::kMiddleLeft );
    if( m_hasCoal )
        DrawMText( mode, bottomTextPt, 0, _T( "(见煤)" ), textHeight, AcDbMText::kTopCenter );
    if( !m_hasCoal )
        DrawMText( mode, bottomTextPt, 0, _T( "(未见煤)" ), textHeight, AcDbMText::kTopCenter );
    return Adesk::kTrue;
}
void WaterPumpDrill::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "孔号" ) );
    names.append( _T( "孔口高程" ) );
    names.append( _T( "底板高程" ) );
    names.append( _T( "水位高程" ) );
    names.append( _T( "水柱高度" ) );
    names.append( _T( "单位涌水量" ) );
    names.append( _T( "渗透系数" ) );
    names.append( _T( "是否见煤" ) );
}
void WaterPumpDrill::readPropertyDataFromValues( const AcStringArray& values )
{
    m_idNum = values[0].kACharPtr();
    m_wellHeadEleve = values[1].kACharPtr();
    m_bottomEleve = values[2].kACharPtr();
    m_waterEleve = values[3].kACharPtr();
    m_waterColumn = values[4].kACharPtr();
    m_unitGush = values[5].kACharPtr();
    m_permeability = values[6].kACharPtr();
    CString strHasCoal;
    strHasCoal.Format( _T( "%s" ), values[7].kACharPtr() );
    m_hasCoal = ( _T( "1" ) == strHasCoal ) ? true : false;
}