#pragma once
#include "MineGE/MineGE.h"
#include "dlimexp.h"
#include "MineGE/DrawTool.h"
//煤矿地质测量钻孔抽象图元
class MGSGE_EXPORT_API MgsGE : public MineGE
{
public:
    ACRX_DECLARE_MEMBERS( MgsGE ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
    // 重载AcDbObject的虚函数(实现dwg读写)
public:
    virtual Acad::ErrorStatus dwgOutFields ( AcDbDwgFiler* pFiler ) const;
    virtual Acad::ErrorStatus dwgInFields ( AcDbDwgFiler* pFiler );
    virtual void regPropertyDataNames( AcStringArray& names ) const {}
    virtual void readPropertyDataFromValues( const AcStringArray& values ) {}
    // 重载AcDbEntity虚函数(图形交互操作)
protected:
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
protected:
    MgsGE();
    virtual ~MgsGE();
    MgsGE( const AcGePoint3d& insrtPt );

protected:
    AcGePoint3d m_insertPt;
} ;
#ifdef MGSGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( MgsGE )
#endif