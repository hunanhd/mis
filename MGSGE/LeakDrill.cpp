#include "StdAfx.h"
#include "LeakDrill.h"
#include "MineGE/DrawSpecial.h"
Adesk::UInt32 LeakDrill::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    LeakDrill, MgsGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    漏水孔, MGSGEAPP
)
LeakDrill::LeakDrill()
{
    m_outRadius = 10;
    m_inRadius = 7;
}
LeakDrill::LeakDrill( const AcGePoint3d& insrtPt ): MgsGE( insrtPt )
{
    m_outRadius = 10;
    m_inRadius = 7;
}
Adesk::Boolean LeakDrill::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    double c = 0.618;
    DrawCircle( mode, m_insertPt, m_outRadius, false );
    DrawCircle( mode, m_insertPt, m_inRadius, false );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    AcGePoint3d leftPt = m_insertPt - v * ( 2 * m_outRadius - m_inRadius );
    AcGePoint3d leftTextPt = m_insertPt - v * ( 2 * m_outRadius - m_inRadius + c );
    AcGePoint3d rightPt = m_insertPt + v * ( 2 * m_outRadius - m_inRadius );
    AcGePoint3d rightTextPt = m_insertPt + v * ( 2 * m_outRadius - m_inRadius + c );
    DrawLine( mode, m_insertPt - v * m_outRadius, leftPt );
    DrawLine( mode, m_insertPt + v * m_outRadius, rightPt );
    v.rotateBy( PI / 2, AcGeVector3d::kZAxis );
    AcGePoint3d topPt = m_insertPt + v * ( 2 * m_outRadius - m_inRadius );
    AcGePoint3d topTextPt = m_insertPt + v * ( 2 * m_outRadius - m_inRadius + c );
    AcGePoint3d bottomPt = m_insertPt - v * ( m_outRadius );
    DrawLine( mode, m_insertPt + v * m_outRadius, topPt );
    AcGePoint3d pt = bottomPt - v * ( ( m_outRadius - m_inRadius ) / 2 );
    DrawWave( mode, pt, -PI / 2, -PI / 5, m_outRadius - m_inRadius );
    double textHeight = m_inRadius * 0.618;
    DrawMText( mode, topTextPt, 0, m_idNum, textHeight, AcDbMText::kBottomCenter );
    AcGePoint3d pt1 = leftTextPt + v * ( textHeight + 0.5 * c );
    AcGePoint3d pt2 = leftTextPt - v * ( textHeight + 0.5 * c );
    int colorIndex = mode->subEntityTraits().color();
    mode->subEntityTraits().setColor( 1 );
    if( !m_wellHeadEleve.IsEmpty() )
        DrawMText( mode, pt1, 0, m_wellHeadEleve + _T( "(m)" ), textHeight, AcDbMText::kMiddleRight );
    mode->subEntityTraits().setColor( colorIndex );
    if( !m_bottomEleve.IsEmpty() )
        DrawMText( mode, pt2, 0, m_bottomEleve + _T( "(m)" ), textHeight, AcDbMText::kMiddleRight );
    pt1 = rightTextPt + v * ( textHeight * 0.5 + 1.5 * c );
    pt2 = pt1 + v * ( textHeight + 3 * c );
    AcGePoint3d pt3 = rightTextPt - v * ( textHeight * 0.5 + 1.5 * c );
    AcGePoint3d pt4 = pt3 - v * ( textHeight + 3 * c );
    if( !m_waterColumn.IsEmpty() )
        DrawMText( mode, pt1, 0, m_waterColumn + _T( "(m)" ), textHeight, AcDbMText::kMiddleLeft );
    if( !m_waterEleve.IsEmpty() )
        DrawMText( mode, pt2, 0, m_waterEleve + _T( "(m)" ), textHeight, AcDbMText::kMiddleLeft );
    if( !m_leakDepth.IsEmpty() )
        DrawMText( mode, pt3, 0, m_leakDepth + _T( "(m)" ), textHeight, AcDbMText::kMiddleLeft );
    if( !m_leakQuanty.IsEmpty() )
        DrawMText( mode, pt4, 0, m_leakQuanty + _T( "(L/min)" ), textHeight, AcDbMText::kMiddleLeft );
    return Adesk::kTrue;
}
void LeakDrill::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "孔号" ) );
    names.append( _T( "孔口高程" ) );
    names.append( _T( "底板高程" ) );
    names.append( _T( "水位高程" ) );
    names.append( _T( "水柱高度" ) );
    names.append( _T( "漏失深度" ) );
    names.append( _T( "漏失量" ) );
}
void LeakDrill::readPropertyDataFromValues( const AcStringArray& values )
{
    m_idNum = values[0].kACharPtr();
    m_wellHeadEleve = values[1].kACharPtr();
    m_bottomEleve = values[2].kACharPtr();
    m_waterEleve = values[3].kACharPtr();
    m_waterColumn = values[4].kACharPtr();
    m_leakDepth = values[5].kACharPtr();
    m_leakQuanty = values[6].kACharPtr();
}
