#include "StdAfx.h"
#include "WaterDrill.h"
Adesk::UInt32 WaterDrill::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    WaterDrill, MgsGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    ̽ˮ���, MGSGEAPP
)
WaterDrill::WaterDrill()
{
    m_radius = 10;
}
WaterDrill::WaterDrill( const AcGePoint3d& insrtPt ): MgsGE( insrtPt )
{
    m_radius = 10;
}
Adesk::Boolean WaterDrill::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    double c = 0.618;
    DrawCircle( mode, m_insertPt, m_radius, false );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( PI / 2, AcGeVector3d::kZAxis );
    double angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    AcGePoint3d pt1 = m_insertPt + v * ( 2.5 * m_radius );
    DrawPolyLine( mode, m_insertPt, pt1, 1 );
    DrawArrow( mode, pt1, angle, 4, m_radius * c );
    AcGePoint3d pt2 = m_insertPt + v * ( 3.5 * m_radius );
    int colorIndex = mode->subEntityTraits().color();
    double textHeight = m_radius * 0.618;
    mode->subEntityTraits().setColor( 1 );
    DrawMText( mode, pt2, 0, m_idNum, textHeight, AcDbMText::kBottomCenter );
    v.rotateBy( -PI / 2, AcGeVector3d::kZAxis );
    AcGePoint3d pt3 = m_insertPt + v * ( c + m_radius );
    v.rotateBy( PI / 2, AcGeVector3d::kZAxis );
    AcGePoint3d pt4 = pt3 + v * ( textHeight + c );
    AcGePoint3d pt5 = pt4 + v * ( textHeight + c );
    mode->subEntityTraits().setColor( colorIndex );
    CString str;
    str.Format( _T( "(m%s)" ), MakeUpperText( _T( "3" ) ) );
    if( !m_waterQ.IsEmpty() )
        DrawMText( mode, pt3, 0, m_waterQ + str, textHeight, AcDbMText::kTopLeft );
    if( !m_VDis.IsEmpty() )
        DrawMText( mode, pt4, 0, m_VDis + _T( "(m)" ), textHeight, AcDbMText::kMiddleLeft );
    if( !m_HDist.IsEmpty() )
        DrawMText( mode, pt5, 0, m_HDist + _T( "(m)" ), textHeight, AcDbMText::kBottomLeft );
    return Adesk::kTrue;
}
void WaterDrill::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "�׺�" ) );
    names.append( _T( "ƽ��" ) );
    names.append( _T( "����" ) );
    names.append( _T( "йˮ��" ) );
}
void WaterDrill::readPropertyDataFromValues( const AcStringArray& values )
{
    m_idNum = values[0].kACharPtr();
    m_HDist = values[1].kACharPtr();
    m_VDis = values[2].kACharPtr();
    m_waterQ = values[3].kACharPtr();
}