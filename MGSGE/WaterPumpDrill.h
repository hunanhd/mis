#pragma once
#include "MgsGE.h"
//抽水孔
class MGSGE_EXPORT_API WaterPumpDrill : public MgsGE
{
public:
    ACRX_DECLARE_MEMBERS( WaterPumpDrill ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    virtual void regPropertyDataNames( AcStringArray& names ) const;
    virtual void readPropertyDataFromValues( const AcStringArray& values );
    // 重载AcDbEntity虚函数(图形交互操作)
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
public:
    WaterPumpDrill();
    WaterPumpDrill( const AcGePoint3d& insrtPt );

private:
    double m_outRadius;
    double m_inRadius;
    CString m_idNum;
    CString m_wellHeadEleve;	//井口高程
    CString m_bottomEleve;		//井底高程
    CString m_waterEleve;		//水位高程
    CString m_waterColumn;		//水柱高度
    CString m_unitGush;		//单位涌水量
    CString m_permeability;		//渗透系数
    bool m_hasCoal;				//是否见煤
} ;
#ifdef MGSGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( WaterPumpDrill )
#endif