#include "StdAfx.h"
#include "NoCoalhole.h"
Adesk::UInt32 NoCoalhole::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    NoCoalhole, MgsGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    δ��ú��, MGSGEAPP
)
NoCoalhole::NoCoalhole()
{
    m_outRadius = 12;
    m_inRadius = 7;
}
NoCoalhole::NoCoalhole( const AcGePoint3d& insrtPt ): MgsGE( insrtPt )
{
    m_outRadius = 12;
    m_inRadius = 7;
}
Adesk::Boolean NoCoalhole::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    double c = 0.618;
    DrawCircle( mode, m_insertPt, m_outRadius, false );
    DrawCircle( mode, m_insertPt, m_inRadius, false );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( PI / 2, AcGeVector3d::kZAxis );
    AcGePoint3d pt1 = m_insertPt + v * ( m_outRadius + 2 * c );
    int colorIndex = mode->subEntityTraits().color();
    double textHeight = m_inRadius * 0.618;
    DrawMText( mode, pt1, 0, m_idNum, textHeight, AcDbMText::kBottomCenter );
    v.rotateBy( PI / 2, AcGeVector3d::kZAxis );
    AcGePoint3d pt2 = m_insertPt + v * ( m_outRadius + c );
    mode->subEntityTraits().setColor( 1 );
    if( !m_wellHeadEleve.IsEmpty() )
        DrawMText( mode, pt2, 0, m_wellHeadEleve + _T( "(m)" ), textHeight, AcDbMText::kMiddleRight );
    mode->subEntityTraits().setColor( colorIndex );
    return Adesk::kTrue;
}
void NoCoalhole::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "�׺�" ) );
    names.append( _T( "�׿ڸ߳�" ) );
}
void NoCoalhole::readPropertyDataFromValues( const AcStringArray& values )
{
    m_idNum = values[0].kACharPtr();
    m_wellHeadEleve = values[1].kACharPtr();
}