#include "StdAfx.h"
#include "MgsGE.h"
Adesk::UInt32 MgsGE::kCurrentVersionNumber = 1 ;
ACRX_NO_CONS_DEFINE_MEMBERS ( MgsGE,  MineGE )
Acad::ErrorStatus MgsGE::dwgOutFields( AcDbDwgFiler* pFiler ) const
{
    assertReadEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgOutFields"));
    Acad::ErrorStatus es = MineGE::dwgOutFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    if ( ( es = pFiler->writeUInt32 ( MgsGE::kCurrentVersionNumber ) ) != Acad::eOk )
        return ( es ) ;
    pFiler->writeItem( m_insertPt );
    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus MgsGE::dwgInFields( AcDbDwgFiler* pFiler )
{
    assertWriteEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgInFields"));
    Acad::ErrorStatus es = MineGE::dwgInFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    Adesk::UInt32 version = 0 ;
    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
        return ( es ) ;
    if ( version > MgsGE::kCurrentVersionNumber )
        return ( Acad::eMakeMeProxy ) ;
    pFiler->readItem( &m_insertPt );
    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus MgsGE::customTransformBy( const AcGeMatrix3d& xform )
{
    m_insertPt.transformBy( xform );
    return Acad::eOk;
}
Acad::ErrorStatus MgsGE::customGetOsnapPoints( AcDb::OsnapMode osnapMode, Adesk::GsMarker gsSelectionMark, const AcGePoint3d& pickPoint, const AcGePoint3d& lastPoint, const AcGeMatrix3d& viewXform, AcGePoint3dArray& snapPoints, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉1种类型的点：插入点
    Acad::ErrorStatus es = Acad::eOk;
    if ( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return es;
}
Acad::ErrorStatus MgsGE::customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    return Acad::eOk;
}
Acad::ErrorStatus MgsGE::customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        if ( idx == 0 ) m_insertPt += offset;
    }
    return Acad::eOk;
}
MgsGE::MgsGE(): MineGE ()
{
}
MgsGE::MgsGE( const AcGePoint3d& insrtPt ): MineGE (), m_insertPt( insrtPt )
{
}
MgsGE::~MgsGE()
{
}
