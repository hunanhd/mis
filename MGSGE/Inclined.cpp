#include "StdAfx.h"
#include "Inclined.h"
#include "MineGE/Drawtool.h"
#include <cmath>
Adesk::UInt32 Inclined::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    Inclined, EdgeGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    斜井, MGSGEAPP
)
Inclined::Inclined () : EdgeGE ()
{
    m_width = 30;
}
Inclined::Inclined( const AcGePoint3d& startPt, const AcGePoint3d& endPt ) : EdgeGE( startPt, endPt )
{
    m_width = 30;
    update();
}
Inclined::~Inclined ()
{
}
void Inclined::update()
{
    caclStartPoint( m_leftStartPt, m_rightStartPt );
    caclEndPoint( m_leftEndPt, m_rightEndPt );
}
void Inclined::caclStartPoint( AcGePoint3d& startPt1, AcGePoint3d& startPt2 )
{
    AcGeVector3d v = m_endPt - m_startPt;
    v.normalize();
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    startPt1 = m_startPt + v * m_width * 0.5;
    v.rotateBy( PI, AcGeVector3d::kZAxis );
    startPt2 = m_startPt + v * m_width * 0.5;
}
void Inclined::caclEndPoint( AcGePoint3d& endPt1, AcGePoint3d& endPt2 )
{
    AcGeVector3d v = m_endPt - m_startPt;
    v.normalize();
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    endPt1 = m_endPt + v * m_width * 0.5;
    v.rotateBy( PI, AcGeVector3d::kZAxis );
    endPt2 = m_endPt + v * m_width * 0.5;
}
void Inclined::dealWithStartPointBoundary( const AcGeRay3d& boundaryLine )
{
    AcGeLine3d line( m_leftStartPt, m_leftEndPt );
    AcGePoint3d pt;
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) ) // 计算左侧轮廓线与边界线的交叉点
    {
        //acutPrintf(_T("\n调整左侧轮廓线的始点坐标%.3f, %.3f"), pt.x, pt.y);
        m_leftStartPt = pt;                        // 调整左侧轮廓线的始点坐标
    }
    line.set( m_rightStartPt, m_rightEndPt );
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) ) // 计算右侧轮廓线与边界线的交叉点
    {
        //acutPrintf(_T("\n调整右侧轮廓线的始点坐标%.3f, %.3f"), pt.x, pt.y);
        m_rightStartPt = pt;                       // 调整右侧轮廓线的始点坐标
    }
}
void Inclined::dealWithEndPointBoundary( const AcGeRay3d& boundaryLine )
{
    AcGeLine3d line( m_leftStartPt, m_leftEndPt );
    AcGePoint3d pt;
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) ) // 计算左侧轮廓线与边界线的交叉点
    {
        //acutPrintf(_T("\n调整左侧轮廓线的末点坐标%.3f, %.3f"), pt.x, pt.y);
        m_leftEndPt = pt;                                         // 调整左侧轮廓线的末点坐标
    }
    line.set( m_rightStartPt, m_rightEndPt );
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) ) // 计算右侧轮廓线与边界线的交叉点
    {
        //acutPrintf(_T("\n调整右侧轮廓线的末点坐标%.3f, %.3f"), pt.x, pt.y);
        m_rightEndPt = pt;                          // 调整右侧轮廓线的末点坐标
    }
}
void Inclined::reverse()
{
    EdgeGE::reverse();
    update();
}
void Inclined::extendByLength( double length )
{
    AcGeVector3d v = m_endPt - m_startPt;
    v.normalize();
    m_endPt = m_endPt + v * length; // 更新末点坐标
    update(); // 更新其它参数
}
Acad::ErrorStatus Inclined::dwgOutFields ( AcDbDwgFiler* pFiler ) const
{
    assertReadEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgOutFields"));
    Acad::ErrorStatus es = EdgeGE::dwgOutFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    if ( ( es = pFiler->writeUInt32 ( Inclined::kCurrentVersionNumber ) ) != Acad::eOk )
        return ( es ) ;
    // 保存始末点坐标
    pFiler->writeItem( m_leftStartPt );
    pFiler->writeItem( m_leftEndPt );
    pFiler->writeItem( m_rightStartPt );
    pFiler->writeItem( m_rightEndPt );
    pFiler->writeItem( m_width );
    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus Inclined::dwgInFields ( AcDbDwgFiler* pFiler )
{
    assertWriteEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgInFields"));
    Acad::ErrorStatus es = EdgeGE::dwgInFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    Adesk::UInt32 version = 0 ;
    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
        return ( es ) ;
    if ( version > Inclined::kCurrentVersionNumber )
        return ( Acad::eMakeMeProxy ) ;
    // 读取始末点坐标
    pFiler->readItem( &m_leftStartPt );
    pFiler->readItem( &m_leftEndPt );
    pFiler->readItem( &m_rightStartPt );
    pFiler->readItem( &m_rightEndPt );
    pFiler->readItem( &m_width );
    return ( pFiler->filerStatus () ) ;
}
void Inclined::minPolygon( AcGePoint3dArray& pts )
{
    pts.append( m_startPt );
    pts.append( m_leftStartPt );
    pts.append( m_leftEndPt );
    pts.append( m_endPt );
    pts.append( m_rightEndPt );
    pts.append( m_rightStartPt );
}
void Inclined::DrawTag( AcGiWorldDraw* mode, const AcGePoint3d& leftPt, const AcGePoint3d& rigthPt, double angle, double width, bool ventIn )
{
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( angle, AcGeVector3d::kZAxis );
    if( !ventIn )
    {
        v.negate();
        angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    }
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    DrawLine( mode, rigthPt, rigthPt - ( ventIn ? 1 : -1 )*v * width * 0.5 );
    DrawLine( mode, leftPt, leftPt + ( ventIn ? 1 : -1 )*v * width * 0.5 );
    CString str;
    str.Format( _T( "%sm" ), m_elevation );
    if( !m_elevation.IsEmpty() )
    {
        if( ventIn )
        {
            DrawMText( mode, rigthPt - v * width * 0.5, angle + PI / 2, str, width / 6, AcDbMText::kMiddleRight );
            DrawMText( mode, leftPt + v * width * 0.5, angle + PI / 2, m_name, width / 6, AcDbMText::kMiddleLeft );
        }
        else
        {
            DrawMText( mode, leftPt - v * width * 0.5, angle + PI / 2, str, width / 6, AcDbMText::kMiddleRight );
            DrawMText( mode, rigthPt + v * width * 0.5, angle + PI / 2, m_name, width / 6, AcDbMText::kMiddleLeft );
        }
    }
    v.rotateBy( -PI * 0.5, AcGeVector3d::kZAxis );
    AcGePoint3d pt1 = leftPt + v * width * 0.5;
    AcGePoint3d pt2 = rigthPt + v * width * 0.5;
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    DrawLine( mode, pt2, pt2 - ( ventIn ? 1 : -1 )*v * width * 0.25 );
    DrawLine( mode, pt1, pt1 + ( ventIn ? 1 : -1 )*v * width * 0.25 );
    v.rotateBy( -PI * 0.5, AcGeVector3d::kZAxis );
    AcGePoint3d pt3 = leftPt + v * width;
    AcGePoint3d pt4 = rigthPt + v * width;
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    DrawLine( mode, pt4, pt4 - ( ventIn ? 1 : -1 )*v * width * 0.25 * 0.5 );
    DrawLine( mode, pt3, pt3 + ( ventIn ? 1 : -1 )*v * width * 0.25 * 0.5 );
    v.rotateBy( -PI * 0.5, AcGeVector3d::kZAxis );
    AcGePoint3d pt5;
    if( ventIn )
    {
        pt5 = leftPt + v * width * 1.5;
    }
    else
    {
        pt5 = rigthPt + v * width * 1.5;
    }
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    AcGePoint3d pt6 = pt5 + v * width * 0.25;
    DrawLine( mode, pt5, pt6 );
    v.rotateBy( -PI * 0.5, AcGeVector3d::kZAxis );
    AcGePoint3d pt7 = pt6 + v * width * 0.68;
    DrawLine( mode, pt6, pt7 );
    DrawArrow( mode, pt7, angle, width / 10, width * 0.25 );
    str.Format( _T( "%s%s" ), m_bevel, MakeUpperText( _T( "°" ) ) );
    if( m_bevel.IsEmpty() ) return;
    DrawMText( mode, pt7 + v * width * 0.45, angle + PI / 2, str, width / 6, AcDbMText::kBottomCenter );
}
Adesk::Boolean Inclined::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled();
    double c = 0.618;
    DrawLine( mode, m_leftStartPt, m_leftEndPt );   // 绘制左线
    DrawLine( mode, m_rightStartPt, m_rightEndPt );	// 绘制右线
    if ( !m_isIn )
    {
        DrawTag( mode, m_leftEndPt, m_rightEndPt, getAngle(), m_width, false );
    }
    else
    {
        DrawTag( mode, m_leftStartPt, m_rightStartPt, getAngle(), m_width, true );
    }
    return Adesk::kTrue;
}
Acad::ErrorStatus Inclined::customTransformBy( const AcGeMatrix3d& xform )
{
    m_startPt.transformBy( xform );
    m_endPt.transformBy( xform );
    update();
    return Acad::eOk;
}
Acad::ErrorStatus Inclined::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉端点
    if ( osnapMode == AcDb::kOsModeEnd )
    {
        snapPoints.append( m_startPt );
        snapPoints.append( m_endPt );
    }
    return Acad::eOk;
}
Acad::ErrorStatus Inclined::customGetGripPoints ( AcGePoint3dArray& gripPoints,
        AcDbIntArray& osnapModes,
        AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_startPt );
    if( m_startPt == m_endPt )
    {
        AcGePoint3d pt( m_startPt );
        pt.x = pt.x + m_width * 0.3;
        gripPoints.append( pt );
    }
    else
    {
        gripPoints.append( m_endPt );
    }
    return Acad::eOk;
}
Acad::ErrorStatus Inclined::customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        if ( idx == 0 )
        {
            m_startPt += offset;
        }
        if ( idx == 1 )
        {
            m_endPt += offset;
        }
        update();
    }
    return Acad::eOk;
}
void Inclined::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "通风类型" ) );
    names.append( _T( "井口标高" ) );
    names.append( _T( "井口名称" ) );
    names.append( _T( "井筒倾角" ) );
}
void Inclined::readPropertyDataFromValues( const AcStringArray& values )
{
    CString strVentType;
    strVentType.Format( _T( "%s" ), values[0].kACharPtr() );
    m_isIn = ( _T( "1" ) == strVentType ) ? false : true;
    m_elevation = values[1].kACharPtr();
    m_name = values[2].kACharPtr();
    m_bevel = values[3].kACharPtr();
}