#pragma once
#include "MgsGE.h"
//竖井
class MGSGE_EXPORT_API Shaft : public MgsGE
{
public:
    ACRX_DECLARE_MEMBERS( Shaft ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    virtual void regPropertyDataNames( AcStringArray& names ) const;
    virtual void readPropertyDataFromValues( const AcStringArray& values );
    // 重载AcDbEntity虚函数(图形交互操作)
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
public:
    Shaft();
    Shaft( const AcGePoint3d& insrtPt );

private:
    double m_outRadius;
    double m_inRadius;
    CString m_name;
    CString m_wellHeadEleve;	//井口高程
    CString m_bottomEleve;		//井底高程
    CString m_using;			//用途
} ;
#ifdef MGSGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( Shaft )
#endif