#include "StdAfx.h"
#include "Coalhole.h"
Adesk::UInt32 Coalhole::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    Coalhole, MgsGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    ��ú���, MGSGEAPP
)
Coalhole::Coalhole()
{
    m_outRadius = 12;
    m_inRadius = 7;
}
Coalhole::Coalhole( const AcGePoint3d& insrtPt ): MgsGE( insrtPt )
{
    m_outRadius = 12;
    m_inRadius = 7;
}
Adesk::Boolean Coalhole::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    double c = 0.618;
    DrawCircle( mode, m_insertPt, m_outRadius, false );
    DrawCircle( mode, m_insertPt, m_inRadius, true );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( PI / 2, AcGeVector3d::kZAxis );
    AcGePoint3d pt1 = m_insertPt + v * ( m_outRadius + 2 * c );
    int colorIndex = mode->subEntityTraits().color();
    double textHeight = m_inRadius * 0.618;
    DrawMText( mode, pt1, 0, m_idNum, textHeight, AcDbMText::kBottomCenter );
    v.rotateBy( PI / 2, AcGeVector3d::kZAxis );
    AcGePoint3d pt2 = m_insertPt + v * ( m_outRadius + c );
    AcGePoint3d pt3 = m_insertPt - v * ( m_outRadius + c );
    v.rotateBy( PI / 2, AcGeVector3d::kZAxis );
    AcGePoint3d pt4 = pt2 - v * ( textHeight + 0.5 * c );
    AcGePoint3d pt5 = pt2 + v * ( textHeight + 0.5 * c );
    mode->subEntityTraits().setColor( 1 );
    if( !m_wellHeadEleve.IsEmpty() )
        DrawMText( mode, pt4, 0, m_wellHeadEleve + _T( "(m)" ), textHeight, AcDbMText::kMiddleRight );
    mode->subEntityTraits().setColor( colorIndex );
    if( !m_bottomEleve.IsEmpty() )
        DrawMText( mode, pt5, 0, m_bottomEleve + _T( "(m)" ), textHeight, AcDbMText::kMiddleRight );
    if( !m_thick.IsEmpty() )
        DrawMText( mode, pt3, 0, m_thick + _T( "(m)" ), textHeight, AcDbMText::kMiddleLeft );
    return Adesk::kTrue;
}
void Coalhole::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "�׺�" ) );
    names.append( _T( "�׿ڸ߳�" ) );
    names.append( _T( "�װ�߳�" ) );
    names.append( _T( "���" ) );
}
void Coalhole::readPropertyDataFromValues( const AcStringArray& values )
{
    m_idNum = values[0].kACharPtr();
    m_wellHeadEleve = values[1].kACharPtr();
    m_bottomEleve = values[2].kACharPtr();
    m_thick = values[3].kACharPtr();
}