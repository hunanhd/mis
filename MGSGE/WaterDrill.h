#pragma once
#include "MgsGE.h"
//̽ˮ���
class MGSGE_EXPORT_API WaterDrill : public MgsGE
{
public:
    ACRX_DECLARE_MEMBERS( WaterDrill ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    virtual void regPropertyDataNames( AcStringArray& names ) const;
    virtual void readPropertyDataFromValues( const AcStringArray& values );
    // ����AcDbEntity�麯��(ͼ�ν�������)
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
public:
    WaterDrill();
    WaterDrill( const AcGePoint3d& insrtPt );

private:
    double m_radius;
    CString m_idNum;
    CString m_HDist;	//ƽ��
    CString m_VDis;		//����
    CString m_waterQ;			//йˮ��
} ;
#ifdef MGSGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( WaterDrill )
#endif