#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// 皮托管图元
class VENTGE_EXPORT_API PitotTube : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( PitotTube ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    PitotTube() ;
    PitotTube( const AcGePoint3d& insertPt, double angle );
    virtual ~PitotTube();
    // 重载AcDbObject的虚函数(实现dwg读写)
public:
    virtual Acad::ErrorStatus dwgOutFields ( AcDbDwgFiler* pFiler ) const;
    virtual Acad::ErrorStatus dwgInFields ( AcDbDwgFiler* pFiler );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
private:
    double m_length; //长度
    AcGePoint3d m_pt1;
    AcGePoint3d m_pt2;
} ;
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( PitotTube )
#endif