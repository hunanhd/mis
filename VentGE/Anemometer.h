#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// ���վ
class VENTGE_EXPORT_API Anemometer : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( Anemometer ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    Anemometer() ;
    Anemometer( const AcGePoint3d& insertPt, double angle );
    virtual ~Anemometer();
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
protected:
    double m_width;
    double m_length;
    double m_length0;
} ;
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( Anemometer )
#endif