#include "StdAfx.h"
#include "ArcTunnel.h"
Adesk::UInt32 ArcTunnel::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    ArcTunnel, EdgeGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    弧线巷道, VENTGEAPP
)
ArcTunnel::ArcTunnel( void ) : EdgeGE()
{
    map( _T( "宽度" ), &m_width );
    map( _T( "第三点坐标" ), &m_thirdPt );
    m_width = 30;
}
ArcTunnel::ArcTunnel( const AcGePoint3d& startPt, const AcGePoint3d& endPt, const AcGePoint3d& thirdPt )
    : EdgeGE( startPt, endPt ), m_thirdPt( thirdPt )
{
    map( _T( "宽度" ), &m_width );
    map( _T( "第三点坐标" ), &m_thirdPt );
    m_width = 30;
    update();
}
AcGePoint3d ArcTunnel::getThirdPt() const
{
    assertReadEnabled();
    return m_thirdPt;
}
void ArcTunnel::setThirdPt( const AcGePoint3d& pt )
{
    assertWriteEnabled();
    m_thirdPt = pt;
    update();
}
AcGePoint3d ArcTunnel::getArcCntPt() const
{
    assertReadEnabled();
    AcGeCircArc3d arc( m_startPt, m_endPt, m_thirdPt );
    AcGePoint3d cnt = arc.center();//圆弧原点
    double radius = arc.radius();//圆弧半径
    AcGeVector3d v = m_startPt - m_endPt;
    AcGePoint3d sePt = m_startPt + v * 0.5;//圆弧始末节点的中点
    AcGeVector3d vt = m_thirdPt - sePt;
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis ); //把始末向量旋转90度
    double anglev = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    double anglevt = vt.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    double cp = abs( anglev - anglevt );
    if ( cp > PI ) //保证夹角为锐角
    {
        cp = 2 * PI - cp;
    }
    if ( cp > PI * 0.5 ) //如果夹角大于90度，说明要获取的点在圆弧的另一面
    {
        v = -v;
    }
    v.normalize();
    return cnt + v * radius;
}
bool ArcTunnel::ptIsOnArc( AcGePoint3d pt ) const
{
    AcGePoint3d cntArcPt = getArcCntPt();
    AcGePoint3d cntSEPt = AcGePoint3d( ( m_startPt.x + m_endPt.x ) / 2, ( m_startPt.y + m_endPt.y ) / 2, 0 );
    AcGeVector3d ptVec = pt - cntSEPt;
    AcGeVector3d cntArcVec = cntArcPt - cntSEPt;
    double anglePtV = ptVec.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    double angleCntArcPtV = cntArcVec.angleTo( AcGeVector3d::kXAxis, AcGeVector3d::kZAxis );
    double cp = abs( anglePtV - angleCntArcPtV );
    if ( cp > PI ) //保证夹角为锐角
    {
        cp = 2 * PI - cp;
    }
    bool ret = false;
    if ( cp < PI * 0.5 ) //如果夹角大于90度，说明改点在圆弧的另一面
    {
        ret = true;
    }
    return ret;
}
double ArcTunnel::getArcAngle( AcGePoint3d insertPt )
{
    AcGePoint3d arcCentPt = getArcCntPt();
    AcGeCircArc3d arc( m_startPt, arcCentPt, m_endPt );
    AcGePoint3d cnt = arc.center();
    AcGeVector3d sptToInsertPtVec = insertPt - m_startPt;
    double sptInsertPtLenth = sptToInsertPtVec.lengthSqrd();
    AcGeVector3d eptToInsertPtVec = insertPt - m_endPt;
    double eptInsertPtLenth = eptToInsertPtVec.lengthSqrd();
    AcGeVector3d cntToPtVec;
    //acutPrintf(_T("\n距始点的距离是:%f,距末点的距离是:%f"),sptInsertPtLenth,eptInsertPtLenth);
    if ( sptInsertPtLenth > eptInsertPtLenth )
    {
        //acutPrintf(_T("\n距离末点比较近,距离是:%f"),eptInsertPtLenth);
        AcGeVector3d thirdPtToEptVec = m_endPt - arcCentPt;
        thirdPtToEptVec.normalize();
        cntToPtVec = insertPt - cnt;
        cntToPtVec.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
        cntToPtVec.normalize();
        double angleTE = cntToPtVec.angleTo( thirdPtToEptVec/*,-AcGeVector3d::kZAxis*/ );
        //acutPrintf(_T("\n相对角度是:%f"),angleTE);
        //if (angleTE > PI)
        //{
        //	angleTE = 2*PI - angleTE;
        //}
        if ( angleTE > 0.5 * PI )
        {
            cntToPtVec = - cntToPtVec;
        }
    }
    else
    {
        //acutPrintf(_T("\n距离始点比较近,距离是:%f"),sptInsertPtLenth);
        AcGeVector3d sptToThirdPtVec = arcCentPt - m_startPt;
        sptToThirdPtVec.normalize();
        cntToPtVec = insertPt - cnt;
        cntToPtVec.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
        cntToPtVec.normalize();
        double angleST = cntToPtVec.angleTo( sptToThirdPtVec/*,-AcGeVector3d::kZAxis*/ );
        //acutPrintf(_T("\n相对角度是:%f"),angleST);
        //if (angleST > PI)
        //{
        //	angleST = 2*PI - angleST;
        //}
        if ( angleST > 0.5 * PI )
        {
            cntToPtVec = - cntToPtVec;
        }
    }
    return cntToPtVec.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
}
static void DividArc( const AcGePoint3d& spt, const AcGePoint3d& ept, const AcGePoint3d& thirdPt,
                      int count, AcGePoint3dArray& pts )
{
    pts.append( spt );
    AcGeCircArc3d arc( spt, thirdPt, ept );
    AcGeVector3d v = arc.refVec();
    int c = ( arc.normal().z > 0 ? 1 : -1 );
    AcGePoint3d cnt = arc.center();
    double dq = abs( arc.endAng() - arc.startAng() ) / count;
    //acutPrintf(_T("\na1:%.3f, a2:%.3f dq:%.3f c:%d"), a1, a2, dq, c);
    for( int i = 1; i < count; i++ )
    {
        v.rotateBy( c * dq, AcGeVector3d::kZAxis );
        pts.append( cnt + v * arc.radius() );
    }
    pts.append( ept );
}
void ArcTunnel::reverse()
{
    EdgeGE::reverse();
    update();
}
void ArcTunnel::minPolygon( AcGePoint3dArray& pts )
{
    AcGeCircArc3d arc( m_startPt, m_thirdPt, m_endPt );
    AcGePoint3d cnt = arc.center();
    double radius = arc.radius();
    AcGeVector3d v = m_thirdPt - cnt;
    v.normalize();
    pts.append( m_startPt );
    DividArc( m_rightStartPt, m_rightEndPt, cnt + v * ( radius - m_width * 0.5 ), 32, pts );
    pts.append( m_endPt );
    DividArc( m_leftEndPt, m_leftStartPt, cnt + v * ( radius + m_width * 0.5 ), 32, pts );
}
AcGeVector3d ArcTunnel::getStartPointInExtendAngle() const
{
    AcGeCircArc3d arc( m_startPt, m_thirdPt, m_endPt );
    AcGeVector3d v = arc.refVec();
    //acutPrintf(_T("\n%.3f, %.3f  %.2f, %.2f"), v.x, v.y, arc.startAng(), arc.endAng());
    int c = ( arc.normal().z > 0 ? 1 : -1 );
    if( c > 0 )
    {
        v.rotateBy( c * arc.startAng() + PI / 2, AcGeVector3d::kZAxis );
    }
    else
    {
        v.rotateBy( c * arc.startAng() - PI / 2, AcGeVector3d::kZAxis );
    }
    return v.normalize(); // 标准化
}
AcGeVector3d ArcTunnel::getEndPointInExtendAngle() const
{
    AcGeCircArc3d arc( m_startPt, m_thirdPt, m_endPt );
    AcGeVector3d v = arc.refVec();
    int c = ( arc.normal().z > 0 ? 1 : -1 );
    if( c > 0 )
    {
        v.rotateBy( c * arc.endAng() - PI / 2, AcGeVector3d::kZAxis );
    }
    else
    {
        v.rotateBy( c * arc.endAng() + PI / 2, AcGeVector3d::kZAxis );
    }
    return v.normalize(); // 标准化
}
void ArcTunnel::dealWithStartPointBoundary( const AcGeRay3d& boundaryLine )
{
    AcGeCircArc3d arc0( m_startPt, m_thirdPt, m_endPt );
    AcGePoint3d cnt = arc0.center();
    double radius = arc0.radius();
    AcGeVector3d v = m_startPt - cnt;
    AcGeVector3d v2 = m_endPt - m_startPt;
    v.normalize();
    AcGeVector3d v3 = v.crossProduct( v2 );
    int c = ( v3.z > 0 ? 1 : -1 );
    v.rotateBy( c * PI / 2, AcGeVector3d::kZAxis );
    AcGePoint3d pt;
    AcGeLine3d line( m_rightStartPt, v );
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) )
    {
        //acutPrintf(_T("\n调整左侧轮廓线的始点坐标%.3f, %.3f"), pt.x, pt.y);
        m_rightStartPt = pt;
    }
    line.set( m_leftStartPt, v );
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) )
    {
        //acutPrintf(_T("\n调整右侧轮廓线的始点坐标%.3f, %.3f"), pt.x, pt.y);
        m_leftStartPt = pt;
    }
}
void ArcTunnel::dealWithEndPointBoundary( const AcGeRay3d& boundaryLine )
{
    AcGeCircArc3d arc0( m_startPt, m_thirdPt, m_endPt );
    AcGePoint3d cnt = arc0.center();
    double radius = arc0.radius();
    AcGeVector3d v = m_endPt - cnt;
    AcGeVector3d v2 = m_startPt - m_endPt;
    v.normalize();
    AcGeVector3d v3 = v.crossProduct( v2 );
    int c = ( v3.z > 0 ? 1 : -1 );
    v.rotateBy( c * PI / 2, AcGeVector3d::kZAxis );
    AcGePoint3d pt;
    AcGeLine3d line( m_rightEndPt, v );
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) )
    {
        //acutPrintf(_T("\n调整左侧轮廓线的末点坐标%.3f, %.3f"), pt.x, pt.y);
        m_rightEndPt = pt;
    }
    line.set( m_leftEndPt, v );
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) )
    {
        //acutPrintf(_T("\n调整右侧轮廓线的末点坐标%.3f, %.3f"), pt.x, pt.y);
        m_leftEndPt = pt;
    }
}
void ArcTunnel::extendByLength( double length )
{
    // 存在错误!!!
    AcGeVector3d v = m_endPt - m_startPt;
    v.normalize();
    m_endPt = m_endPt + v * length; // 更新末点坐标
    update(); // 更新其它参数
}
Acad::ErrorStatus ArcTunnel::dwgOutFields ( AcDbDwgFiler* pFiler ) const
{
    assertReadEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgOutFields"));
    Acad::ErrorStatus es = EdgeGE::dwgOutFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    if ( ( es = pFiler->writeUInt32 ( ArcTunnel::kCurrentVersionNumber ) ) != Acad::eOk )
        return ( es ) ;
    pFiler->writeItem( m_leftStartPt );
    pFiler->writeItem( m_leftEndPt );
    pFiler->writeItem( m_rightStartPt );
    pFiler->writeItem( m_rightEndPt );
    pFiler->writeItem( m_width );
    pFiler->writeItem( m_thirdPt );
    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus ArcTunnel::dwgInFields ( AcDbDwgFiler* pFiler )
{
    assertWriteEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgInFields"));
    Acad::ErrorStatus es = EdgeGE::dwgInFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    Adesk::UInt32 version = 0 ;
    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
        return ( es ) ;
    if ( version > ArcTunnel::kCurrentVersionNumber )
        return ( Acad::eMakeMeProxy ) ;
    pFiler->readItem( &m_leftStartPt );
    pFiler->readItem( &m_leftEndPt );
    pFiler->readItem( &m_rightStartPt );
    pFiler->readItem( &m_rightEndPt );
    pFiler->readItem( &m_width );
    pFiler->readItem( &m_thirdPt );
    return ( pFiler->filerStatus () ) ;
}
void ArcTunnel::update()
{
    caclInPoint( m_rightStartPt, m_leftStartPt );
    caclOutPoint( m_rightEndPt, m_leftEndPt );
}
void ArcTunnel::caclInPoint( AcGePoint3d& startPt1, AcGePoint3d& startPt2 )
{
    AcGeCircArc3d arc( m_startPt, m_thirdPt, m_endPt );
    AcGePoint3d cenPt = arc.center(); // 圆心
    double radius = arc.radius();
    AcGeVector3d v = m_startPt - cenPt;
    v.normalize();
    startPt1 = cenPt + v * ( radius - m_width / 2 );
    startPt2 = cenPt + v * ( radius + m_width / 2 );
}
void ArcTunnel::caclOutPoint( AcGePoint3d& endPt1, AcGePoint3d& endPt2 )
{
    AcGeCircArc3d arc( m_startPt, m_thirdPt, m_endPt );
    AcGePoint3d cenPt = arc.center(); // 圆心
    double radius = arc.radius();
    AcGeVector3d v = m_endPt - cenPt;
    v.normalize();
    endPt1 = cenPt + v * ( radius - m_width / 2 );
    endPt2 = cenPt + v * ( radius + m_width / 2 );
}
static void DrawArc( AcGiWorldDraw* mode, const AcGePoint3d& startPt, const AcGePoint3d& endPt, const AcGePoint3d& thirdPt )
{
    mode->geometry().circularArc( startPt, thirdPt, endPt );
}
Adesk::Boolean ArcTunnel::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled();
    AcGeCircArc3d arc( m_startPt, m_thirdPt, m_endPt );
    AcGePoint3d cnt = arc.center();
    double radius = arc.radius();
    AcGeVector3d v = m_thirdPt - cnt;
    v.normalize();
    DrawArc( mode, m_rightStartPt, m_rightEndPt, cnt + v * ( radius - m_width * 0.5 ) ); // 绘制外弧
    DrawArc( mode, m_leftStartPt, m_leftEndPt, cnt + v * ( radius + m_width * 0.5 ) ); // 绘制内弧
    return Adesk::kTrue;
}
Acad::ErrorStatus ArcTunnel::customTransformBy( const AcGeMatrix3d& xform )
{
    m_startPt.transformBy( xform );
    m_endPt.transformBy( xform );
    m_thirdPt.transformBy( xform );
    update();
    return Acad::eOk;
}
Acad::ErrorStatus ArcTunnel::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉1种类型的点：端点
    if( osnapMode != AcDb::kOsModeEnd )
        return Acad::eOk;
    Acad::ErrorStatus es = Acad::eOk;
    if ( osnapMode == AcDb::kOsModeEnd )
    {
        snapPoints.append( m_startPt );
        snapPoints.append( m_endPt );
        snapPoints.append( m_thirdPt ); // 弧中点
    }
    return es;
}
//- Grip points protocol
Acad::ErrorStatus ArcTunnel::customGetGripPoints (
    AcGePoint3dArray& gripPoints,
    AcDbIntArray& osnapModes,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_startPt );
    gripPoints.append( m_endPt );
    gripPoints.append( m_thirdPt );
    return Acad::eOk;
}
Acad::ErrorStatus ArcTunnel::customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        if ( idx == 0 )
        {
            m_startPt += offset;
        }
        if ( idx == 1 )
        {
            m_endPt += offset;
        }
        if( idx == 2 )
        {
            AcGeCircArc3d arc( m_startPt, m_thirdPt, m_endPt );
            int c = ( arc.normal().z > 0 ? -1 : 1 );
            m_thirdPt += offset;
            if( m_thirdPt == m_startPt || m_thirdPt == m_endPt )
            {
                AcGeVector3d v = m_endPt - m_startPt;
                AcGePoint3d cnt = m_startPt + v * 0.5;
                v.rotateBy( c * PI / 2, AcGeVector3d::kZAxis ); // 按照原来弧线的反方向旋转
                m_thirdPt = cnt + v * 0.5;
            }
        }
        update();
    }
    return Acad::eOk;
}
void ArcTunnel::onAfterSetData( const CString& field, const CString& value )
{
    ArcTunnel* pEnt = ArcTunnel::cast( this );
    if( pEnt == 0 ) return;
    // 调用基类的虚函数
    EdgeGE::onAfterSetData( field, value );
    if( field == _T( "起点坐标" ) )
    {
        pEnt->update();
    }
    else if( field == _T( "末点坐标" ) )
    {
        pEnt->update();
    }
    else if( field == _T( "宽度" ) )
    {
        pEnt->update();
    }
    else if( field == _T( "第三点坐标" ) )
    {
        pEnt->update();
    }
}