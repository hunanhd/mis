#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// 风门：永久风门，临时风门，双向风门
// 风门抽象类
class VENTGE_EXPORT_API Gate : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( Gate ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
protected:
    Gate();
    Gate( const AcGePoint3d& insertPt, double angle );
    virtual void caclBackGroundMinPolygon( AcGePoint3dArray& pts );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode ) ;
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset ) ;
protected:
    void drawArc( AcGiWorldDraw* mode, const AcGePoint3d& insertPt, double angle, double gap, double radius, bool moreAngle = true );
    void drawLine( AcGiWorldDraw* mode, const AcGePoint3d& insertPt, double angle,  double gap, double offset, double length );
protected:
    double m_radius;        // 风门的半径
    double m_length;        // 风门直线的一半长度（从中轴向外的距离）
    // 派生类根据需要添加到附加参数中
    double m_offset;
    double m_gap;
    double m_dTWidth;  // 所在双线巷道的宽度
};
// 永久风门
class VENTGE_EXPORT_API PermanentGate : public Gate
{
public:
    ACRX_DECLARE_MEMBERS( PermanentGate ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    PermanentGate() ;
    PermanentGate( const AcGePoint3d& insertPt, double angle );
};
// 临时风门
class VENTGE_EXPORT_API TemporaryGate : public Gate
{
public:
    ACRX_DECLARE_MEMBERS( TemporaryGate ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode ) ;
public:
    TemporaryGate() ;
    TemporaryGate( const AcGePoint3d& insertPt, double angle );
} ;
// 双向风门(貌似就是阻断风门???)
class VENTGE_EXPORT_API DoubleGate : public Gate
{
public:
    ACRX_DECLARE_MEMBERS( DoubleGate ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
    virtual void caclBackGroundMinPolygon( AcGePoint3dArray& pts );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode ) ;
public:
    DoubleGate();
    DoubleGate( const AcGePoint3d& insertPt, double angle );
};
// 平衡风门(貌似也是阻断风门???)
class VENTGE_EXPORT_API BalanceGate : public Gate
{
public:
    ACRX_DECLARE_MEMBERS( BalanceGate ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
    virtual void caclBackGroundMinPolygon( AcGePoint3dArray& pts );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode ) ;
private:
    // 绘制抛物线(两个半圆)
    void drawParabola( AcGiWorldDraw* mode, const AcGePoint3d& insertPt, double angle, double gap, double radius, bool clockWise = true );
public:
    BalanceGate();
    BalanceGate( const AcGePoint3d& insertPt, double angle );
};
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( Gate )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( PermanentGate )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( TemporaryGate )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( DoubleGate )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( BalanceGate )
#endif
