#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// 风流方向
class VENTGE_EXPORT_API WindDirection : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( WindDirection ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    WindDirection() ;
    WindDirection( const AcGePoint3d& insertPt, double angle );
    virtual ~WindDirection();
    // 设置风流方向长度
    void setLength( double length );
    // 重载MineGE虚函数(数据操作)
protected:
    virtual void regPropertyDataNames( AcStringArray& names ) const;
    virtual void readPropertyDataFromValues( const AcStringArray& values );
    // 重载MineGE虚函数(图形操作)
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
    // 私有函数
private:
    void update();
    // 成员变量
private:
    double m_length;          // 箭头主干一半长度(默认为60)
    double m_arrow_length;    // 箭头分支长度(默认为30)
    double m_arrow_width;     // 箭头宽度(默认10)
    double m_radius;          // 绘制回风标记的正弦曲线(2个半圆)的半径
    int m_vt;                // 风流标记(新风或乏风)
} ;
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( WindDirection )
#endif