#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// ͻˮ��
class VENTGE_EXPORT_API WaterInrushPt : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( WaterInrushPt ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    WaterInrushPt() ;
    WaterInrushPt( const AcGePoint3d& insertPt, double angle );
    virtual ~WaterInrushPt();
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
protected:
    double m_radius; //�뾶
};
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( WaterInrushPt )
#endif