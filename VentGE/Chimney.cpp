#include "StdAfx.h"
#include "Chimney.h"
#include "MineGE/Drawtool.h"
Adesk::UInt32 Chimney::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    Chimney, TagGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    风筒, VENTGEAPP
)
Chimney::Chimney () : TagGE ()
{
    m_length = 40;
    m_width = 5;
    m_space = 10;
    m_lineWidth = 0;
}
Chimney::~Chimney ()
{
}
Acad::ErrorStatus Chimney::dwgOutFields ( AcDbDwgFiler* pFiler ) const
{
    assertReadEnabled () ;
    //----- Save parent class information first.
    Acad::ErrorStatus es = TagGE::dwgOutFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    //----- Object version number needs to be saved first
    if ( ( es = pFiler->writeUInt32 ( Chimney::kCurrentVersionNumber ) ) != Acad::eOk )
        return ( es ) ;
    //----- Output params
    //.....
    int len = m_pts.length();
    pFiler->writeInt32( ( long )len );
    for( int i = 0; i < len; i++ )
    {
        pFiler->writePoint3d( m_pts[i] );
    }
    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus Chimney::dwgInFields ( AcDbDwgFiler* pFiler )
{
    assertWriteEnabled () ;
    //----- Read parent class information first.
    Acad::ErrorStatus es = TagGE::dwgInFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    //----- Object version number needs to be read first
    Adesk::UInt32 version = 0 ;
    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
        return ( es ) ;
    if ( version > Chimney::kCurrentVersionNumber )
        return ( Acad::eMakeMeProxy ) ;
    //- Uncomment the 2 following lines if your current object implementation cannot
    //- support previous version of that object.
    //if ( version < Chimney::kCurrentVersionNumber )
    //	return (Acad::eMakeMeProxy) ;
    //----- Read params
    //.....
    m_pts.removeAll();
    long len = 0;
    pFiler->readInt32( &len );
    for( int i = 0; i < ( int )len; i++ )
    {
        AcGePoint3d pt;
        pFiler->readPoint3d( &pt );
        m_pts.append( pt );
    }
    return ( pFiler->filerStatus () ) ;
}
void Chimney::addControlPoint( const AcGePoint3d& pt )
{
    if( !m_pts.contains( pt ) ) m_pts.append( pt );
}
AcGePoint3dArray Chimney::getControlPoint()
{
    return m_pts;
}
void Chimney::setControlPoint( AcGePoint3dArray pts )
{
    m_pts = pts;
}
static void DrawSegment( AcGiWorldDraw* mode, const AcGePoint3d& spt, const AcGeVector3d& v, double length, double width, double lineWidth )
{
    AcGePoint3d ept = spt + v * length;
    DrawPolyLine( mode, spt, ept, lineWidth );
    AcGeVector3d vv = v;
    vv.rotateBy( PI / 2, AcGeVector3d::kZAxis );
    AcGePoint3d spt1, spt2;
    spt1 = spt + vv * width * 0.5;
    vv.rotateBy( PI, AcGeVector3d::kZAxis );
    spt2 = spt + vv * width * 0.5;
    DrawPolyLine( mode, spt1, spt2, lineWidth );
    AcGePoint3d ept1, ept2;
    ept1 = ept + vv * width * 0.5;
    vv.rotateBy( PI, AcGeVector3d::kZAxis );
    ept2 = ept + vv * width * 0.5;
    DrawPolyLine( mode, ept1, ept2, lineWidth );
}
void Chimney::drawSegment( AcGiWorldDraw* mode, const AcGePoint3d& spt, const AcGePoint3d& ept )
{
    AcGeVector3d v = ept - spt;
    int n = ( int )( ( v.length() + m_space ) / ( m_length + m_space ) );
    //acutPrintf(_T("\n可绘制的个数:%d"), n);
    v.normalize();
    AcGePoint3d pt = spt;
    for( int i = 0; i < n; i++ )
    {
        DrawSegment( mode, pt, v, m_length, m_width, m_lineWidth );
        pt = pt + v * ( m_length + m_space );
    }
    double ll = ( ept - pt ).length();
    if( ll > m_length * 0.5 ) // 如果有长度的50%，则绘制一小段
    {
        DrawSegment( mode, pt, v, ll, m_width, m_lineWidth );
    }
}
Adesk::Boolean Chimney::customWorldDraw ( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    int len = m_pts.length();
    if( len < 2 ) return Adesk::kTrue;
    for( int i = 0; i < len - 1; i++ )
    {
        drawSegment( mode, m_pts[i], m_pts[i + 1] );
    }
    return Adesk::kTrue;
}
Acad::ErrorStatus Chimney::customTransformBy( const AcGeMatrix3d& xform )
{
    int len = m_pts.length();
    for( int i = 0; i < len; i++ )
    {
        m_pts[i].transformBy( xform );
    }
    return Acad::eOk;
}
Acad::ErrorStatus Chimney::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉1种类型的点：端点
    if( osnapMode != AcDb::kOsMaskEnd ) return Acad::eOk;
    if( osnapMode == AcDb::kOsMaskEnd )
    {
        int len = m_pts.length();
        for( int i = 0; i < len; i++ )
        {
            snapPoints.append( m_pts[i] );
        }
    }
    return Acad::eOk;
}
//- Grip points protocol
Acad::ErrorStatus Chimney::customGetGripPoints (
    AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    int len = m_pts.length();
    for( int i = 0; i < len; i++ )
    {
        gripPoints.append( m_pts[i] );
    }
    return Acad::eOk;
}
Acad::ErrorStatus Chimney::customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        m_pts[idx] += offset;
    }
    return Acad::eOk;
}
