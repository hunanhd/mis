#include "StdAfx.h"
#include "WindDirection.h"
#include "MineGE/Drawtool.h"
#include "MineGE/DrawSpecial.h"
Adesk::UInt32 WindDirection::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS ( WindDirection, DirGE,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          风流方向, VENTGEAPP )
WindDirection::WindDirection ()
{
    m_length = 40;
    update();
}
WindDirection::WindDirection( const AcGePoint3d& insertPt, double angle )
    : DirGE( insertPt, angle )
{
    m_length = 40;
    update();
}
WindDirection::~WindDirection ()
{
}
void WindDirection::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "三区" ) );
}
void WindDirection::readPropertyDataFromValues( const AcStringArray& values )
{
    m_vt = _ttoi( values[0].kACharPtr() ) % 2; // 保证值为{0,1}
}
Adesk::Boolean WindDirection::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    // 绘制箭头主干
    DrawLine( mode, m_insertPt, m_angle, m_length );
    DrawLine( mode, m_insertPt, m_angle + PI, m_length );
    // 绘制箭头
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    DrawArrow( mode, m_insertPt + v * m_length, m_angle, m_arrow_width, m_arrow_length );
    // 绘制污风标记
    if( m_vt == 1 )
    {
        v.rotateBy( PI, AcGeVector3d::kZAxis );
        DrawSin( mode, m_insertPt + v * m_length, m_angle, m_radius );;
    }
    return Adesk::kTrue;
}
Acad::ErrorStatus WindDirection::customTransformBy( const AcGeMatrix3d& xform )
{
    // 插入点变换
    m_insertPt.transformBy( xform );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.transformBy( xform );
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis ); // 变换后的旋转角度
    return Acad::eOk;
}
Acad::ErrorStatus WindDirection::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 捕捉2种类型的点：端点和中心点
    if( osnapMode != AcDb::kOsModeCen )
        return Acad::eOk;
    Acad::ErrorStatus es = Acad::eOk;
    if( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return es;
}
Acad::ErrorStatus WindDirection::customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    return Acad::eOk;
}
Acad::ErrorStatus WindDirection::customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        if ( idx == 0 )
        {
            m_insertPt += offset; 			// 插入点偏移
        }
    }
    return Acad::eOk;
}
void WindDirection::setLength( double length )
{
    m_length = length;
    update();
}
void WindDirection::update()
{
    m_arrow_length = m_length * 0.5;
    m_arrow_width = m_arrow_length * 0.5;
    m_radius = m_length * 0.2;
}