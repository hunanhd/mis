#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// 漏风点：漏风源、漏风汇
// 漏风点抽象类
class VENTGE_EXPORT_API AirLeakage : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( AirLeakage ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
protected:
    AirLeakage();
    AirLeakage( const AcGePoint3d& insertPt, double angle );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode ) ;
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset ) ;
public:
    double m_radius;        // 漏风点的半径
    bool m_isSource;		//是否为漏风源
};
// 漏风源
class VENTGE_EXPORT_API AirSource : public AirLeakage
{
public:
    ACRX_DECLARE_MEMBERS( AirSource ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    AirSource() ;
    AirSource( const AcGePoint3d& insertPt, double angle );
};
// 漏风汇
class VENTGE_EXPORT_API AirIn : public AirLeakage
{
public:
    ACRX_DECLARE_MEMBERS( AirIn ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    AirIn() ;
    AirIn( const AcGePoint3d& insertPt, double angle );
};

#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( AirLeakage )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( AirSource )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( AirIn )
#endif
