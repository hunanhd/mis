#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// 风机类
class VENTGE_EXPORT_API Fan : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( Fan ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    Fan() ;
    Fan( const AcGePoint3d& insertPt, double angle );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints(
        AcGePoint3dArray& gripPoints,
        AcDbIntArray& osnapModes,
        AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
public:
    // extra param
    double m_radius; // 半径
    double m_distance; // 距离
};
// 风机类
class VENTGE_EXPORT_API MainFan : public Fan
{
public:
    ACRX_DECLARE_MEMBERS( MainFan ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    MainFan() ;
    MainFan( const AcGePoint3d& insertPt, double angle );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    //virtual Acad::ErrorStatus customTransformBy(const AcGeMatrix3d & xform);
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    //virtual Acad::ErrorStatus gripPoints(
    //	AcGePoint3dArray &gripPoints,
    //	AcDbIntArray &osnapModes,
    //	AcDbIntArray &geomIds ) const;
    //virtual Acad::ErrorStatus moveGripPoints( const AcDbIntArray &indices, const AcGeVector3d &offset );
//public:
//	// extra param
//	double m_radius; // 半径
//	double m_distance; // 距离
};
// 局部扇风机
class VENTGE_EXPORT_API LocalFan : public Fan
{
public:
    ACRX_DECLARE_MEMBERS( LocalFan ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
public:
    LocalFan() ;
    LocalFan( const AcGePoint3d& insertPt, double angle );
private:
    AcGePoint3d vp_add( AcGeVector3d v, AcGePoint3d p );
    void drawFan( AcGiWorldDraw* mode );
};
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( Fan )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( MainFan )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( LocalFan )
#endif
