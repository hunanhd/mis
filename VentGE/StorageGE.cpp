#include "StdAfx.h"
#include "StorageGE.h"
#include "MineGE/Drawtool.h"
Adesk::UInt32 StorageGE::kCurrentVersionNumber = 1 ;
Adesk::UInt32 PowderStorage::kCurrentVersionNumber = 1 ;
Adesk::UInt32 MachineRoom::kCurrentVersionNumber = 1 ;
Adesk::UInt32 ChargeRoom::kCurrentVersionNumber = 1 ;
ACRX_NO_CONS_DEFINE_MEMBERS( StorageGE, EdgeGE )
ACRX_DXF_DEFINE_MEMBERS (
    PowderStorage, StorageGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    爆炸材料库, DEFGEAPP
)
ACRX_DXF_DEFINE_MEMBERS (
    MachineRoom, StorageGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    机电硐室, DEFGEAPP
)

ACRX_DXF_DEFINE_MEMBERS (
    ChargeRoom, StorageGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    充电室, DEFGEAPP
)
StorageGE::StorageGE () : EdgeGE ()
{
    m_boxHeight = 100;
    m_minLength = 150;
    m_width = 30;
}
StorageGE::StorageGE( const AcGePoint3d& startPt, const AcGePoint3d& endPt ) : EdgeGE( startPt, endPt )
{
    m_boxHeight = 100;
    m_minLength = 150;
    m_width = 30;
    update();
}
StorageGE::~StorageGE ()
{
}
void StorageGE::updateBoxWidth()
{
    AcGeVector3d v = m_endPt - m_startPt;
    double c = 0.618;
    m_boxWidth = v.length() * ( 1 - c );
}
void StorageGE::drawBox( AcGiWorldDraw* mode )
{
    AcGeVector3d v = m_endPt - m_startPt;
    double L = v.length();
    if( m_boxWidth < m_minLength ) return; // 如果小于m_minLength，则不显示文字框
    v.normalize();
    AcGeVector3d v2( v );
    v2.rotateBy( PI / 2, AcGeVector3d::kZAxis );
    AcGePoint3d boxPt1 = m_startPt + v * ( L - m_boxWidth ) / 2 + v2 * m_width / 2;
    AcGePoint3d boxPt2 = boxPt1 + v2 * m_boxHeight;
    AcGePoint3d boxPt4 = m_startPt + v * ( L + m_boxWidth ) / 2 + v2 * m_width / 2;
    AcGePoint3d boxPt3 = boxPt4 + v2 * m_boxHeight;
    // 绘制box
    AcGePoint3dArray pts;
    pts.append( boxPt1 );
    pts.append( boxPt2 );
    pts.append( boxPt3 );
    pts.append( boxPt4 );
    //mode->geometry().polygon(pts.length(), pts.asArrayPtr());
    mode->geometry().polyline( pts.length(), pts.asArrayPtr() );
}
void StorageGE::drawText( AcGiWorldDraw* mode )
{
    // 绘制文字
    AcGeVector3d v = m_endPt - m_startPt;
    AcGePoint3d pt = m_startPt + v * 0.5; // 中心点
    if( v.x < 0 ) v.negate();
    double angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    v.normalize();
    v.rotateBy( PI / 2, AcGeVector3d::kZAxis ); // 始终与文字反向
    pt += v * m_width * 0.5;
    DrawMText( mode, pt, angle, m_text, m_width * 0.618, AcDbMText::kBottomCenter );
}
Adesk::Boolean StorageGE::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled();
    DrawLine( mode, m_leftStartPt, m_leftEndPt );   // 绘制左线
    DrawLine( mode, m_rightStartPt, m_rightEndPt );	// 绘制右线
    //drawBox(mode);     // 绘制文字框
    //drawText( mode );
    return Adesk::kTrue;
}
void StorageGE::update()
{
    caclStartPoint( m_leftStartPt, m_rightStartPt );
    caclEndPoint( m_leftEndPt, m_rightEndPt );
}
void StorageGE::caclStartPoint( AcGePoint3d& startPt1, AcGePoint3d& startPt2 )
{
    AcGeVector3d v = m_endPt - m_startPt;
    v.normalize();
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    startPt1 = m_startPt + v * m_width * 0.5;
    v.rotateBy( PI, AcGeVector3d::kZAxis );
    startPt2 = m_startPt + v * m_width * 0.5;
}
void StorageGE::caclEndPoint( AcGePoint3d& endPt1, AcGePoint3d& endPt2 )
{
    AcGeVector3d v = m_endPt - m_startPt;
    v.normalize();
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    endPt1 = m_endPt + v * m_width * 0.5;
    v.rotateBy( PI, AcGeVector3d::kZAxis );
    endPt2 = m_endPt + v * m_width * 0.5;
}
void StorageGE::dealWithStartPointBoundary( const AcGeRay3d& boundaryLine )
{
    AcGeLine3d line( m_leftStartPt, m_leftEndPt );
    AcGePoint3d pt;
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) ) // 计算左侧轮廓线与边界线的交叉点
    {
        //acutPrintf(_T("\n调整左侧轮廓线的始点坐标%.3f, %.3f"), pt.x, pt.y);
        m_leftStartPt = pt;                        // 调整左侧轮廓线的始点坐标
    }
    line.set( m_rightStartPt, m_rightEndPt );
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) ) // 计算右侧轮廓线与边界线的交叉点
    {
        //acutPrintf(_T("\n调整右侧轮廓线的始点坐标%.3f, %.3f"), pt.x, pt.y);
        m_rightStartPt = pt;                       // 调整右侧轮廓线的始点坐标
    }
}
void StorageGE::dealWithEndPointBoundary( const AcGeRay3d& boundaryLine )
{
    AcGeLine3d line( m_leftStartPt, m_leftEndPt );
    AcGePoint3d pt;
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) ) // 计算左侧轮廓线与边界线的交叉点
    {
        //acutPrintf(_T("\n调整左侧轮廓线的末点坐标%.3f, %.3f"), pt.x, pt.y);
        m_leftEndPt = pt;                                         // 调整左侧轮廓线的末点坐标
    }
    line.set( m_rightStartPt, m_rightEndPt );
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) ) // 计算右侧轮廓线与边界线的交叉点
    {
        //acutPrintf(_T("\n调整右侧轮廓线的末点坐标%.3f, %.3f"), pt.x, pt.y);
        m_rightEndPt = pt;                          // 调整右侧轮廓线的末点坐标
    }
}
void StorageGE::reverse()
{
    EdgeGE::reverse();
    update();
}
void StorageGE::extendByLength( double length )
{
    AcGeVector3d v = m_endPt - m_startPt;
    v.normalize();
    m_endPt = m_endPt + v * length; // 更新末点坐标
    update(); // 更新其它参数
}
void StorageGE::minPolygon( AcGePoint3dArray& pts )
{
    pts.append( m_startPt );
    pts.append( m_leftStartPt );
    pts.append( m_leftEndPt );
    pts.append( m_endPt );
    pts.append( m_rightEndPt );
    pts.append( m_rightStartPt );
}
Acad::ErrorStatus StorageGE::customTransformBy( const AcGeMatrix3d& xform )
{
    m_startPt.transformBy( xform );
    m_endPt.transformBy( xform );
    update();
    return Acad::eOk;
}
Acad::ErrorStatus StorageGE::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉端点
    if ( osnapMode == AcDb::kOsModeEnd )
    {
        snapPoints.append( m_startPt );
        snapPoints.append( m_endPt );
    }
    return Acad::eOk;
}
Acad::ErrorStatus StorageGE::customGetGripPoints ( AcGePoint3dArray& gripPoints,
        AcDbIntArray& osnapModes,
        AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_startPt );
    if( m_startPt == m_endPt )
    {
        AcGePoint3d pt( m_startPt );
        pt.x = pt.x + m_width * 0.3;
        gripPoints.append( pt );
    }
    else
    {
        gripPoints.append( m_endPt );
    }
    return Acad::eOk;
}
Acad::ErrorStatus StorageGE::customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        if ( idx == 0 )
        {
            m_startPt += offset;
        }
        if ( idx == 1 )
        {
            m_endPt += offset;
        }
        update();
    }
    return Acad::eOk;
}
PowderStorage::PowderStorage () : StorageGE ()
{
}
PowderStorage::PowderStorage( const AcGePoint3d& startPt, const AcGePoint3d& endPt ) : StorageGE( startPt, endPt )
{
}
PowderStorage::~PowderStorage ()
{
}
MachineRoom::MachineRoom () : StorageGE ()
{
}
MachineRoom::MachineRoom( const AcGePoint3d& startPt, const AcGePoint3d& endPt ) : StorageGE( startPt, endPt )
{
}
MachineRoom::~MachineRoom ()
{
}
ChargeRoom::ChargeRoom () : StorageGE ()
{
}
ChargeRoom::ChargeRoom( const AcGePoint3d& startPt, const AcGePoint3d& endPt ) : StorageGE( startPt, endPt )
{
}
ChargeRoom::~ChargeRoom ()
{
}