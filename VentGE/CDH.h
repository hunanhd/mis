#pragma once
#include "MineGE/BlockGE.h"
#include "dlimexp.h"
// ��ú���(CoalDrillHole)
class VENTGE_EXPORT_API CDH : public BlockGE
{
public:
    ACRX_DECLARE_MEMBERS( CDH ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    CDH();
    CDH( const AcGePoint3d& insertPt );
};
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( CDH )
#endif
