#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// 风筒
class VENTGE_EXPORT_API Chimney : public TagGE
{
public:
    ACRX_DECLARE_MEMBERS( Chimney ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    Chimney () ;
    virtual ~Chimney () ;
    void addControlPoint( const AcGePoint3d& pt );
    void setControlPoint( AcGePoint3dArray pts );
    AcGePoint3dArray getControlPoint();
    virtual Acad::ErrorStatus dwgOutFields ( AcDbDwgFiler* pFiler ) const ;
    virtual Acad::ErrorStatus dwgInFields ( AcDbDwgFiler* pFiler ) ;
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode ) ;
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset ) ;
private:
    void drawSegment( AcGiWorldDraw* mode, const AcGePoint3d& spt, const AcGePoint3d& ept );
private:
    double m_length;               // 风筒长度
    double m_width;                // 风筒宽度
    double m_space;                // 两节风筒之间的间距
    double m_lineWidth;
    AcGePoint3dArray m_pts;         // 风筒控制点
} ;
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( Chimney )
#endif
