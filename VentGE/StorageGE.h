#pragma once
#include "MineGE/EdgeGE.h"
#include "dlimexp.h"
// 3类硐室图元的数据都不是相同的，不应看做是定制的可视化效果
// 不应同于巷道下的弧线巷道
// 弧线巷道只是可视化效果的更进一步定制, 但数据和Tunnel类是一样的
// 因此都直接从StorageGE抽象类派生, 作为"顶层标志图元"
// 硐室图元基类(抽象类)
class VENTGE_EXPORT_API StorageGE : public EdgeGE
{
public:
    ACRX_DECLARE_MEMBERS( StorageGE ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    virtual ~StorageGE();
protected:
    StorageGE();
    StorageGE( const AcGePoint3d& startPt, const AcGePoint3d& endPt );
    // 重载EdgeGE类虚函数(用于实现节点闭合)
public:
    virtual void reverse();
    virtual void dealWithStartPointBoundary( const AcGeRay3d& boundaryLine );
    virtual void dealWithEndPointBoundary( const AcGeRay3d& boundaryLine );
    virtual void extendByLength( double length );
public:
    void drawBox( AcGiWorldDraw* mode );
    void updateBoxWidth();
    void drawText( AcGiWorldDraw* mode );
protected:
    // 具体的绘制实现
    virtual Adesk::Boolean customWorldDraw( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset ) ;
    // 计算图形的最小多边形
    virtual void minPolygon( AcGePoint3dArray& pts );
private:
    void update();
    void caclStartPoint( AcGePoint3d& startPt, AcGePoint3d& endPt );
    void caclEndPoint( AcGePoint3d& startPt, AcGePoint3d& endPt );
    // extra param
    double m_boxHeight; // 文字框高度,默认为60
    double m_boxWidth;  // 文字框宽度,默认始末点长度的1/3
    double m_minLength; // 最小长度(用于显示文字框)，默认150
    double m_width; // 默认为30
    AcGePoint3d m_leftStartPt, m_leftEndPt;
    AcGePoint3d m_rightStartPt, m_rightEndPt;
    CString m_text; // 要显示的文字
} ;
// 火药库图元
class VENTGE_EXPORT_API PowderStorage : public StorageGE
{
public:
    ACRX_DECLARE_MEMBERS( PowderStorage ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    PowderStorage();
    PowderStorage( const AcGePoint3d& startPt, const AcGePoint3d& endPt );
    virtual ~PowderStorage();
} ;
// 机电硐室图元
class VENTGE_EXPORT_API MachineRoom : public StorageGE
{
public:
    ACRX_DECLARE_MEMBERS( MachineRoom ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    MachineRoom();
    MachineRoom( const AcGePoint3d& startPt, const AcGePoint3d& endPt );
    virtual ~MachineRoom();
} ;
// 充电室图元
class VENTGE_EXPORT_API ChargeRoom : public StorageGE
{
public:
    ACRX_DECLARE_MEMBERS( ChargeRoom ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    ChargeRoom();
    ChargeRoom( const AcGePoint3d& startPt, const AcGePoint3d& endPt );
    virtual ~ChargeRoom();
} ;
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( StorageGE )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( PowderStorage )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( MachineRoom )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( ChargeRoom )
#endif
