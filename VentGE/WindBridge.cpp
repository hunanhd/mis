#include "StdAfx.h"
#include "WindBridge.h"
#include "MineGE/DrawTool.h"
Adesk::UInt32 WindBridge::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    WindBridge, DirGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    风桥, VENTGEAPP
)
WindBridge::WindBridge () : DirGE ()
{
    m_width = 35;
    m_sideLength = 40;
    m_sideLength2 = 30;
    m_sideAngle = PI / 4;
    m_linewidth = 1.5;
    m_offset = 30;
}
WindBridge::WindBridge( const AcGePoint3d& insertPt, double angle ) : DirGE( insertPt, angle )
{
    m_width = 35;
    m_sideLength = 40;
    m_sideLength2 = 20;
    m_sideAngle = PI / 4;
    m_linewidth = 1.5;
    m_offset = 30;
}
WindBridge::~WindBridge ()
{
}
Adesk::Boolean WindBridge::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled();
    int colorIndex = mode->subEntityTraits().color();
    mode->subEntityTraits().setColor( 12 ); //棕色
    // 绘制风桥
    drawSide( mode );
    mode->subEntityTraits().setColor( colorIndex );
    return Adesk::kTrue;
}
Acad::ErrorStatus WindBridge::customTransformBy( const AcGeMatrix3d& xform )
{
    m_insertPt.transformBy( xform );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.transformBy( xform );
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    return Acad::eOk;
}
Acad::ErrorStatus WindBridge::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    Acad::ErrorStatus es = Acad::eOk;
    if( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return es;
}
Acad::ErrorStatus WindBridge::customGetGripPoints ( AcGePoint3dArray& gripPoints,
        AcDbIntArray& osnapModes,
        AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    return Acad::eOk;
}
Acad::ErrorStatus WindBridge::customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        if ( idx == 0 )
        {
            AcGeVector3d v( AcGeVector3d::kXAxis );
            v.rotateBy( m_angle, AcGeVector3d::kZAxis );
            m_insertPt += v.dotProduct( offset ) * v; // 计算偏移offset在angle方向上的投影
        }
    }
    return Acad::eOk;
}
void WindBridge::drawSide( AcGiWorldDraw* mode )
{
    AcGeVector3d v1( AcGeVector3d::kXAxis );
    v1.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v1.normalize();
    AcGePoint3d pt4 = m_insertPt + v1 * m_width * 0.5;
    v1.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    pt4 = pt4 + v1 * m_offset * 0.5;
    AcGePoint3d pt2 = pt4 + v1 * m_sideLength;
    AcGePoint3d pt5 = pt4 - v1 * m_offset;
    AcGePoint3d pt7 = pt5 - v1 * m_sideLength;
    v1.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    AcGePoint3d pt1 = pt2 + v1 * m_width;
    AcGePoint3d pt11 = pt4 + v1 * m_width;
    AcGePoint3d pt9 = pt5 + v1 * m_width;
    AcGePoint3d pt8 = pt7 + v1 * m_width;
    v1.rotateBy( -m_sideAngle, AcGeVector3d::kZAxis );
    AcGePoint3d pt12 = pt11 + v1 * m_sideLength2;
    AcGePoint3d pt6 = pt5 - v1 * m_sideLength2;
    v1.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    AcGePoint3d pt3 = pt4 - v1 * m_sideLength2;
    AcGePoint3d pt10 = pt9 + v1 * m_sideLength2;
    DrawPolyLine( mode, pt11, pt1, m_linewidth );
    DrawPolyLine( mode, pt4, pt2, m_linewidth );
    DrawPolyLine( mode, pt5, pt7, m_linewidth );
    DrawPolyLine( mode, pt9, pt8, m_linewidth );
    DrawLine( mode, pt4, pt3 );
    DrawLine( mode, pt11, pt12 );
    DrawLine( mode, pt5, pt6 );
    DrawLine( mode, pt9, pt10 );
}