#pragma once
#include "MineGE/MineGE.h"
#include "dlimexp.h"
// ����
class VENTGE_EXPORT_API Hoses : public MineGE
{
public:
    ACRX_DECLARE_MEMBERS( Hoses ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    Hoses () ;
    virtual ~Hoses () ;
    void addControlPoint( const AcGePoint3d& pt );
    void setControlPoint( AcGePoint3dArray pts );
    AcGePoint3dArray getControlPoint();
    virtual Acad::ErrorStatus dwgOutFields ( AcDbDwgFiler* pFiler ) const ;
    virtual Acad::ErrorStatus dwgInFields ( AcDbDwgFiler* pFiler ) ;
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode ) ;
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset ) ;
private:
    AcGePoint3dArray m_pts;         // ���ܿ��Ƶ�
} ;
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( Hoses )
#endif
