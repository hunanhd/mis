#include "StdAfx.h"
#include "AirCurtain.h"
#include "MineGE/Drawtool.h"
Adesk::UInt32 AirCurtain::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS ( AirCurtain, DirGE,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          空气幕, VENTGEAPP )
AirCurtain::AirCurtain ()
{
    m_width = 30;
    update();
}
AirCurtain::AirCurtain( const AcGePoint3d& insertPt, double angle )
    : DirGE( insertPt, angle )
{
    m_width = 30;
    update();
}
AirCurtain::~AirCurtain ()
{
}
static void DrawDirect( AcGiWorldDraw* mode, const AcGePoint3d& inserPt, double angle, double arrowLength )
{
    double arrowAngle = PI / 6;
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( angle + PI + arrowAngle, AcGeVector3d::kZAxis );
    AcGePoint3d pt1 = inserPt + v * arrowLength;
    v.rotateBy( -2 * arrowAngle, AcGeVector3d::kZAxis );
    AcGePoint3d pt2 = inserPt + v * arrowLength;
    DrawLine( mode, inserPt, pt1 );
    DrawLine( mode, inserPt, pt2 );
}
Adesk::Boolean AirCurtain::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    int colorIndex = mode->subEntityTraits().color();
    mode->subEntityTraits().setColor( 190 );
    // 绘制箭头
    AcGeVector3d v( AcGeVector3d::kXAxis );
    AcGePoint3d bottpmPt, topPt, directPt;
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    directPt = m_insertPt + v * m_length;
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    topPt = m_insertPt + v * m_width * 0.5;
    bottpmPt = m_insertPt - v * m_width * 0.5;
    DrawLine( mode, bottpmPt, directPt );
    DrawLine( mode, topPt, directPt );
    AcGeVector3d v1 = directPt - bottpmPt;
    AcGeVector3d v2 = directPt - topPt;
    //double length1 = v1.length();
    //double length2 = v2.length();
    AcGePoint3d bottpmPt1 = bottpmPt + v1 / 3;
    AcGePoint3d bottpmPt2 = bottpmPt + 2 * v1 / 3;
    AcGePoint3d topPt1 = topPt + v2 / 3;
    AcGePoint3d topPt2 = topPt + 2 * v2 / 3;
    double arrowLenth = m_length * 0.1;
    double angle1 = v1.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    double angle2 = v2.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    DrawDirect( mode, bottpmPt1, angle1, arrowLenth );
    DrawDirect( mode, bottpmPt2, angle1, arrowLenth );
    DrawDirect( mode, topPt1, angle2, arrowLenth );
    DrawDirect( mode, topPt2, angle2, arrowLenth );
    mode->subEntityTraits().setColor( colorIndex );
    return Adesk::kTrue;
}
Acad::ErrorStatus AirCurtain::customTransformBy( const AcGeMatrix3d& xform )
{
    // 插入点变换
    m_insertPt.transformBy( xform );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.transformBy( xform );
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis ); // 变换后的旋转角度
    return Acad::eOk;
}
Acad::ErrorStatus AirCurtain::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 捕捉2种类型的点：端点和中心点
    if( osnapMode != AcDb::kOsModeCen )
        return Acad::eOk;
    Acad::ErrorStatus es = Acad::eOk;
    if( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return es;
}
Acad::ErrorStatus AirCurtain::customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    return Acad::eOk;
}
Acad::ErrorStatus AirCurtain::customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        if ( idx == 0 )
        {
            m_insertPt += offset; 			// 插入点偏移
        }
    }
    return Acad::eOk;
}
void AirCurtain::update()
{
    m_alpha = PI / 4;
    m_length = 0.5 * m_width / tan( m_alpha );
}