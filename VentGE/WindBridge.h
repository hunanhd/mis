#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// ����
class VENTGE_EXPORT_API WindBridge : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( WindBridge ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    WindBridge();
    WindBridge( const AcGePoint3d& insertPt, double angle );
    virtual ~WindBridge();
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset ) ;
public:
    double m_width;
    double m_sideLength;
    double m_sideLength2;
    double m_sideAngle;
    double m_linewidth;
    double m_offset; //����Ŀ���
private:
    void drawSide( AcGiWorldDraw* mode );
} ;
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( WindBridge )
#endif
