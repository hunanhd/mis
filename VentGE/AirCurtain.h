#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// 空气幕
class VENTGE_EXPORT_API AirCurtain : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( AirCurtain ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    AirCurtain() ;
    AirCurtain( const AcGePoint3d& insertPt, double angle );
    virtual ~AirCurtain();
    // 重载MineGE虚函数(图形操作)
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
    // 私有函数
private:
    void update();
    // 成员变量
private:
    double m_length;          // 箭头主干一半长度(默认为60)
    double m_width;     // 箭头宽度(默认10)
    double m_alpha;		//偏角
} ;
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( AirCurtain )
#endif