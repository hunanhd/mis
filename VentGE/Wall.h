#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// ��ǽ�������ǽ�����õ���ǽ����ʱ����ǽ
// ��ǽ������
class VENTGE_EXPORT_API Wall : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( Wall ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
protected:
    Wall();
    Wall( const AcGePoint3d& insertPt, double angle );
    virtual void caclBackGroundMinPolygon( AcGePoint3dArray& pts );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode ) ;
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset ) ;
protected:
    double m_width;   // ���ο����
    double m_height;  // ���ο�߶�
    bool needFill;   // �Ƿ����
};
// ���õ���ǽ
class VENTGE_EXPORT_API PermanentWall : public Wall
{
public:
    ACRX_DECLARE_MEMBERS( PermanentWall ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    PermanentWall() ;
    PermanentWall( const AcGePoint3d& insertPt, double angle );
};
// ��ʱ����ǽ
class VENTGE_EXPORT_API TemporaryWall : public Wall
{
public:
    ACRX_DECLARE_MEMBERS( TemporaryWall ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    TemporaryWall() ;
    TemporaryWall( const AcGePoint3d& insertPt, double angle );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode ) ;
} ;
// ���𵲷�ǽ
class VENTGE_EXPORT_API Firewall : public Wall
{
public:
    ACRX_DECLARE_MEMBERS( Firewall ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    Firewall() ;
    Firewall( const AcGePoint3d& insertPt, double angle );
} ;
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( Wall )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( PermanentWall )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( TemporaryWall )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( Firewall )
#endif
