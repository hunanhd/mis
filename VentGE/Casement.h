#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// 调节风窗：墙调风窗，永久风门墙调风窗，临时风门墙调风窗
// 调风窗抽象类
class VENTGE_EXPORT_API Casement : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( Casement ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
protected:
    Casement();
    Casement( const AcGePoint3d& insertPt, double angle );
    virtual void caclBackGroundMinPolygon( AcGePoint3dArray& pts );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode ) ;
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset ) ;
public:
    double m_radius;        // 风门的半径
    double m_length;        // 风门直线的一半长度（从中轴向外的距离）
    double m_width;         // 调节直线的一半宽度
    double m_linewidth;     // 调节直线的线宽(使用多段线绘制)
    double m_dTWidth;       // 所在双线巷道的宽度
    double m_offset;
};
// 墙调风窗
class VENTGE_EXPORT_API WallCasement : public Casement
{
public:
    ACRX_DECLARE_MEMBERS( WallCasement ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    WallCasement() ;
    WallCasement( const AcGePoint3d& insertPt, double angle );
};
// 永久风门墙调风窗
class VENTGE_EXPORT_API PermanentCasement : public Casement
{
public:
    ACRX_DECLARE_MEMBERS( PermanentCasement ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    PermanentCasement() ;
    PermanentCasement( const AcGePoint3d& insertPt, double angle );
};
// 临时风门墙调风窗
class VENTGE_EXPORT_API TemporaryCasement : public Casement
{
public:
    ACRX_DECLARE_MEMBERS( TemporaryCasement ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    TemporaryCasement() ;
    TemporaryCasement( const AcGePoint3d& insertPt, double angle );
} ;
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( Casement )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( WallCasement )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( PermanentCasement )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( TemporaryCasement )
#endif
