#include "StdAfx.h"
// 定义注册服务名称
#ifndef VENTGE_SERVICE_NAME
#define VENTGE_SERVICE_NAME _T("VENTGE_SERVICE_NAME")
#endif
class CVentGEApp : public AcRxArxApp
{
public:
    CVentGEApp () : AcRxArxApp () {}
    virtual AcRx::AppRetCode On_kInitAppMsg ( void* pkt )
    {
        // You *must* call On_kInitAppMsg here
        AcRx::AppRetCode retCode = AcRxArxApp::On_kInitAppMsg ( pkt ) ;
        acrxRegisterAppMDIAware( pkt );
        // 注册服务
        acrxRegisterService( VENTGE_SERVICE_NAME );
        return ( retCode ) ;
    }
    virtual AcRx::AppRetCode On_kUnloadAppMsg ( void* pkt )
    {
        // You *must* call On_kUnloadAppMsg here
        AcRx::AppRetCode retCode = AcRxArxApp::On_kUnloadAppMsg ( pkt ) ;
        // 删除服务
        delete acrxServiceDictionary->remove( VENTGE_SERVICE_NAME );
        return ( retCode ) ;
    }
    virtual void RegisterServerComponents ()
    {
    }
} ;
IMPLEMENT_ARX_ENTRYPOINT( CVentGEApp )
