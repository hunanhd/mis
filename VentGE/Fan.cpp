#include "StdAfx.h"
#include "Fan.h"
#include "MineGE/Drawtool.h"
Adesk::UInt32 Fan::kCurrentVersionNumber = 1 ;
Adesk::UInt32 MainFan::kCurrentVersionNumber = 1 ;
Adesk::UInt32 LocalFan::kCurrentVersionNumber = 1 ;
ACRX_NO_CONS_DEFINE_MEMBERS( Fan, DirGE )
ACRX_DXF_DEFINE_MEMBERS (
    MainFan, Fan,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    主扇, VENTGEAPP
)
ACRX_DXF_DEFINE_MEMBERS (
    LocalFan, Fan,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    局扇, VENTGEAPP
)
Fan::Fan () : DirGE ()
{
    map( _T( "半径" ), &m_radius );
    map( _T( "距离" ), &m_distance );
    m_radius = 30;
    m_distance = 110;
}
Fan::Fan( const AcGePoint3d& insertPt, double angle ) : DirGE( insertPt, angle )
{
    map( _T( "半径" ), &m_radius );
    map( _T( "距离" ), &m_distance );
    m_radius = 30;
    m_distance = 110;
}
Adesk::Boolean Fan::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    return DirGE::customWorldDraw( mode );
}
Acad::ErrorStatus Fan::customTransformBy( const AcGeMatrix3d& xform )
{
    m_insertPt.transformBy( xform );
    // 构造一个倾角向量
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis ); // 得到原有的倾角向量
    // 执行变换
    v.transformBy( xform );
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    return Acad::eOk;
}
Acad::ErrorStatus Fan::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉1种类型的点：插入点
    Acad::ErrorStatus es = Acad::eOk;
    if ( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
        //AcGeVector3d v( AcGeVector3d::kXAxis );
        //v.rotateBy( m_angle, AcGeVector3d::kZAxis );
        //snapPoints.append( m_insertPt + v * m_distance );
    }
    return es;
}
Acad::ErrorStatus Fan::customGetGripPoints (
    AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds
) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    //AcGeVector3d v( AcGeVector3d::kXAxis );
    //v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    //gripPoints.append( m_insertPt + v * m_distance );
    return Acad::eOk;
}
Acad::ErrorStatus Fan::customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        // 始节点
        if ( idx == 0 ) m_insertPt += offset;
        if ( idx == 1 )
        {
            AcGeVector3d v( AcGeVector3d::kXAxis );
            v.rotateBy( m_angle, AcGeVector3d::kZAxis );
            AcGePoint3d pt = m_insertPt + v * m_distance;
            pt += offset; // 进行偏移
            // 目前暂时不考虑distance的变化
            //distance = insertPt.distanceTo(pt); // 计算新的距离
            // 计算角度
            v = pt - m_insertPt;
            m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
        }
    }
    return Acad::eOk;
}
MainFan::MainFan () : Fan()
{
    m_radius = 30;
    m_distance = 110;
}
MainFan::MainFan( const AcGePoint3d& insertPt, double angle ) : Fan( insertPt, angle )
{
    m_radius = 30;
    m_distance = 110;
}
Adesk::Boolean MainFan::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    DrawCircle( mode, m_insertPt, m_radius, false );
    // 计算点坐标
    AcGeVector3d v1( AcGeVector3d::kXAxis ), v2( AcGeVector3d::kXAxis );
    v1.rotateBy( m_angle + PI / 6, AcGeVector3d::kZAxis ); // 倾角：30度
    v2.rotateBy( m_angle - PI / 6, AcGeVector3d::kZAxis );
    double c = 1.1547005383792515290182975610039; // 2除以根号3
    double L = c * m_distance;
    AcGePoint3d firstPt = m_insertPt + v1 * L;
    AcGePoint3d secondPt = m_insertPt + v2 * L;
    // 绘制3条直线
    DrawLine( mode, m_insertPt, firstPt );
    DrawLine( mode, m_insertPt, secondPt );
    DrawLine( mode, firstPt, secondPt );
    return Adesk::kTrue;
}
Acad::ErrorStatus MainFan::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉1种类型的点：插入点
    if( osnapMode != AcDb::kOsModeCen )
        return Acad::eOk;
    Acad::ErrorStatus es = Acad::eOk;
    if ( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
        AcGeVector3d v( AcGeVector3d::kXAxis );
        v.rotateBy( m_angle, AcGeVector3d::kZAxis );
        snapPoints.append( m_insertPt + v * m_distance );
    }
    return es;
}
LocalFan::LocalFan () : Fan ()
{
    m_radius = 10;
    m_distance = 16;
}
LocalFan::LocalFan( const AcGePoint3d& insertPt, double angle ) : Fan( insertPt, angle )
{
    m_radius = 10;
    m_distance = 16;
}
Adesk::Boolean LocalFan::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    drawFan( mode );
    return Adesk::kTrue;
}
AcGePoint3d LocalFan::vp_add( AcGeVector3d v, AcGePoint3d p )
{
    return AcGePoint3d( v.x + p.x, v.y + p.y, v.z + p.z );
}
void LocalFan::drawFan( AcGiWorldDraw* mode )
{
    AcGeVector3d v1( AcGeVector3d::kXAxis ), v2( AcGeVector3d::kXAxis );
    v1.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v2.rotateBy( m_angle + PI * 0.5, AcGeVector3d::kZAxis );
    AcGePoint3d p1, p2, pt1, pt2, pt3, pt4, pt5, pt6, pt7, pt8;
    p1 = vp_add( v2 * m_distance * 0.25, m_insertPt );
    pt1 = vp_add( v1 * m_distance * 0.5, p1 );
    v1.normalize();
    double sin45 = 0.70710678118654752440084436210485;
    v1.rotateBy( PI / 4, AcGeVector3d::kZAxis );
    pt2 = vp_add( v1 * m_distance * 0.25 / sin45, pt1 );
    v1.normalize();
    v1.rotateBy( - 3 * PI / 4, AcGeVector3d::kZAxis );
    pt3 = vp_add( v1 * m_distance, pt2 );
    v1.normalize();
    v1.rotateBy( - 3 * PI / 4, AcGeVector3d::kZAxis );
    pt4 = vp_add( v1 * m_distance * 0.25 / sin45, pt3 );
    v1.normalize();
    v1.rotateBy( PI / 4, AcGeVector3d::kZAxis );
    pt5 = vp_add( v1 * m_distance, pt4 );
    v1.normalize();
    v1.rotateBy( PI / 4, AcGeVector3d::kZAxis );
    pt6 = vp_add( v1 * m_distance * 0.25 / sin45, pt5 );
    v1.normalize();
    v1.rotateBy( - 3 * PI / 4, AcGeVector3d::kZAxis );
    pt7 = vp_add( v1 * m_distance, pt6 );
    v1.normalize();
    v1.rotateBy( - 3 * PI / 4, AcGeVector3d::kZAxis );
    pt8 = vp_add( v1 * m_distance * 0.25 / sin45, pt7 );
    DrawLine( mode, pt1, pt2 );
    DrawLine( mode, pt2, pt3 );
    DrawLine( mode, pt3, pt4 );
    DrawLine( mode, pt4, pt5 );
    DrawLine( mode, pt5, pt6 );
    DrawLine( mode, pt6, pt7 );
    DrawLine( mode, pt7, pt8 );
    DrawLine( mode, pt8, pt1 );
    AcGePoint3d aPt1, aPt2, aPt3, aPt4, aPt5, aPt6, aPt7;
    aPt1 = vp_add( ( pt8 - pt1 ) * 0.5, pt1 );
    aPt2 = vp_add( ( pt4 - pt1 ) * 0.5, pt1 );
    aPt3 = vp_add( ( pt5 - pt4 ) * 0.5, pt4 );
    aPt4 = vp_add( ( pt8 - pt5 ) * 1.0 / 3, pt5 );
    aPt5 = vp_add( ( pt8 - pt5 ) * 1.0 / 3, aPt4 );
    aPt6 = vp_add( ( pt1 - pt8 ) * 2.0 / 3, aPt4 );
    aPt7 = vp_add( ( pt1 - pt8 ) * 2.0 / 3, aPt5 );
    DrawLine( mode, aPt1, aPt2 );
    DrawLine( mode, aPt2, aPt3 );
    DrawLine( mode, aPt3, aPt6 );
    DrawLine( mode, aPt6, aPt4 );
    DrawLine( mode, aPt5, aPt7 );
    DrawLine( mode, aPt7, aPt1 );
    DrawLine( mode, aPt4, aPt5 );
}