#include "StdAfx.h"
#include "Tunnel.h"
#include "MineGE/Drawtool.h"
#include <cmath>
Adesk::UInt32 Tunnel::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    Tunnel, EdgeGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    巷道, VENTGEAPP
)
Tunnel::Tunnel () : EdgeGE ()
{
    map( _T( "宽度" ), &m_width );
    m_width = 30;
}
Tunnel::Tunnel( const AcGePoint3d& startPt, const AcGePoint3d& endPt ) : EdgeGE( startPt, endPt )
{
    map( _T( "宽度" ), &m_width );
    m_width = 30;
    update();
}
Tunnel::~Tunnel ()
{
}
void Tunnel::update()
{
    caclStartPoint( m_leftStartPt, m_rightStartPt );
    caclEndPoint( m_leftEndPt, m_rightEndPt );
}
void Tunnel::caclStartPoint( AcGePoint3d& startPt1, AcGePoint3d& startPt2 )
{
    AcGeVector3d v = m_endPt - m_startPt;
    v.normalize();
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    startPt1 = m_startPt + v * m_width * 0.5;
    v.rotateBy( PI, AcGeVector3d::kZAxis );
    startPt2 = m_startPt + v * m_width * 0.5;
}
void Tunnel::caclEndPoint( AcGePoint3d& endPt1, AcGePoint3d& endPt2 )
{
    AcGeVector3d v = m_endPt - m_startPt;
    v.normalize();
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    endPt1 = m_endPt + v * m_width * 0.5;
    v.rotateBy( PI, AcGeVector3d::kZAxis );
    endPt2 = m_endPt + v * m_width * 0.5;
}
void Tunnel::dealWithStartPointBoundary( const AcGeRay3d& boundaryLine )
{
    AcGeLine3d line( m_leftStartPt, m_leftEndPt );
    AcGePoint3d pt;
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) ) // 计算左侧轮廓线与边界线的交叉点
    {
        //acutPrintf(_T("\n调整左侧轮廓线的始点坐标%.3f, %.3f"), pt.x, pt.y);
        m_leftStartPt = pt;                        // 调整左侧轮廓线的始点坐标
    }
    line.set( m_rightStartPt, m_rightEndPt );
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) ) // 计算右侧轮廓线与边界线的交叉点
    {
        //acutPrintf(_T("\n调整右侧轮廓线的始点坐标%.3f, %.3f"), pt.x, pt.y);
        m_rightStartPt = pt;                       // 调整右侧轮廓线的始点坐标
    }
}
void Tunnel::dealWithEndPointBoundary( const AcGeRay3d& boundaryLine )
{
    AcGeLine3d line( m_leftStartPt, m_leftEndPt );
    AcGePoint3d pt;
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) ) // 计算左侧轮廓线与边界线的交叉点
    {
        //acutPrintf(_T("\n调整左侧轮廓线的末点坐标%.3f, %.3f"), pt.x, pt.y);
        m_leftEndPt = pt;                                         // 调整左侧轮廓线的末点坐标
    }
    line.set( m_rightStartPt, m_rightEndPt );
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) ) // 计算右侧轮廓线与边界线的交叉点
    {
        //acutPrintf(_T("\n调整右侧轮廓线的末点坐标%.3f, %.3f"), pt.x, pt.y);
        m_rightEndPt = pt;                          // 调整右侧轮廓线的末点坐标
    }
}
void Tunnel::reverse()
{
    EdgeGE::reverse();
    update();
}
void Tunnel::extendByLength( double length )
{
    AcGeVector3d v = m_endPt - m_startPt;
    v.normalize();
    m_endPt = m_endPt + v * length; // 更新末点坐标
    update(); // 更新其它参数
}
Acad::ErrorStatus Tunnel::dwgOutFields ( AcDbDwgFiler* pFiler ) const
{
    assertReadEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgOutFields"));
    Acad::ErrorStatus es = EdgeGE::dwgOutFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    if ( ( es = pFiler->writeUInt32 ( Tunnel::kCurrentVersionNumber ) ) != Acad::eOk )
        return ( es ) ;
    // 保存始末点坐标
    pFiler->writeItem( m_leftStartPt );
    pFiler->writeItem( m_leftEndPt );
    pFiler->writeItem( m_rightStartPt );
    pFiler->writeItem( m_rightEndPt );
    pFiler->writeItem( m_width );
    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus Tunnel::dwgInFields ( AcDbDwgFiler* pFiler )
{
    assertWriteEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgInFields"));
    Acad::ErrorStatus es = EdgeGE::dwgInFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    Adesk::UInt32 version = 0 ;
    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
        return ( es ) ;
    if ( version > Tunnel::kCurrentVersionNumber )
        return ( Acad::eMakeMeProxy ) ;
    // 读取始末点坐标
    pFiler->readItem( &m_leftStartPt );
    pFiler->readItem( &m_leftEndPt );
    pFiler->readItem( &m_rightStartPt );
    pFiler->readItem( &m_rightEndPt );
    pFiler->readItem( &m_width );
    return ( pFiler->filerStatus () ) ;
}
void Tunnel::minPolygon( AcGePoint3dArray& pts )
{
    pts.append( m_startPt );
    pts.append( m_leftStartPt );
    pts.append( m_leftEndPt );
    pts.append( m_endPt );
    pts.append( m_rightEndPt );
    pts.append( m_rightStartPt );
}
Adesk::Boolean Tunnel::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled();
    double c = 0.618;
    DrawLine( mode, m_leftStartPt, m_leftEndPt );   // 绘制左线
    DrawLine( mode, m_rightStartPt, m_rightEndPt );	// 绘制右线
    return Adesk::kTrue;
}
Acad::ErrorStatus Tunnel::customTransformBy( const AcGeMatrix3d& xform )
{
    m_startPt.transformBy( xform );
    m_endPt.transformBy( xform );
    update();
    return Acad::eOk;
}
Acad::ErrorStatus Tunnel::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉端点
    if ( osnapMode == AcDb::kOsModeEnd )
    {
        snapPoints.append( m_startPt );
        snapPoints.append( m_endPt );
    }
    return Acad::eOk;
}
Acad::ErrorStatus Tunnel::customGetGripPoints ( AcGePoint3dArray& gripPoints,
        AcDbIntArray& osnapModes,
        AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_startPt );
    if( m_startPt == m_endPt )
    {
        AcGePoint3d pt( m_startPt );
        pt.x = pt.x + m_width * 0.3;
        gripPoints.append( pt );
    }
    else
    {
        gripPoints.append( m_endPt );
    }
    return Acad::eOk;
}
Acad::ErrorStatus Tunnel::customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        if ( idx == 0 )
        {
            m_startPt += offset;
        }
        if ( idx == 1 )
        {
            m_endPt += offset;
        }
        update();
    }
    return Acad::eOk;
}
void Tunnel::onAfterSetData( const CString& field, const CString& value )
{
    Tunnel* pEnt = Tunnel::cast( this );
    if( pEnt == 0 ) return;
    // 调用基类的虚函数
    EdgeGE::onAfterSetData( field, value );
    if( field == _T( "起点坐标" ) )
    {
        pEnt->update();
    }
    else if( field == _T( "末点坐标" ) )
    {
        pEnt->update();
    }
    else if( field == _T( "宽度" ) )
    {
        pEnt->update();
    }
}
