#pragma once
#include "Tunnel.h"
#include "dlimexp.h"
// 双弧线巷道效果
class VENTGE_EXPORT_API ArcTunnel : public EdgeGE
{
public:
    ACRX_DECLARE_MEMBERS( ArcTunnel ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    ArcTunnel( void );
    ArcTunnel( const AcGePoint3d& startPt, const AcGePoint3d& endPt, const AcGePoint3d& thirdPt );
    // 获取/设置半径参数
    AcGePoint3d getThirdPt() const;
    void setThirdPt( const AcGePoint3d& pt );
    AcGePoint3d getArcCntPt() const;
    bool ptIsOnArc( AcGePoint3d pt ) const;
    double getArcAngle( AcGePoint3d insertPt );
    // 重载EdgeGE类虚函数(用于实现节点闭合)
public:
    virtual void reverse();
    virtual AcGeVector3d getStartPointInExtendAngle() const;
    virtual AcGeVector3d getEndPointInExtendAngle() const;
    virtual void dealWithStartPointBoundary( const AcGeRay3d& boundaryLine );
    virtual void dealWithEndPointBoundary( const AcGeRay3d& boundaryLine );
    virtual void extendByLength( double length );
    virtual void update();
    // 重载AcDbObject的虚函数(实现dwg的读写)
public:
    virtual Acad::ErrorStatus dwgOutFields ( AcDbDwgFiler* pFiler ) const;
    virtual Acad::ErrorStatus dwgInFields ( AcDbDwgFiler* pFiler );
    // 重载MineGE虚函数(图形交互操作)
protected:
    // 具体的绘制实现
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset ) ;
    // 计算图形的最小多边形
    virtual void minPolygon( AcGePoint3dArray& pts );
    // 重载Data基类的虚函数(当几何参数被修改后,其它的参数是否需要同步更新?)
protected:
    // bool set( const CString& field, ... )函数调用后触发该消息
    virtual void onAfterSetData( const CString& field, const CString& value );
private:
    void caclInPoint( AcGePoint3d& startPt, AcGePoint3d& endPt );
    void caclOutPoint( AcGePoint3d& startPt, AcGePoint3d& endPt );
private:
    double m_width; // 默认为30
    AcGePoint3d m_leftStartPt, m_leftEndPt;
    AcGePoint3d m_rightStartPt, m_rightEndPt;
    AcGePoint3d m_thirdPt;
};
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( ArcTunnel )
#endif