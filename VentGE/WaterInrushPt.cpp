#include "StdAfx.h"
#include "WaterInrushPt.h"
#include "MineGE/DrawTool.h"
#include "MineGE/DrawSpecial.h"
Adesk::UInt32 WaterInrushPt::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS ( WaterInrushPt, DirGE,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          突水点, VENTGEAPP )
WaterInrushPt::WaterInrushPt ()
{
    m_radius = 4;
}
WaterInrushPt::WaterInrushPt( const AcGePoint3d& insertPt, double angle )
    : DirGE( insertPt, angle )
{
    m_radius = 4;
}
WaterInrushPt::~WaterInrushPt ()
{
}
Adesk::Boolean WaterInrushPt::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    if( v.x < 0 ) v.negate();
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    DrawCircle( mode, m_insertPt, m_radius, false );
    v.rotateBy( PI / 2 , AcGeVector3d::kZAxis );
    v.normalize();
    AcGePoint3d pt1 = m_insertPt + v * m_radius / 2;
    AcGePoint3d pt2 = m_insertPt - v * m_radius / 2;
    DrawWave( mode, m_insertPt, m_angle, PI / 8, 1.2 * m_radius );
    DrawWave( mode, pt1, m_angle, PI / 8, 1.2 * m_radius );
    DrawWave( mode, pt2, m_angle, PI / 8, 1.2 * m_radius );
    return Adesk::kTrue;
}
Acad::ErrorStatus WaterInrushPt::customTransformBy( const AcGeMatrix3d& xform )
{
    // 插入点变换
    //m_insertPt.transformBy( xform );
    //m_pt.transformBy( xform );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.transformBy( xform );
    //m_radius = v.length();
    // 1) 构造一个圆
    AcDbCircle circle( m_insertPt, AcGeVector3d::kZAxis, m_radius );
    // 2) 圆调用transformBy()方法进行变换
    circle.transformBy( xform );
    //3) 获取更新后的参数
    m_insertPt = circle.center();     // 获取变换后的圆心坐标
    m_radius = circle.radius(); // 获取变换后的圆半径
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis ); // 变换后的旋转角度
    return Acad::eOk;
}

Acad::ErrorStatus WaterInrushPt::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    if( osnapMode != AcDb::kOsModeCen )
        return Acad::eOk;
    Acad::ErrorStatus es = Acad::eOk;
    if( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return es;
}
Acad::ErrorStatus WaterInrushPt::customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    //AcGeVector3d v(AcGeVector3d::kXAxis);
    //v.rotateBy(m_angle,AcGeVector3d::kZAxis);
    //v *= m_radius;
    //gripPoints.append(m_insertPt + v);             // 正方向端点作为夹点
    return Acad::eOk;
}
Acad::ErrorStatus WaterInrushPt::customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    int len = indices.length();
    for( int i = 0; i < len; i++ )
    {
        int idx = indices.at( i );
        if( idx == 0 )
        {
            // 当前夹点是圆心，移动图形
            m_insertPt += offset;       // 对圆心执行偏移变换
        }
        if( idx == 1 )
        {
            // 缩放圆
            // 1) 计算x轴的端点坐标
            AcGeVector3d v( AcGeVector3d::kXAxis );
            v.rotateBy( m_angle, AcGeVector3d::kZAxis );
            AcGePoint3d pt = m_insertPt + v * m_radius;
            // 2) 进行坐标偏移计算
            pt += offset;
            // 3) 计算新坐标与圆心之间的长度，并作为圆半径
            // 坐标相减，得到一个向量，然后得到向量长度
            m_radius = ( pt - m_insertPt ).length();
        }
    }
    return Acad::eOk;
}
