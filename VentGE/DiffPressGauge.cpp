#include "StdAfx.h"
#include "DiffPressGauge.h"
Adesk::UInt32 DiffPressGauge::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    DiffPressGauge, MineGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    水柱计, VENTGEAPP
)
DiffPressGauge::DiffPressGauge(): MineGE ()
{
    m_length = 18;
    m_width = 3;
}
DiffPressGauge::DiffPressGauge( const AcGePoint3d& insrtPt ): MineGE (), m_insertPt( insrtPt )
{
    m_length = 18;
    m_width = 3;
}
DiffPressGauge::~DiffPressGauge()
{
}
Acad::ErrorStatus DiffPressGauge::dwgOutFields( AcDbDwgFiler* pFiler ) const
{
    assertReadEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgOutFields"));
    Acad::ErrorStatus es = MineGE::dwgOutFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    if ( ( es = pFiler->writeUInt32 ( DiffPressGauge::kCurrentVersionNumber ) ) != Acad::eOk )
        return ( es ) ;
    pFiler->writeItem( m_insertPt );
    pFiler->writeItem( m_leftPt );
    pFiler->writeItem( m_rightPt );
    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus DiffPressGauge::dwgInFields( AcDbDwgFiler* pFiler )
{
    assertWriteEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgInFields"));
    Acad::ErrorStatus es = MineGE::dwgInFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    Adesk::UInt32 version = 0 ;
    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
        return ( es ) ;
    if ( version > DiffPressGauge::kCurrentVersionNumber )
        return ( Acad::eMakeMeProxy ) ;
    pFiler->readItem( &m_insertPt );
    pFiler->readItem( &m_leftPt );
    pFiler->readItem( &m_rightPt );
    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus DiffPressGauge::customTransformBy( const AcGeMatrix3d& xform )
{
    m_insertPt.transformBy( xform );
    return Acad::eOk;
}
Acad::ErrorStatus DiffPressGauge::customGetOsnapPoints( AcDb::OsnapMode osnapMode, Adesk::GsMarker gsSelectionMark, const AcGePoint3d& pickPoint, const AcGePoint3d& lastPoint, const AcGeMatrix3d& viewXform, AcGePoint3dArray& snapPoints, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉1种类型的点：插入点
    Acad::ErrorStatus es = Acad::eOk;
    if ( osnapMode == AcDb::kOsModeEnd )
    {
        snapPoints.append( m_insertPt );
        snapPoints.append( m_leftPt );
        snapPoints.append( m_rightPt );
    }
    return es;
}
Acad::ErrorStatus DiffPressGauge::customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    return Acad::eOk;
}
Acad::ErrorStatus DiffPressGauge::customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        if ( idx == 0 ) m_insertPt += offset;
    }
    return Acad::eOk;
}
Adesk::Boolean DiffPressGauge::customWorldDraw( AcGiWorldDraw* mode )
{
    //AcGeVector3d v(AcGeVector3d::kXAxis);
    //AcGePoint3d pt0 = m_insertPt + v * m_length / 2;
    //v.rotateBy(PI/2,AcGeVector3d::kZAxis);
    //AcGePoint3d pt1 = pt0 + v * m_width / 2;
    //AcGePoint3d pt2 = pt0 - v * m_width / 4;
    //AcGePoint3d pt4 = pt0 - v * m_width / 2;
    //v.rotateBy(PI/2,AcGeVector3d::kZAxis);
    //AcGePoint3d pt6 = pt1 + v * m_length;
    //AcGePoint3d pt5 = pt4 + v * m_length;
    //AcGePoint3d pt10 = pt1 + v * 2 * m_length / 5;
    //AcGePoint3d pt7 = pt6 - v * 2 * m_length / 5;
    //v.rotateBy(-PI/2,AcGeVector3d::kZAxis);
    //AcGePoint3d pt8 = pt7 + v * m_length / 5;
    //AcGePoint3d pt9 = pt10 + v * m_length / 5;
    //v.rotateBy(-PI/4,AcGeVector3d::kZAxis);
    //AcGePoint3d pt3 = pt2 + v * 3 * m_length / 4;
    //DrawLine(mode,pt1,pt2);
    //DrawLine(mode,pt2,pt3);
    //DrawLine(mode,pt3,pt4);
    //DrawLine(mode,pt4,pt5);
    //DrawLine(mode,pt5,pt6);
    //DrawLine(mode,pt6,pt7);
    //DrawLine(mode,pt7,pt8);
    //DrawLine(mode,pt8,pt9);
    //DrawLine(mode,pt9,pt10);
    //DrawLine(mode,pt10,pt1);
    myDraw( mode );
    return 	Adesk::kTrue;
}
void DiffPressGauge::myDraw( AcGiWorldDraw* mode )
{
    double theta = PI / 3;
    AcGeVector3d v( 1, 0, 0 );
    AcGePoint3d pt1 = m_insertPt + v * 1.5 * m_width;
    AcGePoint3d pt2 = m_insertPt + v * 2.5 * m_width;
    AcGePoint3d pt3 = m_insertPt - v * 1.5 * m_width;
    AcGePoint3d pt4 = m_insertPt - v * 2.5 * m_width;
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    AcGePoint3d pt5 = pt4 + v * m_length;
    AcGePoint3d pt6 = pt3 + v * m_length;
    AcGePoint3d pt7 = pt1 + v * m_length;
    AcGePoint3d pt8 = pt2 + v * m_length;
    AcGePoint3d pt9 = m_insertPt - v * 1.5 * m_width;
    AcGePoint3d pt10 = m_insertPt - v * 2.5 * m_width;
    m_leftPt = pt5;
    m_rightPt = pt8;
    DrawLine( mode, pt1, pt7 );
    DrawLine( mode, pt2, pt8 );
    DrawLine( mode, pt3, pt6 );
    DrawLine( mode, pt4, pt5 );
    DrawArc( mode, pt1, pt9, pt3, false );
    DrawArc( mode, pt2, pt10, pt4, false );
    DrawMText( mode, m_leftPt + v * m_width / 4, 0, _T( "+" ), m_width * 0.5 );
    DrawMText( mode, m_rightPt + v * m_width / 4, 0, _T( "-" ), m_width * 0.5 );
}