#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// ��վ��
class VENTGE_EXPORT_API MachineStation : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( MachineStation ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    MachineStation() ;
    MachineStation( const AcGePoint3d& insertPt, double angle );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints(
        AcGePoint3dArray& gripPoints,
        AcDbIntArray& osnapModes,
        AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
public:
    // extra param
    double m_radius; // �뾶
    double m_distance; // ����
};
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( MachineStation )
#endif
