#include "StdAfx.h"
#include "MachineStation.h"
#include "MineGE/Drawtool.h"
Adesk::UInt32 MachineStation::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    MachineStation, DirGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    机站, VENTGEAPP
)
MachineStation::MachineStation () : DirGE ()
{
    m_radius = 30;
    m_distance = 110;
}
MachineStation::MachineStation( const AcGePoint3d& insertPt, double angle ) : DirGE( insertPt, angle )
{
    m_radius = 30;
    m_distance = 110;
}
Adesk::Boolean MachineStation::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    int colorIndex = mode->subEntityTraits().color();
    mode->subEntityTraits().setColor( 160 ); //蓝色
    DrawCircle( mode, m_insertPt, m_radius, false );
    // 计算点坐标
    AcGeVector3d v1( AcGeVector3d::kXAxis ), v2( AcGeVector3d::kXAxis );
    v1.rotateBy( m_angle + PI / 6, AcGeVector3d::kZAxis ); // 倾角：30度
    v2.rotateBy( m_angle - PI / 6, AcGeVector3d::kZAxis );
    double c = 1.1547005383792515290182975610039; // 2除以根号3
    double L = c * m_distance;
    AcGePoint3d firstPt = m_insertPt + v1 * L;
    AcGePoint3d secondPt = m_insertPt + v2 * L;
    // 绘制3条直线
    DrawLine( mode, m_insertPt, firstPt );
    DrawLine( mode, m_insertPt, secondPt );
    DrawLine( mode, firstPt, secondPt );
    mode->subEntityTraits().setColor( colorIndex ); //恢复颜色
    return Adesk::kTrue;
}
Acad::ErrorStatus MachineStation::customTransformBy( const AcGeMatrix3d& xform )
{
    m_insertPt.transformBy( xform );
    // 构造一个倾角向量
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis ); // 得到原有的倾角向量
    // 执行变换
    v.transformBy( xform );
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    return Acad::eOk;
}
Acad::ErrorStatus MachineStation::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉1种类型的点：插入点
    Acad::ErrorStatus es = Acad::eOk;
    if ( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return es;
}
Acad::ErrorStatus MachineStation::customGetGripPoints (
    AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds
) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    return Acad::eOk;
}
Acad::ErrorStatus MachineStation::customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        // 始节点
        if ( idx == 0 ) m_insertPt += offset;
        if ( idx == 1 )
        {
            AcGeVector3d v( AcGeVector3d::kXAxis );
            v.rotateBy( m_angle, AcGeVector3d::kZAxis );
            AcGePoint3d pt = m_insertPt + v * m_distance;
            pt += offset; // 进行偏移
            // 目前暂时不考虑distance的变化
            //distance = insertPt.distanceTo(pt); // 计算新的距离
            // 计算角度
            v = pt - m_insertPt;
            m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
        }
    }
    return Acad::eOk;
}