#pragma once
#include "MineGE/MineGE.h"
#include "dlimexp.h"
#include "MineGE/DrawTool.h"
//水柱计图元
class VENTGE_EXPORT_API DiffPressGauge : public MineGE
{
public:
    ACRX_DECLARE_MEMBERS( DiffPressGauge ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
    // 重载AcDbObject的虚函数(实现dwg读写)
public:
    virtual Acad::ErrorStatus dwgOutFields ( AcDbDwgFiler* pFiler ) const;
    virtual Acad::ErrorStatus dwgInFields ( AcDbDwgFiler* pFiler );
    // 重载AcDbEntity虚函数(图形交互操作)
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
public:
    DiffPressGauge();
    virtual ~DiffPressGauge();
    DiffPressGauge( const AcGePoint3d& insrtPt );
private:
    void myDraw( AcGiWorldDraw* mode );
private:
    AcGePoint3d m_insertPt;
    AcGePoint3d m_leftPt;
    AcGePoint3d m_rightPt;
    double m_length;
    double m_width;
} ;
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( DiffPressGauge )
#endif