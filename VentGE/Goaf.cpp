#include "StdAfx.h"
#include "Goaf.h"
#include "MineGE/DrawTool.h"
/* 全局函数(HatchRatio, 计算填充比例) */
extern double CaclHatchRatio( const AcGePoint3dArray& polygon, double originScale );
Adesk::UInt32 Goaf::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    Goaf, MineGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    采空区, VENTGEAPP
)
Goaf::Goaf () : MineGE ()
{
    m_scale = 10;
    m_width = 30;
}
Goaf::~Goaf ()
{
}
void Goaf::addPoint( const AcGePoint3d& pt )
{
    assertWriteEnabled();
    if( !m_pts.contains( pt ) )
    {
        m_pts.append( pt );
    }
}
void Goaf::getPolygon( AcGePoint3dArray& polygon ) const
{
    assertReadEnabled();
    polygon.append( m_pts );
}
static AcGePoint3d CacLineClosePt( const AcGePoint3d& spt, const AcGePoint3d& ept, const AcGePoint3d& pt )
{
    // 构造一条几何线段
    AcGeLineSeg3d line( spt, ept );
    return line.closestPointTo( pt ); // 计算距离中线最近的点
}
void Goaf::splitLine( int pos, const AcGePoint3d& pt )
{
    assertWriteEnabled();
    int n = m_pts.length();
    if( pos < 0 || pos >= n ) return;
    if( m_pts.contains( pt ) ) return;
    m_pts.insertAt( pos + 1, CacLineClosePt( m_pts[pos], m_pts[( pos + 1 ) % n], pt ) );
}
void Goaf::removePoint( int pos )
{
    assertWriteEnabled();
    int n = m_pts.length();
    if( n < 5 ) return; // 至少是一个四边形
    if( pos < 0 || pos >= n ) return;
    m_pts.removeAt( pos );
}
void Goaf::setPoint( int pos, const AcGePoint3d& pt )
{
    assertWriteEnabled();
    int n = m_pts.length();
    if( pos < 0 || pos >= n ) return;
    if( m_pts.contains( pt ) ) return;
    m_pts[pos] = pt;
}
Acad::ErrorStatus Goaf::dwgOutFields ( AcDbDwgFiler* pFiler ) const
{
    assertReadEnabled () ;
    Acad::ErrorStatus es = MineGE::dwgOutFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    if ( ( es = pFiler->writeUInt32( Goaf::kCurrentVersionNumber ) ) != Acad::eOk )
        return ( es ) ;
    pFiler->writeInt32( m_pts.length() );
    for( int i = 0; i < m_pts.length(); i++ )
    {
        pFiler->writePoint3d( m_pts[i] );
    }
    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus Goaf::dwgInFields ( AcDbDwgFiler* pFiler )
{
    assertWriteEnabled () ;
    Acad::ErrorStatus es = MineGE::dwgInFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    Adesk::UInt32 version = 0 ;
    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
        return ( es ) ;
    if ( version > Goaf::kCurrentVersionNumber )
        return ( Acad::eMakeMeProxy ) ;
    long n = 0;
    pFiler->readInt32( &n );
    m_pts.removeAll();
    for( int i = 0; i < ( int )n; i++ )
    {
        AcGePoint3d pt;
        pFiler->readPoint3d( &pt );
        m_pts.append( pt );
    }
    return ( pFiler->filerStatus () ) ;
}
void Goaf::caclBackGroundMinPolygon( AcGePoint3dArray& pts )
{
}
static void DrawBoundary( AcGiWorldDraw* mode, const AcGePoint3dArray& polygon, double offset )
{
    AcGePoint3dArray offset_polygon;
    if( OffSetPolygon( polygon, offset, false, offset_polygon ) )
    {
        DrawPolygon( mode, offset_polygon, false );
    }
}
static void DrawGoaf( AcGiWorldDraw* mode, const AcGePoint3dArray& polygon, double offset, double scale )
{
    AcGePoint3dArray offset_polygon;
    if( OffSetPolygon( polygon, offset, true, offset_polygon ) )
    {
        DrawPolygonHatch( mode, offset_polygon, _T( "GRAVEL" ), scale );
    }
}
Adesk::Boolean Goaf::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled();
    update();
    if( m_pts.length() > 2 )
    {
        // 绘制外框
        DrawBoundary( mode, m_pts, m_width * 0.5 );
        // 绘制采空区填充
        //acutPrintf(_T("\nscale:%.5f"), m_scale);
        const double c = 1.0;
        DrawGoaf( mode, m_pts, m_width * 0.5, c * m_scale );
    }
    return Adesk::kTrue;
}
Acad::ErrorStatus Goaf::customTransformBy( const AcGeMatrix3d& xform )
{
    for( int i = 0; i < m_pts.length(); i++ )
    {
        m_pts[i].transformBy( xform );
    }
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v *= m_scale;
    v.transformBy( xform );
    m_scale = v.length();
    return Acad::eOk;
}
Acad::ErrorStatus Goaf::customGetOsnapPoints( AcDb::OsnapMode osnapMode, Adesk::GsMarker gsSelectionMark, const AcGePoint3d& pickPoint, const AcGePoint3d& lastPoint, const AcGeMatrix3d& viewXform, AcGePoint3dArray& snapPoints, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    if( osnapMode == AcDb::kOsModeEnd )
    {
        snapPoints.append( m_pts );
    }
    return Acad::eOk;
}
Acad::ErrorStatus Goaf::customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_pts );
    return Acad::eOk;
}
Acad::ErrorStatus Goaf::customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    int n = m_pts.length();
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        if( idx >= 0 && idx < n )
        {
            AcGePoint3d pt = m_pts[idx] + offset;
            if( !m_pts.contains( pt ) )
            {
                m_pts[idx] = pt;
            }
        }
    }
    return Acad::eOk;
}
void Goaf::update()
{
    // 根据多边形的边长计算缩放比例
    if( m_pts.length() < 3 ) return;
    m_scale = CaclHatchRatio( m_pts, m_scale );
    //m_scale = m_width*m_scale;
}