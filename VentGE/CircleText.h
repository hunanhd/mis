#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// 圆圈里带图例（字母）图元
class VENTGE_EXPORT_API CircleText : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( CircleText ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    CircleText() ;
    CircleText( const AcGePoint3d& insertPt, double angle );
    virtual ~CircleText();
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
protected:
    double m_radius; //半径
    CString m_type;	//图例
} ;
// 附加动力
class VENTGE_EXPORT_API AddPower : public CircleText
{
public:
    ACRX_DECLARE_MEMBERS( AddPower ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    AddPower() ;
    AddPower( const AcGePoint3d& insertPt, double angle );
};
// 局部阻力
class VENTGE_EXPORT_API LocalResis : public CircleText
{
public:
    ACRX_DECLARE_MEMBERS( LocalResis ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    LocalResis() ;
    LocalResis( const AcGePoint3d& insertPt, double angle );
};
// 热源
class VENTGE_EXPORT_API HeatSource : public CircleText
{
public:
    ACRX_DECLARE_MEMBERS( HeatSource ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    HeatSource() ;
    HeatSource( const AcGePoint3d& insertPt, double angle );
};
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( CircleText )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( AddPower )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( LocalResis )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( HeatSource )
#endif