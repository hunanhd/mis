#include "StdAfx.h"
#include "WorkSurface.h"
#include "MineGE/Drawtool.h"
Adesk::UInt32 WorkSurface::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    WorkSurface, EdgeGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    回采工作面, VENTGEAPP
)
WorkSurface::WorkSurface () : EdgeGE ()
{
    m_width = 30;
    m_trunkWidth = 20;
    m_trunkLength = 70;
    m_arrowWidth = 15;
    m_arrowLength = 20;
    m_clockWise = false;
}
WorkSurface::WorkSurface( const AcGePoint3d& startPt, const AcGePoint3d& endPt ) : EdgeGE( startPt, endPt )
{
    m_width = 30;
    m_trunkWidth = 20;
    m_trunkLength = 70;
    m_arrowWidth = 15;
    m_arrowLength = 20;
    m_clockWise = false;
    update();
}
WorkSurface::~WorkSurface ()
{
}
// 计算箭头的起始中点
static AcGePoint3d CaclArrowPoint( const AcGePoint3d& m_startPt, const AcGePoint3d& m_endPt, double m_width, double angle )
{
    AcGeVector3d v = m_endPt - m_startPt;
    AcGePoint3d pt = m_startPt + v * 0.5;
    v.normalize();
    v.rotateBy( angle, AcGeVector3d::kZAxis );
    return ( pt + v * m_width );
}
void WorkSurface::dealWithStartPointBoundary( const AcGeRay3d& boundaryLine )
{
    AcGeLine3d line( m_leftStartPt, m_leftEndPt );
    AcGePoint3d pt;
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) ) // 计算左侧轮廓线与边界线的交叉点
    {
        //acutPrintf(_T("\n调整左侧轮廓线的始点坐标%.3f, %.3f"), pt.x, pt.y);
        m_leftStartPt = pt;                        // 调整左侧轮廓线的始点坐标
    }
    line.set( m_rightStartPt, m_rightEndPt );
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) ) // 计算右侧轮廓线与边界线的交叉点
    {
        //acutPrintf(_T("\n调整右侧轮廓线的始点坐标%.3f, %.3f"), pt.x, pt.y);
        m_rightStartPt = pt;                       // 调整右侧轮廓线的始点坐标
    }
}
void WorkSurface::dealWithEndPointBoundary( const AcGeRay3d& boundaryLine )
{
    AcGeLine3d line( m_leftStartPt, m_leftEndPt );
    AcGePoint3d pt;
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) ) // 计算左侧轮廓线与边界线的交叉点
    {
        //acutPrintf(_T("\n调整左侧轮廓线的末点坐标%.3f, %.3f"), pt.x, pt.y);
        m_leftEndPt = pt;                                         // 调整左侧轮廓线的末点坐标
    }
    line.set( m_rightStartPt, m_rightEndPt );
    if( Adesk::kTrue == line.intersectWith( boundaryLine, pt ) ) // 计算右侧轮廓线与边界线的交叉点
    {
        //acutPrintf(_T("\n调整右侧轮廓线的末点坐标%.3f, %.3f"), pt.x, pt.y);
        m_rightEndPt = pt;                          // 调整右侧轮廓线的末点坐标
    }
}
void WorkSurface::reverse()
{
    EdgeGE::reverse();
    update();
}
void WorkSurface::extendByLength( double length )
{
    AcGeVector3d v = m_endPt - m_startPt;
    v.normalize();
    m_endPt = m_endPt + v * length; // 更新末点坐标
    update(); // 更新其它参数
}
void WorkSurface::drawArrow( AcGiWorldDraw* mode, bool clockWise )
{
    double angle = ( clockWise ? ( -PI / 2 ) : ( PI / 2 ) );
    // 主干中心始点坐标
    AcGePoint3d pt = CaclArrowPoint( m_startPt, m_endPt, m_width / 2, angle ); // 中点
    AcGeVector3d v = m_endPt - m_startPt;
    v.normalize();  // 向量标准化(|v|=1)
    // 主干两侧的始点坐标
    AcGePoint3d sp1 = pt + v * m_trunkWidth;
    AcGePoint3d sp2 = pt - v * m_trunkWidth;
    v.rotateBy( angle, AcGeVector3d::kZAxis );
    // 主干两侧的末点坐标
    AcGePoint3d tp1 = sp1 + v * m_trunkLength;
    AcGePoint3d tp2 = sp2 + v * m_trunkLength;
    // 箭头尖端坐标
    AcGePoint3d ap = pt + v * ( m_trunkLength + m_arrowLength );
	m_ap = ap;
    v.rotateBy( -1 * angle, AcGeVector3d::kZAxis );
    // 箭头两侧的坐标
    AcGePoint3d ap1 = tp1 + v * m_arrowWidth;
    AcGePoint3d ap2 = tp2 - v * m_arrowWidth;
    // 绘制箭头
    DrawLine( mode, sp1, tp1 );
    DrawLine( mode, tp1, ap1 );
    DrawLine( mode, ap1, ap );
    DrawLine( mode, ap, ap2 );
    DrawLine( mode, ap2, tp2 );
    DrawLine( mode, tp2, sp2 );
}
void WorkSurface::drawText( AcGiWorldDraw* mode )
{
    // 绘制文字
    AcGeVector3d v = m_endPt - m_startPt;
    AcGePoint3d pt = m_startPt + v * 0.5; // 中心点
    if( v.x < 0 ) v.negate();
    double angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    v.normalize();
    v.rotateBy( PI / 2, AcGeVector3d::kZAxis ); // 始终与文字反向
    pt += v * m_width * 0.5;
    DrawMText( mode, pt, angle, _T( "回采工作面" ), m_width * 0.618, AcDbMText::kBottomCenter );
}
Adesk::Boolean WorkSurface::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled();
    DrawLine( mode, m_leftStartPt, m_leftEndPt );   // 绘制左线
    DrawLine( mode, m_rightStartPt, m_rightEndPt );	// 绘制右线
    // 绘制文字
    //drawText( mode );
    // 绘制回采箭头
    drawArrow( mode, m_clockWise );
    return Adesk::kTrue;
}
void WorkSurface::minPolygon( AcGePoint3dArray& pts )
{
    pts.append( m_startPt );
    pts.append( m_leftStartPt );
    pts.append( m_leftEndPt );
    pts.append( m_endPt );
    pts.append( m_rightEndPt );
    pts.append( m_rightStartPt );
}
Acad::ErrorStatus WorkSurface::customGetGripPoints( AcGePoint3dArray& gripPoints,
        AcDbIntArray& osnapModes,
        AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_startPt );
    if( m_startPt == m_endPt )
    {
        AcGePoint3d pt( m_startPt );
        pt.x = pt.x + m_width * 0.3;
        gripPoints.append( pt );
    }
    else
    {
        gripPoints.append( m_endPt );
        double angle = ( m_clockWise ? ( -PI / 2 ) : ( PI / 2 ) );
        // 主干中心始点坐标
        AcGePoint3d pt = CaclArrowPoint( m_startPt, m_endPt, m_width / 2, angle ); // 中点
        AcGeVector3d v = m_endPt - m_startPt;
        v.normalize();  // 向量标准化(|v|=1)
        v.rotateBy( angle, AcGeVector3d::kZAxis );
        // 箭头尖端坐标
        AcGePoint3d ap = pt + v * ( m_trunkLength + m_arrowLength );
        gripPoints.append( ap );
    }
    return Acad::eOk;
}
Acad::ErrorStatus WorkSurface::customTransformBy( const AcGeMatrix3d& xform )
{
    m_startPt.transformBy( xform );
    m_endPt.transformBy( xform );
    update();
    return Acad::eOk;
}
Acad::ErrorStatus WorkSurface::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉端点
    if ( osnapMode == AcDb::kOsModeEnd )
    {
        snapPoints.append( m_startPt );
        snapPoints.append( m_endPt );
    }
    return Acad::eOk;
}

Acad::ErrorStatus WorkSurface::customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        if ( idx == 0 )
        {
            m_startPt += offset;
            update();
        }
        if ( idx == 1 )
        {
            m_endPt += offset;
            update();
        }
        if( idx == 2 )
        {
            double angle = ( m_clockWise ? ( -PI / 2 ) : ( PI / 2 ) );
            AcGeVector3d v = m_endPt - m_startPt;
            v.normalize();
            v.rotateBy( angle, AcGeVector3d::kZAxis );
            // 计算offset在a1方向上的投影长度
            double L = offset.dotProduct( v );
            if( L < 0 && abs( L ) > ( m_trunkLength + m_arrowLength ) ) m_clockWise = !m_clockWise;
        }
    }
    return Acad::eOk;
}
void WorkSurface::update()
{
    caclStartPoint( m_leftStartPt, m_rightStartPt );
    caclEndPoint( m_leftEndPt, m_rightEndPt );
}
void WorkSurface::caclStartPoint( AcGePoint3d& startPt1, AcGePoint3d& startPt2 )
{
    AcGeVector3d v = m_endPt - m_startPt;
    v.normalize();
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    startPt1 = m_startPt + v * m_width * 0.5;
    v.rotateBy( PI, AcGeVector3d::kZAxis );
    startPt2 = m_startPt + v * m_width * 0.5;
}
void WorkSurface::caclEndPoint( AcGePoint3d& endPt1, AcGePoint3d& endPt2 )
{
    AcGeVector3d v = m_endPt - m_startPt;
    v.normalize();
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    endPt1 = m_endPt + v * m_width * 0.5;
    v.rotateBy( PI, AcGeVector3d::kZAxis );
    endPt2 = m_endPt + v * m_width * 0.5;
}
Acad::ErrorStatus WorkSurface::dwgOutFields( AcDbDwgFiler* pFiler ) const
{
    assertReadEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgOutFields"));
    Acad::ErrorStatus es = EdgeGE::dwgOutFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    if ( ( es = pFiler->writeUInt32 ( WorkSurface::kCurrentVersionNumber ) ) != Acad::eOk )
        return ( es ) ;
    // 保存始末点坐标
    pFiler->writeItem( m_leftStartPt );
    pFiler->writeItem( m_leftEndPt );
    pFiler->writeItem( m_rightStartPt );
    pFiler->writeItem( m_rightEndPt );
    pFiler->writeItem( m_width );
	pFiler->writeItem( m_ap );
    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus WorkSurface::dwgInFields( AcDbDwgFiler* pFiler )
{
    assertWriteEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgInFields"));
    Acad::ErrorStatus es = EdgeGE::dwgInFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    Adesk::UInt32 version = 0 ;
    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
        return ( es ) ;
    if ( version > WorkSurface::kCurrentVersionNumber )
        return ( Acad::eMakeMeProxy ) ;
    // 读取始末点坐标
    pFiler->readItem( &m_leftStartPt );
    pFiler->readItem( &m_leftEndPt );
    pFiler->readItem( &m_rightStartPt );
    pFiler->readItem( &m_rightEndPt );
    pFiler->readItem( &m_width );
	pFiler->readItem( &m_ap );
    return ( pFiler->filerStatus () ) ;
}

void WorkSurface::getDirPt( AcGePoint3d& dirPt )
{
	dirPt = m_ap;
}