#include "StdAfx.h"
#include "CircleText.h"
#include "MineGE/DrawTool.h"
Adesk::UInt32 CircleText::kCurrentVersionNumber = 1 ;
Adesk::UInt32 AddPower::kCurrentVersionNumber = 1 ;
Adesk::UInt32 LocalResis::kCurrentVersionNumber = 1 ;
Adesk::UInt32 HeatSource::kCurrentVersionNumber = 1 ;
ACRX_NO_CONS_DEFINE_MEMBERS ( CircleText,  DirGE )
ACRX_DXF_DEFINE_MEMBERS ( AddPower, CircleText,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          附加动力, VENTGEAPP )
ACRX_DXF_DEFINE_MEMBERS ( LocalResis, CircleText,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          局部阻力, VENTGEAPP )
ACRX_DXF_DEFINE_MEMBERS ( HeatSource, CircleText,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          热源, VENTGEAPP )

CircleText::CircleText ()
{
    m_radius = 6;
    m_type = _T( "" );
}
CircleText::CircleText( const AcGePoint3d& insertPt, double angle )
    : DirGE( insertPt, angle )
{
    m_radius = 6;
    m_type = _T( "" );
}
CircleText::~CircleText ()
{
}
Adesk::Boolean CircleText::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    if( v.x < 0 ) v.negate();
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    AcDb::LineWeight originLineWeight = mode->subEntityTraits().lineWeight();
    mode->subEntityTraits().setLineWeight( AcDb::kLnWt030 );
    DrawCircle( mode, m_insertPt, m_radius, false );
    mode->subEntityTraits().setLineWeight( originLineWeight );
    v.rotateBy( PI / 2 , AcGeVector3d::kZAxis );
    v.normalize();
    DrawMText( mode, m_insertPt, m_angle, m_type, 0.6 * m_radius );
    return Adesk::kTrue;
}
Acad::ErrorStatus CircleText::customTransformBy( const AcGeMatrix3d& xform )
{
    // 插入点变换
    //m_insertPt.transformBy( xform );
    //m_pt.transformBy( xform );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.transformBy( xform );
    //m_radius = v.length();
    // 1) 构造一个圆
    AcDbCircle circle( m_insertPt, AcGeVector3d::kZAxis, m_radius );
    // 2) 圆调用transformBy()方法进行变换
    circle.transformBy( xform );
    //3) 获取更新后的参数
    m_insertPt = circle.center();     // 获取变换后的圆心坐标
    m_radius = circle.radius(); // 获取变换后的圆半径
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis ); // 变换后的旋转角度
    return Acad::eOk;
}

Acad::ErrorStatus CircleText::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    if( osnapMode != AcDb::kOsModeCen )
        return Acad::eOk;
    Acad::ErrorStatus es = Acad::eOk;
    if( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return es;
}
Acad::ErrorStatus CircleText::customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    return Acad::eOk;
}
Acad::ErrorStatus CircleText::customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    int len = indices.length();
    for( int i = 0; i < len; i++ )
    {
        int idx = indices.at( i );
        if( idx == 0 )
        {
            // 当前夹点是圆心，移动图形
            m_insertPt += offset;       // 对圆心执行偏移变换
        }
        if( idx == 1 )
        {
            // 缩放圆
            // 1) 计算x轴的端点坐标
            AcGeVector3d v( AcGeVector3d::kXAxis );
            v.rotateBy( m_angle, AcGeVector3d::kZAxis );
            AcGePoint3d pt = m_insertPt + v * m_radius;
            // 2) 进行坐标偏移计算
            pt += offset;
            // 3) 计算新坐标与圆心之间的长度，并作为圆半径
            // 坐标相减，得到一个向量，然后得到向量长度
            m_radius = ( pt - m_insertPt ).length();
        }
    }
    return Acad::eOk;
}
AddPower::AddPower()
{
    m_type = _T( "ΔF" );
}
AddPower::AddPower( const AcGePoint3d& insertPt, double angle ): CircleText( insertPt, angle )
{
    m_type = _T( "ΔF" );
}
LocalResis::LocalResis()
{
    m_type = _T( "ΔH" );
}
LocalResis::LocalResis( const AcGePoint3d& insertPt, double angle ): CircleText( insertPt, angle )
{
    m_type = _T( "ΔH" );
}
HeatSource::HeatSource()
{
    m_type = _T( "H" );
}
HeatSource::HeatSource( const AcGePoint3d& insertPt, double angle )
{
    m_type = _T( "H" );
}