#pragma once
#include "MineGE/MineGE.h"
#include "dlimexp.h"
// 采空区(封闭图形)
class VENTGE_EXPORT_API Goaf : public MineGE
{
public:
    ACRX_DECLARE_MEMBERS( Goaf ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    Goaf();
    virtual ~Goaf();
    // 向采空区添加边界点
    void addPoint( const AcGePoint3d& pt );
    // 获取采空区多边形
    void getPolygon( AcGePoint3dArray& polygon ) const;
    // 分割采空区多边形的第pos条边
    void splitLine( int pos, const AcGePoint3d& pt );
    // 删除采空区多边形的一个顶点
    void removePoint( int pos );
    // 修改第k个边界点
    void setPoint( int pos, const AcGePoint3d& pt );
    void update();
    virtual Acad::ErrorStatus dwgOutFields ( AcDbDwgFiler* pFiler ) const;
    virtual Acad::ErrorStatus dwgInFields ( AcDbDwgFiler* pFiler );
    virtual void caclBackGroundMinPolygon( AcGePoint3dArray& pts );
protected:
    virtual Adesk::Boolean customWorldDraw( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
private:
    AcGePoint3dArray m_pts;
    double m_scale; // GRAVEL填充的比例
    double m_width; // 巷道的宽度
} ;
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( Goaf )
#endif
