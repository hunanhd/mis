#include "StdAfx.h"
#include "Gate.h"
#include "MineGE/Drawtool.h"
Adesk::UInt32 Gate::kCurrentVersionNumber = 1 ;
Adesk::UInt32 PermanentGate::kCurrentVersionNumber = 1 ;
Adesk::UInt32 TemporaryGate::kCurrentVersionNumber = 1 ;
Adesk::UInt32 DoubleGate::kCurrentVersionNumber = 1 ;
Adesk::UInt32 BalanceGate::kCurrentVersionNumber = 1 ;
ACRX_NO_CONS_DEFINE_MEMBERS( Gate, DirGE )
ACRX_DXF_DEFINE_MEMBERS (
    PermanentGate, Gate,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    永久风门, DEFGEAPP
)
ACRX_DXF_DEFINE_MEMBERS (
    TemporaryGate, Gate,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    临时风门, DEFGEAPP
)
ACRX_DXF_DEFINE_MEMBERS (
    DoubleGate, Gate,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    双向风门, DEFGEAPP
)
ACRX_DXF_DEFINE_MEMBERS (
    BalanceGate, Gate,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    平衡风门, DEFGEAPP
)
Gate::Gate() : DirGE()
{
    map( _T( "半径" ), &m_radius );
    map( _T( "长度" ), &m_length );
    map( _T( "半宽" ), &m_dTWidth );
    m_radius = 30;
    m_length = 50;
    m_dTWidth = 30; // 与双线巷道的宽度是相同的，双线巷道宽度调整，同时也应需要修改该宽度值
}
Gate::Gate( const AcGePoint3d& insertPt, double angle ): DirGE( insertPt, angle )
{
    map( _T( "半径" ), &m_radius );
    map( _T( "长度" ), &m_length );
    map( _T( "半宽" ), &m_dTWidth );
    m_radius = 30;
    m_length = 50;
    m_dTWidth = 30; // 与双线巷道的宽度是相同的，双线巷道宽度调整，同时也应需要修改该宽度值
}
void Gate::drawArc( AcGiWorldDraw* mode, const AcGePoint3d& insertPt, double angle, double gap, double radius, bool clockWise )
{
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( angle + PI, AcGeVector3d::kZAxis );
    AcGePoint3d pt = insertPt + v * gap; // 插入点位置偏移
    AcGeVector3d endVector( AcGeVector3d::kXAxis );
    if( clockWise )
    {
        endVector.rotateBy( angle + PI / 2, AcGeVector3d::kZAxis );
    }
    else
    {
        endVector.rotateBy( angle - PI / 2, AcGeVector3d::kZAxis );
    }
    mode->geometry().circularArc( pt, radius, AcGeVector3d::kZAxis, endVector, PI ); // 绘制一个半圆
}
void Gate::drawLine( AcGiWorldDraw* mode, const AcGePoint3d& insertPt, double angle, double gap, double offset, double length )
{
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( angle + PI, AcGeVector3d::kZAxis );
    AcGePoint3d pt = insertPt + v * gap; // 插入点位置偏移
    AcGeVector3d startVector( AcGeVector3d::kXAxis ), endVector( AcGeVector3d::kXAxis );
    // 注意：rotateBy函数同时会修改对应的this对象
    startVector.rotateBy( angle - PI / 2, AcGeVector3d::kZAxis );
    endVector.rotateBy( angle + PI / 2, AcGeVector3d::kZAxis );
    DrawLine( mode, pt + startVector * offset, pt + startVector * length );
    DrawLine( mode, pt + endVector * offset, pt + endVector * length );
}
Adesk::Boolean Gate::customWorldDraw ( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    drawArc( mode, m_insertPt, m_angle, m_gap, m_radius ); // 绘制半圆
    drawLine( mode, m_insertPt, m_angle, m_gap, m_offset, m_length ); // 绘制两侧的直线
    return Adesk::kTrue;
}
Acad::ErrorStatus Gate::customTransformBy( const AcGeMatrix3d& xform )
{
    m_insertPt.transformBy( xform ); // 变化插入点
    AcGeVector3d v( AcGeVector3d::kXAxis * m_radius );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.transformBy( xform );
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis ); // 变换后的旋转角度
    //m_radius = v.length(); // 变化后的半径
    /*AcGeVector3d startVector(AcGeVector3d::kXAxis*m_length);
    startVector.rotateBy(PI/2+m_angle, AcGeVector3d::kZAxis);
    startVector.transformBy(xform);*/
    //m_length = startVector.length(); // 变化后的长度
    return Acad::eOk;
}
//- Osnap points protocol
Acad::ErrorStatus Gate::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉1种类型的点：中点
    if( osnapMode != AcDb::kOsModeCen ) return Acad::eOk;
    if( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return Acad::eOk;
}
//- Grip points protocol
Acad::ErrorStatus Gate::customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 插入点作为夹点
    gripPoints.append( m_insertPt );
    // 圆弧的的一个顶点
    /*AcGeVector3d v(AcGeVector3d::kXAxis*(m_radius+m_gap));
    v.rotateBy(m_angle+PI, AcGeVector3d::kZAxis);
    gripPoints.append(m_insertPt+v);*/
    // 直线的2个端点
    /*AcGeVector3d startVector(AcGeVector3d::kXAxis*m_length),
    	         endVector(AcGeVector3d::kXAxis*m_length);
    startVector.rotateBy(m_angle-PI/2, AcGeVector3d::kZAxis);
    endVector.rotateBy(m_angle+PI/2, AcGeVector3d::kZAxis);
    gripPoints.append(m_insertPt + startVector);
    gripPoints.append(m_insertPt + endVector);*/
    return Acad::eOk;
}
Acad::ErrorStatus Gate::customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        // 插入点
        if ( idx == 0 ) m_insertPt += offset;
        // 圆弧顶点
        /*if (idx == 1)
        {*/
        // 计算圆弧半径
        //AcGeVector3d v(AcGeVector3d::kXAxis*m_radius);
        //v.rotateBy(m_angle+PI, AcGeVector3d::kZAxis);
        //v += offset; // 半径向量进行偏移
        //m_radius = v.length(); // 更新半径
        //m_angle = v.angleTo(AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis); // 变换后的旋转角度
        //}
        //if(idx == 2)
        //{
        //	AcGeVector3d startVector(AcGeVector3d::kXAxis*m_length);
        //	// 注意：rotateBy函数同时会修改对应的this对象
        //	startVector.rotateBy(m_angle-PI/2, AcGeVector3d::kZAxis);
        //	startVector += offset; // 端点向量进行偏移
        //	// 暂时不更新直线长度
        //	//m_length = startVector.length()*2; // 更新直线长度
        //	startVector.rotateBy(PI/2, AcGeVector3d::kZAxis); // 顺时针旋转90度
        //	m_angle = startVector.angleTo(AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis); // 变换后的旋转角度
        //}
        //if(idx == 3)
        //{
        //	AcGeVector3d endVector(AcGeVector3d::kXAxis*m_length);
        //	endVector.rotateBy(PI/2+m_angle, AcGeVector3d::kZAxis);
        //	endVector += offset; // 端点向量进行偏移
        //	// 暂时不更新直线长度
        //	//m_length = endVector.length()*2; // 更新直线长度
        //	endVector.rotateBy(-PI/2, AcGeVector3d::kZAxis); // 逆时针旋转90度
        //	m_angle = endVector.angleTo(AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis); // 变换后的旋转角度
        //}
    }
    return Acad::eOk;
}
static AcGePoint3d CaclPt( const AcGePoint3d& insertPt, const AcGeVector3d& v1, double width, const AcGeVector3d& v2, double height )
{
    return ( insertPt + v1 * width + v2 * height );
}
static double CaclWidth( double w, double dw )
{
    double width = w;
    if( w > dw / 2 )
    {
        width = sqrt( w * w - dw * dw / 4 );
    }
    return width;
}
void Gate::caclBackGroundMinPolygon( AcGePoint3dArray& pts )
{
    AcGeVector3d v1( AcGeVector3d::kXAxis ), v2( AcGeVector3d::kXAxis );
    v1.rotateBy( m_angle + PI / 2, AcGeVector3d::kZAxis );
    v2.rotateBy( m_angle + PI, AcGeVector3d::kZAxis );
    double width = CaclWidth( m_radius, m_dTWidth );
    pts.append( m_insertPt + v1 * m_length );
    pts.append( CaclPt( m_insertPt, v1, m_length, v2, width ) );
    v1.rotateBy( PI, AcGeVector3d::kZAxis );
    pts.append( CaclPt( m_insertPt, v1, m_length, v2, width ) );
    pts.append( m_insertPt + v1 * m_length );
}
PermanentGate::PermanentGate () : Gate ()
{
}
PermanentGate::PermanentGate( const AcGePoint3d& insertPt, double angle ) : Gate( insertPt, angle )
{
}
TemporaryGate::TemporaryGate () : Gate ()
{
    m_offset = 10;
}
TemporaryGate::TemporaryGate( const AcGePoint3d& insertPt, double angle ) : Gate( insertPt, angle )
{
    m_offset = 10;
}
Adesk::Boolean TemporaryGate::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    drawArc( mode, m_insertPt, m_angle, 0, m_radius );
    drawLine( mode, m_insertPt, m_angle, 0, m_offset, m_length );
    return Adesk::kTrue;
}
DoubleGate::DoubleGate () : Gate ()
{
    m_gap = 10;
}
DoubleGate::DoubleGate( const AcGePoint3d& insertPt, double angle ) : Gate( insertPt, angle )
{
    m_gap = 10;
}
void DoubleGate::caclBackGroundMinPolygon( AcGePoint3dArray& pts )
{
    AcGeVector3d v1( AcGeVector3d::kXAxis ), v2( AcGeVector3d::kXAxis );
    v1.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v2.rotateBy( m_angle + PI / 2, AcGeVector3d::kZAxis );
    double width = CaclWidth( m_radius, m_dTWidth ) + m_gap;
    pts.append( CaclPt( m_insertPt, v1, width, v2, m_length ) );
    v1.rotateBy( PI, AcGeVector3d::kZAxis );
    pts.append( CaclPt( m_insertPt, v1, width, v2, m_length ) );
    v2.rotateBy( PI, AcGeVector3d::kZAxis );
    pts.append( CaclPt( m_insertPt, v1, width, v2, m_length ) );
    v1.rotateBy( PI, AcGeVector3d::kZAxis );
    pts.append( CaclPt( m_insertPt, v1, width, v2, m_length ) );
}
Adesk::Boolean DoubleGate::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    drawArc( mode, m_insertPt, m_angle, m_gap, m_radius );
    drawLine( mode, m_insertPt, m_angle, m_gap, 0, m_length );
    drawArc( mode, m_insertPt, m_angle + PI, m_gap, m_radius );
    drawLine( mode, m_insertPt, m_angle + PI, m_gap, 0, m_length );
    return Adesk::kTrue;
}
BalanceGate::BalanceGate () : Gate ()
{
    m_gap = 50; // 圆半径40+偏移10
}
BalanceGate::BalanceGate( const AcGePoint3d& insertPt, double angle ) : Gate( insertPt, angle )
{
    m_gap = 50; // 圆半径40+偏移10
}
void BalanceGate::caclBackGroundMinPolygon( AcGePoint3dArray& pts )
{
}
Adesk::Boolean BalanceGate::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    // 1) 绘制2个抛物线,间距gap
    /*
    drawParabola(mode, m_insertPt, m_angle, m_gap, m_radius, false);
    drawLine(mode, m_insertPt, m_angle, m_gap, 0, m_length+m_radius);
    drawParabola(mode, m_insertPt, m_angle+PI, m_gap, m_radius, true);
    drawLine(mode, m_insertPt, m_angle+PI, m_gap, 0, m_length+m_radius);*/
    // 2) 绘制1个抛物线, 间距gap=0
    drawParabola( mode, m_insertPt, m_angle, 0, m_radius, false );
    drawLine( mode, m_insertPt, m_angle, 0, 0, m_length + m_radius );
    return Adesk::kTrue;
}
void BalanceGate::drawParabola( AcGiWorldDraw* mode, const AcGePoint3d& insertPt, double angle, double gap, double radius, bool clockWise /*= true*/ )
{
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle + PI / 2, AcGeVector3d::kZAxis );
    drawArc( mode, insertPt + v * radius, angle, gap, radius, clockWise );
    v.rotateBy( PI, AcGeVector3d::kZAxis );
    drawArc( mode, insertPt + v * radius, angle + PI, gap, radius, clockWise );
}