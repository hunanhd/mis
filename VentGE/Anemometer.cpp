#include "StdAfx.h"
#include "Anemometer.h"
#include "MineGE/DrawTool.h"
Adesk::UInt32 Anemometer::kCurrentVersionNumber = 1 ;

ACRX_DXF_DEFINE_MEMBERS ( Anemometer, DirGE,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          测风站, VENTGEAPP )
Anemometer::Anemometer()
{
    m_width = 30;
    m_length = 40;
    m_length0 = 15;
}
Anemometer::Anemometer( const AcGePoint3d& insertPt, double angle ): DirGE( insertPt, angle )
{
    m_width = 30;
    m_length = 40;
    m_length0 = 15;
}
Anemometer::~Anemometer()
{
}
Adesk::Boolean Anemometer::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled();
    // 计算点坐标
    AcGeVector3d v1( AcGeVector3d::kXAxis ), v2( AcGeVector3d::kXAxis );
    v1.rotateBy( m_angle + PI * 0.5, AcGeVector3d::kZAxis );
    v1.normalize();
    AcGePoint3d pt1 = m_insertPt + v1 * m_width * 0.5;
    v1.rotateBy( - PI * 0.5, AcGeVector3d::kZAxis );
    AcGePoint3d pt2 = pt1 + v1 * m_length * 0.5;
    v1.rotateBy( PI, AcGeVector3d::kZAxis );
    AcGePoint3d pt3 = pt2 + v1 * m_length;
    v1.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    AcGePoint3d pt4 = pt3 + v1 * m_width;
    v1.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    AcGePoint3d pt5 = pt4 + v1 * m_length;
    v1.rotateBy( PI / 4, AcGeVector3d::kZAxis );
    AcGePoint3d pt7 = pt2 + v1 * m_length0;
    AcGePoint3d pt9 = pt4 - v1 * m_length0;
    v1.rotateBy( - PI * 0.5, AcGeVector3d::kZAxis );
    AcGePoint3d pt6 = pt5 + v1 * m_length0;
    AcGePoint3d pt8 = pt3 - v1 * m_length0;
    DrawPolyLine( mode, pt3, pt2, 1.5 );
    DrawPolyLine( mode, pt4, pt5, 1.5 );
    DrawLine( mode, pt7, pt2 );
    DrawLine( mode, pt3, pt8 );
    DrawLine( mode, pt4, pt9 );
    DrawLine( mode, pt5, pt6 );
    return Acad::eOk;
}
Acad::ErrorStatus Anemometer::customTransformBy( const AcGeMatrix3d& xform )
{
    m_insertPt.transformBy( xform );
    // 构造一个倾角向量
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis ); // 得到原有的倾角向量
    // 执行变换
    v.transformBy( xform );
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    return Acad::eOk;
}
Acad::ErrorStatus Anemometer::customGetOsnapPoints( AcDb::OsnapMode osnapMode, Adesk::GsMarker gsSelectionMark, const AcGePoint3d& pickPoint, const AcGePoint3d& lastPoint, const AcGeMatrix3d& viewXform, AcGePoint3dArray& snapPoints, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉1种类型的点：插入点
    Acad::ErrorStatus es = Acad::eOk;
    if ( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return es;
}
Acad::ErrorStatus Anemometer::customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    return Acad::eOk;
}
Acad::ErrorStatus Anemometer::customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        // 始节点
        if ( idx == 0 ) m_insertPt += offset;
    }
    return Acad::eOk;
}