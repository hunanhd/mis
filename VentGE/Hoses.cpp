#include "StdAfx.h"
#include "Hoses.h"
#include "MineGE/Drawtool.h"
Adesk::UInt32 Hoses::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS ( Hoses, MineGE,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          胶管, VENTGEAPP )
Hoses::Hoses () : MineGE ()
{
}
Hoses::~Hoses ()
{
}
Acad::ErrorStatus Hoses::dwgOutFields ( AcDbDwgFiler* pFiler ) const
{
    assertReadEnabled () ;
    //----- Save parent class information first.
    Acad::ErrorStatus es = MineGE::dwgOutFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    //----- Object version number needs to be saved first
    if ( ( es = pFiler->writeUInt32 ( Hoses::kCurrentVersionNumber ) ) != Acad::eOk )
        return ( es ) ;
    //----- Output params
    //.....
    int len = m_pts.length();
    pFiler->writeInt32( ( long )len );
    for( int i = 0; i < len; i++ )
    {
        pFiler->writePoint3d( m_pts[i] );
    }
    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus Hoses::dwgInFields ( AcDbDwgFiler* pFiler )
{
    assertWriteEnabled () ;
    //----- Read parent class information first.
    Acad::ErrorStatus es = MineGE::dwgInFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    //----- Object version number needs to be read first
    Adesk::UInt32 version = 0 ;
    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
        return ( es ) ;
    if ( version > Hoses::kCurrentVersionNumber )
        return ( Acad::eMakeMeProxy ) ;
    m_pts.removeAll();
    long len = 0;
    pFiler->readInt32( &len );
    for( int i = 0; i < ( int )len; i++ )
    {
        AcGePoint3d pt;
        pFiler->readPoint3d( &pt );
        m_pts.append( pt );
    }
    return ( pFiler->filerStatus () ) ;
}
void Hoses::addControlPoint( const AcGePoint3d& pt )
{
    if( !m_pts.contains( pt ) ) m_pts.append( pt );
}
AcGePoint3dArray Hoses::getControlPoint()
{
    return m_pts;
}
void Hoses::setControlPoint( AcGePoint3dArray pts )
{
    m_pts = pts;
}
Adesk::Boolean Hoses::customWorldDraw ( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    //int len = m_pts.length();
    //if( len < 2 ) return Adesk::kTrue;
    int oringinColor = mode->subEntityTraits().color();
    mode->subEntityTraits().setColor( 42 );
    //AcDb::LineWeight originLineWeight = mode->subEntityTraits().lineWeight();
    //mode->subEntityTraits().setLineWeight(AcDb::LineWeight::kLnWt025);
    DrawSpline( mode, m_pts );
    //mode->subEntityTraits().setLineWeight(originLineWeight);
    mode->subEntityTraits().setColor( oringinColor );
    return Adesk::kTrue;
}
Acad::ErrorStatus Hoses::customTransformBy( const AcGeMatrix3d& xform )
{
    int len = m_pts.length();
    for( int i = 0; i < len; i++ )
    {
        m_pts[i].transformBy( xform );
    }
    return Acad::eOk;
}
Acad::ErrorStatus Hoses::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉1种类型的点：端点
    if( osnapMode != AcDb::kOsMaskEnd ) return Acad::eOk;
    if( osnapMode == AcDb::kOsMaskEnd )
    {
        int len = m_pts.length();
        for( int i = 0; i < len; i++ )
        {
            snapPoints.append( m_pts[i] );
        }
    }
    return Acad::eOk;
}
//- Grip points protocol
Acad::ErrorStatus Hoses::customGetGripPoints (
    AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    int len = m_pts.length();
    for( int i = 0; i < len; i++ )
    {
        gripPoints.append( m_pts[i] );
    }
    return Acad::eOk;
}
Acad::ErrorStatus Hoses::customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        m_pts[idx] += offset;
    }
    return Acad::eOk;
}
