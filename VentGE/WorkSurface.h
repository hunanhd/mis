#pragma once
#include "MineGE/EdgeGE.h"
#include "dlimexp.h"
class VENTGE_EXPORT_API WorkSurface : public EdgeGE
{
public:
    ACRX_DECLARE_MEMBERS( WorkSurface ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    WorkSurface () ;
    WorkSurface ( const AcGePoint3d& startPt, const AcGePoint3d& endPt ) ;
    virtual ~WorkSurface () ;
    // 重载EdgeGE类虚函数(用于实现节点闭合)
public:
    virtual void reverse();
    virtual void dealWithStartPointBoundary( const AcGeRay3d& boundaryLine );
    virtual void dealWithEndPointBoundary( const AcGeRay3d& boundaryLine );
    virtual void extendByLength( double length );
    virtual void update();
    // 重载AcDbObject的虚函数(实现dwg读写)
public:
    virtual Acad::ErrorStatus dwgOutFields ( AcDbDwgFiler* pFiler ) const;
    virtual Acad::ErrorStatus dwgInFields ( AcDbDwgFiler* pFiler );
protected:
    // 具体的绘制实现
    virtual Adesk::Boolean customWorldDraw( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset ) ;
    // 计算图形的最小多边形
    virtual void minPolygon( AcGePoint3dArray& pts );
private:
    void drawArrow( AcGiWorldDraw* mode, bool clockWise );
    void drawText( AcGiWorldDraw* mode );
    void caclStartPoint( AcGePoint3d& startPt, AcGePoint3d& endPt );
    void caclEndPoint( AcGePoint3d& startPt, AcGePoint3d& endPt );
    double m_width; // 默认为30
    AcGePoint3d m_leftStartPt, m_leftEndPt;
    AcGePoint3d m_rightStartPt, m_rightEndPt;
    // 默认为AcDb::kLnWt060
    //AcDb::LineWeight m_oneSideLineWeight;
    bool m_clockWise;
    double m_trunkWidth;   // 主干半宽
    double m_trunkLength;  // 主干长度
    double m_arrowWidth;   // 箭头半宽多出的部分
    double m_arrowLength;  // 箭头长度
	AcGePoint3d m_ap;		//箭头尖端点

public:
	void getDirPt(AcGePoint3d& dirPt);
} ;
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( WorkSurface )
#endif
