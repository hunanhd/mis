#pragma once
#include "MineGE/EdgeGE.h"
#include "dlimexp.h"
// 巷道图元
class VENTGE_EXPORT_API Tunnel : public EdgeGE
{
public:
    ACRX_DECLARE_MEMBERS( Tunnel ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    Tunnel();
    Tunnel( const AcGePoint3d& startPt, const AcGePoint3d& endPt );
    virtual ~Tunnel();
    // 重载EdgeGE类虚函数(用于实现节点闭合)
public:
    virtual void reverse();
    virtual void dealWithStartPointBoundary( const AcGeRay3d& boundaryLine );
    virtual void dealWithEndPointBoundary( const AcGeRay3d& boundaryLine );
    virtual void extendByLength( double length );
    virtual void update();
    // 重载AcDbObject的虚函数(实现dwg读写)
public:
    virtual Acad::ErrorStatus dwgOutFields ( AcDbDwgFiler* pFiler ) const;
    virtual Acad::ErrorStatus dwgInFields ( AcDbDwgFiler* pFiler );
    // 重载MineGE虚函数(图形交互操作)
protected:
    // 具体的绘制实现
    virtual Adesk::Boolean customWorldDraw( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset ) ;
    // 计算图形的最小多边形
    virtual void minPolygon( AcGePoint3dArray& pts );
    // 重载Data基类的虚函数(当几何参数被修改后,其它的参数是否需要同步更新?)
protected:
    // bool set( const CString& field, ... )函数调用后触发该消息
    virtual void onAfterSetData( const CString& field, const CString& value );
private:
    void caclStartPoint( AcGePoint3d& startPt, AcGePoint3d& endPt );
    void caclEndPoint( AcGePoint3d& startPt, AcGePoint3d& endPt );
private:
    double m_width; // 默认为30
    AcGePoint3d m_leftStartPt, m_leftEndPt;
    AcGePoint3d m_rightStartPt, m_rightEndPt;
} ;
#ifdef VENTGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( Tunnel )
#endif
