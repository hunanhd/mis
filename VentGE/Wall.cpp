#include "StdAfx.h"
#include "Wall.h"
#include "MineGE/DrawTool.h"
Adesk::UInt32 Wall::kCurrentVersionNumber = 1 ;
ACRX_NO_CONS_DEFINE_MEMBERS ( Wall, DirGE )
ACRX_DXF_DEFINE_MEMBERS ( PermanentWall, Wall,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          永久密闭, VENTGEAPP
                        )
ACRX_DXF_DEFINE_MEMBERS ( TemporaryWall, Wall,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          临时密闭, VENTGEAPP
                        )
ACRX_DXF_DEFINE_MEMBERS ( Firewall, Wall,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          防火密闭, VENTGEAPP
                        )

Wall::Wall () : DirGE ()
{
    m_height = 80;
    m_width = 20;
    needFill = true;
}
Wall::Wall( const AcGePoint3d& insertPt, double angle ): DirGE( insertPt, angle )
{
    m_height = 80;
    m_width = 20;
    needFill = true;
}
static AcGePoint3d CaclPt( const AcGePoint3d& insertPt, const AcGeVector3d& v1, double width, const AcGeVector3d& v2, double height )
{
    return ( insertPt + v1 * width / 2 + v2 * height / 2 );
}
void Wall::caclBackGroundMinPolygon( AcGePoint3dArray& pts )
{
    AcGeVector3d v1( AcGeVector3d::kXAxis ), v2( AcGeVector3d::kXAxis );
    v1.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v2.rotateBy( m_angle + PI / 2, AcGeVector3d::kZAxis );
    pts.append( CaclPt( m_insertPt, v1, m_width, v2, m_height ) );
    v1.rotateBy( PI, AcGeVector3d::kZAxis );
    pts.append( CaclPt( m_insertPt, v1, m_width, v2, m_height ) );
    v2.rotateBy( PI, AcGeVector3d::kZAxis );
    pts.append( CaclPt( m_insertPt, v1, m_width, v2, m_height ) );
    v1.rotateBy( PI, AcGeVector3d::kZAxis );
    pts.append( CaclPt( m_insertPt, v1, m_width, v2, m_height ) );
}
Adesk::Boolean Wall::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    int colorIndex = mode->subEntityTraits().color();
    mode->subEntityTraits().setColor( 12 ); //棕色
    // 绘制风桥
    // 绘制矩形块，并填充
    DrawRect( mode, m_insertPt, m_angle, m_width, m_height, needFill );
    mode->subEntityTraits().setColor( colorIndex );
    return Adesk::kTrue;
}
Acad::ErrorStatus Wall::customTransformBy( const AcGeMatrix3d& xform )
{
    m_insertPt.transformBy( xform ); // 变化插入点
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.transformBy( xform );
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis ); // 变换后的旋转角度
    return Acad::eOk;
}
Acad::ErrorStatus Wall::customGetOsnapPoints( AcDb::OsnapMode osnapMode, Adesk::GsMarker gsSelectionMark, const AcGePoint3d& pickPoint, const AcGePoint3d& lastPoint, const AcGeMatrix3d& viewXform, AcGePoint3dArray& snapPoints, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉1种类型的点：中点
    if( osnapMode != AcDb::kOsModeCen ) return Acad::eOk;
    if( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return Acad::eOk;
}
Acad::ErrorStatus Wall::customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 插入点作为夹点
    gripPoints.append( m_insertPt );
    return Acad::eOk;
}
Acad::ErrorStatus Wall::customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        // 插入点
        if ( idx == 0 ) m_insertPt += offset;
    }
    return Acad::eOk;
}
PermanentWall::PermanentWall()
{
    needFill = false;
}
PermanentWall::PermanentWall( const AcGePoint3d& insertPt, double angle ): Wall( insertPt, angle )
{
    needFill = false;
}

TemporaryWall::TemporaryWall()
{
}
TemporaryWall::TemporaryWall( const AcGePoint3d& insertPt, double angle ): Wall( insertPt, angle )
{
}
Adesk::Boolean TemporaryWall::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    int colorIndex = mode->subEntityTraits().color();
    mode->subEntityTraits().setColor( 12 ); //棕色
    DrawLine( mode, m_insertPt, PI * 0.5 + m_angle, m_height * 0.5 );
    DrawLine( mode, m_insertPt, m_angle - PI * 0.5, m_height * 0.5 );
    mode->subEntityTraits().setColor( colorIndex );
    return Adesk::kTrue;
}
Firewall::Firewall()
{
}
Firewall::Firewall( const AcGePoint3d& insertPt, double angle ): Wall( insertPt, angle )
{
}
