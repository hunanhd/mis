#include "StdAfx.h"
#include "PitotTube.h"
#include "MineGE/DrawTool.h"
Adesk::UInt32 PitotTube::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS ( PitotTube, DirGE,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          皮托管, VENTGEAPP )

PitotTube::PitotTube ()
{
    m_length = 8;
}
PitotTube::PitotTube( const AcGePoint3d& insertPt, double angle )
    : DirGE( insertPt, angle )
{
    m_length = 8;
}
PitotTube::~PitotTube ()
{
}
Adesk::Boolean PitotTube::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    DrawLine( mode, m_insertPt, m_insertPt - v * 0.5 * m_length );
    v.rotateBy( -PI * 0.5, AcGeVector3d::kZAxis );
    AcGePoint3d pt0 = m_insertPt + v * m_length;
    DrawLine( mode, m_insertPt, pt0 );
    v.rotateBy( PI * 0.25, AcGeVector3d::kZAxis );
    m_pt1 = pt0 + v * m_length / 4;
    v.rotateBy( -PI * 0.5, AcGeVector3d::kZAxis );
    m_pt2 = pt0 + v * m_length / 4;
    DrawLine( mode, pt0, m_pt1 );
    DrawLine( mode, pt0, m_pt2 );
    v.rotateBy( -PI * 0.25, AcGeVector3d::kZAxis );
    DrawMText( mode, m_pt2 + v * m_length / 6, m_angle, _T( "+" ), m_length * 0.1 );
    DrawMText( mode, m_pt1 - v * m_length / 6, m_angle, _T( "-" ), m_length * 0.1 );
    return Adesk::kTrue;
}
Acad::ErrorStatus PitotTube::customTransformBy( const AcGeMatrix3d& xform )
{
    // 插入点变换
    m_insertPt.transformBy( xform );
    return Acad::eOk;
}

Acad::ErrorStatus PitotTube::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    if ( osnapMode == AcDb::kOsModeEnd )
    {
        //snapPoints.append( m_insertPt );
        snapPoints.append( m_pt1 );
        snapPoints.append( m_pt2 );
    }
    return Acad::eOk;
}
Acad::ErrorStatus PitotTube::customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    return Acad::eOk;
}
Acad::ErrorStatus PitotTube::customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    int len = indices.length();
    for( int i = 0; i < len; i++ )
    {
        int idx = indices.at( i );
        if( idx == 0 )
        {
            // 当前夹点是圆心，移动图形
            m_insertPt += offset;       // 对圆心执行偏移变换
        }
    }
    return Acad::eOk;
}
Acad::ErrorStatus PitotTube::dwgOutFields( AcDbDwgFiler* pFiler ) const
{
    assertReadEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgOutFields"));
    Acad::ErrorStatus es = DirGE::dwgOutFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    if ( ( es = pFiler->writeUInt32 ( PitotTube::kCurrentVersionNumber ) ) != Acad::eOk )
        return ( es ) ;
    pFiler->writeItem( m_pt1 );
    pFiler->writeItem( m_pt2 );
    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus PitotTube::dwgInFields( AcDbDwgFiler* pFiler )
{
    assertWriteEnabled () ;
    Acad::ErrorStatus es = DirGE::dwgInFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    Adesk::UInt32 version = 0 ;
    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
        return ( es ) ;
    if ( version > PitotTube::kCurrentVersionNumber )
        return ( Acad::eMakeMeProxy ) ;
    pFiler->readItem( &m_pt1 );
    pFiler->readItem( &m_pt2 );
    return ( pFiler->filerStatus () ) ;
}