#include "StdAfx.h"
#include "AirLeakage.h"
#include "MineGE/Drawtool.h"
Adesk::UInt32 AirLeakage::kCurrentVersionNumber = 1 ;
Adesk::UInt32 AirSource::kCurrentVersionNumber = 1 ;
Adesk::UInt32 AirIn::kCurrentVersionNumber = 1 ;
ACRX_NO_CONS_DEFINE_MEMBERS( AirLeakage, DirGE )
ACRX_DXF_DEFINE_MEMBERS (
    AirSource, AirLeakage,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    漏风源, VENTGEAPP
)
ACRX_DXF_DEFINE_MEMBERS (
    AirIn, AirLeakage,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    漏风汇, VENTGEAPP
)
AirLeakage::AirLeakage() : DirGE()
{
    m_radius = 15;
    m_isSource = true;
}
AirLeakage::AirLeakage( const AcGePoint3d& insertPt, double angle ): DirGE( insertPt, angle )
{
    m_radius = 15;
    m_isSource = true;
}
static void DrawX( AcGiWorldDraw* mode, const AcGePoint3d& insertPt, double angle, double radius )
{
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( angle, AcGeVector3d::kZAxis );
    if( v.x < 0 ) v.negate();
    // 注意：rotateBy函数同时会修改对应的this对象
    v.rotateBy( -PI / 4, AcGeVector3d::kZAxis );
    DrawLine( mode, insertPt + v * radius, insertPt - v * radius );
    v.rotateBy( PI / 2, AcGeVector3d::kZAxis );
    DrawLine( mode, insertPt + v * radius, insertPt - v * radius );
}
Adesk::Boolean AirLeakage::customWorldDraw ( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    DrawCircle( mode, m_insertPt, m_radius, false );
    if( m_isSource )
    {
        DrawX( mode, m_insertPt, m_angle, m_radius );
    }
    else
    {
        DrawCircle( mode, m_insertPt, m_radius * 0.2, true );
    }
    return Adesk::kTrue;
}
Acad::ErrorStatus AirLeakage::customTransformBy( const AcGeMatrix3d& xform )
{
    m_insertPt.transformBy( xform ); // 变化插入点
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.transformBy( xform );
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis ); // 变换后的旋转角度
    //m_radius = v.length(); // 变化后的半径
    /*AcGeVector3d startVector(AcGeVector3d::kXAxis*m_length);
    startVector.rotateBy(PI/2+m_angle, AcGeVector3d::kZAxis);
    startVector.transformBy(xform);*/
    //m_length = startVector.length(); // 变化后的长度
    return Acad::eOk;
}
//- Osnap points protocol
Acad::ErrorStatus AirLeakage::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉1种类型的点：中点
    if( osnapMode != AcDb::kOsModeCen ) return Acad::eOk;
    if( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return Acad::eOk;
}
//- Grip points protocol
Acad::ErrorStatus AirLeakage::customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 插入点作为夹点
    gripPoints.append( m_insertPt );
    return Acad::eOk;
}
Acad::ErrorStatus AirLeakage::customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        // 插入点
        if ( idx == 0 ) m_insertPt += offset;
    }
    return Acad::eOk;
}
AirSource::AirSource () : AirLeakage ()
{
}
AirSource::AirSource( const AcGePoint3d& insertPt, double angle ) : AirLeakage( insertPt, angle )
{
}
AirIn::AirIn () : AirLeakage ()
{
    m_isSource = false;
}
AirIn::AirIn( const AcGePoint3d& insertPt, double angle ) : AirLeakage( insertPt, angle )
{
    m_isSource = false;
}
