#pragma once

#define DIM_STYLE_NAME1 _T("标注样式1")
#define DIM_STYLE_NAME2 _T("标注样式2")
#define DIM_STYLE_NAME3 _T("标注样式3")

// 图元的扩展数据组--绘制效果参数
//#define DRAW_PARAMS_XDATA_GROUP _T("绘制效果附加参数")

// 词典名称--属性数据字段
#define PROPERTY_DATA_FIELD_DICT _T("属性数据字段")

// 词典名称--属性数据字段信息
#define PROPERTY_DATA_FIELD_INFO_DICT _T("属性数据字段信息")

// 数据对象key(存储在扩展词典中)
//#define DATA_OBJECT_EXT_DICT_KEY _T("图元数据对象key")

#define STRING_LIST_DICT _T("字符串列表")
#define INT_LIST_DICT _T("整数列表")
//#define OBJECT_LIST_DICT _T("数据对象列表")
//#define OBJECT_LIST_DATA_DICT _T("数据对象详细列表")
//#define GLOBAL_SINGLE_INFO_DICT _T("全局数据")
#define PARAM_DICT _T("功能参数")

// 块定义文件名
#define BLOCK_DWG_NAME _T("block.dwg")

// 浮点数精度
#define NUM_TOLERANCE 4

// 分组名 与 字段名 之间的连接符号
#define SEPARATOR _T(".")

// 默认分组名
#define DEFAULT_GROUP _T("常用")

// 几何参数分组名
#define PARAM_GROUP _T("几何参数")

// 默认数据库名称
#define DEFAULT_DB_NAME _T("sample.db")

// 是否启动属性数据分组功能
#define USE_GROUP 1