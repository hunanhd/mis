#-*- coding:utf-8 -*-
import numpy as np
from algo.vno import *
from algo.vno_data import *
import json

def __vno_result_to_json(vnet, key_name='edges'):
    result = {}
    edges = []
    dg = vnet.graph()
    for e in dg.es:
        u, v = dg.vs[e.source], dg.vs[e.target]
        x = {
            'id' : e['id'],
            's' : u['id'],
            't' : v['id'],
            'r' : e['r'],
            'q' : e['q']
        }
        edges.append(x)
    result[key_name] = edges
    return result

def __edges_to_json(vnet, _edges, key_name='edges'):
    dg = vnet.graph()
    result = {}
    result[key_name] = [dg.es[i]['id'] for i in _edges]
    return result

def __comps_to_json(vnet, _comps, key_name='components'):
    dg = vnet.graph()
    result = {}
    result[key_name] = dict(enumerate(_comps))
    return result

def run_vno(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    # 3. 构造通风网络对象
    vnet = VentNetwork()    
    if not build_network(graph_datas, vnet.graph()):
        print '构造通风网络失败!!!'
        return {'ret': False, 'error':'构造通风网络失败'}

    print '构造通风网络成功!!!'
    # 4. 添加虚拟源汇,将网络变成单一源汇通风网络
    vnet.addVirtualST()
    # print '虚拟源点:',vnet.vSource(), '虚拟汇点:',vnet.vTarget()
    # 5. 通风网络解算
    if not vno(vnet):
        print '网络解算失败!'
        # 11. 删除虚拟源汇
        vnet.delVirtualST()
        # 返回错误信息
        return {'ret': False, 'error':'网络解算失败'}

    # 6. 打印通风网络解算结果
    print_network(vnet, msg='网络解算结果')
    # 11. 删除虚拟源汇
    vnet.delVirtualST()    
    # 计算结果转换为json
    result = __vno_result_to_json(vnet, 'edges')
    result['ret'] = True
    return json.dumps(result)

def source_edges(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    # 3. 构造通风网络对象
    vnet = VentNetwork()    
    if not build_network(graph_datas, vnet.graph()):
        print '构造通风网络失败'
        return {'ret': False, 'error':'构造通风网络失败'}
    else:
        # 查找进风井
        edges = vnet.sourceEdges()
        if len(edges) == 0:            
            print '通风网络中不存在进风井!'
            return {'ret': False, 'error':'通风网络中不存在进风井'}
        else:
            # 6. 打印通风网络解算结果
            print '通风网络中的有%d个进风井' % (len(edges))     
            result = __edges_to_json(vnet, edges, 'source_edges')
            result['ret'] = True
            return json.dumps(result)

def sink_edges(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    ret = True
    # 3. 构造通风网络对象
    vnet = VentNetwork()    
    if not build_network(graph_datas, vnet.graph()):
        print '构造通风网络失败!!!'
        return {'ret': False, 'error':'构造通风网络失败'}
    else:
        # 查找进风井
        edges = vnet.sinkEdges()
        if len(edges) == 0:
            print '通风网络中不存在回风井!'
            return {'ret': False, 'error':'通风网络中不存在回风井'}
        else:
            # 6. 打印通风网络解算结果
            print '通风网络中的有%d个回风井' % (len(edges))
            result = __edges_to_json(vnet, edges, 'sink_edges')
            result['ret'] = True
            return json.dumps(result)

def is_connected(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    ret = True
    # 3. 构造通风网络对象
    vnet = VentNetwork()    
    if not build_network(graph_datas, vnet.graph()):
        print '构造通风网络失败!!!'
        return False
    else:
        # 判断连通性
        return vnet.isConnected()

def is_dag(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    ret = True
    # 3. 构造通风网络对象
    vnet = VentNetwork()    
    if not build_network(graph_datas, vnet.graph()):
        print '构造通风网络失败!!!'
        return False
    else:
        # 判断是否有向无环图
        return vnet.isDag()

def has_cycles(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    ret = True
    # 3. 构造通风网络对象
    vnet = VentNetwork()    
    if not build_network(graph_datas, vnet.graph()):
        print '构造通风网络失败!!!'
        return False
    else:
        # 判断由于单向回路
        return vnet.hasCycles()

def CC(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    # 3. 构造通风网络对象
    vnet = VentNetwork()    
    if not build_network(graph_datas, vnet.graph()):
        print '构造通风网络失败!!!'
        return {'ret': False, 'error':'构造通风网络失败'}
    else:
        # 查找连通块
        comps = vnet.CC()
        if len(comps) == 0:
            return {'ret': False, 'error':'通风网络是一个无效的图!'}
        else:
            result = __comps_to_json(vnet, comps, 'components')
            result['ret'] = True
            return json.dumps(result)

def SCC(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    ret = True
    # 3. 构造通风网络对象
    vnet = VentNetwork()    
    if not build_network(graph_datas, vnet.graph()):
        print '构造通风网络失败!!!'
        return {'ret': False, 'error':'构造通风网络失败'}
    else:
        # 判断单向回路(强联通块)
        comps = vnet.SCC()
        result = __comps_to_json(vnet, comps, 'loops')
        result['ret'] = True
        return json.dumps(result)

def negative_edges(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    ret = True
    # 3. 构造通风网络对象
    vnet = VentNetwork()
    # 注:需要读取风量
    if not build_network(graph_datas, vnet.graph(), hasQ=True):
        print '构造通风网络失败!!!'
        return {'ret': False, 'error':'构造通风网络失败'}
    else:
        # 查找负风量分支
        edges = vnet.negativeEdges()
        print '通风网络中的有%d个负风量分支' % (len(edges))      
        # 返回计算结果
        result = __edges_to_json(vnet, edges, 'negative_edges')
        result['ret'] = ret
        return json.dumps(result)

def fixQ_edges(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    ret = True
    # 3. 构造通风网络对象
    vnet = VentNetwork()    
    if not build_network(graph_datas, vnet.graph()):
        print '构造通风网络失败!'
        return {'ret': False, 'error':'构造通风网络失败'}
    else:
        # 查找进风井
        edges = vnet.fixQEdges()
        print '通风网络中的有%d个固定风量分支' % (len(edges))    
        # 返回计算结果
        result = __edges_to_json(vnet, edges, 'fixQ_edges')
        result['ret'] = ret
        return json.dumps(result)
        
if __name__=="__main__":
    pass