#coding:utf-8

import os
# import threading
from multiprocessing import Process

import rpc_server

# 调用windows命令taskkill杀死程序
def kill_exe(exe):
    command = 'taskkill /F /IM %s' % exe
    os.system(command)

# 启动一个线程,运行rpc服务器
def run_rpc_server1():
    p = Process(target = rpc_server.main, args=())
    p.daemon = True
    p.start()
    p.join()

if __name__ =='__main__':
    # 启动一个线程,运行rpc服务器
    run_rpc_server1()

    # 启动主界面
    # MainWindow.run()

    #杀死所有的python进程
    # kill_exe('python.exe')
