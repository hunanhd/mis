# -*- coding:utf-8 -*-

from vno import VnoService
from vno.ttypes import *
import ThriftServerHelper
import VnoServiceHandler

class __VnoServiceHandler(object):
    def __init__(self):
        pass
    def Test(self, n1, n2):
        print 'add(%d,%d)=%d' % (n1, n2, n1+n2)
        return n1 + n2
    def RunVno(self, graph_datas):
        return VnoServiceHandler.run_vno(graph_datas)
    def GetSourceEdges(self, graph_datas):
        return VnoServiceHandler.source_edges(graph_datas)
    def GetSinkEdges(self, graph_datas):
        return VnoServiceHandler.sink_edges(graph_datas)
    def IsConnected(self, graph_datas):
        return VnoServiceHandler.is_connected(graph_datas)
    def IsDag(self, graph_datas):
        return VnoServiceHandler.is_dag(graph_datas)
    def HasCycles(self, graph_datas):
        return VnoServiceHandler.has_cycles(graph_datas)
    def CC(self, graph_datas):
        return VnoServiceHandler.CC(graph_datas)
    def SCC(self, graph_datas):
        return VnoServiceHandler.SCC(graph_datas)        
    def GetNegativeEdges(self, graph_datas):
        return VnoServiceHandler.negative_edges(graph_datas)
    def GetFixQEdges(self, graph_datas):
        return VnoServiceHandler.fixQ_edges(graph_datas)        
# 创建服务器
def create_server(host, port):
    # 构造rpc消息处理接口
    handler = __VnoServiceHandler()
    processor = VnoService.Processor(handler)
    # 创建thrift rpc服务器
    server = ThriftServerHelper.make_rpc_server(processor, host=host, port=port)
    return handler, server