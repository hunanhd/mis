var searchData=
[
  ['readblob',['ReadBlob',['../class_kompex_1_1_s_q_lite_blob.html#a8aeaa787eb43b47bc7706abb392d2a34',1,'Kompex::SQLiteBlob']]],
  ['redirection',['Redirection',['../class_kompex_1_1_redirection.html',1,'Kompex']]],
  ['redirection',['Redirection',['../class_kompex_1_1_redirection.html#a6769ba01c75b7cdc22ce93ab0dc8dbf6',1,'Kompex::Redirection::Redirection()'],['../class_kompex_1_1_redirection.html#a4934b51ab74ef2dc1bbb8cc1954c6298',1,'Kompex::Redirection::Redirection(const Redirection &amp;r)']]],
  ['releasememory',['ReleaseMemory',['../class_kompex_1_1_s_q_lite_database.html#aa3d72b4cda1c60e214ea32a7bf5f7d9b',1,'Kompex::SQLiteDatabase::ReleaseMemory(int bytesOfMemory) const '],['../class_kompex_1_1_s_q_lite_database.html#a7e353a3ed82b6d02e57d3d070188c562',1,'Kompex::SQLiteDatabase::ReleaseMemory()']]],
  ['reset',['Reset',['../class_kompex_1_1_s_q_lite_statement.html#a927db41f0bc32393604b3696fc1088cd',1,'Kompex::SQLiteStatement']]],
  ['rollbacktransaction',['RollbackTransaction',['../class_kompex_1_1_s_q_lite_statement.html#ac15a94da5d86d3ef1c2d6d50be7aebba',1,'Kompex::SQLiteStatement']]]
];
