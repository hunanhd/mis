var searchData=
[
  ['open',['Open',['../class_kompex_1_1_s_q_lite_database.html#a2ebca3f60f2b00f2932d606ca0ae722d',1,'Kompex::SQLiteDatabase::Open(const char *filename, int flags, const char *zVfs)'],['../class_kompex_1_1_s_q_lite_database.html#ae3e7815afa1cebf7635a0bef6a9eb6dd',1,'Kompex::SQLiteDatabase::Open(const std::string &amp;filename, int flags, const char *zVfs)'],['../class_kompex_1_1_s_q_lite_database.html#a9a674ad37c24d0af960b453fe8e31cf1',1,'Kompex::SQLiteDatabase::Open(const wchar_t *filename)']]],
  ['openblob',['OpenBlob',['../class_kompex_1_1_s_q_lite_blob.html#ade06809ea257f54d60a75de8cf9d53eb',1,'Kompex::SQLiteBlob']]],
  ['operator_3d',['operator=',['../class_kompex_1_1_redirection.html#a269aa12f32e6a46e77bd4e0106bf335a',1,'Kompex::Redirection::operator=()'],['../class_kompex_1_1_cerr_redirection.html#aaf0c1faae412992d8187c645d612951d',1,'Kompex::CerrRedirection::operator=()'],['../class_kompex_1_1_cout_redirection.html#afcf6bd0d5eadafa0ec444e4c716032ee',1,'Kompex::CoutRedirection::operator=()']]]
];
