var searchData=
[
  ['utf16',['UTF16',['../class_kompex_1_1_s_q_lite_database.html#a4f928b783b9b6ed4b80e1a4703164119a3b9b9029eb28951bec66333958479323',1,'Kompex::SQLiteDatabase::UTF16()'],['../namespace_kompex.html#a875244fbbeac460a73bd844bd919b9aca2cd61b64b58bed10b2f1c1a7b3a60a1d',1,'Kompex::UTF16()']]],
  ['utf16_5faligned',['UTF16_ALIGNED',['../namespace_kompex.html#a875244fbbeac460a73bd844bd919b9aca5d715224b90aad9ae50e4daa137eaedb',1,'Kompex']]],
  ['utf16be',['UTF16BE',['../namespace_kompex.html#a875244fbbeac460a73bd844bd919b9aca819c0f73e2690640aef7d991dd88d301',1,'Kompex']]],
  ['utf16le',['UTF16LE',['../namespace_kompex.html#a875244fbbeac460a73bd844bd919b9aca4d61bb186ee4d4e57eb6ec09f95fc1ad',1,'Kompex']]],
  ['utf8',['UTF8',['../class_kompex_1_1_s_q_lite_database.html#a4f928b783b9b6ed4b80e1a4703164119ab15d8fd77594914a6d1fc7cec60d5c50',1,'Kompex::SQLiteDatabase::UTF8()'],['../namespace_kompex.html#a875244fbbeac460a73bd844bd919b9acacf558580187adde838038b756aaf58a0',1,'Kompex::UTF8()']]]
];
