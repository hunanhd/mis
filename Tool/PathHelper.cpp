#include "StdAfx.h"
#include "PathHelper.h"
// 获取当前模块的路径
CString PathHelper::GetModuleDir( HINSTANCE hInstance )
{
    TCHAR szPath[_MAX_PATH];
    GetModuleFileName( hInstance, szPath, _MAX_PATH );
    //TCHAR drive[_MAX_DRIVE];
    //TCHAR dir[_MAX_DIR];
    //_tsplitpath( szPath, drive, dir, NULL, NULL );
    //TCHAR path[_MAX_PATH];
    //_tmakepath( path, drive, dir, NULL, NULL );
    //return CString( path );
    CString strPath( szPath );
    for ( int nPos = ( int )strPath.GetLength() - 1; nPos >= 0; --nPos )
    {
        TCHAR cChar = strPath[nPos];
        if ( _T( '\\' ) == cChar || _T( '/' ) == cChar )
            return strPath.Mid( 0, nPos + 1 );
    }
    return strPath;
}
// 生成路径
CString PathHelper::BuildPath( const CString& dir, const CString& fileName )
{
    int n1 = dir.GetLength();
    int n2 = fileName.GetLength();
    CString path;
    if( dir.GetAt( n1 - 1 ) == _T( '\\' ) )
    {
        path += dir.Left( n1 - 1 );
    }
    else
    {
        path += dir;
    }
    path += _T( "\\" );
    if( fileName.GetAt( 0 ) == _T( '\\' ) )
    {
        path += fileName.Right( n2 - 1 );
    }
    else
    {
        path += fileName;
    }
    return path;
}
// 获取应用程序执行路径
CString PathHelper::GetExeDir()
{
    TCHAR szPath[MAX_PATH] = {0};
    ::GetModuleFileName( NULL, szPath, MAX_PATH );
    CString strPath( szPath );
    for ( int nPos = ( int )strPath.GetLength() - 1; nPos >= 0; --nPos )
    {
        TCHAR cChar = strPath[nPos];
        if ( _T( '\\' ) == cChar || _T( '/' ) == cChar )
            return strPath.Mid( 0, nPos + 1 );
    }
    return strPath;
}
// 获取应用程序当前目录
CString PathHelper::GetCurDir()
{
    TCHAR szCurDir[MAX_PATH] = {0};
    ::GetCurrentDirectory( MAX_PATH, szCurDir );
    size_t dwLen = _tcslen( szCurDir );
    if ( dwLen <= 0 )
        return _T( "" );
    TCHAR cLastChar = szCurDir[dwLen - 1];
    if ( cLastChar != _T( '\\' ) && cLastChar != _T( '/' ) )
        _tcscat( szCurDir, _T( "\\" ) );
    return szCurDir;
}
// 获取当前系统的临时文件夹的路径
CString PathHelper::GetTempPath()
{
    TCHAR szTempPath[MAX_PATH] = {0};
    ::GetTempPath( MAX_PATH, szTempPath );
    size_t dwLen = _tcslen( szTempPath );
    if ( dwLen <= 0 )
        return _T( "" );
    TCHAR cLastChar = szTempPath[dwLen - 1];
    if ( cLastChar != _T( '\\' ) && cLastChar != _T( '/' ) )
        _tcscat( szTempPath, _T( "\\" ) );
    return szTempPath;
}
// 获取当前系统的临时文件夹的路径下的唯一命名的临时文件名(全路径)
CString PathHelper::GetTempFileName( LPCTSTR lpszFileName )
{
    return GetRandomFileName( GetTempPath(), lpszFileName );
}
// 获取随机文件名(全路径)
CString PathHelper::GetRandomFileName( LPCTSTR lpszPath, LPCTSTR lpszFileName )
{
    CString strPath, strFileName, strExtFileName, strFullPath;
    TCHAR szBuf[MAX_PATH] = {0};
    if ( !IsDirectoryExist( lpszPath ) )
        strPath = GetExeDir();
    else
        strPath = lpszPath;
    strFileName = GetFileNameWithoutExtension( lpszFileName );
    strExtFileName = GetExtension( lpszFileName );
    for ( int i = 2; i < 10000; i++ )
    {
        if ( strExtFileName.IsEmpty() )
        {
            strFullPath = strPath;
            strFullPath += strFileName;
            wsprintf( szBuf, _T( "%d" ), i );
            strFullPath += szBuf;
        }
        else
        {
            strFullPath = strPath;
            strFullPath += strFileName;
            wsprintf( szBuf, _T( "%d." ), i );
            strFullPath += szBuf;
            strFullPath += strExtFileName;
        }
        if ( !IsFileExist( strFullPath ) )
            return strFullPath;
    }
    return _T( "" );
}
// 生成临时文件名
CString PathHelper::GetTimeStampFileName( LPCTSTR prefix )
{
    COleDateTime datetime = COleDateTime::GetCurrentTime();
    CString str;
    str.Format( _T( "%s_%s" ),  prefix, datetime.Format( _T( "%Y%m%d%H%M%S" ) ) );
    return str;
}
BOOL PathHelper::IsFileInDir( LPCTSTR lpszDir, LPCTSTR lpszPath )
{
    CString dir( lpszDir );
    CString path( lpszPath );
    path = path.Mid( 0, dir.GetLength() );
    return ( path.CompareNoCase( dir ) == 0 );
}
BOOL PathHelper::IsFileInTempPath( LPCTSTR lpszPath )
{
    return PathHelper::IsFileInDir( PathHelper::GetTempPath(), lpszPath );
}
// 检测指定路径是否目录
BOOL PathHelper::IsDirectory( LPCTSTR lpszPath )
{
    if ( NULL == lpszPath || NULL == *lpszPath )
        return FALSE;
    DWORD dwAttr = ::GetFileAttributes( lpszPath );
    return ( ( ( dwAttr != 0xFFFFFFFF ) && ( dwAttr & FILE_ATTRIBUTE_DIRECTORY ) ) ? TRUE : FALSE );
}
// 检测指定文件是否存在
BOOL PathHelper::IsFileExist( LPCTSTR lpszFileName )
{
    if ( NULL == lpszFileName || NULL == *lpszFileName )
        return FALSE;
    DWORD dwAttr = ::GetFileAttributes( lpszFileName );
    return ( ( ( dwAttr != 0xFFFFFFFF ) && ( !( dwAttr & FILE_ATTRIBUTE_DIRECTORY ) ) ) ? TRUE : FALSE );
}
// 检测指定目录是否存在
BOOL PathHelper::IsDirectoryExist( LPCTSTR lpszPath )
{
    if ( NULL == lpszPath || NULL == *lpszPath )
        return FALSE;
    DWORD dwAttr = ::GetFileAttributes( lpszPath );
    return ( ( ( dwAttr != 0xFFFFFFFF ) && ( dwAttr & FILE_ATTRIBUTE_DIRECTORY ) ) ? TRUE : FALSE );
}
BOOL PathHelper::CreateDirectory( LPCTSTR lpPathName, LPSECURITY_ATTRIBUTES lpSecurityAttributes )
{
    TCHAR cPath[MAX_PATH] = {0};
    TCHAR cTmpPath[MAX_PATH] = {0};
    TCHAR* lpPos = NULL;
    TCHAR cTmp = _T( '\0' );
    if ( NULL == lpPathName || NULL == *lpPathName )
        return FALSE;
    _tcsncpy( cPath, lpPathName, MAX_PATH );
    for ( int i = 0; i < ( int )_tcslen( cPath ); i++ )
    {
        if ( _T( '\\' ) == cPath[i] )
            cPath[i] = _T( '/' );
    }
    lpPos = _tcschr( cPath, _T( '/' ) );
    while ( lpPos != NULL )
    {
        if ( lpPos == cPath )
        {
            lpPos++;
        }
        else
        {
            cTmp = *lpPos;
            *lpPos = _T( '\0' );
            _tcsncpy( cTmpPath, cPath, MAX_PATH );
            ::CreateDirectory( cTmpPath, lpSecurityAttributes );
            *lpPos = cTmp;
            lpPos++;
        }
        lpPos = _tcschr( lpPos, _T( '/' ) );
    }
    return TRUE;
}
// 获取指定路径的根目录信息
CString PathHelper::GetPathRoot( LPCTSTR lpszPath )
{
    if ( NULL == lpszPath || NULL == *lpszPath )
        return _T( "" );
    CString strPath( lpszPath );
    for( int i = 0; i < strPath.GetLength(); i++ )
    {
        if ( _T( '\\' ) == strPath[i] )
            strPath.SetAt( i, _T( '/' ) );
    }
    int nPos = strPath.Find( _T( '/' ) );
    if ( nPos != -1 )
        strPath = strPath.Mid( 0, nPos + 1 );
    return strPath;
}
// 返回指定路径字符串的目录信息
CString PathHelper::GetDirectory( LPCTSTR lpszPath )
{
    if ( NULL == lpszPath || NULL == *lpszPath )
        return _T( "" );
    CString strPath( lpszPath );
    for ( int nPos = ( int )strPath.GetLength() - 1; nPos >= 0; --nPos )
    {
        TCHAR cChar = strPath[nPos];
        if ( _T( '\\' ) == cChar || _T( '/' ) == cChar )
            return strPath.Mid( 0, nPos + 1 );
    }
    return strPath;
}
// 返回指定路径字符串的文件名和扩展名
CString PathHelper::GetFileName( LPCTSTR lpszPath )
{
    if ( NULL == lpszPath || NULL == *lpszPath )
        return _T( "" );
    CString strPath( lpszPath );
    for ( int nPos = ( int )strPath.GetLength() - 1; nPos >= 0; --nPos )
    {
        TCHAR cChar = strPath[nPos];
        if ( _T( '\\' ) == cChar || _T( '/' ) == cChar )
            return strPath.Mid( nPos + 1 );
    }
    return strPath;
}
// 返回不具有扩展名的路径字符串的文件名
CString PathHelper::GetFileNameWithoutExtension( LPCTSTR lpszPath )
{
    if ( NULL == lpszPath || NULL == *lpszPath )
        return _T( "" );
    CString strPath( lpszPath );
    for ( int nPos = ( int )strPath.GetLength() - 1; nPos >= 0; --nPos )
    {
        TCHAR cChar = strPath[nPos];
        if ( _T( '\\' ) == cChar || _T( '/' ) == cChar )
        {
            strPath = strPath.Mid( nPos + 1 );
            break;
        }
    }
    int nPos = strPath.ReverseFind( _T( '.' ) );
    if ( nPos != -1 )
        strPath = strPath.Mid( 0, nPos );
    return strPath;
}
// 返回指定的路径字符串的扩展名
CString PathHelper::GetExtension( LPCTSTR lpszPath )
{
    if ( NULL == lpszPath || NULL == *lpszPath )
        return _T( "" );
    CString strPath( lpszPath );
    int nPos = strPath.ReverseFind( _T( '.' ) );
    if ( nPos != -1 )
        return strPath.Mid( nPos + 1 );
    else
        return _T( "" );
}
// 根据指定的相对路径获取绝对路径
CString PathHelper::GetFullPath( LPCTSTR lpszPath )
{
    if ( NULL == lpszPath || NULL == *lpszPath )
        return _T( "" );
    CString strPath( lpszPath );
    TCHAR cFirstChar = strPath.GetAt( 0 );
    TCHAR cSecondChar = strPath.GetAt( 1 );
    if ( cFirstChar == _T( '\\' ) || cFirstChar == _T( '/' ) )
    {
        CString strCurDir = GetExeDir();
        CString strRootPath = GetPathRoot( strCurDir );
        return strRootPath + strPath;
    }
    else if ( ::IsCharAlpha( cFirstChar ) && cSecondChar == _T( ':' ) )
    {
        return strPath;
    }
    else
    {
        CString strCurDir = GetExeDir();
        return strCurDir + strPath;
    }
}
