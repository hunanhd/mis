#pragma once
// win32相关的封装函数
#include "EncodeHelper.h"
#include "PathHelper.h"
#include "Win32Utils.h"
#include "Singleton.h"
#include "FlagHelper.h"
#include "StringHelper.h"
#include "ThreadHelper.h"
#include "Registry.h"
#include "CADHelper.h"
#include "MyRandom.h"
#include "Clock.h"
#include "sigslot.h"
#define APP_DIR PathHelper::GetExeDir()
#define APP_FILE_PATH(file) PathHelper::BuildPath(PathHelper::GetExeDir(),file)
// 强制给const修饰的变量赋值,仅用于特殊情况下!!!
template<typename T>
void assign_const( const T& var, const T& value )
{
    *( const_cast<T*>( &var ) ) = value;
}
