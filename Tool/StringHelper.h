#pragma once
#include "dlimexp.h"
// ARX工具类
class /*ARXHELPER_API*/ StringHelper
{
public:
    // 判断是否空字符串
    static bool IsEmptyString( const CString& str );
    // string-->int, double之间的转换
    static bool StringToDouble( const CString& value, double& v );
    static bool StringToInt( const CString& value, int& v );
    static void DoubleToString( double v, CString& value );
    static void IntToString( int v, CString& value );
    static bool StringToBool( const CString& str );
    static void StringToDataTime( const CString& str, COleDateTime& dt );
    //判断字符串是一个数字（整数或浮点数）
    static bool IsNum( const CString& str );
    // 判断字符串是否是一个合法的整数串
    // 格式[+/-][0-9]
    static bool IsInteger( LPCTSTR pSrc );
    // 判断字符串是否是一个合法的浮点数串
    // 格式[+/-][0-9][.][0-9]
    static bool IsNumeric( LPCTSTR pSrc );
    // 生成GUID字符串
    static bool NewGUID( CString& strGUID );
	// 时间戳(格式: 年/月/日 时:分:秒)
	static CString TimeStamp();
    //分隔字符串
    // tokens表示分隔符(可以有多个分隔符)
    static void SplitCString( const CString& str, const CString& tokens, CStringArray& values );
    //合并字符串
    static CString JoinCString( const CStringArray& values, int start = 0, const CString& delim = _T( "_" ) );
	//小数点前面的0补全,并且除掉左右多余的0
	static void DealSpPoints( CString& value );
};
