#include "stdafx.h"
#include "ThreadHelper.h"
#include <tlhelp32.h>
#include <vector>
#include <assert.h>
void ThreadHelper::FindProcessIdByName( const CString& name, std::vector<DWORD>& pids )
{
    HANDLE hProcessSnap;
    //HANDLE hProcess;
    PROCESSENTRY32 pe32;
    //DWORD dwPriorityClass;
    // Take a snapshot of all processes in the system.
    hProcessSnap = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
    if( hProcessSnap == INVALID_HANDLE_VALUE )
    {
        return ;
    }
    // Set the size of the structure before using it.
    pe32.dwSize = sizeof( PROCESSENTRY32 );
    // Retrieve information about the first process,
    // and exit if unsuccessful
    if( !Process32First( hProcessSnap, &pe32 ) )
    {
        CloseHandle( hProcessSnap );    // Must clean up the
        //   snapshot object!
        return ;
    }
    DWORD pid = -1;
    // Now walk the snapshot of processes, and
    // display information about each process in turn
    do
    {
        if( name.CompareNoCase( pe32.szExeFile ) == 0 )
        {
            pids.push_back( pe32.th32ProcessID );
        }
    }
    while( Process32Next( hProcessSnap, &pe32 ) );
    CloseHandle( hProcessSnap );
    //return pid;
}
void ThreadHelper::KillProcess( const CString& name )
{
    std::vector<DWORD> pids;
    FindProcessIdByName( name, pids );
    for( int i = 0; i < pids.size(); i++ )
    {
        HANDLE hProcess = OpenProcess( PROCESS_ALL_ACCESS, FALSE, pids[i] );
        if( hProcess != NULL )
        {
            // 4代表什么意思???
            TerminateProcess( hProcess, 4 );
            CloseHandle( hProcess );
        }
    }
}
int ThreadHelper::ProcessNum( const CString& name )
{
    std::vector<DWORD> pids;
    FindProcessIdByName( name, pids );
    return ( int )pids.size();
}
bool ThreadHelper::IsProcessActive( const CString& name )
{
    std::vector<DWORD> pids;
    FindProcessIdByName( name, pids );
    return !pids.empty();
}
bool ThreadHelper::IsProcessActive2( HANDLE hProcess )
{
    DWORD exitCode;
    GetExitCodeProcess( hProcess, &exitCode );
    return ( exitCode == STILL_ACTIVE );
}
bool ThreadHelper::RunProecess( const CString& exePath, const CString& cmdLine, const CString& cwdPath, HANDLE& hProcess, HANDLE& hThread, bool bShow )
{
    PROCESS_INFORMATION pi;
    STARTUPINFO si;
    memset( &si, 0, sizeof( si ) );
    si.cb = sizeof( si );
    si.wShowWindow = bShow ? SW_SHOW : SW_HIDE;
    si.dwFlags = STARTF_USESHOWWINDOW;
    // 命令行第1个参数前必须要有空格!!!
    CString newCmdLine = _T( " " ) + cmdLine;
    BOOL ret = CreateProcess( ( LPCTSTR )exePath, ( LPTSTR )( LPCTSTR )newCmdLine, NULL, FALSE, NULL, NULL, NULL, ( LPCTSTR )cwdPath, &si, &pi );
    if( ret )
    {
        //WaitForSingleObject(pi.hProcess, INFINITE);
        //CloseHandle(pi.hThread);
        //CloseHandle(pi.hProcess);
        // 返回进程和线程句柄
        hProcess = pi.hProcess;
        hThread = pi.hThread;
    }
    return ( ret == TRUE );
}
void ThreadHelper::MsgWaitForThread( HANDLE hThread )
{
    // http://www.cnblogs.com/Sunwayking/articles/1976980.html
    while( TRUE )
    {
        //wait for m_hThread to be over，and wait for
        //QS_ALLINPUT（Any message is in the queue)
        DWORD dwRet = MsgWaitForMultipleObjects( 1, &hThread, FALSE, INFINITE, QS_ALLINPUT );
        if( WAIT_OBJECT_0 + 1 == dwRet )
        {
            //get the message from Queue
            //and dispatch it to specific window
            MSG msg;
            PeekMessage( &msg, NULL, 0, 0, PM_REMOVE );
            DispatchMessage( &msg );
            continue;
        }
        else
        {
            break;
        }
    }
}
typedef struct
{
    HWND hwnd;
    ThreadHelper::WindowNameCmpFunc cmp;
    CString caption;
} WndNameInfo;
typedef struct
{
    HWND hwnd;
    ThreadHelper::WndCmpFunc cmp;
    DWORD dwProcessId;
} WndProcInfo;
BOOL CALLBACK EnumWindowsByProcId( HWND hwnd, LPARAM lParam )
{
    WndProcInfo* pInfo = ( WndProcInfo* )lParam;
    // 比较进程id
    DWORD dwProcessId;
    ::GetWindowThreadProcessId( hwnd, &dwProcessId );
    /*CString msg;
    msg.Format(_T("进程id:%ld 句柄:%ld"), dwProcessId, hwnd);
    AfxMessageBox(msg);*/
    //注意：当查找到了，应该返回FALSE中止枚举下去
    if( pInfo->dwProcessId != dwProcessId ) return TRUE;
    //判断进程上的窗口是否符合我们的要求
    if( pInfo->cmp( hwnd ) )
    {
        pInfo->hwnd = hwnd;
        return FALSE;
    }
    return TRUE;
}
//http://www.cnblogs.com/wdhust/archive/2011/04/11/2012004.html
//查找cad进程
BOOL CALLBACK EnumWindowsByName( HWND hwnd, LPARAM lParam )
{
    WndNameInfo* pInfo = ( WndNameInfo* )lParam;
    // 比较窗口标题
    const int MAX_LEN = 256;
    TCHAR caption[MAX_LEN];
    assert( hwnd != NULL );
    ::GetWindowText( hwnd, caption, MAX_LEN );
    if( pInfo->cmp( caption, pInfo->caption ) )
    {
        pInfo->hwnd = hwnd;
        //注意：当查找到了，应该返回FALSE中止枚举下去
        return FALSE;
    }
    return TRUE;
}
HWND ThreadHelper::GetHwndByWindowName( const CString& name, ThreadHelper::WindowNameCmpFunc cmp )
{
    WndNameInfo info = {NULL, cmp, name};
    EnumWindows( EnumWindowsByName, ( LPARAM )&info );
    return info.hwnd;
}
HWND ThreadHelper::GetHwndByProcId( DWORD procId, WndCmpFunc cmp )
{
    WndProcInfo info = {NULL, cmp, procId};
    EnumWindows( EnumWindowsByProcId, ( LPARAM )&info );
    return info.hwnd;
}