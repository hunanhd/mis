#include "StdAfx.h"
#include "FlagHelper.h"
void FlagHelper::SetFlag( unsigned int& flags, unsigned int f )
{
    flags = flags | f;
}
void FlagHelper::DelFlag( unsigned int& flags, unsigned int f )
{
    flags = flags & ( ~f );
}
void FlagHelper::ToggleFlag( unsigned int& flags, unsigned int f )
{
    flags = flags ^ f;
}
bool FlagHelper::GetFlag( unsigned int flags, unsigned int f )
{
    if( flags & f )
    {
        return true;
    }
    else
    {
        return false;
    }
}
