#ifndef FLAG_HELPER_H
#define FLAG_HELPER_H
#include "dlimexp.h"
class /*ARXHELPER_API*/ FlagHelper
{
public:
    static void SetFlag( unsigned int& flags, unsigned int f );
    static void DelFlag( unsigned int& flags, unsigned int f );
    static void ToggleFlag( unsigned int& flags, unsigned int f );
    static bool GetFlag( unsigned int flags, unsigned int f );
};
#endif // FLAG_HELPER_H
