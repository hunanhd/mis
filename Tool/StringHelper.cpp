#include "StdAfx.h"
#include "StringHelper.h"
#include <string>
bool StringHelper::IsEmptyString( const CString& str )
{
    return ( str.GetLength() == 0 );
}
// 也可以使用下面的2个方法进行转换
//double d = _tstof(strText);
//int nItem = _ttoi(strText);
bool StringHelper::StringToDouble( const CString& value, double& v )
{
    v = _tstof( value );
    return true;
    //if( value.GetLength() == 0 )
    //{
    //    v = 0;
    //    return true;
    //}
    //else
    //{
    //    return ( RTNORM == acdbDisToF( value, 2, &v ) );
    //}
}
bool StringHelper::StringToInt( const CString& value, int& v )
{
    v = _ttoi( value );
    return true;
    //double vv;
    //if( !StringToDouble( value, vv ) ) return false;
    //v = ( int )vv;
    //return true;
}
void StringHelper::DoubleToString( double v, CString& value )
{
    value.Format( _T( "%f" ), v );
    //ACHAR fmtval[50];
    //if( RTNORM == acdbRToS( v, 2, -1, fmtval ) ) value = fmtval;
}
void StringHelper::IntToString( int v, CString& value )
{
    value.Format( _T( "%d" ), v );
}
bool StringHelper::IsNum( const CString& str )
{
    int n = str.GetLength();
    for( int i = 0; i < n; i++ )
        if ( ( str[i] < '0' || str[i] > '9' ) && str[i] != '.' )
            return false;
    return true;
}
// 将字符串转换成bool类型
// 在COleVariant中，bool类型使用short表示(boolValue---VT_BOOL)
// -1表示true，0表示false
bool StringHelper::StringToBool( const CString& str )
{
    if( str.GetLength() == 0 ) return false;
    return ( _ttoi( str ) != 0 );
}
void StringHelper::StringToDataTime( const CString& str, COleDateTime& dt )
{
    if( ( str.GetLength() == 0 ) || !dt.ParseDateTime( str ) )
    {
        dt = COleDateTime::GetCurrentTime();
    }
}
// 判断字符串是否是一个合法的整数串
// 格式[+/-][0-9]
bool StringHelper::IsInteger( LPCTSTR pSrc )
{
    if( *pSrc == _T( '+' ) || *pSrc == _T( '-' ) ) pSrc++;
    for( ; *pSrc != _T( '\0' ) && _istdigit( *pSrc ); pSrc++ );
    return ( *pSrc == _T( '\0' ) );
}
// 判断字符串是否是一个合法的浮点数串
// 格式[+/-][0-9][.][0-9]
bool StringHelper::IsNumeric( LPCTSTR pSrc )
{
    if( *pSrc == _T( '+' ) || *pSrc == _T( '-' ) ) pSrc++;
    for( bool bp = false; _istdigit( *pSrc ) || ( *pSrc == _T( '.' ) && bp == false ); )
    {
        if( *pSrc++ == _T( '.' ) ) bp = true;
    }
    return ( *pSrc == _T( '\0' ) );
}
// GUID字符串格式
#define GUID_FORMAT _T("{%08lX-%04X-%04x-%02X%02X-%02X%02X%02X%02X%02X%02X}")
bool StringHelper::NewGUID( CString& strGUID )
{
    GUID m_guid;
    bool ret = ( S_OK == ::CoCreateGuid( &m_guid ) );
    if( ret )
    {
        strGUID.Format( GUID_FORMAT,
                        m_guid.Data1, m_guid.Data2, m_guid.Data3,
                        m_guid.Data4[0], m_guid.Data4[1], m_guid.Data4[2],
                        m_guid.Data4[3], m_guid.Data4[4], m_guid.Data4[5],
                        m_guid.Data4[6], m_guid.Data4[7] );
    }
    return ret;
}
//分隔字符串
void StringHelper::SplitCString( const CString& str, const CString& tokens, CStringArray& values )
{
    int nTokenPos = 0;
    CString strToken = str.Tokenize( tokens, nTokenPos );
    while ( !strToken.IsEmpty() )
    {
        values.Add( strToken );
        strToken = str.Tokenize( tokens, nTokenPos );
    }
}
CString StringHelper::JoinCString( const CStringArray& values, int start, const CString& delim )
{
    int n = ( int )values.GetCount();
    if( start < 0 || start >= n ) return _T( "" );
    CString str;
    str.Format( _T( "%s" ), values[start] );
    for( int i = start + 1; i < n; i++ )
    {
        str.AppendFormat( _T( "%s%s" ), delim, values[i] );
    }
    return str;
}
void StringHelper::DealSpPoints( CString& value )
{
	//小数点前面的0补全,并且除掉左右多余的0
	if( value.Find( _T( '.' ) ) == -1 ) return;
	CString strValue;
	strValue.Format( _T( "%.8f" ), _tstof( value ) );
	value = strValue;
	value.Replace( _T( "0" ), _T( " " ) );	//替换0为空格
	value.Trim();	//裁剪
	value.Replace( _T( " " ), _T( "0" ) );
	if( value[0] == _T( '.' ) ) value.Insert( 0, _T( "0" ) );
	int lenth = value.GetLength();
	if( 0 == lenth )
	{
		return;
	}
	if( value[lenth - 1] == _T( '.' ) )
	{
		value.Replace( _T( "." ), _T( " " ) );
		value.Trim();	//裁剪
	}
}
static std::string CTimeStamp( void )
{
	time_t aclock;
	time( &aclock );                 // Get time in seconds
	struct tm* local_time = localtime( &aclock );  // Convert time to struct tm form
	char str_time[100];
	//char* str_time = asctime( local_time );
	//str_time[24] = 0;
	strftime(str_time, sizeof(str_time), "%Y/%m/%d %H:%M:%S", local_time);
	return str_time;
}
static CString OleDateTimeStamp( void )
{
	COleDateTime dt = COleDateTime::GetCurrentTime();
	return dt.Format( _T( "%Y/%m/%d %H:%M:%S" ) );
}
CString StringHelper::TimeStamp()
{
	return OleDateTimeStamp();
}
