﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Windows;

using AcadApplication = Autodesk.AutoCAD.ApplicationServices.Application;
using AcadDocument = Autodesk.AutoCAD.ApplicationServices.Document;
using AcadWindows = Autodesk.AutoCAD.Windows;

// 所有c#封装类都使用了M前缀
using arx;

[assembly:
    ExtensionApplication(typeof(MisVno.MyInitialization))
]
[assembly:
    CommandClass(typeof(MisVno.MyCommands))
]

namespace MisVno
{
    public class MyInitialization : Autodesk.AutoCAD.Runtime.IExtensionApplication
    {
        public void Initialize()
        {
        }
        public void Terminate()
        {
        }
    }
    public class MyCommands
    {
        // thrift测试命令
        [CommandMethod("ThriftTest")]
        public static void ThriftTest()
        {
            RpcClientHelper.CallVnoService(new VnoDelegate(VnoClientHelper.Test));
        }
        // 通风网络解算
        [CommandMethod("ArxVno")]
        public static void ArxVno()
        {
            RpcClientHelper.CallVnoService(new VnoDelegate(VnoClientHelper.RunVno));
        }
        // 查找进风井
        [CommandMethod("ArxSourceEdges")]
        public static void ArxSourceEdges()
        {
            RpcClientHelper.CallVnoService(new VnoDelegate(VnoClientHelper.GetSourceEdges));
        }
        // 查找回风井
        [CommandMethod("ArxSinkEdges")]
        public static void ArxSinkEdges()
        {
            RpcClientHelper.CallVnoService(new VnoDelegate(VnoClientHelper.GetSinkEdges));
        }
        // 查找构筑物分支
        [CommandMethod("ArxGateEdges")]
        public static void ArxGateEdges()
        {
            VnoClientHelper.GetGateEdges();
        }
        // 查找风机分支
        [CommandMethod("ArxFanEdges")]
        public static void ArxFanEdges()
        {
            VnoClientHelper.GetFanEdges();
        }
        // 判断连通性
        [CommandMethod("ArxIsConnected")]
        public static void ArxIsConnected()
        {
            RpcClientHelper.CallVnoService(new VnoDelegate(VnoClientHelper.TestIsConnected));
        }
        // 是否有向无环图
        [CommandMethod("ArxIsDag")]
        public static void ArxIsDag()
        {
            RpcClientHelper.CallVnoService(new VnoDelegate(VnoClientHelper.TestIsDag));
        }
        // 是否有单向回路
        [CommandMethod("ArxHasCycles")]
        public static void ArxHasCycles()
        {
            RpcClientHelper.CallVnoService(new VnoDelegate(VnoClientHelper.TestHasCycles));
        }
        // 查找连通块
        [CommandMethod("ArxCC")]
        public static void ArxCC()
        {
            RpcClientHelper.CallVnoService(new VnoDelegate(VnoClientHelper.GetConnectedComponents));
        }
        // 查找单向回路
        [CommandMethod("ArxSCC")]
        public static void ArxSCC()
        {
            RpcClientHelper.CallVnoService(new VnoDelegate(VnoClientHelper.GetSingleLoops));
        }
        // 查找负风量分支
        [CommandMethod("ArxNegativeEdges")]
        public static void ArxNegativeEdges()
        {
            //不使用rpc版本
            //VnoClientHelper.GetNegativeEdges();
            //使用rpc版本
            RpcClientHelper.CallVnoService(new VnoDelegate(VnoClientHelper.GetNegativeEdgesWithRpc));
        }
        // 查找固定风量分支
        [CommandMethod("ArxFixQEdges")]
        public static void ArxFixQEdges()
        {
            //不使用rpc版本
            //VnoClientHelper.GetFixQEdges();
            //使用rpc版本
            RpcClientHelper.CallVnoService(new VnoDelegate(VnoClientHelper.GetFixQEdgesWithRpc));
        }
    }
}
