﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Windows;

using AcadApplication = Autodesk.AutoCAD.ApplicationServices.Application;
using AcadDocument = Autodesk.AutoCAD.ApplicationServices.Document;
using AcadWindows = Autodesk.AutoCAD.Windows;

using Newtonsoft.Json.Linq;

namespace MisVno
{
    class VentNetworkHelper
    {
        public VentNetworkHelper()
        {
        }
        public static JObject BuildJson(bool hasQ)
        {
            JObject json = new JObject();
            // 查找所有的分支和节点
            ObjectIdCollection edgeIds = new ObjectIdCollection();
            VentSystemelper.FindEdgeGEs(ref edgeIds);
            //ObjectIdCollection nodeIds = new ObjectIdCollection();
            //VentSystemelper.FindJointGEs(ref nodeIds);

            // 构造拓扑关系
            JArray edges = new JArray();
            foreach (ObjectId objId in edgeIds)
            {
                ObjectId s = arx.MEntity.Data.GetObjectId(objId, "始节点ID");
                ObjectId t = arx.MEntity.Data.GetObjectId(objId, "末节点ID");
                JObject e = new JObject();
                e["id"] = objId.Handle.ToString();
                e["s"] = s.Handle.ToString();
                e["t"] = t.Handle.ToString();
                e["r"] = arx.MExtDict.Data.GetDouble(objId, "通风.风阻");
                if (hasQ)
                {
                    e["q"] = arx.MExtDict.Data.GetDouble(objId, "通风.解算风量");
                }
                edges.Add(e);
            }
            json["edges"] = edges;

            // 查找所有的风机
            ObjectIdCollection fanIds = new ObjectIdCollection();
            VentSystemelper.FindMainFans(ref fanIds);
            JArray fans = new JArray();
            foreach (ObjectId objId in fanIds)
            {
                JObject fan = new JObject();
                fan["a0"] = arx.MExtDict.Data.GetDouble(objId, "通风.a0");
                fan["a1"] = arx.MExtDict.Data.GetDouble(objId, "通风.a1");
                fan["a2"] = arx.MExtDict.Data.GetDouble(objId, "通风.a2");
                ObjectId hostId = arx.MEntity.Data.GetObjectId(objId, "宿主ID");
                fan["eID"] = hostId.Handle.ToString();
                fans.Add(fan);
            }
            json["fans"] = fans;

            // 查找所有的固定风量
            JArray fixQEdges = new JArray();
            foreach (ObjectId objId in edgeIds)
            {
                double fq = arx.MExtDict.Data.GetDouble(objId, "通风.固定风量");
                if( fq  > 0)
                {
                    JObject e = new JObject();
                    e["eID"] = objId.Handle.ToString();
                    e["fq"] = fq;
                    fixQEdges.Add(e);
                }
            }
            json["qFixs"] = fixQEdges;

            // 查找所有的构筑物

            // 设置其它参数
            json["maxCount"] = 1000;
            json["precise"] = 0.001;
            json["Q"] = 1000;
            //return json.ToString();
            return json;
        }
        public static void UpdateVnoFromJson(JObject json, string key)
        {
            try
            {
                foreach (JObject e in json["edges"])
                {
                    ObjectId objId = arx.MUtil.StrtoObjectId(e["id"].ToString());
                    arx.MExtDict.Data.SetString(objId, "通风.解算风量", e["q"].ToString());
                }
            }
            catch (System.Exception e)
            {
                arx.MUtil.Printf(string.Format("\n异常原因:%s", e.StackTrace));
            }
        }
        public static void JsonArrayToObjectIds(JObject json, string key, ref ObjectIdCollection objIds)
        {
            try
            {
                foreach (var item in json[key])
                {
                    ObjectId objId = arx.MUtil.StrtoObjectId(item.ToString());
                    objIds.Add(objId);
                }
            }
            catch (System.Exception e)
            {
                arx.MUtil.Printf(string.Format("\n异常原因:%s", e.StackTrace));
            }
        }
    }
}
