﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Windows;

using AcadApplication = Autodesk.AutoCAD.ApplicationServices.Application;
using AcadDocument = Autodesk.AutoCAD.ApplicationServices.Document;
using AcadWindows = Autodesk.AutoCAD.Windows;

namespace MisVno
{
    class UIHelper
    {
        // TODO:非模态对话框显示网络解算结果(所有巷道的数据列表)
        public static void ShowVno()
        {

        }
        // TODO:非模态对话框显示所有进风井
        public static void ShowSourceEdges(ObjectIdCollection objIds)
        {

        }
        // TODO:非模态对话框显示所有回风井
        public static void ShowSinkEdges(ObjectIdCollection objIds)
        {

        }
        // TODO:非模态对话框显示风门构筑物
        public static void ShowGates(ObjectIdCollection objIds)
        {

        }
        // TODO:非模态对话框显示风机
        public static void ShowFans(ObjectIdCollection objIds)
        {

        }
        // TODO:消息对话框显示是否连通图
        public static void ShowIsConnected(bool ret)
        {

        }
        // TODO:消息对话框显示是否有向无环图
        public static void ShowIsDag(bool ret)
        {

        }
        // TODO:消息对话框显示是否有单向回路
        public static void ShowHasCycles(bool ret)
        {

        }
        // TODO:非模态对话框显示连通块
        public static void ShowCC(System.Collections.Generic.Dictionary<string, ObjectIdCollection> comps)
        {
            foreach (var item in comps)
            {
                arx.MUtil.Printf("\n连通块"+item.Key + ":");
                foreach (ObjectId objId in item.Value)
                {
                    arx.MUtil.Printf("  "+objId.Handle.ToString());
                }
                arx.MUtil.Printf("\n");
            }
        }
        // TODO:非模态对话框显示单向回路(强连通块)
        public static void ShowSCC(System.Collections.Generic.Dictionary<string, ObjectIdCollection> loops)
        {
            if (loops.Count == 0)
            {
                arx.MUtil.Printf("\n恭喜,系统中不存在单向回路!!!");
                return;
            }
            foreach (var item in loops)
            {
                arx.MUtil.Printf("\n单向回路" + item.Key + ":");
                foreach (ObjectId objId in item.Value)
                {
                    arx.MUtil.Printf("  " + objId.Handle.ToString());
                }
                arx.MUtil.Printf("\n");
            }
        }
        // TODO:非模态对话框显示负风量巷道
        public static void ShowNegativeEdges(ObjectIdCollection objIds)
        {

        }
        // TODO:非模态对话框显示固定风量巷道
        public static void ShowFixQEdges(ObjectIdCollection objIds)
        {

        }
    }
}
