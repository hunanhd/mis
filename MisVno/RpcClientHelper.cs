﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Thrift;
using Thrift.Protocol;
using Thrift.Server;
using Thrift.Transport;

using vno;

namespace MisVno
{
    public delegate void VnoDelegate(VnoService.Client client);

    class RpcClientHelper
    {
        public static void CallVnoService(VnoDelegate func)
        {
            try
            {
                TTransport transport = new TSocket("localhost", 9090);
                TProtocol protocol = new TBinaryProtocol(transport);
                VnoService.Client client = new VnoService.Client(protocol);
                try
                {
                    transport.Open();
                    func(client);
                }
                finally
                {
                    transport.Close();
                }
            }
            catch (TApplicationException x)
            {
                arx.MUtil.Printf("\n" + x.StackTrace);
            }
        }
    }
}
