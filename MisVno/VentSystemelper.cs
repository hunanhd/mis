﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Windows;

using AcadApplication = Autodesk.AutoCAD.ApplicationServices.Application;
using AcadDocument = Autodesk.AutoCAD.ApplicationServices.Document;
using AcadWindows = Autodesk.AutoCAD.Windows;

namespace MisVno
{
    class VentSystemelper
    {
        public static void FindFans(ref ObjectIdCollection objIds)
        {
            VentSystemelper.FindMainFans(ref objIds);
            VentSystemelper.FindLocalFans(ref objIds);
        }
        // 查找主扇
        public static void FindMainFans(ref ObjectIdCollection objIds)
        {
            arx.MUtil.GetEntsByType("MainFan", ref objIds, true);
        }

        // 查找构筑物
        public static void FindGates(ref ObjectIdCollection objIds)
        {
            arx.MUtil.GetEntsByType("Gate", ref objIds, true);
            arx.MUtil.GetEntsByType("Casement", ref objIds, true);
        }

        // 查找分支图元：巷道、工作面、硐室等
        public static void FindEdgeGEs(ref ObjectIdCollection objIds)
        {
            arx.MUtil.GetEntsByType("EdgeGE", ref objIds, true);
        }
        // 查找节点
        public static void FindJointGEs(ref ObjectIdCollection objIds)
        {
            arx.MUtil.GetEntsByType("JointGE", ref objIds, true);
        }
        // 查找掘进巷道
        public static void FindTTunnels(ref ObjectIdCollection objIds)
        {
            //arx.MUtil.GetEntsByType("TTunnel", ref objIds, true);
        }

        // 过滤掘进巷道
        public static void FilterTTunnels(ref ObjectIdCollection objIds)
        {
            //arx.MUtil.GetEntsByType("TTunnel", ref objIds, true);
        }

        // 查找局扇
        public static void FindLocalFans(ref ObjectIdCollection objIds)
        {
            arx.MUtil.GetEntsByType("LocalFan", ref objIds, true);
        }

        // 查找风筒
        public static void FindChimneys(ref ObjectIdCollection objIds)
        {
            arx.MUtil.GetEntsByType("Chimney", ref objIds, true);
        }

        // 查找所有的密闭
        public static void FindBlocks(ref ObjectIdCollection objIds)
        {
            arx.MUtil.GetEntsByType("Wall", ref objIds, true);
            arx.MUtil.GetEntsByType("DoubleGate", ref objIds, true);
            arx.MUtil.GetEntsByType("BalanceGate", ref objIds, true);
        }

        // 获取标签图元(TagGE)的宿主图元
        public static void GetHosts(ObjectIdCollection objIds, ref ObjectIdCollection hosts)
        {
            foreach (ObjectId objId in objIds)
            {
                if (arx.MUtil.IsEqualType("TagGE", objId, true))
                {
                    ObjectId host = arx.MEntity.Data.GetObjectId(objId, "宿主ID");
                    hosts.Add(host);
                }
            }
        }
        // 获取方向图元的插入点坐标
        public static void GetInsertPtsOfDirGE(ObjectIdCollection objIds, ref Point3dCollection pts)
        {
            foreach (ObjectId objId in objIds)
            {
                if (arx.MUtil.IsEqualType("DirGE", objId, true))
                {
                    Point3d pt = arx.MExtDict.Data.GetPoint(objId, "坐标");
                    pts.Add(pt);
                }
            }
        }
    }
}
