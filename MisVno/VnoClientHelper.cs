using System;
using System.Collections.Generic;
using Thrift;
using Thrift.Protocol;
using Thrift.Server;
using Thrift.Transport;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Windows;

using AcadApplication = Autodesk.AutoCAD.ApplicationServices.Application;
using AcadDocument = Autodesk.AutoCAD.ApplicationServices.Document;
using AcadWindows = Autodesk.AutoCAD.Windows;

using vno;
using Newtonsoft.Json.Linq;

namespace MisVno
{

    public class VnoClientHelper
    {
        public static void Test(VnoService.Client client)
        {
            int sum = client.Test(10, 20);
            arx.MUtil.Printf(string.Format("\n1+1={0}", sum));
        }
        public static void RunVno(VnoService.Client client)
        {
            // 构造计算所需参数
            JObject json1 = VentNetworkHelper.BuildJson(false);
            arx.MUtil.Printf("\n" + json1.ToString());

            // 远程调用
            string jsonStr = client.RunVno(json1.ToString());
            arx.MUtil.Printf("\n计算结果:\n" + jsonStr);
            
            // 提取计算结果
            JObject json2 = JObject.Parse(jsonStr);
            if (json2["ret"].ToString() == "True")
            {
                arx.MUtil.Printf("\n计算成功!");
                // 写入到图元里
                VentNetworkHelper.UpdateVnoFromJson(json2, "edges");
                // TODO:非模态对话框显示网络解算结果(巷道列表)
                UIHelper.ShowVno();
            }
        }
        public static void GetSourceEdges(VnoService.Client client)
        {
            // 构造计算所需参数
            JObject json1 = VentNetworkHelper.BuildJson(false);
            arx.MUtil.Printf("\n" + json1.ToString());

            // 远程调用
            string jsonStr = client.GetSourceEdges(json1.ToString());
            arx.MUtil.Printf("\n计算结果:\n" + jsonStr);

            // 提取计算结果
            JObject json2 = JObject.Parse(jsonStr);
            if (json2["ret"].ToString() == "True")
            {
                arx.MUtil.Printf("\n计算成功!");
                ObjectIdCollection objIds = new ObjectIdCollection();
                // 从json结果中读取图元id
                VentNetworkHelper.JsonArrayToObjectIds(json2, "source_edges", ref objIds);
                // 非模态对话框显示进风井
                UIHelper.ShowSourceEdges(objIds);
            }
        }

        public static void GetSinkEdges(VnoService.Client client)
        {
            // 构造计算所需参数
            JObject json1 = VentNetworkHelper.BuildJson(false);
            arx.MUtil.Printf("\n" + json1.ToString());

            // 远程调用
            string jsonStr = client.GetSinkEdges(json1.ToString());
            arx.MUtil.Printf("\n计算结果:\n" + jsonStr);

            // 提取计算结果
            JObject json2 = JObject.Parse(jsonStr);
            if (json2["ret"].ToString() == "True")
            {
                arx.MUtil.Printf("\n计算成功!");

                ObjectIdCollection objIds = new ObjectIdCollection();
                // 从json结果中读取图元id
                VentNetworkHelper.JsonArrayToObjectIds(json2, "sink_edges", ref objIds);
                // 非模态对话框显示回风井
                UIHelper.ShowSinkEdges(objIds);
            }
        }
        public static void GetGateEdges()
        {
            ObjectIdCollection objIds = new ObjectIdCollection();
            //objIds.Add(ObjectId.Null);
            // ObjectIdCollection的Count属性有bug,始终返回0!!!
            //arx.MUtil.Printf(string.Format("\n风门个数:%d", objIds.Count));
            VentSystemelper.FindGates(ref objIds);            
            foreach (ObjectId objId in objIds)
            {
                arx.MUtil.Printf("\n构筑物分支:"+arx.MUtil.ObjectIdToStr(objId));
            }
            // TODO: 非模态对话框显示风门构筑物
            UIHelper.ShowGates(objIds);
        }
        public static void GetFanEdges()
        {
            ObjectIdCollection objIds = new ObjectIdCollection();
            //objIds.Add(ObjectId.Null);
            // ObjectIdCollection的Count属性有bug,始终返回0!!!
            arx.MUtil.Printf(string.Format("\n风门个数:%d", objIds.Count));
            VentSystemelper.FindFans(ref objIds);
            foreach (ObjectId objId in objIds)
            {
                arx.MUtil.Printf("\n风机分支:" + arx.MUtil.ObjectIdToStr(objId));
            }
            // TODO: 非模态对话框显示风机
            UIHelper.ShowFans(objIds);
        }
        public static void TestIsConnected(VnoService.Client client)
        {
            // 构造计算所需参数
            JObject json1 = VentNetworkHelper.BuildJson(false);
            arx.MUtil.Printf("\n" + json1.ToString());

            // 远程调用
            bool ret = client.IsConnected(json1.ToString());
            arx.MUtil.Printf("\n是否连通:\n" + ret.ToString());
            // TODO:消息对话框显示是否连通图
            UIHelper.ShowIsConnected(ret);
        }
        public static void TestIsDag(VnoService.Client client)
        {
            // 构造计算所需参数
            JObject json1 = VentNetworkHelper.BuildJson(false);
            arx.MUtil.Printf("\n" + json1.ToString());

            // 远程调用
            bool ret = client.IsDag(json1.ToString());
            arx.MUtil.Printf("\n是否有向无环图:\n" + ret.ToString());
            // TODO:消息对话框显示是否有向无环图
            UIHelper.ShowIsDag(ret);
        }
        public static void TestHasCycles(VnoService.Client client)
        {
            // 构造计算所需参数
            JObject json1 = VentNetworkHelper.BuildJson(false);
            arx.MUtil.Printf("\n" + json1.ToString());

            // 远程调用
            bool ret = client.HasCycles(json1.ToString());
            arx.MUtil.Printf("\n是否有单向回路:\n" + ret.ToString());
            // TODO:消息对话框显示是否有单向回路
            UIHelper.ShowHasCycles(ret);
        }
        private static System.Collections.Generic.Dictionary<string, ObjectIdCollection> ParseComps(JToken json)
        {
            if (json == null) return null;
            System.Collections.Generic.Dictionary<string, ObjectIdCollection> dict =
                    new System.Collections.Generic.Dictionary<string, ObjectIdCollection>();
            JObject _comps = json.ToObject<JObject>();
            foreach (JProperty prop in _comps.Properties())
            {
                arx.MUtil.Printf(prop.Name + ":" + prop.Value.ToString());
                ObjectIdCollection objIds = new ObjectIdCollection();
                foreach (var item in prop.Value)
                {
                    ObjectId objId = arx.MUtil.StrtoObjectId(item.ToString());
                    objIds.Add(objId);
                }
                dict.Add(prop.Name, objIds);
            }
            return dict;
        }
        public static void GetConnectedComponents(VnoService.Client client)
        {
            // 构造计算所需参数
            JObject json1 = VentNetworkHelper.BuildJson(false);
            arx.MUtil.Printf("\n" + json1.ToString());

            // 远程调用
            string jsonStr = client.CC(json1.ToString());
            arx.MUtil.Printf("\n计算结果:\n" + jsonStr);

            // 提取计算结果
            JObject json2 = JObject.Parse(jsonStr);
            if (json2["ret"].ToString() == "True")
            {
                arx.MUtil.Printf("\n计算成功!");
                // 从json结果读取连通块
                System.Collections.Generic.Dictionary<string, ObjectIdCollection> comps 
                    = ParseComps(json2["components"]);
                if (comps != null)
                {
                    // TODO:非模态对话框显示连通块
                    UIHelper.ShowCC(comps);
                }
            }            
        }
        public static void GetSingleLoops(VnoService.Client client)
        {
            // 构造计算所需参数
            JObject json1 = VentNetworkHelper.BuildJson(false);
            arx.MUtil.Printf("\n" + json1.ToString());

            // 远程调用
            string jsonStr = client.SCC(json1.ToString());
            arx.MUtil.Printf("\n计算结果:\n" + jsonStr);

            // 提取计算结果
            JObject json2 = JObject.Parse(jsonStr);
            if (json2["ret"].ToString() == "True")
            {
                arx.MUtil.Printf("\n计算成功!");
                // 从json结果读取单向回路
                System.Collections.Generic.Dictionary<string, ObjectIdCollection> comps
                    = ParseComps(json2["loops"]);
                if (comps != null)
                {
                    // TODO:非模态对话框显示单向回路
                    UIHelper.ShowSCC(comps);
                }
            }
        }
        public static void GetNegativeEdges()
        {
            ObjectIdCollection objIds = new ObjectIdCollection();
            VentSystemelper.FindEdgeGEs(ref objIds);

            ObjectIdCollection negObjIds = new ObjectIdCollection();
            foreach (ObjectId objId in objIds)
            {
                double q = arx.MExtDict.Data.GetDouble(objId, "通风.解算风量");
                if (q < 0)
                {
                    negObjIds.Add(objId);
                }
            }
            // TODO:非模态对话框显示负风量巷道
            UIHelper.ShowNegativeEdges(negObjIds);
        }

        public static void GetNegativeEdgesWithRpc(VnoService.Client client)
        {
            // 构造计算所需参数
            JObject json1 = VentNetworkHelper.BuildJson(true);
            arx.MUtil.Printf("\n" + json1.ToString());

            // 远程调用
            string jsonStr = client.GetNegativeEdges(json1.ToString());
            arx.MUtil.Printf("\n计算结果:\n" + jsonStr);

            // 提取计算结果
            JObject json2 = JObject.Parse(jsonStr);
            if (json2["ret"].ToString() == "True")
            {
                arx.MUtil.Printf("\n计算成功!");

                ObjectIdCollection objIds = new ObjectIdCollection();
                // 从json结果中读取图元id
                VentNetworkHelper.JsonArrayToObjectIds(json2, "negative_edges", ref objIds);
                // TODO:非模态对话框显示负风量巷道
                UIHelper.ShowNegativeEdges(objIds);
            }
        }

        public static void GetFixQEdges()
        {
            ObjectIdCollection objIds = new ObjectIdCollection();
            VentSystemelper.FindEdgeGEs(ref objIds);

            ObjectIdCollection fixQObjIds = new ObjectIdCollection();
            foreach (ObjectId objId in objIds)
            {
                double fq = arx.MExtDict.Data.GetDouble(objId, "通风.固定风量");
                if (fq > 0)
                {
                    fixQObjIds.Add(objId);
                }
            }
            // TODO:非模态对话框显示固定风量巷道
            UIHelper.ShowFixQEdges(fixQObjIds);
        }

        public static void GetFixQEdgesWithRpc(VnoService.Client client)
        {
            // 构造计算所需参数
            JObject json1 = VentNetworkHelper.BuildJson(true);
            arx.MUtil.Printf("\n" + json1.ToString());

            // 远程调用
            string jsonStr = client.GetFixQEdges(json1.ToString());
            arx.MUtil.Printf("\n计算结果:\n" + jsonStr);

            // 提取计算结果
            JObject json2 = JObject.Parse(jsonStr);
            if (json2["ret"].ToString() == "True")
            {
                arx.MUtil.Printf("\n计算成功!");

                ObjectIdCollection objIds = new ObjectIdCollection();
                // 从json结果中读取图元id
                VentNetworkHelper.JsonArrayToObjectIds(json2, "fixQ_edges", ref objIds);
                // TODO:非模态对话框显示固定风量巷道
                UIHelper.ShowNegativeEdges(objIds);
            }
        }
    }
}
