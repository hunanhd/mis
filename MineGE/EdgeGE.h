#pragma once
#include "MineGE.h"
//起连接作用的图元，例如巷道、硐室、工作面、掘进工作面
// 它们在通风网络中可以看做是一条分支
class MINEGE_EXPORT_API EdgeGE : public MineGE
{
public:
    ACRX_DECLARE_MEMBERS( EdgeGE ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    void setStartId(const AcDbObjectId& objId);
    AcDbObjectId getStartId() const;
    void setEndId(const AcDbObjectId& objId);
    AcDbObjectId getEndId() const;

    // 设置始末点坐标
    void setSEPoint( const AcGePoint3d& startPt, const AcGePoint3d& endPt );
    // 获取始末点坐标
    void getSEPoint( AcGePoint3d& startPt, AcGePoint3d& endPt ) const;
    // 计算两坐标之间的角度
    double getAngle() const;
    // 虚函数(用于实现节点闭合)
public:
    // 反向(交换始末点位置)
    virtual void reverse();
    // 处理连接点闭合情况
    // 返回的向量都是标准化的向量(模--向量长度为1)
    // 注："零向量"表示不处理闭合
    virtual AcGeVector3d getStartPointInExtendAngle() const;
    virtual AcGeVector3d getEndPointInExtendAngle() const;
    virtual void dealWithStartPointBoundary( const AcGeRay3d& boundaryLine );
    virtual void dealWithEndPointBoundary( const AcGeRay3d& boundaryLine );
    virtual void extendByLength( double length );
    virtual void update();
    // 重载AcDbObject的虚函数(实现dwg读写)
public:
    virtual Acad::ErrorStatus dwgOutFields ( AcDbDwgFiler* pFiler ) const;
    virtual Acad::ErrorStatus dwgInFields ( AcDbDwgFiler* pFiler );
    // 重载AcDbEntity虚函数(图形交互操作)
protected:
    virtual Adesk::Boolean subWorldDraw( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus subTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus subMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
    virtual Acad::ErrorStatus subErase( Adesk::Boolean erasing );	// 处理删除时的闭合
protected:
    EdgeGE();
    EdgeGE( const AcGePoint3d& startPt, const AcGePoint3d& endPt );
private:
    void doEdgeGEJunctionClosure();    // 处理闭合
protected:
    AcGePoint3d m_startPt, m_endPt;    // 始末点坐标
    AcDbObjectId m_startId, m_endId;  // 始末节点ID
} ;
#ifdef MINEGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( EdgeGE )
#endif