#pragma once
#include "MineGE.h"
class MINEGE_EXPORT_API BlockGE : public MineGE
{
public:
    ACRX_DECLARE_MEMBERS( BlockGE ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    // 获取插入点坐标
    AcGePoint3d getInsertPt() const;
    // 设置插入点坐标
    void setInsertPt( const AcGePoint3d& pt );
    // 获取方向角度
    double getAngle() const;
    // 设置方向角度
    void setAngle( double angle );
    // 获取缩放比例
    double getScale() const;
    //设置缩放比例
    void setScale( double scale );
    // 设置参数
    void set( const AcGePoint3d& insertPt, double angle = 0.0, double scale = 1.0 );
    // 设置块名称
    void setBlock( const CString& blockName );
    // 获取块名称
    CString getBlock() const;
    //virtual void configExtraParams();
    // 重载MineGE虚函数(数据操作)
protected:
    // 注册要读取的属性数据字段
    virtual void regPropertyDataNames( AcStringArray& names ) const;
    // MineGE基类负责读取数据,派生类负责将数据填充到自己的成员变量中
    virtual void readPropertyDataFromValues( const AcStringArray& values );
    // 重载MineGE虚函数(图形操作)
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customGetGripPoints(
        AcGePoint3dArray& gripPoints,
        AcDbIntArray& osnapModes,
        AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
    // 重载AcDbEntity虚函数(dwg序列化操作)
public:
    virtual Acad::ErrorStatus dwgOutFields ( AcDbDwgFiler* pFiler ) const;
    virtual Acad::ErrorStatus dwgInFields ( AcDbDwgFiler* pFiler );
protected:
    BlockGE();
    BlockGE( const CString& blockName );
    BlockGE( const CString& blockName, const AcGePoint3d& insertPt, double angle = 0.0, double scale = 1.0 );
    virtual ~BlockGE();
private:
    void explodeBlock( AcGeVoidPointerArray& ents );
protected:
    AcGePoint3d m_insertPt; // 插入点坐标
    double m_angle;         // 角度
    double m_scale;         // 缩放比例
    CString m_blockName;    // 块名称
    AcStringArray m_blockAttrValues; // 从图元提取的属性数据
};
// 自定义图元(基于BlockGE实现)
// 要求：块的名称是英文名称(最好是按照类的命名格式),即块名称就是"类型"名称!!!
class MINEGE_EXPORT_API CustomGE : public BlockGE
{
public:
    ACRX_DECLARE_MEMBERS( CustomGE ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    CustomGE();
    CustomGE( const CString& blockName );
    CustomGE( const CString& blockName, const AcGePoint3d& insertPt, double angle = 0.0, double scale = 1.0 );
    virtual ~CustomGE();
    // 重载MineGE虚函数
protected:
    // 返回实际类型名称
    virtual CString getRealTypeName() const;
};
#ifdef MINEGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( BlockGE )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( CustomGE )
#endif
