#include "stdafx.h"
#include "EdgeGE.h"
//#include <algorithm>
#include <vector>

// 查找连接点junctionPt关联的分支图元(包含隐形的图元)
static void FindLinesByPoint( const AcGePoint3d& junctionPt, AcDbObjectIdArray& objIds )
{
    AcDbBlockTable* pBlkTbl;
    acdbHostApplicationServices()->workingDatabase()->getSymbolTable( pBlkTbl, AcDb::kForRead );
    AcDbBlockTableRecord* pBlkTblRcd;
    pBlkTbl->getAt( ACDB_MODEL_SPACE, pBlkTblRcd, AcDb::kForRead );
    pBlkTbl->close();
    AcDbBlockTableRecordIterator* pBlkTblRcdItr;
    pBlkTblRcd->newIterator( pBlkTblRcdItr );
    for ( pBlkTblRcdItr->start(); !pBlkTblRcdItr->done(); pBlkTblRcdItr->step() )
    {
        // 不采用transaction的方法查找EdgeGE，
        // 等价于排除当前正在以write状态编辑的EdgeGE
        // 重要(***)
        AcDbEntity* pEnt = 0;
        if( Acad::eOk != pBlkTblRcdItr->getEntity( pEnt, AcDb::kForRead ) ) continue;
        EdgeGE* pEdge = EdgeGE::cast( pEnt );
        if( pEdge != 0 )
        {
            AcGePoint3d startPt, endPt;
            pEdge->getSEPoint( startPt, endPt );
            if( startPt == junctionPt || endPt == junctionPt )
            {
                objIds.append( pEdge->objectId() );
            }
        }
        pEnt->close();
    }
    delete pBlkTblRcdItr;
    pBlkTblRcd->close();
}
struct JunctionEdgeInfo
{
    AcDbObjectId objId;             // 图元的id
    bool startOrEnd;                // 连接点是始点还是末点
    AcGeVector3d angle;             // 向内延伸方向向量
    bool operator==( const JunctionEdgeInfo& info )
    {
        return ( objId == info.objId ) &&
            ( startOrEnd == info.startOrEnd ) &&
            ( angle == info.angle );
    }
};
typedef std::vector<JunctionEdgeInfo> EdgeInfo;
typedef EdgeInfo::iterator EdgeInfoIter;
static EdgeGE* OpenEdge2( AcTransaction* pTrans, const AcDbObjectId& objId, AcDb::OpenMode openMode )
{
    AcDbObject* pObj = 0;
    if( Acad::eOk != pTrans->getObject( pObj, objId, openMode ) ) return 0;
    EdgeGE* pEdge = EdgeGE::cast( pObj );
    return pEdge;
}
static void BuildJunctionEdgeInfo( const AcDbObjectIdArray& objIds,
                                  const AcGePoint3d& junctionPt,
                                  EdgeInfo& ges )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    int n = objIds.length();
    for( int i = 0; i < n; i++ )
    {
        AcDbObjectId objId = objIds.at( i );
        JunctionEdgeInfo info = {objId, true, AcGeVector3d::kIdentity};
        EdgeGE* pEdge = OpenEdge2( pTrans, objId, AcDb::kForRead );
        if( pEdge == 0 ) continue;
        AcGePoint3d startPt, endPt;
        pEdge->getSEPoint( startPt, endPt );
        if( startPt == junctionPt )
        {
            info.startOrEnd = true;
            info.angle = pEdge->getStartPointInExtendAngle();
        }
        else if( endPt == junctionPt )
        {
            info.startOrEnd = false;
            info.angle = pEdge->getEndPointInExtendAngle();
        }
        if( info.angle.isZeroLength() ) continue; // 零向量表示不处理闭合
        //acutPrintf(_T("\nid:%d\tangle:%.3f\tdraw:%s"),
        //			info.objId,
        //			info.angle.angleTo(AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis),
        //			pEdge->getCurrentDraw());
        ges.push_back( info );
    }
    actrTransactionManager->endTransaction();
}
static void DealWithJoint( AcTransaction* pTrans,  const JunctionEdgeInfo& info, const AcDbObjectId& jointId )
{
    EdgeGE* pEdge = OpenEdge2( pTrans, info.objId, AcDb::kForWrite ); // 以write模式打开
    if( pEdge == 0 ) return;

    if( info.startOrEnd )
    {
        pEdge->setStartId(jointId);
    }
    else
    {
        pEdge->setEndId(jointId);
    }
}
static void JointRelationsImpl(EdgeInfo& ges, const AcDbObjectId& jointId)
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;

    for( EdgeInfoIter itr = ges.begin(); itr != ges.end(); itr++ )
    {
        DealWithJoint(pTrans, *itr, jointId);
    }
    actrTransactionManager->endTransaction();
}
static void UpdateEdge( const AcDbObjectIdArray& objIds )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    int n = objIds.length();
    for( int i = 0; i < n; i++ )
    {
        AcDbObjectId objId = objIds.at( i );
        EdgeGE* pEdge = OpenEdge2( pTrans, objId, AcDb::kForWrite );
        pEdge->recordGraphicsModified( true ); // 标签图形已经修改，需要更新图形
    }
    actrTransactionManager->endTransaction();
}
void JointRelations_Helper( const AcGePoint3d& junctionPt, const AcDbObjectId& jointId )
{
    //if(jointId.isNull()) return ;

    //acutPrintf(_T("\n闭合点:(%.3f, %.3f)"), junctionPt.x, junctionPt.y);
    // 查找关联的巷道
    AcDbObjectIdArray objIds;
    FindLinesByPoint( junctionPt, objIds );
    int len = objIds.length();
    //acutPrintf(_T("\n找到要处理闭合的分支个数:%d"), len);
    if( len < 1 ) return;
    EdgeInfo ges;
    BuildJunctionEdgeInfo( objIds, junctionPt, ges );   // 查找junctionPt坐标处的关联分支类图元
    if( ges.size() > 0 )
    {
        JointRelationsImpl( ges, jointId );            // 处理并修改相邻巷道的参数
    }
    //UpdateEdge( objIds );                            // 更新实体
}