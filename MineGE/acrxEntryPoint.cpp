#include "StdAfx.h"
#include "HelperClass.h"
#include "ReactorHelper.h"
#include "config.h"
#include "MineGE.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
// 定义注册服务名称
#ifndef MINEGE_SERVICE_NAME
#define MINEGE_SERVICE_NAME _T("MINEGE_SERVICE_NAME")
#endif
class CMineGEApp : public AcRxArxApp
{
public:
    CMineGEApp () : AcRxArxApp () {}
    virtual AcRx::AppRetCode On_kInitAppMsg ( void* pkt )
    {
        AcRx::AppRetCode retCode = AcRxArxApp::On_kInitAppMsg ( pkt ) ;
        acrxRegisterAppMDIAware( pkt );
        // 注册服务
        acrxRegisterService( MINEGE_SERVICE_NAME );
        ReactorHelper::CreateEditorReactor();
        ReactorHelper::CreateDocManagerReactor();
        ReactorHelper::CreateDocumentReactorMap();
        acutPrintf( _T( "\nMineGE::On_kInitAppMsg\n" ) );
        return ( retCode ) ;
    }
    virtual AcRx::AppRetCode On_kUnloadAppMsg ( void* pkt )
    {
        AcRx::AppRetCode retCode = AcRxArxApp::On_kUnloadAppMsg ( pkt ) ;
        // 删除AcEditorReactor
        ReactorHelper::RemoveEditorReactor();
        ReactorHelper::RemoveDocManagerReactor();
        ReactorHelper::RemoveDocumentReactorMap();
        // 删除服务
        delete acrxServiceDictionary->remove( MINEGE_SERVICE_NAME );
        acutPrintf( _T( "\nMineGE::On_kUnloadAppMsg\n" ) );
        return ( retCode ) ;
    }
    static void PrintAddress()
    {
        AcDbDatabase* pDB = acdbHostApplicationServices()->workingDatabase();
        AcEditor* pEditor = acedEditor;
        AcApDocument* pDoc = curDoc();
        AcDbDatabase* pDocDB = pDoc->database();
        AcEdInputPointManager* pIPM = pDoc->inputPointManager();
        acutPrintf( _T( "\nworking database: %ld" ), ( long )pDB );
        acutPrintf( _T( "\neditor: %ld" ), ( long )pEditor );
        acutPrintf( _T( "\ncurrent doc: %ld" ), ( long )pDoc );
        acutPrintf( _T( "\ncurrent doc database: %ld" ), ( long )pDocDB );
        acutPrintf( _T( "\ncurrent doc InputPointManager: %ld" ), ( long )pIPM );
    }
    // 可以用来处理与文档对象AcDocument相关联的reactor
    //  例如AcEdInputPointMonitor等
    // 在On_kLoadDwgMsg和On_kUnloadDwgMsg消息中文档对象还是有效的；
    // 而在On_kInitAppMsg和On_kUnloadAppMsg
    // 文档对象已经失效; 数据库对象同样也是如此
    virtual AcRx::AppRetCode On_kLoadDwgMsg( void* pkt )
    {
        AcRx::AppRetCode retCode = AcRxArxApp::On_kLoadDwgMsg ( pkt ) ;
        //PrintAddress();
        // 添加反应器
        ReactorHelper::AddDocumentReactor( curDoc() );
        acutPrintf( _T( "\nMineGE::On_kLoadDwgMsg\n" ) );
        return retCode;
    }
    virtual AcRx::AppRetCode On_kUnloadDwgMsg( void* pkt )
    {
        AcRx::AppRetCode retCode = AcRxArxApp::On_kUnloadDwgMsg ( pkt ) ;
        ReactorHelper::RemoveDocumentReactor( curDoc() );
        acutPrintf( _T( "\nMineGE::On_kUnloadDwgMsg\n" ) );
        return retCode;
    }
    virtual void RegisterServerComponents ()
    {
    }
    static void MineGE_ZoomTest( void )
    {
        AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "请选择一个图元实体:" ) );
        if( objId.isNull() ) return;
        ArxEntityHelper::ZoomToEntity( objId );
    }
    static void MineGE_TestBackground()
    {
        AcDbObjectId objId = ArxUtilHelper::SelectEntity( _T( "\n请选择一个图元:" ) );
        if( objId.isNull() ) return;
        if( !ArxUtilHelper::IsEqualType( _T( "MineGE" ), objId ) ) return;
        AcTransaction* pTrans = actrTransactionManager->startTransaction();
        if( pTrans == 0 ) return;
        AcDbObject* pObj;
        if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForWrite ) )
        {
            actrTransactionManager->abortTransaction();
            return;
        }
        MineGE* pGE = MineGE::cast(pObj);
        if( pGE == 0 )
        {
            actrTransactionManager->abortTransaction();
            return;
        }
        pGE->setBackground( 200, 122, 50 );
        actrTransactionManager->endTransaction();
    }
    /*static void MineGE_movedata(void)
    {
        extern void MoveData();
        MoveData();
    }*/
} ;
IMPLEMENT_ARX_ENTRYPOINT( CMineGEApp )
ACED_ARXCOMMAND_ENTRY_AUTO( CMineGEApp, MineGE,  _ZoomTest, ZoomTest, ACRX_CMD_TRANSPARENT, NULL )
//ACED_ARXCOMMAND_ENTRY_AUTO(CMineGEApp, MineGE,  _movedata, movedata, ACRX_CMD_TRANSPARENT, NULL)
ACED_ARXCOMMAND_ENTRY_AUTO(CMineGEApp, MineGE,  _TestBackground, TestBackground, ACRX_CMD_TRANSPARENT, NULL)
