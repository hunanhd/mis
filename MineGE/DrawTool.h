#pragma once
#include "dlimexp.h"
#include "config.h"
/* 全局函数(实现在DrawTool.cpp) */
// 3重堆叠格式，参见cad帮助："创建多行文字"
//	1) 符号: ^ (上下堆叠)
//	2) 符号: / (左右堆叠，中间一个左斜线)
//	3) 符号: # (上下堆叠，中间一个横线 )
extern MINEGE_EXPORT_API  CString MakeUpperText( const CString& inStr );
extern MINEGE_EXPORT_API  CString MakeLowerText( const CString& inStr );
extern MINEGE_EXPORT_API  AcGePoint2d Point3D_To_2D( const AcGePoint3d& pt );
extern MINEGE_EXPORT_API AcGePoint3d Point2D_To_3D( const AcGePoint2d& pt );
// 计算矩形框多边形
extern MINEGE_EXPORT_API void BuildRect( const AcGePoint3d& pt, double angle, double width, double height, AcGePoint3dArray& pts );
// 基础图形绘制
extern MINEGE_EXPORT_API void DrawLine( AcGiWorldDraw* mode, const AcGePoint3d& spt, const AcGePoint3d& ept );
extern MINEGE_EXPORT_API void DrawLine( AcGiWorldDraw* mode, const AcGePoint3d& pt, double angle, double length );
extern MINEGE_EXPORT_API void DrawDotLine( AcGiWorldDraw* mode, const AcGePoint3d& spt, const AcGePoint3d& ept );
extern MINEGE_EXPORT_API void DrawCircle( AcGiWorldDraw* mode, const AcGePoint3d& pt, double radius, bool fill );
extern MINEGE_EXPORT_API void DrawArc( AcGiWorldDraw* mode, const AcGePoint3d& spt, const AcGePoint3d& pt, const AcGePoint3d& ept, bool fill );
extern MINEGE_EXPORT_API void DrawArc( AcGiWorldDraw* mode, const AcGePoint3d& pt, double radius, double startAngle, double sweepAngle, bool fill );
extern MINEGE_EXPORT_API void DrawPolyLine( AcGiWorldDraw* mode, const AcGePoint3d& spt, const AcGePoint3d& ept, double width );
extern MINEGE_EXPORT_API void DrawPolyLine( AcGiWorldDraw* mode, const AcGePoint3d& pt, double angle, double width, double length );
extern MINEGE_EXPORT_API AcDbObjectId CreateTextStyle( const CString& style, const CString& winFont, Adesk::Boolean bold, Adesk::Boolean italic, int charset, int pitchAndFamily );
extern MINEGE_EXPORT_API AcDbObjectId GetTextStyle( const CString& style );
extern MINEGE_EXPORT_API void DrawMText( AcGiWorldDraw* mode, const AcGePoint3d& pt, double angle, const CString& str, double height, AcDbMText::AttachmentPoint ap = AcDbMText::kMiddleCenter, const CString& style = _T( "GDESS" ) );
extern MINEGE_EXPORT_API void DrawRect( AcGiWorldDraw* mode, const AcGePoint3d& pt, double angle, double width, double height, bool fill );
extern MINEGE_EXPORT_API void DrawSpline( AcGiWorldDraw* mode, const AcGePoint3dArray& pts );
extern MINEGE_EXPORT_API void DrawArrow( AcGiWorldDraw* mode, const AcGePoint3d& pt, double angle, double length, double width );
extern MINEGE_EXPORT_API void DrawPolygon( AcGiWorldDraw* mode, const AcGePoint3dArray& polygon, bool fill );
extern MINEGE_EXPORT_API void CreatePolygonLoop( AcDbHatch* pHatch, const AcGePoint3dArray& pts );
extern MINEGE_EXPORT_API void CreateCircleLoop( AcDbHatch* pHatch, const AcGePoint3d& pt, double radius );
extern MINEGE_EXPORT_API void CreatePreDefinedHatch( AcDbHatch* pHatch, const CString& patName, double scale );
extern MINEGE_EXPORT_API void CreateGradientObject( AcDbHatch* pHatch, const CString& gradName, const AcCmColor& c1, const AcCmColor& c2 );
extern MINEGE_EXPORT_API void DrawPolygonHatch( AcGiWorldDraw* mode, const AcGePoint3dArray& pts, const CString& patName, double scale );
extern MINEGE_EXPORT_API void DrawCircleGradient( AcGiWorldDraw* mode, const AcGePoint3d& pt, double radius, const CString& gradName, const AcCmColor& c1, const AcCmColor& c2 );
extern MINEGE_EXPORT_API void DrawBackGround( AcGiWorldDraw* mode , const AcGePoint3dArray& pts , int colorIndx );
// 返回值:
//		0  -- 错误
//		1  -- 顺时针
//	   -1  -- 逆时针
extern MINEGE_EXPORT_API int ClockWise( const AcGePoint3dArray& polygon );
// 闭合多边形偏移
// 参数is_nner--true表示向内偏移，false表示向外偏移
extern MINEGE_EXPORT_API bool OffSetPolygon( const AcGePoint3dArray& polygon, double offset, bool is_inner, AcGePoint3dArray& offset_polygon );

extern MINEGE_EXPORT_API Acad::ErrorStatus GetLinetypeId( const CString& linetype, AcDbObjectId& linetypeId );
extern MINEGE_EXPORT_API AcDbObjectId AddLineType( CString lineTypeName );