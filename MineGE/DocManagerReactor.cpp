#include "StdAfx.h"
#include "DocManagerReactor.h"
DocManagerReactor::DocManagerReactor()
{
}
void DocManagerReactor::documentToBeActivated( AcApDocument* pDoc )
{
    AcApDocManagerReactor::documentActivated( pDoc );
}
void DocManagerReactor::documentCreated( AcApDocument* pDoc )
{
    AcApDocManagerReactor::documentCreated( pDoc );
}
void DocManagerReactor::documentToBeDestroyed( AcApDocument* pDoc )
{
    AcApDocManagerReactor::documentToBeDestroyed( pDoc );
}