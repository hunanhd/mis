#include "StdAfx.h"
#include "JointGE.h"
#include "Drawtool.h"
#include "DrawSpecial.h"
#include "DrawHelper.h"

#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"

Adesk::UInt32 JointGE::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    JointGE, MineGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    节点, MINEGEAPP
)
JointGE::JointGE() : m_radius( 13.5 )
{
    map( _T( "坐标" ), &m_insertPt );
    map( _T( "半径" ), &m_radius );
}
JointGE::JointGE( const AcGePoint3d& insertPt ) : m_radius( 13.5 ), m_insertPt( insertPt )
{
    map( _T( "坐标" ), &m_insertPt );
    map( _T( "半径" ), &m_radius );
}
AcGePoint3d JointGE::getInsertPt() const
{
    assertReadEnabled();
    return m_insertPt;
}
void JointGE::setInsertPt( const AcGePoint3d& pt )
{
    assertWriteEnabled();
    m_insertPt = pt;
}
Acad::ErrorStatus JointGE::dwgOutFields ( AcDbDwgFiler* pFiler ) const
{
    assertReadEnabled () ;
    //----- Save parent class information first.
    Acad::ErrorStatus es = MineGE::dwgOutFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    //----- Object version number needs to be saved first
    if ( ( es = pFiler->writeUInt32 ( JointGE::kCurrentVersionNumber ) ) != Acad::eOk )
        return ( es ) ;
    pFiler->writeItem( m_insertPt );
    pFiler->writeItem( m_radius );
    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus JointGE::dwgInFields ( AcDbDwgFiler* pFiler )
{
    assertWriteEnabled () ;
    //----- Read parent class information first.
    Acad::ErrorStatus es = MineGE::dwgInFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    //----- Object version number needs to be read first
    Adesk::UInt32 version = 0 ;
    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
        return ( es ) ;
    if ( version > JointGE::kCurrentVersionNumber )
        return ( Acad::eMakeMeProxy ) ;
    pFiler->readItem( &m_insertPt );
    pFiler->readItem( &m_radius );
    return ( pFiler->filerStatus () ) ;
}
Adesk::Boolean JointGE::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    DrawCircle( mode, m_insertPt, m_radius, false );
    DrawCross( mode, m_insertPt, m_radius );
    return Adesk::kTrue;
}
Acad::ErrorStatus JointGE::customTransformBy( const AcGeMatrix3d& xform )
{
    m_insertPt.transformBy( xform );
    return Acad::eOk;
}
Acad::ErrorStatus JointGE::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉1种类型的点：插入点
    Acad::ErrorStatus es = Acad::eOk;
    if ( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return es;
}
Acad::ErrorStatus JointGE::customGetGripPoints (
    AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds
) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    return Acad::eOk;
}
Acad::ErrorStatus JointGE::customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        // 始节点
        if ( idx == 0 ) m_insertPt += offset;
    }
    return Acad::eOk;
}

Acad::ErrorStatus JointGE::subTransformBy( const AcGeMatrix3d& xform )
{
    //DrawHelper::JointRelations(m_insertPt, AcDbObjectId::kNull);
    return MineGE::subTransformBy( xform );
}
Acad::ErrorStatus JointGE::subMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    //DrawHelper::JointRelations(m_insertPt, AcDbObjectId::kNull);
    return MineGE::subMoveGripPointsAt( indices, offset );
}
Acad::ErrorStatus JointGE::subErase(Adesk::Boolean erasing)
{
    //acutPrintf(_T("\nid:%d call EdgeGE::subErase()..."), objectId());
    Acad::ErrorStatus retCode = MineGE::subErase ( erasing ) ;
    if( Acad::eOk == retCode )
    {
        if( !isNewObject() )
        {
            DrawHelper::JointRelations(m_insertPt, AcDbObjectId::kNull);
        }
    }
    return Acad::eOk;
}
