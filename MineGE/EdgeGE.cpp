#include "StdAfx.h"
#include "EdgeGE.h"
#include "JointGE.h"
#include "DrawHelper.h"
Adesk::UInt32 EdgeGE::kCurrentVersionNumber = 1 ;
// 有修改
ACRX_NO_CONS_DEFINE_MEMBERS ( EdgeGE, MineGE )
EdgeGE::EdgeGE()
{
    map( _T( "起点坐标" ), &m_startPt );
    map( _T( "末点坐标" ), &m_endPt );
    map( _T( "始节点ID" ), &m_startId );
    map( _T( "末节点ID" ), &m_endId );
}
EdgeGE::EdgeGE( const AcGePoint3d& startPt, const AcGePoint3d& endPt )
: m_startPt( startPt ), m_endPt( endPt )
{
    map( _T( "起点坐标" ), &m_startPt );
    map( _T( "末点坐标" ), &m_endPt );
    map( _T( "始节点ID" ), &m_startId );
    map( _T( "末节点ID" ), &m_endId );
}
void EdgeGE::setSEPoint( const AcGePoint3d& startPt, const AcGePoint3d& endPt )
{
    assertWriteEnabled();
    m_startPt = startPt;
    m_endPt = endPt;
    update();
}
void EdgeGE::getSEPoint( AcGePoint3d& startPt, AcGePoint3d& endPt ) const
{
    assertReadEnabled();
    startPt = m_startPt;
    endPt = m_endPt;
}
static void SwapPoint( AcGePoint3d& pt1, AcGePoint3d& pt2 )
{
    AcGePoint3d tpt = pt1;
    pt1 = pt2;
    pt2 = tpt;
}
static void SwapObjectId( AcDbObjectId& objId1, AcDbObjectId& objId2)
{
    AcDbObjectId tObjId = objId1;
    objId1 = objId2;
    objId2 = tObjId;
}
void EdgeGE::reverse()
{
    assertWriteEnabled();
    //updateDrawParam( false ); // to draw
    SwapPoint( m_startPt, m_endPt );
    SwapObjectId( m_startId, m_endId );
    //updateDrawParam( true ); // back to ge
}
void EdgeGE::dealWithStartPointBoundary( const AcGeRay3d& boundaryLine )
{
}
void EdgeGE::dealWithEndPointBoundary( const AcGeRay3d& boundaryLine )
{
}
double EdgeGE::getAngle() const
{
    assertReadEnabled();
    AcGeVector3d v = m_endPt - m_startPt;
    return v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
}
AcGeVector3d EdgeGE::getStartPointInExtendAngle() const
{
    assertReadEnabled();
    //updateDrawParam( false ); // to draw
    AcGeVector3d v = m_endPt - m_startPt;
    return v.normalize(); // 标准化
}
AcGeVector3d EdgeGE::getEndPointInExtendAngle() const
{
    assertReadEnabled();
    //updateDrawParam( false ); // to draw
    AcGeVector3d v = m_endPt - m_startPt;
    v.rotateBy( PI, AcGeVector3d::kZAxis ); // 旋转180度
    return v.normalize(); // 标准化
}
void EdgeGE::extendByLength( double length )
{
    assertWriteEnabled();
    //updateDrawParam( false ); // to draw
    //updateDrawParam( true );
}
Acad::ErrorStatus EdgeGE::dwgOutFields ( AcDbDwgFiler* pFiler ) const
{
    assertReadEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgOutFields"));
    Acad::ErrorStatus es = MineGE::dwgOutFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    if ( ( es = pFiler->writeUInt32 ( EdgeGE::kCurrentVersionNumber ) ) != Acad::eOk )
        return ( es ) ;
    // 保存始末点坐标
    pFiler->writeItem( m_startPt );
    pFiler->writeItem( m_endPt );

    // 写入id
    pFiler->writeHardPointerId(m_startId);
    pFiler->writeHardPointerId(m_endId);

    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus EdgeGE::dwgInFields ( AcDbDwgFiler* pFiler )
{
    assertWriteEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgInFields"));
    Acad::ErrorStatus es = MineGE::dwgInFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    Adesk::UInt32 version = 0 ;
    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
        return ( es ) ;
    if ( version > EdgeGE::kCurrentVersionNumber )
        return ( Acad::eMakeMeProxy ) ;
    // 读取始末点坐标
    pFiler->readItem( &m_startPt );
    pFiler->readItem( &m_endPt );

    // 读取id
    AcDbHardPointerId objId;
    pFiler->readHardPointerId(&objId);
    m_startId = objId;
    
    objId.setNull();
    pFiler->readHardPointerId(&objId);
    m_endId = objId;

    return ( pFiler->filerStatus () ) ;
}
void EdgeGE::doEdgeGEJunctionClosure()
{
    if( !isNewObject() )
    {
        DrawHelper::EdgeGEJunctionClosure( m_startPt );
        DrawHelper::EdgeGEJunctionClosure( m_endPt );
    }
}
Acad::ErrorStatus EdgeGE::subTransformBy( const AcGeMatrix3d& xform )
{
    //acutPrintf(_T("\nid:%d call EdgeGE::subTransformBy().."), objectId());
    doEdgeGEJunctionClosure(); // 处理闭合(当前正在编辑的图元不参与闭合处理)
    return MineGE::subTransformBy( xform );
}
Acad::ErrorStatus EdgeGE::subMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    //acutPrintf(_T("\nid:%d call EdgeGE::subMoveGripPointsAt()..."), objectId());
    doEdgeGEJunctionClosure(); // 处理闭合(当前正在编辑的图元不参与闭合处理)
    return MineGE::subMoveGripPointsAt( indices, offset );
    //return Acad::eOk;
}
Acad::ErrorStatus EdgeGE::subErase( Adesk::Boolean erasing )
{
    //acutPrintf(_T("\nid:%d call EdgeGE::subErase()..."), objectId());
    Acad::ErrorStatus retCode = MineGE::subErase ( erasing ) ;
    if( Acad::eOk == retCode )
    {
        doEdgeGEJunctionClosure(); // 处理闭合(当前正在编辑的图元不参与闭合处理)
    }
    return Acad::eOk;
}
void EdgeGE::update()
{
}

void EdgeGE::setStartId(const AcDbObjectId& objId)
{
    assertWriteEnabled () ;
    m_startId = objId;
}

AcDbObjectId EdgeGE::getStartId() const
{
    assertReadEnabled () ;
    return m_startId;
}

void EdgeGE::setEndId(const AcDbObjectId& objId)
{
    assertWriteEnabled () ;
    m_endId = objId;
}

AcDbObjectId EdgeGE::getEndId() const
{
    assertReadEnabled () ;
    return m_endId;
}

static void DrawJoint(AcGiWorldDraw* mode, const AcGePoint3d& pt)
{
    JointGE joint(pt);
    joint.worldDraw(mode);
}
Adesk::Boolean EdgeGE::subWorldDraw(AcGiWorldDraw* mode)
{
    assertReadEnabled () ;
    Adesk::Boolean ret = MineGE::subWorldDraw( mode );
    // 绘制节点
    //if(ret == Adesk::kTrue)
    //{
    //    if(!m_startId.isNull())
    //    {
    //        DrawJoint(mode, m_startPt);
    //    }
    //    if(!m_endId.isNull())
    //    {
    //        DrawJoint(mode, m_endPt);
    //    }
    //}
    return ret;
}
