#pragma once
#include "dlimexp.h"
#include "ArxHelper/Entity.h"
// 矿井系统图元基类(抽象类)
class MINEGE_EXPORT_API MineGE : public BaseEntity
{
public:
    ACRX_DECLARE_MEMBERS( MineGE ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber;
    // 公共接口
public:
    virtual ~MineGE ();
    // 获取类型名称
    CString getTypeName() const;
	// 设置背景填充色
	void setBackground(Adesk::UInt8 r, Adesk::UInt8 g, Adesk::UInt8 b);
	// 清除背景填充色
	void clearBackground();
    // 虚函数--数据操作
protected:
    // 获取图元实际类型(默认返回类的名称)
    virtual CString getRealTypeName() const;
    // 注册要读取的属性数据字段
    virtual void regPropertyDataNames( AcStringArray& names ) const {}
    // MineGE基类负责读取数据,派生类负责将数据填充到自己的成员变量中
    virtual void readPropertyDataFromValues( const AcStringArray& values ) {}
    // 虚函数--图形操作
protected:
    // 绘制图形
    virtual Adesk::Boolean customWorldDraw( AcGiWorldDraw* mode )
    {
        return Adesk::kTrue;
    }
    // 变换操作(移动、选择、镜像)--目前暂不考虑"镜像"操作
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform )
    {
        return Acad::eOk;
    }
    // 捕捉点计算
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const
    {
        return Acad::eOk;
    }
    // 夹点计算
    virtual Acad::ErrorStatus customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
    {
        return Acad::eOk;
    }
    // 夹点编辑
    virtual Acad::ErrorStatus customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
    {
        return Acad::eOk;
    }
    // 计算图形的最小多边形包围盒(用于背景绘制,实现"消隐"效果)
    virtual void minPolygon( AcGePoint3dArray& pts ) {} // 默认实现，什么也不做
public:
    virtual Acad::ErrorStatus dwgOutFields ( AcDbDwgFiler* pFiler ) const;
    virtual Acad::ErrorStatus dwgInFields ( AcDbDwgFiler* pFiler );
    // AcDbEntity的重载函数
protected:
    // 绘制函数额外增加背景色的绘制
    virtual Adesk::Boolean subWorldDraw( AcGiWorldDraw* mode );
    // 变换操作(移动、选择、镜像)--目前暂不考虑"镜像"操作
    virtual Acad::ErrorStatus subTransformBy( const AcGeMatrix3d& xform );
    // 对象捕捉点
    virtual Acad::ErrorStatus subGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    // 夹点计算
    virtual Acad::ErrorStatus subGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const ;
    // 夹点编辑
    virtual Acad::ErrorStatus subMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset ) ;
    // 包围盒重载(用于确定缩放的范围)
    // 一个"紧凑"的3d包围盒(立方体)
    virtual Acad::ErrorStatus subGetGeomExtents( AcDbExtents& extents ) const;
    // new对象并成功提交到数据库之后,初始化可视化效果参数(扩展数据)和数据对象(扩展词典)
    virtual Acad::ErrorStatus subClose( void );
protected:
    // 构造函数
    MineGE();
private:
    // 绘制背景消隐
    void drawBackground( AcGiWorldDraw* mode );
    // 初始化属性数据
    void initPropertyData();
    // 读取属性数据用于后续的计算和绘制
    void readPropertyData() const;
private:
	double m_rgb;      // 背景填充颜色(rgb格式,long类型,用double存储)
};
#ifdef MINEGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( MineGE )
#endif