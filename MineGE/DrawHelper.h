#pragma once
#include "dlimexp.h"
/*
	颜色表
-----------------
1	|	Red		|
-----------------
2	|	Yellow	|
-----------------
3	|	Green	|
-----------------
4	|	Cyan	|
-----------------
5	|	Blue	|
-----------------
6	|	Magenta	|
-----------------
7	|White/Black|
-----------------
*/
// 可视化效果辅助类
class MINEGE_EXPORT_API DrawHelper
{
public:
    // EdgeGE类型图元闭合处理
    static void EdgeGEJunctionClosure( const AcGePoint3d& junctionPt );
    // EdgeGE类型图元闭合处理
    static void EdgeGEJunctionClosure2( const AcDbObjectId& objId );
    // 节点关联处理
    static void JointRelations( const AcGePoint3d& junctionPt, const AcDbObjectId& jointId );
    // 节点关联处理
    static void JointRelations2( const AcDbObjectId& jointId );
    // 分支类图元反向
    // 交换巷道、工作面等图元的始末点位置
    // 同时反向关联的方向类图元,例如风流方向，风门等
    static void ReverseDirection( const AcDbObjectId& objId );
    // 获取当前已注册的MineGE图元类型(用于draw的参数设置)
    static void GetAllRegGETypesForDraw( AcStringArray& allGETypes );
    // 查找指定类型geType的所有图元
    static void FindMineGEs( const CString& geType, AcDbObjectIdArray& objIds );
    // 查找图元的所有关联的标签图元
    static void GetAllTagGEById( const AcDbObjectId& objId, AcDbObjectIdArray& objIds );
    // 查找与图元objId关联的指定类型的标签图元(包括从geType派生的图元类型)
    static void GetTagGEById2( const AcDbObjectId& objId, const CString& geType, AcDbObjectIdArray& objIds );
    // 用一种颜色显示图元的所有标签图元
    static void ShowAllTagGE( const AcDbObjectId& objId, unsigned short colorIndex );
    // 用一种颜色显示标签图元所关联的宿主图元HostGE
    static void ShowHostGE( const AcDbObjectId& objId, unsigned short colorIndex );
    static bool GetHostGE( const AcDbObjectId& objId, AcDbObjectId& host );
    static AcDbObjectId GetRelatedTW( AcDbObjectId objId );
    static AcDbObjectIdArray GetRelatedTunnel( AcDbObjectId tunnelId );

	// 获取EdgeGE图元的始末点坐标
	static void GetEdgePoints( const AcDbObjectIdArray& objIds, AcGePoint3dArray& spts, AcGePoint3dArray& epts );
	static bool GetEdgePoint( const AcDbObjectId& objId, AcGePoint3d& spt, AcGePoint3d& ept );
	static bool SetEdgePoint( const AcDbObjectId& objId, const AcGePoint3d& spt, const AcGePoint3d& ept );

    // 根据坐标查找对应的JointGE图元
    static void GetAllJointGEByPoints(const AcGePoint3dArray& junctionPts, AcDbObjectIdArray& jointIds);
    static AcDbObjectId GetJointGEByPoint(const AcGePoint3d& junctionPt);
    // 根据节点id查找关联的分支巷道
    static void GetRelatedEdgesByJoint( const AcDbObjectIdArray& jointIds, AcDbObjectIdArray& edgeIds);
};