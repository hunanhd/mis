#include "StdAfx.h"
#include "MineGE/Drawtool.h"
#include "DrawSpecial.h"
#define PI 3.1415926535897932384626433832795
void DrawCross( AcGiWorldDraw* mode, const AcGePoint3d& pt, double radius )
{
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v *= radius;
    for( int i = 0; i < 4; i++ )
    {
        AcGePoint3dArray pts;
        pts.append( pt );
        pts.append( pt + v );
        mode->geometry().worldLine( pts.asArrayPtr() );
        v.rotateBy( PI / 2, AcGeVector3d::kZAxis );
    }
}
// 绘制正弦(2个半圆)
void DrawSin( AcGiWorldDraw* mode, const AcGePoint3d& pt, double angle, double radius )
{
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( angle, AcGeVector3d::kZAxis );
    // 绘制第1个半圆
    mode->geometry().circularArc( pt + v * radius, radius, AcGeVector3d::kZAxis, v, PI );
    // 绘制第2个半圆
    // 注：不使用v.negate()方法, 该方法会修改v
    mode->geometry().circularArc( pt + v * radius * 3, radius, AcGeVector3d::kZAxis, -v, PI );
}
void DrawShaft( AcGiWorldDraw* mode, const AcGePoint3d& pt, double radius )
{
    // 绘制外环
    DrawCircle( mode, pt, radius, false );
    // 绘制内环
    DrawArc( mode, pt, radius * 0.8, PI / 6, PI, true );
    DrawArc( mode, pt, radius * 0.8, PI * 7 / 6, PI, false );
}
void DrawWave( AcGiWorldDraw* mode, const AcGePoint3d& pt, double DirecAngle, double startAngle, double length )
{
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( DirecAngle, AcGeVector3d::kZAxis );
    AcGePoint3d leftPt1, leftPt2, rightPt1, rightPt2;
    rightPt2 = pt + v * length * 0.5;
    leftPt2 = pt - v * length * 0.5;
    v.rotateBy( -startAngle, AcGeVector3d::kZAxis );
    rightPt1 = pt + v * length / ( 4 * cos( startAngle ) );
    leftPt1 = pt - v * length / ( 4 * cos( startAngle ) );
    AcGePoint3dArray pts;
    pts.append( leftPt2 );
    pts.append( leftPt1 );
    pts.append( pt );
    pts.append( rightPt1 );
    pts.append( rightPt2 );
    DrawSpline( mode, pts );
}