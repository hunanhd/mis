#include "StdAfx.h"
#include "DrawHelper.h"
#include "EdgeGE.h"
#include "TagGE.h"
#include "JointGE.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"

#include <map>
/* 全局函数(实现在EdgeJunctionClosure.cpp) */
extern void EdgeGEJunctionClosure_Helper( const AcGePoint3d& junctionPt );
/* 全局函数(实现在JointRelations.cpp) */
extern void JointRelations_Helper( const AcGePoint3d& junctionPt, const AcDbObjectId& jointId );
// 构造坐标->节点的词典
typedef std::map<CString, AcDbObjectId> PointIdMap;
// 节点--分支词典
typedef std::map<CString, AcDbObjectIdArray*> JointEdgeMap;

static MineGE* CreateGEByType( const CString& geType )
{
    AcRxObject* pClass = acrxClassDictionary->at( geType );
    if( pClass == 0 ) return 0;
    AcRxObject* pObj = AcRxClass::cast( pClass )->create();
    MineGE* pGE = MineGE::cast( pObj );
    if( pGE == 0 )
    {
        delete pObj;
        return 0;
    }
    return pGE;
}
void DrawHelper::EdgeGEJunctionClosure( const AcGePoint3d& junctionPt )
{
    EdgeGEJunctionClosure_Helper( junctionPt );
}
static void UpdateEdgeGE( const AcDbObjectIdArray& objIds )
{
    int len = objIds.length();
    for( int i = 0; i < len; i++ )
    {
        DrawHelper::EdgeGEJunctionClosure2( objIds[i] );
    }
}
void DrawHelper::EdgeGEJunctionClosure2( const AcDbObjectId& objId )
{
    AcDbEntity* pEnt;
    if( Acad::eOk != acdbOpenAcDbEntity( pEnt, objId, AcDb::kForRead ) ) return;
    EdgeGE* pEdge = EdgeGE::cast( pEnt );
    if( pEdge == 0 )
    {
        pEnt->close();
        return;
    }
    AcGePoint3d startPt, endPt;
    pEdge->getSEPoint( startPt, endPt );
    pEdge->close();
    EdgeGEJunctionClosure( startPt );
    EdgeGEJunctionClosure( endPt );
}
void DrawHelper::GetAllRegGETypesForDraw( AcStringArray& allGETypes )
{
    ArxClassHelper::GetArxClassTypes( _T( "MineGE" ), allGETypes, false );
}
//static void printId(const CString& msg, const AcDbObjectIdArray& objIds)
//{
//	acutPrintf(_T("%s\n"), msg);
//	int len=objIds.length();
//	for(int i=0;i<len;i++)
//	{
//		acutPrintf(_T("\t--%d\n"), objIds[i]);
//	}
//	acutPrintf(_T("\n"));
//}
void DrawHelper::FindMineGEs( const CString& geType, AcDbObjectIdArray& objIds )
{
    AcRxClass* pClass = AcRxClass::cast( acrxClassDictionary->at( geType ) );
    if( pClass == 0 ) return; // 该类型图元尚未在CAD中注册
    // 查找所有geType及派生类的图元
    ArxDataTool::GetEntsByType( geType, objIds, true );
}
static void GetAllTagGEById_Helper( const AcDbObjectId& objId, const AcDbObjectIdArray& tagObjIds, AcDbObjectIdArray& objIds )
{
    if( tagObjIds.isEmpty() ) return;
    //acutPrintf(_T("\n找到%d个图元，进行比较判断..."), tagObjIds.length());
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    int len = tagObjIds.length();
    for( int i = 0; i < len; i++ )
    {
        AcDbObject* pObj;
        if( Acad::eOk != pTrans->getObject( pObj, tagObjIds[i], AcDb::kForRead ) ) continue;
        TagGE* pTag = TagGE::cast( pObj );
        if( pTag == 0 ) continue;
        if( pTag->getRelatedGE() == objId )
        {
            objIds.append( tagObjIds[i] );
        }
    }
    actrTransactionManager->endTransaction();
}
void DrawHelper::GetAllTagGEById( const AcDbObjectId& objId, AcDbObjectIdArray& objIds )
{
    if( objId.isNull() ) return;
    //acutPrintf(_T("\n查找图元..."));
    AcDbObjectIdArray tagObjIds;
    ArxDataTool::GetEntsByType( _T( "TagGE" ), tagObjIds, true );
    // 筛选过滤出与ObjId关联的tag
    GetAllTagGEById_Helper( objId, tagObjIds, objIds );
    //acutPrintf(_T("\n剩余%d个图元..."), objIds.length());
}
static void GetTagGEById2_Helper( const CString& geType, const AcDbObjectIdArray& allObjIds, AcDbObjectIdArray& objIds )
{
    AcRxClass* pClass = AcRxClass::cast( acrxClassDictionary->at( geType ) );
    if( pClass == 0 ) return;
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    int len = allObjIds.length();
    for( int i = 0; i < len; i++ )
    {
        AcDbObject* pObj;
        if( Acad::eOk != pTrans->getObject( pObj, allObjIds[i], AcDb::kForRead ) ) continue;
        if( pObj->isKindOf( pClass ) )
        {
            objIds.append( allObjIds[i] );
        }
    }
    actrTransactionManager->endTransaction();
}
void DrawHelper::GetTagGEById2( const AcDbObjectId& objId, const CString& geType, AcDbObjectIdArray& objIds )
{
    // 查找图元ObjId关联的所有标签
    AcDbObjectIdArray allObjIds;
    DrawHelper::GetAllTagGEById( objId, allObjIds );
    if( allObjIds.isEmpty() ) return;
    // 筛选出类型为geType的tag
    GetTagGEById2_Helper( geType, allObjIds, objIds );
}
void DrawHelper::ReverseDirection( const AcDbObjectId& objId )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    AcDbObject* pObj;
    if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForWrite ) )
    {
        actrTransactionManager->abortTransaction();
        return;
    }
    EdgeGE* pEdge = EdgeGE::cast( pObj );
    if( pEdge == 0 )
    {
        actrTransactionManager->abortTransaction();
        return;
    }
    pEdge->reverse();  // 交换始末点坐标
    double angle = pEdge->getAngle(); // 反向后的角度

    // 调整所有方向类图元方向，包括风流方向
    AcDbObjectIdArray objIds;
    DrawHelper::GetTagGEById2( objId, _T( "DirGE" ), objIds );
    int len = objIds.length();
    for( int i = 0; i < len; i++ )
    {
        if( Acad::eOk != pTrans->getObject( pObj, objIds[i], AcDb::kForWrite ) ) continue;
        DirGE* pDir = DirGE::cast( pObj );
        pDir->setDirectionAngle( angle );
    }
    actrTransactionManager->endTransaction();
}
bool DrawHelper::GetHostGE( const AcDbObjectId& objId, AcDbObjectId& host )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return false;
    AcDbObject* pObj;
    if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForRead ) )
    {
        actrTransactionManager->abortTransaction();
        return false;
    }
    TagGE* pTag = TagGE::cast( pObj );
    if( pTag == 0 )
    {
        actrTransactionManager->abortTransaction();
        return false;
    }
    host = pTag->getRelatedGE();
    actrTransactionManager->endTransaction();
    return true;
}
static void PrintMsg( int n )
{
    CString msg;
    msg.Format( _T( "该图元包含%d个标签图元" ), n );
    AfxMessageBox( msg );
}
void DrawHelper::ShowAllTagGE( const AcDbObjectId& objId, unsigned short colorIndex )
{
    AcDbObjectIdArray objIds;
    DrawHelper::GetAllTagGEById( objId, objIds );
    PrintMsg( objIds.length() );
    if( objIds.isEmpty() ) return;
    // 记录原来的颜色
    AcArray<Adesk::UInt16> colors;
    if( !ArxEntityHelper::GetEntitiesColor( objIds, colors ) ) return;
    // 用黄色高亮显示标签图元
    ArxEntityHelper::SetEntitiesColor( objIds, colorIndex );
    // 中断
    ArxUtilHelper::Pause();
    // 恢复原有颜色
    ArxEntityHelper::SetEntitiesColor2( objIds, colors );
}
void DrawHelper::ShowHostGE( const AcDbObjectId& objId, unsigned short colorIndex )
{
    // 获取标签图元关联的宿主
    AcDbObjectId host;
    if( !GetHostGE( objId, host ) ) return;
    AcDbObjectIdArray objIds;
    objIds.append( host );
    // 记录宿主图元的原颜色
    AcArray<Adesk::UInt16> colors;
    if( !ArxEntityHelper::GetEntitiesColor( objIds, colors ) ) return;
    // 用黄色高亮显示宿主图元
    ArxEntityHelper::SetEntitiesColor( objIds, colorIndex );
    // 中断
    ArxUtilHelper::Pause();
    // 恢复宿主的原有颜色
    ArxEntityHelper::SetEntitiesColor2( objIds, colors );
}
AcDbObjectId DrawHelper::GetRelatedTW( AcDbObjectId objId )
{
    AcDbObjectId tWorkId;
    AcDbObjectIdArray objIds;
    DrawHelper::FindMineGEs( _T( "EdgeGE" ), objIds );
    int len = objIds.length();
    AcGePoint3d spt, ept;
    //获取选择巷道的始末节点
    AcDbObject* pObj;
    acdbOpenObject( pObj, objId, AcDb::kForRead );
    EdgeGE* pEdge = EdgeGE::cast( pObj );
    pEdge->getSEPoint( spt, ept );
    pObj->close();
    AcGePoint3dArray findedPts;
    findedPts.append( spt );
    findedPts.append( ept );
    for ( int i = 0; i < len; i++ )
    {
        bool isTTunnel = ArxUtilHelper::IsEqualType( _T( "TTunnel" ), objIds[i] );
        //获取其他巷道的始末节点
        AcDbObject* pObj;
        acdbOpenObject( pObj, objIds[i], AcDb::kForRead );
        EdgeGE* pEdge = EdgeGE::cast( pObj );
        pEdge->getSEPoint( spt, ept );
        pObj->close();

        //acutPrintf(_T("\n始节点:(%f,%f),末节点(%f,%f)"),spt.x,spt.y,ept.x,ept.y);
        if ( findedPts.contains( spt ) && findedPts.contains( ept ) )
        {
            continue;
        }
        else if ( findedPts.contains( spt ) && !isTTunnel )
        {
            findedPts.append( ept );
            i = -1;
        }
        else if ( findedPts.contains( ept ) && !isTTunnel )
        {
            findedPts.append( spt );
            i = -1;
        }
        else if ( ( findedPts.contains( spt ) || findedPts.contains( ept ) ) && isTTunnel )
        {
            tWorkId = objIds[i];
            break;
        }
    }
    return tWorkId;
}
static void GetSEPointById( const AcDbObjectId& objId, AcGePoint3d& spt, AcGePoint3d& ept )
{
    AcTransaction* pTran = actrTransactionManager->startTransaction();
    if ( 0 == pTran ) return;
    AcDbObject* pObj;
    if ( Acad::eOk != pTran->getObject( pObj, objId, AcDb::kForRead ) ) return;
    EdgeGE* pEdge = EdgeGE::cast( pObj );
    pEdge->getSEPoint( spt, ept );
    actrTransactionManager->endTransaction();
}
AcDbObjectIdArray DrawHelper::GetRelatedTunnel( AcDbObjectId ttunnelId )
{
    AcDbObjectIdArray objIds;
    DrawHelper::FindMineGEs( _T( "EdgeGE" ), objIds );
    int len = objIds.length();
    //acutPrintf(_T("\nEdgeGE的数目：%d"),len);
    AcGePoint3d ttunnelSpt, ttunnelEpt;
    //获取选择工作面的始末节点
    GetSEPointById( ttunnelId, ttunnelSpt, ttunnelEpt );
    AcDbObjectIdArray tunnelsRetIds;
    //AcGePoint3d pt = ttunnelSpt;
    AcGePoint3dArray findedPts;
    findedPts.append( ttunnelSpt );
    for ( int i = 0; i < len; i++ )
    {
        AcGePoint3d tunnelSpt, tunnelEpt;
        GetSEPointById( objIds[i], tunnelSpt, tunnelEpt );
        if ( findedPts.contains( tunnelSpt ) && !findedPts.contains( tunnelEpt ) )
        {
            findedPts.append( tunnelEpt );
            i = -1;
        }
        else if ( findedPts.contains( tunnelEpt ) && !findedPts.contains( tunnelSpt ) )
        {
            findedPts.append( tunnelSpt );
            i = -1;
        }
    }
    for ( int i = 0; i < len; i++ )
    {
        AcGePoint3d tunnelSpt, tunnelEpt;
        GetSEPointById( objIds[i], tunnelSpt, tunnelEpt );
        if ( ( findedPts.contains( tunnelSpt ) || findedPts.contains( tunnelEpt ) ) )
        {
            tunnelsRetIds.append( objIds[i] );
        }
    }
    return tunnelsRetIds;
}

void DrawHelper::GetEdgePoints( const AcDbObjectIdArray& objIds, AcGePoint3dArray& spts, AcGePoint3dArray& epts )
{
	AcTransaction* pTrans = actrTransactionManager->startTransaction();
	if( pTrans == 0 ) return;

	int len = objIds.length();
	for( int i = 0; i < len; i++ )
	{
		AcDbObject* pObj;
		if( Acad::eOk != pTrans->getObject( pObj, objIds[i], AcDb::kForRead ) ) continue;

		EdgeGE* pEdge = EdgeGE::cast( pObj );
		if( pEdge == 0 ) continue;

		AcGePoint3d spt, ept;
		pEdge->getSEPoint( spt, ept );

		spts.append( spt );
		epts.append( ept );
	}
	actrTransactionManager->endTransaction();
}

bool DrawHelper::GetEdgePoint( const AcDbObjectId& objId, AcGePoint3d& spt, AcGePoint3d& ept )
{
	AcTransaction* pTrans = actrTransactionManager->startTransaction();
	if( pTrans == 0 ) return false;

	AcDbObject* pObj;
	if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForRead ) )
	{
		actrTransactionManager->abortTransaction();
		return false;
	}
	EdgeGE* pEdge = EdgeGE::cast( pObj );
	if( pEdge == 0 )
	{
		actrTransactionManager->abortTransaction();
		return false;
	}
	pEdge->getSEPoint( spt, ept );
	actrTransactionManager->endTransaction();

	return true;
}

bool DrawHelper::SetEdgePoint( const AcDbObjectId& objId, const AcGePoint3d& spt, const AcGePoint3d& ept )
{
	AcTransaction* pTrans = actrTransactionManager->startTransaction();
	if( pTrans == 0 ) return false;
	AcDbObject* pObj;
	if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForWrite ) )
	{
		actrTransactionManager->abortTransaction();
		return false;
	}
	EdgeGE* pEdge = EdgeGE::cast( pObj );
	if( pEdge == 0 )
	{
		actrTransactionManager->abortTransaction();
		return false;
	}
	pEdge->setSEPoint( spt, ept );
	pEdge->update();
	actrTransactionManager->endTransaction();
	return true;
}
void DrawHelper::JointRelations(const AcGePoint3d& junctionPt, const AcDbObjectId& jointId)
{
    JointRelations_Helper(junctionPt, jointId);
}
static bool GetJointPt(const AcDbObjectId& jointId, AcGePoint3d& junctionPt)
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return false;

    AcDbObject* pObj;
    if( Acad::eOk != pTrans->getObject( pObj, jointId, AcDb::kForRead ) )
    {
        actrTransactionManager->abortTransaction();
        return false;
    }
    JointGE* pJoint = JointGE::cast( pObj );
    if( pJoint == 0 )
    {
        actrTransactionManager->abortTransaction();
        return false;
    }

    junctionPt = pJoint->getInsertPt();
    actrTransactionManager->endTransaction();
    return true;
}
void DrawHelper::JointRelations2(const AcDbObjectId& jointId)
{
    AcGePoint3d junctionPt;
    if( !GetJointPt(jointId, junctionPt) ) return;
    JointRelations_Helper(junctionPt, jointId);
}
// 使用事务
static void BuildJointMap_Trans(const AcDbObjectIdArray& allObjIds, PointIdMap& jm)
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    for(int i=0;i<allObjIds.length();i++)
    {
        AcDbObjectId objId = allObjIds[i];
        acutPrintf(_T("\nid:%s"), ArxUtilHelper::ObjectIdToStr(objId));
        AcDbObject* pObj;
        if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForRead ) )
        {
            //actrTransactionManager->abortTransaction();
            continue;
        }
        JointGE* pJoint = JointGE::cast( pObj );
        if( pJoint == 0 )
        {
            //actrTransactionManager->abortTransaction();
            continue;
        }
        CString ptStr = ArxUtilHelper::Point3dToString(pJoint->getInsertPt());
        acutPrintf(_T("\n-->%s:%s"), ptStr, ArxUtilHelper::ObjectIdToStr(pJoint->objectId()));
        jm.insert(PointIdMap::value_type(ptStr, pJoint->objectId()));
        actrTransactionManager->endTransaction();
    }
}

// 不使用事务!
static void BuildJointMap(const AcDbObjectIdArray& allObjIds, PointIdMap& jm)
{
    for(int i=0;i<allObjIds.length();i++)
    {
        AcDbObjectId objId = allObjIds[i];
        AcDbEntity* pEnt;
        if( Acad::eOk != acdbOpenAcDbEntity( pEnt, objId, AcDb::kForRead ) ) continue;

        JointGE* pJoint = JointGE::cast( pEnt );
        if( pJoint != 0 ) 
        {
            CString ptStr = ArxUtilHelper::Point3dToString(pJoint->getInsertPt());
            //acutPrintf(_T("\n-->%s:%s"), ptStr, ArxUtilHelper::ObjectIdToStr(pJoint->objectId()));
            jm.insert(PointIdMap::value_type(ptStr, pJoint->objectId()));
        }
        pEnt->close();
    }
}

void DrawHelper::GetAllJointGEByPoints(const AcGePoint3dArray& junctionPts, AcDbObjectIdArray& objIds)
{
    // 查找所有节点图元
    AcDbObjectIdArray allObjIds;
    ArxDataTool::GetEntsByType( _T( "JointGE" ), allObjIds, true );
    // 构造坐标-->节点的词典
    PointIdMap jm;
    // 注: 该函数不能使用事务,只能使用open/close机制,原因未知!
    BuildJointMap(allObjIds, jm);
    // 查找对应的节点图元
    for(int i=0;i<junctionPts.length();i++)
    {
        CString ptStr = ArxUtilHelper::Point3dToString(junctionPts[i]);
        PointIdMap::iterator itr = jm.find(ptStr);
        if(itr != jm.end())
        {
            objIds.append(itr->second);
        }
        else
        {
            objIds.append(AcDbObjectId::kNull);
        }
    }
}

void DrawHelper::GetRelatedEdgesByJoint(const AcDbObjectIdArray& jointIds, AcDbObjectIdArray& edgeIds)
{
    // 查找所有分支图元
    AcDbObjectIdArray allObjIds;
    ArxDataTool::GetEntsByType( _T( "EdgeGE" ), allObjIds, true );

    JointEdgeMap jem;

    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;

    int len = allObjIds.length();
    for ( int i = 0; i < len; i++ )
    {
        AcDbObject* pObj = 0;
        if( Acad::eOk == pTrans->getObject( pObj, allObjIds[i], AcDb::kForRead ) )
        {
            EdgeGE* pEdge = EdgeGE::cast( pObj );
            if ( pEdge != 0 )
            {
                CString startId = ArxUtilHelper::ObjectIdToStr(pEdge->getStartId());
                CString endId = ArxUtilHelper::ObjectIdToStr(pEdge->getEndId());
                if(jem.find(startId) == jem.end())
                {
                    AcDbObjectIdArray* a = new AcDbObjectIdArray();
                    jem.insert(JointEdgeMap::value_type(startId, a));
                }
                jem[startId]->append(pEdge->objectId());

                if(jem.find(endId) == jem.end())
                {
                    AcDbObjectIdArray* a = new AcDbObjectIdArray();
                    jem.insert(JointEdgeMap::value_type(endId, a));
                }
                jem[endId]->append(pEdge->objectId());
            }
        }
    }
    actrTransactionManager->endTransaction();

    for(int i=0;i<jointIds.length();i++)
    {
        CString idStr = ArxUtilHelper::ObjectIdToStr(jointIds[i]);
        if(jem.find(idStr) != jem.end())
        {
            AcDbObjectIdArray& ids = *(jem[idStr]);
            for(int j=0;j<ids.length();j++)
            {
                edgeIds.append(ids[j]);
            }
        }
    }

    // 回收内存
    for(JointEdgeMap::iterator itr=jem.begin();itr!=jem.end();++itr)
    {
        delete itr->second;
    }
    jem.clear();
}

AcDbObjectId DrawHelper::GetJointGEByPoint(const AcGePoint3d& junctionPt)
{
    AcDbObjectIdArray objIds;
    AcGePoint3dArray pts;
    pts.append(junctionPt);
    DrawHelper::GetAllJointGEByPoints(pts, objIds);
    return objIds[0];
}
