#pragma once
#include "dlimexp.h"
// ����ͼ�λ���
extern MINEGE_EXPORT_API void DrawCross( AcGiWorldDraw* mode, const AcGePoint3d& pt, double radius );
extern MINEGE_EXPORT_API void DrawSin( AcGiWorldDraw* mode, const AcGePoint3d& pt, double angle, double radius );
extern MINEGE_EXPORT_API void DrawShaft( AcGiWorldDraw* mode, const AcGePoint3d& pt, double radius );
extern MINEGE_EXPORT_API void DrawWave( AcGiWorldDraw* mode, const AcGePoint3d& pt, double DirecAngle, double startAngle, double length );
