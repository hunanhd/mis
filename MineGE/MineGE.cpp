#include "StdAfx.h"
#include "MineGE.h"
#include "config.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
static void InitPropertyDatas( const AcDbObjectId& dictId, const CString& type )
{
    AcStringArray hybirdFields;
    FieldHelper::GetAllFields( type, hybirdFields );
    if( hybirdFields.isEmpty() ) return;
    BaseDao* dao = DictData::CreateDao( dictId );
    if( dao == 0 ) return;
    for( int i = 0; i < hybirdFields.length(); i++ )
    {
        dao->addField( hybirdFields[i].kACharPtr() );
    }
    delete dao;
}
static bool GetPropertyDatas( const AcDbObjectId& dictId, const AcStringArray& names, AcStringArray& values )
{
    BaseDao* dao = DictData::CreateDao( dictId );
    if( dao == 0 ) return false;
    int len = names.length();
    for( int i = 0; i < len; i++ )
    {
        CString value;
        bool ret = dao->getString( names[i].kACharPtr(), value );
        values.append( value );
    }
    delete dao;
    return true;
}
Adesk::UInt32 MineGE::kCurrentVersionNumber = 1 ;
// 有修改，使得MineGE成为抽象类
ACRX_NO_CONS_DEFINE_MEMBERS ( MineGE, BaseEntity )
MineGE::MineGE() : m_rgb(0)
{
	// 获取cad绘图窗口背景色
	Adesk::UInt8 r, g, b;
	if( ArxUtilHelper::GetBackgroundColor( r, g, b ) )
	{
		m_rgb = (double)ArxUtilHelper::RGB_2_LONG(r, g, b);
	}
	// 关联字段
	map(_T("填充"), &m_rgb);
}
MineGE::~MineGE ()
{
}
CString MineGE::getTypeName() const
{
    return this->getRealTypeName();
}
CString MineGE::getRealTypeName() const
{
    // 返回类型名称
    // 使用了虚函数方法isA()
    return this->isA()->name();
}
void MineGE::initPropertyData()
{
    //assertWriteEnabled();
    if( !extensionDictionary().isNull() )
    {
        return;
    }
    else
    {
        Acad::ErrorStatus es = upgradeOpen();
        if( es == Acad::eOk || es == Acad::eWasOpenForWrite )
        {
            createExtensionDictionary();
            InitPropertyDatas( extensionDictionary(), getTypeName() );// 扩展词典作为数据对象
        }
        if( es == Acad::eOk )
        {
            downgradeOpen();
        }
    }
}
void MineGE::readPropertyData() const
{
    assertReadEnabled();
    // 读取要查询的字段名称集合
    AcStringArray fields;
    this->regPropertyDataNames( fields );
    if( fields.isEmpty() ) return;
    // 猜测完整的字段名
    AcStringArray hybirdFields;
    FieldHelper::GuessRealFields( this->getTypeName(), fields, hybirdFields );
    // 查询数据，并写入到values中
    AcStringArray values;
    if( !GetPropertyDatas( this->extensionDictionary(), hybirdFields, values ) )
    {
        for( int i = 0; i < fields.length(); i++ )
        {
            // 如果获取数据失败，添加与names等长的空字符串
            values.append( _T( "" ) );
        }
    }
    // 将查询到的数据返回到pGEDraw(强制去掉this的const属性)
    ( ( MineGE* )this )->readPropertyDataFromValues( values );
}
Acad::ErrorStatus MineGE::dwgOutFields( AcDbDwgFiler* pFiler ) const
{
    //acutPrintf(_T("\nid:%d call MineGE::dwgOutFields()..."), objectId());
    assertReadEnabled () ;
    Acad::ErrorStatus es = BaseEntity::dwgOutFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    if ( ( es = pFiler->writeUInt32 ( MineGE::kCurrentVersionNumber ) ) != Acad::eOk )
        return ( es ) ;
    pFiler->writeItem( m_rgb );
    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus MineGE::dwgInFields( AcDbDwgFiler* pFiler )
{
    //acutPrintf(_T("\nid:%d call MineGE::dwgInFields()..."), objectId());
    assertWriteEnabled () ;
    Acad::ErrorStatus es = BaseEntity::dwgInFields ( pFiler );
    if ( es != Acad::eOk )
        return ( es ) ;
    Adesk::UInt32 version = 0 ;
    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
        return ( es ) ;
    if ( version > MineGE::kCurrentVersionNumber )
        return ( Acad::eMakeMeProxy ) ;
	pFiler->readItem( &m_rgb );
    return ( pFiler->filerStatus () ) ;
}
void MineGE::drawBackground( AcGiWorldDraw* mode )
{
    AcGePoint3dArray pts;
    this->minPolygon( pts );
    // 用户没有定义边界
    if( pts.isEmpty() ) return;
    //// 获取cad绘图窗口背景色
    //Adesk::UInt8 r, g, b;
    //if( !ArxUtilHelper::GetBackgroundColor( r, g, b ) ) return;
    AcGiSubEntityTraits& traits = mode->subEntityTraits();
    // 保存原有的属性
    Adesk::UInt16 cl = traits.color();
    AcGiFillType ft = traits.fillType();
	// ACI颜色转换成RGB
	Adesk::UInt8 r, g, b;
	ArxUtilHelper::LONG_2_RGB((Adesk::UInt32)m_rgb, r, g, b);
    AcCmEntityColor bgColor( r, g, b );
    traits.setTrueColor( bgColor );
    traits.setFillType( kAcGiFillAlways ); // 填充
    //acutPrintf(_T("\n颜色索引：%d"), bgColor.colorIndex());
    mode->geometry().polygon( pts.length(), pts.asArrayPtr() );
    // 恢复属性
    traits.setFillType( ft );
    traits.setColor( cl );
}
Adesk::Boolean MineGE::subWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    // 1、读取属性数据
    readPropertyData();
    // 2、绘制背景块
    // 该方法在动态效果中可能会有一些问题,特别是jig
    // 猜测原因:绘制填充的多边形可能比较慢
    //drawBackground( mode );
    // 3、绘制图形
    this->customWorldDraw( mode );
    return BaseEntity::subWorldDraw( mode );
}
Acad::ErrorStatus MineGE::subTransformBy( const AcGeMatrix3d& xform )
{
    // 1、读取属性数据
    readPropertyData();
    // 2、调用派生类函数
    return this->customTransformBy( xform );
}
Acad::ErrorStatus MineGE::subGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 1、读取属性数据
    readPropertyData();
    // 2、调用派生类函数
    return this->customGetOsnapPoints( osnapMode, gsSelectionMark, pickPoint, lastPoint, viewXform, snapPoints, geomIds );
}
Acad::ErrorStatus MineGE::subGetGripPoints ( AcGePoint3dArray& gripPoints,
        AcDbIntArray& osnapModes,
        AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 1、读取属性数据
    readPropertyData();
    // 2、调用派生类函数
    return this->customGetGripPoints( gripPoints, osnapModes, geomIds );
}
Acad::ErrorStatus MineGE::subMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    // 1、读取属性数据
    readPropertyData();
    // 2、调用派生类函数
    return this->customMoveGripPointsAt( indices, offset );
}
Acad::ErrorStatus MineGE::subGetGeomExtents( AcDbExtents& extents ) const
{
    assertReadEnabled () ;
    // 1、更新参数到MineGE中
    //updateDrawParam( false );
    Acad::ErrorStatus es = this->getGeomExtents( extents );
    // Draw没有重载实现subGetGeomExtents
    if( Acad::eOk != es )
    {
        //acutPrintf(_T("\n使用背景消隐多边形计算缩放区域...\n"));
        // 使用caclBackGroundMinPolygon()方法计算的多边形代替
        AcGePoint3dArray pts;
        ( ( MineGE* )this )->minPolygon( pts );
        if( pts.isEmpty() )
        {
            es = Acad::eInvalidExtents;
        }
        else
        {
            int len = pts.length();
            for( int i = 0; i < len; i++ )
            {
                extents.addPoint( pts[i] );
            }
            es = Acad::eOk;
        }
    }
    return es;
}
// cad会频繁的调用subClose
Acad::ErrorStatus MineGE::subClose( void )
{
    //acutPrintf(_T("\nid:%d call MineGE::subClose()...\n"), objectId());
    Acad::ErrorStatus es = BaseEntity::subClose () ;
    // new对象并成功提交到数据库之后
    // 初始化可视化效果参数(扩展数据)
    // 构造数据对象(扩展词典)
    if( es == Acad::eOk )
    {
        initPropertyData();
    }
    return es;
}
void MineGE::clearBackground()
{
	assertWriteEnabled();
	Adesk::UInt8 r, g, b;
	if( ArxUtilHelper::GetBackgroundColor( r, g, b ) )
	{
		m_rgb = (double)ArxUtilHelper::RGB_2_LONG(r, g, b);
	}
}
void MineGE::setBackground(Adesk::UInt8 r, Adesk::UInt8 g, Adesk::UInt8 b)
{
	assertWriteEnabled();
	m_rgb = (double)ArxUtilHelper::RGB_2_LONG(r, g, b);
}
