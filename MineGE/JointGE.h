#pragma once
#include "MineGE.h"
class MINEGE_EXPORT_API JointGE : public MineGE
{
public:
    ACRX_DECLARE_MEMBERS( JointGE ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber;
public:
    JointGE();
    JointGE( const AcGePoint3d& insertPt );
public:
    // 获取插入点坐标
    AcGePoint3d getInsertPt() const;
    // 设置插入点坐标
    void setInsertPt( const AcGePoint3d& pt );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints(
        AcGePoint3dArray& gripPoints,
        AcDbIntArray& osnapModes,
        AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
public:
    virtual Acad::ErrorStatus dwgOutFields ( AcDbDwgFiler* pFiler ) const;
    virtual Acad::ErrorStatus dwgInFields ( AcDbDwgFiler* pFiler );

protected:
    virtual Acad::ErrorStatus subTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus subMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
    virtual Acad::ErrorStatus subErase( Adesk::Boolean erasing );	// 处理删除节点时的闭合

public:
    // key param
    AcGePoint3d m_insertPt;  // 插入点坐标
    double m_radius;  // 半径
};
#ifdef MINEGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( JointGE )
#endif