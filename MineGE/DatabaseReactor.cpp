#include "StdAfx.h"
#include "DatabaseReactor.h"
#include "TagGE.h"
#include "EdgeGE.h"
#include "JointGE.h"
#include "DrawHelper.h"
#include "HelperClass.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
DatabaseReactor::DatabaseReactor ( AcDbDatabase* pDb ) : AcDbDatabaseReactor(), mpDatabase( pDb )
{
    if ( pDb )
    {
        //acutPrintf(_T("\nMineGEErase_DbReactor : %ld"), (long)pDb);
        pDb->addReactor ( this ) ;
    }
}
DatabaseReactor::~DatabaseReactor ()
{
    Detach () ;
}
void DatabaseReactor::Attach ( AcDbDatabase* pDb )
{
    Detach () ;
    if ( mpDatabase == NULL )
    {
        if ( ( mpDatabase = pDb ) != NULL )
            pDb->addReactor ( this ) ;
    }
}
void DatabaseReactor::Detach ()
{
    if ( mpDatabase )
    {
        mpDatabase->removeReactor ( this ) ;
        mpDatabase = NULL ;
    }
}
AcDbDatabase* DatabaseReactor::Subject () const
{
    return ( mpDatabase ) ;
}
bool DatabaseReactor::IsAttached () const
{
    return ( mpDatabase != NULL ) ;
}
static void FilterTagGE( const AcDbObjectId& objId,
                         const AcDbObjectIdArray& allObjIds,
                         Adesk::Boolean pErased,
                         AcDbObjectIdArray& objIds )
{
    if( allObjIds.isEmpty() ) return;
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    int len = allObjIds.length();
    for( int i = 0; i < len; i++ )
    {
        AcDbObject* pObj;
        if( Acad::eOk != pTrans->getObject( pObj, allObjIds[i], AcDb::kForRead, !pErased ) ) continue;
        TagGE* pTag = TagGE::cast( pObj );
        if( pTag == 0 ) continue;
        if( pTag->getRelatedGE() == objId )
        {
            objIds.append( allObjIds[i] );
        }
    }
    actrTransactionManager->endTransaction();
}
static void GetEntsByType_Helper( const CString& type, Adesk::Boolean pErased, AcDbObjectIdArray& allObjIds )
{
    if( pErased )
    {
        ArxDataTool::GetEntsByType( type, allObjIds, true );
    }
    else
    {
        ArxDataTool::GetErasedEntsByType( type, allObjIds, true );
    }
}
static void GetAllTagGEById( const AcDbObjectId& objId, Adesk::Boolean pErased, AcDbObjectIdArray& objIds )
{
    //acutPrintf(_T("\n开始获取id..."));
    if( objId.isNull() ) return;
    AcDbObjectIdArray allObjIds;
    GetEntsByType_Helper( _T( "TagGE" ), pErased, allObjIds );
    // 过滤筛选出于objId关联的TagGE
    FilterTagGE( objId, allObjIds, pErased, objIds );
    //acutPrintf(_T("\n结束获取id..."));
}
static void EraseAllTagGE( const AcDbObjectId& objId, Adesk::Boolean pErased )
{
    AcDbObjectIdArray objIds;
    GetAllTagGEById( objId, pErased, objIds );
    ArxEntityHelper::EraseObjects2( objIds, pErased );
}
void DatabaseReactor::objectErased( const AcDbDatabase* dwg, const AcDbObject* dbObj, Adesk::Boolean pErased )
{
    AcDbDatabaseReactor::objectErased ( dwg, dbObj, pErased );
    //acutPrintf(_T("\n开始 ==> 数据库监视：id:%ld  %s"), dbObj->objectId(), (pErased?_T("删除"):_T("反删除")));
    if( !pErased ) return;
    // 查找图元需要遍历整个cad数据库，因此效率较低
    // 因此，一定要进行判断，否者会影响到正常图元(例如删除1000条直线)的删除
    if( dbObj->isKindOf( MineGE::desc() ) )
    {
        // 删除图元关联的所有TagGE
        EraseAllTagGE( dbObj->objectId(), pErased );
    }
    //acutPrintf(_T("\n结束 ==> 数据库监视：id:%ld  %s"), dbObj->objectId(), (pErased?_T("删除"):_T("反删除")));
}
// 从DictDao复制过来进行修改,因为在反应器中不能使用事务,
// 所以DictPointerDao传入的是一个词典指针, 而不是一个id
class DictPointerDao
{
public:
    // fix: 此时不需要自动创建新的key,否则cad会崩溃!
    DictPointerDao( AcDbDictionary* pDict ) : m_pDict( pDict ), m_createNewKey( false )
    {
    }
    ~DictPointerDao()
    {
    }
    virtual void clear()
    {
        if( m_pDict == 0 ) return;
        //清空
        //pDict->erase();
        AcStringArray names;
        AcDbDictionaryIterator* itr = m_pDict->newIterator();
        while( !itr->done() )
        {
            names.append( itr->name() );
            itr->next();
        }
        delete itr;
        for( int i = 0; i < names.length(); i++ )
        {
            m_pDict->remove( names[i].kACharPtr() );
        }
    }
    virtual bool set( const CString& key, const CString& value )
    {
        if( m_pDict == 0 ) return false;
        AcDbXrecord* pXrec = 0;
        // key不存在或者其它原因
        Acad::ErrorStatus es = m_pDict->getAt( key, ( AcDbObject*& ) pXrec, AcDb::kForWrite );
        if( Acad::eOk != es && Acad::eKeyNotFound != es )
        {
            return false;
        }
        if( Acad::eKeyNotFound == es )
        {
            if( m_createNewKey )
            {
                pXrec = new AcDbXrecord();
                AcDbObjectId xrecObjId;
                if( Acad::eOk != m_pDict->setAt( key, pXrec, xrecObjId ) )
                {
                    delete pXrec;
                    return false;
                }
                else
                {
                    // 在事务里新增的对象,必须要绑定到当前的transaction中,否则得到的指针无效!!!
                    // 参见: ObjectARX Developer's Guide -> Advance Topics -> Transaction Management -> Newly Created Objects and Transactions
                    if( Acad::eOk != actrTransactionManager->addNewlyCreatedDBRObject( pXrec ) )
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }
        ArxXRecordManager dem( pXrec, true );
        CString temp_value;
        if( !dem.getEntry( FIRST_ENTRY, temp_value ) )
        {
            return ( dem.addEntry( value ) != INVALID_ENTRY );
        }
        else
        {
            return dem.modifyEntry( FIRST_ENTRY, value ); // 每个XRECORD只存放一个数据
        }
    }
    virtual bool get( const CString& key, CString& value )
    {
        if( m_pDict == 0 ) return false;
        AcDbXrecord* pXrec = 0;
        // key不存在或者其它原因
        Acad::ErrorStatus es = m_pDict->getAt( key, ( AcDbObject*& ) pXrec, AcDb::kForWrite );
        if( Acad::eOk != es && Acad::eKeyNotFound != es )
        {
            return false;
        }
        if( pXrec == 0 )
        {
            return false;
        }
        else
        {
            ArxXRecordManager dem( pXrec, true );
            return dem.getEntry( FIRST_ENTRY, value ); // 每个XRECORD只存放一个数据
        }
    }
    virtual bool add( const CString& key )
    {
        if( m_pDict == 0 ) return false;
        AcDbXrecord* pXrec = 0;
        // key不存在或者其它原因
        Acad::ErrorStatus es = m_pDict->getAt( key, ( AcDbObject*& ) pXrec, AcDb::kForWrite );
        if( Acad::eOk != es && Acad::eKeyNotFound != es )
        {
            return false;
        }
        if( Acad::eKeyNotFound == es )
        {
            pXrec = new AcDbXrecord();
            AcDbObjectId xrecObjId;
            if( Acad::eOk != m_pDict->setAt( key, pXrec, xrecObjId ) )
            {
                delete pXrec;
                pXrec = 0;
                return false;
            }
            else
            {
                // 在事务里新增的对象,必须要绑定到当前的transaction中,否则得到的指针无效!!!
                // 参见: ObjectARX Developer's Guide -> Advance Topics -> Transaction Management -> Newly Created Objects and Transactions
                if( Acad::eOk != actrTransactionManager->addNewlyCreatedDBRObject( pXrec ) )
                {
                    return false;
                }
                else
                {
                    ArxXRecordManager dem( pXrec, true );
                    CString temp_value;
                    return ( dem.addEntry( temp_value ) != INVALID_ENTRY );// 每个XRECORD只存放一个数据
                }
            }
        }
        return true;
    }
    virtual bool remove( const CString& key )
    {
        if( m_pDict == 0 ) return false;
        AcDbObjectId objId;
        if( Acad::eOk != m_pDict->remove( key, objId ) )
        {
            return false;
        }
        else
        {
            //ArxEntityHelper::EraseObject2( objId, true ); // 删除对象
            return true;
        }
    }
private:
    AcDbDictionary* m_pDict;
    bool m_createNewKey;
};
static bool CompareDatas( MineGE* pGE, AcDbDictionary* pDict )
{
    AcStringArray fields, geo_values;
    pGE->getAllDatas( fields, geo_values );
    bool ret = true;
    // 与属性数据中的进行比较
    DictPointerDao* dao = new DictPointerDao( pDict );
    for( int i = 0; i < fields.length(); i++ )
    {
        CString field = fields[i].kACharPtr();
        CString geo_value = geo_values[i].kACharPtr(); // 几何参数值
        CString prop_value;                            // 属性数据值
        // 属性数据中该字段不存在或者读取失败!
        if( !dao->get( FieldHelper::MakeHybirdField( PARAM_GROUP, field ), prop_value ) ) continue;
        // 几何参数 与 属性数据 进行比较
        if( geo_value.CompareNoCase( prop_value ) != 0 )
        {
            ret = false;
            break;
        }
    }
    delete dao;
    return ret;
}
// 将图元的几何参数同步到属性数据
static void ModifyPropertys( AcDbDictionary* pDict, MineGE* pGE )
{
    // 获取图元的所有几何参数
    AcStringArray fields, values;
    pGE->getAllDatas( fields, values );
    // 与属性数据中的进行比较
    DictPointerDao* dao = new DictPointerDao( pDict );
    for( int i = 0; i < fields.length(); i++ )
    {
        CString field = fields[i].kACharPtr();
        CString new_value = values[i].kACharPtr();
        // 如果字段存在,则修改属性数据的值
        dao->set( FieldHelper::MakeHybirdField( PARAM_GROUP, field ), new_value );
    }
    delete dao;
}
// 将图元的属性数据同步到几何参数
static void ModifyParams( MineGE* pGE, AcDbDictionary* pDict )
{
    // 获取图元的所有几何参数
    AcStringArray fields, values;
    pGE->getAllDatas( fields, values );
    // 与属性数据中的进行比较
    DictPointerDao* dao = new DictPointerDao( pDict );
    for( int i = 0; i < fields.length(); i++ )
    {
        CString field = fields[i].kACharPtr();
        CString new_value;
        // 属性数据中该字段不存在或读取失败!!!
        if( dao->get( FieldHelper::MakeHybirdField( PARAM_GROUP, field ), new_value ) )
        {
            pGE->setString( field, new_value );
        }
    }
    delete dao;
}
static void PrintPropertys( AcDbDictionary* pDict, MineGE* pGE )
{
    // 获取图元的所有几何参数
    AcStringArray fields, values;
    pGE->getAllDatas( fields, values );
    // 与属性数据中的进行比较
    DictPointerDao* dao = new DictPointerDao( pDict );
    for( int i = 0; i < fields.length(); i++ )
    {
        CString field = fields[i].kACharPtr();
        CString new_value = values[i].kACharPtr();
        CString old_value;
        dao->get( field, old_value );
        acutPrintf( _T( "\n属性数据:field - %s   old - %s    new - %s" ), field, old_value, new_value );
    }
    delete dao;
}
// 将图元的属性数据同步到几何参数
static void PrintParam( MineGE* pGE, AcDbDictionary* pDict )
{
    // 获取图元的所有几何参数
    AcStringArray fields, values;
    pGE->getAllDatas( fields, values );
    // 与属性数据中的进行比较
    DictPointerDao* dao = new DictPointerDao( pDict );
    for( int i = 0; i < fields.length(); i++ )
    {
        CString field = fields[i].kACharPtr();
        CString old_value = values[i].kACharPtr();
        CString new_value;
        dao->get( field, new_value );
        acutPrintf( _T( "\n图元参数:field: - %s   old - %s    new - %s" ), field, old_value, new_value );
    }
    delete dao;
}
// 在objectModified中无法启动事务, 必须使用open/close机制
void DatabaseReactor::objectModified( const AcDbDatabase* dwg, const AcDbObject* dbObj )
{
    AcDbDatabaseReactor::objectModified ( dwg, dbObj );
    // 判断是否扩展词典被修改(扩展词典一般是和AcDbObject绑定在一起)
    // 如果词典的owner是AcDbEntity, 表明它是一个图元的扩展词典,而不是一个普通词典
    // 此时需要更新图元的数据以及绘制效果!!!
    if( dbObj->isKindOf( AcDbDictionary::desc() ) )
    {
        AcDbDictionary* pDict = AcDbDictionary::cast( dbObj );
        AcDbObjectId objId = dbObj->ownerId();
        // 用open/close机制打开图元
        AcDbObject* pObj;
        if( Acad::eOk != acdbOpenObject( pObj, objId, AcDb::kForWrite ) ) return;
        MineGE* pGE = MineGE::cast( pObj );
        if( pGE != 0 )
        {
            // 比较MineGE图元的几何参数与属性数据,如果不相同,就更新(也就是修改)图元的几何参数
            // 几何参数保存在图元的成员变量
            // 属性数据保存在图元的属性数据
            if( !CompareDatas( pGE, pDict ) )
            {
                PrintParam( pGE, pDict );
                ModifyParams( pGE, pDict );
                acutPrintf( _T( "\n属性数据被修改，同步到图元参数...." ) );
            }
            else
            {
                // 强制更新显示效果
                // 几何参数可能没有修改,但部分属性数据的修改也会影响图元的显示效果
                pGE->recordGraphicsModified( true );
                //acutPrintf(_T("\n属性数据与图元的参数相等..."));
                //PrintParam(pGE, pDict);
            }
        }
        pObj->close();
    }
    // 如果图元的几何参数被修改,同步修改图元的属性数据里对应的字段
    else if( dbObj->isKindOf( MineGE::desc() ) )
    {
        MineGE* pGE = MineGE::cast( dbObj );
        // 用open/close机制打开扩展词典
        AcDbObject* pObj;
        if( Acad::eOk != acdbOpenObject( pObj, pGE->extensionDictionary(), AcDb::kForWrite ) ) return;
        AcDbDictionary* pDict = AcDbDictionary::cast( pObj );
        if( pDict == 0 )
        {
            pObj->close();
            return;
        }
        // 比较MineGE图元的几何参数与属性数据,如果不相同,就更新(也就是修改)图元的属性数据
        // 几何参数保存在图元的成员变量
        // 属性数据保存在图元的属性数据
        if( !CompareDatas( pGE, pDict ) )
        {
            PrintPropertys( pDict, pGE );
            ModifyPropertys( pDict, pGE );
            acutPrintf( _T( "\n图元参数被修改，同步到属性数据...." ) );
        }
        else
        {
            //acutPrintf(_T("\n图元的参数与属性数据相等..."));
            //PrintPropertys(pDict, pGE);
        }
        pObj->close();
    }
}
static void DoEdgeGEJunctionClosure( EdgeGE* pEdge )
{
    AcGePoint3d spt, ept;
    pEdge->getSEPoint( spt, ept );
    DrawHelper::EdgeGEJunctionClosure( spt );
    DrawHelper::EdgeGEJunctionClosure( ept );
}
static void DoEdgeGEJunctionClosure2( const AcDbObject* pObj )
{
    EdgeGE* pEdge = EdgeGE::cast( pObj );
    if( pEdge != 0 )
    {
        //DrawHelper::EdgeGEJunctionClosure2(pEdge->objectId());
        DoEdgeGEJunctionClosure( pEdge );
    }
}
static void DoJointRelations( const AcDbObject* pObj )
{
    JointGE* pJoint = JointGE::cast( pObj );
    if( pJoint != 0 )
    {
        //DrawHelper::EdgeGEJunctionClosure2(pEdge->objectId());
        DrawHelper::JointRelations2( pJoint->objectId() );
    }
}
static void DoJointRelations2( const AcDbObject* pObj )
{
    EdgeGE* pEdge = EdgeGE::cast( pObj );
    if( pEdge != 0 )
    {
        //DrawHelper::EdgeGEJunctionClosure2(pEdge->objectId());
        AcGePoint3d spt, ept;
        pEdge->getSEPoint(spt, ept);

        AcGePoint3dArray pts;
        pts.append(spt);
        pts.append(ept);
        AcDbObjectIdArray jointIds;
        DrawHelper::GetAllJointGEByPoints(pts, jointIds);
        if(pts.length() == jointIds.length())
        {
            for(int i=0;i<pts.length();i++)
            {
                DrawHelper::JointRelations( pts[i], jointIds[i] );
            }            
        }        
    }
}
void DatabaseReactor::objectAppended( const AcDbDatabase* db, const AcDbObject* pObj )
{
    //acutPrintf(_T("\noid:%d call objectAppended()...:%s"), pObj->objectId());
    // 处理分支闭合
    DoEdgeGEJunctionClosure2( pObj );
    // 处理节点关联
    DoJointRelations( pObj );
    DoJointRelations2( pObj );
}