#include "StdAfx.h"
#include "EditorReactor.h"
#include "EdgeGE.h"
#include "JointGE.h"
#include "DrawHelper.h"
#include "ArxHelper/HelperClass.h"

EditorReactor::EditorReactor ( const bool autoInitAndRelease ) : AcEditorReactor(), mbAutoInitAndRelease( autoInitAndRelease )
{
    if( autoInitAndRelease )
    {
        if( acedEditor )
        {
            //acutPrintf(_T("\nEdgeGE_EditorReactor : %ld"), (long)acedEditor);
            acedEditor->addReactor ( this ) ;
        }
        else
            mbAutoInitAndRelease = false ;
    }
}
EditorReactor::~EditorReactor ()
{
    Detach () ;
}
void EditorReactor::Attach ()
{
    Detach () ;
    if ( !mbAutoInitAndRelease )
    {
        if ( acedEditor )
        {
            acedEditor->addReactor ( this ) ;
            mbAutoInitAndRelease = true ;
        }
    }
}
void EditorReactor::Detach ()
{
    if ( mbAutoInitAndRelease )
    {
        if ( acedEditor )
        {
            acedEditor->removeReactor ( this ) ;
            mbAutoInitAndRelease = false ;
        }
    }
}
AcEditor* EditorReactor::Subject () const
{
    return ( acedEditor ) ;
}
bool EditorReactor::IsAttached () const
{
    return ( mbAutoInitAndRelease ) ;
}
static void MergeSSet( const ads_name& sset1, ads_name& sset )
{
    long length = 0;
    acedSSLength( sset1, &length );
    for( long i = 0; i < length; i++ )
    {
        ads_name en;
        acedSSName( sset1, i, en );
        acedSSAdd( en, sset, sset );
    }
}
static void MakeSSet( ads_name& sset )
{
    // 'P'并不一定能得到上一次操作的图形
    // get entities just operated on
    ads_name sset1, sset2;
    acedSSGet( _T( "P" ), NULL, NULL, NULL, sset1 );
    acedSSGet( _T( "I" ), NULL, NULL, NULL, sset2 );
    MergeSSet( sset1, sset );
    acedSSFree( sset1 );
    MergeSSet( sset2, sset );
    acedSSFree( sset2 );
}
static void GetEntitiesOnOperate1( AcDbObjectIdArray& objIds )
{
    // 构造选择集
    ads_name sset;
    acedSSAdd( NULL, NULL, sset );
    MakeSSet( sset );
    long length = 0;
    acedSSLength( sset, &length );
    bool ret = true;
    for ( long i = 0; i < length; i++ )
    {
        ads_name en;
        acedSSName( sset, i, en );
        AcDbObjectId eId;
        if( acdbGetObjectId( eId, en ) != Acad::eOk )
        {
            ret = false;
            break;
        }
        if( !objIds.contains( eId ) )
        {
            objIds.append( eId );
        }
    }
    if( !ret ) objIds.removeAll();
    acedSSFree( sset );
}
static void GetEntitiesOnOperate2( AcDbObjectIdArray& objIds )
{
    acedGetCurrentSelectionSet( objIds );
}
static void GetPtsOfEdgeOnOperate( const AcDbObjectIdArray& edgeIds, AcGePoint3dArray& pts )
{
    if( edgeIds.isEmpty() ) return;
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    if( pTrans == 0 ) return;
    int len = edgeIds.length();
    for ( int i = 0; i < len; i++ )
    {
        AcDbObject* pObj = 0;
        if( Acad::eOk == pTrans->getObject( pObj, edgeIds[i], AcDb::kForRead ) )
        {
            EdgeGE* pEdge = EdgeGE::cast( pObj );
            if ( pEdge != 0 )
            {
                //acutPrintf(_T("\n分支图元id:%d需要处理..."), eId);
                AcGePoint3d startPt, endPt;
                pEdge->getSEPoint( startPt, endPt );
                if( !pts.contains( startPt ) ) pts.append( startPt );
                if( !pts.contains( endPt ) ) pts.append( endPt );
            }
        }
    }
    actrTransactionManager->endTransaction();
}
static void EncloseEdge( const AcDbObjectIdArray& objIds )
{       
    AcGePoint3dArray pts;
    GetPtsOfEdgeOnOperate( objIds, pts );

    int len = pts.length();
    for( int i = 0; i < len; i++ )
    {
        DrawHelper::EdgeGEJunctionClosure( pts.at( i ) );
    }
}
static void RelationEdges( const AcDbObjectIdArray& edgeIds )
{
    if(edgeIds.isEmpty()) return;

    // 查找分支坐标
    AcGePoint3dArray pts;
    GetPtsOfEdgeOnOperate( edgeIds, pts );

    // 根据坐标查找节点
    AcDbObjectIdArray jointIds;
    DrawHelper::GetAllJointGEByPoints(pts, jointIds);
    //acutPrintf(_T("\n%d:%d"), pts.length(), jointIds.length());
    // 个数必须相等
    if(pts.length() != jointIds.length()) return;

    for( int i = 0; i < pts.length(); i++ )
    {
        //acutPrintf(_T("\n%s: %s"), ArxUtilHelper::Point3dToString(pts[i]), ArxUtilHelper::ObjectIdToStr(jointIds[i]));
        DrawHelper::JointRelations( pts[i], jointIds[i] );
    }
}
static void RelationJointsImpl(const AcDbObjectIdArray& jointIds)
{
    if( jointIds.isEmpty() ) return;
    // 处理每个节点
    int len = jointIds.length();
    for( int i = 0; i < len; i++ )
    {
        DrawHelper::JointRelations2( jointIds[i] );
    }
    // 处理与节点关联的分支
    AcDbObjectIdArray edgeIds;
    DrawHelper::GetRelatedEdgesByJoint(jointIds, edgeIds);
    RelationEdges(edgeIds);
}

static void RelationJoints( const AcDbObjectIdArray& objIds )
{
    if(objIds.isEmpty()) return;
    // 如果编辑的图元中包括节点
    AcDbObjectIdArray jointIds;
    ArxDataTool::FilterEntsByType(_T("JointGE"), objIds, jointIds);
    RelationJointsImpl(jointIds);
}
static void RelationJoints2(const AcDbObjectIdArray& objIds)
{
    if(objIds.isEmpty()) return;
    // 如果编辑的图元中包括分支
    AcDbObjectIdArray edgeIds;
    ArxDataTool::FilterEntsByType(_T("EdgeGE"), objIds, edgeIds);
    RelationEdges(edgeIds);
}
// 需要定制的命令
static ACHAR* _cmds[] =
{
    _T( "MOVE" ),
    _T( "ROTATE" ),
    _T( "SCALE" ),
    _T( "STRECTCH" ),
    _T( "GRIP_STRETCH" )
};
void EditorReactor::commandEnded( const ACHAR* cmdStr )
{
    // If AutoCAD is shutting down, then do nothing.
    if ( !acdbHostApplicationServices()->workingDatabase() )
        return;
    AcStringArray cmds;
    int len = sizeof( _cmds ) / sizeof( _cmds[0] );
    for( int i = 0; i < len; i++ )
    {
        cmds.append( _cmds[i] );
    }
    if( cmds.contains( cmdStr ) )
    {
        //acutPrintf(_T("\n有效的命令执行: %s"), cmdStr);
        AcDbObjectIdArray objIds;
        GetEntitiesOnOperate1( objIds );
        //GetEntitiesOnOperate2(objIds);
        // 闭合分支始末端点
        EncloseEdge( objIds );
        // 更新节点的关联
        RelationJoints( objIds );
        // 更新分支的始末点关联
        RelationJoints2( objIds );
    }
    AcEditorReactor::commandEnded ( cmdStr ) ;
}
void EditorReactor::commandWillStart( const ACHAR* cmdStr )
{
    AcEditorReactor::commandWillStart ( cmdStr ) ;
}
