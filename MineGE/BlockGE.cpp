#include "StdAfx.h"
#include "BlockGE.h"
//#include "BlockDraw_ConfigDlg.h"
#include "HelperClass.h"
// 根据块名称获取块定义id
static AcDbObjectId GetBlockDefinitionObjectId( const CString& blockName )
{
    AcDbBlockTable* pBlockTable;
    acdbHostApplicationServices()->workingDatabase()->getSymbolTable( pBlockTable, AcDb::kForRead );
    AcDbBlockTableRecord* pBlockTableRecord;
    Acad::ErrorStatus es = pBlockTable->getAt( blockName, pBlockTableRecord, AcDb::kForRead );
    pBlockTable->close();
    AcDbObjectId objId;
    if( es == Acad::eOk )
    {
        objId = pBlockTableRecord->objectId();
        pBlockTableRecord->close();
    }
    return objId;
}
// 获取块定义的变换矩阵
static AcGeMatrix3d GetBlockTransformMatrix( const AcDbObjectId& blkId, const AcGePoint3d& insertPt, double angle, double scale )
{
    AcDbBlockReference bRef( insertPt, blkId );
    bRef.setRotation( angle );
    bRef.setScaleFactors( scale );
    return bRef.blockTransform();
}
// 获取块定义中的所有属性名称
static void GetBlockAttrDefinitionNames( const AcDbObjectId& blkId, AcStringArray& attNames )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    AcDbObject* pObj;
    if( Acad::eOk != pTrans->getObject( pObj, blkId, AcDb::kForRead ) )
    {
        actrTransactionManager->abortTransaction();
        return;
    }
    AcDbBlockTableRecord* pBTR = AcDbBlockTableRecord::cast( pObj );
    // BUG：不能调用hasAttributeDefinitions()方法
    // 调用之后，如果没有在块编辑器中对块进行修改，
    // 那么进行移动、夹点编辑等操作，没有动态显示效果
    //if(!pBTR->hasAttributeDefinitions())
    //{
    //	// 没有属性定义
    //	acutPrintf(_T("\n没有属性定义"));
    //	actrTransactionManager->abortTransaction();
    //	return;
    //}
    AcDbBlockTableRecordIterator* pIterator;
    if( Acad::eOk != pBTR->newIterator( pIterator ) )
    {
        actrTransactionManager->abortTransaction();
        return;
    }
    // 遍历块中的图元，查找AcDbAttributeDefinition
    for( pIterator->start( true ); !pIterator->done(); pIterator->step( true ) )
    {
        AcDbObjectId objId;
        if( Acad::eOk != pIterator->getEntityId( objId ) ) continue;
        if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForWrite ) ) continue;
        AcDbEntity* pEnt = AcDbEntity::cast( pObj );
        if( pEnt->isKindOf( AcDbAttributeDefinition::desc() ) )
        {
            AcDbAttributeDefinition* pAttDef = AcDbAttributeDefinition::cast( pEnt );
            pAttDef->convertIntoMTextAttributeDefinition( Adesk::kTrue );
            // 获取属性定义的名称
            ACHAR* pTag = pAttDef->tag();
            attNames.append( AcString( pTag ) );
            acutDelString( pTag );
        }
    }
    delete pIterator;
    actrTransactionManager->endTransaction();
}
// 将块定义分解成独立的图元,将属性替换成多行文字
static void BlockToEntity( const AcDbObjectId& blkId, const AcGeMatrix3d& blkXform,
                           const AcStringArray& names, const AcStringArray& attValues,
                           AcGeVoidPointerArray& ents )
{
    AcTransaction* pTrans = actrTransactionManager->startTransaction();
    AcDbObject* pObj;
    if( Acad::eOk != pTrans->getObject( pObj, blkId, AcDb::kForRead ) )
    {
        actrTransactionManager->abortTransaction();
        return;
    }
    AcDbBlockTableRecord* pBTR = AcDbBlockTableRecord::cast( pObj );
    // BUG：不能调用hasAttributeDefinitions()方法
    // 调用之后，如果没有在块编辑器中对块进行修改，
    // 那么进行移动、夹点编辑等操作，没有动态显示效果
    //if(!pBTR->hasAttributeDefinitions())
    //{
    //	// 没有属性定义
    //	acutPrintf(_T("\n没有属性定义"));
    //	actrTransactionManager->abortTransaction();
    //	return;
    //}
    AcDbBlockTableRecordIterator* pIterator;
    if( Acad::eOk != pBTR->newIterator( pIterator ) )
    {
        actrTransactionManager->abortTransaction();
        return;
    }
    // 遍历块中的图元，查找AcDbAttributeDefinition
    for( pIterator->start( true ); !pIterator->done(); pIterator->step( true ) )
    {
        AcDbObjectId objId;
        if( Acad::eOk != pIterator->getEntityId( objId ) ) continue;
        if( Acad::eOk != pTrans->getObject( pObj, objId, AcDb::kForWrite ) ) continue;
        AcDbEntity* pEnt = AcDbEntity::cast( pObj );
        if( !pEnt->isKindOf( AcDbAttributeDefinition::desc() ) )
        {
            AcDbEntity* pClone = AcDbEntity::cast( pEnt->clone() );
            pClone->transformBy( blkXform );
            // 添加到实体集合
            ents.append( pClone );
        }
        else
        {
            AcDbAttributeDefinition* pAttDef = AcDbAttributeDefinition::cast( pEnt );
            pAttDef->convertIntoMTextAttributeDefinition( Adesk::kTrue );
            // 获取标签名称
            ACHAR* pTag = pAttDef->tag();
            int pos = names.find( pTag );
            if( pos != -1 )
            {
                // 获取多行文本对象
                AcDbMText* pMText = pAttDef->getMTextAttributeDefinition();
                pMText->transformBy( blkXform );
                pMText->setContents( attValues[pos].kACharPtr() );
                // 添加到实体集合
                ents.append( pMText );
            }
            acutDelString( pTag );
        }
    }
    delete pIterator;
    actrTransactionManager->endTransaction();
}
// 删除多个图元
static void DeleteEntities( AcGeVoidPointerArray& ents )
{
    int n = ents.length();
    for( int i = 0; i < n; i++ )
    {
        AcDbEntity* pEnt = ( AcDbEntity* )ents[i];
        delete pEnt;
    }
}
// 绘制多个图元
static void DrawEntities( AcGiWorldDraw* mode, AcGeVoidPointerArray& ents )
{
    int n = ents.length();
    for( int i = 0; i < n; i++ )
    {
        AcDbEntity* pEnt = ( AcDbEntity* )ents[i];
        pEnt->worldDraw( mode );
    }
}
Adesk::UInt32 BlockGE::kCurrentVersionNumber = 1 ;
ACRX_NO_CONS_DEFINE_MEMBERS( BlockGE, MineGE )
BlockGE::BlockGE () : MineGE (), m_scale( 1.0 ), m_angle( 0.0 )
{
    map( _T( "坐标" ), &m_insertPt );
    map( _T( "角度" ), &m_angle );
    map( _T( "比例" ), &m_scale );
    map( _T( "块" ), &m_blockName );
}
BlockGE::BlockGE( const CString& blockName ) : MineGE(), m_blockName( blockName ), m_scale( 1.0 ), m_angle( 0.0 )
{
    map( _T( "坐标" ), &m_insertPt );
    map( _T( "角度" ), &m_angle );
    map( _T( "比例" ), &m_scale );
    map( _T( "块" ), &m_blockName );
}
BlockGE::BlockGE ( const CString& blockName, const AcGePoint3d& insertPt, double angle, double scale )
    : MineGE (), m_blockName( blockName ), m_insertPt( insertPt ), m_angle( angle ), m_scale( scale )
{
    map( _T( "坐标" ), &m_insertPt );
    map( _T( "角度" ), &m_angle );
    map( _T( "比例" ), &m_scale );
    map( _T( "块" ), &m_blockName );
}
BlockGE::~BlockGE ()
{
}
AcGePoint3d BlockGE::getInsertPt() const
{
    assertReadEnabled();
    return m_insertPt;
}
void BlockGE::setInsertPt( const AcGePoint3d& pt )
{
    assertWriteEnabled();
    m_insertPt = pt;
}
double BlockGE::getAngle() const
{
    assertReadEnabled();
    return m_angle;
}
void BlockGE::setAngle( double angle )
{
    assertWriteEnabled();
    m_angle = angle;
}
double BlockGE::getScale() const
{
    assertReadEnabled();
    return m_scale;
}
void BlockGE::setScale( double scale )
{
    assertWriteEnabled();
    m_scale = scale;
}
//void BlockGE::configExtraParams()
//{
// 切换资源
//CAcModuleResourceOverride myResources;
//// 显示已有的块定义列表, 让用户选择块名称
//// 从而修改可视化效果
//BlockDraw_ConfigDlg dlg;
//dlg.m_blockName = m_blockName;
//if( IDOK == dlg.DoModal() )
//{
//    m_blockName = dlg.m_blockName;
//}
//}
void BlockGE::regPropertyDataNames( AcStringArray& names ) const
{
    // 获取块定义id
    AcDbObjectId blkId = GetBlockDefinitionObjectId( m_blockName );
    if( !blkId.isNull() )
    {
        // 在块定义中查找所有属性名称
        // 这些属性名称即视为要注册的"字段"
        GetBlockAttrDefinitionNames( blkId, names );
    }
}
void BlockGE::readPropertyDataFromValues( const AcStringArray& values )
{
    m_blockAttrValues.removeAll();
    m_blockAttrValues.append( values );
}
void BlockGE::explodeBlock( AcGeVoidPointerArray& ents )
{
    // 填充属性数据
    AcStringArray names;
    regPropertyDataNames( names );
    //if( names.isEmpty() )
    //{
    //    acutPrintf( _T( "\n没有注册要提取的字段..." ) );
    //    return;
    //}
    //if( names.length() != m_blockAttrValues.length() )
    //{
    //    acutPrintf( _T( "\n注册的属性数据与读取的数据个数不相等!" ) );
    //    return;
    //}
    AcDbObjectId blkId = GetBlockDefinitionObjectId( m_blockName );
    if( blkId.isNull() )
    {
        acutPrintf( _T( "\n未知的块定义:%s" ), m_blockName );
        return;
    }
    // 计算变换矩阵
    AcGeMatrix3d blkXform = GetBlockTransformMatrix( blkId, m_insertPt, m_angle, m_scale );
    // 分解块定义
    BlockToEntity( blkId, blkXform, names, m_blockAttrValues, ents );
}
Adesk::Boolean BlockGE::customWorldDraw ( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    // 分解块并填充数据
    AcGeVoidPointerArray ents;
    explodeBlock( ents );
    // 绘制图形
    DrawEntities( mode, ents );
    // 删除图形实体指针
    DeleteEntities( ents );
    return Acad::eOk;
}
Acad::ErrorStatus BlockGE::customTransformBy( const AcGeMatrix3d& xform )
{
    m_insertPt.transformBy( xform );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.transformBy( xform );
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    m_scale = v.length();
    return Acad::eOk;
}
Acad::ErrorStatus BlockGE::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled();
    snapPoints.append( m_insertPt );
    return Acad::eOk;
}
Acad::ErrorStatus BlockGE::customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 从测试结果来看，块只有一个夹点：插入点
    gripPoints.append( m_insertPt );
    return Acad::eOk;
}
Acad::ErrorStatus BlockGE::customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    m_insertPt += offset;
    return Acad::eOk;
}
Acad::ErrorStatus BlockGE::dwgOutFields ( AcDbDwgFiler* pFiler ) const
{
    assertReadEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgOutFields"));
    Acad::ErrorStatus es = MineGE::dwgOutFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    if ( ( es = pFiler->writeUInt32 ( BlockGE::kCurrentVersionNumber ) ) != Acad::eOk )
        return ( es ) ;
    pFiler->writeItem( m_insertPt );
    pFiler->writeItem( m_angle );
    pFiler->writeItem( m_scale );
    pFiler->writeString( AcString( m_blockName ) );
    pFiler->writeItem( m_blockAttrValues.length() );
    for( int i = 0; i < m_blockAttrValues.length(); i++ )
    {
        pFiler->writeString( m_blockAttrValues[i] );
    }
    return ( pFiler->filerStatus () ) ;
}
Acad::ErrorStatus BlockGE::dwgInFields ( AcDbDwgFiler* pFiler )
{
    assertWriteEnabled () ;
    //acutPrintf(_T("\nEdgeGE::dwgInFields"));
    Acad::ErrorStatus es = MineGE::dwgInFields ( pFiler ) ;
    if ( es != Acad::eOk )
        return ( es ) ;
    Adesk::UInt32 version = 0 ;
    if ( ( es = pFiler->readUInt32 ( &version ) ) != Acad::eOk )
        return ( es ) ;
    if ( version > BlockGE::kCurrentVersionNumber )
        return ( Acad::eMakeMeProxy ) ;
    pFiler->readItem( &m_insertPt );
    pFiler->readItem( &m_angle );
    pFiler->readItem( &m_scale );
    AcString str;
    pFiler->readString( str );
    m_blockName = str.kACharPtr();
    m_blockAttrValues.removeAll();
    int n = 0;
    pFiler->readItem( &n );
    for( int i = 0; i < n; i++ )
    {
        AcString attr;
        pFiler->readString( attr );
        m_blockAttrValues.append( attr.kACharPtr() );
    }
    return ( pFiler->filerStatus () ) ;
}
void BlockGE::set( const AcGePoint3d& insertPt, double angle/*=0.0*/, double scale/*=1.0*/ )
{
    this->m_insertPt = insertPt;
    this->m_angle = angle;
    this->m_scale = scale;
}
void BlockGE::setBlock( const CString& blockName )
{
    m_blockName = blockName;
}
CString BlockGE::getBlock() const
{
    return m_blockName;
}
Adesk::UInt32 CustomGE::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS (
    CustomGE, BlockGE,
    AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
    AcDbProxyEntity::kNoOperation,
    自定义图元, VENTGEAPP
)
CustomGE::CustomGE () : BlockGE ()
{
}
CustomGE::CustomGE ( const CString& blockName ) : BlockGE ( blockName )
{
}
CustomGE::CustomGE( const CString& blockName, const AcGePoint3d& insertPt, double angle, double scale )
    : BlockGE( blockName, insertPt, angle, scale )
{
}
CustomGE::~CustomGE ()
{
}
CString CustomGE::getRealTypeName() const
{
    return m_blockName;
}
