﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace MisLauncher
{
    static class CADHelper
    {
        public static string GetCADLocation()
        {
            Register reg = new Register();
            reg.Domain = RegDomain.LocalMachine;
            reg.SubKey = "Software\\Autodesk\\AutoCAD\\R18.0\\ACAD-8001:804";
            reg.RegeditKey = "AcadLocation";
            if(!reg.IsSubKeyExist())
            {
                return "";
            }
            return reg.ReadRegeditKey().ToString();
        }
        public static string GetCADSupportDir()
        {
            string app_data = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
            return app_data + "\\Autodesk\\AutoCAD 2010\\R18.0\\chs\\Support\\";
        }
        public static bool ModifyCuiFiles()
        {
            string cuiRoot = CADHelper.GetCADSupportDir();
            string curDir = System.Environment.CurrentDirectory+"\\";

            bool ret = true;
            try
            {
                string cuiFile1 = curDir + "\\CUIX\\JLCAD\\acad.CUIX";
                string cuiFile2 = cuiRoot + "acad.CUIX";
                System.IO.File.Copy(cuiFile1, cuiFile2, true);

                string mnrFile1 = curDir + "\\CUIX\\JLCAD\\acad.mnr";
                string mnrFile2 = cuiRoot + "acad.mnr";
                System.IO.File.Copy(mnrFile1, mnrFile2, true);
            }
            catch (System.Exception e)
            {                
                MessageBox.Show(string.Format("修改CUIX文件失败\n原因:{0}", e.Message));
                ret = false;
            }
            return ret;
        }
        public static bool RestoreCuiFiles()
        {
            string cuiRoot = CADHelper.GetCADSupportDir();
            string curDir = System.Environment.CurrentDirectory + "\\";

            bool ret = true;
            try
            {
                string cuiFile1 = curDir + "\\CUIX\\AutoCAD\\acad.CUIX";
                string cuiFile2 = cuiRoot + "acad.CUIX";
                System.IO.File.Copy(cuiFile1, cuiFile2, true);

                string mnrFile1 = curDir + "\\CUIX\\AutoCAD\\acad.mnr";
                string mnrFile2 = cuiRoot + "acad.mnr";
                System.IO.File.Copy(mnrFile1, mnrFile2, true);
            }
            catch (System.Exception e)
            {
                MessageBox.Show(string.Format("恢复CUIX文件失败\n原因:{0}", e.Message));
                ret = false;
            }
            return ret;
        }

        private static void RegMisArx(string subKey, string arxFile)
        {
            string curDir = System.Environment.CurrentDirectory + "\\";

            Register reg = new Register();
            reg.Domain = RegDomain.LocalMachine;
            reg.SubKey = "Software\\Autodesk\\AutoCAD\\R18.0\\ACAD-8001:804\\Applications\\"+subKey;
            reg.WriteRegeditKey("LOADCTRLS", 2, RegValueKind.DWord);
            reg.WriteRegeditKey("LOADER", curDir + arxFile, RegValueKind.String);
        }
        private static void RegMisMgd(string subKey, string dllFile)
        {
            string curDir = System.Environment.CurrentDirectory + "\\";

            Register reg = new Register();
            reg.Domain = RegDomain.LocalMachine;
            reg.SubKey = "Software\\Autodesk\\AutoCAD\\R18.0\\ACAD-8001:804\\Applications\\" + subKey;
            reg.WriteRegeditKey("LOADCTRLS", 14, RegValueKind.DWord);
            reg.WriteRegeditKey("LOADER", curDir + dllFile, RegValueKind.String);
            reg.WriteRegeditKey("MANAGED", 1, RegValueKind.DWord);
        }
        private static void UnRegMisArx(string subKey)
        {
            Register reg = new Register();
            reg.Domain = RegDomain.LocalMachine;
            reg.SubKey = "Software\\Autodesk\\AutoCAD\\R18.0\\ACAD-8001:804\\Applications\\" + subKey;
            reg.DeleteSubKey();
        }
        private static void UnRegMisMgd(string subKey)
        {
            Register reg = new Register();
            reg.Domain = RegDomain.LocalMachine;
            reg.SubKey = "Software\\Autodesk\\AutoCAD\\R18.0\\ACAD-8001:804\\Applications\\" + subKey;
            reg.DeleteSubKey();
        }
        private static void SwitchInfoCenterOn(bool bOn)
        {
            string curDir = System.Environment.CurrentDirectory + "\\";

            Register reg = new Register();
            reg.Domain = RegDomain.CurrentUser;
            reg.SubKey = "Software\\Autodesk\\AutoCAD\\R18.0\\ACAD-8001:804\\InfoCenter";
            if (bOn)
            {
                reg.WriteRegeditKey("InfoCenterOn", "1", RegValueKind.String);
            }
            else
            {
                reg.WriteRegeditKey("InfoCenterOn", "0", RegValueKind.String);
            }
        }
        public static void RegLoaderInfo()
        {
            string curDir = System.Environment.CurrentDirectory + "\\";
            RegMisArx("mis_arx", "ArxLoader.arx");
            RegMisMgd("mis_mgd", "MisApp.dll");
            SwitchInfoCenterOn(false);
        }
        public static void UnRegLoaderInfo()
        {
            UnRegMisArx("mis_arx");
            UnRegMisMgd("mis_mgd");
            SwitchInfoCenterOn(true);
        }
        public static string SelectDwgFile()
        {
            string dwgFile = "";

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            //openFileDialog1.InitialDirectory = "D:\\Patch";
            openFileDialog1.Filter = "AutoCAD图形文件 (*.dwg)|*.dwg";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                dwgFile = openFileDialog1.FileName;
            }
            return dwgFile;
        }
    }
}
