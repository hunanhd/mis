﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;

namespace MisLauncher
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.skinEngine1.SkinFile = "MSN.ssk";
        }

        private void launchAutoCAD(string dwgFile)
        {
            // 隐藏对话框
            this.Hide();

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            string appPath = CADHelper.GetCADLocation() + "\\acad.exe";
            process.StartInfo.FileName = appPath;
            process.StartInfo.Arguments = dwgFile;
            process.StartInfo.UseShellExecute = true;
            process.StartInfo.CreateNoWindow = false;
            process.StartInfo.RedirectStandardOutput = false;
            process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
            // Start the process
            process.Start();
            System.Threading.Thread.Sleep(1000);
            // Wait that the process exits
            process.WaitForExit();

            if (process.HasExited)
            {
                // 恢复对话框
                this.Show();
                // Now read the output of the DOS application
                //string Result = process.StandardOutput.ReadToEnd();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            launchAutoCAD("");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string dwgFile = CADHelper.SelectDwgFile();
            if (dwgFile != "")
            {
                launchAutoCAD(dwgFile);
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (ProcHelper.SearchProc("acad"))
            {
                MessageBox.Show("AutoCAD正在运行!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                e.Cancel = true; // 取消关闭
                return;
            }
            else 
            {
                CADHelper.UnRegLoaderInfo();
                CADHelper.RestoreCuiFiles();
                base.OnClosing(e);
            }
        }
    }
}
