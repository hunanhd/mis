﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

// 界面美化(使用IrisSkin2 皮肤组件,美化效果一般,也凑乎用!)
// http://www.jianshu.com/p/b2335af279f9
namespace MisLauncher
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (ProcHelper.SearchProc("acad"))
            {
                MessageBox.Show("AutoCAD正在运行!","警告",MessageBoxButtons.OK,MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
            }
            //else if (ProcHelper.SearchProc("MisLauncher"))
            //{
            //    MessageBox.Show("本程序正在运行!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
            //}
            else if (CADHelper.GetCADLocation() == "")
            {
                MessageBox.Show("请安装AutoCAD2010(64位)!", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
            }
            else if (!CADHelper.ModifyCuiFiles())
            {
            }
            else
            {
                // 向AutoCAD的注册表添加启动信息
                CADHelper.RegLoaderInfo();

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }
        }
    }
}
