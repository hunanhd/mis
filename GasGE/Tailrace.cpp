#include "StdAfx.h"
#include "Tailrace.h"
#include "MineGE/DrawTool.h"
Adesk::UInt32 Tailrace::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS ( Tailrace,
                          DirGE, AcDb::kDHL_CURRENT,
                          AcDb::kMReleaseCurrent, AcDbProxyEntity::kNoOperation,
                          放水器, GASGEAPP )
Tailrace::Tailrace ()
{
    m_lenth = 3;
}
Tailrace::Tailrace( const AcGePoint3d& insertPt, double angle )
    : DirGE( insertPt, angle )
{
    m_lenth = 3;
    caculPts();
}
Tailrace::~Tailrace ()
{
}
Adesk::Boolean Tailrace::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    if( v.x < 0 ) v.negate();
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    caculPts();
    DrawLine( mode, m_insertPt, m_pt0 );
    DrawLine( mode, m_pt2, m_pt3 );
    DrawLine( mode, m_pt3, m_pt5 );
    DrawLine( mode, m_pt5, m_pt8 );
    DrawLine( mode, m_pt2, m_pt15 );
    DrawLine( mode, m_pt14, m_pt15 );
    DrawLine( mode, m_pt13, m_pt14 );
    DrawLine( mode, m_pt13, m_pt12 );
    DrawLine( mode, m_pt7, m_pt10 );
    DrawLine( mode, m_pt6, m_pt11 );
    DrawArc( mode, m_pt2, m_pt0, m_pt3, false );
    AcGePoint3dArray pts;
    pts.append( m_pt4 );
    pts.append( m_pt9 );
    pts.append( m_pt15 );
    pts.append( m_pt14 );
    pts.append( m_pt13 );
    pts.append( m_pt12 );
    pts.append( m_pt8 );
    pts.append( m_pt7 );
    pts.append( m_pt5 );
    DrawBackGround( mode, pts, 7 );
    return Adesk::kTrue;
}
Acad::ErrorStatus Tailrace::customTransformBy( const AcGeMatrix3d& xform )
{
    // 插入点变换
    m_insertPt.transformBy( xform );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.transformBy( xform );
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis ); // 变换后的旋转角度
    caculPts();
    return Acad::eOk;
}
Acad::ErrorStatus Tailrace::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    if( osnapMode != AcDb::kOsModeCen )
        return Acad::eOk;
    Acad::ErrorStatus es = Acad::eOk;
    if( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return es;
}
Acad::ErrorStatus Tailrace::customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    //gripPoints.append(m_spt1);
    return Acad::eOk;
}
Acad::ErrorStatus Tailrace::customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        if ( idx == 0 )
        {
            m_insertPt += offset; 			// 插入点偏移
            caculPts();
        }
    }
    return Acad::eOk;
}
static void DividArc( const AcGePoint3d& spt, const AcGePoint3d& ept, const AcGePoint3d& thirdPt,
                      int count, AcGePoint3dArray& pts )
{
    pts.append( spt );
    AcGeCircArc3d arc( spt, thirdPt, ept );
    AcGePoint3d cnt = arc.center();
    //	double radius = arc.radius();
    AcGeVector3d v1 = spt - cnt;
    AcGeVector3d v2 = ept - cnt;
    int c = ( arc.normal().z > 0 ? 1 : -1 ); // 弧线方向
    double a1 = v1.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    double a2 = v2.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    double dq = abs( a2 - a1 ) / count;
    //acutPrintf(_T("\na1:%.3f, a2:%.3f dq:%.3f c:%d"), a1, a2, dq, c);
    for( int i = 1; i < count; i++ )
    {
        v1.rotateBy( c * dq, AcGeVector3d::kZAxis );
        pts.append( cnt + v1 );
        //acutPrintf(_T("\n点%d:(%d,%d)"),i,pts[i].x,pts[i].y);
    }
    pts.append( ept );
}
void Tailrace::caclBackGroundMinPolygon( AcGePoint3dArray& pts )
{
    DividArc( m_pt2, m_pt3, m_pt0, 90, pts );
    pts.append( m_pt8 );
    pts.append( m_pt5 );
    pts.append( m_pt4 );
}
void Tailrace::caculPts()
{
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.rotateBy( -0.5 * PI, AcGeVector3d::kZAxis );
    v.normalize();
    m_pt0 = m_insertPt + v * m_lenth / 4;
    m_pt1 = m_insertPt + v * m_lenth * 3 / 4;
    v.rotateBy( -PI * 0.5, AcGeVector3d::kZAxis );
    m_pt2 = m_pt1 + v * m_lenth / 2;
    v.rotateBy( PI, AcGeVector3d::kZAxis );
    m_pt3 = m_pt2 + v * m_lenth;
    v.rotateBy( -PI * 0.5, AcGeVector3d::kZAxis );
    m_pt4 = m_pt3 + v * m_lenth / 3;
    m_pt5 = m_pt4 + v * m_lenth / 3 * 2;
    v.rotateBy( -PI * 0.5, AcGeVector3d::kZAxis );
    m_pt6 = m_pt5 + v * m_lenth / 3;
    m_pt7 = m_pt6 + v * m_lenth / 3;
    m_pt8 = m_pt7 + v * m_lenth / 3;
    v.rotateBy( -PI * 0.5, AcGeVector3d::kZAxis );
    m_pt9 = m_pt8 + v * m_lenth / 3 * 2;
    m_pt12 = m_pt8 + v * m_lenth / 6;
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    m_pt13 = m_pt12 + v * m_lenth / 6;
    v.rotateBy( -PI * 0.5, AcGeVector3d::kZAxis );
    m_pt14 = m_pt13 + v * m_lenth / 6;
    v.rotateBy( -PI * 0.5, AcGeVector3d::kZAxis );
    m_pt15 = m_pt14 + v * m_lenth / 6;

    v.rotateBy( -PI * 3 / 4, AcGeVector3d::kZAxis );
    m_pt10 = m_pt7 + v * m_lenth / 4;
    v.rotateBy( PI * 0.5, AcGeVector3d::kZAxis );
    m_pt11 = m_pt6 + v * m_lenth / 4;
}
void Tailrace::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "大小" ) );
}
void Tailrace::readPropertyDataFromValues( const AcStringArray& values )
{
    CString strLenth;
    strLenth.Format( _T( "%s" ), values[0].kACharPtr() );
    m_lenth = _tstof( strLenth );
    if( 0 >= m_lenth ) m_lenth = 3;
    caculPts();
}