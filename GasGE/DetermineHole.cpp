#include "StdAfx.h"
#include "DetermineHole.h"
#include "MineGE/DrawTool.h"
Adesk::UInt32 DetermineHole::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS ( DetermineHole,
                          DirGE, AcDb::kDHL_CURRENT,
                          AcDb::kMReleaseCurrent, AcDbProxyEntity::kNoOperation,
                          测定孔, DEFGEAPP )
DetermineHole::DetermineHole ()
{
    m_lenth = 7;
}
DetermineHole::DetermineHole( const AcGePoint3d& insertPt, double angle )
    : DirGE( insertPt, angle )
{
    m_lenth = 7;
    caculPts();
}
DetermineHole::~DetermineHole ()
{
}
Adesk::Boolean DetermineHole::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    caculPts();
    DrawLine( mode, m_spt1, m_spt2 );
    DrawLine( mode, m_spt2, m_spt3 );
    DrawLine( mode, m_spt3, m_spt4 );
    DrawLine( mode, m_spt4, m_spt1 );
    return Adesk::kTrue;
}
Acad::ErrorStatus DetermineHole::customTransformBy( const AcGeMatrix3d& xform )
{
    // 插入点变换
    m_insertPt.transformBy( xform );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.transformBy( xform );
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis ); // 变换后的旋转角度
    caculPts();
    return Acad::eOk;
}
Acad::ErrorStatus DetermineHole::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    if( osnapMode != AcDb::kOsModeCen )
        return Acad::eOk;
    Acad::ErrorStatus es = Acad::eOk;
    if( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return es;
}
Acad::ErrorStatus DetermineHole::customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    //gripPoints.append(m_spt1);
    return Acad::eOk;
}
Acad::ErrorStatus DetermineHole::customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        if ( idx == 0 )
        {
            m_insertPt += offset; 			// 插入点偏移
            caculPts();
        }
    }
    return Acad::eOk;
}
void DetermineHole::caculPts()
{
    double pi = 3.1415926;
    AcGePoint3d spt;
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.rotateBy( pi * 0.5, AcGeVector3d::kZAxis );
    spt = m_insertPt + v * m_lenth / 4;
    v.rotateBy( -pi * 0.5, AcGeVector3d::kZAxis );
    m_spt1 = spt + v * m_lenth / 2;
    v.rotateBy( -pi * 0.5, AcGeVector3d::kZAxis );
    m_spt2 = m_spt1 + v * m_lenth / 2;
    v.rotateBy( -pi * 0.5, AcGeVector3d::kZAxis );
    m_spt3 = m_spt2 + v * m_lenth;
    v.rotateBy( -pi * 0.5, AcGeVector3d::kZAxis );
    m_spt4 = m_spt3 + v * m_lenth / 2;
}
void DetermineHole::caclBackGroundMinPolygon( AcGePoint3dArray& pts )
{
    pts.append( m_spt1 );
    pts.append( m_spt2 );
    pts.append( m_spt3 );
    pts.append( m_spt4 );
}
void DetermineHole::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "大小" ) );
}
void DetermineHole::readPropertyDataFromValues( const AcStringArray& values )
{
    CString strLenth;
    strLenth.Format( _T( "%s" ), values[0].kACharPtr() );
    m_lenth = _tstof( strLenth );
    if( 0 >= m_lenth ) m_lenth = 10;
    caculPts();
}
