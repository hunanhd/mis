#include "StdAfx.h"
#include "Sensor.h"
#include "MineGE/DrawTool.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
Adesk::UInt32 Sensor::kCurrentVersionNumber = 1 ;
Adesk::UInt32 CommonSensor::kCurrentVersionNumber = 1 ;
Adesk::UInt32 TempeSensor::kCurrentVersionNumber = 1 ;
Adesk::UInt32 FlowSensor::kCurrentVersionNumber = 1 ;
Adesk::UInt32 PressSensor::kCurrentVersionNumber = 1 ;
Adesk::UInt32 DifferPressSensor::kCurrentVersionNumber = 1 ;
Adesk::UInt32 CH4Sensor::kCurrentVersionNumber = 1 ;
Adesk::UInt32 COSensor::kCurrentVersionNumber = 1 ;
Adesk::UInt32 VentSensor::kCurrentVersionNumber = 1 ;
ACRX_NO_CONS_DEFINE_MEMBERS ( Sensor,  DirGE )
ACRX_DXF_DEFINE_MEMBERS ( CommonSensor, Sensor,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          通用传感器, GASGEAPP )
ACRX_DXF_DEFINE_MEMBERS ( TempeSensor, Sensor,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          温度传感器, GASGEAPP )
ACRX_DXF_DEFINE_MEMBERS ( FlowSensor, Sensor,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          流量传感器, GASGEAPP )
ACRX_DXF_DEFINE_MEMBERS ( PressSensor, Sensor,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          压力传感器, GASGEAPP )
ACRX_DXF_DEFINE_MEMBERS ( DifferPressSensor, Sensor,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          压差传感器, GASGEAPP )
ACRX_DXF_DEFINE_MEMBERS ( CH4Sensor, Sensor,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          瓦斯浓度传感器, GASGEAPP )
ACRX_DXF_DEFINE_MEMBERS ( COSensor, Sensor,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          CO浓度传感器, GASGEAPP )
ACRX_DXF_DEFINE_MEMBERS ( VentSensor, Sensor,
                          AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation,
                          风速传感器, GASGEAPP )
Sensor::Sensor ()
{
    m_radius = 3;
    m_temp = _T( "" );
    m_type = _T( "S" );
    m_unit = _T( "" );
    m_colorIndex = 0;
}
Sensor::Sensor( const AcGePoint3d& insertPt, double angle )
    : DirGE( insertPt, angle )
{
    m_radius = 3;
    m_temp = _T( "" );
    m_type = _T( "S" );
    m_unit = _T( "" );
    m_colorIndex = 0;
}
Sensor::~Sensor ()
{
}
Adesk::Boolean Sensor::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    int oringinColor = mode->subEntityTraits().color();
    mode->subEntityTraits().setColor( m_colorIndex );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    if( v.x < 0 ) v.negate();
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    DrawCircle( mode, m_insertPt, m_radius, false );
    v.rotateBy( PI / 2 , AcGeVector3d::kZAxis );
    v.normalize();
    AcGePoint3d pt = m_insertPt;
    if( _tstof( m_temp ) > 0 )
    {
        pt += 0.2 * m_radius * v;
    }
    DrawMText( mode, pt, m_angle, m_type, 0.6 * m_radius );
    v.rotateBy( PI, AcGeVector3d::kZAxis );
    pt = m_insertPt + 0.5 * m_radius * v;
    if( _tstof( m_temp ) < 0 ) return Adesk::kTrue;
    CString temp;
	StringHelper::DealSpPoints( m_temp );
    temp.Format( _T( "%s%s" ), m_temp, m_unit );
    DrawMText( mode, pt, m_angle, temp , 0.2 * m_radius );
    mode->subEntityTraits().setColor( oringinColor );
    return Adesk::kTrue;
}
Acad::ErrorStatus Sensor::customTransformBy( const AcGeMatrix3d& xform )
{
    // 插入点变换
    //m_insertPt.transformBy( xform );
    //m_pt.transformBy( xform );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.transformBy( xform );
    //m_radius = v.length();
    // 1) 构造一个圆
    AcDbCircle circle( m_insertPt, AcGeVector3d::kZAxis, m_radius );
    // 2) 圆调用transformBy()方法进行变换
    circle.transformBy( xform );
    //3) 获取更新后的参数
    m_insertPt = circle.center();     // 获取变换后的圆心坐标
    m_radius = circle.radius(); // 获取变换后的圆半径
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis ); // 变换后的旋转角度
    return Acad::eOk;
}

Acad::ErrorStatus Sensor::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    if( osnapMode != AcDb::kOsModeCen )
        return Acad::eOk;
    Acad::ErrorStatus es = Acad::eOk;
    if( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return es;
}
Acad::ErrorStatus Sensor::customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    //AcGeVector3d v(AcGeVector3d::kXAxis);
    //v.rotateBy(m_angle,AcGeVector3d::kZAxis);
    //v *= m_radius;
    //gripPoints.append(m_insertPt + v);             // 正方向端点作为夹点
    return Acad::eOk;
}
Acad::ErrorStatus Sensor::customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    int len = indices.length();
    for( int i = 0; i < len; i++ )
    {
        int idx = indices.at( i );
        if( idx == 0 )
        {
            // 当前夹点是圆心，移动图形
            m_insertPt += offset;       // 对圆心执行偏移变换
        }
        if( idx == 1 )
        {
            // 缩放圆
            // 1) 计算x轴的端点坐标
            AcGeVector3d v( AcGeVector3d::kXAxis );
            v.rotateBy( m_angle, AcGeVector3d::kZAxis );
            AcGePoint3d pt = m_insertPt + v * m_radius;
            // 2) 进行坐标偏移计算
            pt += offset;
            // 3) 计算新坐标与圆心之间的长度，并作为圆半径
            // 坐标相减，得到一个向量，然后得到向量长度
            m_radius = ( pt - m_insertPt ).length();
        }
    }
    return Acad::eOk;
}
void Sensor::readPropertyDataFromValues( const AcStringArray& values )
{
    CString strLenth;
    strLenth.Format( _T( "%s" ), values[0].kACharPtr() );
    m_temp = strLenth;
}
CommonSensor::CommonSensor()
{
    m_temp = _T( "" );
    m_type = _T( "DS" );
    m_unit = _T( "" );
    m_colorIndex = 238;
}
CommonSensor::CommonSensor( const AcGePoint3d& insertPt, double angle ): Sensor( insertPt, angle )
{
    m_temp = _T( "" );
    m_type = _T( "DS" );
    m_unit = _T( "" );
    m_colorIndex = 238;
}
void CommonSensor::regPropertyDataNames( AcStringArray& names ) const
{
    //names.append(_T("温度"));
}
TempeSensor::TempeSensor()
{
    m_temp = _T( "" );
    m_type = _T( "T" );
    m_unit = _T( "℃" );
    m_colorIndex = 6;
}
TempeSensor::TempeSensor( const AcGePoint3d& insertPt, double angle ): Sensor( insertPt, angle )
{
    m_temp = _T( "" );
    m_type = _T( "T" );
    m_unit = _T( "℃" );
    m_colorIndex = 6;
}
void TempeSensor::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "温度" ) );
}
FlowSensor::FlowSensor()
{
    m_temp = _T( "" );
    m_type = _T( "Q" );
    CString str;
    str.Format( _T( "m%s/min" ), MakeUpperText( _T( "3" ) ) ); // m^3/min
    m_unit = str;
}
FlowSensor::FlowSensor( const AcGePoint3d& insertPt, double angle ): Sensor( insertPt, angle )
{
    m_temp = _T( "" );
    m_type = _T( "Q" );
    CString str;
    str.Format( _T( "m%s/min" ), MakeUpperText( _T( "3" ) ) ); // m^3/min
    m_unit = str;
}
void FlowSensor::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "流量" ) );
}
PressSensor::PressSensor()
{
    m_temp = _T( "" );
    m_type = _T( "P" );
    m_unit = _T( "Pa" );
    m_colorIndex = 4;
}
PressSensor::PressSensor( const AcGePoint3d& insertPt, double angle ): Sensor( insertPt, angle )
{
    m_temp = _T( "" );
    m_type = _T( "P" );
    m_unit = _T( "Pa" );
    m_colorIndex = 4;
}
void PressSensor::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "压力" ) );
}
DifferPressSensor::DifferPressSensor()
{
    m_temp = _T( "" );
    m_type = _T( "ΔP" );
    m_unit = _T( "Pa" );
    m_colorIndex = 140;
}
DifferPressSensor::DifferPressSensor( const AcGePoint3d& insertPt, double angle ): Sensor( insertPt, angle )
{
    m_temp = _T( "" );
    m_type = _T( "ΔP" );
    m_unit = _T( "Pa" );
    m_colorIndex = 140;
}
void DifferPressSensor::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "压差" ) );
}
CH4Sensor::CH4Sensor()
{
    m_temp = _T( "" );
    CString str;
    str.Format( _T( "CH%s" ), MakeLowerText( _T( "4" ) ) );
    m_type = str;
    m_unit = _T( "%" );
    m_colorIndex = 3;
}
CH4Sensor::CH4Sensor( const AcGePoint3d& insertPt, double angle ): Sensor( insertPt, angle )
{
    m_temp = _T( "" );
    CString str;
    str.Format( _T( "CH%s" ), MakeLowerText( _T( "4" ) ) );
    m_type = str;
    m_unit = _T( "%" );
    m_colorIndex = 3;
}
void CH4Sensor::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "瓦斯浓度" ) );
}
COSensor::COSensor()
{
    m_temp = _T( "" );
    m_type = _T( "CO" );
    m_unit = _T( "%" );
    m_colorIndex = 1;
}
COSensor::COSensor( const AcGePoint3d& insertPt, double angle ): Sensor( insertPt, angle )
{
    m_radius = 3;
    m_temp = _T( "" );
    m_type = _T( "CO" );
    m_unit = _T( "%" );
    m_colorIndex = 1;
}
void COSensor::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "CO浓度" ) );
}
VentSensor::VentSensor()
{
    m_temp = _T( "" );
    m_type = _T( "V" );
    m_unit = _T( "m/s" );
    m_colorIndex = 8;
}
VentSensor::VentSensor( const AcGePoint3d& insertPt, double angle ): Sensor( insertPt, angle )
{
    m_temp = _T( "" );
    m_type = _T( "V" );
    m_unit = _T( "m/s" );
    m_colorIndex = 8;
}
void VentSensor::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "风速" ) );
}
