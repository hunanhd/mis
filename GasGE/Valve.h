#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// 阀门
class GASGE_EXPORT_API Valve : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( Valve ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    Valve() ;
    Valve( const AcGePoint3d& insertPt, double angle );
    virtual ~Valve();
    virtual void regPropertyDataNames( AcStringArray& names ) const;
    virtual void readPropertyDataFromValues( const AcStringArray& values );
    virtual void caclBackGroundMinPolygon( AcGePoint3dArray& pts );
private:
    void caculPts();
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
private:
    double m_lenth;          // 阀门的线长
    AcGePoint3d m_spt1;
    AcGePoint3d m_spt2;
    AcGePoint3d m_spt3;
    AcGePoint3d m_spt4;
    AcGePoint3d m_spt5;
    AcGePoint3d m_spt6;
    AcGePoint3d m_spt7;
} ;
#ifdef GASGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( Valve )
#endif