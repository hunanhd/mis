#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// ��ˮ��
class GASGE_EXPORT_API Tailrace : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( Tailrace ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    Tailrace() ;
    Tailrace( const AcGePoint3d& insertPt, double angle );
    virtual ~Tailrace();
    virtual void regPropertyDataNames( AcStringArray& names ) const;
    virtual void readPropertyDataFromValues( const AcStringArray& values );
    virtual void caclBackGroundMinPolygon( AcGePoint3dArray& pts );
private:
    void caculPts();
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
private:
    double m_lenth;
    AcGePoint3d m_pt0;
    AcGePoint3d m_pt1;
    AcGePoint3d m_pt2;
    AcGePoint3d m_pt3;
    AcGePoint3d m_pt4;
    AcGePoint3d m_pt5;
    AcGePoint3d m_pt6;
    AcGePoint3d m_pt7;
    AcGePoint3d m_pt8;
    AcGePoint3d m_pt9;
    AcGePoint3d m_pt10;
    AcGePoint3d m_pt11;
    AcGePoint3d m_pt12;
    AcGePoint3d m_pt13;
    AcGePoint3d m_pt14;
    AcGePoint3d m_pt15;
} ;
#ifdef GASGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( Tailrace )
#endif