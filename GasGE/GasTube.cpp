#include "StdAfx.h"
#include "GasTube.h"
Adesk::UInt32 GasTube::kCurrentVersionNumber = 1 ;
Adesk::UInt32 MainTube::kCurrentVersionNumber = 1 ;
Adesk::UInt32 TrunkTube::kCurrentVersionNumber = 1 ;
Adesk::UInt32 BranchTube::kCurrentVersionNumber = 1 ;
ACRX_NO_CONS_DEFINE_MEMBERS( GasTube, EdgeGE )
ACRX_DXF_DEFINE_MEMBERS ( MainTube, GasTube, AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation, 永久瓦斯瓦斯管路, GASGEAPP )
ACRX_DXF_DEFINE_MEMBERS ( TrunkTube, GasTube, AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation, 移动泵抽瓦斯管路, GASGEAPP )
ACRX_DXF_DEFINE_MEMBERS ( BranchTube, GasTube, AcDb::kDHL_CURRENT, AcDb::kMReleaseCurrent,
                          AcDbProxyEntity::kNoOperation, 移动泵排瓦斯管路, GASGEAPP )
GasTube::GasTube () : EdgeGE ()
{
}
GasTube::GasTube( const AcGePoint3d& startPt, const AcGePoint3d& endPt ) : EdgeGE( startPt, endPt )
{
}
GasTube::~GasTube ()
{
}
void GasTube::extendByLength( double length )
{
    AcGeVector3d v = m_endPt - m_startPt;
    v.normalize();
    m_endPt = m_endPt + v * length; // 修改末点坐标
}
Adesk::Boolean GasTube::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    AcGePoint3dArray pts;
    pts.append( m_startPt );
    pts.append( m_endPt );
    mode->geometry().worldLine( pts.asArrayPtr() );
    //DrawPolyLine( mode, m_startPt, m_endPt, 10 );
    return Adesk::kTrue;
}
//----- AcDbEntity protocols
Acad::ErrorStatus GasTube::customTransformBy( const AcGeMatrix3d& xform )
{
    m_startPt.transformBy( xform );
    m_endPt.transformBy( xform );
    return Acad::eOk;
}
//- Osnap points protocol
Acad::ErrorStatus GasTube::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    // 只捕捉1种类型的点：端点
    if( osnapMode != AcDb::kOsModeEnd )
        return Acad::eOk;
    Acad::ErrorStatus es = Acad::eOk;
    if ( osnapMode == AcDb::kOsModeEnd )
    {
        snapPoints.append( m_startPt );
        snapPoints.append( m_endPt );
    }
    return es;
}
//- Grip points protocol
Acad::ErrorStatus GasTube::customGetGripPoints (
    AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    //----- This method is never called unless you return eNotImplemented
    //----- from the new getGripPoints() method below (which is the default implementation)
    gripPoints.append( m_startPt );
    gripPoints.append( m_endPt );
    return Acad::eOk;
}
Acad::ErrorStatus GasTube::customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    //----- This method is never called unless you return eNotImplemented
    //----- from the new moveGripPointsAt() method below (which is the default implementation)
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        // 始节点
        if ( idx == 0 ) m_startPt += offset;
        if ( idx == 1 ) m_endPt += offset;
    }
    return Acad::eOk;
}
Acad::ErrorStatus GasTube::subGetGeomExtents( AcDbExtents& extents ) const
{
    assertReadEnabled();
    extents.addPoint( m_startPt );
    extents.addPoint( m_endPt );
    return Acad::eOk;
}
MainTube::MainTube( const AcGePoint3d& startPt, const AcGePoint3d& endPt ) : GasTube( startPt, endPt )
{
}
MainTube::MainTube()
{
}
TrunkTube::TrunkTube( const AcGePoint3d& startPt, const AcGePoint3d& endPt ) : GasTube( startPt, endPt )
{
}
TrunkTube::TrunkTube()
{
}
BranchTube::BranchTube( const AcGePoint3d& startPt, const AcGePoint3d& endPt ) : GasTube( startPt, endPt )
{
}
BranchTube::BranchTube()
{
}
