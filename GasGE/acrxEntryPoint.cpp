#include "StdAfx.h"
#include "resource.h"
// 定义注册服务名称
#ifndef GASGE_SERVICE_NAME
#define GASGE_SERVICE_NAME _T("GASGE_SERVICE_NAME")
#endif
//-----------------------------------------------------------------------------
//----- ObjectARX EntryPoint
class CGasGEApp : public AcRxArxApp
{
public:
    CGasGEApp () : AcRxArxApp () {}
    virtual AcRx::AppRetCode On_kInitAppMsg ( void* pkt )
    {
        // You *must* call On_kInitAppMsg here
        AcRx::AppRetCode retCode = AcRxArxApp::On_kInitAppMsg ( pkt ) ;
        acrxRegisterAppMDIAware( pkt );
        // 注册服务
        acrxRegisterService( GASGE_SERVICE_NAME );
        return ( retCode ) ;
    }
    virtual AcRx::AppRetCode On_kUnloadAppMsg ( void* pkt )
    {
        // TODO: Add your code here
        // You *must* call On_kUnloadAppMsg here
        AcRx::AppRetCode retCode = AcRxArxApp::On_kUnloadAppMsg ( pkt ) ;
        // 删除服务
        delete acrxServiceDictionary->remove( GASGE_SERVICE_NAME );
        return ( retCode ) ;
    }
    virtual void RegisterServerComponents ()
    {
    }
} ;
//-----------------------------------------------------------------------------
IMPLEMENT_ARX_ENTRYPOINT( CGasGEApp )

