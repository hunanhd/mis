#include "StdAfx.h"
#include "Valve.h"
#include "MineGE/DrawTool.h"
Adesk::UInt32 Valve::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS ( Valve,
                          DirGE, AcDb::kDHL_CURRENT,
                          AcDb::kMReleaseCurrent, AcDbProxyEntity::kNoOperation,
                          阀门, GASGEAPP )
Valve::Valve ()
{
    m_lenth = 4;
}
Valve::Valve( const AcGePoint3d& insertPt, double angle )
    : DirGE( insertPt, angle )
{
    m_lenth = 4;
    caculPts();
}
Valve::~Valve ()
{
}
Adesk::Boolean Valve::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    if( v.x < 0 ) v.negate();
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    caculPts();
    DrawLine( mode, m_insertPt, m_spt1 );
    DrawLine( mode, m_insertPt, m_spt2 );
    DrawLine( mode, m_insertPt, m_spt3 );
    DrawLine( mode, m_insertPt, m_spt4 );
    DrawLine( mode, m_insertPt, m_spt5 );
    DrawLine( mode, m_spt1, m_spt2 );
    DrawLine( mode, m_spt3, m_spt4 );
    DrawLine( mode, m_spt6, m_spt7 );
    return Adesk::kTrue;
}
Acad::ErrorStatus Valve::customTransformBy( const AcGeMatrix3d& xform )
{
    // 插入点变换
    m_insertPt.transformBy( xform );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.transformBy( xform );
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis ); // 变换后的旋转角度
    caculPts();
    return Acad::eOk;
}
Acad::ErrorStatus Valve::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    if( osnapMode != AcDb::kOsModeCen )
        return Acad::eOk;
    Acad::ErrorStatus es = Acad::eOk;
    if( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return es;
}
Acad::ErrorStatus Valve::customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    //gripPoints.append(m_spt1);             // 正方向上端端点作为夹点
    return Acad::eOk;
}
Acad::ErrorStatus Valve::customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        if ( idx == 0 )
        {
            m_insertPt += offset; 			// 插入点偏移
            caculPts();
        }
    }
    return Acad::eOk;
}
void Valve::caclBackGroundMinPolygon( AcGePoint3dArray& pts )
{
    pts.append( m_spt1 );
    pts.append( m_spt2 );
    pts.append( m_spt3 );
    pts.append( m_spt4 );
}
void Valve::caculPts()
{
    double pi = 3.1415926;
    //AcGePoint3d spt1,spt2,spt3,spt4,spt5,spt6,spt7;
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.rotateBy( pi / 6, AcGeVector3d::kZAxis );
    m_spt1 = m_insertPt + v * m_lenth;
    v.rotateBy( -2 * pi / 3, AcGeVector3d::kZAxis );
    m_spt2 = m_spt1 + v * m_lenth;
    v.rotateBy( -pi / 3, AcGeVector3d::kZAxis );
    m_spt3 = m_insertPt + v * m_lenth;
    v.rotateBy( -2 * pi / 3, AcGeVector3d::kZAxis );
    m_spt4 = m_spt3 + v * m_lenth;
    m_spt5 = m_insertPt + v * m_lenth * 3 / 4;
    v.rotateBy( pi * 0.5, AcGeVector3d::kZAxis );
    m_spt6 = m_spt5 + v * 1.7320508075688772 * m_lenth / 4;
    v.rotateBy( pi, AcGeVector3d::kZAxis );
    m_spt7 = m_spt5 + v * m_lenth * 1.7320508075688772 / 4;
}
void Valve::readPropertyDataFromValues( const AcStringArray& values )
{
    CString strLenth;
    strLenth.Format( _T( "%s" ), values[0].kACharPtr() );
    m_lenth = _tstof( strLenth );
    if( 0 >= m_lenth ) m_lenth = 5;
    caculPts();
    // 	acutPrintf(_T("\n长度(字符串):%s"),strLenth);
    // 	acutPrintf(_T("\n长度:%lf"),m_lenth );
}
void Valve::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "大小" ) );
}