#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// ��������
class GASGE_EXPORT_API BackFire : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( BackFire ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    BackFire() ;
    BackFire( const AcGePoint3d& insertPt, double angle );
    virtual ~BackFire();
    virtual void regPropertyDataNames( AcStringArray& names ) const;
    virtual void readPropertyDataFromValues( const AcStringArray& values );
    virtual void caclBackGroundMinPolygon( AcGePoint3dArray& pts );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
private:
    double m_radius;
} ;
#ifdef GASGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( BackFire )
#endif