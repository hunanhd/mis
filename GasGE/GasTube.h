#pragma once
#include "MineGE/EdgeGE.h"
#include "dlimexp.h"
// 瓦斯抽采管路抽象类
class GASGE_EXPORT_API GasTube : public EdgeGE
{
public:
    ACRX_DECLARE_MEMBERS( GasTube ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    GasTube();
    GasTube( const AcGePoint3d& startPt, const AcGePoint3d& endPt );
    virtual ~GasTube();
    virtual void extendByLength( double length );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    // 变换操作(移动、选择、镜像)--目前暂不考虑"镜像"操作
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    //- Osnap points protocol
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    //- Grip points protocol
    virtual Acad::ErrorStatus customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset ) ;
    // 包围盒重载(用于确定缩放的范围)
    // 一个"紧凑"的3d包围盒(立方体)
    virtual Acad::ErrorStatus subGetGeomExtents( AcDbExtents& extents ) const;
} ;
// 永久瓦斯抽采管路
class GASGE_EXPORT_API MainTube : public GasTube
{
public:
    ACRX_DECLARE_MEMBERS( MainTube ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    MainTube() ;
    MainTube( const AcGePoint3d& startPt, const AcGePoint3d& endPt );
};
// 移动泵抽瓦斯管路
class GASGE_EXPORT_API TrunkTube : public GasTube
{
public:
    ACRX_DECLARE_MEMBERS( TrunkTube ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    TrunkTube() ;
    TrunkTube( const AcGePoint3d& startPt, const AcGePoint3d& endPt );
};
// 移动泵排瓦斯管路
class GASGE_EXPORT_API BranchTube : public GasTube
{
public:
    ACRX_DECLARE_MEMBERS( BranchTube ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    BranchTube() ;
    BranchTube( const AcGePoint3d& startPt, const AcGePoint3d& endPt );
};

#ifdef GASGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( GasTube )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( MainTube )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( TrunkTube )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( BranchTube )
#endif
