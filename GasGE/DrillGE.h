#pragma once
#include "MineGE/EdgeGE.h"
#include "dlimexp.h"
// 瓦斯抽采钻孔图元
class GASGE_EXPORT_API DrillGE : public EdgeGE
{
public:
    ACRX_DECLARE_MEMBERS( DrillGE ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    DrillGE();
    DrillGE( const AcGePoint3d& startPt, const AcGePoint3d& endPt );
    virtual ~DrillGE();
    virtual void extendByLength( double length );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    // 变换操作(移动、选择、镜像)--目前暂不考虑"镜像"操作
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    //- Osnap points protocol
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    //- Grip points protocol
    virtual Acad::ErrorStatus customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset ) ;
    // 包围盒重载(用于确定缩放的范围)
    // 一个"紧凑"的3d包围盒(立方体)
    virtual Acad::ErrorStatus subGetGeomExtents( AcDbExtents& extents ) const;
} ;
#ifdef GASGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( DrillGE )
#endif
