#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// 传感器抽象类
class GASGE_EXPORT_API Sensor : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( Sensor ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    Sensor() ;
    Sensor( const AcGePoint3d& insertPt, double angle );
    virtual ~Sensor();
    virtual void regPropertyDataNames( AcStringArray& names ) const {}
    virtual void readPropertyDataFromValues( const AcStringArray& values );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
protected:
    double m_radius; //传感器半径
    CString m_temp;	//传感器显示值
    CString m_type;	//传感器图例
    CString m_unit; //传感器单位
    int m_colorIndex;
} ;
// 通用传感器
class GASGE_EXPORT_API CommonSensor : public Sensor
{
public:
    ACRX_DECLARE_MEMBERS( CommonSensor ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
    virtual void regPropertyDataNames( AcStringArray& names ) const;
public:
    CommonSensor() ;
    CommonSensor( const AcGePoint3d& insertPt, double angle );
};
// 温度传感器
class GASGE_EXPORT_API TempeSensor : public Sensor
{
public:
    ACRX_DECLARE_MEMBERS( TempeSensor ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
    virtual void regPropertyDataNames( AcStringArray& names ) const;
public:
    TempeSensor() ;
    TempeSensor( const AcGePoint3d& insertPt, double angle );
};
// 流量传感器
class GASGE_EXPORT_API FlowSensor : public Sensor
{
public:
    ACRX_DECLARE_MEMBERS( FlowSensor ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
    virtual void regPropertyDataNames( AcStringArray& names ) const;
public:
    FlowSensor() ;
    FlowSensor( const AcGePoint3d& insertPt, double angle );
};
// 压力传感器
class GASGE_EXPORT_API PressSensor : public Sensor
{
public:
    ACRX_DECLARE_MEMBERS( PressSensor ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
    virtual void regPropertyDataNames( AcStringArray& names ) const;
public:
    PressSensor() ;
    PressSensor( const AcGePoint3d& insertPt, double angle );
};
// 压差传感器
class GASGE_EXPORT_API DifferPressSensor : public Sensor
{
public:
    ACRX_DECLARE_MEMBERS( DifferPressSensor ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
    virtual void regPropertyDataNames( AcStringArray& names ) const;
public:
    DifferPressSensor() ;
    DifferPressSensor( const AcGePoint3d& insertPt, double angle );
};
// 瓦斯浓度传感器
class GASGE_EXPORT_API CH4Sensor : public Sensor
{
public:
    ACRX_DECLARE_MEMBERS( CH4Sensor ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
    virtual void regPropertyDataNames( AcStringArray& names ) const;
public:
    CH4Sensor() ;
    CH4Sensor( const AcGePoint3d& insertPt, double angle );
};
// CO浓度传感器
class GASGE_EXPORT_API COSensor : public Sensor
{
public:
    ACRX_DECLARE_MEMBERS( COSensor ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
    virtual void regPropertyDataNames( AcStringArray& names ) const;
public:
    COSensor() ;
    COSensor( const AcGePoint3d& insertPt, double angle );
};
// 风速传感器
class GASGE_EXPORT_API VentSensor : public Sensor
{
public:
    ACRX_DECLARE_MEMBERS( VentSensor ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
    virtual void regPropertyDataNames( AcStringArray& names ) const;
public:
    VentSensor() ;
    VentSensor( const AcGePoint3d& insertPt, double angle );
};
#ifdef GASGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( Sensor )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( CommonSensor )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( TempeSensor )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( FlowSensor )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( PressSensor )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( DifferPressSensor )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( CH4Sensor )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( COSensor )
ACDB_REGISTER_OBJECT_ENTRY_AUTO( VentSensor )
#endif