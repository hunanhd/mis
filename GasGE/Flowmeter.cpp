#include "StdAfx.h"
#include "Flowmeter.h"
#include "MineGE/DrawTool.h"
Adesk::UInt32 Flowmeter::kCurrentVersionNumber = 1 ;
ACRX_DXF_DEFINE_MEMBERS ( Flowmeter,
                          DirGE, AcDb::kDHL_CURRENT,
                          AcDb::kMReleaseCurrent, AcDbProxyEntity::kNoOperation,
                          流量计, DEFGEAPP )
Flowmeter::Flowmeter ()
{
    m_lenth = 5;
}
Flowmeter::Flowmeter( const AcGePoint3d& insertPt, double angle )
    : DirGE( insertPt, angle )
{
    m_lenth = 5;
    caculPts();
}
Flowmeter::~Flowmeter ()
{
}
Adesk::Boolean Flowmeter::customWorldDraw( AcGiWorldDraw* mode )
{
    assertReadEnabled () ;
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    if( v.x < 0 ) v.negate();
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis );
    caculPts();
    DrawLine( mode, m_spt1, m_spt2 );
    DrawLine( mode, m_spt2, m_spt3 );
    DrawLine( mode, m_spt3, m_spt4 );
    DrawLine( mode, m_spt4, m_spt1 );
    DrawLine( mode, m_spt1, m_spt3 );
    AcGePoint3dArray pts;
    pts.append( m_spt1 );
    pts.append( m_spt4 );
    pts.append( m_spt3 );
    //caclBackGroundMinPolygon(pts);
    DrawBackGround( mode, pts, 7 );
    return Adesk::kTrue;
    return Adesk::kTrue;
}
Acad::ErrorStatus Flowmeter::customTransformBy( const AcGeMatrix3d& xform )
{
    // 插入点变换
    m_insertPt.transformBy( xform );
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.transformBy( xform );
    m_angle = v.angleTo( AcGeVector3d::kXAxis, -AcGeVector3d::kZAxis ); // 变换后的旋转角度
    caculPts();
    return Acad::eOk;
}
Acad::ErrorStatus Flowmeter::customGetOsnapPoints (
    AcDb::OsnapMode osnapMode,
    Adesk::GsMarker gsSelectionMark,
    const AcGePoint3d& pickPoint,
    const AcGePoint3d& lastPoint,
    const AcGeMatrix3d& viewXform,
    AcGePoint3dArray& snapPoints,
    AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    if( osnapMode != AcDb::kOsModeCen )
        return Acad::eOk;
    Acad::ErrorStatus es = Acad::eOk;
    if( osnapMode == AcDb::kOsModeCen )
    {
        snapPoints.append( m_insertPt );
    }
    return es;
}
Acad::ErrorStatus Flowmeter::customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const
{
    assertReadEnabled () ;
    gripPoints.append( m_insertPt );
    //gripPoints.append(m_spt1);
    return Acad::eOk;
}
Acad::ErrorStatus Flowmeter::customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset )
{
    assertWriteEnabled () ;
    for( int i = 0; i < indices.length(); i++ )
    {
        int idx = indices.at( i );
        if ( idx == 0 )
        {
            m_insertPt += offset; 			// 插入点偏移
            caculPts();
        }
    }
    return Acad::eOk;
}
void Flowmeter::caculPts()
{
    double pi = 3.1415926;
    AcGePoint3d spt;
    AcGeVector3d v( AcGeVector3d::kXAxis );
    v.rotateBy( m_angle, AcGeVector3d::kZAxis );
    v.rotateBy( pi * 0.5, AcGeVector3d::kZAxis );
    spt = m_insertPt + v * m_lenth / 2;
    v.rotateBy( -pi * 0.5, AcGeVector3d::kZAxis );
    m_spt1 = spt + v * m_lenth / 4;
    v.rotateBy( -pi * 0.5, AcGeVector3d::kZAxis );
    m_spt2 = m_spt1 + v * m_lenth;
    v.rotateBy( -pi * 0.5, AcGeVector3d::kZAxis );
    m_spt3 = m_spt2 + v * m_lenth / 2;
    v.rotateBy( -pi * 0.5, AcGeVector3d::kZAxis );
    m_spt4 = m_spt3 + v * m_lenth;
}
void Flowmeter::caclBackGroundMinPolygon( AcGePoint3dArray& pts )
{
    pts.append( m_spt1 );
    pts.append( m_spt2 );
    pts.append( m_spt3 );
    pts.append( m_spt4 );
}
void Flowmeter::regPropertyDataNames( AcStringArray& names ) const
{
    names.append( _T( "大小" ) );
}
void Flowmeter::readPropertyDataFromValues( const AcStringArray& values )
{
    CString strLenth;
    strLenth.Format( _T( "%s" ), values[0].kACharPtr() );
    m_lenth = _tstof( strLenth );
    if( 0 >= m_lenth ) m_lenth = 10;
    caculPts();
}
