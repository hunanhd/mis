#pragma once
#include "MineGE/EdgeGE.h"
#include "dlimexp.h"
// ��˹��
class GASGE_EXPORT_API GasPump : public EdgeGE
{
public:
    ACRX_DECLARE_MEMBERS( GasPump ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    GasPump() ;
    GasPump( const AcGePoint3d& startPt, const AcGePoint3d& endPt );
    virtual ~GasPump();
    virtual void caclBackGroundMinPolygon( AcGePoint3dArray& pts );
    virtual void reverse();
    virtual void extendByLength( double length );
    virtual void regPropertyDataNames( AcStringArray& names ) const;
    virtual void readPropertyDataFromValues( const AcStringArray& values );
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    // �任����(�ƶ���ѡ�񡢾���)--Ŀǰ�ݲ�����"����"����
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints ( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customMoveGripPointsAt ( const AcDbIntArray& indices, const AcGeVector3d& offset ) ;
public:
    void update();
    double m_radius;
    double m_angle;
} ;
#ifdef GASGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( GasPump )
#endif