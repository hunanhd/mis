#pragma once
#include "MineGE/TagGE.h"
#include "dlimexp.h"
// �ⶨ��
class GASGE_EXPORT_API DetermineHole : public DirGE
{
public:
    ACRX_DECLARE_MEMBERS( DetermineHole ) ;
protected:
    static Adesk::UInt32 kCurrentVersionNumber ;
public:
    DetermineHole() ;
    DetermineHole( const AcGePoint3d& insertPt, double angle );
    virtual ~DetermineHole();
    virtual void regPropertyDataNames( AcStringArray& names ) const;
    virtual void readPropertyDataFromValues( const AcStringArray& values );
private:
    void caculPts();
protected:
    virtual Adesk::Boolean customWorldDraw ( AcGiWorldDraw* mode );
    virtual Acad::ErrorStatus customTransformBy( const AcGeMatrix3d& xform );
    virtual Acad::ErrorStatus customGetOsnapPoints (
        AcDb::OsnapMode osnapMode,
        Adesk::GsMarker gsSelectionMark,
        const AcGePoint3d& pickPoint,
        const AcGePoint3d& lastPoint,
        const AcGeMatrix3d& viewXform,
        AcGePoint3dArray& snapPoints,
        AcDbIntArray& geomIds ) const ;
    virtual Acad::ErrorStatus customGetGripPoints( AcGePoint3dArray& gripPoints, AcDbIntArray& osnapModes, AcDbIntArray& geomIds ) const;
    virtual Acad::ErrorStatus customMoveGripPointsAt( const AcDbIntArray& indices, const AcGeVector3d& offset );
    virtual void caclBackGroundMinPolygon( AcGePoint3dArray& pts );
private:
    double m_lenth;
    AcGePoint3d m_spt1;
    AcGePoint3d m_spt2;
    AcGePoint3d m_spt3;
    AcGePoint3d m_spt4;
} ;
#ifdef GASGE_MODULE
ACDB_REGISTER_OBJECT_ENTRY_AUTO( DetermineHole )
#endif