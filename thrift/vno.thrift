
namespace cpp vno
namespace php vno
namespace py vno
namespace csharp vno

service VnoService {
	// 测试函数
	i32 Test(1:i32 num1, 2:i32 num2),
	// 网络解算
	string RunVno(1:string graph_datas),
	// 查找进风井
	string GetSourceEdges(1:string graph_datas),
	// 查找回风井
	string GetSinkEdges(1:string graph_datas),
	// 连通性判定
	bool IsConnected(1:string graph_datas),
	// 是否有向无环图
	bool IsDag(1:string graph_datas),
	// 是否有单向回路
	bool HasCycles(1:string graph_datas),
	// 查找连通块
	string CC(1:string graph_datas),
	// 查找单向回路
	string SCC(1:string graph_datas),
	// 查找负风量分支
	string GetNegativeEdges(1:string graph_datas),
	// 查找固定风量分支
	string GetFixQEdges(1:string graph_datas),
	
	// 发送命令到cad(例如zoom a),第2个参数switch_to_cad表示是否激活cad窗口到最前面
	bool SendCommandToCAD(1:string cmd, 2:bool switch_to_cad),
	// 向rpc请求需要cad提供数据,rpc会分配一个密钥(secret_key)
	string RequestJsonDatasFromCAD(1:string input_datas),
	// 通过密钥向rpc的缓存请求数据(cad的数据都放在rpc服务器的缓存里)
	string GetJsonDatasFromRpcCache(1:string secret_key),
	// cad主动发送数据到rpc,rpc将数据进行缓存,客户端凭借密钥从缓存中提取数据
	void PostJsonDatasFromCAD(1:string secret_key, 2:string input_datas, 4:string out_datas),
}