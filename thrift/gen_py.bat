@echo off

rem 生成py代码
thrift.exe -r --gen py vno.thrift

rem 新建文件夹
rd /s /q "..\python\vno\"
md "..\python\vno\"
xcopy "gen-py\vno" "..\python\vno\"  /c /e /y

rem 暂停
pause