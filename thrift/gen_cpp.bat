@echo off

rem 生成c++代码
thrift.exe -r --gen cpp vno.thrift

rem 新建文件夹
rem rd /s /q "..\ArxVno\gen-cpp\"
rem md "..\ArxVno\gen-cpp\"
rem 复制通过IDL生成的cpp代码
rem xcopy "gen-cpp" "..\ArxVno\gen-cpp\"  /c /e /y

rem 复制文件
rem copy /y RpcClient.h "..\ArxVno\"
rem copy /y SQLClientHelper.h "..\ArxVno\"
rem copy /y SQLClientHelper.cpp "..\ArxVno\"

rem 暂停
pause