@echo off

rem 生成py代码
thrift.exe -r --gen csharp vno.thrift

rem 新建文件夹
rem xcopy "gen-csharp" "..\MisVno\"  /c /e /y
rem c#会自动把子文件的文件复制到项目的根目录下，所以复制子文件夹就没有必要的，直接复制文件
xcopy "gen-csharp\vno" "..\MisVno\"  /c /e /y

rem 暂停
pause